@ECHO off
ECHO --- Starting Thinklab REST server in debug mode ----
ECHO Max memory is %THINKLAB_MAX_MEMORY%
REM Server start script that assumes a root dir with all compiled projects.
java -Xms1024M -Xmx8196M -XX:MaxPermSize=512m -Xdebug  -Xbootclasspath/p:lib/jsr166.jar -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=1044 -Djava.library.path="lib/ext/" -cp "bin/;../org.integratedmodelling.thinklab.api/bin/;../org.integratedmodelling.thinklab.common/bin/;../org.integratedmodelling.thinklab.resources/bin/;../org.integratedmodelling.thinklab.auth/bin/;../org.integratedmodelling.thinklab.common/lib/*;lib/*;../org.integratedmodelling.thinklab.auth/lib/*;../org.integratedmodelling.thinkql/bin/" org.integratedmodelling.thinklab.main.RESTServer > thinklab.log
ECHO -----------------------------
ECHO --- Thinklab REST debug server stopped ---

