#!/bin/sh

# change these as necessary: for a collab/knowledge server 4G should be OK; a modeling server can use all the memory you give it.
# JAVA_OPTS="-Xms1024M -Xmx8196M -XX:MaxPermSize=256m -server"

# where this instance's installation is located.
# THINKLAB_HOME=/opt/tl0

# change the following two to define the REST root URL context and the port the server will run on
# export THINKLAB_REST_CONTEXT="tl0"
# export THINKLAB_REST_PORT=8182

# this points to the property file that configures the collaboration environment. If absent, the server will not serve collaboration functions,.
# export THINKLAB_AUTH_PROPERTIES=/opt/collab/collab.properties

cd ${THINKLAB_HOME}
java  ${JAVA_OPTS} -Djava.library.path="${THINKLAB_HOME}/lib/ext/" -cp "${THINKLAB_HOME}/lib/*" org.integratedmodelling.thinklab.main.RESTServer > ${THINKLAB_HOME}/thinklab.log

