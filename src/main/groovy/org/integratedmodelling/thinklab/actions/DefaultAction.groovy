package org.integratedmodelling.thinklab.actions

import org.integratedmodelling.thinklab.Thinklab
import org.integratedmodelling.thinklab.api.knowledge.IConcept
import org.integratedmodelling.thinklab.api.knowledge.IProperty
import org.integratedmodelling.thinklab.api.listeners.IMonitor


/**
 * The base class for a Thinklab action. No space or time semantics built in.
 * 
 * @author Ferd
 *
 */
abstract class DefaultAction extends Script {

    public class V {
    }

    protected void info(String text) {
        Object o = getBinding().getVariable("_monitor");
        if (o != null && o instanceof IMonitor) {
            ((IMonitor)o).info(text);
        }
    }

    protected void warning(String text) {
        Object o = getBinding().getVariable("_monitor");
        if (o != null && o instanceof IMonitor) {
            ((IMonitor)o).warn(text);
        }
    }

    protected void error(String text) {
        Object o = getBinding().getVariable("_monitor");
        if (o != null && o instanceof IMonitor) {
            ((IMonitor)o).error(text);
        }
    }

    /*
     * window into the knowledge manager
     */
    protected IConcept _getConcept(String string) {
        return Thinklab.c(string);
    }

    protected IProperty _getProperty(String string) {
        return Thinklab.p(string);
    }

    @Override
    public Object run() {
        // TODO Auto-generated method stub
        return super.run();
    }
}
