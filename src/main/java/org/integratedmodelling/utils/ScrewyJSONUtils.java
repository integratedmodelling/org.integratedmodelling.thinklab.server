package org.integratedmodelling.utils;

import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ScrewyJSONUtils {

    static public Object wrap(Object o) throws JSONException {
        
        Object ret = null;
        
        if (o == null) {
            
          ret = JSONObject.NULL;  
        
        } else if (o instanceof Map<?,?>) {
            
            ret = new JSONObject();
            for (Object k : ((Map<?,?>)o).keySet()) {
                putTheBastard((JSONObject) ret, k.toString(), ((Map<?,?>)o).get(k));
            }
            
        } else if (o instanceof List<?>) {
            
            ret = new JSONArray();
            for (Object e : ((List<?>)o)) {
                addTheBastard((JSONArray) ret, e);
            }
            
        } else if (o.getClass().isArray()) {

            ret = new JSONArray();
            for (Object e : (Object[])o) {
                addTheBastard((JSONArray) ret, e);
            }
            
        } else if (o instanceof String || o instanceof Number) {
            return o;
        }
        
        return ret;
    }
    
    /*
     * stuck with idiotic org.json, which only uses literal polymorphic constructors and 
     * provides an idiotic one with Object arg that stringifies everything.
     */
    private static void putTheBastard(JSONObject ret, String key, Object o) throws JSONException {

        if (o == null) {
            ret.put(key, JSONObject.NULL);
        } else if (o instanceof List<?>) {
            ret.put(key, (JSONArray)wrap(o));
        } else if (o instanceof Map<?,?>) {
            ret.put(key, (JSONObject)wrap(o));
        } else if (o instanceof String) {
            ret.put(key, (String)o);
        } else if (o instanceof Number) {
            ret.put(key, (Number)o);
        }        
    }

    // never seen a worse API before.
    private static void addTheBastard(JSONArray ret, Object o) throws JSONException {

        if (o == null) {
            ret.put(JSONObject.NULL);
        } else if (o instanceof List<?>) {
            ret.put((JSONArray)wrap(o));
        } else if (o instanceof Map<?,?>) {
            ret.put((JSONObject)wrap(o));
        } else if (o instanceof String) {
            ret.put((String)o);
        } else if (o instanceof Number) {
            ret.put((Number)o);
        }
        
    }
    
}
