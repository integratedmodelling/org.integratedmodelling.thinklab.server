package org.integratedmodelling.thinklab.scripting;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.modelling.ICurrency;
import org.integratedmodelling.thinklab.api.modelling.IModel;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.api.modelling.parsing.IConceptDefinition;
import org.integratedmodelling.thinklab.api.modelling.parsing.INamespaceDefinition;
import org.integratedmodelling.thinklab.common.utils.CamelCase;
import org.integratedmodelling.thinklab.modelling.lang.ConceptObject;
import org.integratedmodelling.thinklab.modelling.lang.Measurement;
import org.integratedmodelling.thinklab.modelling.lang.Model;
import org.integratedmodelling.thinklab.modelling.lang.Presence;
import org.integratedmodelling.thinklab.modelling.lang.Ranking;
import org.integratedmodelling.thinklab.modelling.lang.UnitDefinition;

/**
 * Builder of various models.
 * 
 * @author Ferd
 *
 */
public class ModelFactory {

    private static IConceptDefinition cdef(IConcept c) {
        if (c == null)
            return null;
        IConceptDefinition ret = new ConceptObject();
        ret.setId(c.getLocalName());
        ret.setNamespace((INamespaceDefinition) Thinklab.get().getNamespace(c.getConceptSpace()));
        return ret;
    }

    public static IModel measure(IConcept observable, String unit, INamespace namespace)
            throws ThinklabException {

        Measurement measurement = new Measurement();
        measurement.setId(CamelCase.toLowerCase(observable.getLocalName(), '-'));
        measurement.addObservable(cdef(observable));
        measurement.setUnit(new UnitDefinition(unit));
        Model model = new Model();
        model.setNamespace((INamespaceDefinition) namespace);
        model.wrap(measurement);
        model.initialize(Thinklab.get().getRootResolver());
        return model;
    }

    public static IModel count(IConcept observable, String unit) {
        return null;
    }

    public static IModel rank(IConcept observable, INamespace namespace) {
        Ranking ranking = new Ranking();
        ranking.setId(CamelCase.toLowerCase(observable.getLocalName(), '-'));
        ranking.addObservable(cdef(observable));
        Model model = new Model();
        model.setNamespace((INamespaceDefinition) namespace);
        model.wrap(ranking);
        model.initialize(Thinklab.get().getRootResolver());
        return model;
    }

    public static IModel presence(IConcept observable, INamespace namespace) {

        Presence presence = new Presence();
        presence.setId(CamelCase.toLowerCase(observable.getLocalName(), '-'));
        presence.addObservable(cdef(observable));
        Model model = new Model();
        model.setNamespace((INamespaceDefinition) namespace);
        model.wrap(presence);
        model.initialize(Thinklab.get().getRootResolver());
        return model;
    }

    public static IModel presence(IObservable observable, INamespace namespace) {

        Presence presence = new Presence();
        presence.setId(CamelCase.toLowerCase(observable.getLocalName(), '-'));
        if (observable.getInherentType() != null) {
            presence.setInherentSubjectType(cdef(observable.getInherentType()));
        }
        presence.addObservable(cdef(observable.getType()));
        Model model = new Model();
        model.setNamespace((INamespaceDefinition) namespace);
        model.wrap(presence);
        model.initialize(Thinklab.get().getRootResolver());
        return model;
    }

    public static IModel classify(IConcept observable, Object... classifiers) {
        return null;
    }

    public static IModel probability(IConcept observable) {
        return null;
    }

    public static IModel percentage(IConcept observable, IConcept generic) {
        return null;
    }

    public static IModel proportion(IConcept observable, IConcept generic) {
        return null;
    }

    public static IModel ratio(IConcept observable, IConcept compareTo) {
        return null;
    }

    public static IModel value(IConcept observable, ICurrency currency) {
        return null;
    }

    public static IModel uncertainty(IConcept observable) {
        return null;
    }
}
