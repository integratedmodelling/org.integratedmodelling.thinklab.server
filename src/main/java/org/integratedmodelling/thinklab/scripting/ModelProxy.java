package org.integratedmodelling.thinklab.scripting;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.modelling.IModel;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.api.modelling.parsing.IConceptDefinition;
import org.integratedmodelling.thinklab.api.modelling.parsing.INamespaceDefinition;
import org.integratedmodelling.thinklab.common.utils.CamelCase;
import org.integratedmodelling.thinklab.modelling.lang.ConceptObject;
import org.integratedmodelling.thinklab.modelling.lang.Measurement;
import org.integratedmodelling.thinklab.modelling.lang.Model;
import org.integratedmodelling.thinklab.modelling.lang.Presence;
import org.integratedmodelling.thinklab.modelling.lang.Ranking;
import org.integratedmodelling.thinklab.modelling.lang.UnitDefinition;

/**
 * Proxy for a model that can be created directly in a script using the Groovy API.
 * 
 * @author Ferd
 *
 */
public class ModelProxy {

    IModel _model;

    public IModel getModel() {
        return _model;
    }

    public ModelProxy discretize(Object... discretization) {
        // add discretization, reinitialize
        return this;
    }

    public ModelProxy value(Object value) {
        // add constant data source
        return this;
    }

    public ModelProxy withData(Object dataSource) {
        // add datasource
        return this;
    }

}
