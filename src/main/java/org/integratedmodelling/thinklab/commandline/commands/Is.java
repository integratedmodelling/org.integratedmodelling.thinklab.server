/**
 * Copyright 2011 The ARIES Consortium (http://www.ariesonline.org) and
 * www.integratedmodelling.org. 

   This file is part of Thinklab.

   Thinklab is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published
   by the Free Software Foundation, either version 3 of the License,
   or (at your option) any later version.

   Thinklab is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Thinklab.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.integratedmodelling.thinklab.commandline.commands;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.runtime.ISession;
import org.integratedmodelling.thinklab.command.Command;
import org.integratedmodelling.thinklab.interfaces.annotations.ThinklabCommand;
import org.integratedmodelling.thinklab.interfaces.commands.ICommandHandler;

@ThinklabCommand(
        name = "is",
        argumentNames = "c1,c2",
        argumentTypes = "thinklab:Text,thinklab:Text",
        argumentDescriptions = "pop,pop")
public class Is implements ICommandHandler {

    public Object execute(Command command, ISession session) throws ThinklabException {

        // TODO this should figure out what the semantic type is for, cross
        // check properly, and
        // call the appropriate methods. So far it only handles concepts.
        String s1 = command.getArgumentAsString("c1");
        String s2 = command.getArgumentAsString("c2");

        boolean res = Thinklab.c(s1).is(Thinklab.c(s2));

        return Thinklab.get().annotate(res);
    }

}
