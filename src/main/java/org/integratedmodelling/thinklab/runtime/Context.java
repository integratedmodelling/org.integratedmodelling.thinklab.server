package org.integratedmodelling.thinklab.runtime;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabUnsupportedOperationException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.lang.IRemoteSerializable;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.listeners.IMonitorable;
import org.integratedmodelling.thinklab.api.modelling.ICoverage;
import org.integratedmodelling.thinklab.api.modelling.IDataset;
import org.integratedmodelling.thinklab.api.modelling.IModel;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.api.modelling.IObservation;
import org.integratedmodelling.thinklab.api.modelling.IState;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.api.modelling.ISubjectGenerator;
import org.integratedmodelling.thinklab.api.modelling.agents.IObservationGraphNode;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.api.runtime.IContext;
import org.integratedmodelling.thinklab.api.runtime.ISession;
import org.integratedmodelling.thinklab.api.runtime.ITask;
import org.integratedmodelling.thinklab.modelling.lang.Subject;
import org.integratedmodelling.thinklab.modelling.lang.SubjectGenerator;
import org.integratedmodelling.thinklab.modelling.lang.agents.TemporalCausalGraph;
import org.integratedmodelling.thinklab.modelling.monitoring.Monitor;
import org.integratedmodelling.thinklab.modelling.states.State;

import com.google.common.collect.ImmutableMap;

/**
 * Candidate to
 * @author Ferd
 * 
 */
public class Context implements IContext, IMonitorable, IRemoteSerializable {

    // values for _status
    final static int EMPTY          = 0;
    final static int DEFINED        = 1;
    final static int INITIALIZED    = 2;
    final static int RUNNING        = 3;
    final static int CONTEXTUALIZED = 4;
    final static int ERROR          = -1;
    final static int INTERRUPTED    = -2;

    static Long      ___ID          = 1L;

    private static long nextId() {
        synchronized (___ID) {
            return ___ID++;
        }
    }

    class RunThread implements Runnable {

        @Override
        public void run() {
            _status = RUNNING;
            try {
                /**
                 * TODO install listener to be notified of structural changes
                 */
                _subject.contextualize();
            } catch (ThinklabException e) {
                _monitor.error(e);
                _status = ERROR;
                return;
            }
            _status = _monitor.isStopped() ? INTERRUPTED : CONTEXTUALIZED;
            if (_status == CONTEXTUALIZED) {
                _monitor.info("coverage from observation of " + _subject.getId() + " is "
                        + _coverage.getCoverage(), null);
            }
        }
    }

    /*
     * Current subject, observer and coverage - modified at each observation
     */
    ISubject                  _subject;
    ICoverage                 _coverage;

    // this is set only when the object is created and is passed along to copies
    long                      _contextId;
    int                       _status                          = EMPTY;
    int                       _currentTime                     = -1;
    boolean                   _reset                           = false;

    /*
     * these don't change or only change explicitly from the API
     */
    ISession                  _session;
    Monitor                   _monitor;
    List<String>              _scenarios                       = new ArrayList<String>();
    INamespace                _namespace;
    String                    _name;

    // if false, all observations made will not call the subject accessor's initializers.
    boolean                   _callSubjectAccessorInitializers = true;
    private long              _timeOfLastChange                = new Date().getTime();
    private String            _subjectToResolve;
    private ISubjectGenerator _rootGenerator;

    public Context(ISession session, IMonitor monitor, String name) {
        _session = session;
        _namespace = session.getNamespace();
        _name = name;
        _monitor = (Monitor) monitor;
        _contextId = nextId();
    }

    public Context(Context c) {

        /**
         * TODO some of these probably must be deep copies - particularly the observer.
         */
        this._callSubjectAccessorInitializers = c._callSubjectAccessorInitializers;
        this._coverage = c._coverage;
        this._monitor = c._monitor;
        this._namespace = c._namespace;
        this._session = c._session;
        this._status = c._status;
        this._contextId = c._contextId;
        this._subject = c._subject;
        this._currentTime = c._currentTime;
        this._reset = c._reset;
        this._rootGenerator = c._rootGenerator;
    }

    /**
     * Use to create the main subject or a single contextual observation in an existing one.
     * 
     * @param observable
     * @return
     */
    @Override
    public ITask observeAsynchronous(ITask.Listener listener, Object... observables) {

        Task ret = null;
        ret = observeInternal(listener, observables);
        ret.start();
        _reset = false;
        // FIXME move to task
        _timeOfLastChange = new Date().getTime();
        return ret;
    }

    /**
     * Call after critical operations to decide whether to continue.
     * 
     * @return
     */
    public boolean hasErrors() {
        return _monitor.hasErrors();
    }

    // /**
    // * Create visualization in passed directory. If path is not absolute, create it in the configured
    // Thinklab
    // * archive directory (defaults to ${HOME}/thinklab/archive).
    // * @param directory
    // * @return
    // * @deprecated switch to remote REST visualization
    // */
    // public Context visualize(String directory) {
    //
    // // Visualization fds = new Visualization((SubjectObserver) _observer, _monitor);
    // // if (!directory.contains(":") && !directory.startsWith("/")) {
    // // directory = Thinklab.get().getWorkspace("archive" + File.separator + directory).toString();
    // // }
    // // try {
    // // fds.persist(directory);
    // // } catch (ThinklabException e) {
    // // _monitor.error(e);
    // // }
    // return this;
    // }

    public Context withScenario(Object... scenarios) throws ThinklabValidationException {

        _scenarios.clear();

        for (Object o : scenarios) {
            if (o instanceof String) {
                _scenarios.add((String) o);
            } else if (o instanceof INamespace && ((INamespace) o).isScenario()) {
                _scenarios.add(((INamespace) o).getId());
            } else {
                throw new ThinklabValidationException("cannot use " + o + "  as a scenario");
            }
        }
        return this;
    }

    /**
     * Observe anything either in the current context or as a main observation. Pass only one subject
     * generator (or its ID), or however many things to observe in the current subject; if doing the latter,
     * you should call this indirectly with the with() semantics.
     * 
     * @param observable
     * @return
     */
    private Task observeInternal(ITask.Listener listener, Object... observable) {
        return new Task(this, listener, _reset, _scenarios, observable);
    }

    public static String describeObservable(Object observable) {

        if (observable instanceof IModel) {
            return "model " + ((IModel) observable).getName();
        } else if (observable instanceof IConcept) {
            return "concept " + observable;
        } else if (observable instanceof ISubjectGenerator) {
            return "object " + ((ISubjectGenerator) observable).getName();
        } else if (observable instanceof IObservable) {
            return ((IObservable) observable).getFormalName();
        }
        return observable.toString();
    }

    public IMonitor getMonitor() {
        return _monitor;
    }

    public List<String> getScenarios() {
        return _scenarios;
    }

    public TemporalCausalGraph<ISubject, IObservationGraphNode> getCausalGraph() {
        return ((Subject) _subject).getCausalGraph();
    }

    public IState getState(Object o) {

        /**
         * TODO allow to search state by observer, model or anything else useful. TODO ensure things work
         * properly if called during run - must return most recent state without conflicts.
         */
        IConcept c = o instanceof IConcept ? (IConcept) o : Thinklab.c(o.toString());

        for (IState s : _subject.getStates()) {
            if (s.getObservable().is(c))
                return s;
        }

        return null;
    }

    public static Context create(SubjectGenerator sg, ISession session, IMonitor monitor)
            throws ThinklabException {
        Context ret = new Context(session, monitor, sg.getId());
        ret.observe(sg);
        return ret;
    }

    @Override
    public void setMonitor(IMonitor monitor) {
        _monitor = (Monitor) monitor;
    }

    @Override
    public boolean isEmpty() {
        return _status == EMPTY;
    }

    @Override
    public String getName() {
        return _subject == null ? null : _subject.getId();
    }

    @Override
    public boolean isFinished() {
        return _status == CONTEXTUALIZED;
    }

    @Override
    public boolean isRunning() {
        return _status == RUNNING;
    }

    @Override
    public IContext inScenario(String... scenarios) {

        Context ret = new Context(this);

        for (String s : scenarios) {
            ret._scenarios.add(s);
        }

        return ret;
    }

    @Override
    public ITask run() {

        Task ret = new Task(this, null);
        ret.start();
        return ret;
    }

    @Override
    public ICoverage getCoverage() {
        return _coverage;
    }

    @Override
    public Object adapt() {
        return ImmutableMap
                .of(IRemoteSerializable.RESULT_TYPE_KEY, IContext.class.getSimpleName(), "coverage", _coverage == null ? 0
                        : _coverage
                                .getCoverage(), "context", _contextId, "name", _name);
    }

    @Override
    public ITask setTime(int time) {
        // TODO must step sequentially
        return null;
    }

    @Override
    public long getId() {
        return _contextId;
    }

    public int getCurrentTimeIndex() {
        return _currentTime;
    }

    public long getLastChangeTimestamp() {
        return _timeOfLastChange;
    }

    /**
     * Return the observation indexed by path, either a IState or ISubject. If not found return null with no
     * error.
     * 
     * @param path
     * @return
     */
    public IObservation get(String path) {
        return _subject == null ? null : ((Subject) _subject).get(path);
    }

    @Override
    public ISubject getSubject() {
        return _subject;
    }

    @Override
    public IContext observe(Object... observable) {

        if (observable != null && observable.length > 0 && observable[0] instanceof ISubjectGenerator)
            _rootGenerator = (ISubjectGenerator) observable[0];

        ITask task = observeAsynchronous(null, observable);
        return task.finish();
    }

    @Override
    public List<ITask> getTasks() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void persist(File file, String path) throws ThinklabException {

        IDataset dataset = ((Subject) _subject).getBackingDataset();

        if (dataset == null) {
            throw new ThinklabUnsupportedOperationException(
                    "cannot save a context unless states are dataset-backed");
        }

        if (path == null || path.equals("NONE")) {
            dataset.persist(file.toString());
        } else {
            IObservation obs = this.get(path);
            if (obs instanceof IState) {
                ((State) obs).persist(file);
            } else {
                ((Subject) obs).getBackingDataset().persist(file.toString());
            }
        }
    }

    @Override
    public IContext resetContext(boolean reset) {
        _reset = reset;
        return this;
    }

    public ISubjectGenerator getRootSubjectGenerator() {
        return _rootGenerator;
    }
}
