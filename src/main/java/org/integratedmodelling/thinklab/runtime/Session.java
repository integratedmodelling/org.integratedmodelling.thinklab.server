/**
 * Copyright 2011 The ARIES Consortium (http://www.ariesonline.org) and
 * www.integratedmodelling.org. 

   This file is part of Thinklab.

   Thinklab is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published
   by the Free Software Foundation, either version 3 of the License,
   or (at your option) any later version.

   Thinklab is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Thinklab.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.integratedmodelling.thinklab.runtime;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.listeners.IListener;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.listeners.INotification;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.api.runtime.IContext;
import org.integratedmodelling.thinklab.api.runtime.ISession;
import org.integratedmodelling.thinklab.api.runtime.ITask;
import org.integratedmodelling.thinklab.api.runtime.ITask.Listener;
import org.integratedmodelling.thinklab.api.runtime.IUserModel;
import org.integratedmodelling.thinklab.modelling.ModelManager;
import org.integratedmodelling.thinklab.modelling.monitoring.Monitor;
import org.integratedmodelling.thinklab.session.TTYUserModel;

public class Session implements ISession {

    private static final int MAX_DEFAULT_CONTEXTS = 10;

    String workspace = null;
    INamespace namespace = null;

    /*
     * monitors. TODO remove when contexts are operational.
     */
    HashMap<Long, IMonitor> _taskMonitors = new HashMap<Long, IMonitor>();
    IMonitor _sessionMonitor = new Monitor(-1L, this);

    /*
     * context stack. We keep as much as practical. TODO there should be no need to
     * keep monitors when this is fully operational.
     */
    Deque<IContext> _contexts = new ArrayDeque<IContext>(MAX_DEFAULT_CONTEXTS);

    /*
     * notifications set by monitors in model executions
     */
    ArrayList<INotification> _notifications = new ArrayList<INotification>();

    String id = "user" + nextId();

    static long __ID = 0L;

    private synchronized static long nextId() {
        return __ID++;
    }

    //	HashMap<String, Stack<Object>> vars = new HashMap<String, Stack<Object>>();

    Properties properties = new Properties();

    IListener[] listeners = null;

    private IUserModel userModel;

    public Session() {
        userModel = createUserModel();
    }

    public void setUserModel(IUserModel model) {
        userModel = model;
    }

    /* (non-Javadoc)
     * @see org.integratedmodelling.ima.core.ISession#getSessionID()
     */
    public String getID() {
        return id;
    }

    public IListener[] getListeners() {
        return listeners;
    }

    @Override
    public Properties getProperties() {
        return properties;
    }

    @Override
    public IUserModel getUserModel() {
        return this.userModel;
    }

    protected IUserModel createUserModel() {
        return new TTYUserModel();
    }

    @Override
    public String getWorkspace() {

        if (workspace == null) {
            workspace = UUID.randomUUID().toString();
        }
        return workspace;
    }

    @Override
    public void print(String s) {
        if (getUserModel().getOutputStream() != null)
            getUserModel().getOutputStream().print(s);
    }

    @Override
    public void println(String s) {
        if (getUserModel().getOutputStream() != null)
            getUserModel().getOutputStream().println(s);
    }

    @Override
    public void listen(IListener... listeners) {
        this.listeners = listeners;
    }

    @Override
    public void notify(INotification notification) {
        synchronized (_notifications) {
            _notifications.add(notification);
        }
    }

    @Override
    public List<INotification> getNotifications(boolean clear) {

        List<INotification> ret = new ArrayList<INotification>();
        synchronized (_notifications) {

            for (INotification n : _notifications)
                ret.add(n);
            if (clear)
                _notifications = new ArrayList<INotification>();
        }
        return ret;
    }

    public void registerTaskMonitor(long taskId, IMonitor monitor) {
        _taskMonitors.put(taskId, monitor);
    }

    public IMonitor getTaskMonitor(int taskId) {
        return _taskMonitors.get(taskId);
    }

    public IMonitor getSessionMonitor(int taskId) {
        return _sessionMonitor;
    }

    @Override
    public INamespace getNamespace() {

        if (namespace == null) {
            try {
                namespace = ((ModelManager) (Thinklab.get().getModelManager()))
                        .getUserNamespace(getUserModel());
            } catch (ThinklabException e) {
                throw new ThinklabRuntimeException(e);
            }
        }
        return namespace;
    }

    public IContext pushContext(IContext context) {
        if (_contexts.size() == MAX_DEFAULT_CONTEXTS) {
            _contexts.pollLast();
        }
        _contexts.push(context);
        return context;
    }

    @Override
    public IContext getTopContext() {
        return _contexts.peek();
    }

    @Override
    public List<IContext> getContexts() {
        return new ArrayList<IContext>(_contexts);
    }

    public IContext getContext(long id) {
        for (IContext c : _contexts) {
            if (c.getId() == id) {
                return c;
            }
        }
        return null;
    }

    @Override
    public ITask observe(Object subjectGenerator, Listener listener) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<ITask> getTasks() {
        // TODO Auto-generated method stub
        return null;
    }
}
