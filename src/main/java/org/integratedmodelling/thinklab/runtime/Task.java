package org.integratedmodelling.thinklab.runtime;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabUnsupportedOperationException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.lang.IRemoteSerializable;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.listeners.INotification;
import org.integratedmodelling.thinklab.api.modelling.IModel;
import org.integratedmodelling.thinklab.api.modelling.ISubjectGenerator;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.api.modelling.parsing.IConceptDefinition;
import org.integratedmodelling.thinklab.api.runtime.IContext;
import org.integratedmodelling.thinklab.api.runtime.ISession;
import org.integratedmodelling.thinklab.api.runtime.ITask;
import org.integratedmodelling.thinklab.common.monitoring.Notification;
import org.integratedmodelling.thinklab.common.utils.CamelCase;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.common.vocabulary.Observable;
import org.integratedmodelling.thinklab.debug.CallTracer;
import org.integratedmodelling.thinklab.modelling.lang.Subject;
import org.integratedmodelling.thinklab.modelling.monitoring.Monitor;
import org.integratedmodelling.thinklab.scripting.ModelProxy;

import com.ibm.icu.text.NumberFormat;

public class Task extends Thread implements ITask, IRemoteSerializable {

    static Long ___ID = 1L;

    private static long nextId() {
        synchronized (___ID) {
            return ___ID++;
        }
    }

    Context               _context;
    // either _observable or _toResolve are set. If the second is set, it's a name to be
    // resolved after project update.
    Object[]              _observable;
    String                _toResolve;

    ITask.Listener        _listener;
    volatile ITask.Status _status = Status.WAITING;
    long                  _taskId;
    IMonitor              _monitor;

    private boolean       _isInitial;
    private String        _description;
    private List<String>  _scenarios;

    private ISession      _session;
    private boolean       _reset;
    private boolean       _runTask;                // if true, this task will run a previously initialized
                                                    // context.

    Task(Context context, ITask.Listener listener, boolean reset, List<String> scenarios,
            Object... observable) {
        _context = context;
        _session = context._session;
        _observable = observable;
        _listener = listener;
        _taskId = nextId();
        _isInitial = _context._subject == null;
        _monitor = _context.getMonitor();
        _scenarios = scenarios;
        _reset = reset;
        _description = _observable[0] + (_observable.length > 1 ? "... " : " ") + " in " + _context.getName();
    }

    /**
     * Constructor to use when the context must be created or loaded after resolution.
     * 
     * @param session
     * @param monitor
     * @param listener
     * @param observable
     * @param scenarios
     */
    public Task(ISession session, IMonitor monitor, ITask.Listener listener, String observable,
            List<String> scenarios) {
        _toResolve = observable;
        _listener = listener;
        _taskId = nextId();
        _isInitial = true;
        _scenarios = scenarios;
        _session = session;
        _monitor = monitor;
        _description = observable;
    }

    /*
     * WHEN THIS IS USED, run() will run the temporal actions.
     */
    public Task(Context context, Listener listener) {
        _context = context;
        _session = context._session;
        _listener = listener;
        _runTask = true;
        _taskId = nextId();
        _description = "temporal contextualization of " + _context.getName();
    }

    @Override
    public long getTaskId() {
        return _taskId;
    }

    @Override
    public String getCommand() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getDescription() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void interrupt() {
        _monitor.stop(this);
        synchronized (_status) {
            _status = Status.INTERRUPTED;
        }
    }

    @Override
    public IContext finish() {
        // TODO Auto-generated method stub
        for (;;) {
            synchronized (_status) {
                if (_status == Status.FINISHED) {
                    break;
                }
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {

                }
            }
        }
        return _context;
    }

    @Override
    public Object adapt() {

        Map<String, Object> ret = new HashMap<String, Object>();

        ret.put("status", _status.toString());
        ret.put("id", _taskId);
        ret.put("description", _description);
        if (_context != null) {
            if (_isInitial) {
                ret.put("context", _context.adapt());
            } else {
                ret.put("contextId", _context.getId());
            }
        }
        return ret;
    }

    /*
     * FIXME/TODO: the following are awkward because they keep referencing members of Context. The whole Task
     * class should be a member of Context - let it sleep for now.
     * 
     * (non-Javadoc)
     * 
     * @see java.lang.Thread#run()
     */
    @Override
    public void run() {

        /*
         * use a task monitor with this task's ID after resolution.
         */
        IMonitor monitor = new Monitor(_taskId, _session);

        try {

            if (_runTask) {

                _context.getSubject().setMonitor(monitor);
                monitor.info("running temporal transitions", null);
                _context.getSubject().contextualize();

            } else {

                /*
                 * reload projects using the context's monitor.
                 */
                Thinklab.get().load(false, _monitor);

                /*
                 * resolve the observable if needed
                 */
                if (_toResolve != null) {
                    Object mo = resolveObservable(_toResolve);
                    if (mo == null) {
                        _status = Status.ERROR;
                        _monitor.error("cannot observe " + _toResolve);
                        return;
                    }
                    _observable = new Object[] { mo };
                } else {
                    for (int i = 0; i < _observable.length; i++) {
                        _observable[i] = resolveObservable(_observable[i]);
                    }
                }

                // probably too much checking - TODO remove after testing
                if (_observable.length < 1) {
                    _status = Status.ERROR;
                    _monitor.error("internal error: cannot find observables");
                    return;
                }

                /*
                 * establish the context - either a previous one or a new one if we're observing a new subject -
                 * and notify the listener.
                 */
                if (_observable[0] instanceof ISubjectGenerator) {
                    _context = new Context(_session, _monitor, ((ISubjectGenerator) _observable[0]).getId());
                    ((Session) _session).pushContext(_context);
                    if (_listener != null) {
                        _listener.contextAvailable(_context);
                    }
                    observeSubject((ISubjectGenerator) _observable[0], monitor);
                    monitor.message(Notification.CONTEXT_AVAILABLE, _context.getId(), _taskId);
                } else if (_observable.length == 1) {
                    observeInContext(makeObservable(_observable[0]), monitor);
                } else {

                    throw new ThinklabUnsupportedOperationException("multiple observables not supported");
                    // boolean needResolution = false;
                    // for (Object o : _observable) {
                    // _context._subject.observe(makeObservable(resolveObservable(o)));
                    // needResolution = true;
                    // }
                    // if (needResolution) {
                    //
                    // _context._coverage = _context._subject.initialize(_scenarios);
                    //
                    // if (!_context._coverage.isEmpty()) {
                    // monitor.info("observation of " + Context.describeObservable(_observable) + " in "
                    // + _context._subject + " covers "
                    // + NumberFormat.getPercentInstance().format(_context._coverage.getCoverage()), null);
                    // _context._status = Context.INITIALIZED;
                    // } else {
                    // monitor.warn("observation of " + Context.describeObservable(_observable) + " in "
                    // + _context._subject + " is empty");
                    // _context._status = Context.EMPTY;
                    // }
                    // }
                }
            }
        } catch (Exception e) {
            synchronized (_status) {
                _status = Status.ERROR;
            }
            monitor.error(e);
        } finally {
            monitor.message(Notification.TASK_FINISHED, _context == null ? 0 : _context._currentTime);
            synchronized (_status) {
                if (_status != Status.ERROR) {
                    _status = Status.FINISHED;
                }
            }
        }
    }

    private void observeSubject(ISubjectGenerator obs, IMonitor monitor) throws ThinklabException {

        try {
            if (_context._subject == null) {

                _context._subject = obs.createSubject();
                ((Subject) _context._subject).setContextId(_context._contextId);
                _context._subject.setMonitor(monitor);

                // dataset is uninstantiated until after the first resolve(), which establishes
                // the final scale for the subject.
                _context._coverage = _context._subject.initialize(_scenarios);
                _context._status = Context.INITIALIZED;
                if (_context._coverage.isEmpty()) {
                    monitor.error("subject initialization failed: empty coverage");
                }

            }
        } catch (Throwable e) {
            monitor.error(e);
        }
    }

    /*
     * * FIXME - return a Task that will run the intended obs.
     */
    private Task observeInContext(IObservable observable, IMonitor monitor) throws ThinklabException {

        _context = (Context) _session.getTopContext();

        if (_context == null) {
            throw new ThinklabValidationException("cannot observe " + observable + " outside of a context");
        }

        _context._subject.setMonitor(monitor);

        if (_reset) {
            monitor.info("resetting context", null);
            ((Subject) (_context._subject)).setContextId(_context._contextId);
            ((Subject) (_context._subject)).removeStates();
            _context._coverage = _context._subject.initialize(_scenarios);
            _context._status = Context.INITIALIZED;
        }

        _context._coverage = _context._subject.observe(observable, _scenarios, false);
        if (_reset) {
            _context._coverage = _context._subject.initialize(_scenarios);
        }

        if (!_context._coverage.isEmpty()) {
            monitor.info(
                    "observation of " + observable.getFormalName() + " in " + _context._subject.getId()
                            + " covers "
                            + NumberFormat.getPercentInstance().format(_context._coverage.getCoverage()), null);
        } else {
            monitor.warn("observation of " + observable.getFormalName() + " in "
                    + _context._subject.getId() + " is empty");
        }

        return null;
    }

    Object resolveObservable(Object observable) throws ThinklabException {

        CallTracer.indent("resolveObservable()", this, observable);
        Object ret = null;

        if (observable instanceof String) {
            if (observable.toString().contains(":")) {
                ret = Thinklab.get().getConcept(observable.toString());
            } else {
                ret = Thinklab.get().findModelObject(observable.toString());
            }

        } else if (observable instanceof IConceptDefinition) {
            ret = Thinklab.c(((IConceptDefinition) observable).getName());
        } else if (observable instanceof IConcept || observable instanceof IModel
                || observable instanceof ISubjectGenerator) {
            ret = observable;
        } else if (observable instanceof ModelProxy) {
            ret = ((ModelProxy) observable).getModel();
        }

        if (ret == null) {
            CallTracer.msg("ERROR: unable to observe the given observable parameter.");
            CallTracer.unIndent();
            throw new ThinklabValidationException("cannot observe "
                    + CallTracer.detailedDescription(observable));
        }

        CallTracer.msg("returning valid observable: " + CallTracer.detailedDescription(ret));
        CallTracer.unIndent();
        return ret;
    }

    private IObservable makeObservable(Object observable) {

        CallTracer.indent("makeObservable()", this, observable);
        IObservable result;

        if (observable instanceof IModel) {
            result = new Observable((IModel) observable, ((IModel) observable).getId());
            CallTracer.msg("got Observable result for IModel parameter: "
                    + CallTracer.detailedDescription(result));
            CallTracer.unIndent();
            return result;
        } else if (observable instanceof IConcept) {
            result = new Observable((IConcept) observable,
                    NS.getObservationTypeFor((IConcept) observable),
                    // TODO use current subject type for inherency
                    CamelCase.toLowerCase(((IConcept) observable).getLocalName(), '-'));
            CallTracer.msg("got Observable result for IConcept parameter: "
                    + CallTracer.detailedDescription(result));
            CallTracer.unIndent();
            return result;
        }

        CallTracer.msg("parameter was not IConcept or IModel. returning null.");
        CallTracer.unIndent();
        return null;
    }

    @Override
    public Status getStatus() {
        synchronized (_status) {
            return _status;
        }
    }

    @Override
    public IContext getContext() {
        return _context;
    }

    @Override
    public List<INotification> getNotifications() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Date getStartTime() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Date getEndTime() {
        // TODO Auto-generated method stub
        return null;
    }

}
