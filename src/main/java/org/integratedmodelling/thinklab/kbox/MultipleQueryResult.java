/**
 * Copyright 2011 The ARIES Consortium (http://www.ariesonline.org) and
 * www.integratedmodelling.org. 

   This file is part of Thinklab.

   Thinklab is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published
   by the Free Software Foundation, either version 3 of the License,
   or (at your option) any later version.

   Thinklab is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Thinklab.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.integratedmodelling.thinklab.kbox;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.integratedmodelling.collections.ImmutableList;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.thinklab.api.knowledge.ISemanticObject;
import org.integratedmodelling.thinklab.api.knowledge.kbox.IKbox;
import org.integratedmodelling.thinklab.api.metadata.IMetadata;

/**
 * a query result that presents a set of query results as a single cursor. It can optionally
 * be configured with a metadata comparator, which will return the results in an order
 * determined by applying the comparator to the metadata of each object. 
 * 
 * If a comparator is configured, the first get() will retrieve all metadata and sort the
 * objects, after which no further add() are possible.
 * 
 * @author Ferdinando
 *
 */
public class MultipleQueryResult extends ImmutableList<ISemanticObject<?>> {

    int max = -1;
    int tot = 0;
    Comparator<IMetadata> comparator = null;

    ArrayList<Pair<IKbox, Long>> sorted = null;
    ArrayList<List<ISemanticObject<?>>> results = new ArrayList<List<ISemanticObject<?>>>();
    ArrayList<Integer> counts = new ArrayList<Integer>();

    class It implements Iterator<ISemanticObject<?>> {

        int n = 0;

        @Override
        public boolean hasNext() {
            return n < tot;
        }

        @Override
        public ISemanticObject<?> next() {
            return get(n);
        }

        @Override
        public void remove() {
        }

    }

    public MultipleQueryResult() {
    }

    public MultipleQueryResult(Comparator<IMetadata> comparator) {
        this.comparator = comparator;
    }

    private void sort() {

        /*
         * retrieve all metadata, sort them and build an index for the result, then throw
         * the metadata away.
         */
        for (int i = 0; i < tot; i++) {

        }
    }

    /*
     * add result; if we have more than we want, 
     * return false to notify it (the return value 
     * is basically an answer to "want more?")
     */
    public boolean add(List<ISemanticObject<?>> result) {
        if (max > 0 && tot >= max)
            return false;
        int rc = result.size();
        if (rc > 0) {
            results.add(result);
            counts.add(rc);
            tot += rc;
        }
        return (max > 0) ? (tot < max) : true;
    }

    private Pair<Integer, List<ISemanticObject<?>>> pickResult(int n) {

        int nr = 0, t = 0;
        List<ISemanticObject<?>> q = null;

        if (n >= tot || n < 0)
            return null;

        for (int i = 0; i < counts.size(); i++) {
            if (n < (t + counts.get(i))) {
                q = results.get(i);
                nr = n - t;
                break;
            }
            t += counts.get(i);
        }

        return new Pair<Integer, List<ISemanticObject<?>>>(nr, q);
    }

    @Override
    public ISemanticObject<?> get(int n) {

        if (comparator != null && sorted == null) {
            sort();
        }

        if (sorted != null) {
            try {
                return sorted.get(n).getFirst().retrieve(sorted.get(n).getSecond());
            } catch (ThinklabException e) {
                throw new ThinklabRuntimeException(e);
            }
        }

        Pair<Integer, List<ISemanticObject<?>>> rr = pickResult(n);
        if (rr != null) {
            return rr.getSecond().get(rr.getFirst());
        }
        return null;
    }

    @Override
    public int size() {
        return tot;
    }

    @Override
    public boolean contains(Object arg0) {
        return false;
    }

    @Override
    public Iterator<ISemanticObject<?>> iterator() {
        return new It();
    }

    @Override
    public Object[] toArray() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <T> T[] toArray(T[] arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    public IMetadata getMetadata(int n) {

        if (comparator != null && sorted == null) {
            sort();
        }

        if (sorted != null) {
            return sorted.get(n).getFirst().getObjectMetadata(sorted.get(n).getSecond());
        }

        Pair<Integer, List<ISemanticObject<?>>> rr = pickResult(n);
        if (rr != null) {
            return ((KBoxResult) (rr.getSecond())).getMetadata(rr.getFirst());
        }
        return null;
    }

}
