package org.integratedmodelling.thinklab.kbox.neo4j;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.TermQuery;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.collections.Triple;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabIOException;
import org.integratedmodelling.exceptions.ThinklabInternalErrorException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.exceptions.ThinklabUnsupportedOperationException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.lang.Quantifier;
import org.integratedmodelling.lang.SemanticType;
import org.integratedmodelling.list.ReferenceList;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IKnowledge;
import org.integratedmodelling.thinklab.api.knowledge.IProperty;
import org.integratedmodelling.thinklab.api.knowledge.ISemanticObject;
import org.integratedmodelling.thinklab.api.knowledge.kbox.IKbox;
import org.integratedmodelling.thinklab.api.knowledge.kbox.ISpatialKbox;
import org.integratedmodelling.thinklab.api.knowledge.query.IOperator;
import org.integratedmodelling.thinklab.api.knowledge.query.IQuery;
import org.integratedmodelling.thinklab.api.lang.IMetadataHolder;
import org.integratedmodelling.thinklab.api.lang.IReferenceList;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.listeners.INotification;
import org.integratedmodelling.thinklab.api.metadata.IMetadata;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.debug.DataRecorder;
import org.integratedmodelling.thinklab.geospace.GeoNS;
import org.integratedmodelling.thinklab.geospace.Geospace;
import org.integratedmodelling.thinklab.geospace.literals.ShapeValue;
import org.integratedmodelling.thinklab.interfaces.IStorageMetadataProvider;
import org.integratedmodelling.thinklab.interfaces.knowledge.SemanticQuery;
import org.integratedmodelling.thinklab.interfaces.knowledge.datastructures.IntelligentMap;
import org.integratedmodelling.thinklab.kbox.KBoxResult;
import org.integratedmodelling.thinklab.modelling.lang.Metadata;
import org.integratedmodelling.thinklab.query.Query;
import org.neo4j.gis.spatial.DefaultLayer;
import org.neo4j.gis.spatial.GeometryEncoder;
import org.neo4j.gis.spatial.SpatialDatabaseRecord;
import org.neo4j.gis.spatial.SpatialDatabaseService;
import org.neo4j.gis.spatial.SpatialRecord;
import org.neo4j.gis.spatial.pipes.GeoPipeFlow;
import org.neo4j.gis.spatial.pipes.GeoPipeline;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.ReturnableEvaluator;
import org.neo4j.graphdb.StopEvaluator;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.Traverser;
import org.neo4j.graphdb.Traverser.Order;
import org.neo4j.graphdb.index.Index;
import org.neo4j.graphdb.index.IndexHits;
import org.neo4j.graphdb.traversal.Evaluators;
import org.neo4j.graphdb.traversal.TraversalDescription;
import org.neo4j.index.lucene.QueryContext;
import org.neo4j.index.lucene.ValueContext;
import org.neo4j.kernel.EmbeddedGraphDatabase;
import org.neo4j.kernel.Traversal;
import org.opengis.referencing.crs.CoordinateReferenceSystem;

import com.google.common.collect.Sets;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryCollection;

public class NeoKBox implements IKbox, ISpatialKbox {

    static final String                            TYPE_PROPERTY    = "_type";
    static final String                            HASNODE_PROPERTY = "_hasnode";

    /*
     * index names. One for types, one for properties and one (currently unused) for relationships. Spatial
     * indices have the name of the property they refer to.
     */
    static final String                            BASE_INDEX       = "pindex";
    static final String                            RELS_INDEX       = "rindex";
    static final String                            TYPE_INDEX       = "tindex";

    private static IntelligentMap<KboxTypeAdapter> _typeAdapters    = null;

    private static String toId(IKnowledge c, Integer index) {
        String ret = c.toString().replace(':', '_');
        if (index != null) {
            ret += INDEX_SEPARATOR + index;
        }
        return ret;
    }

    /*
     * literal property IDs have this + an index appended to if they are multiple.
     */
    private static final String INDEX_SEPARATOR  = "##";

    /*
     * keys for retrieval of data recorded in debug mode
     */
    public static final String  STORAGE_DEBUG_ID = "STORAGE";

    private static String fromId(String c) {
        /*
         * remove counter if any
         */
        Pair<String, Integer> pp = splitIndex(c);
        return pp.getFirst().replace('_', ':');
    }

    private static Pair<String, Integer> splitIndex(String c) {

        String s = c;
        Integer idx = null;
        if (c.contains(INDEX_SEPARATOR)) {
            int pos = c.indexOf(INDEX_SEPARATOR);
            s = c.substring(0, pos);
            idx = Integer.parseInt(c.substring(pos + 2));
        }

        return new Pair<String, Integer>(s, idx);

    }

    protected EmbeddedGraphDatabase _db  = null;

    /*
     * created to wrap _db only once if/when first used.
     */
    SpatialDatabaseService          _sdb = null;

    String                          _url = null;
    List<IProperty>                 _sortProperties;

    public NeoKBox(String url) {

        if (_typeAdapters == null) {
            initializeTypeAdapters();
        }
        this._url = url;
    }

    /*
     * just a set of IDs but with a very nice (and expensive) toString() method for debugging.
     */
    class NodeSet extends HashSet<Long> {
        private static final long serialVersionUID = 680868968423384500L;

        NodeSet() {
            super();
        }

        NodeSet(Collection<Long> coll) {
            super(coll);
        }

        @Override
        public String toString() {

            String ret = "";
            for (Long id : this) {
                Node n = _db.getNodeById(id);
                ret += id + "{\n";
                for (String s : n.getPropertyKeys()) {
                    ret += "  " + s + " = " + n.getProperty(s) + "\n";
                }
                ret += "}\n";
            }
            return ret;
        }

    }

    public SpatialDatabaseService getSpatialDB() {

        if (_sdb == null) {
            _sdb = new SpatialDatabaseService(_db);
        }
        return _sdb;
    }

    /**
     * Register a new type adapter for the a literal type that we want handled.
     * 
     * @param type
     * @param type
     */
    public static void registerTypeAdapter(IConcept type, KboxTypeAdapter adapter) {

        if (_typeAdapters == null) {
            _typeAdapters = new IntelligentMap<KboxTypeAdapter>();
        }
        _typeAdapters.put(type, adapter);
    }

    @Override
    public synchronized long store(Object o) throws ThinklabException {
        return store(o, null);
    }

    public synchronized long store(Object o, IMonitor monitor) throws ThinklabException {

        long ret = -1;
        ISemanticObject<?> instance = Thinklab.get().annotate(o);

        Transaction tx = _db.beginTx();

        try {

            Node node = storeInstanceInternal(instance, new HashMap<ISemanticObject<?>, Node>(), monitor);
            _db.getReferenceNode().createRelationshipTo(node, new RelationshipType() {
                @Override
                public String name() {
                    return HASNODE_PROPERTY;
                }
            });

            ret = node.getId();
            tx.success();

        } catch (Throwable e) {

            tx.failure();
            if (monitor != null)
                monitor.error(e);
            throw new ThinklabIOException(e);

        }

        tx.finish();

        if (Thinklab.get().getNotificationLevel() == INotification.DEBUG) {
            DataRecorder.get().add(new Pair<Long, Object>(ret, o), STORAGE_DEBUG_ID, false);
        }

        return ret;
    }

    private Node storeInstanceInternal(ISemanticObject<?> instance, Map<ISemanticObject<?>, Node> refs,
            IMonitor monitor) throws ThinklabException {

        Node node = refs.get(instance);

        if (node != null)
            return node;

        node = createNode(instance);
        refs.put(instance, node);

        for (final Triple<IProperty, ISemanticObject<?>, Integer> s : instance.getCountedRelationships()) {

            if (s.getSecond().isLiteral()) {
                storeProperty(node, s.getFirst(), s.getSecond(), s.getThird(), monitor);
            } else {

                Node target = storeInstanceInternal(s.getSecond(), refs, monitor);
                Relationship rel = node.createRelationshipTo(target, new RelationshipType() {
                    @Override
                    public String name() {
                        return toId(s.getFirst(), null);
                    }
                });

                _db.index().forRelationships(RELS_INDEX).add(rel, "name", toId(s.getFirst(), null));
            }
        }

        storeMetadata(node, instance, monitor);

        return node;
    }

    protected Node createNode(ISemanticObject<?> instance) {

        Node node = _db.createNode();
        String type = instance.getDirectType().toString();
        node.setProperty(TYPE_PROPERTY, type);
        Index<Node> index = _db.index().forNodes(TYPE_INDEX);
        index.add(node, TYPE_PROPERTY, type);
        return node;
    }

    protected void storeMetadata(Node node, ISemanticObject<?> instance, IMonitor monitor)
            throws ThinklabException {

        /*
         * if object has metadata, additionally store those metadata whose key is a known property and whose
         * value has a corresponding type adapter. Check the metadata both for the semantic object and its
         * demoted incarnation.
         */
        IMetadata metadata = null;
        if (instance instanceof IMetadataHolder) {
            metadata = ((IMetadataHolder) instance).getMetadata();
            if (instance instanceof IStorageMetadataProvider) {
                ((IStorageMetadataProvider) instance).addStorageMetadata(metadata, monitor);
            }
        }
        if (instance.demote() instanceof IMetadataHolder) {
            if (metadata == null)
                metadata = ((IMetadataHolder) instance.demote()).getMetadata();
            else
                metadata.merge(((IMetadataHolder) instance.demote()).getMetadata(), false);

            if (instance.demote() instanceof IStorageMetadataProvider) {
                ((IStorageMetadataProvider) instance.demote()).addStorageMetadata(metadata, monitor);
            }
        }

        if (metadata != null) {

            for (String key : metadata.getKeys()) {
                IProperty p = Thinklab.get().getProperty(key);
                if (p == null)
                    continue;
                IConcept c = Thinklab.get().getLiteralConceptForJavaClass(metadata.get(key).getClass());
                if (c == null)
                    continue;

                KboxTypeAdapter adapter = _typeAdapters.get(c);

                if (adapter != null) {
                    adapter.setAndIndexProperty(node, this, p, metadata.get(key), null, monitor);
                }
            }
        }
    }

    private void storeProperty(Node node, IProperty p, ISemanticObject<?> s, Integer count, IMonitor monitor)
            throws ThinklabException {

        KboxTypeAdapter adapter = _typeAdapters.get(s.getDirectType());

        if (adapter == null)
            throw new ThinklabUnsupportedOperationException("kbox: cannot store literal of type "
                    + s.getDirectType());

        adapter.setAndIndexProperty(node, this, p, s.demote(), count, monitor);
    }

    @Override
    public void remove(long handle) throws ThinklabException {
        HashSet<Node> nodes = new HashSet<Node>();
        HashSet<Relationship> rels = new HashSet<Relationship>();
        removeInternal(_db.getNodeById(handle), nodes, rels);
        zap(nodes, rels);
    }

    private void zap(HashSet<Node> nodes, HashSet<Relationship> rels) throws ThinklabIOException {

        Transaction tx = _db.beginTx();
        try {
            for (Relationship r : rels) {
                r.delete();
            }
            for (Node n : nodes) {
                n.delete();
            }
            tx.success();
        } catch (Exception e) {
            tx.failure();
            throw new ThinklabIOException(e.getMessage());
        } finally {
            tx.finish();
        }

    }

    private void removeInternal(Node n, HashSet<Node> nodes, HashSet<Relationship> rels)
            throws ThinklabException {

        if (nodes.contains(n))
            return;

        nodes.add(n);

        for (Relationship r : n.getRelationships(Direction.OUTGOING)) {
            removeInternal(r.getEndNode(), nodes, rels);
            rels.add(r);
        }

        /*
         * node may have incoming relationship HAS_NODE or other from plugins
         */
        for (Relationship r : n.getRelationships(Direction.INCOMING)) {
            rels.add(r);
        }

    }

    @Override
    public void clear() throws ThinklabException {

        try {
            URL urf = new URL(_url);
            File dir = new File(urf.getFile());
            _db.shutdown();
            FileUtils.deleteDirectory(dir);
            _db = new EmbeddedGraphDatabase(urf.getFile());
        } catch (Exception e) {
            throw new ThinklabIOException(e);
        }
    }

    @Override
    public List<ISemanticObject<?>> query(IQuery query) throws ThinklabException {

        if (query != null && !(query instanceof Query)) {
            throw new ThinklabUnsupportedOperationException("query type not supported: " + query);
        }

        return new KBoxResult(this, queryObjects((Query) query));
    }

    @Override
    public List<ISemanticObject<?>> query(IQuery query, Comparator<IMetadata> prioritizer, IMonitor monitor)
            throws ThinklabException {

        class Comp implements Comparator<Object> {

            Comparator<IMetadata> _p;

            public Comp(Comparator<IMetadata> prioritizer) {
                _p = prioritizer;
            }

            @Override
            public int compare(Object o1, Object o2) {
                return _p.compare((IMetadata) o1, (IMetadata) o2);
            }
        }

        if (query != null && !(query instanceof Query)) {
            throw new ThinklabUnsupportedOperationException("query type not supported: " + query);
        }

        List<Long> result = queryObjects((Query) query);
        List<Object> meta = new ArrayList<Object>();
        for (Long id : result) {
            meta.add(getObjectMetadata(id));
        }

        return new KBoxResult(this, result, meta, new Comp(prioritizer), monitor);
    }

    @Override
    public String getUri() {
        return _url;
    }

    @Override
    public ISemanticObject<?> retrieve(long id) throws ThinklabException {

        /*
         * if it's a model object that we already have in memory, use the object in memory and spare ourselves
         * some trouble.
         * 
         * FV removed - this means that models will not be recreated properly, but we only use the kbox
         * to store ModelData now.
         */
        // IMetadata metadata = getObjectMetadata(id);
        //
        // if (metadata.get(NS.HAS_NAMESPACE_ID) != null) {
        // String oid = metadata.get(NS.HAS_NAMESPACE_ID) + "." + metadata.get(NS.HAS_ID);
        // IModelObject obj = Thinklab.get().findModelObject(oid);
        // if (obj != null) {
        // return (ISemanticObject<?>) obj;
        // }
        // }

        IReferenceList rl = retrieveList(_db.getNodeById(id), new HashMap<Node, IReferenceList>());
        ISemanticObject<?> ret = Thinklab.get().entify(rl);

        if (ret instanceof IMetadataHolder) {
            Metadata md = (Metadata) ((IMetadataHolder) ret).getMetadata();
            md.put(NS.HAS_KBOX_ID, id);
        }

        return ret;
    }

    @Override
    public void removeAll(IQuery query) throws ThinklabException {
        HashSet<Node> nodes = new HashSet<Node>();
        HashSet<Relationship> rels = new HashSet<Relationship>();
        for (long l : queryObjects((Query) query)) {
            try {
                removeInternal(_db.getNodeById(l), nodes, rels);
            } catch (ThinklabException e) {
                throw new ThinklabRuntimeException(e);
            }
        }
        zap(nodes, rels);
    }

    @Override
    public List<ISemanticObject<?>> retrieveAll() throws ThinklabException {

        ArrayList<Long> res = new ArrayList<Long>();
        Traverser traverser = _db.getReferenceNode().traverse(Order.DEPTH_FIRST, StopEvaluator.DEPTH_ONE,
                ReturnableEvaluator.ALL_BUT_START_NODE, new RelationshipType() {
                    @Override
                    public String name() {
                        return HASNODE_PROPERTY;
                    }
                }, Direction.OUTGOING);

        for (Node n : traverser.getAllNodes()) {
            res.add(n.getId());
        }

        return new KBoxResult(this, res);
    }

    /*
     * non-public
     */
    private IReferenceList retrieveList(Node node, HashMap<Node, IReferenceList> refs)
            throws ThinklabException {

        IReferenceList ref = ReferenceList.list();
        if (refs.containsKey(node)) {
            return refs.get(node);
        } else {
            refs.put(node, ref);
        }

        ArrayList<Object> rl = new ArrayList<Object>();
        IConcept concept = Thinklab.c(node.getProperty(TYPE_PROPERTY).toString());
        rl.add(concept);

        /*
         * literals
         */
        for (Pair<IProperty, Object> lp : getPropertyValues(node)) {
            rl.add(ReferenceList.list(lp.getFirst(), Thinklab.get().conceptualize(lp.getSecond())));
        }

        /*
         * follow outgoing relationships to other objects
         */
        for (Relationship r : node.getRelationships(Direction.OUTGOING)) {
            rl.add(ReferenceList.list(Thinklab.p(fromId(r.getType().name())),
                    retrieveList(r.getEndNode(), refs)));
        }

        return ref.assign(ReferenceList.list(rl.toArray()));
    }

    private Pair<IProperty, Object> getPropertyValue(String pid, Node node, Integer index)
            throws ThinklabException {

        String pname = pid + (index == null ? "" : (INDEX_SEPARATOR + index));
        if (!node.hasProperty(pname))
            return null;

        Object pvalue = node.getProperty(pname);

        String ppid = fromId(pid);

        if (pvalue instanceof String && pvalue.toString().startsWith(DecodingTypeAdapter.DECODE_PREFIX)) {

            /*
             * object is a complex literal proxied by the properties in the node. We use its type adapter
             * (which must be a DecodingTypeAdapter) to resolve those properties to the final value.
             */
            IConcept type = Thinklab.c(pvalue.toString()
                    .substring(DecodingTypeAdapter.DECODE_PREFIX.length()));
            KboxTypeAdapter ta = _typeAdapters.get(type);

            return new Pair<IProperty, Object>(Thinklab.p(ppid), ((DecodingTypeAdapter) ta).decode(
                    Thinklab.p(ppid), node, index));

        }

        /*
         * properties added by third-party plugins must be skipped too - if we're really unlucky, third-party
         * plugins may have added properties that translate to ontologies but let's be optimistic.
         */
        if (!SemanticType.validate(ppid))
            return null;

        /*
         * this should take care of any strangeness, but potentially allow some subtle internal errors that
         * silently skip previously valid properties (can happen if the ontologies have changed and the kbox
         * hasn't been synchronized).
         */
        if (Thinklab.get().getProperty(ppid) == null)
            return null;

        /*
         * standard Neo4j node property pointing to an actual literal - just conceptualize it, if it's there
         * it must be conceptualizable.
         */
        return new Pair<IProperty, Object>(Thinklab.p(ppid), pvalue);

    }

    /*
     * get all literal properties of a node that have known semantic mappings to relationships.
     */
    private Collection<Pair<IProperty, Object>> getPropertyValues(Node node) throws ThinklabException {

        ArrayList<Pair<IProperty, Object>> ret = new ArrayList<Pair<IProperty, Object>>();
        HashSet<String> multiple = new HashSet<String>();

        for (String p : node.getPropertyKeys()) {

            // skip system properties
            if (p.startsWith("_"))
                continue;

            // deal with the multiple ones later
            if (p.contains(INDEX_SEPARATOR)) {
                Pair<String, Integer> ps = splitIndex(p);
                multiple.add(ps.getFirst());
                continue;
            }

            Pair<IProperty, Object> pp = getPropertyValue(p, node, null);
            if (pp != null) {
                ret.add(pp);
            }
        }

        for (String pid : multiple) {

            for (int i = 0;; i++) {
                Pair<IProperty, Object> pp = getPropertyValue(pid, node, i);
                if (pp == null)
                    break;
                ret.add(pp);
            }

        }
        return ret;

    }

    private List<Long> applySorting(Set<Long> results, SemanticQuery query, int flags) {

        ArrayList<Long> ret = new ArrayList<Long>();
        if (query instanceof IMetadataHolder
                && ((IMetadataHolder) query).getMetadata().getString(IKbox.SORT_FIELD) != null) {

            /*
             * TODO sort if the query's metadata specify it. Use node metadata and a ComparatorChain from
             * apache.commons.collections.
             */

        } else {
            for (Long n : results)
                ret.add(n);
        }

        return ret;
    }

    /**
     * FIXME still incomplete re: handling of quantifiers and optimization - barely implemented enough to
     * support model testing.
     * 
     * @param query
     * @param ncontext the nodes that we're restricting - we should be a restriction if this is not null.
     * @param pcontext the property context, either for an operator on a field or a relationship to other
     * objects. Can be null.
     * @param flags
     * @return
     * @throws ThinklabException
     */
    private Set<Long> retrieveMatches(Query query, Set<Long> ncontext, IProperty pcontext, int flags,
            boolean isNegation) throws ThinklabException {

        if (query == null) {

            Set<Long> set = null;

            if (pcontext.isLiteralProperty()) {

                set = getNodesWithProperty(pcontext);

            } else {

                /*
                 * just the existence of the relationship
                 */
                set = getSourcesOfRelationship(pcontext, flags);

            }

            return filterResultSet(set, ncontext, isNegation);

        } else if (query.isConnector()) {

            /*
             * get each result set and intersect according to connector
             */
            Set<Long> ret = null;
            boolean isNegate = query.getQuantifier().equals(Quantifier.NONE());
            if (isNegation) {
                isNegate = !isNegate;
            }

            for (SemanticQuery r : query.getRestrictions()) {

                Set<Long> rest = retrieveMatches((Query) r, ncontext, pcontext, flags, isNegate);

                if (ret == null) {
                    ret = rest;
                } else {
                    if (query.getQuantifier().equals(Quantifier.ALL())) {
                        ret.retainAll(rest);
                        if (ret.isEmpty())
                            return ret;
                    } else if (query.getQuantifier().equals(Quantifier.ANY())) {
                        ret.addAll(rest);
                    } else {
                        /*
                         * TODO
                         */
                        throw new ThinklabUnsupportedOperationException(
                                "query: implementation of quantifiers other than any and all is incomplete");
                    }
                }

            }

            return ret == null ? new NodeSet() : ret;

        } else if (query.isRestriction()) {

            /*
             * nothing to restrict, no need to continue.
             */
            if (ncontext != null && ncontext.isEmpty())
                return ncontext;

            /*
             * if we have no restriction, we just want anything that is related to us via the property.
             */
            if (query.getRestrictions() == null || query.getRestrictions().size() == 0) {
                return retrieveMatches(null, ncontext, query.getProperty(), flags, isNegation);
            }

            /*
             * if isRestriction() returns true, we only have one restriction query to worry about.
             */
            Query restriction = (Query) query.getRestrictions().iterator().next();

            /*
             * get the target node set, intersected appropriately with whatever node context we have.
             */
            return retrieveMatches(restriction, ncontext, query.getProperty(), flags, isNegation);

        } else {

            if (query instanceof IOperator) {

                /*
                 * get result set of index search after translating operator
                 */
                return getNodesMatching((IOperator) query, ncontext, pcontext, flags, isNegation);

            } else {

                /*
                 * we may get here under a property restriction with a source nodeset.
                 */
                if (pcontext != null && ncontext != null && ncontext.isEmpty())
                    return ncontext;

                /*
                 * Most important case (we usually start all queries here):
                 * 
                 * select object based on concept's semantic closure and restrict to given properties.
                 * 
                 * TODO FIXME this one really should collect all POD operators in the restrictions and apply
                 * them in one shot to the property index, then proceed intersecting only the other
                 * restrictions. Otherwise we're making potentially huge sets that don't need to exist in
                 * memory.
                 */
                Set<IConcept> closure = null;
                if ((query.getFlags() & Query.USE_SEMANTIC_CLOSURE) != 0) {
                    closure = query.getSubject().getSemanticClosure();
                } else if ((query.getFlags() & Query.USE_OBSERVABLE_CLOSURE) != 0) {
                    closure = NS.getObservableClosure(query.getSubject());
                } else {
                    closure = Sets.newHashSet(query.getSubject());
                }
                Set<Long> main = getNodesOfType(closure, flags);

                /*
                 * restrict the result set by applying all restriction (all in AND). This loop should be
                 * changed to produce one index search for all POD operators taken together.
                 */
                for (SemanticQuery r : query.getRestrictions()) {
                    main = retrieveMatches((Query) r, main, null, flags, isNegation);
                    if (main.size() == 0) {
                        break;
                    }
                }

                /*
                 * if these were the target of a restriction, retain all in node context that relate to these
                 * through the restriction property.
                 * 
                 * TODO count the matches and insert node based on quantifier match instead of just inserting
                 * the node. This one is just the implementation for "any".
                 */
                if (pcontext != null && ncontext != null) {
                    HashSet<Long> ret = new NodeSet();
                    for (long id : ncontext) {
                        Node node = _db.getNodeById(id);
                        // FIXME can we do "get all relationships that point to found node" instead? Likely to
                        // be less to scan.
                        for (Relationship rel : node.getRelationships(Direction.OUTGOING)) {
                            if (isProperty(rel, pcontext) && main.contains(rel.getEndNode().getId())) {
                                ret.add(id);
                            }
                        }
                    }
                    return ret;
                } else if (ncontext != null) {
                    ncontext.retainAll(main);
                }

                return main;
            }
        }
    }

    private Set<Long> filterResultSet(Set<Long> set, Set<Long> ncontext, boolean isNegation) {

        if (isNegation) {
            Set<Long> ret = ncontext != null ? new NodeSet(ncontext) : getAllNodes();
            ret.removeAll(set);
            set = ret;
        } else {
            if (ncontext != null) {
                Set<Long> ret = new NodeSet(ncontext);
                ret.retainAll(set);
                set = ret;
            }
        }
        return set;
    }

    private Set<Long> getNodesWithProperty(IProperty pcontext) {

        Set<Long> ret = new HashSet<Long>();
        String id = toId(pcontext, null);
        for (Node n : _db.getAllNodes()) {
            if (n.hasProperty(id))
                ret.add(n.getId());
        }
        return ret;
    }

    private Set<Long> getAllNodes() {

        Set<Long> ret = new HashSet<Long>();
        for (Node n : _db.getAllNodes()) {
            ret.add(n.getId());
        }
        return ret;
    }

    private boolean isProperty(Relationship rel, IProperty pcontext) {
        IProperty p = Thinklab.p(fromId(rel.getType().name()));
        return p.is(pcontext);
    }

    private Set<Long> getNodesMatching(IOperator query, Set<Long> ncontext, IProperty pcontext, int flags,
            boolean isNegation) throws ThinklabException {

        // find type adapter based on property range. If no range, assume string.
        Collection<IConcept> types = pcontext.getRange();
        KboxTypeAdapter zio = _typeAdapters.get(types.size() > 0 ? types.iterator().next() : Thinklab.TEXT);

        if (zio == null) {
            throw new ThinklabUnsupportedOperationException("neo4j kbox can't handle the range of property "
                    + pcontext);
        }

        Set<Long> set = zio.searchIndex(this, pcontext, query, isNegation);
        if (ncontext != null) {
            Set<Long> ret = new NodeSet(ncontext);
            ret.retainAll(set);
            set = ret;
        }
        return set;
    }

    private Set<Long> getNodesOfType(Set<IConcept> zio, int flags) {

        HashSet<Long> ret = new NodeSet();

        for (IConcept c : zio) {

            if (c.toString().equals("im.geo:Region"))
                System.out.println("");

            /*
             * one by one and join the sets. Somehow I cannot manage to get a properly behaving single query
             * with all terms in OR.
             */
            org.apache.lucene.search.Query query = new TermQuery(new Term(TYPE_PROPERTY, c.toString()));
            HashSet<Long> cret = new NodeSet();
            for (Node n : _db.index().forNodes(TYPE_INDEX).query(query))
                cret.add(n.getId());
            ret.addAll(cret);
        }
        return ret;
    }

    private Set<Long> getSourcesOfRelationship(final IProperty property, int flags) {

        Set<Long> ret = new NodeSet();
        IndexHits<Relationship> hits = _db.index().forRelationships(RELS_INDEX)
                .query("name", toId(property, null));
        for (Relationship r : hits) {
            ret.add(r.getStartNode().getId());
        }
        return ret;
    }

    private List<Long> queryObjects(Query query) throws ThinklabException {

        List<Long> ret = new ArrayList<Long>();

        if (query == null || query.isEmpty()) {

            TraversalDescription td = Traversal.description().depthFirst()
                    .evaluator(Evaluators.excludeStartPosition()).relationships(new RelationshipType() {
                        @Override
                        public String name() {
                            return HASNODE_PROPERTY;
                        }
                    }, Direction.OUTGOING);
            for (Node n : td.traverse(_db.getReferenceNode()).nodes()) {
                ret.add(n.getId());
            }

            return ret;
        }

        /*
         * TODO if metadata contain a start node (e.g. root), add it to the initial node context and
         * restructure the query appropriately
         */
        Set<Long> results = retrieveMatches(query, null, null, query.getFlags(), false);
        return applySorting(selectTopNodes(results, query.getFlags()), query, query.getFlags());

    }

    /**
     * Select the nodes that are in HASNODE_PROPERTY relationship with the root node.
     * 
     * @param results
     * @param flags 
     * @return
     */
    private Set<Long> selectTopNodes(Set<Long> results, int flags) {

        if ((flags & Query.ALLOW_SECONDARY_RESULTS) != 0) {
            return results;
        }

        Set<Long> ret = new HashSet<Long>();
        RelationshipType rel = new RelationshipType() {
            @Override
            public String name() {
                return HASNODE_PROPERTY;
            }
        };
        for (long n : results) {
            Node node = _db.getNodeById(n);
            if (node.getSingleRelationship(rel, Direction.INCOMING) != null) {
                ret.add(n);
            }
        }
        return ret;
    }

    /*
     * -----------------------------------------------------------------------------------------------------
     * type handling
     * -----------------------------------------------------------------------------------------------------
     */

    /*
     * class to simplify handling of base types.
     */
    public abstract class TypeAdapter implements KboxTypeAdapter {

        protected abstract void setAndIndex(Node node, IProperty property, Object value, Integer index,
                IMonitor monitor) throws ThinklabException;

        @Override
        public void setAndIndexProperty(Node node, IKbox kbox, IProperty property, Object value,
                Integer index, IMonitor monitor) throws ThinklabException {
            setAndIndex(node, property, value, index, monitor);
        }
    }

    /**
     * Properties that store strange objects in multiple node properties leave their mark as a
     * "@decode:<concept>" string value in the properties. If that is found, the concept is parsed and used as
     * key to find one of these in the type adapters, and that is used to decode the object from the entire
     * node.
     * 
     * @author Ferd
     * 
     */
    interface DecodingTypeAdapter extends KboxTypeAdapter {

        public static final String DECODE_PREFIX = "@decode#";

        /**
         * Turn the relevant node properties into the proper semantics for the literal the property
         * represents.
         * 
         * @param property
         * @param node
         * @return
         * @throws ThinklabException
         */
        public Object decode(IProperty property, Node node, Integer index) throws ThinklabException;
    }

    class NumberTypeAdapter extends TypeAdapter {

        @Override
        protected void setAndIndex(Node node, IProperty property, Object value, Integer index,
                IMonitor monitor) {

            if (value instanceof Boolean)
                value = new Integer((Boolean) value ? 1 : 0);

            node.setProperty(toId(property, index), value);
            Index<Node> idx = _db.index().forNodes(BASE_INDEX);
            idx.add(node, toId(property, index), new ValueContext(value).indexNumeric());
        }

        @Override
        public Set<Long> searchIndex(IKbox kbox, IProperty property, IOperator operator, boolean negate)
                throws ThinklabException {

            /*
             * TODO FIXME This only works with UNIQUE relationships without an index counter.
             */

            Set<Long> ret = new NodeSet();
            Index<Node> index = _db.index().forNodes(BASE_INDEX);
            Pair<IConcept, Object[]> op = operator.getQueryParameters();
            Object qc = null;

            Object opp = op.getSecond()[0];
            if (opp instanceof Boolean)
                opp = new Integer((Boolean) opp ? 1 : 0);

            if (op.getFirst().equals(NS.OPERATION_EQUALS)) {

                /*
                 * mah - can't find anything to match a number
                 */
                qc = QueryContext.numericRange(toId(property, null), (Number) opp, (Number) opp, true, true);
            } else if (op.getFirst().equals(NS.OPERATION_GREATER_THAN)) {
                qc = QueryContext.numericRange(toId(property, null), (Number) opp, Double.MAX_VALUE, false,
                        true);
            } else if (op.getFirst().equals(NS.OPERATION_LESS_THAN)) {
                qc = QueryContext.numericRange(toId(property, null), Double.MIN_VALUE, (Number) opp, true,
                        false);
            } else if (op.getFirst().equals(NS.OPERATION_GREATER_OR_EQUAL)) {
                qc = QueryContext.numericRange(toId(property, null), (Number) opp, Double.MAX_VALUE, true,
                        true);
            } else if (op.getFirst().equals(NS.OPERATION_LESS_OR_EQUAL)) {
                qc = QueryContext.numericRange(toId(property, null), Double.MIN_VALUE, (Number) opp, true,
                        true);
            } else if (op.getFirst().equals(NS.OPERATION_NOT_EQUALS)) {

                /*
                 * TODO this should be an OR of a less than and a greater than
                 */
            }
            for (Node n : index.query(qc))
                ret.add(n.getId());

            return checkComplement(ret, negate);
        }
    }

    class ShapeTypeAdapter extends TypeAdapter implements DecodingTypeAdapter {

        IConcept _concept;

        public ShapeTypeAdapter(IConcept c) {
            _concept = c;
        }

        @Override
        public Object decode(IProperty property, Node node, Integer index) throws ThinklabException {

            /*
             * create the appropriate literal and return its conceptualized representation.
             */
            String idName = toId(property, index);
            DefaultLayer layer = getSpatialDB().getOrCreateDefaultLayer(idName);
            GeometryEncoder encoder = layer.getGeometryEncoder();
            Geometry g = encoder.decodeGeometry(node);
            ShapeValue shape = null;

            /*
             * FIXME these are identical - the whole shape hierarchy turned out to be 
             * a pain to manage, but there may be some value in keeping it so I'm leaving
             * the switch for now.
             */
            if (_concept.equals(NS.POINT) || _concept.equals(NS.POINT_DTYPE)) {
                shape = new ShapeValue(g, Geospace.get().getDefaultCRS());
            } else if (_concept.equals(NS.POLYGON) || _concept.equals(NS.POLYGON_DTYPE)) {
                shape = new ShapeValue(g, Geospace.get().getDefaultCRS());
            } else if (_concept.equals(NS.LINE) || _concept.equals(NS.LINE_DTYPE)) {
                shape = new ShapeValue(g, Geospace.get().getDefaultCRS());
            } else if (_concept.equals(NS.SHAPE) || _concept.equals(NS.GEOMETRY_DTYPE)) {
                shape = new ShapeValue(g, Geospace.get().getDefaultCRS());
            }
            return shape;
        }

        @Override
        protected void setAndIndex(Node node, IProperty property, Object value, Integer index,
                IMonitor monitor) throws ThinklabException {

            /*
             * Extract the geometry. PROJECTIONS MUST BE COMPARABLE before store() is called.
             */
            Geometry geometry = null;
            if (value instanceof ShapeValue) {
                geometry = ((ShapeValue) value).getGeometry();
            } else if (value instanceof Geometry) {
                geometry = (Geometry) value;
            } else {
                throw new ThinklabInternalErrorException(
                        "kbox: trying to store a non-spatial value as a shape");
            }

            String idName = toId(property, index);
            DefaultLayer layer = getSpatialDB().getOrCreateDefaultLayer(idName);
            GeometryEncoder encoder = layer.getGeometryEncoder();
            if (geometry instanceof GeometryCollection) {
                /*
                 * sorry. Encoder doesn't seem to support it.
                 */
                geometry = geometry.getBoundary();
            }
            encoder.encodeGeometry(geometry, node);
            layer.add(node);

            node.setProperty(toId(property, index), DECODE_PREFIX + _concept);
        }

        @Override
        public Set<Long> searchIndex(IKbox kbox, IProperty property, IOperator operator, boolean negate)
                throws ThinklabException {

            /*
             * TODO FIXME No attention for the index here
             */

            Set<Long> ret = new NodeSet();
            Pair<IConcept, Object[]> op = operator.getQueryParameters();
            String idName = toId(property, null);
            DefaultLayer layer = getSpatialDB().getOrCreateDefaultLayer(idName);
            GeoPipeline pipeline = null;

            if (op.getFirst().equals(NS.OPERATION_INTERSECTS)) {
                pipeline = GeoPipeline.startIntersectSearch(layer, getGeometry(op));
            } else if (op.getFirst().equals(NS.OPERATION_CONTAINS)) {
                pipeline = GeoPipeline.startContainSearch(layer, getGeometry(op));
            } else if (op.getFirst().equals(NS.OPERATION_CONTAINED_BY)) {
                pipeline = GeoPipeline.startWithinSearch(layer, getGeometry(op));
            } else if (op.getFirst().equals(NS.OPERATION_COVERED_BY)) {
                pipeline = GeoPipeline.startCoveredBySearch(layer, getGeometry(op));
            } else if (op.getFirst().equals(NS.OPERATION_COVERS)) {
                pipeline = GeoPipeline.startCoverSearch(layer, getGeometry(op));
            } else if (op.getFirst().equals(NS.OPERATION_CROSSES)) {
                pipeline = GeoPipeline.startCrossSearch(layer, getGeometry(op));
            } else if (op.getFirst().equals(NS.OPERATION_INTERSECTS_ENVELOPE)) {
                pipeline = GeoPipeline.startIntersectWindowSearch(layer, getGeometry(op)
                        .getEnvelopeInternal());
            } else if (op.getFirst().equals(NS.OPERATION_OVERLAPS)) {
                pipeline = GeoPipeline.startOverlapSearch(layer, getGeometry(op));
            } else if (op.getFirst().equals(NS.OPERATION_TOUCHES)) {
                pipeline = GeoPipeline.startTouchSearch(layer, getGeometry(op));
            } else if (op.getFirst().equals(NS.OPERATION_NEAREST_NEIGHBOUR)) {
                pipeline = GeoPipeline.startNearestNeighborLatLonSearch(layer, getCentroid(op),
                        getDistance(op, layer.getCoordinateReferenceSystem()));
            } else if (op.getFirst().equals(NS.OPERATION_EQUALS)) {
                pipeline = GeoPipeline.startEqualExactSearch(layer, getGeometry(op), 0);
            }

            if (pipeline != null) {

                for (SpatialRecord result : pipeline) {

                    // Envelope envr = result.getGeometry().getEnvelopeInternal();
                    // Envelope envq = getGeometry(op).getEnvelopeInternal();
                    //
                    // String s = "RESULT: " + envr + " int " + envq;
                    // DataRecorder.debug(s);

                    /*
                     * TODO check - this is found out experimentally, hope the API is stable
                     */
                    for (SpatialDatabaseRecord record : ((GeoPipeFlow) result).getRecords()) {

                        // Envelope eeenv = record.getGeometry().getEnvelopeInternal();
                        // Node n = _db.getNodeById(record.getNodeId());
                        // String pop = (String) n.getProperty("modelling.thinklab_hasId", "NOTHING");
                        // String fuck = eeenv.intersects(envq) ? "YES" : "NO";
                        // String ss = pop + ": " + eeenv + " intersecting " + envq + " " + fuck;
                        // DataRecorder.debug(ss);

                        ret.add(record.getNodeId());
                    }
                }
            }

            return checkComplement(ret, negate);
        }

        private double getDistance(Pair<IConcept, Object[]> op, CoordinateReferenceSystem crs)
                throws ThinklabValidationException {

            if (op.getSecond().length < 2 || !(op.getSecond()[1] instanceof Number))
                throw new ThinklabValidationException("spatial operator: no distance parameter passed");

            double ret = ((Number) (op.getSecond()[1])).doubleValue();

            /*
             * TODO translate passed meters to CRS unit
             */

            return ret;
        }

        private Coordinate getCentroid(Pair<IConcept, Object[]> op) throws ThinklabException {
            return getGeometry(op).getCentroid().getCoordinate();
        }

        private Geometry getGeometry(Pair<IConcept, Object[]> op) throws ThinklabException {
            return ((ShapeValue) op.getSecond()[0]).transform(Geospace.get().getDefaultCRS()).getGeometry();
        }
    }

    /*
     * insert default type adapters. More can be added for specific types by calling static
     * registerTypeAdapter() externally.
     */
    private void initializeTypeAdapters() {

        registerTypeAdapter(Thinklab.c(NS.TEXT), new TypeAdapter() {

            @Override
            protected void setAndIndex(Node node, IProperty property, Object value, Integer idx,
                    IMonitor monitor) {
                node.setProperty(toId(property, idx), value);
                Index<Node> index = _db.index().forNodes(BASE_INDEX);
                index.add(node, toId(property, idx), value);
            }

            @Override
            public Set<Long> searchIndex(IKbox kbox, IProperty property, IOperator operator, boolean negate)
                    throws ThinklabException {

                /*
                 * TODO FIXME Index ignored!
                 */

                Index<Node> index = _db.index().forNodes(BASE_INDEX);
                Pair<IConcept, Object[]> op = operator.getQueryParameters();
                IndexHits<Node> zio = null;
                if (op.getFirst().equals(NS.OPERATION_EQUALS) || op.getFirst().equals(NS.OPERATION_LIKE)) {
                    zio = index.query(toId(property, null), new QueryContext(op.getSecond()[0]));
                }

                Set<Long> ret = new NodeSet();
                if (zio != null) {
                    for (Node n : zio)
                        ret.add(n.getId());
                }

                return checkComplement(ret, negate);
            }
        });

        /*
         * numbers. We treat booleans as a number - neo4j doesn't seem prepared for booleans.
         */
        registerTypeAdapter(Thinklab.c(NS.INTEGER), new NumberTypeAdapter());
        registerTypeAdapter(Thinklab.c(NS.DOUBLE), new NumberTypeAdapter());
        registerTypeAdapter(Thinklab.c(NS.FLOAT), new NumberTypeAdapter());
        registerTypeAdapter(Thinklab.c(NS.LONG), new NumberTypeAdapter());
        registerTypeAdapter(Thinklab.c(NS.BOOLEAN), new NumberTypeAdapter());

        /*
         * shapes
         */
        registerTypeAdapter(Thinklab.c(NS.SHAPE), new ShapeTypeAdapter(Thinklab.c(NS.SHAPE)));
        registerTypeAdapter(Thinklab.c(NS.POLYGON), new ShapeTypeAdapter(Thinklab.c(NS.POLYGON)));
        registerTypeAdapter(Thinklab.c(NS.POINT), new ShapeTypeAdapter(Thinklab.c(NS.POINT)));
        registerTypeAdapter(Thinklab.c(NS.LINE), new ShapeTypeAdapter(Thinklab.c(NS.LINE)));

        /*
         * TODO time values and periods
         */
    }

    protected Set<Long> checkComplement(Set<Long> ret, boolean negate) {

        if (negate) {
            Set<Long> all = getAllNodes();
            all.removeAll(ret);
            ret = all;
        }
        return ret;

    }

    @Override
    public void open() {
        try {
            if (_db == null) {
                URL urf = new URL(_url);
                _db = new EmbeddedGraphDatabase(urf.getFile());
                Runtime.getRuntime().addShutdownHook(new Thread() {
                    @Override
                    public void run() {
                        if (_db != null)
                            _db.shutdown();
                    }
                });
            }
        } catch (Exception e) {
            throw new ThinklabRuntimeException(e);
        }
    }

    @Override
    public void close() {
        if (_db != null) {
            _db.shutdown();
            _db = null;
        }
    }

    @Override
    public IMetadata getObjectMetadata(long handle) {

        Metadata ret = new Metadata();
        Node node = _db.getNodeById(handle);

        if (node == null)
            return ret;

        try {
            for (Pair<IProperty, Object> lp : getPropertyValues(node)) {
                ret.put(lp.getFirst().toString(), lp.getSecond());
            }
        } catch (ThinklabException e) {
            // ignore, return what we have
        }

        return ret;
    }

    @Override
    public List<Long> queryIDs(IQuery query) throws ThinklabException {

        if (query != null && !(query instanceof Query)) {
            throw new ThinklabUnsupportedOperationException("query type not supported: " + query);
        }

        return queryObjects((Query) query);
    }

    @Override
    public List<ISemanticObject<?>> closestTo(IConcept type, IProperty locationProperty, Object location,
            double distanceInKm) {

        List<Long> ret = new ArrayList<Long>();
        List<Object> dst = new ArrayList<Object>();

        String idName = toId(locationProperty, null);
        DefaultLayer layer = getSpatialDB().getOrCreateDefaultLayer(idName);
        Coordinate point = null;

        // no objects, if we continue getEnvelope() will be null and the thing will
        // throw an exception from inside neo4j-spatial.
        if (layer.getIndex().isEmpty())
            return new ArrayList<ISemanticObject<?>>();

        if (location instanceof ShapeValue) {
            ShapeValue centroid;
            try {
                centroid = ((ShapeValue) location).transform(Geospace.get().getLatLonCRS()).getCentroid();
            } catch (ThinklabException e) {
                return new ArrayList<ISemanticObject<?>>();
            }
            point = centroid.getGeometry().getCoordinate();
        }

        GeoPipeline pipeline = GeoPipeline.startNearestNeighborLatLonSearch(layer, point, distanceInKm).sort(
                "Distance");

        for (SpatialRecord result : pipeline) {
            for (SpatialDatabaseRecord record : ((GeoPipeFlow) result).getRecords()) {
                Node node = _db.getNodeById(record.getNodeId());

                IConcept concept = Thinklab.c(node.getProperty(TYPE_PROPERTY).toString());
                if (concept.is(type)) {
                    Coordinate xy = record.getGeometry().getCoordinate();
                    ret.add(node.getId());
                    dst.add(new Double(GeoNS.getDistance(point, xy)));
                }
            }
        }

        return new KBoxResult(this, ret, dst, new Comparator<Object>() {
            @Override
            public int compare(Object arg0, Object arg1) {
                return ((Double) arg0).compareTo((Double) arg1);
            }
        }, null);
    }

}
