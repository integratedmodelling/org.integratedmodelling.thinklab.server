package org.integratedmodelling.thinklab.server.commands;

import java.io.File;
import java.io.IOException;

import org.integratedmodelling.auth.data.Datarecord;
import org.integratedmodelling.exceptions.ThinklabAuthorizationException;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabIOException;
import org.integratedmodelling.exceptions.ThinklabResourceNotFoundException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.project.IProject;
import org.integratedmodelling.thinklab.api.runtime.IUser;
import org.integratedmodelling.thinklab.api.runtime.IUserAssets;
import org.integratedmodelling.thinklab.command.Command;
import org.integratedmodelling.thinklab.common.utils.ZipArchive;
import org.integratedmodelling.thinklab.interfaces.annotations.ThinklabCommand;
import org.integratedmodelling.thinklab.rest.AuthorizedCommand;
import org.integratedmodelling.thinklab.rest.FileMedia;
import org.restlet.data.MediaType;

/**
 * Retrieve an asset (project or otherwise) that is accessible to a previously authorized user (the user key 
 * must be present in the REST session manager). According to asset type, the response may be JSON, a file,
 * an image or anything else appropriate.
 * 
 * @author Ferd
 *
 */
@ThinklabCommand(
        name = "get-asset",
        argumentNames = "asset,key,type",
        argumentTypes = "thinklab:Text,thinklab:Text,thinklab:Text",
        argumentDescriptions = "asset ID,user security key,asset type")
public class GetAsset extends AuthorizedCommand {

    @Override
    public Object execute(Command command, IUser user) throws ThinklabException {

        String urn = command.getArgumentAsString("asset");
        String type = command.getArgumentAsString("type");

        /*
         * TODO Generalize and systematize assets categories when
         * practical (e.g. results from user observations, direct data, other digital media).
         * Can at some point also return the WCS/WFS services directly using data IDs.
         */
        if (type.equals(IUserAssets.TYPE_PROJECT)) {

            IProject project = Thinklab.get().getProject(urn);

            if (project == null) {
                throw new ThinklabValidationException("project " + urn + " not registered");
            }

            if (!user.getAssets().getCoreProjectNames().contains(urn)) {
                throw new ThinklabValidationException("project " + urn + " not authorized for user "
                        + user.getUsername());
            }

            File tmp = null;
            try {
                tmp = File.createTempFile("project", "zip");
            } catch (IOException e) {
                throw new ThinklabIOException(e);
            }

            // zip it up, avoid git dir
            new ZipArchive(project.getLoadPath(), "*.git", "*.settings").archive(tmp);

            // send file to client
            return new FileMedia(tmp, new MediaType("application/zip"));

        } else if (type.equals(IUserAssets.TYPE_DATA_RECORD)) {

            Datarecord dr = Datarecord.retrieve(urn);
            if (dr == null) {
                throw new ThinklabResourceNotFoundException("asset " + urn + " not available");
            }

            /*
             * check authorization (user groups etc) when embedded
             */
            if (dr.allowedGroups.size() > 0) {
                for (String group : dr.allowedGroups) {
                    if (!user.getGroups().contains(group)) {
                        throw new ThinklabAuthorizationException("asset " + urn
                                + " is not authorized for this user");
                    }
                }
            }
            if (dr.allowedRoles.size() > 0) {
                for (String role : dr.allowedRoles) {
                    if (!user.getRoles().contains(role)) {
                        throw new ThinklabAuthorizationException("asset " + urn
                                + " is not authorized for this user");
                    }
                }
            }

            return dr;

        }

        return null;
    }
}
