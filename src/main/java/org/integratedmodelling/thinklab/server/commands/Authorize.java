package org.integratedmodelling.thinklab.server.commands;

import java.util.ArrayList;
import java.util.HashMap;

import org.integratedmodelling.auth.User;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.lang.IRemoteSerializable;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.project.IProject;
import org.integratedmodelling.thinklab.api.runtime.ISession;
import org.integratedmodelling.thinklab.api.runtime.IUser;
import org.integratedmodelling.thinklab.api.runtime.IUserAssets;
import org.integratedmodelling.thinklab.command.Command;
import org.integratedmodelling.thinklab.interfaces.annotations.ThinklabCommand;
import org.integratedmodelling.thinklab.interfaces.commands.ICommandHandler;
import org.integratedmodelling.thinklab.rest.RESTManager;
import org.integratedmodelling.thinklab.rest.interfaces.IStatelessService;

@ThinklabCommand(
        name = "authorize",
        argumentNames = "user,password",
        argumentTypes = "thinklab:Text,thinklab:Text",
        argumentDescriptions = "user name,security key")
public class Authorize implements ICommandHandler, IStatelessService {

    @Override
    public Object execute(Command command, ISession session) throws ThinklabException {

        HashMap<String, Object> ret = new HashMap<String, Object>();

        String username = command.getArgumentAsString("user");
        String password = command.getArgumentAsString("password");

        IUser user = User.retrieve(username);

        if (user != null) {

            if (!user.getSecurityKey().equals(password)) {
                throw new ThinklabValidationException("authorization failed for user " + username
                        + ": wrong security key");
            }

            IUserAssets assets = user.getAssets();
            RESTManager.get().registerAssetRequest(user, password, assets);

            ArrayList<Object> cproj = new ArrayList<Object>();
            for (String cp : assets.getCoreProjectNames()) {
                IProject proj = Thinklab.get().getProject(cp);
                if (cp != null && proj instanceof IRemoteSerializable) {
                    cproj.add(((IRemoteSerializable) proj).adapt());
                }
            }
            ArrayList<Object> oproj = new ArrayList<Object>();
            for (String cp : assets.getAllowedProjectNames()) {
                IProject proj = Thinklab.get().getProject(cp);
                if (cp != null && proj instanceof IRemoteSerializable) {
                    oproj.add(((IRemoteSerializable) proj).adapt());
                }
            }
            ArrayList<String> servers = new ArrayList<String>();
            servers.addAll(assets.getAllowedServers());

            ArrayList<String> roles = new ArrayList<String>();
            ArrayList<String> groups = new ArrayList<String>();
            roles.addAll(user.getRoles());
            groups.addAll(user.getGroups());

            ret.put("roles", roles);
            ret.put("groups", groups);
            ret.put("coreProjects", cproj);
            ret.put("projects", oproj);
            ret.put("servers", servers);

        } else {
            throw new ThinklabValidationException("authentication failed: user " + username
                    + " is unknown to this node");
        }

        return ret;
    }
}
