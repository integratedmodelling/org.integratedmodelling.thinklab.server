package org.integratedmodelling.thinklab.server.commands;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.runtime.IUser;
import org.integratedmodelling.thinklab.command.Command;
import org.integratedmodelling.thinklab.interfaces.annotations.ThinklabCommand;
import org.integratedmodelling.thinklab.rest.AuthorizedCommand;

@ThinklabCommand(name = "lock")
public class Lock extends AuthorizedCommand {

    @Override
    public Object execute(Command command, IUser user) throws ThinklabException {

        /*
         * TODO also check if this server is in the user's modeling servers
         */
        if (user.getRoles().contains(IUser.ROLE_ADMINISTRATOR)) {
            if (Thinklab.get().isLocked()) {
                if (!Thinklab.get().getLockingUser().getUsername().equals(user.getUsername())) {
                    /*
                     * fail - locked by other user
                     */
                } else {
                    Thinklab.get().unlock();
                }
            } else {
                Thinklab.get().lock(user, false);
            }
        }

        return Thinklab.get().isLocked();
    }
}
