package org.integratedmodelling.thinklab.geospace.extents;

import java.util.BitSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.annotation.Nullable;

import org.geotools.geometry.jts.ReferencedEnvelope;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabInternalErrorException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.lang.IRemoteSerializable;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IProperty;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.metadata.IMetadata;
import org.integratedmodelling.thinklab.api.modelling.IExtent;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.IScale;
import org.integratedmodelling.thinklab.api.modelling.ITopologicallyComparable;
import org.integratedmodelling.thinklab.api.space.ISpatialExtent;
import org.integratedmodelling.thinklab.api.time.ITemporalExtent;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.geospace.Geospace;
import org.integratedmodelling.thinklab.geospace.coverage.raster.RasterActivationLayer;
import org.integratedmodelling.thinklab.geospace.coverage.vector.VectorCoverage;
import org.integratedmodelling.thinklab.geospace.literals.ShapeValue;
import org.integratedmodelling.thinklab.interfaces.IStorageMetadataProvider;
import org.integratedmodelling.thinklab.modelling.lang.Metadata;
import org.integratedmodelling.thinklab.modelling.states.State;
import org.opengis.referencing.crs.CoordinateReferenceSystem;

/**
 * Slated to substitute all other spatial extents - defines itself to the optimal
 * representation by merging other partially specified ones.
 * 
 * @author Ferd
 *
 */
public class SpaceExtent extends State implements ISpatialExtent, Iterable<ShapeValue>,
        IStorageMetadataProvider, IRemoteSerializable {

    ShapeValue                _shape          = null;
    Grid                      _grid           = null;
    ReferencedEnvelope        _envelope       = null;
    CoordinateReferenceSystem _crs            = null;
    VectorCoverage            _features       = null;
    double                    _gridResolution = -1.0;

    int                       _type           = INCONSISTENT;

    // the four states this can be in
    /**
     * Extent has only been set with info that require a merge before
     * we can establish what the usable extent will be.
     */
    public static final int   INCONSISTENT    = -1;

    /**
     * extent represents a shape location (may be single, multiple or a point) of
     * multiplicity 1.
     */
    public static final int   SINGLE_SHAPE    = 0;

    /**
     * Extent represents more than one shape, with one state per shape.
     */
    public static final int   MULTIPLE_SHAPE  = 1;

    /**
     * Extent represents a grid, possibly with a shape that doesn't cover it
     * entirely.
     */
    public static final int   GRID            = 2;

    /*
     * cloning constructor
     */
    private SpaceExtent(SpaceExtent extent) {
        super(null);
        _shape = extent._shape;
        _grid = extent._grid;
        _envelope = extent._envelope;
        _crs = extent._crs;
        _features = extent._features;
        _type = extent._type;
    }

    @Override
    public BitSet getMask() {
        return _grid == null ? null : ((RasterActivationLayer) (_grid.activationLayer));
    }

    public SpaceExtent(ShapeValue shape) {
        super(null);
        _shape = shape;
        _envelope = shape.getEnvelope();
        _type = SINGLE_SHAPE;
    }

    public SpaceExtent(Grid grid) {
        super(null);
        _grid = grid;
        _shape = grid.getShape();
        _envelope = grid.getEnvelope();
        _type = GRID;
    }

    public SpaceExtent(ShapeValue shape, int x, int y) throws ThinklabException {
        super(null);
        // TODO no activation layer
        _grid = new Grid(shape, x, y, true);
        _shape = shape;
        _envelope = shape.getEnvelope();
        _type = GRID;
    }

    public SpaceExtent(VectorCoverage vector) {
        super(null);
        _features = vector;
        _type = MULTIPLE_SHAPE;
    }

    public SpaceExtent() {
        super(null);
        _type = INCONSISTENT;
    }

    public SpaceExtent(double resolution) {
        super(null);
        _gridResolution = resolution;
        _type = INCONSISTENT;
    }

    @Override
    public IExtent merge(IExtent extent, boolean force) throws ThinklabException {

        if (!(extent instanceof SpaceExtent)) {
            throw new ThinklabValidationException("space extent cannot merge non-space extent");
        }

        SpaceExtent ret = new SpaceExtent(this);
        SpaceExtent oth = (SpaceExtent) extent;

        /*
         * TODO figure out mandatory vs. not. These are all false, which probably
         * shouldn't be - either pass to merge or be smarter.
         */
        if (oth._grid != null) {
            ret.set(oth._grid, force);
        } else if (oth._features != null) {
            ret.set(oth._features, oth._shape, force);
        } else if (oth._shape != null) {
            ret.set(oth._shape, force);
        } else if (oth._gridResolution > 0.0) {
            ret.setGridResolution(oth._gridResolution, force);
        }

        return ret;
    }

    /*
     * constructor & merger w/ multiple shapes + optional boundary. If mandatory,
     * we HAVE to have these, and incompatible previous specs are an error.
     */
    public void set(VectorCoverage vector, @Nullable ShapeValue shape, boolean mandatory)
            throws ThinklabException {

        if (_type == INCONSISTENT) {

            if (_gridResolution > 0.0) {

                /*
                 * make grid; subset to union of shapes
                 */

            } else {
                _features = vector;
                _type = MULTIPLE_SHAPE;
            }
        } else if (_type == GRID) {

            /*
             * TODO
             * error if mandatory, else activate the grid
             */
        }

        if (shape != null) {

            /*
             * intersect existing shape if any
             */
            if (_shape != null) {
                _shape = _shape.intersection(shape);
            } else {
                _shape = shape;
            }

        }
    }

    /*
     * constructor & merger w/ grid + optional boundary. If mandatory,
     * we HAVE to have these, and incompatible previous specs are an error.
     */
    public void set(Grid grid, boolean mandatory) throws ThinklabException {

        if (_type == INCONSISTENT) {
            _grid = grid;
        } else if (!mandatory) {

            throw new ThinklabValidationException("conflicting spatial extent merge: grid -> "
                    + getTypeLabel());

        } else {

            /*
             * TODO - we want to force a grid.
             */
        }
    }

    private String getTypeLabel() {

        switch (_type) {
        case INCONSISTENT:
            return "inconsistent";
        case GRID:
            return "grid";
        case SINGLE_SHAPE:
            return "single shape";
        case MULTIPLE_SHAPE:
            return "multiple shape";
        }
        return null;
    }

    /*
     * constructor & merger w/ single shape. If mandatory,
     * we HAVE to have these, and incompatible previous specs are an error.
     */
    public void set(ShapeValue shape, boolean mandatory) throws ThinklabException {

        /*
         * TODO lots to do here - particularly, if we force a new shape on a grid, must recompute
         * multiplicity etc. The new shape may be larger if this is called by union() (which does
         * not happen at the moment, but intersection() can).
         */

        if (_type == INCONSISTENT) {

            _shape = shape;
            if (_gridResolution > 0.0) {
                _grid = new Grid(shape, _gridResolution);
                _type = GRID;
            }

        } else if (_type == GRID) {

            /*
             * mandatory? subset the grid : complain
             */
            if (mandatory) {
                _grid.createActivationLayer(shape);
            } else {
                throw new ThinklabValidationException("conflicting spatial extent merge: shape -> grid");
            }

        } else if (_type == SINGLE_SHAPE) {

            /*
             * if mandatory intersect shapes; if not, complain
             */
            if (mandatory) {
                _shape = _shape.intersection(shape);
            } else {
                throw new ThinklabValidationException("conflicting spatial extent merge: shape -> shape");
            }

        } else if (_type == MULTIPLE_SHAPE) {

            /*
             * filter the shapes that intersect the given one and set them to
             * the intersection.
             */
            _features = intersectFeatures(_features, shape);
        }
    }

    private VectorCoverage intersectFeatures(VectorCoverage _features2, ShapeValue shape) {
        throw new ThinklabRuntimeException("unsupported shape intersection");
    }

    /*
     * Set the grid resolution and become a grid if we have a boundary. If mandatory,
     * we HAVE to have this, and incompatible previous specs are an error.
     */
    public void setGridResolution(double res, boolean mandatory) throws ThinklabException {

        _gridResolution = res;

        /*
         * adopt, adapt and improve
         */
        if (_type == GRID) {

            /*
             * resample if mandatory, complain otherwise
             */
        } else if (_type == SINGLE_SHAPE) {

            /*
             * make grid
             */
            _grid = new Grid(_shape, _gridResolution);
            _type = GRID;

        } else if (_type == MULTIPLE_SHAPE) {

            /*
             * make grid with union
             */
        }

    }

    @Override
    public boolean isConsistent() {
        return _type != INCONSISTENT;
    }

    /*
     * return one of GRID, MULTIPLE_SHAPE, SINGLE_SHAPE or INCONSISTENT
     */
    public int getType() {
        return _type;
    }

    /*
     * end new
     */
    //
    // @Override
    // public double[] getDataAsDoubles() throws ThinklabException {
    // // TODO Auto-generated method stub
    // return null;
    // }
    //
    // @Override
    // public double getDoubleValue(int index) throws ThinklabException {
    // // TODO Auto-generated method stub
    // return 0;
    // }

    @Override
    public long getValueCount() {
        return getMultiplicity();
    }

    @Override
    public boolean isSpatiallyDistributed() {
        return getValueCount() > 1;
    }

    @Override
    public boolean isTemporallyDistributed() {
        return false;
    }

    @Override
    public IObserver getObserver() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getMultiplicity() {

        if (_grid != null) {
            return _grid.getCellCount();
        }

        // TODO the rest

        return 1;
    }

    @Override
    public IExtent intersection(IExtent other) throws ThinklabException {

        if (!(other instanceof SpaceExtent)) {
            throw new ThinklabValidationException("non-spatial extent being intersected with a spatial one");
        }

        SpaceExtent oth = (SpaceExtent) other;
        ShapeValue mys = getShape();
        ShapeValue its = oth.getShape();

        if (mys == null && its == null) {
            return new SpaceExtent();
        } else if (mys == null && its != null) {
            return new SpaceExtent(its);
        } else if (mys != null && its == null) {
            return new SpaceExtent(mys);
        }

        return new SpaceExtent(mys.intersection(its));
    }

    @Override
    public IExtent union(IExtent other) throws ThinklabException {

        if (!(other instanceof SpaceExtent)) {
            throw new ThinklabValidationException("non-spatial extent being intersected with a spatial one");
        }

        SpaceExtent oth = (SpaceExtent) other;
        ShapeValue mys = getShape();
        ShapeValue its = oth.getShape();

        if (mys == null && its == null) {
            return new SpaceExtent();
        } else if (mys == null && its != null) {
            return new SpaceExtent(its);
        } else if (mys != null && its == null) {
            return new SpaceExtent(mys);
        }

        return new SpaceExtent(mys.union(its));
    }

    @Override
    public boolean contains(IExtent o) throws ThinklabException {
        SpaceExtent e = (SpaceExtent) o;
        return _shape.contains(e._shape);
    }

    @Override
    public boolean overlaps(IExtent o) throws ThinklabException {
        SpaceExtent e = (SpaceExtent) o;
        return _shape.overlaps(e._shape);
    }

    @Override
    public boolean intersects(IExtent o) throws ThinklabException {
        SpaceExtent e = (SpaceExtent) o;
        return _shape.intersects(e._shape);
    }

    @Override
    public void addStorageMetadata(IMetadata metadata, IMonitor monitor) {
        /*
         * TODO check if we want anything else. This will be used in model query
         * to locate us and determine our %coverage of a context, so it'd better
         * be accurate.
         */
        if (_shape != null) {
            ((Metadata) metadata).put(NS.GEOSPACE_HAS_SHAPE, _shape);
        }
    }

    @Override
    public IConcept getDomainConcept() {
        return Geospace.get().SpatialDomain();
    }

    @Override
    public IProperty getDomainProperty() {
        return Thinklab.p(NS.SPATIAL_EXTENT_PROPERTY);
    }

    @Override
    public IExtent collapse() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ISpatialExtent getExtent(int stateIndex) {

        if (_grid != null) {
            return new SpaceExtent(_grid.getCell(stateIndex));
        }

        // TODO return n-th shape if vector coverage != null

        // TODO return self if index = 0 && it's a single shape.

        return null;
    }

    @Override
    public boolean isCovered(int stateIndex) {
        // TODO Auto-generated method stub
        return _grid == null || _grid.isCovered(stateIndex);
    }

    @Override
    public Object getValue(int index) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Iterator<ShapeValue> iterator() {
        // TODO Auto-generated method stub
        return null;
    }

    public boolean isGrid() {
        return _grid != null;
    }

    public Grid getGrid() {
        return _grid;
    }

    public ShapeValue getShape() {
        return _shape;
    }

    public ReferencedEnvelope getEnvelope() {
        return _envelope;
    }

    @Override
    public boolean isTemporal() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isSpatial() {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public ISpatialExtent getSpace() {
        return this;
    }

    @Override
    public ITemporalExtent getTime() {
        return null;
    }

    @Override
    public IProperty getCoverageProperty() {
        return Thinklab.p(NS.GEOSPACE_HAS_SHAPE);
    }

    @Override
    public Pair<ITopologicallyComparable<?>, Double> checkCoverage(ITopologicallyComparable<?> obj)
            throws ThinklabException {

        if (_shape == null) {
            throw new ThinklabInternalErrorException("checking coverage on an indefinite spatial extent");
        }

        ShapeValue shape = null;
        if (obj instanceof SpaceExtent) {
            shape = ((SpaceExtent) obj).getShape();
        } else if (obj instanceof ShapeValue) {
            shape = (ShapeValue) obj;
        }

        if (shape == null) {
            throw new ThinklabInternalErrorException("checking spatial coverage of a non-spatial object");
        }

        ShapeValue common = _shape.intersection(shape);
        double prop = common.getGeometry().getArea() / _shape.getGeometry().getArea();

        return new Pair<ITopologicallyComparable<?>, Double>(common, prop);
    }

    @Override
    public ITopologicallyComparable<IExtent> union(ITopologicallyComparable<?> obj) throws ThinklabException {

        SpaceExtent ret = new SpaceExtent(this);

        if (_shape == null) {
            return ret;
        }

        ShapeValue shape = null;
        if (obj instanceof SpaceExtent) {
            shape = ((SpaceExtent) obj).getShape();
        } else if (obj instanceof ShapeValue) {
            shape = (ShapeValue) obj;
        }

        if (shape == null) {
            return new SpaceExtent();
        }

        ShapeValue common = _shape.union(shape);

        ret.set(common, true);

        return ret;
    }

    @Override
    public ITopologicallyComparable<IExtent> intersection(ITopologicallyComparable<?> obj)
            throws ThinklabException {

        SpaceExtent ret = new SpaceExtent(this);

        if (_shape == null) {
            return ret;
        }

        ShapeValue shape = null;
        if (obj instanceof SpaceExtent) {
            shape = ((SpaceExtent) obj).getShape();
        } else if (obj instanceof ShapeValue) {
            shape = (ShapeValue) obj;
        }

        if (shape == null) {
            return new SpaceExtent();
        }

        ShapeValue common = _shape.intersection(shape);

        ret.set(common, true);

        return ret;
    }

    @Override
    public double getCoveredExtent() {
        try {
            return _shape == null ? 0 : _shape.getArea();
        } catch (ThinklabException e) {
            throw new ThinklabRuntimeException(e);
        }
    }

    @Override
    public Class<?> getDataClass() {
        return SpaceExtent.class;
    }

    @Override
    public String toString() {
        return "S[" + _envelope + "]";
    }

    @Override
    public boolean isEmpty() {
        return _shape == null || _shape.getGeometry().isEmpty();
    }

    @Override
    public double getMinX() {
        return _envelope.getMinX();
    }

    @Override
    public double getMinY() {
        return _envelope.getMinY();
    }

    @Override
    public double getMaxX() {
        return _envelope.getMaxX();
    }

    @Override
    public double getMaxY() {
        return _envelope.getMaxY();
    }

    @Override
    public IScale getScale() {

        // TODO won't be called, but it should return the scale we're part of
        return null;
    }

    @Override
    public Object adapt() {

        Map<String, Object> ret = new HashMap<String, Object>();

        ret.put("multiplicity", getValueCount());
        ret.put("domain", getDomainConcept().toString());

        ret.put("minx", getEnvelope().getMinX());
        ret.put("miny", getEnvelope().getMinY());
        ret.put("maxx", getEnvelope().getMaxX());
        ret.put("maxy", getEnvelope().getMaxY());

        if (_grid != null) {
            ret.put("grid", _grid.adapt());
        }
        ShapeValue shape = getShape();
        if (shape != null) {
            ret.put("shape", shape.toString());
        }
        try {
            ret.put("crs", Geospace.getCRSIdentifier(_crs, true));
        } catch (ThinklabException e) {
        }
        return ret;
    }

    @Override
    public int[] getDimensionSizes(boolean rowFirst) {

        if (_features != null) {
            // TODO number of features return new int[] {)
        } else if (_grid != null) {
            if (rowFirst) {
                return new int[] { _grid.getYCells(), _grid.getXCells() };
            }
            return new int[] { _grid.getXCells(), _grid.getYCells() };
        }
        return new int[] { 1 };
    }

    @Override
    public int[] getDimensionOffsets(int linearOffset, boolean rowFirst) {

        if (_features != null) {
            return new int[] { linearOffset };
        } else if (_grid != null) {
            if (rowFirst) {
                int[] xy = _grid.getXYCoordinates(linearOffset);
                return new int[] { xy[1], xy[0] };
            }
            return _grid.getXYCoordinates(linearOffset);
        }
        return new int[] { 0 };
    }

    @Override
    public ITopologicallyComparable<?> getExtent() {
        return _shape;
    }
}
