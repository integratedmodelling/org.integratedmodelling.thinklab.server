package org.integratedmodelling.thinklab.geospace.agents;

import java.util.Collection;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.annotations.SubjectType;
import org.integratedmodelling.thinklab.api.modelling.IExtent;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.api.modelling.IState;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.geospace.GeoNS;
import org.integratedmodelling.thinklab.geospace.extents.SpaceExtent;
import org.integratedmodelling.thinklab.geospace.gis.Coverage2DState;
import org.integratedmodelling.thinklab.geospace.gis.GISOperations;
import org.integratedmodelling.thinklab.modelling.lang.Subject;
import org.jgrasstools.gears.libs.modules.FlowNode;

/**
 * A specialized subject created when a im.hydrology:Watershed is computed.
 * @author Ferd
 */
@SubjectType(GeoNS.WATERSHED_CONCEPT_ID)
public class Watershed extends Subject {

    IState _dem;
    IState _totalContributingAreas;
    IState _flowDirections;
    IState _channelNetwork;

    /*
     * computed as required.
     */
    Collection<FlowNode> _streamHeads;

    public Watershed(IObservable type, Collection<IExtent> extents, Object object, INamespace namespace,
            String name) {
        super(type, extents, object, namespace, name);
    }

    public void setData(IState dem, IState tca, IState flow, IState channels) {

        _dem = dem;
        _totalContributingAreas = tca;
        _flowDirections = flow;
        _channelNetwork = channels;
    }

    public Collection<FlowNode> getStreamHeads() throws ThinklabException {

        if (_streamHeads == null) {
            _streamHeads = GISOperations.getStreamHeads(((Coverage2DState) _flowDirections).getCoverage(),
                    ((SpaceExtent) (getScale().getSpace())).getGrid().getActivationLayer());
        }

        return _streamHeads;
    }
}
