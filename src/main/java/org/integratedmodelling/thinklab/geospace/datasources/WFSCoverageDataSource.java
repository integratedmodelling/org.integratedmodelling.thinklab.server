/**
 * Copyright 2011 The ARIES Consortium (http://www.ariesonline.org) and
 * www.integratedmodelling.org. 

   This file is part of Thinklab.

   Thinklab is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published
   by the Free Software Foundation, either version 3 of the License,
   or (at your option) any later version.

   Thinklab is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Thinklab.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.integratedmodelling.thinklab.geospace.datasources;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabIOException;
import org.integratedmodelling.thinklab.api.annotations.Concept;
import org.integratedmodelling.thinklab.api.modelling.IDataSource;
import org.integratedmodelling.thinklab.geospace.coverage.CoverageFactory;

@Concept("geospace:WFSDataSource")
public class WFSCoverageDataSource extends VectorCoverageDataSource {

    /**
     * WFS service URL
     */
    String     server;

    /**
     * attribute containing the data we want. If no attribute, it's 0/1 for
     * presence/absence.
     */
    String     attr;

    /**
     * CQL expression to filter features if requested
     */
    String     filter;

    Properties properties  = new Properties();
    boolean    initialized = false;

    /**
     * 
     * @param service the WFS service URL. Cannot be null.
     * @param id the coverage ID. 
     * @param attribute
     * @param filter
     * @param valueType
     * @param valueDefault
     */
    public WFSCoverageDataSource(String service, String id, String attribute, String filter,
            String valueType, String valueDefault) {

        this.server = service;
        this._id = id;
        this.attr = attribute;
        this.filter = filter;
    }

    @Override
    public void initialize() throws ThinklabException {

        if (!initialized) {

            initialized = true;
            URL url;
            try {
                url = new URL(server);
            } catch (MalformedURLException e) {
                throw new ThinklabIOException(e);
            }

            try {
                this.coverage = CoverageFactory.readVector(url, _id, attr, filter);
            } catch (ThinklabException e) {
                if (_monitor != null && _errors == 0) {
                    _monitor.error("vector data source " + _id + " is inaccessible");
                    _errors++;
                }
                throw e;
            }
            initialized = true;
        }
    }

    public String toString() {
        return "wfs [" + _id + "]";
    }

    @Override
    public IDataSource getDatasource(String attribute) {
        return new WFSCoverageDataSource(server, _id, attribute, this.filter, null, null);
    }

}
