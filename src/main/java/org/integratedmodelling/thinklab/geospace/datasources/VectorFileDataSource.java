package org.integratedmodelling.thinklab.geospace.datasources;

import java.io.File;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabUnsupportedOperationException;
import org.integratedmodelling.thinklab.api.modelling.IDataSource;
import org.integratedmodelling.thinklab.geospace.coverage.CoverageFactory;

public class VectorFileDataSource extends VectorCoverageDataSource {

    boolean initialized = false;
    File    _file;
    String  _filter     = null;
    String  _attribute  = null;
    String  _tableUrl   = null;

    public VectorFileDataSource(String id, String file, String filter, String attr, String tableUrl)
            throws ThinklabUnsupportedOperationException {

        _id = id;
        _filter = filter;
        _attribute = attr;
        _tableUrl = tableUrl;

        File f = new File(file);
        if (f.exists()) {
            _file = f;
        }

        if (!_file.exists()) {
            throw new ThinklabUnsupportedOperationException("file " + file
                    + " cannot be read or does not have a supported raster format");
        }
    }

    @Override
    protected void initialize() throws ThinklabException {

        if (!initialized) {
            this.coverage = CoverageFactory.readVector(_file, _id, _attribute, _filter);
            initialized = true;
        }
    }

    @Override
    public IDataSource getDatasource(String attribute) {
        try {
            return new VectorFileDataSource(_id, _file.toString(), _filter, attribute, _tableUrl);
        } catch (ThinklabUnsupportedOperationException e) {
            return null;
        }
    }

}
