/**
 * Copyright 2011 The ARIES Consortium (http://www.ariesonline.org) and
 * www.integratedmodelling.org. 

   This file is part of Thinklab.

   Thinklab is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published
   by the Free Software Foundation, either version 3 of the License,
   or (at your option) any later version.

   Thinklab is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Thinklab.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.integratedmodelling.thinklab.geospace.datasources;

import java.util.Properties;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.geospace.coverage.ICoverage;
import org.integratedmodelling.thinklab.geospace.coverage.raster.AbstractRasterCoverage;
import org.integratedmodelling.thinklab.geospace.coverage.raster.WCSCoverage;

public class WCSGridDataSource extends RegularRasterGridDataSource {

    private Properties _properties = new Properties();
    private String _service;

    public WCSGridDataSource(String service, String id, double[] noData) throws ThinklabException {

        _service = service;
        _id = id;

        _properties.put(WCSCoverage.WCS_SERVICE_PROPERTY, _service);

        for (double d : noData) {
            if (!Double.isNaN(d)) {
                String s = _properties.getProperty(AbstractRasterCoverage.NODATA_PROPERTY, "");
                if (s.length() > 0)
                    s += ",";
                s += d;
                _properties.put(AbstractRasterCoverage.NODATA_PROPERTY, s);
            }
        }
    }

    public String toString() {
        return "wcs [" + _id + "]";
    }

    @Override
    protected ICoverage readData() throws ThinklabException {

        if (this._coverage == null) {
            this._coverage = new WCSCoverage(_id, _properties, _monitor);
        }
        return this._coverage;
    }

}
