/**
 * Copyright 2011 The ARIES Consortium (http://www.ariesonline.org) and
 * www.integratedmodelling.org. 

   This file is part of Thinklab.

   Thinklab is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published
   by the Free Software Foundation, either version 3 of the License,
   or (at your option) any later version.

   Thinklab is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Thinklab.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.integratedmodelling.thinklab.geospace.datasources;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.integratedmodelling.common.HashableObject;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.listeners.IMonitorable;
import org.integratedmodelling.thinklab.api.metadata.IMetadata;
import org.integratedmodelling.thinklab.api.modelling.IAccessor;
import org.integratedmodelling.thinklab.api.modelling.IAction;
import org.integratedmodelling.thinklab.api.modelling.IDataSource;
import org.integratedmodelling.thinklab.api.modelling.IExtent;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.IScale;
import org.integratedmodelling.thinklab.common.data.TableAccessor;
import org.integratedmodelling.thinklab.geospace.Geospace;
import org.integratedmodelling.thinklab.geospace.coverage.ICoverage;
import org.integratedmodelling.thinklab.geospace.coverage.raster.AbstractRasterCoverage;
import org.integratedmodelling.thinklab.geospace.coverage.raster.RasterCoverage;
import org.integratedmodelling.thinklab.geospace.extents.Grid;
import org.integratedmodelling.thinklab.geospace.extents.SpaceExtent;
import org.integratedmodelling.thinklab.geospace.literals.ShapeValue;
import org.integratedmodelling.thinklab.modelling.interfaces.IRawAccessor;
import org.integratedmodelling.thinklab.modelling.lang.Metadata;
import org.integratedmodelling.thinklab.modelling.lang.Scale;
import org.integratedmodelling.thinklab.modelling.lang.StateAccessor;

public abstract class RegularRasterGridDataSource extends HashableObject implements IDataSource, IMonitorable {

    protected ICoverage   _coverage    = null;
    protected Grid        _finalExtent = null;
    private IMetadata     _metadata    = new Metadata();
    private TableAccessor _table       = null;
    protected IMonitor    _monitor;
    protected String      _id;
    int                   _errors      = 0;

    /*
     * set in specialized constructor when we supply it with a coverage that is
     * already matched to the final context of use. This happens when a datasource
     * is created to provide an accessor for another that has transformed the
     * original coverage.
     */
    private boolean       _preMatched  = false;

    public RegularRasterGridDataSource() {
    }

    public void setLookupTable(File table, String keyDef) {

    }

    @Override
    public IMetadata getMetadata() {
        return _metadata;
    }

    public RegularRasterGridDataSource(ICoverage coverage, Grid Grid) {
        this._coverage = coverage;
        this._finalExtent = Grid;
        this._preMatched = true;
    }

    @Override
    public void setMonitor(IMonitor monitor) {
        _monitor = monitor;
    }

    @Override
    public IMonitor getMonitor() {
        return _monitor;
    }

    public Object getValue(int index) {

        if (!((AbstractRasterCoverage) _coverage).isLoaded()) {
            try {
                ((RasterCoverage) (this._coverage)).loadData();
            } catch (ThinklabException e1) {
                throw new ThinklabRuntimeException(e1);
            }
        }

        try {

            Object ret = _coverage.getSubdivisionValue(index, _finalExtent);
            if (!(ret instanceof Number))
                return ret;
            ret = ((Number) ret).doubleValue();

            /*
             * TODO/FIXME - this is already done in the coverage, although differently (just
             * comparing the value) - will never get here as it will be already a Double.NaN
             * if the same nodata values of the coverage are used.
             */
            double[] nd = ((AbstractRasterCoverage) _coverage).getNodataValue();
            if (nd != null && ret != null && (ret instanceof Double) && !Double.isNaN((Double) ret)) {
                for (double d : nd) {
                    if (((Double) ret).equals(d)) {
                        ret = Double.NaN;
                        break;
                    }
                }
            }
            return _table == null ? ret : _table.get(ret);

        } catch (ThinklabException e) {
            throw new ThinklabRuntimeException(e);
        }
    }

    @Override
    public IAccessor getAccessor(IScale context, IObserver observer, IMonitor monitor)
            throws ThinklabException {

        if (_coverage == null) {
            _coverage = readData();
        }

        if (!_preMatched)
            _finalExtent = getFinalExtent(context);

        return new RasterGridAccessor(new ArrayList<IAction>(), observer, monitor);
    }

    /*
     * -------------------------------------------------------------------------------------------
     * read the coverage
     * -------------------------------------------------------------------------------------------
     */

    /**
     * Do whatever is needed to instantiate _coverage.
     * @throws ThinklabException
     */
    protected abstract ICoverage readData() throws ThinklabException;

    /**
     * Return the final grid extent implied by the context. It should also validate
     * the context, ensuring we don't want multiplicity in domains where we cannot
     * provide it.
     * 
     * @param context
     * @return
     */
    protected Grid getFinalExtent(IScale context) throws ThinklabException {

        IExtent space = context.getSpace();

        if (!(space instanceof SpaceExtent && ((SpaceExtent) space).isGrid())) {
            throw new ThinklabValidationException("cannot compute a raster datasource in a non-grid context");
        }

        // if we got here, the resolver wanted us, so let's not take responsibility for this.
        // if (space.getMultiplicity() != context.getMultiplicity()) {
        // throw new ThinklabValidationException(
        // "extents requested to raster datasource span more domains than space");
        // }

        return ((SpaceExtent) space).getGrid();
    }

    /*
     * -------------------------------------------------------------------------------------------------
     * simple accessor
     * -------------------------------------------------------------------------------------------------
     */
    class RasterGridAccessor extends StateAccessor implements IRawAccessor {

        public RasterGridAccessor(List<IAction> actions, IObserver observer, IMonitor monitor) {
            super(actions, monitor);
            _observer = observer;
        }

        boolean   isFirst = true;
        int       _idx;
        IObserver _observer;

        @Override
        public String toString() {
            return "[raster " + _coverage.getLayerName() + "]";
        }

        @Override
        public void process(int stateIndex) throws ThinklabException {
            _idx = stateIndex;
        }

        @Override
        public Object getValue(String outputKey) {
            if (isFirst && !_preMatched) {
                try {
                    Thinklab.get().logger().info(_coverage.getEnvelope() + " -> " + _finalExtent);
                    _coverage = _coverage.requireMatch(_finalExtent, _observer, _monitor, false);
                    Thinklab.get().logger().info("match achieved for " + _coverage.getLayerName());
                } catch (ThinklabException e) {
                    throw new ThinklabRuntimeException("error retrieving data for " + outputKey + ": "
                            + e.getMessage());
                }
                isFirst = false;
            }

            return RegularRasterGridDataSource.this.getValue(_idx);
        }

        @Override
        public String getName() {
            return _coverage.getLayerName();
        }

        @Override
        public String getDatasourceLabel() {
            return "[raw data from: " + _coverage.getLayerName() + "]";
        }

    }

    @Override
    public IScale getCoverage() {

        Scale ret = new Scale();

        try {
            _coverage = readData();
        } catch (ThinklabException e) {
            Thinklab.get().logger().warn("raster data source couldn't be read: " + e.getMessage());
            if (_monitor != null && _errors == 0) {
                _monitor.error("raster data source " + _id + " is inaccessible");
                _errors++;
            }
            throw new ThinklabRuntimeException(e);
        }

        try {
            ShapeValue shape = new ShapeValue(_coverage.getEnvelope());
            shape = shape.transform(Geospace.get().getDefaultCRS());
            int x = ((AbstractRasterCoverage) _coverage).getXCells();
            int y = ((AbstractRasterCoverage) _coverage).getYCells();
            ret.mergeExtent(new SpaceExtent(shape, x, y), true);
        } catch (ThinklabException e) {
            // shouldn't happen - just in case
            if (_monitor != null && _errors == 0) {
                _monitor.error("raster data source " + _id + " is corrupted");
                _errors++;
            }
            throw new ThinklabRuntimeException(e);
        }

        return ret;
    }

}
