package org.integratedmodelling.thinklab.geospace.datasources;

import java.io.File;

import org.geotools.coverage.grid.io.AbstractGridCoverage2DReader;
import org.geotools.coverage.grid.io.AbstractGridFormat;
import org.geotools.coverage.grid.io.GridFormatFinder;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabUnsupportedOperationException;
import org.integratedmodelling.thinklab.api.metadata.IMetadata;
import org.integratedmodelling.thinklab.geospace.coverage.ICoverage;
import org.integratedmodelling.thinklab.geospace.coverage.raster.RasterCoverage;
import org.integratedmodelling.thinklab.modelling.lang.Metadata;

public class RasterFileDataSource extends RegularRasterGridDataSource {

    AbstractGridFormat _format = null;
    File _file = null;
    IMetadata _metadata = new Metadata();

    public RasterFileDataSource(String id, String file) throws ThinklabUnsupportedOperationException {

        _id = id;
        File f = new File(file);
        if (f.exists()) {
            _file = f;
            try {
                _format = GridFormatFinder.findFormat(file);
            } catch (Throwable e) {

                /*
                 * stupid SPI throws exceptions when optional formats are not available 
                 * because we haven't bought ARC. Just ignore, and _format will be null if
                 * any real problem happened.
                 */
            }
        }

        if (_format == null) {
            throw new ThinklabUnsupportedOperationException("file " + file
                    + " cannot be read or does not have a supported raster format");
        }
    }

    @Override
    protected ICoverage readData() throws ThinklabException {

        if (_coverage == null) {
            AbstractGridCoverage2DReader reader = _format.getReader(_file);
            try {
                return new RasterCoverage(_id, reader.read(null));
            } catch (Exception e) {
                throw new ThinklabUnsupportedOperationException("file " + _file
                        + " cannot be read or does not have a supported raster format");
            }
        }
        return _coverage;
    }
}
