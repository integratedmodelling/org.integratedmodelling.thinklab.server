/**
 * Copyright 2011 The ARIES Consortium (http://www.ariesonline.org) and
 * www.integratedmodelling.org. 

   This file is part of Thinklab.

   Thinklab is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published
   by the Free Software Foundation, either version 3 of the License,
   or (at your option) any later version.

   Thinklab is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Thinklab.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.integratedmodelling.thinklab.geospace.datasources;

import java.util.ArrayList;
import java.util.List;

import org.geotools.feature.FeatureIterator;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.integratedmodelling.common.HashableObject;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.exceptions.ThinklabUnsupportedOperationException;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.listeners.IMonitorable;
import org.integratedmodelling.thinklab.api.metadata.IMetadata;
import org.integratedmodelling.thinklab.api.modelling.IAccessor;
import org.integratedmodelling.thinklab.api.modelling.IDataSource;
import org.integratedmodelling.thinklab.api.modelling.IExtent;
import org.integratedmodelling.thinklab.api.modelling.IObjectSource;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.IReifiableObject;
import org.integratedmodelling.thinklab.api.modelling.IScale;
import org.integratedmodelling.thinklab.api.modelling.IStateAccessor;
import org.integratedmodelling.thinklab.geospace.Geospace;
import org.integratedmodelling.thinklab.geospace.coverage.ICoverage;
import org.integratedmodelling.thinklab.geospace.coverage.vector.AbstractVectorCoverage;
import org.integratedmodelling.thinklab.geospace.extents.Grid;
import org.integratedmodelling.thinklab.geospace.extents.SpaceExtent;
import org.integratedmodelling.thinklab.geospace.literals.ShapeValue;
import org.integratedmodelling.thinklab.modelling.lang.Metadata;
import org.integratedmodelling.thinklab.modelling.lang.Observer;
import org.integratedmodelling.thinklab.modelling.lang.Scale;
import org.opengis.feature.simple.SimpleFeature;

import com.vividsolutions.jts.geom.Geometry;

/**
 * @author Ferdinando
 */
public abstract class VectorCoverageDataSource extends HashableObject implements IDataSource, IObjectSource,
        IMonitorable {

    protected ICoverage coverage = null;
    IMetadata           metadata = new Metadata();
    IMonitor            _monitor;
    protected String    _id;

    protected int       _errors  = 0;

    @Override
    public void setMonitor(IMonitor monitor) {
        _monitor = monitor;
    }

    @Override
    public IMonitor getMonitor() {
        return _monitor;
    }

    class ShapeObject implements IReifiableObject {

        SimpleFeature _feature;
        ShapeValue    _shape;
        IScale        _scale;

        public ShapeObject(SimpleFeature feature) {
            _feature = feature;
            _shape = new ShapeValue((Geometry) _feature.getDefaultGeometry(),
                    coverage.getCoordinateReferenceSystem());
        }

        @Override
        public IScale getScale(IScale parent) {

            if (_scale == null) {

                SpaceExtent pspace = (SpaceExtent) parent.getSpace();
                try {
                    _shape = _shape.transform(pspace.getShape().getCRS());
                    _shape = _shape.intersection(pspace.getShape());
                    SpaceExtent space = new SpaceExtent(_shape);
                    _scale = new Scale(new IExtent[] { space });
                } catch (ThinklabException e) {
                    throw new ThinklabRuntimeException(e);
                }
            }

            return _scale;
        }

        @Override
        public Object getAttributeValue(String attribute, IObserver observer) {

            Object ret = _feature.getAttribute(attribute);
            if (ret == null)
                return null;

            if (observer != null && ((Observer<?>) observer).isNumericTransformation()) {
                ret = Double.parseDouble(ret.toString());
            }

            return ret;
        }

    }

    /**
     * This must ensure that coverage contains a valid vector coverage.
     * 
     * @throws ThinklabException
     */
    protected abstract void initialize() throws ThinklabException;

    @Override
    public IAccessor getAccessor(IScale context, IObserver observer, IMonitor monitor)
            throws ThinklabException {

        initialize();

        SpaceExtent space = context.getSpace() instanceof SpaceExtent ? (SpaceExtent) (context.getSpace())
                : null;

        if (space != null && space.getGrid() != null) {

            ICoverage cov = coverage.requireMatch(space.getGrid(), observer, monitor, true);
            RegularRasterGridDataSource ds = new RegularRasterGridDataSource(cov, space.getGrid()) {

                IMetadata _metadata = new Metadata();

                @Override
                protected ICoverage readData() throws ThinklabException {
                    return _coverage;
                }

                @Override
                protected Grid getFinalExtent(IScale context) throws ThinklabException {
                    return _finalExtent;
                }

                @Override
                public IMetadata getMetadata() {
                    return _metadata;
                }
            };

            return ds.getAccessor(context, observer, monitor);

        } else {

            /*
             * TODO accessor for vectors - may need to do the monster conversion if
             * the context's shapes are different
             */
            throw new ThinklabUnsupportedOperationException(
                    "vector accessors not there yet, please be patient");
        }
    }

    @Override
    public IMetadata getMetadata() {
        return metadata;
    }

    @Override
    public IScale getCoverage() {

        Scale ret = new Scale();

        try {
            initialize();
        } catch (ThinklabException e) {
            if (_monitor != null && _errors == 0) {
                _monitor.error("vector data source " + _id + " is inaccessible");
                _errors++;
            }
            return ret;
        }

        if (coverage == null) {
            if (_monitor != null && _errors == 0) {
                _monitor.error("vector data source " + _id + " is inaccessible");
                _errors++;
            }
            throw new ThinklabRuntimeException("cannot read vector data source");
        }

        try {
            ShapeValue shape = new ShapeValue(coverage.getEnvelope());
            shape = shape.transform(Geospace.get().getDefaultCRS());
            ret.mergeExtent(new SpaceExtent(shape), true);
        } catch (ThinklabException e) {
            if (_monitor != null && _errors == 0) {
                _monitor.error("vector data source " + _id + " is inaccessible");
                _errors++;
            }
            throw new ThinklabRuntimeException(e);
        }

        return ret;
    }

    @Override
    public List<IReifiableObject> getObjects(IScale scale) {

        IExtent space = scale.getSpace();
        if (!(space instanceof SpaceExtent)) {
            throw new ThinklabRuntimeException(
                    "cannot extract objects from a vector coverage in a non-spatial context");
        }

        ReferencedEnvelope env = ((SpaceExtent) space).getEnvelope();
        ArrayList<IReifiableObject> ret = new ArrayList<IReifiableObject>();

        FeatureIterator<SimpleFeature> fit = null;
        try {
            initialize();
            fit = ((AbstractVectorCoverage) coverage).getFeatureIterator(env);
            while (fit.hasNext()) {
                ret.add(new ShapeObject(fit.next()));
            }

        } catch (ThinklabException e) {
            throw new ThinklabRuntimeException(e);
        } finally {
            if (fit != null) {
                fit.close();
            }
        }

        return ret;
    }

    /**
     * Extract the first shape as a shape value.
     * 
     * @return
     * @throws ThinklabException
     */
    public ShapeValue extractFirst() throws ThinklabException {

        FeatureIterator<SimpleFeature> fit = null;
        SimpleFeature feature = null;
        try {
            initialize();
            fit = ((AbstractVectorCoverage) coverage).getFeatureIterator(null);
            if (fit.hasNext()) {
                feature = fit.next();
            }
        } finally {
            if (fit != null) {
                fit.close();
            }
        }

        return new ShapeValue((Geometry) feature.getDefaultGeometry(),
                coverage.getCoordinateReferenceSystem());
    }

    @Override
    public IStateAccessor getAccessor(String attribute, IObserver observer, IScale context, IMonitor monitor)
            throws ThinklabException {

        /*
         * if observer is presence, return rasterizing accessor with no attribute 
         */

        /*
         * else must find attribute with compatible observer and rasterize it, with
         * mediation if necessary.
         */
        // TODO Auto-generated method stub
        return null;
    }

}
