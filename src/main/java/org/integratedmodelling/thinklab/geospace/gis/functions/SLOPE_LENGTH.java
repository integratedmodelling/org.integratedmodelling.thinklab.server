package org.integratedmodelling.thinklab.geospace.gis.functions;

import java.util.Map;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IExpression;
import org.integratedmodelling.thinklab.api.project.IProject;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.geospace.gis.operators.SlopeLengthAccessor;
import org.integratedmodelling.thinklab.interfaces.annotations.Function;

@Function(id = "gis.slope-length", parameterNames = { "method" }, returnTypes = { NS.SUBJECT_ACCESSOR })
public class SLOPE_LENGTH implements IExpression {

    IProject project;

    @Override
    public Object eval(Map<String, Object> parameters, IConcept... context) throws ThinklabException {

        Double thr = null;

        if (parameters.containsKey("method")) {
            thr = Double.parseDouble(parameters.get("threshold").toString());
        }

        return new SlopeLengthAccessor(thr != null, thr == null ? 0.0 : thr);
    }

    @Override
    public void setProjectContext(IProject project) {
        this.project = project;
    }

}
