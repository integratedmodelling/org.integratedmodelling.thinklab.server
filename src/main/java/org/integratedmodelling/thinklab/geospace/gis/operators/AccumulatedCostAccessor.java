package org.integratedmodelling.thinklab.geospace.gis.operators;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.knowledge.IProperty;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.modelling.IModel;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.IState;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;

import es.unex.sextante.core.OutputFactory;
import es.unex.sextante.core.ParametersSet;
import es.unex.sextante.dataObjects.IRasterLayer;
import es.unex.sextante.geotools.GTOutputFactory;
import es.unex.sextante.gridAnalysis.accCost.AccCostAlgorithm;

public class AccumulatedCostAccessor extends BaseGISAccessor {

    public final static int EUCLIDEAN = AccCostAlgorithm.EUCLIDEAN;
    public final static int CHESSBOARD = AccCostAlgorithm.CHESSBOARD;
    public final static int MANHATTAN = AccCostAlgorithm.MANHATTAN;
    public final static int CHAMFER = AccCostAlgorithm.CHAMFER;
    public final static int WINDOW5X5 = AccCostAlgorithm.WINDOW5X5;

    IObservable _outputConcept;
    IObserver _outputObserver;

    public AccumulatedCostAccessor(int distanceType) {
    }

    @Override
    public ISubject process(ISubject subject, ISubject context, IProperty property, IMonitor monitor)
            throws ThinklabException {

        AccCostAlgorithm alg = new AccCostAlgorithm();
        ParametersSet parms = alg.getParameters();

        //		if (getInput(Thinklab.c(Geospace.NS.ELEVATION)) == null) {
        //			_monitor.error(new ThinklabValidationException("slope length: DEM not found in observed dependencies"));
        //		}

        IState input = getInputStates().iterator().hasNext() ? getInputStates().iterator().next() : null;
        if (input != null) {
            _monitor.error("cannot find input layer");
            return subject;
        }
        IRasterLayer inp = getRaster(input);

        try {
            //			parms.getParameter(AccCostAlgorithm.USETHRESHOLD).
            //				setParameterValue(new Boolean(_useThreshold));
            //			parms.getParameter(AccCostAlgorithm.THRESHOLD).
            //				setParameterValue(new Double(_threshold));
            //			parms.getParameter(AccCostAlgorithm.DEM).	
            //				setParameterValue(dem);

            OutputFactory outputFactory = new GTOutputFactory();
            alg.execute(getTaskMonitor(), outputFactory);

            //			OutputObjectsSet outputs = alg.getOutputObjects();
            //			Output slope = outputs.getOutput(SlopeLengthAlgorithm.SLOPELENGTH);
            //			publishRasterAsState((IRasterLayer) slope.getOutputObject(), 
            //					_outputConcept, _outputObserver);

        } catch (Exception e) {
            _monitor.error(e);
        }

        return subject;
    }

    @Override
    public void notifyExpectedOutput(IObservable observable, IObserver observer, String name) {
        _outputConcept = observable;
        _outputObserver = observer;
    }

    @Override
    public void notifyModel(IModel model) {
        // TODO Auto-generated method stub

    }

}
