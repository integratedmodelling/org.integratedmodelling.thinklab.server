package org.integratedmodelling.thinklab.geospace.gis.operators;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.knowledge.IProperty;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.modelling.IModel;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.geospace.Geospace;
import org.integratedmodelling.thinklab.modelling.lang.Subject;

import es.unex.sextante.core.OutputFactory;
import es.unex.sextante.core.OutputObjectsSet;
import es.unex.sextante.core.ParametersSet;
import es.unex.sextante.dataObjects.IRasterLayer;
import es.unex.sextante.geotools.GTOutputFactory;
import es.unex.sextante.morphometry.slope.SlopeAlgorithm;
import es.unex.sextante.outputs.Output;

public class SlopeAccessor extends BaseGISAccessor {

    public final static int METHOD_MAXIMUM_SLOPE = SlopeAlgorithm.METHOD_MAXIMUM_SLOPE;
    public final static int METHOD_TARBOTON = SlopeAlgorithm.METHOD_TARBOTON;
    public final static int METHOD_BURGESS = SlopeAlgorithm.METHOD_BURGESS;
    public final static int METHOD_BAUER = SlopeAlgorithm.METHOD_BAUER;
    public final static int METHOD_HEERDEGEN = SlopeAlgorithm.METHOD_HEERDEGEN;
    public final static int METHOD_ZEVENBERGEN = SlopeAlgorithm.METHOD_ZEVENBERGEN;
    public final static int METHOD_HARALICK = SlopeAlgorithm.METHOD_HARALICK;

    public final static int UNITS_RADIANS = SlopeAlgorithm.UNITS_RADIANS;
    public final static int UNITS_DEGREES = SlopeAlgorithm.UNITS_DEGREES;
    public final static int UNITS_PERCENTAGE = SlopeAlgorithm.UNITS_PERCENTAGE;

    int _method;

    IObservable _outputConcept;
    IObserver _outputObserver;

    public SlopeAccessor(int method) {
        _method = method;
    }

    @Override
    public ISubject process(ISubject subject, ISubject context, IProperty property, IMonitor monitor)
            throws ThinklabException {

        SlopeAlgorithm alg = new SlopeAlgorithm();
        ParametersSet parms = alg.getParameters();

        if (getInput(Thinklab.c(Geospace.NS.ELEVATION)) == null) {
            _monitor.error(new ThinklabValidationException(
                    "slope length: DEM not found in observed dependencies"));
        }

        /*
         * units based on the output observer 
         */
        int unit = SlopeAlgorithm.UNITS_DEGREES;

        IRasterLayer dem = getInputAsRaster(Thinklab.c(Geospace.NS.ELEVATION));

        try {
            parms.getParameter(SlopeAlgorithm.UNITS).setParameterValue(new Integer(unit));
            parms.getParameter(SlopeAlgorithm.METHOD).setParameterValue(new Integer(_method));
            parms.getParameter(SlopeAlgorithm.DEM).setParameterValue(dem);

            OutputFactory outputFactory = new GTOutputFactory();
            alg.execute(getTaskMonitor(), outputFactory);

            OutputObjectsSet outputs = alg.getOutputObjects();
            Output slope = outputs.getOutput(SlopeAlgorithm.SLOPE);
            publishRasterAsState((IRasterLayer) slope.getOutputObject(), _outputConcept, _outputObserver, (Subject)subject);

        } catch (Exception e) {
            _monitor.error(e);
        }
        return subject;
    }

    @Override
    public void notifyExpectedOutput(IObservable observable, IObserver observer, String name) {
        _outputConcept = observable;
        _outputObserver = observer;
    }

    @Override
    public void notifyModel(IModel model) {
        // TODO Auto-generated method stub

    }

}
