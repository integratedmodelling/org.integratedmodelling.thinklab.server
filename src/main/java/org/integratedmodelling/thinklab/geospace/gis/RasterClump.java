package org.integratedmodelling.thinklab.geospace.gis;

public class RasterClump {

    //    2 /****************************************************************************
    //    3  *
    //    4  * MODULE:       r.clump
    //    5  *
    //    6  * AUTHOR(S):    Michael Shapiro - CERL
    //    7  *
    //    8  * PURPOSE:      Recategorizes data in a raster map layer by grouping cells
    //    9  *               that form physically discrete areas into unique categories.
    //   10  *
    //   11  * COPYRIGHT:    (C) 2006 by the GRASS Development Team
    //   12  *
    //   13  *               This program is free software under the GNU General Public
    //   14  *               License (>=v2). Read the file COPYING that comes with GRASS
    //   15  *               for details.
    //   16  *
    //   17  ***************************************************************************/
    //   18 
    //   19 #include <time.h>
    //   20 #include <grass/gis.h>
    //   21 #include <grass/glocale.h>
    //   22 #include "local_proto.h"
    //   23 
    //   24 #define INCR 1024
    //   25 
    //   26 
    //   27 CELL clump(int in_fd, int out_fd)
    //   28 {
    //   29     register int col;
    //   30     register CELL *prev_clump, *cur_clump;
    //   31     register CELL *index;
    //   32     register int n;
    //   33     CELL *prev_in, *cur_in;
    //   34     CELL *temp_cell, *temp_clump, *out_cell;
    //   35     CELL X, UP, LEFT, NEW, OLD;
    //   36     CELL label;
    //   37     int nrows, ncols;
    //   38     int row;
    //   39     int len;
    //   40     int pass;
    //   41     int nalloc;
    //   42     long cur_time;
    //   43     int column;
    //   44 
    //   45 
    //   46     nrows = G_window_rows();
    //   47     ncols = G_window_cols();
    //   48 
    //   49     /* allocate reclump index */
    //   50     nalloc = INCR;
    //   51     index = (CELL *) G_malloc(nalloc * sizeof(CELL));
    //   52     index[0] = 0;
    //   53 
    //   54     /* allocate CELL buffers one column larger than current window */
    //   55     len = (ncols + 1) * sizeof(CELL);
    //   56     prev_in = (CELL *) G_malloc(len);
    //   57     cur_in = (CELL *) G_malloc(len);
    //   58     prev_clump = (CELL *) G_malloc(len);
    //   59     cur_clump = (CELL *) G_malloc(len);
    //   60     out_cell = (CELL *) G_malloc(len);
    //   61 
    //   62 /******************************** PASS 1 ************************************
    //   63  * first pass thru the input simulates the clump to determine
    //   64  * the reclumping index.
    //   65  * second pass does the clumping for real
    //   66  */
    //   67     time(&cur_time);
    //   68     for (pass = 1; pass <= 2; pass++) {
    //   69     /* second pass must generate a renumbering scheme */
    //   70     if (pass == 2) {
    //   71         CELL cat, *renumber;
    //   72 
    //   73         renumber = (CELL *) G_malloc((label + 1) * sizeof(CELL));
    //   74         cat = 1;
    //   75         for (n = 1; n <= label; n++)
    //   76         renumber[n] = 0;
    //   77         for (n = 1; n <= label; n++)
    //   78         renumber[index[n]] = 1;
    //   79         for (n = 1; n <= label; n++)
    //   80         if (renumber[n])
    //   81             renumber[n] = cat++;
    //   82         for (n = 1; n <= label; n++)
    //   83         index[n] = renumber[index[n]];
    //   84         G_free(renumber);
    //   85     }
    //   86 
    //   87     /* fake a previous row which is all zero */
    //   88     G_zero(prev_in, len);
    //   89     G_zero(prev_clump, len);
    //   90 
    //   91     /* create a left edge of zero */
    //   92     cur_in[0] = 0;
    //   93     cur_clump[0] = 0;
    //   94 
    //   95     /* initialize clump labels */
    //   96     label = 0;
    //   97 
    //   98     G_message(_("Pass %d..."), pass);
    //   99     for (row = 0; row < nrows; row++) {
    //  100         if (G_get_map_row(in_fd, cur_in + 1, row) < 0)
    //  101         G_fatal_error(_("Unable to read raster map row %d "),
    //  102                   row);
    //  103         
    //  104         G_percent(row+1, nrows, 2);
    //  105         X = 0;
    //  106         for (col = 1; col <= ncols; col++) {
    //  107         LEFT = X;
    //  108         X = cur_in[col];
    //  109         if (X == 0) {   /* don't clump zero data */
    //  110             cur_clump[col] = 0;
    //  111             continue;
    //  112         }
    //  113 
    //  114         UP = prev_in[col];
    //  115 
    //  116         /*
    //  117          * if the cell value is different above and to the left
    //  118          * then we must start a new clump
    //  119          *
    //  120          * this new clump may eventually collide with another
    //  121          * clump and have to be merged
    //  122          */
    //  123         if (X != LEFT && X != UP) { /* start a new clump */
    //  124             label++;
    //  125             cur_clump[col] = label;
    //  126             if (pass == 1) {
    //  127             if (label >= nalloc) {
    //  128                 nalloc += INCR;
    //  129                 index =
    //  130                 (CELL *) G_realloc(index,
    //  131                            nalloc * sizeof(CELL));
    //  132             }
    //  133             index[label] = label;
    //  134             }
    //  135             continue;
    //  136         }
    //  137         if (X == LEFT && X != UP) { /* same clump as to the left */
    //  138             cur_clump[col] = cur_clump[col - 1];
    //  139             continue;
    //  140         }
    //  141         if (X == UP && X != LEFT) { /* same clump as above */
    //  142             cur_clump[col] = prev_clump[col];
    //  143             continue;
    //  144         }
    //  145 
    //  146         /*
    //  147          * at this point the cell value X is the same as LEFT and UP
    //  148          * so it should go into the same clump. It is possible for
    //  149          * the clump value on the left to differ from the clump value
    //  150          * above. If this happens we have a conflict and one of the
    //  151          * LEFT or UP needs to be reclumped
    //  152          */
    //  153         if (cur_clump[col - 1] == prev_clump[col]) {    /* ok */
    //  154             cur_clump[col] = prev_clump[col];
    //  155             continue;
    //  156         }
    //  157 
    //  158         /* conflict! preserve the clump from above and change the left.
    //  159          * Must also go back to the left in the current row and to the right
    //  160          * in the previous row to change all the clump values as well.
    //  161          *
    //  162          */
    //  163 
    //  164         NEW = prev_clump[col];
    //  165         OLD = cur_clump[col - 1];
    //  166         cur_clump[col] = NEW;
    //  167 
    //  168         /* to left
    //  169            for (n = 1; n < col; n++)
    //  170            if (cur_clump[n] == OLD)
    //  171            cur_clump[n] = NEW;
    //  172          */
    //  173 
    //  174         temp_cell = cur_clump++;
    //  175         n = col - 1;
    //  176         while (n-- > 0)
    //  177             if (*cur_clump == OLD)
    //  178             *cur_clump++ = NEW;
    //  179             else
    //  180             cur_clump++;
    //  181         cur_clump = temp_cell;
    //  182 
    //  183         /* to right
    //  184            for (n = col+1; n <= ncols; n++)
    //  185            if (prev_clump[n] == OLD)
    //  186            prev_clump[n] = NEW;
    //  187          */
    //  188 
    //  189         temp_cell = prev_clump;
    //  190         prev_clump += col + 1;
    //  191         n = ncols - col;
    //  192         while (n-- > 0)
    //  193             if (*prev_clump == OLD)
    //  194             *prev_clump++ = NEW;
    //  195             else
    //  196             prev_clump++;
    //  197         prev_clump = temp_cell;
    //  198 
    //  199         /* modify the indexes
    //  200            if (pass == 1)
    //  201            for (n = 1; n <= label; n++)
    //  202            if (index[n] == OLD)
    //  203            index[n] = NEW;
    //  204          */
    //  205 
    //  206         if (pass == 1) {
    //  207             temp_cell = index++;
    //  208             n = label;
    //  209             while (n-- > 0)
    //  210             if (*index == OLD)
    //  211                 *index++ = NEW;
    //  212             else
    //  213                 index++;
    //  214             index = temp_cell;
    //  215         }
    //  216         }
    //  217 
    //  218         if (pass == 2) {
    //  219         /*
    //  220            for (col = 1; col <= ncols; col++)
    //  221            out_cell[col] = index[cur_clump[col]];
    //  222 
    //  223            if (G_put_raster_row (out_fd, out_cell+1, CELL_TYPE) < 0)
    //  224            G_fatal_error (_("Unable to properly write output raster map"));
    //  225          */
    //  226         col = ncols;
    //  227         temp_clump = cur_clump + 1; /* skip left edge */
    //  228         temp_cell = out_cell;
    //  229 
    //  230         while (col-- > 0)
    //  231             *temp_cell++ = index[*temp_clump++];
    //  232 
    //  233         for (column = 0; column < ncols; column++) {
    //  234             if (out_cell[column] == 0)
    //  235             G_set_null_value(&out_cell[column], 1, CELL_TYPE);
    //  236         }
    //  237         if (G_put_raster_row(out_fd, out_cell, CELL_TYPE) < 0)
    //  238             G_fatal_error(_("Failed writing raster map row %d"),
    //  239                   row);
    //  240         }
    //  241 
    //  242         /* switch the buffers so that the current buffer becomes the previous */
    //  243         temp_cell = cur_in;
    //  244         cur_in = prev_in;
    //  245         prev_in = temp_cell;
    //  246 
    //  247         temp_clump = cur_clump;
    //  248         cur_clump = prev_clump;
    //  249         prev_clump = temp_clump;
    //  250     }
    //  251 
    //  252     print_time(&cur_time);
    //  253     }
    //  254     return 0;
    //  255 }
    //  256 
    //  257 int print_time(long *start)
    //  258 {
    //  259     int hours, minutes, seconds;
    //  260     long done;
    //  261 
    //  262     time(&done);
    //  263 
    //  264     seconds = done - *start;
    //  265     *start = done;
    //  266 
    //  267     hours = seconds / 3600;
    //  268     minutes = (seconds - hours * 3600) / 60;
    //  269     seconds = seconds % 60;
    //  270 
    //  271     if (hours)
    //  272     G_verbose_message("%2d:%02d:%02d", hours, minutes, seconds);
    //  273     else if (minutes)
    //  274     G_verbose_message("%d:%02d", minutes, seconds);
    //  275     else
    //  276     G_verbose_message("%d seconds", seconds);
    //  277 
    //  278     return 0;
    //  279 }

}
