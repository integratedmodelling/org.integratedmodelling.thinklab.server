package org.integratedmodelling.thinklab.geospace.gis.operators;

import java.awt.image.RenderedImage;
import java.util.HashMap;

import javax.media.jai.iterator.RandomIter;
import javax.media.jai.iterator.RandomIterFactory;

import org.geotools.coverage.grid.GridCoverage2D;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IProperty;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.IState;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.geospace.coverage.raster.RasterCoverage;
import org.integratedmodelling.thinklab.geospace.extents.Grid;
import org.integratedmodelling.thinklab.geospace.extents.SpaceExtent;
import org.integratedmodelling.thinklab.geospace.gis.GISOperations;
import org.integratedmodelling.thinklab.modelling.interfaces.IExtendedDataset;
import org.integratedmodelling.thinklab.modelling.lang.Subject;
import org.integratedmodelling.thinklab.modelling.lang.SubjectAccessor;
import org.integratedmodelling.thinklab.modelling.states.DatasetBackedState;
import org.integratedmodelling.thinklab.modelling.states.IndexedObjectState;
import org.integratedmodelling.thinklab.modelling.states.IndexedState;

import es.unex.sextante.core.ITaskMonitor;
import es.unex.sextante.dataObjects.IRasterLayer;
import es.unex.sextante.dataObjects.IVectorLayer;
import es.unex.sextante.geotools.GTRasterLayer;

public abstract class BaseGISAccessor extends SubjectAccessor {

    protected ITaskMonitor getTaskMonitor() {
        return new GISOperations.TaskMonitor(_monitor);
    }

    /**
     * Build a coverage from the numeric transposition of the
     * passed state.
     * 
     * @param observable
     * @return
     */
    protected IRasterLayer getInputAsRaster(IConcept observable) {

        IState state = getInput(observable);
        if (state == null)
            return null;

        GTRasterLayer ret = null;

        try {

            RasterCoverage coverage = new RasterCoverage(state);
            ret = new GTRasterLayer();
            ret.create(coverage.getCoverage());

        } catch (ThinklabException e) {
            _monitor.error("error building a raster representation of state " + observable);
            return null;
        }

        return ret;
    }

    protected IRasterLayer getRaster(IState state) {

        GTRasterLayer ret = null;

        try {

            RasterCoverage coverage = new RasterCoverage(state);
            ret = new GTRasterLayer();
            ret.create(coverage.getCoverage());

        } catch (ThinklabException e) {
            _monitor.error("error building a raster representation of state " + state.getDirectType());
            return null;
        }

        return ret;
    }

    /*
     * build a feature collection from all the subjects of the passed
     * type.
     */
    protected IVectorLayer getInputAsVector(IConcept observable) {

        /*
         * TODO
         */

        return null;
    }

    /**
     * 
     * @param concept
     * @param observer
     */
    protected void publishRasterAsState(IRasterLayer layer, IObservable concept, IObserver observer, Subject subject) {

        GTRasterLayer l = (GTRasterLayer) layer;
        SpaceExtent ext = (SpaceExtent) _scale.getSpace();

        if (ext.getGrid() == null) {
            _monitor.error("cannot return a raster layer from a non-grid extent");
            return;
        }

        Grid grid = ext.getGrid();
        Double[] data = new Double[grid.getCellCount()];

        for (int i = 0; i < grid.getCellCount(); i++) {
            int[] xy = grid.getXYCoordinates(i);
            data[i] = l.getCellValueAsDouble(xy[0], xy[1]);
        }

        IndexedState state = 
                subject.getBackingDataset() == null ? 
                       new IndexedObjectState(concept, subject.getScale(), observer) :
                       new DatasetBackedState(concept, subject.getScale(), observer, (IExtendedDataset)subject.getBackingDataset());
                       
         state.setValue(data);
                       
        try {
            ((Subject) subject).addState(state);
        } catch (ThinklabException e) {
            _monitor.error(e);
        }

    }
    

    public IState publishRasterAsState(GridCoverage2D layer, IObservable observable, IObserver observer, Subject subject, IMonitor monitor) throws ThinklabException {

        SpaceExtent ext = (SpaceExtent) subject.getScale().getSpace();

        if (ext.getGrid() == null) {
            throw new ThinklabValidationException("cannot return a raster layer from a non-grid extent");
        }

        Grid grid = ext.getGrid();
        Double[] data = new Double[grid.getCellCount()];
        RenderedImage image = layer.getRenderedImage();
        RandomIter itera = RandomIterFactory.create(image, null);

        for (int i = 0; i < grid.getCellCount(); i++) {
            int[] xy = grid.getXYCoordinates(i);
            data[i] = itera.getSampleDouble(xy[0], xy[1], 0);
        }

        IndexedState state = 
                subject.getBackingDataset() == null ? 
                       new IndexedObjectState(observable, subject.getScale(), observer) :
                       new DatasetBackedState(observable, subject.getScale(), observer, (IExtendedDataset)subject.getBackingDataset());
                       
         state.setValue(data);
         
         try {
             ((Subject) subject).addState(state);
         } catch (ThinklabException e) {
             monitor.error(e);
         }
         
         return state;
    }


    protected void publishFeaturesAsSubjects(IVectorLayer features, IConcept concept,
            HashMap<String, IProperty> attributeTable) {
    }

}
