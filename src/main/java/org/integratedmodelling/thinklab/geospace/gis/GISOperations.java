package org.integratedmodelling.thinklab.geospace.gis;

import java.awt.image.RenderedImage;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.media.jai.iterator.RandomIter;
import javax.media.jai.iterator.RandomIterFactory;

import org.geotools.coverage.grid.GridCoverage2D;
import org.integratedmodelling.collections.Triple;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.listeners.INotification;
import org.integratedmodelling.thinklab.api.modelling.IDataset;
import org.integratedmodelling.thinklab.api.modelling.IState;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.common.utils.StringUtils;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.debug.DataRecorder;
import org.integratedmodelling.thinklab.geospace.GeoNS;
import org.integratedmodelling.thinklab.geospace.coverage.raster.RasterCoverage;
import org.integratedmodelling.thinklab.geospace.extents.Grid;
import org.integratedmodelling.thinklab.geospace.extents.SpaceExtent;
import org.integratedmodelling.thinklab.geospace.interfaces.IGridMask;
import org.integratedmodelling.thinklab.modelling.interfaces.IExtendedDataset;
import org.integratedmodelling.thinklab.modelling.lang.Metadata;
import org.integratedmodelling.thinklab.modelling.states.DatasetBackedState;
import org.integratedmodelling.thinklab.modelling.states.IndexedObjectState;
import org.jgrasstools.gears.libs.modules.FlowNode;
import org.jgrasstools.gears.libs.modules.Variables;
import org.jgrasstools.gears.libs.monitor.IJGTProgressMonitor;
import org.jgrasstools.gears.utils.RegionMap;
import org.jgrasstools.gears.utils.coverage.CoverageUtilities;
import org.jgrasstools.hortonmachine.modules.demmanipulation.markoutlets.OmsMarkoutlets;
import org.jgrasstools.hortonmachine.modules.demmanipulation.pitfiller.OmsPitfiller;
import org.jgrasstools.hortonmachine.modules.demmanipulation.wateroutlet.OmsExtractBasin;
import org.jgrasstools.hortonmachine.modules.geomorphology.flow.OmsFlowDirections;
import org.jgrasstools.hortonmachine.modules.geomorphology.slope.OmsSlope;
import org.jgrasstools.hortonmachine.modules.geomorphology.tca.OmsTca;
import org.jgrasstools.hortonmachine.modules.network.extractnetwork.OmsExtractNetwork;

import es.unex.sextante.core.ITaskMonitor;
import es.unex.sextante.core.OutputFactory;
import es.unex.sextante.core.OutputObjectsSet;
import es.unex.sextante.core.ParametersSet;
import es.unex.sextante.dataObjects.IRasterLayer;
import es.unex.sextante.geotools.GTOutputFactory;
import es.unex.sextante.geotools.GTRasterLayer;
import es.unex.sextante.hydrology.fillSinks.FillSinksAlgorithm;
import es.unex.sextante.outputs.Output;

public class GISOperations {

    public final static String  INFO_CLASS                 = "GIS_PROCESS";
    private static final double BASIN_IMPORTANCE_THRESHOLD = 0.05;
    private static final int    WATERSHEDS_TO_EXTRACT      = 1;

    public static class TaskMonitor implements ITaskMonitor, IJGTProgressMonitor {

        IMonitor       _monitor;
        private String _name;

        public TaskMonitor(IMonitor monitor) {
            _monitor = monitor;
        }

        @Override
        public void setProgress(int iStep) {
            _monitor.addProgress(iStep, "GIS processing");
        }

        @Override
        public void setProgress(int step, int totalNumberOfSteps) {
            _monitor.defineProgressSteps(totalNumberOfSteps);
            _monitor.addProgress(step, "GIS processing");
        }

        @Override
        public void setProgressText(String sText) {
            // if (sText != null && !sText.trim().isEmpty())
            // _monitor.info(sText, INFO_CLASS);
        }

        @Override
        public boolean isCanceled() {
            return _monitor.isStopped();
        }

        @Override
        public void close() {
        }

        @Override
        public void setDeterminate(boolean bDeterminate) {
        }

        @Override
        public void setProcessDescription(String sDescription) {
        }

        @Override
        public void setDescriptionPrefix(String sPrefix) {
        }

        // JGT methods
        @Override
        public void beginTask(String name, int totalWork) {
            _name = name;
            _monitor.defineProgressSteps(totalWork);
            // _monitor.info("beginning " + name, INFO_CLASS);
        }

        @Override
        public void message(String message) {
            // _monitor.info(message, INFO_CLASS);
        }

        @Override
        public void errorMessage(String message) {
            _monitor.error(message);
        }

        @Override
        public void done() {
            // _monitor.info("finished " + _name, INFO_CLASS);
        }

        @Override
        public void internalWorked(double work) {
            // TODO Auto-generated method stub

        }

        @Override
        public void setCanceled(boolean value) {
            if (value) {
                _monitor.stop(null);
            }
        }

        @Override
        public void setTaskName(String name) {
            _name = name;
        }

        @Override
        public void subTask(String name) {
        }

        @Override
        public void worked(int work) {
            _monitor.addProgress(work, "");
        }

        @Override
        public <T> T adapt(Class<T> adaptee) {
            // TODO Auto-generated method stub
            return null;
        }
    }

    public static IRasterLayer stateToRaster(IState state) throws ThinklabException {

        GTRasterLayer ret = null;
        RasterCoverage coverage = new RasterCoverage(state);
        ret = new GTRasterLayer();
        ret.create(coverage.getCoverage());
        return ret;
    }

    public static Collection<FlowNode> getStreamHeads(GridCoverage2D inFlow, IGridMask mask)
            throws ThinklabException {

        List<FlowNode> ret = new ArrayList<FlowNode>();
        RegionMap regionMap = CoverageUtilities.getRegionParamsFromGridCoverage(inFlow);
        int cols = regionMap.getCols();
        int rows = regionMap.getRows();

        RenderedImage flowRI = inFlow.getRenderedImage();
        RandomIter flowIter = RandomIterFactory.create(flowRI, null);

        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < cols; c++) {
                FlowNode flowNode = new FlowNode(flowIter, cols, rows, c, r);
                // TODO check translation of row, col to x, y
                if (flowNode.isSource() /* && (mask == null || mask.isActive(c, r)) */) {
                    ret.add(flowNode);
                }
            }
        }
        flowIter.done();
        return ret;
    }

    /**
     * Turn a Thinklab state into a Geotools GridCoverage2D.
     * 
     * @param state
     * @return
     * @throws ThinklabException
     */
    public static GridCoverage2D stateToCoverage(IState state) throws ThinklabException {
        RasterCoverage coverage = new RasterCoverage(state);
        return coverage.getCoverage();
    }

    public static IState rasterToState(IRasterLayer layer, IObservable observable, ISubject context)
            throws ThinklabException {

        GTRasterLayer l = (GTRasterLayer) layer;
        SpaceExtent ext = (SpaceExtent) context.getScale().getSpace();

        if (ext.getGrid() == null) {
            throw new ThinklabValidationException("cannot return a raster layer from a non-grid extent");
        }

        Grid grid = ext.getGrid();
        Double[] data = new Double[grid.getCellCount()];

        for (int i = 0; i < grid.getCellCount(); i++) {
            int[] xy = grid.getXYCoordinates(i);
            data[i] = l.getCellValueAsDouble(xy[0], xy[1]);
        }

        return new IndexedObjectState(data, observable, context);
    }

    public static IState coverageToState(GridCoverage2D layer, IObservable observable, ISubject context,
            String label, IDataset dataset) throws ThinklabException {

        SpaceExtent ext = (SpaceExtent) context.getScale().getSpace();

        /*
         * FIXME just return a new Coverage2DState
         */

        if (ext.getGrid() == null) {
            throw new ThinklabValidationException("cannot return a raster layer from a non-grid extent");
        }

        Grid grid = ext.getGrid();
        double[] data = new double[grid.getCellCount()];
        RenderedImage image = layer.getRenderedImage();
        RandomIter itera = RandomIterFactory.create(image, null);

        for (int i = 0; i < grid.getCellCount(); i++) {
            int[] xy = grid.getXYCoordinates(i);
            data[i] = itera.getSampleDouble(xy[0], xy[1], 0);
        }

        IState ret = dataset == null ? new IndexedObjectState(data, observable, context)
                : new DatasetBackedState(data, observable, context, (IExtendedDataset) dataset);

        /*
         * TODO collect metadata from concept hierarchy
         */
        ((Metadata) (ret.getMetadata())).put(NS.DISPLAY_LABEL_PROPERTY, label);

        return ret;
    }

    /**
     * Simply pass a non-corrected DEM to receive the same DEM with pits filled. Won't do
     * any checking on the input semantics. Of course it will complain vigorously if not 
     * a spatial raster state.
     * 
     * TODO switch to JGrassTools.
     * 
     * @param dem
     * @return
     * @throws ThinklabException 
     */
    public static IState fillSinks(IState demState, ISubject context, IMonitor monitor)
            throws ThinklabException {

        FillSinksAlgorithm alg = new FillSinksAlgorithm();
        ParametersSet parms = alg.getParameters();
        Output fdem = null;
        TaskMonitor mon = new TaskMonitor(monitor);

        IRasterLayer dem = stateToRaster(demState);

        try {
            parms.getParameter(FillSinksAlgorithm.DEM).setParameterValue(dem);
            OutputFactory outputFactory = new GTOutputFactory();

            alg.execute(mon, outputFactory);

            OutputObjectsSet outputs = alg.getOutputObjects();
            fdem = outputs.getOutput(FillSinksAlgorithm.RESULT);

        } catch (Exception e) {
            monitor.error(e);
            throw new ThinklabValidationException(e);
        }
        return rasterToState((IRasterLayer) fdem.getOutputObject(), demState.getObservable(), context);

    }

    /**
     * Perform the classic set of watershed computations and return all states produced. This one uses
     * JGrass.
     * 
     * @param dem
     * @param context
     * @param monitor
     * @param dataset if not null, used as a backup for states computed.
     * @return
     */
    public static Map<IConcept, GridCoverage2D> watershedAnalysis(IState demState, ISubject context, IMonitor monitor)
            throws ThinklabException {
        Map<IConcept, GridCoverage2D> ret = new HashMap<IConcept, GridCoverage2D>();
        Grid space = ((SpaceExtent) (demState.getSpace())).getGrid();
        GridCoverage2D dem = stateToCoverage(demState);

        /*
         * Pit filling.
         * 
         * TODO intercept pit estimate and provide feedback - this one is the 
         * longest running. Also there should be a provision for caching.
         */
        OmsPitfiller pitfiller = new OmsPitfiller();
        pitfiller.inElev = dem;
        pitfiller.pm = new TaskMonitor(monitor);
        pitfiller.doProcess = true;
        pitfiller.doReset = false;
        monitor.info("filling hydrological sinks...", INFO_CLASS);
        try {
            pitfiller.process();
        } catch (Exception e) {
            throw new ThinklabException(e);
        }

        ret.put(GeoNS.PIT_FILLED_ELEVATION, pitfiller.outPit);

        /*
         * flow directions
         */
        OmsFlowDirections omsflowdirections = new OmsFlowDirections();
        omsflowdirections.inPit = pitfiller.outPit;
        omsflowdirections.pm = pitfiller.pm;
        omsflowdirections.doProcess = true;
        omsflowdirections.doReset = false;
        monitor.info("computing flow directions...", INFO_CLASS);
        try {
            omsflowdirections.process();
        } catch (Exception e) {
            throw new ThinklabException(e);
        }

        ret.put(GeoNS.FLOW_DIRECTION, omsflowdirections.outFlow);

        /*
         * omstca!
         */
        OmsTca omstca = new OmsTca();
        omstca.inFlow = omsflowdirections.outFlow;
        omstca.doProcess = true;
        omstca.doReset = false;
        omstca.pm = omsflowdirections.pm;
        monitor.info("computing drainage area per cell...", INFO_CLASS);
        try {
            omstca.process();
        } catch (Exception e) {
            throw new ThinklabException(e);
        }

        /*
         * TODO intercept warnings from watershed delineation that doesn't end at the boundaries
         * and push them up to the monitor.
         */
        ret.put(GeoNS.TOTAL_CONTRIBUTING_AREA, omstca.outTca);

        /*
         * outlets
         */
        OmsMarkoutlets out = new OmsMarkoutlets();
        out.inFlow = omsflowdirections.outFlow;
        out.doProcess = true;
        out.doReset = false;
        out.pm = omsflowdirections.pm;
        monitor.info("finding outlets...", INFO_CLASS);
        try {
            out.process();
        } catch (Exception e) {
            throw new ThinklabException(e);
        }

        /*
         * TODO (if requested): channel network
         */

        /*
         * extract all outlets as single coordinates first, then make objects out of them.
         */
        RenderedImage image = out.outFlow.getRenderedImage();
        int npix = image.getHeight() * image.getWidth();
        RandomIter itera = RandomIterFactory.create(image, null);
        RenderedImage imtca = omstca.outTca.getRenderedImage();
        RandomIter itca = RandomIterFactory.create(imtca, null);
        ArrayList<Triple<Double, Double, Double>> outlets = new ArrayList<Triple<Double, Double, Double>>();
        for (int x = 0; x < image.getWidth(); x++) {
            for (int y = 0; y < image.getHeight(); y++) {
                if (itera.getSampleDouble(x, y, 0) == FlowNode.OUTLET) {
                    double importance = (itca.getSampleDouble(x, y, 0) / ((double) npix));
                    if (importance >= BASIN_IMPORTANCE_THRESHOLD) {
                        DataRecorder.debug("outlet at " + x + "," + y + " drains " + importance);
                        double[] xy = space.getCoordinatesAt(x, y);
                        outlets.add(new Triple<Double, Double, Double>(xy[0], xy[1], importance));
                    }
                }
            }
        }

        monitor.info(
                "found " + outlets.size() + " basins covering "
                        + StringUtils.percent(BASIN_IMPORTANCE_THRESHOLD) + " or more", INFO_CLASS);

        /*
         * TODO create subject outlets so we can lookup the hydrograph etc. They should contain
         * the total drained area.
         */
        IGridMask mask = null;
        if (outlets.size() > 0) {

            /*
             * sort outlets in descending drainage area order and take the one(s) with highest drainage.
             */
            Collections.sort(outlets, new Comparator<Triple<Double, Double, Double>>() {

                @Override
                public int compare(Triple<Double, Double, Double> arg0, Triple<Double, Double, Double> arg1) {
                    return arg1.getThird().compareTo(arg0.getThird());
                }
            });

            /*
             * if there's no activation layer, make a fully active one.
             */
            mask = space.requireActivationLayer(true);

            for (int i = 0; i < WATERSHEDS_TO_EXTRACT && i < outlets.size(); i++) {

                monitor.info("basin " + (i + 1) + " covers " + StringUtils.percent(outlets.get(0).getThird())
                        + " of area", INFO_CLASS);

                /*
                 * extract the basin. TODO make this and the threshold configurable.
                 */
                OmsExtractBasin ebasin = new OmsExtractBasin();
                ebasin.inFlow = omsflowdirections.outFlow;
                ebasin.pEast = outlets.get(0).getFirst();
                ebasin.pNorth = outlets.get(0).getSecond();
                ebasin.doProcess = true;
                ebasin.doReset = false;
                ebasin.doVector = false; // TODO set to true when we generate the shape;
                try {
                    ebasin.process();
                } catch (Exception e) {
                    throw new ThinklabException(e);
                }

                /*
                 * TODO add computed geometry to mask; set into scale as shape in the
                 * end.
                 */

                /*
                 * load watershed mask into scale's activation layer, merging with
                 * any current mask already set.
                 */
                RenderedImage bimg = ebasin.outBasin.getRenderedImage();
                RandomIter bit = RandomIterFactory.create(bimg, null);
                for (int x = 0; x < image.getWidth(); x++) {
                    for (int y = 0; y < image.getHeight(); y++) {
                        if (bit.getSampleDouble(x, y, 0) == 1.0) {
                            if (mask.isActive(x, space.getYCells() - y - 1)) {
                                mask.activate(x, space.getYCells() - y - 1);
                            }
                        } else {
                            mask.deactivate(x, space.getYCells() - y - 1);
                        }
                    }
                }
            }

        } else {
            monitor.warn("no watersheds found: check DEM");
        }

        
        /*
         * slope for the channel network 
         */
        OmsSlope slope = new OmsSlope();
        slope.inFlow = omsflowdirections.outFlow;
        slope.inPit = pitfiller.outPit;
        slope.doProcess = true;
        slope.doReset = false;
        slope.pm = pitfiller.pm;
        try {
            slope.process();
        } catch (Exception e) {
            throw new ThinklabException(e);
        }

        monitor.info("extracting channel network...", INFO_CLASS);

        /*
         * channel network
         * TODO try to observe a stream layer and use it to anchor the streams if available 
         * (it's another jgrass module - see HM presentation).
         */
        OmsExtractNetwork onet = new OmsExtractNetwork();
        onet.inFlow = omsflowdirections.outFlow;
        onet.inTca = omstca.outTca;
        // onet.inSlope = slope.outSlope;

        // TODO pass area threshold from parameters and in future, scale analysis. For now, take the cells
        // that drain
        // at least 9% of the basin area.
        onet.pThres = (mask == null ? 
                        ((double) space.getXCells() * (double) space.getYCells()) :
                         (double) mask.totalActiveCells()) * .09;
        onet.pMode = Variables.TCA;

        monitor.info("TCA threshold is " + onet.pThres, INFO_CLASS);

        onet.pm = pitfiller.pm;
        onet.doProcess = true;
        onet.doReset = false;
        try {
            onet.process();
        } catch (Exception e) {
            throw new ThinklabException(e);
        }

        monitor.info("watershed analysis completed.", INFO_CLASS);

        ret.put(GeoNS.STREAM_PRESENCE, onet.outNet);

        if (Thinklab.get().getNotificationLevel() >= INotification.DEBUG) {
            for (GridCoverage2D g : ret.values()) {
                g.show();
            }
        }

        return ret;
    }
}
