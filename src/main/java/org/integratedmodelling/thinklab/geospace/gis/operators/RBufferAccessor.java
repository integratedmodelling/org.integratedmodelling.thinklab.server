package org.integratedmodelling.thinklab.geospace.gis.operators;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.knowledge.IProperty;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.modelling.IModel;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.IState;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.geospace.gis.GISOperations;
import org.integratedmodelling.thinklab.modelling.lang.Subject;
import org.integratedmodelling.thinklab.modelling.states.State;

import es.unex.sextante.core.OutputFactory;
import es.unex.sextante.core.OutputObjectsSet;
import es.unex.sextante.core.ParametersSet;
import es.unex.sextante.dataObjects.IRasterLayer;
import es.unex.sextante.geotools.GTOutputFactory;
import es.unex.sextante.gridTools.rasterBuffer.RasterBufferAlgorithm;
import es.unex.sextante.outputs.Output;

public class RBufferAccessor extends BaseGISAccessor {

    /*
     * TODO use Sextante's, but the values in it are identical and screwed.
     */
    public static final int FIXED_DISTANCE = 0;
    public static final int USE_CELL_VALUE_AS_BUFFER_DISTANCE = 1;

    double _distance;
    int _method = FIXED_DISTANCE;
    IObservable _outputConcept;
    IObserver _outputObserver;

    public RBufferAccessor(double distance, int method) {
        _distance = distance;
    }

    @Override
    public ISubject process(ISubject subject, ISubject context, IProperty property, IMonitor monitor)
            throws ThinklabException {

        RasterBufferAlgorithm alg = new RasterBufferAlgorithm();
        ParametersSet parms = alg.getParameters();

        /*
         * there should be just one non-raw input 
         */
        IState input = null;
        for (IState s : getInputStates()) {
            if (!((State) s).isRaw()) {
                input = s;
                break;
            }
        }

        if (input == null) {
            _monitor.error("cannot determine input state for raster buffering");
            return null;
        } else {
            _monitor.info("distance buffering executing on " + input.getDirectType(),
                    GISOperations.INFO_CLASS);
        }

        /*
         * TODO translate distance to the appropriate measurement for the
         * projection. 
         */
        IRasterLayer inp = getRaster(input);

        try {
            parms.getParameter(RasterBufferAlgorithm.METHOD).setParameterValue(new Integer(_method));
            parms.getParameter(RasterBufferAlgorithm.DIST).setParameterValue(new Double(_distance));
            parms.getParameter(RasterBufferAlgorithm.INPUT).setParameterValue(inp);

            OutputFactory outputFactory = new GTOutputFactory();
            alg.execute(getTaskMonitor(), outputFactory);

            OutputObjectsSet outputs = alg.getOutputObjects();
            Output slope = outputs.getOutput(RasterBufferAlgorithm.BUFFER_LAYER);
            publishRasterAsState((IRasterLayer) slope.getOutputObject(), _outputConcept, _outputObserver, (Subject)subject);

        } catch (Exception e) {
            _monitor.error(e);
        }
        return subject;
    }

    @Override
    public void notifyExpectedOutput(IObservable observable, IObserver observer, String name) {
        _outputConcept = observable;
        _outputObserver = observer;
    }

    @Override
    public void notifyModel(IModel model) {
        // TODO Auto-generated method stub

    }

}
