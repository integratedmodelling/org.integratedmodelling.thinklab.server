/**
 * Copyright 2011 The ARIES Consortium (http://www.ariesonline.org) and
 * www.integratedmodelling.org. 

   This file is part of Thinklab.

   Thinklab is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published
   by the Free Software Foundation, either version 3 of the License,
   or (at your option) any later version.

   Thinklab is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Thinklab.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.integratedmodelling.thinklab.geospace.gis;

import java.awt.image.RenderedImage;

import javax.media.jai.iterator.RandomIter;
import javax.media.jai.iterator.RandomIterFactory;

import org.geotools.coverage.grid.GridCoverage2D;
import org.geotools.feature.FeatureIterator;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.geospace.Geospace;
import org.integratedmodelling.thinklab.geospace.coverage.raster.RasterActivationLayer;
import org.integratedmodelling.thinklab.geospace.coverage.raster.RasterCoverage;
import org.integratedmodelling.thinklab.geospace.coverage.vector.AbstractVectorCoverage;
import org.integratedmodelling.thinklab.geospace.extents.Grid;
import org.integratedmodelling.thinklab.geospace.interfaces.IGridMask;
import org.integratedmodelling.thinklab.geospace.literals.ShapeValue;
import org.opengis.feature.simple.SimpleFeature;

public class ThinklabRasterizer {

    /**
     * Convert the passed vector coverage into a raster coverage that adopts the 
     * passed extent.
     * 
     * @param vCoverage
     * @param extent
     * @return
     */
    public static RasterCoverage rasterize(AbstractVectorCoverage vCoverage, String valueId, float noData,
            Grid extent, IObserver observer, String valueDefault, IMonitor monitor, boolean isWFS)
            throws ThinklabException {

        if (extent.getCRS() != null)
            vCoverage = (AbstractVectorCoverage) vCoverage.requireMatch(extent, observer, monitor, false);

        GridCoverage2D coverage = null;
        FeatureRasterizer rasterizer = new FeatureRasterizer(extent, noData, (valueId == null ? null
                : vCoverage.getAttributeDescriptor(valueId)), monitor);

        FeatureIterator<SimpleFeature> iterator = null;
        try {

            ReferencedEnvelope dataEnvelope = null;

            try {
                dataEnvelope = extent.getEnvelope().transform(vCoverage.getCoordinateReferenceSystem(), true);
            } catch (Exception e) {
                throw new ThinklabValidationException(e);
            }

            iterator = vCoverage.getFeatureIterator(extent.getEnvelope(), valueId);

            coverage = rasterizer.rasterize(vCoverage.getLayerName() + "_" + (valueId == null ? "" : valueId)
                    + "_raster",
                    iterator,
                    valueId,
                    observer,
                    valueDefault,
                    dataEnvelope,
                    // FIXME this MAY work for WFS in the current implementation, but how the hell do I know.
                    Geospace.getCRSIdentifier(dataEnvelope.getCoordinateReferenceSystem(), true).equals(
                            "EPSG:4326")
                            && isWFS);

        } finally {
            if (iterator != null)
                iterator.close();
        }

        RasterCoverage ret = new RasterCoverage(vCoverage.getLayerName() + "_"
                + (valueId == null ? "" : valueId) + "_raster", coverage);

        if (rasterizer.getClassification() != null) {
            ret.setClassMappings(rasterizer.getClassification());
        }

        return ret;
    }

    private static IGridMask rasterizeShape(ShapeValue shape, Grid grid, int value) throws ThinklabException {

        RasterActivationLayer ret = (RasterActivationLayer) createMask(grid);
        GridCoverage2D coverage = null;

        ret.activate();
        
        // temporarily disabled to check issues with upside down features
        
        //        FeatureRasterizer rasterizer = new FeatureRasterizer(grid, 0.0f, null, null);
//
//        coverage = rasterizer.rasterize(shape, value);
//
//        
//        /*
//         * turn coverage into mask
//         */
//        RenderedImage image = coverage.getRenderedImage();
//        RandomIter itera = RandomIterFactory.create(image, null);
//
//        for (int i = 0; i < grid.getCellCount(); i++) {
//
//            int[] xy = grid.getXYCoordinates(i);
//
//            if (itera.getSampleDouble(xy[0], xy[1], 0) > 0.0) {
//                ret.activate(xy[0], xy[1]);
//            }
//        }
        return ret;
    }

    public static IGridMask createMask(Grid grid) {
        RasterActivationLayer ret = new RasterActivationLayer(grid.getXCells(), grid.getYCells(), false, grid);
        ret.setCRS(grid.getCRS());
        return ret;
    }

    public static IGridMask createMask(ShapeValue shape, Grid grid) throws ThinklabException {
        return rasterizeShape(shape, grid, 1);
    }

    public static IGridMask addToMask(ShapeValue shape, IGridMask mask) throws ThinklabException {
        mask.or(rasterizeShape(shape, mask.getGrid(), 1));
        return mask;
    }

}
