package org.integratedmodelling.thinklab.geospace.gis.operators;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.knowledge.IProperty;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.modelling.IModel;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.geospace.Geospace;
import org.integratedmodelling.thinklab.modelling.lang.Subject;

import es.unex.sextante.core.OutputFactory;
import es.unex.sextante.core.OutputObjectsSet;
import es.unex.sextante.core.ParametersSet;
import es.unex.sextante.dataObjects.IRasterLayer;
import es.unex.sextante.geotools.GTOutputFactory;
import es.unex.sextante.morphometry.aspect.AspectAlgorithm;
import es.unex.sextante.outputs.Output;

public class AspectAccessor extends BaseGISAccessor {

    public final static int METHOD_MAXIMUM_SLOPE = AspectAlgorithm.METHOD_MAXIMUM_SLOPE;
    public final static int METHOD_TARBOTON = AspectAlgorithm.METHOD_TARBOTON;
    public final static int METHOD_BURGESS = AspectAlgorithm.METHOD_BURGESS;
    public final static int METHOD_BAUER = AspectAlgorithm.METHOD_BAUER;
    public final static int METHOD_HEERDEGEN = AspectAlgorithm.METHOD_HEERDEGEN;
    public final static int METHOD_ZEVENBERGEN = AspectAlgorithm.METHOD_ZEVENBERGEN;
    public final static int METHOD_HARALICK = AspectAlgorithm.METHOD_HARALICK;

    public final static int UNITS_RADIANS = AspectAlgorithm.UNITS_RADIANS;
    public final static int UNITS_DEGREES = AspectAlgorithm.UNITS_DEGREES;
    public final static int UNITS_PERCENTAGE = AspectAlgorithm.UNITS_PERCENTAGE;

    int _method;

    IObservable _outputConcept;
    IObserver _outputObserver;

    public AspectAccessor(int method) {
        _method = method;
    }

    @Override
    public ISubject process(ISubject subject, ISubject context, IProperty property, IMonitor monitor)
            throws ThinklabException {

        AspectAlgorithm alg = new AspectAlgorithm();
        ParametersSet parms = alg.getParameters();

        if (getInput(Thinklab.c(Geospace.NS.ELEVATION)) == null) {
            _monitor.error(new ThinklabValidationException(
                    "slope length: DEM not found in observed dependencies"));
        }

        /*
         * units based on the output observer 
         */
        int unit = AspectAlgorithm.UNITS_DEGREES;

        IRasterLayer dem = getInputAsRaster(Thinklab.c(Geospace.NS.ELEVATION));

        try {
            parms.getParameter(AspectAlgorithm.UNITS).setParameterValue(new Integer(unit));
            parms.getParameter(AspectAlgorithm.METHOD).setParameterValue(new Integer(_method));
            parms.getParameter(AspectAlgorithm.DEM).setParameterValue(dem);

            OutputFactory outputFactory = new GTOutputFactory();
            alg.execute(getTaskMonitor(), outputFactory);

            OutputObjectsSet outputs = alg.getOutputObjects();
            Output slope = outputs.getOutput(AspectAlgorithm.ASPECT);
            publishRasterAsState((IRasterLayer) slope.getOutputObject(), _outputConcept, _outputObserver, (Subject)subject);

        } catch (Exception e) {
            _monitor.error(e);
        }
        return subject;
    }

    @Override
    public void notifyExpectedOutput(IObservable observable, IObserver observer, String name) {
        _outputConcept = observable;
        _outputObserver = observer;
    }

    @Override
    public void notifyModel(IModel model) {
        // TODO Auto-generated method stub

    }

}
