package org.integratedmodelling.thinklab.geospace.gis;

import java.awt.image.RenderedImage;

import javax.media.jai.iterator.RandomIter;
import javax.media.jai.iterator.RandomIterFactory;

import org.geotools.coverage.grid.GridCoverage2D;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.IScale;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.geospace.extents.Grid;
import org.integratedmodelling.thinklab.geospace.extents.SpaceExtent;
import org.integratedmodelling.thinklab.modelling.states.IndexedObjectState;

/**
 * Wraps a GridCoverage2D into a IState to enable switching back and forth without
 * copying data.
 * 
 * @author Ferd
 *
 */
public class Coverage2DState extends IndexedObjectState {

    GridCoverage2D _coverage;
    private final Grid _grid;
    private final RenderedImage _image;
    private final RandomIter _itera;

    public Coverage2DState(GridCoverage2D coverage, IObservable observable, IScale scale, IObserver observer) {

        super(observable, scale, observer);
        SpaceExtent ext = (SpaceExtent) scale.getSpace();

        if (ext.getGrid() == null) {
            throw new ThinklabRuntimeException("cannot return a raster layer from a non-grid extent");
        }

        _grid = ext.getGrid();
        _image = coverage.getRenderedImage();
        _itera = RandomIterFactory.create(_image, null);
        _coverage = coverage;
    }

    @Override
    public void setValue(int index, Object val) {
        throw new ThinklabRuntimeException("coverage states are read-only");
    }

    @Override
    public Object getValue(int contextIndex) {
        int[] xy = _grid.getXYCoordinates(contextIndex);
        return _itera.getSampleDouble(xy[0], xy[1], 0);
    }

    @Override
    public long getValueCount() {
        return (_grid.getXCells() * _grid.getYCells());
    }

    @Override
    public Class<?> getDataClass() {
        return Number.class;
    }

    public GridCoverage2D getCoverage() {
        return _coverage;
    }

}
