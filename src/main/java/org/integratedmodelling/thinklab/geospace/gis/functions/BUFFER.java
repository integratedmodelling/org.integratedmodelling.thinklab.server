package org.integratedmodelling.thinklab.geospace.gis.functions;

import java.util.Map;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IExpression;
import org.integratedmodelling.thinklab.api.project.IProject;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.geospace.gis.operators.RBufferAccessor;
import org.integratedmodelling.thinklab.interfaces.annotations.Function;

/**
 * Raster buffering. TODO should automatically switch to vector buffering if
 * the context is vector.
 * 
 * @author Ferd
 *
 */
@Function(
        id = "gis.buffer",
        parameterNames = { "distance", "use-data-as-distance" },
        returnTypes = { NS.SUBJECT_ACCESSOR })
public class BUFFER implements IExpression {

    IProject project;

    @Override
    public Object eval(Map<String, Object> parameters, IConcept... context) throws ThinklabException {

        int method = RBufferAccessor.FIXED_DISTANCE;

        double distance = 10;
        if (parameters.containsKey("distance")) {
            distance = Double.parseDouble(parameters.get("distance").toString());
        }

        if (parameters.containsKey("use-data-as-distance")) {
            if (parameters.get("use-data-as-distance") instanceof Boolean
                    && (Boolean) parameters.get("use-data-as-distance")) {
                method = RBufferAccessor.USE_CELL_VALUE_AS_BUFFER_DISTANCE;
            }
        }

        return new RBufferAccessor(distance, method);
    }

    @Override
    public void setProjectContext(IProject project) {
        this.project = project;
    }

}
