package org.integratedmodelling.thinklab.geospace.gis.functions;

import java.util.Map;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IExpression;
import org.integratedmodelling.thinklab.api.project.IProject;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.geospace.gis.operators.AccumulatedCostAccessor;
import org.integratedmodelling.thinklab.interfaces.annotations.Function;

@Function(id = "gis.distance", parameterNames = { "method" }, returnTypes = { NS.SUBJECT_ACCESSOR })
public class DISTANCE implements IExpression {

    IProject project;

    @Override
    public Object eval(Map<String, Object> parameters, IConcept... context) throws ThinklabException {
        return new AccumulatedCostAccessor(AccumulatedCostAccessor.EUCLIDEAN);
    }

    @Override
    public void setProjectContext(IProject project) {
        this.project = project;
    }

}
