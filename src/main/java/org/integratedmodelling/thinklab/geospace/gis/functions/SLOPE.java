package org.integratedmodelling.thinklab.geospace.gis.functions;

import java.util.Map;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IExpression;
import org.integratedmodelling.thinklab.api.project.IProject;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.geospace.gis.operators.SlopeAccessor;
import org.integratedmodelling.thinklab.interfaces.annotations.Function;

@Function(id = "gis.slope", parameterNames = { "method" }, returnTypes = { NS.SUBJECT_ACCESSOR })
public class SLOPE implements IExpression {

    IProject project;

    @Override
    public Object eval(Map<String, Object> parameters, IConcept... context) throws ThinklabException {

        int method = SlopeAccessor.METHOD_HARALICK;

        if (parameters.containsKey("method")) {

            String m = parameters.get("method").toString().toLowerCase();
            if (m.equals("burgess")) {
                method = SlopeAccessor.METHOD_BURGESS;
            } else if (m.equals("bauer")) {
                method = SlopeAccessor.METHOD_BAUER;
            } else if (m.equals("tarboton")) {
                method = SlopeAccessor.METHOD_TARBOTON;
            } else if (m.equals("heerdegen")) {
                method = SlopeAccessor.METHOD_HEERDEGEN;
            } else if (m.equals("zevenbergen")) {
                method = SlopeAccessor.METHOD_ZEVENBERGEN;
            } else if (m.equals("haralick")) {
                method = SlopeAccessor.METHOD_HARALICK;
            } else if (m.equals("maximum-slope")) {
                method = SlopeAccessor.METHOD_MAXIMUM_SLOPE;
            } else {
                throw new ThinklabValidationException("slope method " + m + " not recognized");
            }
        }

        return new SlopeAccessor(method);
    }

    @Override
    public void setProjectContext(IProject project) {
        this.project = project;
    }

}
