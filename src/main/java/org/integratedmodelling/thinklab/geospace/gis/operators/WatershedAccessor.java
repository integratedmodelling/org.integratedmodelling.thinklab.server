package org.integratedmodelling.thinklab.geospace.gis.operators;

import java.util.ArrayList;
import java.util.Map;

import org.geotools.coverage.grid.GridCoverage2D;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabInternalErrorException;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IExpression;
import org.integratedmodelling.thinklab.api.knowledge.IProperty;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.modelling.IDataset;
import org.integratedmodelling.thinklab.api.modelling.IModel;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.IState;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.api.project.IProject;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.geospace.GeoNS;
import org.integratedmodelling.thinklab.geospace.gis.GISOperations;
import org.integratedmodelling.thinklab.interfaces.annotations.Function;
import org.integratedmodelling.thinklab.modelling.lang.Subject;

/**
 * Watershed computation for one pre-defined scale (will adjust it to the size of the largest watershed).
 * Used as a model to compute one watershed. Not an object source yet, so will probably make a mess
 * when used to compute the concept within a region.
 */
@Function(id = "hydrology.watershed", parameterNames = {}, returnTypes = { NS.SUBJECT_ACCESSOR })
public class WatershedAccessor extends BaseGISAccessor implements IExpression {

    // IObservable _outputConcept;
    // IObserver _outputObserver;
    IModel                                  _model;
    ArrayList<Pair<IObservable, IObserver>> _outputs = new ArrayList<Pair<IObservable, IObserver>>();

    public WatershedAccessor() {
    }

    IProject project;

    @Override
    public Object eval(Map<String, Object> parameters, IConcept... context) throws ThinklabException {
        return new WatershedAccessor();
    }

    @Override
    public void setProjectContext(IProject project) {
        this.project = project;
    }

    @Override
    public ISubject process(ISubject subject, ISubject context, IProperty property, IMonitor monitor)
            throws ThinklabException {

        GeoNS.synchronize();

        IDataset dataset = ((Subject) subject).getBackingDataset();

        /*
         * TODO check scale; if not a grid, make it one. If a point, see what we can
         * do. If scale changed, reset it in subject observer.
         */

        IState elevation = null;
        for (IState s : subject.getStates()) {
            if (s.getObservable().is(GeoNS.ELEVATION)) {
                elevation = s;
                break;
            }
        }

        if (elevation == null) {
            /*
             * internal and unlikely: we shouldn't get here if we couldn't resolve elevation
             */
            throw new ThinklabInternalErrorException("watershed accessor cannot find elevation data: aborting");
        }

        /*
         * cross fingers
         */
        Map<IConcept, GridCoverage2D> results = GISOperations.watershedAnalysis(elevation, subject, monitor);

        /*
         * make these available through the observer, not the subject, so that they can 
         * resolve downstream dependencies too. 
         */
        for (Pair<IObservable, IObserver> oo : _outputs) {
            GridCoverage2D coverage = results.get(oo.getSecond().getObservable().getType());
            if (coverage != null) {
                this.publishRasterAsState(coverage, oo.getSecond().getObservable(), oo.getSecond(), (Subject) subject, monitor);
            }
        }

        return subject;
    }

    @Override
    public void notifyExpectedOutput(IObservable observable, IObserver observer, String name) {
        _outputs.add(new Pair<IObservable, IObserver>(observable, observer));
    }

    @Override
    public void notifyModel(IModel model) {
        _model = model;
    }

}
