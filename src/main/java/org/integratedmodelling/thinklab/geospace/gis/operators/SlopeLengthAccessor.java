package org.integratedmodelling.thinklab.geospace.gis.operators;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.knowledge.IProperty;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.modelling.IModel;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.geospace.Geospace;
import org.integratedmodelling.thinklab.modelling.lang.Subject;

import es.unex.sextante.core.OutputFactory;
import es.unex.sextante.core.OutputObjectsSet;
import es.unex.sextante.core.ParametersSet;
import es.unex.sextante.dataObjects.IRasterLayer;
import es.unex.sextante.geotools.GTOutputFactory;
import es.unex.sextante.hydrology.slopeLength.SlopeLengthAlgorithm;
import es.unex.sextante.outputs.Output;

public class SlopeLengthAccessor extends BaseGISAccessor {

    double _threshold;
    boolean _useThreshold;

    IObservable _outputConcept;
    IObserver _outputObserver;

    public SlopeLengthAccessor(boolean useThreshold, double threshold) {
        _threshold = threshold;
        _useThreshold = useThreshold;
    }

    @Override
    public ISubject process(ISubject subject, ISubject context, IProperty property, IMonitor monitor)
            throws ThinklabException {

        SlopeLengthAlgorithm alg = new SlopeLengthAlgorithm();
        ParametersSet parms = alg.getParameters();

        if (getInput(Thinklab.c(Geospace.NS.ELEVATION)) == null) {
            _monitor.error(new ThinklabValidationException(
                    "slope length: DEM not found in observed dependencies"));
        }

        IRasterLayer dem = getInputAsRaster(Thinklab.c(Geospace.NS.ELEVATION));

        try {
            parms.getParameter(SlopeLengthAlgorithm.USETHRESHOLD).setParameterValue(
                    new Boolean(_useThreshold));
            parms.getParameter(SlopeLengthAlgorithm.THRESHOLD).setParameterValue(new Double(_threshold));
            parms.getParameter(SlopeLengthAlgorithm.DEM).setParameterValue(dem);

            OutputFactory outputFactory = new GTOutputFactory();
            alg.execute(getTaskMonitor(), outputFactory);

            OutputObjectsSet outputs = alg.getOutputObjects();
            Output slope = outputs.getOutput(SlopeLengthAlgorithm.SLOPELENGTH);
            publishRasterAsState((IRasterLayer) slope.getOutputObject(), _outputConcept, _outputObserver, (Subject)subject);

        } catch (Exception e) {
            _monitor.error(e);
        }
        return subject;
    }

    @Override
    public void notifyExpectedOutput(IObservable observable, IObserver observer, String name) {
        _outputConcept = observable;
        _outputObserver = observer;
    }

    @Override
    public void notifyModel(IModel model) {
        // TODO Auto-generated method stub

    }

}
