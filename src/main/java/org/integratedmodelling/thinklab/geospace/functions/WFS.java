package org.integratedmodelling.thinklab.geospace.functions;

import java.util.Map;

import org.integratedmodelling.auth.data.Datarecord;
import org.integratedmodelling.exceptions.ThinklabAuthorizationException;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.data.IDataAsset;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IExpression;
import org.integratedmodelling.thinklab.api.project.IProject;
import org.integratedmodelling.thinklab.cache.CacheUtils;
import org.integratedmodelling.thinklab.common.utils.URLUtils;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.geospace.Geospace;
import org.integratedmodelling.thinklab.geospace.datasources.WFSCoverageDataSource;
import org.integratedmodelling.thinklab.interfaces.annotations.Function;
import org.integratedmodelling.thinklab.modelling.resolver.ModelData;

@Function(id = "wfs", parameterNames = {
        "urn",
        "service",
        "id",
        "attr",
        "filter",
        "extract",
        "value-type",
        "default-value",
        "table",
        "key" }, returnTypes = { NS.DATASOURCE, NS.SHAPE })
public class WFS implements IExpression {

    private IProject _project;

    @Override
    public Object eval(Map<String, Object> parameters, IConcept... context) throws ThinklabException {

        String id = null;
        String service = null;

        if (parameters.containsKey("urn")) {

            if (Thinklab.get().getLockingUser() == null) {
                throw new ThinklabAuthorizationException("data URN " + parameters.get("urn")
                        + " unavailable or unauthorized");
            }

            Datarecord dr = CacheUtils.getDatarecord(parameters.get("urn").toString(), Thinklab.get()
                    .getLockingUser());
            if (dr != null) {
                service = dr.getAttribute(IDataAsset.KEY_URL);
                id = dr.getAttribute(IDataAsset.KEY_DATAID);
            }

        } else {

            Object serv = parameters.get("service");
            if (serv instanceof String) {
                service = serv.toString();
            } else if (serv instanceof String[]) {
                String[] ss = (String[]) serv;
                for (String s : ss) {
                    if (URLUtils.ping(s)) {
                        service = s;
                        break;
                    }
                }
            }
            id = parameters.get("id").toString();
        }

        if (service == null || id == null)
            return null;

        String attribute = parameters.containsKey("attr") ? parameters.get("attr").toString()
                : null;
        String filter = parameters.containsKey("filter") ? parameters.get("filter").toString() : null;
        String valueType = parameters.containsKey("value-type") ? parameters.get("value-type").toString()
                : null;
        String valueDefault = parameters.containsKey("default-value") ? parameters.get("default-value")
                .toString() : null;
        String extract = parameters.containsKey("extract") ? parameters.get("extract").toString() : null;

        if (attribute != null && attribute.equals(ModelData.PRESENCE_ATTRIBUTE)) {
            attribute = null;
        }

        if (filter == null && extract != null) {
            filter = (extract.isEmpty() || extract.equals("true")) ? null : extract;
        }

        Object ret = new WFSCoverageDataSource(service, id, attribute, filter, valueType, valueDefault);

        if (extract != null) {

            /*
             * extract the first shape
             */
            ret = ((WFSCoverageDataSource) ret).extractFirst().transform(Geospace.getCRSFromID("urn:ogc:def:crs:EPSG:4326"));
        }

        return ret;
    }

    @Override
    public void setProjectContext(IProject project) {
        _project = project;
    }

}
