package org.integratedmodelling.thinklab.geospace.functions;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IExpression;
import org.integratedmodelling.thinklab.api.project.IProject;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.geospace.Geospace;
import org.integratedmodelling.thinklab.geospace.coverage.CoverageFactory;
import org.integratedmodelling.thinklab.geospace.coverage.vector.VectorCoverage;
import org.integratedmodelling.thinklab.geospace.extents.Grid;
import org.integratedmodelling.thinklab.geospace.extents.SpaceExtent;
import org.integratedmodelling.thinklab.geospace.literals.ShapeValue;
import org.integratedmodelling.thinklab.interfaces.annotations.Function;

@Function(id = "space", parameterNames = {
        "shape",
        "projection",
        "features",
        "grid",
        "vector-file",
        "service",
        "shape-id" }, returnTypes = { NS.SPACE_DOMAIN })
public class SPACE implements IExpression {

    IProject project;

    @Override
    public Object eval(Map<String, Object> parameters, IConcept... context) throws ThinklabException {

        ShapeValue shape = null;
        String crs = "EPSG:4326";
        double resolution = -1.0;
        VectorCoverage vectors = null;

        if (parameters.containsKey("projection")) {
            crs = parameters.get("projection").toString();
        }

        /*
         * shape is explicit (shape) or relative to a corner or center
         */
        if (parameters.containsKey("shape")) {
            if (parameters.get("shape") instanceof ShapeValue) {
                shape = (ShapeValue) parameters.get("shape");
            } else {
                shape = new ShapeValue(parameters.get("shape").toString());
            }

            shape = shape.transform(Geospace.get().getDefaultCRS());
        }

        if (parameters.containsKey("grid")) {
            if (parameters.get("grid") instanceof Integer) {
                resolution = ((Integer) (parameters.get("grid"))).doubleValue();
            } else {
                resolution = Grid.parseResolution(parameters.get("grid").toString());
            }
        }

        if (parameters.containsKey("features")) {
            URL url;
            try {
                url = new URL(parameters.get("features").toString());
            } catch (MalformedURLException e) {
                throw new ThinklabValidationException(e);
            }
            vectors = (VectorCoverage) CoverageFactory.readVector(url, null, null, null);
        }

        Grid grid = null;
        if (shape != null && resolution > 0.0) {
            grid = new Grid(shape, resolution);
        }

        SpaceExtent ret = null;

        if (grid != null) {
            ret = new SpaceExtent(grid);
        } else if (vectors != null) {
            ret = new SpaceExtent(vectors);
            if (shape != null) {
                ret.set(shape, true);
            }
        } else if (shape != null) {
            ret = new SpaceExtent(shape);
        } else if (resolution > 0.0) {
            ret = new SpaceExtent(resolution);
        } else {
            ret = new SpaceExtent();
        }

        return ret;

    }

    @Override
    public void setProjectContext(IProject project) {
        this.project = project;
    }

}
