package org.integratedmodelling.thinklab.geospace.functions;

import java.io.File;
import java.util.Map;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IExpression;
import org.integratedmodelling.thinklab.api.project.IProject;
import org.integratedmodelling.thinklab.common.utils.MiscUtilities;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.geospace.datasources.VectorFileDataSource;
import org.integratedmodelling.thinklab.interfaces.annotations.Function;
import org.integratedmodelling.thinklab.modelling.resolver.ModelData;

@Function(
        id = "vector",
        parameterNames = { "file", "id", "filter", "attr", "table", "key" },
        returnTypes = { NS.DATASOURCE })
public class VECTOR implements IExpression {

    IProject project;

    @Override
    public Object eval(Map<String, Object> parameters, IConcept... context) throws ThinklabException {

        File input = new File(project.getLoadPath() + File.separator + parameters.get("file").toString());

        if (!input.exists()) {
            input = new File(parameters.get("file").toString());
        }

        if (input.exists()) {

            String id = parameters.containsKey("id") ? parameters.get("id").toString() : MiscUtilities
                    .getFileBaseName(input.toString());

            String attr = parameters.containsKey("attr") ? parameters.get("attr").toString() : null;
            String filter = parameters.containsKey("id") ? parameters.get("id").toString() : null;
            String tableUrl = parameters.containsKey("table") ? parameters.get("table").toString() : null;

            if (attr != null && attr.equals(ModelData.PRESENCE_ATTRIBUTE)) {
                attr = null;
            }

            return new VectorFileDataSource(id, input.toString(), filter, attr, tableUrl);
        }
        return null;
    }

    @Override
    public void setProjectContext(IProject project) {
        this.project = project;
    }

}
