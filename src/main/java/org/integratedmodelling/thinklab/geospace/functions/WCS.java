package org.integratedmodelling.thinklab.geospace.functions;

import java.io.File;
import java.util.Map;

import org.integratedmodelling.auth.data.Datarecord;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.data.IDataAsset;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IExpression;
import org.integratedmodelling.thinklab.api.project.IProject;
import org.integratedmodelling.thinklab.cache.CacheUtils;
import org.integratedmodelling.thinklab.common.utils.URLUtils;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.geospace.datasources.WCSGridDataSource;
import org.integratedmodelling.thinklab.interfaces.annotations.Function;

@Function(
        id = "wcs",
        parameterNames = { "service", "id", "no-data", "table", "key" },
        returnTypes = { NS.DATASOURCE })
public class WCS implements IExpression {

    IProject _project;

    @Override
    public Object eval(Map<String, Object> parameters, IConcept... context) throws ThinklabException {

        String id = null;
        String service = null;
        String table = null;
        String key = null;

        if (parameters.containsKey("urn")) {

            if (Thinklab.get().getLockingUser() == null || Thinklab.get().getLockingUser().isAnonymous()) {
                return null;
            }

            Datarecord dr = CacheUtils.getDatarecord(parameters.get("urn").toString(), Thinklab.get()
                    .getLockingUser());
            if (dr != null) {
                service = dr.getAttribute(IDataAsset.KEY_URL);
                id = dr.getAttribute(IDataAsset.KEY_DATAID);
            }

        } else {

            Object serv = parameters.get("service");
            if (serv instanceof String) {
                service = serv.toString();
            } else if (serv instanceof String[]) {
                String[] ss = (String[]) serv;
                for (String s : ss) {
                    if (URLUtils.ping(s)) {
                        service = s;
                        break;
                    }
                }
            }
            id = parameters.get("id").toString();
        }

        if (service == null || id == null)
            return null;

        double noData = Double.NaN;

        /*
         * TODO support a list of nodata values
         */
        if (parameters.containsKey("no-data")) {
            noData = Double.parseDouble(parameters.get("no-data").toString());
        }

        if (parameters.containsKey("table")) {

            table = parameters.get("table").toString();
            if (!parameters.containsKey("key")) {
                throw new ThinklabValidationException("wcs: must specify 'key' parameter along with 'table'");
            }
            key = parameters.get("key").toString();
        }

        WCSGridDataSource ret = new WCSGridDataSource(service, id, new double[] { noData });

        if (table != null) {

            File input = new File(_project.getLoadPath() + File.separator + table);
            if (!input.exists()) {
                input = new File(table);
            }
            /*
             * TODO use TableFactory in commons package
             */
        }

        return ret;
    }

    @Override
    public void setProjectContext(IProject project) {
        _project = project;
    }

}
