package org.integratedmodelling.thinklab.geospace.functions;

import java.io.File;
import java.util.Map;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IExpression;
import org.integratedmodelling.thinklab.api.project.IProject;
import org.integratedmodelling.thinklab.common.utils.MiscUtilities;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.geospace.datasources.RasterFileDataSource;
import org.integratedmodelling.thinklab.interfaces.annotations.Function;

@Function(id = "raster", parameterNames = { "id", "file", "table", "key" }, returnTypes = { NS.DATASOURCE })
public class RASTER implements IExpression {

    IProject _project;

    @Override
    public Object eval(Map<String, Object> parameters, IConcept... context) throws ThinklabException {

        File input = new File(_project.getLoadPath() + File.separator + parameters.get("file").toString());

        if (!input.exists()) {
            input = new File(parameters.get("file").toString());
        }

        if (input.exists()) {

            String id = parameters.containsKey("id") ? parameters.get("id").toString() : MiscUtilities
                    .getFileBaseName(input.toString());

            return new RasterFileDataSource(id, input.toString());
        }
        return null;
    }

    @Override
    public void setProjectContext(IProject project) {
        _project = project;
    }

}
