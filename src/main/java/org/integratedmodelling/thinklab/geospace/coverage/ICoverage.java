/**
 * Copyright 2011 The ARIES Consortium (http://www.ariesonline.org) and
 * www.integratedmodelling.org. 

   This file is part of Thinklab.

   Thinklab is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published
   by the Free Software Foundation, either version 3 of the License,
   or (at your option) any later version.

   Thinklab is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Thinklab.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.integratedmodelling.thinklab.geospace.coverage;

import java.io.File;

import org.geotools.geometry.jts.ReferencedEnvelope;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.geospace.extents.Area;
import org.opengis.referencing.crs.CoordinateReferenceSystem;

public interface ICoverage {

    /**
     * Return the value associated with the n-th subdivision according to the SubdivisionOrdering associated with
     * the coverage. The value should be what the passed conceptual model wants.
     * 
     * @param subdivisionOrder
     * @return 
     * @throws ThinklabValidationException 
     * @throws ThinklabException 
     */
    public abstract Object getSubdivisionValue(int subdivisionOrder, Area extent)
            throws ThinklabValidationException;

    public abstract String getSourceUrl();

    public abstract String getCoordinateReferenceSystemCode() throws ThinklabException;

    public abstract CoordinateReferenceSystem getCoordinateReferenceSystem();

    public abstract String getLayerName();

    /**
     * Return a coverage that matches the passed raster extent, warping if necessary. Return self if
     * the match is there, a new one otherwise. If the coverage cannot be adjusted, throw an exception.
     * 
     * @param arealExtent the extent that this coverage needs to cover.
     * @param observer the type of observation we're expected to make with this transformation
     * @param _monitor report info and stuff here
     * @param allowClassChange if true, we can generate a coverage of a different class (e.g. a raster
     *        instead of a vector); otherwise we must stay our type, and throw an exception if impossible.
     * @return a new coverage or self. It should never return null.
     */
    public abstract ICoverage requireMatch(Area arealExtent, IObserver observer, IMonitor monitor,
            boolean allowClassChange) throws ThinklabException;

    /**
     * Write the coverage to a suitable GIS format, determined by the extension in the
     * requested file name.
     * 
     * @param f file to write to.
     * @throws ThinklabException
     */
    public void write(File f) throws ThinklabException;

    /**
     * 
     * @return
     */
    public abstract ReferencedEnvelope getEnvelope();

}
