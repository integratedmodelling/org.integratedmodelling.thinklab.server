package org.integratedmodelling.thinklab.geospace.coverage.vector;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.geotools.data.DataStore;
import org.geotools.data.DataStoreFinder;
import org.geotools.data.wfs.WFSDataStoreFactory;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabIOException;

public class WFSCoverage extends AbstractVectorCoverage {

    public static final int TIMEOUT     = 100000;
    public static final int BUFFER_SIZE = 512;

    DataStore               store       = null;

    public WFSCoverage(URL service, String id, String valueField, String filter) throws ThinklabException {
        super(service, id, valueField, filter);
    }

    @Override
    protected DataStore getDataStore() throws ThinklabException {

        if (store == null) {

            Integer wfsTimeout = TIMEOUT;
            Integer wfsBufsize = BUFFER_SIZE;

            // if we don't do this, it will take the first layer in WFS
            coverageId = layerName;

            Map<Object, Object> connectionParameters = new HashMap<Object, Object>();
            connectionParameters.put(WFSDataStoreFactory.URL.key, sourceUrl
                    + "?REQUEST=getCapabilities&VERSION=1.1.0");
            connectionParameters.put(WFSDataStoreFactory.TIMEOUT.key, wfsTimeout);
            connectionParameters.put(WFSDataStoreFactory.BUFFER_SIZE.key, wfsBufsize);

            try {
                store = DataStoreFinder.getDataStore(connectionParameters);
            } catch (IOException e) {
                throw new ThinklabIOException(e);
            }
        }

        return store;
    }

}
