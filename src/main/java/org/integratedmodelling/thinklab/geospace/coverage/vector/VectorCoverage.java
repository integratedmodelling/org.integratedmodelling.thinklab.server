/**
 * Copyright 2011 The ARIES Consortium (http://www.ariesonline.org) and
 * www.integratedmodelling.org. 

   This file is part of Thinklab.

   Thinklab is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published
   by the Free Software Foundation, either version 3 of the License,
   or (at your option) any later version.

   Thinklab is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Thinklab.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.integratedmodelling.thinklab.geospace.coverage.vector;

import java.net.MalformedURLException;
import java.net.URL;

import org.geotools.data.DataStore;
import org.geotools.data.shapefile.ShapefileDataStore;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;

public class VectorCoverage extends AbstractVectorCoverage {

    DataStore store = null;

    public VectorCoverage(URL url, String layerName, String valueField, String filter)
            throws ThinklabException {
        super(url, layerName, valueField, filter);
    }

    //	public VectorCoverage(FeatureCollection<SimpleFeatureType, SimpleFeature> features,
    //			CoordinateReferenceSystem crs) {
    //		
    //		/*
    //		 * ACHTUNG this has no source or datastore, so it may give us trouble later
    //		 */
    //		this.featureCollection = features;
    //		this.crs = crs;
    //		this.envelope = new ReferencedEnvelope(features.getBounds(), crs);
    //		
    //		try {
    //			this.crsCode = Geospace.getCRSIdentifier(crs, false);
    //		} catch (ThinklabException e) {
    //			throw new ThinklabRuntimeException(e);
    //		}
    //	}

    @Override
    protected DataStore getDataStore() throws ThinklabException {

        if (store == null) {
            try {
                store = new ShapefileDataStore(new URL(sourceUrl));
            } catch (MalformedURLException e) {
                throw new ThinklabValidationException(e);
            }
        }

        return store;
    }
}
