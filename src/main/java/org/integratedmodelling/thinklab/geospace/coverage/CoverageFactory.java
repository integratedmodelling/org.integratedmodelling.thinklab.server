/**
 * Copyright 2011 The ARIES Consortium (http://www.ariesonline.org) and
 * www.integratedmodelling.org. 

   This file is part of Thinklab.

   Thinklab is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published
   by the Free Software Foundation, either version 3 of the License,
   or (at your option) any later version.

   Thinklab is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Thinklab.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.integratedmodelling.thinklab.geospace.coverage;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import org.geotools.coverage.GridSampleDimension;
import org.geotools.coverage.grid.GridCoverage2D;
import org.geotools.gce.arcgrid.ArcGridReader;
import org.geotools.gce.geotiff.GeoTiffReader;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabIOException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.thinklab.geospace.Geospace;
import org.integratedmodelling.thinklab.geospace.coverage.raster.RasterCoverage;
import org.integratedmodelling.thinklab.geospace.coverage.vector.VectorCoverage;
import org.integratedmodelling.thinklab.geospace.coverage.vector.WFSCoverage;
import org.integratedmodelling.thinklab.geospace.extents.Grid;

/**
 * Use this to create instances of ICoverage from scratch or from raster or vector files and services.
 * 
 * @author Ferd
 *
 */
public class CoverageFactory {

    final static String[] supportedRasterExtensions = { "tif", "tiff" };

    final static String[] supportedVectorExtensions = { "shp" };

    public static final String CRS_PROPERTY = "crs";
    public static final String FIELD_NAMES_PROPERTY = "field.names";
    public static final String PROTOTYPE_PROPERTY_PREFIX = "field.prototype";
    public static final String GEOMETRY_TYPE_PROPERTY = "field.names";

    /**
     * Read the source and set properties, but do not render any image or waste any more memory
     * than necessary at this stage. Load the data using loadImage, possibly after setting different
     * crop, projection and no-data values.
     * 
     * @param url
     * @param properties
     * @throws ThinklabException
     */
    public synchronized static ICoverage readRaster(String url, Properties properties)
            throws ThinklabException {

        GridCoverage2D coverage = null;

        if (url.endsWith(".tif") || url.endsWith(".tiff")) {

            try {

                GeoTiffReader reader = new GeoTiffReader(url, Geospace.get().getGeotoolsHints());

                coverage = (GridCoverage2D) reader.read(null);

            } catch (Exception e) {
                throw new ThinklabValidationException(e);
            }

        } else if (url.toString().endsWith(".adf")) {

            try {

                ArcGridReader reader = new ArcGridReader(url, null);
                coverage = (GridCoverage2D) reader.read(null);

            } catch (Exception e) {
                throw new ThinklabValidationException(e);
            }

        }

        if (coverage == null) {
            throw new ThinklabIOException("read error loading coverage from " + url);
        }

        /*
         * TODO enable handling of multi-band coverages WITHIN RasterCoverage. Could be used for distributions, too.
         */
        GridSampleDimension[] sdims = coverage.getSampleDimensions();
        return new RasterCoverage(url.toString(), coverage, sdims[0], sdims.length == 1);
    }

    public static boolean supportsFormat(String ext) {

        return Arrays.binarySearch(supportedRasterExtensions, ext) >= 0
                || Arrays.binarySearch(supportedVectorExtensions, ext) >= 0;
    }

    public static ICoverage makeCoverage(Grid ext, Map<Collection<Integer>, Double> data)
            throws ThinklabException {

        double[] dataset = new double[ext.getYCells() * ext.getXCells()];

        for (Collection<Integer> o : data.keySet()) {

            Iterator<Integer> it = o.iterator();

            int y = it.next();
            int x = it.next();
            double d = data.get(o);

            dataset[(y * ext.getXCells()) + x] = d;
        }

        RasterCoverage ret = new RasterCoverage("", ext, dataset);

        return ret;
    }

    /**
     * Read a WFS unless the URL is a file
     * 
     * @param url
     * @param _id
     * @param attr
     * @param filter
     * @return
     * @throws ThinklabException 
     */
    public static ICoverage readVector(URL url, String id, String attr, String filter)
            throws ThinklabException {

        ICoverage ret = null;
        if (url.getProtocol().startsWith("file")) {
            ret = new VectorCoverage(url, id, attr, filter);
        } else {
            ret = new WFSCoverage(url, id, attr, filter);
        }

        return ret;
    }

    /**
     * Read a vector coverage from a file
     * 
     * @param url
     * @param _id
     * @param attr
     * @param filter
     * @return
     * @throws ThinklabException 
     * @throws MalformedURLException 
     */
    public static ICoverage readVector(File file, String id, String attr, String filter)
            throws ThinklabException {
        try {
            return new VectorCoverage(file.toURI().toURL(), id, attr, filter);
        } catch (MalformedURLException e) {
            throw new ThinklabValidationException(e);
        }
    }

}
