package org.integratedmodelling.thinklab.geospace.coverage.raster;

import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.thinklab.geospace.extents.Grid;
import org.integratedmodelling.thinklab.geospace.interfaces.IGridMask;

public class RectangleMask implements IGridMask {

    @Override
    public void intersect(IGridMask other) throws ThinklabValidationException {
        // TODO Auto-generated method stub

    }

    @Override
    public void or(IGridMask other) throws ThinklabValidationException {
        // TODO Auto-generated method stub

    }

    @Override
    public Pair<Integer, Integer> getCell(int index) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isActive(int linearIndex) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isActive(int x, int y) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void activate(int x, int y) {
        // TODO Auto-generated method stub

    }

    @Override
    public void deactivate(int x, int y) {
        // TODO Auto-generated method stub

    }

    @Override
    public int totalActiveCells() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int nextActiveOffset(int fromOffset) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int[] nextActiveCell(int fromX, int fromY) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Pair<Integer, Integer> nextActiveCell(int fromOffset) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Grid getGrid() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void invert() {
        // TODO Auto-generated method stub

    }

    @Override
    public void deactivate() {
        // TODO Auto-generated method stub

    }

    @Override
    public void activate() {
        // TODO Auto-generated method stub

    }

}
