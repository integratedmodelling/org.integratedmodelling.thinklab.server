package org.integratedmodelling.thinklab.debug.commands;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.runtime.ISession;
import org.integratedmodelling.thinklab.command.Command;
import org.integratedmodelling.thinklab.debug.CallTracer;
import org.integratedmodelling.thinklab.interfaces.annotations.ThinklabCommand;
import org.integratedmodelling.thinklab.interfaces.commands.ICommandHandler;

/**
 * Trace tools. Subcommands:
 * 
 * calls - use CallTracer to document resolution.
 * queries - quick summary of recent model queries and their results (use 'q' command for details)
 * 
 * @author Ferd
 *
 */
@ThinklabCommand(
        name = "trace",
        optionalArgumentNames = "what",
        optionalArgumentDefaultValues = "_NONE_",
        optionalArgumentTypes = "thinklab:Text",
        optionalArgumentDescriptions = "what to trace")
public class Trace implements ICommandHandler {

    @Override
    public Object execute(Command command, ISession session)
            throws ThinklabException {

        String z = command.getArgumentAsString("what");

        if (z.equals("calls")) {

            CallTracer.dump(session.getUserModel().getOutputStream());

        } else if (z.equals("queries")) {

            int i = 0;
            // for (Object o : DataRecorder.get().list(ModelQuery.DEBUG_ID)) {
            //
            // @SuppressWarnings("unchecked")
            // Triple<IQuery, KBoxResult, String> qr = (Triple<IQuery, KBoxResult, String>) o;
            // session.println(
            // i++ + ": " +
            // qr.getThird() + " -> " + qr.getSecond()
            // );
            // }
        }

        return null;
    }

}
