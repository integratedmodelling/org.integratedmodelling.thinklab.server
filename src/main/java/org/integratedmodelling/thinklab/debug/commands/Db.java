package org.integratedmodelling.thinklab.debug.commands;

import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.knowledge.kbox.IKbox;
import org.integratedmodelling.thinklab.api.runtime.ISession;
import org.integratedmodelling.thinklab.command.Command;
import org.integratedmodelling.thinklab.debug.DataRecorder;
import org.integratedmodelling.thinklab.interfaces.annotations.ThinklabCommand;
import org.integratedmodelling.thinklab.interfaces.commands.ICommandHandler;
import org.integratedmodelling.thinklab.kbox.neo4j.NeoKBox;
import org.integratedmodelling.thinklab.query.Queries;

/**
 * Database inspector. Subcommands:
 * 
 * stored  - list all objects stored in this session and their ID.
 * rm <id> - remove object with given ID.
 * find <type> - list all objects of given type.
 * 
 * @author Ferd
 *
 */
@ThinklabCommand(
        name = "db",
        optionalArgumentNames = "arg1,arg2,arg3",
        optionalArgumentDefaultValues = "_NONE_,_NONE_,_NONE_",
        optionalArgumentTypes = "thinklab:Text,thinklab:Text,thinklab:Text",
        optionalArgumentDescriptions = "arg1,arg2,arg3")
public class Db implements ICommandHandler {

    @SuppressWarnings("unchecked")
    @Override
    public Object execute(Command command, ISession session) throws ThinklabException {

        String z = command.getArgumentAsString("arg1");

        if (z.equals("stored")) {

            /*
             * list all objects stored in this session
             */
            int i = 0;
            for (Object o : DataRecorder.get().list(NeoKBox.STORAGE_DEBUG_ID)) {

                Pair<Long, Object> qr = (Pair<Long, Object>) o;
                session.println("[" + qr.getFirst() + "] " + qr.getSecond());

            }
        } else if (z.equals("rm")) {

            int n = Integer.parseInt(command.getArgumentAsString("arg2"));

            IKbox kbox = Thinklab.get().requireKbox("thinklab");
            kbox.remove(n);

        } else if (z.equals("find")) {

            String c = command.getArgumentAsString("arg2");
            IKbox kbox = Thinklab.get().requireKbox("thinklab");
            session.println(kbox.query(Queries.select(Thinklab.c(c))).toString());

        }

        return null;
    }

}
