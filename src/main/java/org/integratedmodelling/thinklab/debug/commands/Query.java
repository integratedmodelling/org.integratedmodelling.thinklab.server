package org.integratedmodelling.thinklab.debug.commands;

import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.runtime.ISession;
import org.integratedmodelling.thinklab.command.Command;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.interfaces.annotations.ThinklabCommand;
import org.integratedmodelling.thinklab.interfaces.commands.ICommandHandler;

@ThinklabCommand(
        name = "q",
        optionalArgumentNames = "arg1,arg2,arg3",
        optionalArgumentDefaultValues = "_NONE_,_NONE_,_NONE_",
        optionalArgumentTypes = "thinklab:Text,thinklab:Text,thinklab:Text",
        optionalArgumentDescriptions = "arg1,arg2,arg3")
public class Query implements ICommandHandler {

    @SuppressWarnings("unchecked")
    @Override
    public Object execute(Command command, ISession session) throws ThinklabException {

        String z = command.getArgumentAsString("arg1");

        if (z.equals("_NONE_")) {

            // int i = 0;
            // for (Object o : DataRecorder.get().list(ModelQuery.DEBUG_ID)) {
            //
            // Triple<IQuery, KBoxResult, String> qr = (Triple<IQuery, KBoxResult, String>) o;
            // session.println("Query " + i++ + ": " + qr.getFirst().asList().prettyPrint() + "\n"
            // + qr.getThird() + " -> " + qr.getSecond());
            // }
        } else if (z.equals("clear")) {

            // DataRecorder.get().clear(ModelQuery.DEBUG_ID);

        } else if (z.equals("trait")) {

            /*
             * default: show trait space of passed concept
             */
            IConcept c = Thinklab.get().getConcept(command.getArgumentAsString("arg2"));

            if (c != null && c.is(Thinklab.c(NS.TRAIT))) {
                session.println("Trait " + c);
                for (IConcept cc : NS.getTraitSpace(c)) {
                    session.println("  " + cc);
                }
            } else {
                String subc = command.getArgumentAsString("arg2");
                if (subc.equals("list")) {
                    c = Thinklab.get().getConcept(command.getArgumentAsString("arg3"));
                    if (c != null && c.is(Thinklab.c(NS.TRAIT))) {
                        session.println("TraitObservable " + c);
                        for (Pair<IConcept, IConcept> cc : NS.getTraits(c)) {
                            session.println("  " + cc);
                        }
                    } // TODO etc
                }
            }
        } else {

            // int n = z.equals("last") ? DataRecorder.get().list(ModelQuery.DEBUG_ID).size() - 1 : Integer
            // .parseInt(z);
            //
            // Triple<IQuery, KBoxResult, String> qr = (Triple<IQuery, KBoxResult, String>) DataRecorder.get()
            // .list(ModelQuery.DEBUG_ID).get(n);
            // IKbox kbox = Thinklab.get().requireKbox("thinklab");
            // session.println("? " + qr.getFirst());
            // session.println("> " + kbox.query(qr.getFirst()));
        }
        return null;
    }
}
