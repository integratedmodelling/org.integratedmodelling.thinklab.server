package org.integratedmodelling.thinklab.debug.commands;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.ISemanticObject;
import org.integratedmodelling.thinklab.api.knowledge.kbox.IKbox;
import org.integratedmodelling.thinklab.api.knowledge.query.IQuery;
import org.integratedmodelling.thinklab.api.modelling.IExtent;
import org.integratedmodelling.thinklab.api.modelling.IModel;
import org.integratedmodelling.thinklab.api.modelling.IScale;
import org.integratedmodelling.thinklab.api.runtime.ISession;
import org.integratedmodelling.thinklab.command.Command;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.interfaces.annotations.ThinklabCommand;
import org.integratedmodelling.thinklab.interfaces.commands.ICommandHandler;
import org.integratedmodelling.thinklab.query.Queries;
import org.integratedmodelling.thinklab.query.Query;

@ThinklabCommand(
        name = "r",
        optionalArgumentNames = "arg1,arg2,arg3",
        optionalArgumentDefaultValues = "_NONE_,_NONE_,_NONE_",
        optionalArgumentTypes = "thinklab:Text,thinklab:Text,thinklab:Text",
        optionalArgumentDescriptions = "arg1,arg2,arg3")
public class Retrieve implements ICommandHandler {

    @Override
    public Object execute(Command command, ISession session) throws ThinklabException {

        String arg1 = command.getArgumentAsString("arg1");

        int n = -1;
        try {
            n = Integer.parseInt(arg1);
        } catch (Exception e) {
            // no problem
        }

        IKbox kbox = Thinklab.get().requireKbox("thinklab");

        if (n >= 0) {

            ISemanticObject<?> s = kbox.retrieve(n);

            session.println("sem> " + s.getSemantics().prettyPrint());
            try {
                Thread.sleep(500);
            } catch (InterruptedException e1) {
            }
            session.println("obj> " + s);
            try {
                Thread.sleep(500);
            } catch (InterruptedException e1) {
            }
            if (s instanceof IModel) {

                IScale scale = ((IModel) s).getCoverage();
                for (IExtent e : scale) {
                    session.println("ext> " + e);
                }
            }

        } else {

            /*
             * lookup a model by observable
             */
            IConcept c = Thinklab.c(arg1);
            IQuery q = Queries.select(NS.MODEL).restrict(
                    Thinklab.p(NS.HAS_OBSERVABLE),
                    Queries.select(NS.OBSERVABLE)
                            .restrict(
                                    NS.HAS_OBSERVATION_TYPE,
                                    Queries.select(
                                            NS.isObject(c) ? NS.DIRECT_OBSERVATION : NS.INDIRECT_OBSERVATION)
                                            .setFlags(Query.USE_SEMANTIC_CLOSURE))
                            .restrict(NS.HAS_OBSERVABLE_CONCEPT, Queries.select(arg1)));

            session.println("? " + q);
            session.println("> " + kbox.query(q));

        }
        return null;
    }

}
