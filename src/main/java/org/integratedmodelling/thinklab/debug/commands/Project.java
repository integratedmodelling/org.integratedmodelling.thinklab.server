package org.integratedmodelling.thinklab.debug.commands;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.runtime.ISession;
import org.integratedmodelling.thinklab.command.Command;
import org.integratedmodelling.thinklab.interfaces.annotations.ThinklabCommand;
import org.integratedmodelling.thinklab.interfaces.commands.ICommandHandler;

/**
 * Project inspector. Subcommands:
 * 
 * <nothing> list all projects and status.
 * load <p>|all 
 * unload <p>|all
 * 
 * @author Ferd
 *
 */
@ThinklabCommand(
        name = "p",
        optionalArgumentNames = "arg1,arg2,arg3",
        optionalArgumentDefaultValues = "_NONE_,_NONE_,_NONE_",
        optionalArgumentTypes = "thinklab:Text,thinklab:Text,thinklab:Text",
        optionalArgumentDescriptions = "arg1,arg2,arg3")
public class Project implements ICommandHandler {

    @Override
    public Object execute(Command command, ISession session) throws ThinklabException {

        String z = command.getArgumentAsString("arg1");

        if (z.equals("_NONE_")) {
        } else if (z.equals("load")) {
            String sub = command.getArgumentAsString("arg2");
            if (sub.equals("all")) {
                Thinklab.get().load(false, command.getMonitor());
            } else {

            }
        } else if (z.equals("unload")) {
            String sub = command.getArgumentAsString("arg2");
            if (sub.equals("all")) {
            }
        }

        return null;
    }

}
