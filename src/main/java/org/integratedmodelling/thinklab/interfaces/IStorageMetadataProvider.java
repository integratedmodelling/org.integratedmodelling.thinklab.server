package org.integratedmodelling.thinklab.interfaces;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.lang.IMetadataHolder;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.metadata.IMetadata;

/**
 * Classes implementing this may be requested to provide metadata that
 * will be used for querying. If stored objects are StorageMetadataProviders,
 * addStorageMetadata will be called by the kbox to supplement their
 * metadata.
 * 
 * @author ferdinando.villa
 *
 */
public interface IStorageMetadataProvider extends IMetadataHolder {

    public abstract void addStorageMetadata(IMetadata metadata, IMonitor monitor) throws ThinklabException;

}
