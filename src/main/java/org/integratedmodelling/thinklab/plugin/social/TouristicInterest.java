package org.integratedmodelling.thinklab.plugin.social;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.modelling.lang.BaseStateAccessor;

/**
 * Gets the number of Panoramio photos in the bounding box of each state. Works only in a spatial context and runs a web query
 * at each point.
 * 
 * Needs an API to retrieve the N. of photos. Also, the API has a usage limit of 100k/day, so we need caching and 
 * some subsampling strategy at high resolution.
 * 
 * @author Ferd
 *
 */

public class TouristicInterest extends BaseStateAccessor {

    @Override
    public void process(int stateIndex) throws ThinklabException {
        // TODO Auto-generated method stub

    }

    @Override
    public void setValue(String inputKey, Object value) {
        // TODO Auto-generated method stub

    }

    @Override
    public Object getValue(String outputKey) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void reset() {
        // TODO Auto-generated method stub

    }

}
