package org.integratedmodelling.thinklab.cache;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import javax.imageio.ImageIO;

import org.apache.jcs.JCS;
import org.apache.jcs.access.exception.CacheException;
import org.integratedmodelling.auth.data.Datarecord;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabIOException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.runtime.IUser;
import org.integratedmodelling.thinklab.rest.RESTHelper;

public class CacheUtils {

    static JCS _dataCache;
    static JCS _imageCache;
    static JCS _wcsCache;
    static JCS _wfsCache;

    static JCS getDataCache() throws ThinklabException {

        if (_dataCache == null) {
            try {
                _dataCache = JCS.getInstance("data");
            } catch (CacheException e) {
                throw new ThinklabIOException(e);
            }
        }
        return _dataCache;
    }

    /**
    * Retrieves a BufferedImage from the passed cache; return null if not there.
     * @throws ThinklabIOException 
    *
    * @throws ImageNotFoundException
    * @throws CacheException
    * @throws IOException
    */
    public static BufferedImage getImage(String key, JCS imageCache) throws ThinklabIOException {
        BufferedImage img = null;
        byte[] imageAsBytes = (byte[]) imageCache.get(key);
        if (imageAsBytes != null) {
            // Convert the byte array back to the bufferedimage
            InputStream in = new ByteArrayInputStream(imageAsBytes);
            try {
                img = javax.imageio.ImageIO.read(in);
            } catch (IOException e) {
                throw new ThinklabIOException(e);
            }
        }
        return img;
    }

    /**
    * Save passed image to cache with given key. If it's already there, just return it.
     * @throws ThinklabIOException 
    *
    * @throws ImageNotFoundException
    * @throws CacheException
    * @throws IOException
    */
    public static BufferedImage loadImage(String key, BufferedImage img, JCS imageCache)
            throws ThinklabIOException {
        try {
            if (img != null) {
                // Convert the image to bytes, BufferedImages cannot be put into the cache
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ImageIO.write(img, "png", baos);
                byte[] bytesOut = baos.toByteArray();
                imageCache.put(key, bytesOut);
            }
        } catch (Exception e) {
            throw new ThinklabIOException(e);
        }
        return img;
    }

    /**
     * If data record is in cache, return it; otherwise ask it to primary server and store it.
     * 
     * @param urn
     * @return
     */
    public static Datarecord getDatarecord(String urn, IUser user) {

        JCS dataCache = null;
        try {
            dataCache = getDataCache();
        } catch (ThinklabException e) {
            // continue without cache
        }

        Datarecord ret = null;
        if (dataCache != null) {

            Map<?, ?> map = (Map<?, ?>) dataCache.get(urn);
            if (map != null) {
                ret = new Datarecord(map);
            } else if (user.getServerURL() != null) {
                ret = RESTHelper.getAsset(urn, user, Datarecord.class);
            }

            if (map == null && ret != null) {
                try {
                    dataCache.put(urn, ret.adapt());
                } catch (CacheException e) {
                    Thinklab.get().logger().warn("unexpected error saving " + urn + " to data cache");
                }
            }
        }

        return ret;
    }

}
