/**
 * Copyright 2011 The ARIES Consortium (http://www.ariesonline.org) and
 * www.integratedmodelling.org. 

   This file is part of Thinklab.

   Thinklab is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published
   by the Free Software Foundation, either version 3 of the License,
   or (at your option) any later version.

   Thinklab is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Thinklab.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.integratedmodelling.thinklab;

import java.io.File;
import java.lang.annotation.Annotation;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.jcs.JCS;
import org.integratedmodelling.collections.NumericInterval;
import org.integratedmodelling.collections.OS;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.collections.Triple;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabIOException;
import org.integratedmodelling.exceptions.ThinklabInternalErrorException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.lang.RankingScale;
import org.integratedmodelling.thinklab.api.IThinklab;
import org.integratedmodelling.thinklab.api.annotations.Concept;
import org.integratedmodelling.thinklab.api.annotations.Literal;
import org.integratedmodelling.thinklab.api.annotations.SubjectType;
import org.integratedmodelling.thinklab.api.configuration.IConfiguration;
import org.integratedmodelling.thinklab.api.factories.IKnowledgeFactory;
import org.integratedmodelling.thinklab.api.factories.IModelManager;
import org.integratedmodelling.thinklab.api.factories.IPluginManager;
import org.integratedmodelling.thinklab.api.factories.IProjectManager;
import org.integratedmodelling.thinklab.api.knowledge.IAuthority;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IOntology;
import org.integratedmodelling.thinklab.api.knowledge.IProperty;
import org.integratedmodelling.thinklab.api.knowledge.ISemanticObject;
import org.integratedmodelling.thinklab.api.knowledge.kbox.IKbox;
import org.integratedmodelling.thinklab.api.lang.IList;
import org.integratedmodelling.thinklab.api.lang.IModelResolver;
import org.integratedmodelling.thinklab.api.lang.IPrototype;
import org.integratedmodelling.thinklab.api.lang.IReferenceList;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.listeners.IProjectLifecycleListener;
import org.integratedmodelling.thinklab.api.modelling.IModelObject;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.api.modelling.resolution.IObservationKbox;
import org.integratedmodelling.thinklab.api.plugin.IPluginLifecycleListener;
import org.integratedmodelling.thinklab.api.plugin.IThinklabPlugin;
import org.integratedmodelling.thinklab.api.project.IDependencyGraph;
import org.integratedmodelling.thinklab.api.project.IProject;
import org.integratedmodelling.thinklab.api.runtime.IUser;
import org.integratedmodelling.thinklab.command.CommandDeclaration;
import org.integratedmodelling.thinklab.command.CommandManager;
import org.integratedmodelling.thinklab.common.ThinklabProperties;
import org.integratedmodelling.thinklab.common.classification.Classifier;
import org.integratedmodelling.thinklab.common.configuration.Configuration;
import org.integratedmodelling.thinklab.common.configuration.Env;
import org.integratedmodelling.thinklab.common.project.ProjectManager;
import org.integratedmodelling.thinklab.common.utils.ClassUtils;
import org.integratedmodelling.thinklab.common.utils.ClassUtils.AnnotationVisitor;
import org.integratedmodelling.thinklab.common.utils.Lock;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.interfaces.annotations.Function;
import org.integratedmodelling.thinklab.interfaces.annotations.ListingProvider;
import org.integratedmodelling.thinklab.interfaces.annotations.RESTResourceHandler;
import org.integratedmodelling.thinklab.interfaces.annotations.ThinklabCommand;
import org.integratedmodelling.thinklab.interfaces.commands.ICommandHandler;
import org.integratedmodelling.thinklab.modelling.ModelManager;
import org.integratedmodelling.thinklab.plugin.PluginManager;
import org.integratedmodelling.thinklab.plugin.ThinklabPlugin;
import org.integratedmodelling.thinklab.rest.RESTManager;
import org.integratedmodelling.thinklab.rest.interfaces.IStatelessService;
import org.integratedmodelling.utils.template.MVELTemplate;
import org.restlet.resource.ServerResource;
import org.restlet.service.MetadataService;

/**
 * Thinklab implements all fundamental interfaces in the Thinklab API, serving as a 
 * one-stop access point for the system.
 * 
 * There is only one instance of Thinklab, always accessible using Thinklab.get(). 
 * Use Thinklab.boot() to start Thinklab and Thinklab.shutdown() to stop it.
 * 
 * Thinklab delegates calls to working and properly initialized instances of IKnowledgeManager, 
 * IProjectManager, IConfiguration, IPluginManager and IModelManager. 
 * 
 * @author Ferdinando Villa
 *
 */
public class Thinklab implements IThinklab, IKnowledgeFactory, IConfiguration, IPluginManager,
        IProjectManager, IModelManager {

    public static final String PLUGIN_ID    = "org.integratedmodelling.thinklab.core";

    public static final String DEFAULT_KBOX = "thinklab";

    public static IConcept     DOUBLE;
    public static IConcept     BOOLEAN;
    public static IConcept     TEXT;
    public static IConcept     LONG;
    public static IConcept     INTEGER;
    public static IConcept     FLOAT;
    public static IConcept     NUMBER;
    public static IConcept     THING;
    public static IConcept     NOTHING;

    public static IProperty    CLASSIFICATION_PROPERTY;
    public static IProperty    ABSTRACT_PROPERTY;
    HashMap<String, URL>       resources    = new HashMap<String, URL>();

    private MetadataService    _metadataService;

    static Thinklab            _this        = null;

    protected KnowledgeManager _km;
    protected Configuration    _configuration;
    protected PluginManager    _pluginManager;
    protected ProjectManager   _projectManager;
    protected ModelManager     _modelManager;

    Log                        logger       = LogFactory.getLog(this.getClass());

    protected long             _bootTime;

    Lock                       _lock;

    /*
     * prevent BS warnings from various libs.
     */
    static {
        System.setProperty("com.sun.media.jai.disableMediaLib", "true");
    }

    public Thinklab() throws ThinklabException {

        JCS.setConfigFilename("/org/integratedmodelling/thinklab/cache.ccf");

        _configuration = new Configuration();
        _modelManager = new ModelManager();
        _km = new KnowledgeManager(_modelManager.getResolver(null));
        _pluginManager = new PluginManager();
        _projectManager = new ProjectManager();
    }

    /* (non-Javadoc)
     * @see org.integratedmodelling.thinklab.plugin.IThinklabPlugin#getClassLoader()
     */
    public ClassLoader getClassLoader() {
        return this.getClass().getClassLoader();
    }

    /* (non-Javadoc)
     * @see org.integratedmodelling.thinklab.plugin.IThinklabPlugin#logger()
     */
    public Log logger() {
        return logger;
    }

    protected final void startup() throws ThinklabException {

        Env.setThinklab(this);

        _bootTime = new Date().getTime();

        _km.initialize();

        INTEGER = getConcept(NS.INTEGER);
        FLOAT = getConcept(NS.FLOAT);
        TEXT = getConcept(NS.TEXT);
        LONG = getConcept(NS.LONG);
        DOUBLE = getConcept(NS.DOUBLE);
        NUMBER = getConcept(NS.NUMBER);
        BOOLEAN = getConcept(NS.BOOLEAN);

        CLASSIFICATION_PROPERTY = getProperty(NS.CLASSIFICATION_PROPERTY);
        ABSTRACT_PROPERTY = getProperty(NS.ABSTRACT_PROPERTY);

        /*
         * install known, useful API classes into annotation factory.
         */
        registerAnnotatedClass(Pair.class, getConcept(NS.PAIR));
        registerAnnotatedClass(Triple.class, getConcept(NS.TRIPLE));
        registerAnnotatedClass(NumericInterval.class, getConcept(NS.NUMERIC_INTERVAL));
        registerAnnotatedClass(Classifier.class, getConcept(NS.CLASSIFIER));
        registerAnnotatedClass(RankingScale.class, getConcept(NS.RANKING_SCALE));

        /*
         * TODO use plugin manager for this
         */
        visitAnnotations();

        /*
         * Merge thinklab properties with system ones
         */
        ThinklabProperties.boot(_configuration);

        /*
         * register all plugins
         */
        _pluginManager.registerPluginPath(getWorkspace(SUBSPACE_PLUGINS));
        _pluginManager.boot();

        /*
         * and finally the projects if a startup directory has been mentioned in the
         * properties. This should only happen in node servers and not in modeling
         * servers.
         */
        if (_configuration.getProperties().getProperty(IConfiguration.THINKLAB_PROJECT_DIR_PROPERTY) != null) {
            File deploypath = new File(_configuration.getProperties()
                    .getProperty(IConfiguration.THINKLAB_PROJECT_DIR_PROPERTY));
            _projectManager.registerProjectDirectory(deploypath);
        }

        _lock = new Lock(".lck");

    }

    @Override
    public KnowledgeManager getKnowledgeManager() {
        return _km;
    }

    private void visitAnnotations() throws ThinklabException {

        ClassUtils.visitAnnotations(this.getClass().getPackage().getName(), Literal.class,
                new AnnotationVisitor() {
                    @Override
                    public void visit(Annotation acls, Class<?> target) throws ThinklabException {
                        registerLiteral(target, (Literal) acls);
                    }
                });

        ClassUtils.visitAnnotations(this.getClass().getPackage().getName(), Concept.class,
                new AnnotationVisitor() {
                    @Override
                    public void visit(Annotation acls, Class<?> target) throws ThinklabException {
                        registerAnnotation(target, (Concept) acls);
                    }
                });
        ClassUtils.visitAnnotations(this.getClass().getPackage().getName(), ThinklabCommand.class,
                new AnnotationVisitor() {
                    @Override
                    public void visit(Annotation acls, Class<?> target) throws ThinklabException {
                        registerCommand(target, (ThinklabCommand) acls);
                    }
                });
        ClassUtils.visitAnnotations(this.getClass().getPackage().getName(), RESTResourceHandler.class,
                new AnnotationVisitor() {
                    @Override
                    public void visit(Annotation acls, Class<?> target) throws ThinklabException {
                        registerRESTResource(target, (RESTResourceHandler) acls);
                    }
                });
        ClassUtils.visitAnnotations(this.getClass().getPackage().getName(), ListingProvider.class,
                new AnnotationVisitor() {
                    @Override
                    public void visit(Annotation acls, Class<?> target) throws ThinklabException {
                        registerListingProvider(target, (ListingProvider) acls);
                    }
                });
        ClassUtils.visitAnnotations(this.getClass().getPackage().getName(), Function.class,
                new AnnotationVisitor() {
                    @Override
                    public void visit(Annotation acls, Class<?> target) throws ThinklabException {
                        registerFunction(target, (Function) acls);
                    }
                });
        ClassUtils.visitAnnotations(this.getClass().getPackage().getName(), SubjectType.class,
                new AnnotationVisitor() {
                    @Override
                    public void visit(Annotation acls, Class<?> target) throws ThinklabException {
                        registerSubjectClass(target, (SubjectType) acls);
                    }
                });
    }

    private void registerFunction(Class<?> cls, Function annotation) throws ThinklabException {

        String id = annotation.id();
        String[] parameterNames = annotation.parameterNames();
        String[] returnType = annotation.returnTypes();

        try {
            _modelManager.registerFunction(id, parameterNames, cls, returnType);
        } catch (Exception e) {
            throw new ThinklabValidationException(e);
        }
    }

    private void registerListingProvider(Class<?> cls, ListingProvider annotation) throws ThinklabException {

        String name = annotation.label();
        String sname = annotation.itemlabel();
        try {
            CommandManager.get().registerListingProvider(name, sname, cls);
        } catch (Exception e) {
            throw new ThinklabValidationException(e);
        }
    }

    @SuppressWarnings("unchecked")
    private void registerRESTResource(Class<?> cls, RESTResourceHandler annotation) throws ThinklabException {

        String path = annotation.id();
        String description = annotation.description();
        String argument = annotation.arguments();
        String options = annotation.options();
        RESTManager.get().registerService(path, (Class<? extends ServerResource>) cls, description, argument,
                options);
    }

    @SuppressWarnings("unchecked")
    private void registerSubjectClass(Class<?> cls, SubjectType annotation) throws ThinklabException {

        String concept = annotation.value();
        getModelManager().registerSubjectClass(concept, (Class<? extends ISubject>) cls);
    }

    private void registerCommand(Class<?> cls, ThinklabCommand annotation) throws ThinklabException {

        String name = annotation.name();
        String description = annotation.description();

        CommandDeclaration declaration = new CommandDeclaration(name, description, !(IStatelessService.class.isAssignableFrom(cls)));

        String retType = annotation.returnType();

        if (!retType.equals(""))
            declaration.setReturnType(Thinklab.c(retType));

        String[] aNames = annotation.argumentNames().split(",");
        String[] aTypes = annotation.argumentTypes().split(",");
        String[] aDesc = annotation.argumentDescriptions().split(",");

        for (int i = 0; i < aNames.length; i++) {
            if (!aNames[i].isEmpty())
                declaration.addMandatoryArgument(aNames[i], aDesc[i], aTypes[i]);
        }

        String[] oaNames = annotation.optionalArgumentNames().split(",");
        String[] oaTypes = annotation.optionalArgumentTypes().split(",");
        String[] oaDesc = annotation.optionalArgumentDescriptions().split(",");
        String[] oaDefs = annotation.optionalArgumentDefaultValues().split(",");

        for (int i = 0; i < oaNames.length; i++) {
            if (!oaNames[i].isEmpty())
                declaration.addOptionalArgument(oaNames[i], oaDesc[i], oaTypes[i], oaDefs[i]);
        }

        String[] oNames = annotation.optionNames().split(",");
        String[] olNames = annotation.optionLongNames().split(",");
        String[] oaLabel = annotation.optionArgumentLabels().split(",");
        String[] oTypes = annotation.optionTypes().split(",");
        String[] oDesc = annotation.optionDescriptions().split(",");

        for (int i = 0; i < oNames.length; i++) {
            if (!oNames[i].isEmpty())
                declaration.addOption(oNames[i], olNames[i], (oaLabel[i].equals("") ? null : oaLabel[i]),
                        oDesc[i], oTypes[i]);
        }

        try {
            CommandManager.get().registerCommand(declaration, (ICommandHandler) cls.newInstance());
        } catch (Exception e) {
            throw new ThinklabValidationException(e);
        }

    }

    private void registerAnnotation(Class<?> clls, Concept a) throws ThinklabException {
        _km.registerAnnotation(clls, a.value());
    }

    private void registerLiteral(Class<?> clls, Literal a) throws ThinklabException {
        _km.registerLiteralAnnotation(clls, a.concept(), a.datatype(), a.javaClass());
    }

    public ClassLoader swapClassloader() {
        ClassLoader clsl = Thread.currentThread().getContextClassLoader();
        Thread.currentThread().setContextClassLoader(getClassLoader());
        return clsl;
    }

    public void resetClassLoader(ClassLoader clsl) {
        Thread.currentThread().setContextClassLoader(clsl);
    }

    public URL getResourceURL(String resource) throws ThinklabIOException {
        return getResourceURL(resource, null);
    }

    public URL getResourceURL(String resource, ThinklabPlugin plugin) throws ThinklabIOException {

        URL ret = null;

        try {
            File f = new File(resource);

            if (f.exists()) {
                ret = f.toURI().toURL();
            } else if (resource.contains("://")) {
                ret = new URL(resource);
            } else {
                ret = getClassLoader().getResource(resource);
            }
        } catch (MalformedURLException e) {
            throw new ThinklabIOException(e);
        }

        return ret;
    }

    public boolean hasResource(String name) {
        return resources.get(name) != null;
    }

    @Override
    public Version getVersion() {
        return _configuration.getVersion();
    }

    public File getConfigPath() {
        return getWorkspace(SUBSPACE_CONFIG);
    }

    /**
     * Return the only instance of Thinklab, your favourite knowledge manager.
     * 
     * @return
     */
    public static Thinklab get() {
        return _this;
    }

    /**
     * Entry point in Thinklab: call boot() before you do anything. Calling more than
     * once without calling shutdown() has no effect.
     * 
     * @throws ThinklabException
     */
    public static void boot() throws ThinklabException {

        if (_this == null) {
            _this = new Thinklab();
            _this.startup();
        }
    }

    /**
     * You must call shutdown() when you're done to ensure integrity of 
     * data and everything. This said, I always abort applications without
     * getting there and not much happens.
     */
    public static void shutdown() {

        if (_this != null) {

            _this.logger.info("Thinklab shutting down");

            _this._pluginManager.shutdown();
            _this._km.shutdown();

            _this._configuration = null;
            _this._km = null;
            _this._pluginManager = null;
            _this = null;
        }

    }

    /**
     * Quickest way to get a IConcept from a string. Throws an unchecked exception if not present.
     * 
     * @param conceptId
     * @return
     */
    public static IConcept c(String conceptId) {
        IConcept ret = get().getConcept(conceptId);
        if (ret == null) {
            throw new ThinklabRuntimeException("concept " + conceptId + " is unknown");
        }
        return ret;
    }

    /**
     * Quickest way to get a IProeprty from a string. Throws an unchecked exception if not present.
     * 
     * @param propertyId
     * @return
     */
    public static IProperty p(String propertyId) {

        IProperty ret = get().getProperty(propertyId);
        if (ret == null) {
            throw new ThinklabRuntimeException("property " + propertyId + " is unknown");
        }
        return ret;
    }

    @Override
    public IProjectManager getProjectManager() {
        return _projectManager;
    }

    public IPluginManager getPluginManager() {
        return _pluginManager;
    }

    public MetadataService getMetadataService() throws ThinklabException {

        if (this._metadataService == null) {
            this._metadataService = new MetadataService();
            try {
                this._metadataService.start();
            } catch (Exception e) {
                throw new ThinklabInternalErrorException(e);
            }
        }
        return _metadataService;
    }

    public void shutdown(String hook, final int seconds, Map<String, String> params) throws ThinklabException {

        // TODO update to new config
        String home = System.getProperty(Env.THINKLAB_HOME_PROPERTY);

        if (hook != null) {

            if (home == null) {
                throw new ThinklabRuntimeException(
                        "can't use the hook system: thinklab home and/or installation directories not defined");
            }

            File hdest = new File(home + File.separator + "tmp" + File.separator + "hooks");
            File hsour = new File(home + File.separator + "hooks" + File.separator + hook + ".hook");

            if (!hsour.exists()) {
                throw new ThinklabRuntimeException("shutdown hook " + hook + " not installed");
            }

            hdest.mkdirs();
            hdest = new File(hdest + File.separator + hook);

            MVELTemplate tmpl = new MVELTemplate(hsour);
            tmpl.write(hdest, params);
        }

        /*
         * schedule shutdown
         */
        new Thread() {

            @Override
            public void run() {

                int status = 0;
                if (seconds > 0) {
                    try {
                        sleep(seconds * 1000);
                    } catch (InterruptedException e) {
                        status = 255;
                    }
                }

                if (_lock.isLocked()) {
                    _lock.unlock();
                }

                System.exit(status);

            }
        }.start();
    }

    @Override
    public IProperty getProperty(String prop) {
        return _km.getProperty(prop);
    }

    @Override
    public IConcept getConcept(String prop) {
        return _km.getConcept(prop);
    }

    @Override
    public IConcept getLeastGeneralCommonConcept(IConcept... cc) {
        return _km.getLeastGeneralCommonConcept(Arrays.asList(cc));
    }

    @Override
    public IKbox createKbox(String uri) throws ThinklabException {
        return _km.createKbox(uri);
    }

    @Override
    public void dropKbox(String uri) throws ThinklabException {
        _km.dropKbox(uri);
    }

    @Override
    public IObservationKbox requireKbox(String uri) throws ThinklabException {
        return _km.requireKbox(uri);
    }

    @Override
    public ISemanticObject<?> parse(String literal, IConcept concept) throws ThinklabException {
        return _km.parse(literal, concept);
    }

    @Override
    public ISemanticObject<?> annotate(Object object) throws ThinklabException {

        if (object instanceof ISemanticObject)
            return (ISemanticObject<?>) object;

        return _km.annotate(object);
    }

    @Override
    public Object instantiate(IList semantics) throws ThinklabException {
        return _km.instantiate(semantics);
    }

    @Override
    public ISemanticObject<?> entify(IList semantics) throws ThinklabException {
        return _km.entify(semantics);
    }

    public IReferenceList conceptualize(Object object) throws ThinklabException {
        return ((KnowledgeManager) _km).conceptualize(object);
    }

    @Override
    public void registerAnnotatedClass(Class<?> cls, IConcept concept) {
        _km.registerAnnotatedClass(cls, concept);
    }

    public CommandManager getCommandManager() {
        return _km.getCommandManager();
    }

    @Override
    public IModelManager getModelManager() {
        return _modelManager;
    }

    @Override
    public void registerPluginPath(File path) {
        _pluginManager.registerPluginPath(path);
    }

    @Override
    public void addPluginLifecycleListener(IPluginLifecycleListener listener) {
        _pluginManager.addPluginLifecycleListener(listener);
    }

    @Override
    public List<IThinklabPlugin> getPlugins() {
        return _pluginManager.getPlugins();
    }

    @Override
    public File getWorkspace() {
        return _configuration.getWorkspace();
    }

    @Override
    public File getWorkspace(String subspace) {
        return _configuration.getWorkspace(subspace);
    }

    @Override
    public File getScratchArea() {
        return _configuration.getScratchArea();
    }

    @Override
    public File getScratchArea(String subArea) {
        return _configuration.getScratchArea(subArea);
    }

    @Override
    public File getTempArea(String subArea) {
        return _configuration.getTempArea(subArea);
    }

    // @Override
    // public File getLoadPath() {
    // return _configuration.getLoadPath();
    // }
    //
    // @Override
    // public File getLoadPath(String subArea) {
    // return _configuration.getLoadPath(subArea);
    // }

    @Override
    public File getDataPath() {
        return _configuration.getDataPath();
    }

    @Override
    public Properties getProperties() {
        return _configuration.getProperties();
    }

    public boolean isJavaLiteralClass(Class<?> cls) {
        return _km.isJavaLiteralClass(cls);
    }

    public boolean isLiteralConcept(IConcept concept) {
        return _km.isLiteralConcept(concept);
    }

    public File getProjectPath() {
        return getWorkspace("projects");
    }

    @Override
    public IProject getProject(String projectId) {
        return _projectManager.getProject(projectId);
    }

    @Override
    public Collection<IProject> getProjects() {
        return _projectManager.getProjects();
    }

    @Override
    public IProject deployProject(String pluginId, String resourceId, IMonitor monitor)
            throws ThinklabException {
        return _projectManager.deployProject(pluginId, resourceId, monitor);
    }

    @Override
    public void undeployProject(String projectId) throws ThinklabException {
        _projectManager.undeployProject(projectId);
    }

    @Override
    public void registerProjectDirectory(File projectDirectory) throws ThinklabException {
        _projectManager.registerProjectDirectory(projectDirectory);
    }

    public ISemanticObject<?> getSemanticObject(IReferenceList list, Object object) {
        return _km.getSemanticObject(list, object);
    }

    public long getBootTime() {
        return _bootTime;
    }

    public IConcept getLiteralConceptForJavaClass(Class<? extends Object> class1) {
        return _km.getLiteralConceptForJavaClass(class1);
    }

    public ISemanticObject<?> getSemanticLiteral(IReferenceList semantics) {
        return _km.getSemanticLiteral(semantics);
    }

    @Override
    public IConcept getXSDMapping(String string) {
        return _km.getXSDMapping(string);
    }

    @Override
    public INamespace getNamespace(String ns) {
        INamespace ret = _modelManager.getNamespace(ns);
        if (ret == null) {
            ret = _km.getCoreNamespace(ns);
        }
        return ret;
    }

    @Override
    public void releaseNamespace(String namespace) {
        _modelManager.releaseNamespace(namespace);
    }

    @Override
    public Collection<INamespace> getNamespaces() {
        return _modelManager.getNamespaces();
    }

    @Override
    public List<String> registerProject(File... projectDir) throws ThinklabException {
        return _projectManager.registerProject(projectDir);
    }

    @Override
    public void unregisterProject(String projectId) {
        _projectManager.unregisterProject(projectId);
    }

    @Override
    public void unloadProject(String projectId) throws ThinklabException {
        _projectManager.unloadProject(projectId);
    }

    @Override
    public IOntology refreshOntology(URL url, String name) throws ThinklabException {
        return _km.refreshOntology(url, name);
    }

    @Override
    public void releaseOntology(String s) {
        _km.releaseOntology(s);
    }

    @Override
    public void releaseAllOntologies() {
        _km.releaseAllOntologies();
    }

    @Override
    public IOntology getOntology(String ontName) {
        return _km.getOntology(ontName);
    }

    @Override
    public Collection<IOntology> getOntologies(boolean includeInternal) {
        return _km.getOntologies(includeInternal);
    }

    @Override
    public IOntology createOntology(String id, String ontologyPrefix) throws ThinklabException {
        return _km.createOntology(id, ontologyPrefix);
    }

    @Override
    public Collection<IConcept> getRootConcepts() {
        return _km.getRootConcepts();
    }

    @Override
    public Collection<IConcept> getConcepts() {
        return _km.getConcepts();
    }

    @Override
    public File exportOntology(String ontologyId) throws ThinklabException {
        return _km.exportOntology(ontologyId);
    }

    @Override
    public IConcept getRootConcept() {
        return _km.getRootConcept();
    }

    @Override
    public void addListener(IProjectLifecycleListener listener) {
        _projectManager.addListener(listener);
    }

    @Override
    public IConfiguration getConfiguration() {
        return _configuration;
    }

    @Override
    public void warn(String string) {
        logger().warn(string);
    }

    @Override
    public void error(String string) {
        logger.error(string);
    }

    @Override
    public void info(String string) {
        logger.info(string);
    }

    @Override
    public File getProjectDirectory(String id) {
        return _configuration.getProjectDirectory(id);
    }

    @Override
    public File getProjectDirectory() {
        return _configuration.getProjectDirectory();
    }

    @Override
    public void persistProperties() throws ThinklabException {
        _configuration.persistProperties();
    }

    @Override
    public OS getOS() {
        return _configuration.getOS();
    }

    @Override
    public INamespace getCoreNamespace(String ns) {
        return _km.getCoreNamespace(ns);
    }

    @Override
    public IModelResolver getRootResolver() {
        return _modelManager.getRootResolver();
    }

    @Override
    public boolean isModelFile(File f) {
        return _modelManager.isModelFile(f);
    }

    @Override
    public List<INamespace> load(boolean forceReload, IMonitor monitor) throws ThinklabException {
        return _projectManager.load(forceReload, monitor);
    }

    @Override
    public List<INamespace> loadProject(String projectId, IMonitor monitor) throws ThinklabException {
        return _projectManager.loadProject(projectId, monitor);
    }

    @Override
    public List<INamespace> getScenarios() {
        return _modelManager.getScenarios();
    }

    @Override
    public IPrototype getFunctionPrototype(String id) {
        return _modelManager.getFunctionPrototype(id);
    }

    @Override
    public int getNotificationLevel() {
        return _configuration.getNotificationLevel();
    }

    /**
     * Utility: find anything with this name within the set of objects that can be
     * modeled or observed. That means concepts, subject generators and models.
     * 
     * @param observableName
     * @return
     */
    public Object getObjectByID(String observableName) {

        Object ret = null;
        if ((ret = getConcept(observableName)) != null)
            return ret;
        return _modelManager.findModelObject(observableName);
    }

    @Override
    public IModelObject findModelObject(String name) {
        return _modelManager.findModelObject(name);
    }

    @Override
    public INamespace getObservationNamespace() {
        return _modelManager.getObservationNamespace();
    }

    @Override
    public IDependencyGraph getDependencyGraph() {
        return _projectManager.getDependencyGraph();
    }

    @Override
    public INamespace loadFile(File resourceId, String namespaceId, IModelResolver resolver,
            boolean substituteExisting, IMonitor monitor, Set<String> context) throws ThinklabException {
        return _modelManager
                .loadFile(resourceId, namespaceId, resolver, substituteExisting, monitor, context);
    }

    @Override
    public void registerSubjectClass(String concept, Class<? extends ISubject> cls) {
        _modelManager.registerSubjectClass(concept, cls);
    }

    @Override
    public Class<? extends ISubject> getSubjectClass(IConcept type) {
        return _modelManager.getSubjectClass(type);
    }

    @Override
    public boolean hasBeenLoaded() {
        return _projectManager.hasBeenLoaded();
    }

    @Override
    public boolean isLocked() {
        return _lock.isLocked();
    }

    public Lock getLock() {
        return _lock;
    }

    @Override
    public IAuthority getAuthority(String id) {
        return _km.getAuthority(id);
    }

    @Override
    public IOntology requireOntology(String id) {
        return _km.requireOntology(id);
    }

    @Override
    public IUser getLockingUser() {
        return _lock.getLockingUser();
    }

    @Override
    public boolean lock(IUser user, boolean force) {
        return _lock.lock(user, force);
    }

    @Override
    public boolean unlock() {
        return _lock.unlock();
    }
}
