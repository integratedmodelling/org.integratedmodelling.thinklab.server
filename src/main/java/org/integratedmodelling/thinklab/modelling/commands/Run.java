package org.integratedmodelling.thinklab.modelling.commands;

import groovy.lang.Binding;
import groovy.lang.GroovyShell;

import java.io.File;

import org.codehaus.groovy.control.CompilerConfiguration;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabResourceNotFoundException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.project.IProject;
import org.integratedmodelling.thinklab.api.runtime.ISession;
import org.integratedmodelling.thinklab.command.Command;
import org.integratedmodelling.thinklab.interfaces.annotations.ThinklabCommand;
import org.integratedmodelling.thinklab.interfaces.commands.ICommandHandler;

@ThinklabCommand(
        name = "run",
        argumentNames = "script,project",
        argumentTypes = "thinklab:Text,thinklab:Text",
        argumentDescriptions = "base name of the script to run,name of project providing the script" // ,
        //		optionDescriptions="context,scenario(s),use previous context",
        //		optionLongNames="context,scenario,additive",
        //		optionNames="c,s,a",
        //		optionArgumentLabels="context,scenario,additive",
        //		optionTypes="thinklab:Text,thinklab:Text,owl:Nothing"
)
public class Run implements ICommandHandler {

    @Override
    public Object execute(Command command, ISession session) throws ThinklabException {

        IProject project = Thinklab.get().getProject(command.getArgumentAsString("project"));
        File f = null;
        if (project != null) {
            f = new File(project.getLoadPath() + File.separator + ".scripts" + File.separator
                    + command.getArgumentAsString("script") + ".groovy");
        }

        if (f == null || !f.exists()) {
            throw new ThinklabResourceNotFoundException("script " + command.getArgumentAsString("script")
                    + " in project " + command.getArgumentAsString("project"));
        }

        CompilerConfiguration compiler = new CompilerConfiguration();
        compiler.setScriptBaseClass("org.integratedmodelling.thinklab.script.ThinklabScriptBase");

        Binding binding = new Binding();
        binding.setVariable("_session", session);
        binding.setVariable("_monitor", command.getMonitor());

        GroovyShell shell = new GroovyShell(this.getClass().getClassLoader(), binding, compiler);

        Object ret = null;
        try {
            ret = shell.evaluate(f);
        } catch (Exception e) {
            throw new ThinklabValidationException(e);
        }

        return ret;
    }

}
