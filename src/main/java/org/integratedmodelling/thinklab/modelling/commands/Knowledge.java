package org.integratedmodelling.thinklab.modelling.commands;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.runtime.ISession;
import org.integratedmodelling.thinklab.command.Command;
import org.integratedmodelling.thinklab.interfaces.annotations.ThinklabCommand;
import org.integratedmodelling.thinklab.interfaces.commands.ICommandHandler;

/**
 * Access to the collaborative knowledge development services in Thinklab.
 * 
 * @author ferdinando.villa
 *
 */
@ThinklabCommand(
        name = "knowledge",
        argumentNames = "arg0",
        argumentTypes = "thinklab:Text",
        argumentDescriptions = "subcommand",
        optionDescriptions = "context,scenario(s),use previous context",
        optionLongNames = "context,scenario,additive",
        optionNames = "c,s,a",
        optionArgumentLabels = "context,scenario,additive",
        optionTypes = "thinklab:Text,thinklab:Text,owl:Nothing")
public class Knowledge implements ICommandHandler {

    @Override
    public Object execute(Command command, ISession session) throws ThinklabException {
        return null;
    }

}
