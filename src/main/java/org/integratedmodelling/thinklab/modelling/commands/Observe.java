package org.integratedmodelling.thinklab.modelling.commands;

import java.util.ArrayList;
import java.util.List;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.thinklab.api.runtime.IContext;
import org.integratedmodelling.thinklab.api.runtime.ISession;
import org.integratedmodelling.thinklab.command.Command;
import org.integratedmodelling.thinklab.interfaces.annotations.ThinklabCommand;
import org.integratedmodelling.thinklab.interfaces.commands.ICommandHandler;
import org.integratedmodelling.thinklab.runtime.Session;
import org.integratedmodelling.thinklab.runtime.Task;

@ThinklabCommand(
        name = "observe",
        argumentNames = "observable",
        argumentTypes = "thinklab:Text",
        argumentDescriptions = "what to observe",
        optionDescriptions = "context,scenario(s),reset context to pristine,run asynchronously",
        optionLongNames = "context,scenario,reset,asynchronous",
        optionNames = "c,s,reset,async",
        optionArgumentLabels = "context,scenario,true/false,true/false",
        optionTypes = "thinklab:LongInteger,thinklab:Text,thinklab:Boolean,thinklab:Boolean")
public class Observe implements ICommandHandler {

    static final String PREVIOUS_CONTEXT_ID = "_previous_context";
    static final String PREVIOUS_OBSERVER_ID = "_previous_observer";

    @Override
    public Object execute(Command command, ISession session) throws ThinklabException {

        String observableName = command.getArgumentAsString("observable");
        List<String> scenarios = new ArrayList<String>();
        boolean reset = command.hasOption("reset")
                && Boolean.parseBoolean(command.getOptionAsString("reset"));

        if (command.hasOption("scenario")) {
            for (String s : command.getOptionAsString("scenario").split(",")) {
                scenarios.add(s);
            }
        }

        long context = -1l;
        if (command.hasOption("context")) {
            context = Long.parseLong(command.getOptionAsString("context"));
        }

        Task ret = null;
        if (context < 0) {
            ret = new Task(session, command.getMonitor(), null, observableName, scenarios);
            ret.start();
        } else {
            IContext ctx = ((Session) session).getContext(context);
            if (ctx == null) {
                throw new ThinklabValidationException("context " + context + " does not exist");
            }
            ret = (Task) ctx.resetContext(reset).inScenario(scenarios.toArray(new String[scenarios.size()]))
                    .observeAsynchronous(null, observableName);
        }

        return ret;
    }
}
