package org.integratedmodelling.thinklab.modelling.commands;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.lang.IRemoteSerializable;
import org.integratedmodelling.thinklab.api.metadata.IMetadata;
import org.integratedmodelling.thinklab.api.modelling.IObservation;
import org.integratedmodelling.thinklab.api.modelling.IScale;
import org.integratedmodelling.thinklab.api.modelling.IState;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.api.runtime.IContext;
import org.integratedmodelling.thinklab.api.runtime.ISession;
import org.integratedmodelling.thinklab.command.Command;
import org.integratedmodelling.thinklab.common.utils.NumberUtils;
import org.integratedmodelling.thinklab.common.visualization.ColorMap;
import org.integratedmodelling.thinklab.common.visualization.Histogram;
import org.integratedmodelling.thinklab.common.visualization.Viewport;
import org.integratedmodelling.thinklab.interfaces.annotations.ThinklabCommand;
import org.integratedmodelling.thinklab.interfaces.commands.ICommandHandler;
import org.integratedmodelling.thinklab.modelling.lang.Subject;
import org.integratedmodelling.thinklab.modelling.states.State;
import org.integratedmodelling.thinklab.visualization.VisualizationFactory;

/**
 * Inspection and maintenance of computed or computing contexts. Used by remote GUIs to
 * visualize ongoing results of computations or to retrieve previously serialized
 * contexts.
 * 
 * API:
 *
 *  cmd=load <serialized context id>  // load context from user repository
 *      -> a serialized context descriptor with the ID
 *  cmd=remove ctx=<id>
 *      -> removes the context from the active list, freeing the memory
 *  cmd=persist ctx=<id> remove=<boolean>
 *      -> persists the context and optionally removes it. Returns
 *         the descriptor of the serialized context (TBD including 
 *         permanent ID).
 *  cmd=list
 *      -> two lists of numeric IDs: one tagged by "active"
 *         containing descriptors of "live" contexts in order of 
 *         creation, and one tagged by "inactive" containing bare
 *         descriptions of contexts that have timed out (structure
 *         TBD) and including the location of the context in the
 *         user repository if it was persisted.
 *         Live contexts should also contain their current time to 
 *         expiration.
 *  cmd=reset ctx=<id>
 *      -> reset the context by re-observing the root subject and 
 *         return its structure.
 *  cmd=structure path=<path> ctx=<id>
 *      -> context (sub-contexts and states). Each object is a
 *         map with path, name, type and auxiliary info. TBD. For
 *         states, the descriptor should contain the definition
 *         of the representative observer. For sub-contexts, the
 *         same structure is given, so the output is in fact a 
 *         tree. The root context path is identified by the slash
 *         argument.
 *  cmd=run ctx=<id>
 *      -> start temporal transitions. Immediately return ok/error
 *         status while simulation is ongoing. Do nothing if finished. 
 *  cmd=step ctx=<id>
 *      -> run the next temporal transitions. Immediately return 
 *         ok/error status while simulation is ongoing. Do nothing 
 *         if finished.
 *  cmd=status ctx=<id> [after=<timestamp>]
 *      -> a map containing structure (indexed by "structure"), 
 *         and the current status of the context. Also return a "timestamp"
 *         with the time of last context change. If after is
 *         passed, return a no-op with "ignore=true" unless the
 *         last change in the context happened after the passed
 *         timestamp.
 *  cmd=get-media path=<path> ctx=<id> type=<MIMEtype> index=<index> [attribute=<field> [pN=<parameter value>]*]
 *      -> descriptor for the visualization of the object 
 *         identified by <path> at context <index>. Result will have the MIME
 *         type requested. Optionally the request may specify an attribute of the
 *         object, such as the workflow graph for a task, the legend
 *         or the colormap, and add parameters (starting at p0) to further specialize the target
 *         or the result.
 *  cmd=get-data-summary path=<path> ctx=<id> type=<MIMEtype> [attribute=<field> [pN=<parameter value>]*]
 *      -> descriptor for the statistical summary of the object 
 *         identified by <path> at context <index> including an histogram of the data, data
 *         type, color map, aggregated value and any other information relevant to content. 
 *  cmd=get-data-summary path=<path> ctx=<id> type=<MIMEtype> [attribute=<field> [pN=<parameter value>]*]
 *      -> data content of object 
 *         identified by <path> at context <index> including an histogram of the data and
 *         any other information relevant to content. 
 *  cmd=get-data path=<path> ctx=<id>
 *      -> descriptor for the visualization of the object 
 *         identified by <path>. 
 *  cmd=export ctx=<id> [path=<path> format=<format-id> {file=<file> | remote=<true|false>}]
 *      -> handle to the exported version of the given path, using
 *         the default format unless one is provided. If file is given, persist to
 *         local file. Create a dataset for a subject (or if path is not passed), 
 *         an appropriate media (default or format-id) if a state.
 *  cmd=image path=<path> ctx=<id> x=<columns> y=<rows>
 *      -> an image representing the current values of the state
 *         identified by <path> fitting the passed dimensions.
 *  cmd=history path=<path> ctx=<id>
 *      -> a list of chronological steps describing each observation
 *         made in this context. Each step contains the observed
 *         objects, the provenance and dataflow diagrams, and the
 *         start/end time. TBD logging etc.
 *         
 * @author ferdinando.villa
 *
 */
@ThinklabCommand(
        name = "context",
        argumentNames = "cmd",
        argumentTypes = "thinklab:Text",
        argumentDescriptions = "subcommand",
        optionalArgumentNames = "stateindex,ctx,path,remove,format,x,y,after,viewport,index,type,file,remote",
        optionalArgumentTypes = "thinklab:ShortInteger,thinklab:Text,thinklab:Text,thinklab:Boolean,thinklab:Text,thinklab:ShortInteger,thinklab:ShortInteger,thinklab:LongInteger,thinklab:Text,thinklab:Text,thinklab:Text,thinklab:Text,thinklab:Boolean",
        optionalArgumentDefaultValues = "-1,NONE,NONE,false,NONE,-1,-1,-1,NONE,NONE,NONE,NONE,false",
        optionalArgumentDescriptions = "zio,zio,zio,zio,zio,zio,zio,zio,zio,zio,zio,zio,zio")
public class Context implements ICommandHandler {

    @Override
    public Object execute(Command command, ISession session) throws ThinklabException {

        Object ret = null;
        String cmd = command.getArgumentAsString("cmd");
        org.integratedmodelling.thinklab.runtime.Context context = null;
        int ctx = -1;
        if (command.has("ctx")) {
            ctx = Integer.parseInt(command.getArgumentAsString("ctx"));
            for (IContext c : session.getContexts()) {
                if (((org.integratedmodelling.thinklab.runtime.Context) c).getId() == ctx) {
                    context = (org.integratedmodelling.thinklab.runtime.Context) c;
                    break;
                }
            }
        }
        String path = null;
        if (command.has("path")) {
            path = command.getArgumentAsString("path");
        }

        if (cmd.equals("status")) {

            Map<String, Object> r = new HashMap<String, Object>();
            r.put("structure", getStructure((Subject) context.getSubject(), true));
            r.put("current", context.getCurrentTimeIndex());
            r.put("timestamp", context.getLastChangeTimestamp());
            r.put("id", context.getId());
            r.put("name", context.getName());
            ret = r;

        } else if (cmd.equals("list")) {

        } else if (cmd.equals("load")) {

        } else if (cmd.equals("remove")) {

        } else if (cmd.equals("persist")) {
            if (command.has("file")) {
                context.persist(new File(command.getArgumentAsString("file")), path);
            }
        } else if (cmd.equals("reset")) {

        } else if (cmd.equals("structure")) {

            if (path == null)
                path = "/";
            IObservation subject = context.get(path);
            if (subject instanceof IRemoteSerializable) {
                ret = ((IRemoteSerializable) subject).adapt();
            }

        } else if (cmd.equals("run")) {

            ret = context.run();

        } else if (cmd.equals("step")) {

        } else if (cmd.equals("get-media")) {

            IObservation obs = context.get(path);
            String mType = command.getArgumentAsString("type");
            String attr = command.getArgumentAsString("attribute");
            Viewport viewport = Viewport.fromString(command.getArgumentAsString("viewport"));
            int[] index = NumberUtils.fromString(command.getArgumentAsString("index"));
            if (attr == null) {
                return getMedia(obs, mType, index, viewport);
            }
            /*
             * TODO handle attribute and its parameters.
             */

        } else if (cmd.equals("get-data-summary")) {

            Map<String, Object> r = new HashMap<String, Object>();
            IObservation obs = context.get(path);
            int[] index = NumberUtils.fromString(command.getArgumentAsString("index"));
            if (obs instanceof IState) {
                IScale.Index ind = obs.getScale().getIndex(index);
                Histogram histogram = VisualizationFactory.getHistogram((IState) obs, ind, 10);
                ColorMap colormap = (ColorMap) VisualizationFactory.getColormap((IState) obs, ind);
                if (histogram != null)
                    r.put(IMetadata.STATE_HISTOGRAM, histogram.toString());
                if (colormap != null)
                    r.put(IMetadata.STATE_COLORMAP, colormap.toString());
                ret = r;
            }

        } else if (cmd.equals("get-data")) {

            IObservation obs = context.get(path);
            String mType = command.getArgumentAsString("type");
            String attr = command.getArgumentAsString("attribute");
            Viewport viewport = Viewport.fromString(command.getArgumentAsString("viewport"));
            int[] index = NumberUtils.fromString(command.getArgumentAsString("index"));
            if (attr == null) {
                return getMedia(obs, mType, index, viewport);
            }
            /*
             * TODO handle attribute and its parameters.
             */

        } else if (cmd.equals("get-value")) {

            IObservation obs = context.get(path);
            // String mType = command.getArgumentAsString("type");
            // int[] index = NumberUtils.fromString(command.getArgumentAsString("index"));
            int idx = Integer.parseInt(command.getArgumentAsString("stateindex"));
            if (obs instanceof State) {
                return ((State) obs).describeValue(idx);
            }

            /*
             * TODO handle attribute and its parameters.
             */

        } else if (cmd.equals("export")) {

        } else if (cmd.equals("image")) {

        } else if (cmd.equals("history")) {

        }

        return ret;
    }

    private Object getMedia(IObservation obs, String mType, int[] index, Viewport viewport) {
        return VisualizationFactory.get()
                .getMedia(obs, obs.getScale().getIndex(index), viewport, mType, null);
    }

    /**
     * Return just the id, observable and list of states and subjects for each
     * subject. States are described only as observables. Scale is included
     * optionally so we only ask for it once.
     * 
     * @param s
     * @return
     */
    Map<?, ?> getStructure(Subject s, boolean addScale) {

        Map<Object, Object> ret = new HashMap<Object, Object>();

        ArrayList<Object> states = new ArrayList<Object>();
        for (IState st : s.getStates()) {
            if (st instanceof IRemoteSerializable) {
                states.add(((IRemoteSerializable) st).adapt());
            }
        }

        ArrayList<Object> subjects = new ArrayList<Object>();
        for (ISubject st : s.getSubjects()) {
            subjects.add(getStructure((Subject) st, addScale));
        }

        ret.put("id", s.getName());
        ret.put("observable", ((IRemoteSerializable) (s.getObservable())).adapt());
        ret.put("internal-id", s.getInternalID());
        ret.put("states", states);
        ret.put("subjects", subjects);

        if (addScale) {
            ret.put("scale", ((IRemoteSerializable) (s.getScale())).adapt());
        }

        return ret;
    }

    Object serialize(Object o) {
        if (o instanceof IRemoteSerializable) {
            return ((IRemoteSerializable) o).adapt();
        }
        return null;
    }

}
