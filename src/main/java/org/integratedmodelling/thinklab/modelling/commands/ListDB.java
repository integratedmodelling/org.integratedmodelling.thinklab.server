package org.integratedmodelling.thinklab.modelling.commands;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabIOException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.knowledge.ISemanticObject;
import org.integratedmodelling.thinklab.api.modelling.resolution.IObservationKbox;
import org.integratedmodelling.thinklab.api.runtime.ISession;
import org.integratedmodelling.thinklab.command.Command;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.interfaces.annotations.ThinklabCommand;
import org.integratedmodelling.thinklab.interfaces.commands.ICommandHandler;
import org.integratedmodelling.thinklab.modelling.resolver.ModelData;
import org.integratedmodelling.thinklab.query.Queries;
import org.integratedmodelling.thinklab.rest.FileMedia;
import org.integratedmodelling.thinklab.rest.interfaces.IStatelessService;
import org.restlet.data.MediaType;

@ThinklabCommand(name = "list-db")
public class ListDB implements ICommandHandler, IStatelessService {

    @Override
    public Object execute(Command command, ISession session) throws ThinklabException {

        IObservationKbox kb = Thinklab.get().requireKbox(Thinklab.DEFAULT_KBOX);
        File outfile = null;
        PrintWriter writer = null;

        try {
            outfile = File.createTempFile("listdb", ".csv");
            FileOutputStream out = new FileOutputStream(outfile);
            writer = new PrintWriter(out);
            writer.println("Data,Observable,Observation,Trait,Inherent,Namespace,Model,Space");
            boolean warned = false;

            for (ISemanticObject<?> o : kb.query(Queries.select(Thinklab.c(NS.MODEL_DATA)))) {

                if (o == null) {
                    if (!warned) {
                        command.getMonitor().warn("kbox contains outdated models: please refresh it");
                        warned = true;
                    }
                    continue;
                }

                ModelData md = (ModelData) (o.demote());

                writer.println((md.hasDirectData | md.hasDirectObjects ? "\"YES\"," : "\"NO\",")
                        + "\"" + md.type + "\","
                        + "\"" + checkNull(md.oType) + "\","
                        + "\"" + checkNull(md.tType) + "\","
                        + "\"" + checkNull(md.iType) + "\","
                        + "\"" + checkNull(md.namespaceId) + "\","
                        + "\"" + checkNull(md.id) + "\","
                        + "\"" + checkNull(md.spaceExtent) + "\""
                        );
            }
        } catch (Exception e) {
            throw new ThinklabIOException(e);
        } finally {
            if (writer != null) {
                writer.close();
            }
        }

        return new FileMedia(outfile, MediaType.TEXT_CSV);
    }

    private String checkNull(Object o) {
        return o == null ? "N/A" : o.toString();
    }
}
