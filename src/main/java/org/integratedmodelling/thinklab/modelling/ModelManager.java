package org.integratedmodelling.thinklab.modelling;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.integratedmodelling.collections.NumericInterval;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.collections.Path;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabIOException;
import org.integratedmodelling.exceptions.ThinklabInternalErrorException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.interpreter.ModelGenerator;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.data.ITable;
import org.integratedmodelling.thinklab.api.factories.IModelManager;
import org.integratedmodelling.thinklab.api.knowledge.IAuthority;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IExpression;
import org.integratedmodelling.thinklab.api.knowledge.IOntology;
import org.integratedmodelling.thinklab.api.knowledge.IProperty;
import org.integratedmodelling.thinklab.api.knowledge.ISemanticObject;
import org.integratedmodelling.thinklab.api.knowledge.kbox.IKbox;
import org.integratedmodelling.thinklab.api.lang.IExpressionLanguageAdapter;
import org.integratedmodelling.thinklab.api.lang.IList;
import org.integratedmodelling.thinklab.api.lang.IModelResolver;
import org.integratedmodelling.thinklab.api.lang.IPrototype;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.listeners.IMonitorable;
import org.integratedmodelling.thinklab.api.metadata.IMetadata;
import org.integratedmodelling.thinklab.api.modelling.IAnnotation;
import org.integratedmodelling.thinklab.api.modelling.ICategorizingObserver;
import org.integratedmodelling.thinklab.api.modelling.IClassification;
import org.integratedmodelling.thinklab.api.modelling.IClassifier;
import org.integratedmodelling.thinklab.api.modelling.IClassifyingObserver;
import org.integratedmodelling.thinklab.api.modelling.ICountingObserver;
import org.integratedmodelling.thinklab.api.modelling.IMeasuringObserver;
import org.integratedmodelling.thinklab.api.modelling.IModel;
import org.integratedmodelling.thinklab.api.modelling.IModelObject;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.IPercentageObserver;
import org.integratedmodelling.thinklab.api.modelling.IPresenceObserver;
import org.integratedmodelling.thinklab.api.modelling.IProbabilityObserver;
import org.integratedmodelling.thinklab.api.modelling.IProportionObserver;
import org.integratedmodelling.thinklab.api.modelling.IRankingObserver;
import org.integratedmodelling.thinklab.api.modelling.IRatioObserver;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.api.modelling.ISubjectGenerator;
import org.integratedmodelling.thinklab.api.modelling.IUncertaintyObserver;
import org.integratedmodelling.thinklab.api.modelling.IUnit;
import org.integratedmodelling.thinklab.api.modelling.IValuingObserver;
import org.integratedmodelling.thinklab.api.modelling.parsing.IClassificationDefinition;
import org.integratedmodelling.thinklab.api.modelling.parsing.IConceptDefinition;
import org.integratedmodelling.thinklab.api.modelling.parsing.IFunctionCall;
import org.integratedmodelling.thinklab.api.modelling.parsing.ILanguageDefinition;
import org.integratedmodelling.thinklab.api.modelling.parsing.INamespaceDefinition;
import org.integratedmodelling.thinklab.api.modelling.parsing.IPropertyDefinition;
import org.integratedmodelling.thinklab.api.project.IProject;
import org.integratedmodelling.thinklab.api.runtime.ISession;
import org.integratedmodelling.thinklab.api.runtime.IUserModel;
import org.integratedmodelling.thinklab.common.classification.Classifier;
import org.integratedmodelling.thinklab.common.configuration.Env;
import org.integratedmodelling.thinklab.common.data.LookupTable;
import org.integratedmodelling.thinklab.common.owl.Ontology;
import org.integratedmodelling.thinklab.common.utils.MiscUtilities;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.interfaces.knowledge.datastructures.IntelligentMap;
import org.integratedmodelling.thinklab.modelling.lang.Annotation;
import org.integratedmodelling.thinklab.modelling.lang.Categorization;
import org.integratedmodelling.thinklab.modelling.lang.Classification;
import org.integratedmodelling.thinklab.modelling.lang.ConceptObject;
import org.integratedmodelling.thinklab.modelling.lang.Count;
import org.integratedmodelling.thinklab.modelling.lang.FunctionCall;
import org.integratedmodelling.thinklab.modelling.lang.LookupTableAccessor;
import org.integratedmodelling.thinklab.modelling.lang.Measurement;
import org.integratedmodelling.thinklab.modelling.lang.Metadata;
import org.integratedmodelling.thinklab.modelling.lang.Model;
import org.integratedmodelling.thinklab.modelling.lang.ModelObject;
import org.integratedmodelling.thinklab.modelling.lang.Namespace;
import org.integratedmodelling.thinklab.modelling.lang.Presence;
import org.integratedmodelling.thinklab.modelling.lang.Probability;
import org.integratedmodelling.thinklab.modelling.lang.PropertyObject;
import org.integratedmodelling.thinklab.modelling.lang.Proportion;
import org.integratedmodelling.thinklab.modelling.lang.Ranking;
import org.integratedmodelling.thinklab.modelling.lang.Ratio;
import org.integratedmodelling.thinklab.modelling.lang.Subject;
import org.integratedmodelling.thinklab.modelling.lang.SubjectGenerator;
import org.integratedmodelling.thinklab.modelling.lang.Uncertainty;
import org.integratedmodelling.thinklab.modelling.lang.UnitDefinition;
import org.integratedmodelling.thinklab.modelling.lang.Value;
import org.integratedmodelling.thinklab.modelling.lang.expressions.GroovyLanguageAdapter;
import org.integratedmodelling.thinklab.modelling.resolver.ObservationKbox;
import org.integratedmodelling.thinklab.proxy.ModellingModule;
import org.integratedmodelling.thinklab.query.Queries;

import com.google.inject.Guice;
import com.google.inject.Injector;

/**
 * A model manager that can parse the Thinklab language and build a model map, without actually being capable
 * of running the model objects.
 * 
 * @author Ferd
 * 
 */
public class ModelManager implements IModelManager {

    public static final String                  DEFAULT_EXPRESSION_LANGUAGE = "groovy";
    private static final String                 OBSERVATION_NAMESPACE_ID    = "imobs";
    /*
     * we provide a default namespace for models that come right out of a kbox.
     */
    static INamespace                           _defaultModelNamespace      = null;

    private final Hashtable<String, IModel>     modelsById                  = new Hashtable<String, IModel>();
    private final Hashtable<String, INamespace> namespacesById              = new Hashtable<String, INamespace>();

    IntelligentMap<Class<? extends ISubject>>   _subjectClasses             = new IntelligentMap<Class<? extends ISubject>>();

    /*
     * FIXME type handling just a stub, no optional vs. mandatory distinction, no description.
     */
    class FunctionDescriptor implements IPrototype {
        public FunctionDescriptor(String id, String[] parameterNames, Class<?> cls, String[] returnType) {
            this._id = id;
            this._parameterNames = parameterNames;
            this._class = cls;
            this._returnType = returnType;
        }

        String   _id;
        String[] _returnType;
        String[] _parameterNames;
        Class<?> _class;

        @Override
        public String getId() {
            return _id;
        }

        @Override
        public IConcept[] getReturnTypes() {

            IConcept[] ret = new IConcept[_returnType == null ? 0 : _returnType.length];
            int i = 0;
            if (_returnType != null) {
                for (String t : _returnType) {
                    ret[i++] = Thinklab.c(t);
                }
            }
            return ret;
        }

        @Override
        public List<String> getMandatoryArgumentNames() {
            return Arrays.asList(_parameterNames);
        }

        @Override
        public List<String> getOptionalArgumentNames() {
            return new ArrayList<String>();
        }

        @Override
        public IConcept[] getArgumentTypes(String argumentName) {
            return new IConcept[] { Thinklab.THING };
        }

        @Override
        public String getDescription() {
            return "";
        }

        @Override
        public boolean requiresSession() {
            // TODO Auto-generated method stub
            return false;
        }
    }

    private final HashMap<String, FunctionDescriptor> _functions = new HashMap<String, ModelManager.FunctionDescriptor>();

    /**
     * This one resolves namespace source files across imported plugins and handles errors.
     * @author Ferd
     * 
     */
    public class ModelResolver implements IModelResolver {

        ArrayList<Pair<String, Integer>> infos             = new ArrayList<Pair<String, Integer>>();
        String                           resourceId        = "";
        IProject                         project;
        Namespace                        namespace;
        URL                              resourceUrl;

        /*
         * set in context resolver, passed around to subordinate resolvers.
         */
        IMonitor                         _monitor;

        int                              _storingNamespace = 0;

        // // timestamp of resource we load the NS from. Always a file, so always > 0.
        // long _resourceTimestamp;
        // // timestamp stored in DB record for the NS. It's 0 if the NS was never seen before.
        // long _dbTimestamp = 0l;

        boolean                          _isInteractive    = false;
        IModelObject                     _lastProcessed    = null;

        /*
         * timestamp of kbox-stored namespace, if this is < _timestamp and neither is 0 we need to refresh the
         * kbox with the contents of the namespace.
         */
        private InputStream              _interactiveInput;
        private PrintStream              _interactiveOutput;

        public ModelResolver() {
        }

        public ModelResolver(IProject project) {
            this.project = project;
        }

        /*
         * create a resolver for interactive use.
         */
        public ModelResolver(InputStream input, PrintStream output) {

            _isInteractive = true;
            _interactiveInput = input;
            _interactiveOutput = output;
        }

        @Override
        public boolean onException(Throwable e, int lineNumber) {

            /*
             * add error directly to namespace if ns isn't null and interactive is false.
             */
            if (namespace != null && e instanceof ThinklabException) {
                namespace.addError(0, e.getMessage(), lineNumber);
            }
            Thinklab.get().logger()
                    .error(resourceId + ": " + lineNumber + ": " + MiscUtilities.throwableToString(e));
            if (_interactiveOutput != null) {
                _interactiveOutput.println("error: " + e.getMessage());
            }

            return true;
        }

        @Override
        public boolean isInteractive() {
            return _isInteractive;
        }

        @Override
        public boolean onWarning(String warning, int lineNumber) {

            /*
             * add warning directly to namespace
             */
            if (namespace != null) {
                namespace.addWarning(warning, lineNumber);
            }

            Thinklab.get().logger().warn(resourceId + ": " + lineNumber + ": " + warning);
            if (_isInteractive) {
                _interactiveOutput.println("warning: " + warning);
            }
            return true;
        }

        @Override
        public boolean onInfo(String info, int lineNumber) {
            infos.add(new Pair<String, Integer>(info, lineNumber));
            Thinklab.get().logger().info(resourceId + ": " + lineNumber + ": " + info);
            if (_isInteractive) {
                _interactiveOutput.println("info: " + info);
            }
            return true;
        }

        @Override
        public void onNamespaceDeclared() {

            if (namespace.hasErrors()) {
                return;
            }

            try {
                _storingNamespace = ((ObservationKbox) (Thinklab.get().requireKbox(Thinklab.DEFAULT_KBOX)))
                        .removeNamespaceIfOlder(namespace.getId(), namespace.getTimeStamp());
            } catch (ThinklabException e) {
                onException(e, -1);
            }
        }

        @Override
        public void onNamespaceDefined() {

            /*
             * at this point, this should be moot as we define everything incrementally, but leave it here for
             * any final tasks we may want to implement.
             */
            try {
                namespace.initialize();
            } catch (Exception e) {
                onException(e, 0);
            }

            /*
             * if was stored and not changed, do nothing
             */
            if (_storingNamespace > 0) {

                try {
                    IKbox kbox = Thinklab.get().requireKbox(Thinklab.DEFAULT_KBOX);
                    namespace.setTimeStamp(namespace.getTimeStamp());
                    kbox.store(namespace);
                    _storingNamespace = 0;
                } catch (Exception e) {
                    onException(e, -1);
                }
            }

            Thinklab.get().logger().info("namespace " + namespace.getId() + " created from " + resourceId);

        }

        @Override
        public IConceptDefinition requireLocalConcept(String id, int line, int options) {

            IConcept c = namespace.getOntology().getConcept(id);
            if (c == null) {
                c = ((Ontology) (namespace.getOntology())).createConcept(id, options);
            }
            ConceptObject ret = new ConceptObject(namespace, id);
            ret.setLineNumbers(line, line);
            return ret;
        }

        @Override
        public IConceptDefinition resolveExternalConcept(String id, int line) {

            if (Thinklab.get().getConcept(id) == null) {
                onException(new ThinklabValidationException("concept " + id + " unknown"), line);
                return null;
            }

            ConceptObject co = new ConceptObject();
            co.setId(id);

            /*
             * TODO decide how to handle the import with the namespace
             */
            return co;
        }

        @Override
        public IPropertyDefinition resolveExternalProperty(String id, int line) {

            if (Thinklab.get().getProperty(id) == null) {
                onException(new ThinklabValidationException("concept " + id + " unknown"), line);
            }

            PropertyObject co = new PropertyObject();
            co.setId(id);

            /*
             * TODO decide how to handle the import with the namespace
             */

            return co;
        }

        @Override
        public void onModelObjectDefined(IModelObject ret, IMonitor monitor) {

            if (ret.hasErrors()) {
                return;
            }

            if (ret.getId().equals("watershed")) {
                System.out.println("hi");
            }

            /*
             * actualize all knowledge so that the object is complete and we can create observables as
             * required.
             */
            try {
                namespace.synchronizeKnowledge(this);

                /*
                 * validate, using resolver to report any problems.
                 */
                ((ModelObject<?>) ret).validate(this);

                /*
                 * this one is to allow models to perform top-level initializers when we don't want to
                 * recursively call initialize() on model object members.
                 */
                ((ModelObject<?>) ret).initializeTopLevelObject();

            } catch (Exception e) {
                onException(e, ret.getFirstLineNumber());
            }

            if (!isGeneratedId(ret.getId())) {
                namespace.getSymbolTable().put(ret.getId(), ret);
            }

            /*
             * store all models. Models are storage metadata providers.
             */
            if (!ret.isInactive() && ret instanceof IModel && _storingNamespace > 0) {

                IKbox obox = null;
                try {
                    obox = Thinklab.get().requireKbox(Thinklab.DEFAULT_KBOX);
                } catch (ThinklabException e1) {
                    onException(e1, -1);
                }

                if (obox != null) {
                    boolean store = true;

                    try {
                        if (_storingNamespace == 2) {
                            /*
                             * only store models that are not already in, because they may have changed
                             * their error status due to non-language issues like network access to data.
                             */
                            List<ISemanticObject<?>> res = obox.query(Queries.select(NS.MODEL_DATA).restrict(
                                    NS.HAS_NAME, Queries.is(ret.getName())));
                            store = res.size() == 0;
                        }
                        if (store) {

                            /*
                             * this also stores the attribute and any other inferred models if any 
                             * can be derived (logics is in ObservationKbox)
                             */
                            ((ObservationKbox) obox).store(ret, monitor);
                        }
                    } catch (ThinklabException e) {
                        onException(e, ret.getLastLineNumber());
                    }

                }
            }

            _lastProcessed = ret;
        }

        @Override
        public ILanguageDefinition newLanguageObject(Class<?> cls, IMonitor monitor) {

            ILanguageDefinition ret = null;

            if (cls.equals(INamespace.class)) {
                ret = new Namespace();
            } else if (cls.equals(ICategorizingObserver.class)) {
                ret = new Categorization();
            } else if (cls.equals(IClassifyingObserver.class)) {
                ret = new Classification();
            } else if (cls.equals(IMeasuringObserver.class)) {
                ret = new Measurement();
            } else if (cls.equals(IValuingObserver.class)) {
                ret = new Value();
            } else if (cls.equals(IRankingObserver.class)) {
                ret = new Ranking();
            } else if (cls.equals(IPresenceObserver.class)) {
                ret = new Presence();
            } else if (cls.equals(ICountingObserver.class)) {
                ret = new Count();
            } else if (cls.equals(IUncertaintyObserver.class)) {
                ret = new Uncertainty();
            } else if (cls.equals(IModel.class)) {
                ret = new Model();
            } else if (cls.equals(IConcept.class)) {
                ret = new ConceptObject();
            } else if (cls.equals(IProperty.class)) {
                ret = new PropertyObject();
            } else if (cls.equals(IUnit.class)) {
                ret = new UnitDefinition();
            } else if (cls.equals(IMetadata.class)) {
                ret = new Metadata();
            } else if (cls.equals(IFunctionCall.class)) {
                ret = new FunctionCall();
            } else if (cls.equals(IClassificationDefinition.class)) {
                ret = new org.integratedmodelling.thinklab.common.classification.Classification();
            } else if (cls.equals(ISubjectGenerator.class)) {
                ret = new SubjectGenerator();
            } else if (cls.equals(IAnnotation.class)) {
                ret = new Annotation();
            } else if (cls.equals(ITable.class)) {
                return new LookupTable();
            } else if (cls.equals(IProbabilityObserver.class)) {
                return new Probability();
            } else if (cls.equals(IPercentageObserver.class)) {
                return new Proportion(false);
            } else if (cls.equals(IProportionObserver.class)) {
                return new Proportion(true);
            } else if (cls.equals(IRatioObserver.class)) {
                return new Ratio();
            }

            /*
             * pass the monitor if we have one.
             */
            if (ret instanceof IMonitorable) {
                ((IMonitorable) ret).setMonitor(monitor);
            }

            return ret;
        }

        @Override
        public boolean isGeneratedId(String id) {
            return ModelManager.isGeneratedId(id);
        }

        @Override
        public String generateId(IModelObject o) {
            return ModelManager.generateId(o);
        }

        @Override
        public IModelObject getLastProcessedObject() {
            IModelObject ret = _lastProcessed;
            _lastProcessed = null;
            return ret;
        }

        @Override
        public INamespace getNamespace(String id, int lineNumber) {

            INamespace ns = namespacesById.get(id);

            if (ns == null && this.project != null && this.project.providesNamespace(id)) {
                try {
                    ns = loadFile(new File(this.project.findResourceForNamespace(id).toString()), id, this,
                            false, _monitor, new HashSet<String>());
                } catch (ThinklabException e) {
                    onException(e, lineNumber);
                }
            }

            return ns;
        }

        @Override
        public IModelResolver getNamespaceResolver(String namespace, String resource) {

            /*
             * in interactive use, we allow the namespace to be defined incrementally
             */
            if (namespacesById.get(namespace) != null && !isInteractive()) {
                Thinklab.get().logger().warn("warning: namespace " + namespace + " is being redefined");
                releaseNamespace(namespace);
            }

            /*
             * create namespace
             */
            Namespace ns = (Namespace) (isInteractive() ? namespacesById.get(namespace) : null);
            URL url = null;

            long timestamp = new Date().getTime();

            if (ns == null) {

                try {
                    ns = new Namespace(namespace);
                    ns.setProject(project);
                    ns.setMonitor(_monitor);
                } catch (ThinklabException e1) {
                    onException(e1, 0);
                }
                ns.setResourceUrl(resource);

                /*
                 * resolve the resource ID to an openable URL
                 */
                if (resource != null) {

                    File f = new File(resource);

                    try {
                        if (f.exists() && f.isFile() && f.canRead()) {
                            timestamp = f.lastModified();
                            url = f.toURI().toURL();
                        } else if (resource.contains(":/")) {
                            url = new URL(resource);
                            if (url.toString().startsWith("file:")) {
                                f = new File(url.getFile());
                                timestamp = f.lastModified();
                            }
                        }
                    } catch (Exception e) {
                        onException(e, -1);
                    }
                }

                namespacesById.put(namespace, ns);
            }

            ns.setTimeStamp(timestamp);

            /*
             * create new resolver with same project and new namespace
             */
            ModelResolver ret = new ModelResolver(project);
            ret.namespace = ns;
            ret.resourceId = resource;
            ret.resourceUrl = url;
            ret._interactiveInput = _interactiveInput;
            ret._interactiveOutput = _interactiveOutput;
            ret._isInteractive = _isInteractive;
            ret._storingNamespace = _storingNamespace;

            return ret;
        }

        @Override
        public INamespaceDefinition getNamespace() {
            return namespace;
        }

        @Override
        public InputStream openStream() {

            InputStream ret = null;

            if (resourceUrl == null) {
                onException(
                        new ThinklabInternalErrorException("internal error: namespace " + namespace.getId()
                                + " has no associated resource"), -1);
            }

            try {
                ret = resourceUrl.openStream();
            } catch (IOException e) {
                onException(e, -1);
            }

            return ret;
        }

        @Override
        public Map<String, Object> getSymbolTable() {
            return this.namespace.getSymbolTable();
        }

        @Override
        public IProject getProject() {
            return project;
        }

        @Override
        public boolean validateFunctionCall(IFunctionCall ret) {
            // TODO check function against known prototypes
            return true;
        }

        @Override
        public void defineSymbol(String id, Object value, int lineNumber) {
            namespace.getSymbolTable().put(id, value);
        }

        @Override
        public IConcept getConceptFor(String key) {

            if (key.equals(OBJECT_CONCEPT)) {
                return Thinklab.c(NS.OBJECT);
            } else if (key.equals(ATTRIBUTE_CONCEPT)) {
                return Thinklab.c(NS.ATTRIBUTE_TRAIT);
            } else if (key.equals(PROCESS_CONCEPT)) {
                return Thinklab.c(NS.PROCESS);
            } else if (key.equals(QUALITY_CONCEPT)) {
                return Thinklab.c(NS.QUALITY);
            } else if (key.equals(QUANTITY_CONCEPT)) {
                return Thinklab.c(NS.QUANTITY);
            } else if (key.equals(THING_CONCEPT)) {
                return Thinklab.c(NS.PHYSICAL_OBJECT);
            } else if (key.equals(ENERGY_CONCEPT)) {
                return Thinklab.c(NS.ENERGY);
            } else if (key.equals(ENTROPY_CONCEPT)) {
                return Thinklab.c(NS.ENTROPY);
            } else if (key.equals(LENGTH_CONCEPT)) {
                return Thinklab.c(NS.LENGTH);
            } else if (key.equals(MASS_CONCEPT)) {
                return Thinklab.c(NS.MASS);
            } else if (key.equals(VOLUME_CONCEPT)) {
                return Thinklab.c(NS.VOLUME);
            } else if (key.equals(WEIGHT_CONCEPT)) {
                return Thinklab.c(NS.WEIGHT);
            } else if (key.equals(MONETARY_VALUE_CONCEPT)) {
                return Thinklab.c(NS.MONETARY_VALUE);
            } else if (key.equals(DURATION_CONCEPT)) {
                return Env.KM.getConcept(NS.DURATION);
            } else if (key.equals(PREFERENCE_VALUE_CONCEPT)) {
                return Thinklab.c(NS.PREFERENCE_VALUE);
            } else if (key.equals(ACCELERATION_CONCEPT)) {
                return Thinklab.c(NS.ACCELERATION);
            } else if (key.equals(AREA_CONCEPT)) {
                return Thinklab.c(NS.AREA);
            } else if (key.equals(DENSITY_CONCEPT)) {
                return Thinklab.c(NS.DENSITY);
            } else if (key.equals(ELECTRIC_POTENTIAL_CONCEPT)) {
                return Thinklab.c(NS.ELECTRIC_POTENTIAL);
            } else if (key.equals(CHARGE_CONCEPT)) {
                return Thinklab.c(NS.CHARGE);
            } else if (key.equals(RESISTANCE_CONCEPT)) {
                return Thinklab.c(NS.RESISTANCE);
            } else if (key.equals(RESISTIVITY_CONCEPT)) {
                return Thinklab.c(NS.RESISTIVITY);
            } else if (key.equals(PRESSURE_CONCEPT)) {
                return Thinklab.c(NS.PRESSURE);
            } else if (key.equals(SLOPE_CONCEPT)) {
                return Thinklab.c(NS.SLOPE);
            } else if (key.equals(SPEED_CONCEPT)) {
                return Thinklab.c(NS.SPEED);
            } else if (key.equals(TEMPERATURE_CONCEPT)) {
                return Thinklab.c(NS.TEMPERATURE);
            } else if (key.equals(VISCOSITY_CONCEPT)) {
                return Thinklab.c(NS.VISCOSITY);
            } else if (key.equals(SOCIAL_AGENT_CONCEPT)) {
                return Thinklab.c(NS.SOCIAL_AGENT);
            } else if (key.equals(DELIBERATIVE_AGENT_CONCEPT)) {
                return Thinklab.c(NS.DELIBERATIVE_AGENT);
            } else if (key.equals(REACTIVE_AGENT_CONCEPT)) {
                return Thinklab.c(NS.REACTIVE_AGENT);
            } else if (key.equals(ORGANIZED_AGENT_CONCEPT)) {
                return Thinklab.c(NS.ORGANIZED_AGENT);
            } else if (key.equals(AGENT_CONCEPT)) {
                return Thinklab.c(NS.AGENT);
            } else if (key.equals(EVENT_CONCEPT)) {
                return Thinklab.c(NS.EVENT);
            } else if (key.equals(QUALITY_SPACE_CONCEPT)) {
                return Thinklab.c(NS.QUALITY_SPACE);
            } else if (key.equals(ORDERING_CONCEPT)) {
                return Thinklab.c(NS.ORDERING);
            } else if (key.equals(SUBJECTIVE_SPECIFIER)) {
                return Thinklab.c(NS.SUBJECTIVE_TRAIT);
            } else if (key.equals(IDENTITY_CONCEPT)) {
                return Thinklab.c(NS.IDENTITY_TRAIT);
            } else if (key.equals(REALM_CONCEPT)) {
                return Thinklab.c(NS.REALM_TRAIT);
            }

            return Thinklab.c(key);
        }

        @Override
        public IExpressionLanguageAdapter getLanguageAdapter() {

            if (this.namespace != null) {

                if (this.namespace.getExpressionLanguage().equals("groovy")) {
                    return new GroovyLanguageAdapter(this.namespace);
                }
            }
            return null;
        }

        @Override
        public IModelResolver getProjectResolver(IProject p) {
            ModelResolver ret = new ModelResolver(p);
            ret._interactiveInput = _interactiveInput;
            ret._interactiveOutput = _interactiveOutput;
            ret._isInteractive = _isInteractive;
            return ret;
        }

        @Override
        public INamespace importNamespace(String id, int lineNumber, IMonitor monitor) {
            try {
                return project.findNamespaceForImport(id, this, monitor);
            } catch (ThinklabException e) {
                onException(e, lineNumber);
            }
            return null;
        }

        @Override
        public IClassifier getUniversalClassifier() {
            return Classifier.Universal();
        }

        @Override
        public IClassifier getNumberMatcherClassifier(Number n) {
            return Classifier.NumberMatcher(n);
        }

        @Override
        public IClassifier getRangeMatcherClassifier(NumericInterval interval) {
            return Classifier.RangeMatcher(interval);
        }

        @Override
        public IClassifier getNullMatcherClassifier() {
            return Classifier.NullMatcher();
        }

        @Override
        public IClassifier getMultipleClassifier(IList set) {
            return Classifier.Multiple(set);
        }

        @Override
        public IClassifier getConceptMatcherClassifier(IConcept concept) {
            return Classifier.ConceptMatcher(concept);
        }

        @Override
        public IClassifier getStringMatcherClassifier(String string) {
            return Classifier.StringMatcher(string);
        }

        @Override
        public String validateObjectName(String name, boolean canChange, int lineNumber) {

            if (!namespace.getSymbolTable().containsKey(name)) {
                return name;
            }

            if (canChange) {
                for (int i = 1;; i++) {
                    String nname = name + "-" + i;
                    if (!namespace.getSymbolTable().containsKey(nname)) {
                        name = nname;
                        break;
                    }
                }
            } else {
                onException(new ThinklabValidationException("cannot redefine name " + name), lineNumber);
            }

            return name;
        }

        @Override
        public IConcept getConcept(String name) {
            return Thinklab.c(name);
        }

        @Override
        public IProperty getProperty(String name) {
            return Thinklab.p(name);
        }

        @Override
        public IClassifier getBooleanMatcherClassifier(boolean value) {
            return Classifier.BooleanMatcher(value);
        }

        @Override
        public IModelResolver getContextResolver(IMonitor monitor) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Identity getIdentity(IConcept c) {

            if (NS.isQuality(c))
                return Identity.QUALITY;
            else if (NS.isProcess(c))
                return Identity.PROCESS;
            else if (NS.isObject(c))
                return Identity.THING;

            return null;
        }

        @Override
        public IFunctionCall getTableLookupFunctionCall(ITable lookupTable, List<String> args) {
            return new LookupTableAccessor(lookupTable, args);
        }

        @Override
        public IConceptDefinition addTraits(IConcept concept, ArrayList<IConcept> traits, int lineNumber) {
            IConcept c = concept;
            try {
                c = NS.addTraits(concept, traits);
            } catch (ThinklabValidationException e) {
                onException(e, lineNumber);
            }
            return new ConceptObject((Namespace) namespacesById.get(c.getConceptSpace()), c.getLocalName());
        }

        @Override
        public boolean isParticular(IConcept concept) {
            return NS.isParticular(concept);
        }

        @Override
        public boolean isQuality(IConcept concept) {
            return NS.isQuality(concept);
        }

        @Override
        public boolean isObject(IConcept concept) {
            return NS.isObject(concept);
        }

        @Override
        public boolean isProcess(IConcept concept) {
            return NS.isProcess(concept);
        }

        @Override
        public boolean isObservable(IConcept c) {
            return NS.isObservable(c);
        }

        @Override
        public boolean isTrait(IConcept cc, boolean isBaseTrait) {
            return NS.isTrait(cc, isBaseTrait);
        }

        @Override
        public boolean isClass(IConcept ccl) {
            return NS.isClass(ccl);
        }

        @Override
        public IClassification createTraitClassification(IConcept cc, IConcept concept) {
            return org.integratedmodelling.thinklab.common.classification.Classification.create(cc, concept);
        }

        @Override
        public Collection<IConcept> getTraitSpace(IConcept traitType) {
            return NS.getTraitSpace(traitType);
        }

        @Override
        public IAuthority getAuthority(String id) {
            return Thinklab.get().getAuthority(id);
        }

        @Override
        public void validateObservable(IConceptDefinition observable, ObservableRole role, IObserver observer, int line, IConceptDefinition... context) {
            NS.validateObservable(this, observable, role, observer, line, context);
        }

        @Override
        public boolean isSubjective(IConcept concept) {
            return NS.isSubjective(concept);
        }

    }

    public ModelResolver getResolver(IProject project) {
        return new ModelResolver(project);
    }

    public ModelResolver getInteractiveResolver(InputStream input, PrintStream output) {
        return new ModelResolver(input, output);
    }

    public static boolean isGeneratedId(String id) {
        return id == null ? false : id.endsWith("___");
    }

    public static String generateId(IModelObject o) {
        return UUID.randomUUID().toString() + "___";
    }

    public static INamespace getDefaultModelNamespace() {
        return _defaultModelNamespace;
    }

    public IExpression getExpressionForFunctionCall(IFunctionCall functionCall)
            throws ThinklabInternalErrorException {

        IExpression exp = null;
        FunctionDescriptor fd = _functions.get(functionCall.getId());
        if (fd != null) {
            try {
                exp = (IExpression) fd._class.newInstance();
            } catch (Exception e) {
                throw new ThinklabInternalErrorException(e);
            }
        }
        return exp;
    }

    /*
     * we put all model observable instances here.
     */
    ISession _session = null;

    public ModelManager() {
        if (_defaultModelNamespace == null) {
            _defaultModelNamespace = new Namespace();
            ((Namespace) _defaultModelNamespace).setId("org.integratedmodelling.ks.models");
        }
    }

    @Override
    public void releaseNamespace(String namespace) {

        Namespace ns = (Namespace) getNamespace(namespace);

        ns.releaseKnowledge();

        namespacesById.remove(namespace);

        ArrayList<String> toRemove = new ArrayList<String>();
        for (String s : modelsById.keySet()) {
            if (s.startsWith(namespace + ".")) {
                toRemove.add(s);
            }
        }
        for (String s : toRemove) {
            modelsById.remove(s);
        }
        toRemove.clear();

    }

    @Override
    public Collection<INamespace> getNamespaces() {
        return namespacesById.values();
    }

    @Override
    public INamespace loadFile(File file, String namespaceId, IModelResolver resolver,
            boolean substituteExisting, IMonitor monitor, Set<String> context) throws ThinklabException {

        if (context.contains(namespaceId)) {
            return namespacesById.get(namespaceId);
        }

        context.add(namespaceId);

        INamespace ret = null;

        /*
         * TODO check - this loads it even if there is one already and substituteExisting == false
         */

        if (file.toString().endsWith(".tql")) {

            Thinklab.get().logger().info("loading " + file + " for " + namespaceId);

            IModelResolver res = resolver.getNamespaceResolver(namespaceId, file.toString());

            Injector injector = Guice.createInjector(new ModellingModule());
            ModelGenerator thinkqlParser = injector.getInstance(ModelGenerator.class);
            ret = thinkqlParser.parse(namespaceId, file.toString(), res, monitor);

            ((INamespaceDefinition) ret).setTimeStamp(file.lastModified());
            ((INamespaceDefinition) ret).setLocalFile(file);

            // ret = new NamespaceProxy(namespaceId, file, res, monitor);

            if (namespaceId != null && !namespaceId.equals(ret.getId())) {
                throw new ThinklabValidationException("resource " + file.toString() + " declares namespace: "
                        + ret.getId() + " when " + namespaceId + " was expected");
            }

        } else if (file.toString().endsWith(".owl")) {

            Thinklab.get().logger().info("loading " + file.toString() + " for " + namespaceId);

            File ofile = new File(file.toString());
            try {
                Thinklab.get().refreshOntology(ofile.toURI().toURL(), namespaceId);
            } catch (MalformedURLException e) {
                throw new ThinklabIOException(e);
            }

            IOntology ontology = Thinklab.get().getOntology(namespaceId);
            if (ontology == null) {
                throw new ThinklabIOException("ontology " + namespaceId + " cannot be read");
            }

            ret = new Namespace();
            ((INamespaceDefinition) ret).setId(namespaceId);
            ((INamespaceDefinition) ret).setResourceUrl(file.toString());
            ((INamespaceDefinition) ret).setLocalFile(ofile);
            ((INamespaceDefinition) ret).setTimeStamp(ofile.lastModified());
            ((Namespace) ret).setOntology(ontology);

        }

        // this was moved from inside the .owl block, because now the .tql files need to be manually added as
        // well.
        namespacesById.put(namespaceId, ret);
        return ret;
    }

    @Override
    public INamespace getNamespace(String ns) {
        INamespace ret = namespacesById.get(ns);
        if (ret == null) {

        }
        return ret;
    }

    public void registerFunction(String id, String[] parameterNames, Class<?> cls, String[] returnType) {
        _functions.put(id, new FunctionDescriptor(id, parameterNames, cls, returnType));
    }

    public Collection<IPrototype> getFunctionPrototypes() {

        /*
         * one day I'll understand why it can't just cast the f'ing collection.
         */
        ArrayList<IPrototype> ret = new ArrayList<IPrototype>();
        for (FunctionDescriptor f : _functions.values()) {
            ret.add(f);
        }
        return ret;
    }

    /**
     * TODO modularize eventually (the way the client library does it).
     * @param fileExtension
     * @return
     */
    public boolean canParseExtension(String fileExtension) {
        return fileExtension.equals("tql") || fileExtension.equals("owl");
    }

    /**
     * If the ontology isn't part of a namespace already, make a namespace for it and register it. Used for
     * core ontologies read outside of Thinklab projects.
     * 
     * @param o
     */
    // public void wrapOntology(IOntology o) {
    //
    // if (!namespacesById.containsKey(o.getConceptSpace())) {
    //
    // Namespace ns = new Namespace();
    // ns.setId(o.getConceptSpace());
    // ns.setResourceUrl(((Ontology)o).getResourceUrl());
    // ns.setOntology(o);
    //
    // namespacesById.put(o.getConceptSpace(), ns);
    // }
    // }

    @Override
    public IModelResolver getRootResolver() {
        return new ModelResolver();
    }

    @Override
    public boolean isModelFile(File f) {
        String extension = MiscUtilities.getFileExtension(f.toString());
        return canParseExtension(extension);
    }

    @Override
    public List<INamespace> getScenarios() {

        ArrayList<INamespace> ret = new ArrayList<INamespace>();
        for (INamespace n : getNamespaces()) {
            if (n.isScenario()) {
                ret.add(n);
            }
        }
        return ret;
    }

    @Override
    public IPrototype getFunctionPrototype(String id) {
        return _functions.get(id);
    }

    @Override
    public IModelObject findModelObject(String name) {

        String ns = Path.getLeading(name, '.');
        String id = Path.getLast(name, '.');

        INamespace namespace = getNamespace(ns);
        if (namespace != null) {
            return namespace.getModelObject(id);
        }

        return null;
    }

    static int counter = 1;

    public INamespace getUserNamespace(IUserModel userModel) throws ThinklabException {

        /*
         * FIXME use user data
         */
        String key = "thinklab.user" + counter++;
        if (namespacesById.containsKey(key)) {
            return namespacesById.get(key);
        }

        INamespace ret = new Namespace(key);
        namespacesById.put(key, ret);
        return ret;
    }

    @Override
    public INamespace getObservationNamespace() {
        INamespace ret = getNamespace(OBSERVATION_NAMESPACE_ID);
        if (ret == null) {
            try {
                ret = new Namespace(OBSERVATION_NAMESPACE_ID);
            } catch (ThinklabException e) {
                throw new ThinklabRuntimeException(e);
            }
            namespacesById.put(OBSERVATION_NAMESPACE_ID, ret);
        }
        return ret;
    }

    @Override
    public void registerSubjectClass(String concept, Class<? extends ISubject> cls) {
        _subjectClasses.put(concept, cls);
    }

    @Override
    public Class<? extends ISubject> getSubjectClass(IConcept type) {
        Class<? extends ISubject> ret = _subjectClasses.get(type);
        return (Class<? extends ISubject>) (ret == null ? Subject.class : ret);
    }

    /*
     * called by KM after reading all the internal ontologies into internal namespaces.
     */
    public void addNamespace(INamespace ns) {
        namespacesById.put(ns.getId(), ns);
    }

}
