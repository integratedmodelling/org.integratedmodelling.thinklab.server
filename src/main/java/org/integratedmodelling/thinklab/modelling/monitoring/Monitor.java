package org.integratedmodelling.thinklab.modelling.monitoring;

import java.util.Date;

import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.listeners.INotification;
import org.integratedmodelling.thinklab.api.runtime.ISession;
import org.integratedmodelling.thinklab.common.monitoring.Notification;
import org.integratedmodelling.thinklab.common.utils.MiscUtilities;
import org.integratedmodelling.thinklab.runtime.Session;

/**
 * Helper to simplify handling of multiple listeners and two-way communication with 
 * running tasks.
 * 
 * @author Ferd
 *
 */
public class Monitor implements IMonitor {

    long     _taskId;
    ISession _session;
    boolean  _stop      = false;
    boolean  _hasErrors = false;

    /*
     * TODO set in constructor to user-overriddable default.
     */
    int      _logLevel  = INotification.INFO;

    public Monitor(long taskId, ISession session) {
        _taskId = taskId;
        _session = session;
        if (_session != null) {
            ((Session) _session).registerTaskMonitor(taskId, this);
        }
    }

    @Override
    public long getTaskId() {
        return _taskId;
    }

    static Throwable getRootCause(Throwable t) {
        if (t.getCause() == null)
            return t;
        return getRootCause(t.getCause());
    }

    /*
     * TODO see if we want to float this to the interface.
     */
    public ISession getSession() {
        return _session;
    }

    private String toText(Object o) {

        String s = null;
        if (o instanceof String) {
            s = (String) o;
        } else if (o instanceof Throwable) {
            o = getRootCause((Throwable) o);
            s = ((Throwable) o).getMessage() == null ? MiscUtilities.getExceptionPrintout((Throwable) o)
                    : ((Throwable) o).getMessage();
        } else if (o != null) {
            s = o.toString();
        }
        return s;
    }

    @Override
    public void warn(Object o) {
        if (_logLevel >= INotification.WARNING && _session != null)
            _session.notify(new Notification(_taskId, INotification.WARNING, toText(o), new Date().getTime()));
    }

    @Override
    public void info(Object o, String infoClass) {
        if (_logLevel >= INotification.INFO && _session != null)
            _session.notify(new Notification(_taskId, INotification.INFO, toText(o), new Date().getTime()));
    }

    @Override
    public void error(Object o) {
        if (_logLevel >= INotification.ERROR && _session != null)
            _session.notify(new Notification(_taskId, INotification.ERROR, toText(o), new Date().getTime()));
        _hasErrors = true;
    }

    // monitor for an arbitrary task - FIXME having the task id in the monitor is probably FUBAR design
    public void warn(int task, Object o) {
        if (_logLevel >= INotification.WARNING && _session != null)
            _session.notify(new Notification(task, INotification.WARNING, toText(o), new Date().getTime()));
    }

    public void info(int task, Object o, String infoClass) {
        if (_logLevel >= INotification.INFO && _session != null)
            _session.notify(new Notification(task, INotification.INFO, toText(o), new Date().getTime()));
    }

    public void error(int task, Object o) {
        if (_logLevel >= INotification.ERROR && _session != null)
            _session.notify(new Notification(task, INotification.ERROR, toText(o), new Date().getTime()));
        _hasErrors = true;
    }

    @Override
    public void debug(Object o) {
        if (_logLevel >= INotification.DEBUG)
            _session.notify(new Notification(_taskId, INotification.DEBUG, toText(o), new Date().getTime()));
    }

    @Override
    public void stop(Object o) {
        _stop = true;
    }

    @Override
    public boolean isStopped() {
        return _stop;
    }

    @Override
    public void addProgress(int steps, String description) {
        // TODO Auto-generated method stub

    }

    @Override
    public void send(String message) {

        if (message.equals("stop")) {
            _stop = true;
        } /* TODO etc */
    }

    @Override
    public void setLogLevel(int level) {
        _logLevel = level;
    }

    @Override
    public int getDefaultLogLevel() {
        // TODO tie to session/user preferences
        return INotification.INFO;
    }

    @Override
    public boolean hasErrors() {
        return _hasErrors;
    }

    @Override
    public void defineProgressSteps(int n) {
        // TODO Auto-generated method stub

    }

    /**
     * Send a specially formatted info message that can be turned into a parsed notification
     * at the receiver's end.
     * 
     * FIXME Should go in the API (and send() should be removed) but modifying the API would break 
     * things that would need to be fixed by someone who would respond in six months.
     * 
     * @param messageType
     * @param args
     */
    public void message(String messageType, Object... args) {
        if (_session != null)
            _session.notify(new Notification(_taskId, new Date().getTime(), messageType, args));
    }
}
