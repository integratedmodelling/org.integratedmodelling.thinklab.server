package org.integratedmodelling.thinklab.modelling.monitoring;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.knowledge.IProperty;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.modelling.IModel;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.modelling.lang.SubjectAccessor;

/*
 * simply report statistics to the user using the monitor and final
 * states. Nice to use in "identification" models that do nothing but
 * gather observations.
 */
public class StatisticsAccessor extends SubjectAccessor {

    @Override
    public void notifyExpectedOutput(IObservable observable, IObserver observer, String key) {
    }

    @Override
    public ISubject process(ISubject subject, ISubject context, IProperty property, IMonitor monitor)
            throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void notifyModel(IModel model) {
        // TODO Auto-generated method stub

    }

}
