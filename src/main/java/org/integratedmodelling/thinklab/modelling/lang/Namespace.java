package org.integratedmodelling.thinklab.modelling.lang;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.collections.Triple;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.thinklab.KnowledgeManager;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.annotation.SemanticObject;
import org.integratedmodelling.thinklab.api.annotations.Concept;
import org.integratedmodelling.thinklab.api.annotations.Property;
import org.integratedmodelling.thinklab.api.knowledge.IAxiom;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IOntology;
import org.integratedmodelling.thinklab.api.knowledge.IProperty;
import org.integratedmodelling.thinklab.api.lang.IModelResolver;
import org.integratedmodelling.thinklab.api.lang.IReferenceList;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.listeners.IMonitorable;
import org.integratedmodelling.thinklab.api.metadata.IMetadata;
import org.integratedmodelling.thinklab.api.modelling.IExtent;
import org.integratedmodelling.thinklab.api.modelling.IModelObject;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.api.modelling.IScale;
import org.integratedmodelling.thinklab.api.modelling.parsing.IFunctionCall;
import org.integratedmodelling.thinklab.api.modelling.parsing.IMetadataDefinition;
import org.integratedmodelling.thinklab.api.modelling.parsing.IModelObjectDefinition;
import org.integratedmodelling.thinklab.api.modelling.parsing.INamespaceDefinition;
import org.integratedmodelling.thinklab.api.project.IProject;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.modelling.ModelManager;

@Concept(NS.NAMESPACE)
public class Namespace extends SemanticObject<INamespace> implements INamespaceDefinition, IMonitorable {

    @Property(NS.HAS_TIMESTAMP)
    long                                     _timeStamp;

    @Property(NS.HAS_ID)
    String                                   _id;

    @Property(NS.IS_SCENARIO)
    boolean                                  _isScenario;
    boolean                                  _isPrivate;
    boolean                                  _isInactive;
    boolean                                  _isInternal         = false;

    IMonitor                                 _monitor;

    public ArrayList<IFunctionCall>          _extentFunctions    = new ArrayList<IFunctionCall>();

    ArrayList<String>                        _trainingNamespaces = new ArrayList<String>();
    ArrayList<String>                        _lookupNamespaces   = new ArrayList<String>();
    IMetadata                                _resolutionCriteria = null;

    String                                   _expressionLanguage = ModelManager.DEFAULT_EXPRESSION_LANGUAGE;

    ArrayList<IModelObject>                  _modelObjects       = new ArrayList<IModelObject>();
    ArrayList<IAxiom>                        _axioms             = new ArrayList<IAxiom>();
    ArrayList<String>                        _disjointNamespaces = new ArrayList<String>();
    HashSet<IAxiom>                          _axiomHash          = new HashSet<IAxiom>();
    HashSet<String>                          _imported           = new HashSet<String>();

    HashMap<String, Object>                  _symbolTable        = new HashMap<String, Object>();

    File                                     _localFile;
    Metadata _metadata = new Metadata();

    /*
     * use symbol table filled in by the parser instead
     */
    @Deprecated
    HashMap<String, IModelObject>            _namedObjects       = new HashMap<String, IModelObject>();

    private ArrayList<Pair<String, Integer>> _errors             = new ArrayList<Pair<String, Integer>>();
    private ArrayList<Pair<String, Integer>> _warnings           = new ArrayList<Pair<String, Integer>>();

    IOntology                                _ontology;
    String                                   _resourceUrl;

    IProject                                 _project;

    IScale                                   _coverage           = null;

    // these shouldn't be here, but ok
    int                                      _lastLineNumber     = 0;
    int                                      _firstLineNumber    = 0;

    @Property(NS.HAS_ERRORS)
    boolean                                  _hasErrors;

    private int                              _nextAxiom;

    public Namespace(IReferenceList list) {
        super(list);
    }

    public Namespace() {
    }

    public Namespace(String id) throws ThinklabException {
        setId(id);
        _id = id;
        if (_ontology == null) {

            if (Thinklab.get().getOntology(_id) != null)
                Thinklab.get().releaseOntology(_id);

            KnowledgeManager km = Thinklab.get().getKnowledgeManager();
            _ontology = km.requireOntology(_id, _project == null ? NS.DEFAULT_THINKLAB_ONTOLOGY_PREFIX
                    : _project.getOntologyNamespacePrefix());
        }
        synchronizeKnowledge(null);
    }

    @Override
    public Map<String, Object> getSymbolTable() {
        return _symbolTable;
    }

    /**
     * Exec all axioms accumulated so far to actualize gathered knowledge.
     * @throws ThinklabException 
     */
    @Override
    public void synchronizeKnowledge(IModelResolver resolver) {

        ArrayList<IAxiom> axioms = new ArrayList<IAxiom>();
        for (int i = _nextAxiom; i < _axioms.size(); i++) {
            axioms.add(_axioms.get(i));
        }

        for (String error : _ontology.define(axioms)) {
            if (resolver != null)
                resolver.onException(new ThinklabValidationException(error), 1);
            addError(0, error, 1);
        }

        _nextAxiom = _axioms.size();
    }

    public void initialize() throws ThinklabException {

    }

    @Override
    public List<IModelObject> getModelObjects() {
        return _modelObjects;
    }

    @Override
    public long getTimeStamp() {
        return _timeStamp;
    }

    @Override
    public void setOntology(IOntology ontology) {
        this._ontology = ontology;
    }

    @Override
    public IOntology getOntology() {
        return _ontology;
    }

    @Override
    public String getId() {
        return _id;
    }

    @Override
    public INamespace demote() {
        return this;
    }

    @Override
    public void setId(String id) {
        _id = id;
    }

    @Override
    public void addAxiom(IAxiom axiom) {

        if (_axiomHash.contains(axiom))
            return;
        _axioms.add(axiom);
        _axiomHash.add(axiom);
    }

    @Override
    public void setResourceUrl(String resourceUrl) {
        _resourceUrl = resourceUrl;
    }

    @Override
    public void setTimeStamp(long timestamp) {
        _timeStamp = timestamp;
    }

    @Override
    public void addImportedNamespace(String namespace) {
        _imported.add(namespace);
    }

    @Override
    public void addModelObject(IModelObjectDefinition modelObject) {
        _modelObjects.add((IModelObject) modelObject);
    }

    @Override
    public void setProject(IProject project) {
        _project = project;
    }

    @Override
    public IConcept getConcept(String s) {
        return _ontology == null ? null : _ontology.getConcept(s);
    }

    @Override
    public IProperty getProperty(String s) {
        return _ontology == null ? null : _ontology.getProperty(s);
    }

    @Override
    public IProject getProject() {
        return _project;
    }

    @Override
    public IModelObject getModelObject(String mod) {

        Object o = _symbolTable.get(mod);
        if (o instanceof IModelObject)
            return (IModelObject) o;

        return _namedObjects.get(mod);
    }

    @Override
    public String getResourceUrl() {
        return _resourceUrl;
    }

    @Override
    public int getFirstLineNumber() {
        return _firstLineNumber;
    }

    @Override
    public int getLastLineNumber() {
        return _lastLineNumber;
    }

    @Override
    public void setLineNumbers(int startLine, int endLine) {
        _firstLineNumber = startLine;
        _lastLineNumber = endLine;
    }

    @Override
    public void setExpressionLanguage(String language) {
        _expressionLanguage = language;
    }

    @Override
    public String getExpressionLanguage() {
        return _expressionLanguage;
    }

    public void releaseKnowledge() {
        if (_ontology != null)
            Thinklab.get().releaseOntology(_ontology.getConceptSpace());
        _ontology = null;
    }

    @Override
    public IScale getCoverage() {

        if (_coverage == null) {

            for (IFunctionCall fc : _extentFunctions) {

                Object o = null;
                try {
                    o = fc.call();
                } catch (ThinklabException e) {
                    throw new ThinklabRuntimeException(e);
                }
                if (!(o instanceof IExtent)) {
                    throw new ThinklabRuntimeException("function " + fc.getId()
                            + " in coverage of namespace " + getId() + " does not return a valid extent");
                }

                try {

                    Scale scale = new Scale((IExtent) o);
                    _coverage = _coverage == null ? scale : _coverage.union(scale);

                } catch (ThinklabException e) {
                    throw new ThinklabRuntimeException(e);
                }
            }

            if (_coverage /* still */== null) {
                // signal we have tried and prevent messy conditional code later
                _coverage = new Scale();
            }

        }

        return _coverage;
    }

    @Override
    public boolean isInternal() {
        return _isInternal;
    }

    @Override
    public void setInternal(boolean b) {
        _isInternal = b;
    }

    @Override
    public void addCoveredExtent(IFunctionCall extent, IModelResolver resolver, int lineNumber) {
        _extentFunctions.add(extent);
    }

    @Override
    public void addWarning(String warning, int lineNumber) {
        _warnings.add(new Pair<String, Integer>(warning, lineNumber));
    }

    @Override
    public void addError(int errorCode, String errorMessage, int lineNumber) {
        _hasErrors = true;
        this._errors.add(new Pair<String, Integer>(errorMessage, lineNumber));
    }

    @Override
    public boolean hasErrors() {
        return _hasErrors;
    }

    @Override
    public boolean hasWarnings() {
        return _warnings.size() > 0;
    }

    @Override
    public Collection<Triple<Integer, String, Integer>> getErrors() {

        List<Triple<Integer, String, Integer>> ret = new ArrayList<Triple<Integer, String, Integer>>();
        for (Pair<String, Integer> e : _errors) {
            ret.add(new Triple<Integer, String, Integer>(0, e.getFirst(), e.getSecond()));
        }
        return ret;
    }

    @Override
    public Collection<Pair<String, Integer>> getWarnings() {
        return _warnings;
    }

    @Override
    public List<String> getTrainingNamespaces() {
        return _trainingNamespaces;
    }

    @Override
    public List<String> getLookupNamespaces() {
        return _lookupNamespaces;
    }

    @Override
    public void addLookupNamespace(String tns) {
        _lookupNamespaces.add(tns);
    }

    @Override
    public void addTrainingNamespace(String tns) {
        _trainingNamespaces.add(tns);
    }

    // /**
    // * Only in interactive sessions, the namespace may contain a context being
    // * defined by the user. If there, return it.
    // * @return
    // */
    // public ISubjectObserver getCurrentContext() {
    // return _currentContext;
    // }
    //
    // public void setCurrentContext(ISubjectObserver resolver) {
    // _currentContext = resolver;
    // }
    //
    // public ISubjectGenerator getCurrentContextGenerator() {
    // return _currentContextGenerator;
    // }
    //
    // public void setCurrentContextGenerator(ISubjectGenerator subject) {
    // _currentContextGenerator = subject;
    // }

    @Override
    public boolean isScenario() {
        return _isScenario;
    }

    @Override
    public void setScenario(boolean isScenario) {
        _isScenario = isScenario;
    }

    @Override
    public IMetadata getResolutionCriteria() {
        return _resolutionCriteria;
    }

    @Override
    public void setResolutionCriteria(IMetadata metadata) {
        _resolutionCriteria = metadata;
    }

    @Override
    public Collection<String> getDisjointNamespaces() {
        return _disjointNamespaces;
    }

    @Override
    public void addDisjointNamespace(String dn) {
        _disjointNamespaces.add(dn);
    }

    @Override
    public Collection<INamespace> getImportedNamespaces() {
        ArrayList<INamespace> ret = new ArrayList<INamespace>();
        for (String s : _imported)
            ret.add(Thinklab.get().getNamespace(s));
        return ret;
    }

    @Override
    public void setMonitor(IMonitor monitor) {
        _monitor = monitor;
    }

    @Override
    public IMonitor getMonitor() {
        return _monitor;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Namespace ? ((Namespace) obj).getId().equals(getId()) : false;
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    @Override
    public File getLocalFile() {
        return _localFile;
    }

    @Override
    public void setLocalFile(File file) {
        _localFile = file;
    }

    @Override
    public boolean isPrivate() {
        return _isPrivate;
    }

    @Override
    public void setPrivate(boolean isPrivate) {
        _isPrivate = isPrivate;
    }

    @Override
    public boolean isInactive() {
        return _isInactive;
    }

    @Override
    public void setInactive(boolean isInactive) {
        _isInactive = isInactive;
    }

    public String toString() {
        return "NS[" + _id + (_isInternal ? " (i)" : "") + "]";
    }

    @Override
    public IMetadata getMetadata() {
        return _metadata;
    }

    @Override
    public void setMetadata(IMetadataDefinition md) {
        _metadata.merge(((Metadata)md), false);
    }

    @Override
    public void setDescription(String docstring) {
        _metadata.put(IMetadata.DC_COMMENT, docstring);
    }
}
