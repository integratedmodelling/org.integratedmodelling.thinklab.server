package org.integratedmodelling.thinklab.modelling.lang.agents;

import java.util.Map;

import org.integratedmodelling.exceptions.ThinklabResourceNotFoundException;
import org.integratedmodelling.exceptions.ThinklabUnsupportedOperationException;
import org.integratedmodelling.thinklab.api.knowledge.IProperty;
import org.integratedmodelling.thinklab.api.metadata.IMetadata;
import org.integratedmodelling.thinklab.api.modelling.IObjectState;
import org.integratedmodelling.thinklab.api.modelling.IScale;
import org.integratedmodelling.thinklab.api.modelling.IState;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.modelling.lang.agents.ScaleMediatorMetadata.ScaleMediatorTag;

/**
 * pass-through interpolation - don't do any translation; only expose the original scale.
 * 
 * @author luke
 * 
 */
public class ScaleMediatorNull extends ScaleMediator {

    protected static final IMetadata metadata = new ScaleMediatorMetadata(ScaleMediatorTag.oneDimensional,
            ScaleMediatorTag.twoDimensional, ScaleMediatorTag.threeDimensional,
            ScaleMediatorTag.arbitraryDimensions);

    public ScaleMediatorNull(ISubject subject, ObservationController controller)
            throws ThinklabResourceNotFoundException {
        super(subject, controller);
    }

    @Override
    protected IState getStateForTargetScale(SubjectObservation observation, IProperty property,
            IScale targetScale) throws ThinklabUnsupportedOperationException {
        // has the output scale been generated for this observation yet?
        if (!targetScale.equals(subject.getScale())) {
            throw new ThinklabUnsupportedOperationException("Cannot use " + this.getClass().getSimpleName()
                    + " to do scale mediation (target scale must match subject).");
        }

        Map<IProperty, IObjectState> resultMap = observation.outputStates.get(targetScale);
        if (resultMap == null) {
            generateTargetScale(observation, targetScale);
            resultMap = observation.outputStates.get(targetScale);
        }

        return resultMap.get(property);
    }

    @Override
    protected Map<IProperty, IObjectState> generateTargetScale(SubjectObservation observation,
            IScale targetScale) throws ThinklabUnsupportedOperationException {
        Map<IProperty, IObjectState> result = observation.agentStateNode.getAgentState().getStates();
        observation.outputStates.put(targetScale, result);
        return result;
    }
}
