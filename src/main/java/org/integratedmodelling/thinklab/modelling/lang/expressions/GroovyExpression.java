package org.integratedmodelling.thinklab.modelling.lang.expressions;

import groovy.lang.Binding;
import groovy.lang.GroovyShell;
import groovy.lang.Script;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.codehaus.groovy.control.CompilerConfiguration;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.annotations.Concept;
import org.integratedmodelling.thinklab.api.annotations.Property;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.ICondition;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.project.IProject;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.geospace.Geospace;
import org.integratedmodelling.thinklab.time.Time;

@Concept(NS.EXPRESSION_DEFINITION)
public class GroovyExpression implements ICondition {

    @Property(NS.HAS_EXPRESSION)
    protected String _code;

    Script _script;
    IProject _project;

    @Property(NS.HAS_CONCEPT_SPACE)
    Set<IConcept> _domain;

    @Property(NS.HAS_NAMESPACE_ID)
    String _namespaceId;

    @Property(NS.IS_NEGATED)
    boolean _negated = false;

    INamespace _namespace;

    /*
     * used by Thinklab to instantiate
     */
    public GroovyExpression() {
    }

    public void initializeFromStorage() {

        if (_namespaceId != null) {
            _namespace = Thinklab.get().getNamespace(_namespaceId);
        }
    }

    /*
     * used by Thinklab - when using the API use the String constructor. MUST be called in all
     * cases.
     */
    public void initialize(Map<String, IObserver> inputs, Map<String, IObserver> outputs) {
        compile(preprocess(_code, inputs, outputs));
    }

    /**
     * Simple expression without context or receivers. NOT PREPROCESSED in the context it's in.
     *
     * @param code
     */
    public GroovyExpression(String code) {
        _code = code;
        initialize(null, null);
    }

    public GroovyExpression(String code, INamespace namespace, Set<IConcept> _domain) {
        _namespace = namespace;
        if (_namespace != null) {
            _namespaceId = _namespace.getId();
        }
        _code = code;
    }

    private void compile(String code) {
        CompilerConfiguration compiler = new CompilerConfiguration();
        compiler.setScriptBaseClass(getBaseClass());
        GroovyShell shell = new GroovyShell(this.getClass().getClassLoader(), new Binding(), compiler);
        _script = shell.parse(code);
    }

    private String getBaseClass() {

        /*
         * choose proper class according to domains so that the appropriate functions are
         * supported.
         */
        if (_domain != null) {

            if (_domain.contains(Geospace.get().SpatialDomain()) && _domain.contains(Time.TIME_DOMAIN)) {
                return "org.integratedmodelling.thinklab.actions.SpatioTemporalActionScript";
            } else if (_domain.contains(Geospace.get().SpatialDomain())) {
                return "org.integratedmodelling.thinklab.actions.SpatialActionScript";
            } else if (_domain.contains(Time.TIME_DOMAIN)) {
                return "org.integratedmodelling.thinklab.actions.TemporalActionScript";
            }
        }
        return "org.integratedmodelling.thinklab.actions.ActionScript";
    }

    @Override
    public Object eval(Map<String, Object> parameters, IConcept... context) throws ThinklabException {
        try {
            _script.setBinding(getBinding(parameters));
            return _script.run();
        } catch (Throwable t) {
            throw new ThinklabException(t);
        }
    }

    private Binding getBinding(Map<String, Object> parameters) {

        HashMap<String, Object> p = new HashMap<String, Object>();
        p.put("_p", parameters);
        p.putAll(parameters);
        Binding ret = new Binding(p);

        /*
         * TODO bind receivers, project, schedule, events etc.
         */

        return ret;
    }

    @Override
    public void setProjectContext(IProject project) {
        _project = project;
    }

    private String preprocess(String code, Map<String, IObserver> inputs, Map<String, IObserver> outputs) {

        String c = code;

        /*
         * replace "unknown" with "null"
         */
        c = replaceIdentifier("unknown", "null", c);
        
        /*
         * replace each input variable with a reference to it in the parameter map. This should
         * take care of all the dash-separated names, but not of those specified in error.
         */
        if (inputs != null) {
            for (String key : inputs.keySet()) {
                c = replaceIdentifier(key, "_p.get(\"" + key + "\")", c);
            }
        }

        if (_domain != null) {

            if (_domain.contains(Geospace.get().SpatialDomain())) {
                /*
                 * TODO preprocess "space" token
                 */
            }
            if (_domain.contains(Time.TIME_DOMAIN)) {
                /*
                 * TODO preprocess "time" token
                 */
            }
        }

        /*
         * find external concepts first
         */
        Pattern extConcept = Pattern.compile("[a-z|\\.]+:[A-Z][A-Za-z0-9]*");
        Matcher matcher = extConcept.matcher(c);

        ArrayList<String> toSwap = new ArrayList<String>();
        while (matcher.find()) {
            String s = matcher.group();
            if (Thinklab.get().getConcept(s) != null) {
                toSwap.add(s);
            }
        }

        for (String sw : toSwap) {
            c = replaceIdentifier(sw, "_getConcept(\"" + sw + "\")", c);
        }

        /*
         * then if we have a namespace, lookup internal concepts after
         * we swapped all the fully qualified ones.
         */
        if (_namespace != null) {
            /*
             * TODO lookup all concepts from the ontology in the current NS
             */
            for (IConcept conc : _namespace.getOntology().getConcepts()) {
                // GOD this is so weak
                if (!c.contains(":" + conc.getLocalName())) {
                    c = replaceIdentifier(conc.getLocalName(), "_getConcept(\"" + conc + "\")", c);
                }
            }
        }

        return c;
    }

    /*
     * TODO this is entirely tentative - the string replace approach is weak and should be replaced
     * with a reasonable lexical analyzer, acting only on legitimate identifiers.
     */
    String replaceIdentifier(String identifier, String replacement, String code) {
        return code.replace(identifier, replacement);
    }

    @Override
    public String toString() {
        return _code;
    }

    @Override
    public void setNegated(boolean negate) {
        _negated = negate;
    }

    @Override
    public boolean isNegated() {
        return _negated;
    }

}
