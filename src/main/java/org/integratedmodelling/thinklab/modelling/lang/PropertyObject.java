package org.integratedmodelling.thinklab.modelling.lang;

import org.integratedmodelling.thinklab.api.annotations.Concept;
import org.integratedmodelling.thinklab.api.knowledge.IProperty;
import org.integratedmodelling.thinklab.api.lang.IModelResolver;
import org.integratedmodelling.thinklab.api.modelling.parsing.IPropertyDefinition;
import org.integratedmodelling.thinklab.common.vocabulary.NS;

@Concept(NS.PROPERTY_DEFINITION)
public class PropertyObject extends ModelObject<PropertyObject> implements IPropertyDefinition {

    /*
     * used when property objects are returned but the property is created beforehand
     */
    public PropertyObject(IProperty property) {
        _id = property.getLocalName();
        _namespaceId = property.getConceptSpace();
    }

    public PropertyObject() {
    }

    @Override
    public PropertyObject demote() {
        return this;
    }

    @Override
    public String getName() {

        /*
         * namespace == null only happens in error, but let it through so
         * we don't get a null pointer exception, and we report the error 
         * anyway.
         */
        String ns = "UNDEFINED";
        if (getNamespace() != null)
            ns = getNamespace().getId();
        else
            System.out.println("ZIO CALAMARO.");

        return ns + ":" + _id;
    }

    @Override
    public void initialize(IModelResolver resolver) {
        // TODO Auto-generated method stub

    }

}
