package org.integratedmodelling.thinklab.modelling.lang;

import java.io.PrintStream;
import java.util.List;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.lang.IModelResolver;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.modelling.IAccessor;
import org.integratedmodelling.thinklab.api.modelling.IAction;
import org.integratedmodelling.thinklab.api.modelling.IClassification;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.IPercentageObserver;
import org.integratedmodelling.thinklab.api.modelling.IProportionObserver;
import org.integratedmodelling.thinklab.api.modelling.IStateAccessor;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.api.modelling.parsing.IClassificationDefinition;
import org.integratedmodelling.thinklab.api.modelling.parsing.IConceptDefinition;
import org.integratedmodelling.thinklab.api.modelling.parsing.INamespaceDefinition;
import org.integratedmodelling.thinklab.api.modelling.parsing.IRelativeObserverDefinition;
import org.integratedmodelling.thinklab.common.configuration.Env;
import org.integratedmodelling.thinklab.common.data.IndexedCategoricalDistribution;
import org.integratedmodelling.thinklab.common.utils.MiscUtilities;
import org.integratedmodelling.thinklab.common.vocabulary.NS;

public class Proportion extends Observer<Proportion> implements IProportionObserver, IPercentageObserver,
        IRelativeObserverDefinition {

    IClassification _discretization;
    IConcept        _comparisonConcept;
    boolean         _isProportion;
    boolean         _isIndirect;

    public Proportion(boolean isProportion) {
        _isProportion = isProportion;
    }

    @Override
    public Proportion demote() {
        return this;
    }

    @Override
    public IConcept getObservedType(IConcept concept) {

        IConcept ret = concept;

        if (_isIndirect) {
            ret = NS.makeProportion(concept, _comparisonConcept);
            if (ret == null) {
                addError(new ThinklabValidationException(concept
                        + ": proportions must compare two related observables, differing by a trait or by inheritance."),
                        _firstLineNumber);
                return concept;
            }
        } else if (!concept.is(Env.c(NS.PROPORTION))) {
            addError(new ThinklabValidationException(concept
                    + ": the observable in this statement must be a proportion or percentage. Use the indirect form (with 'of') to annotate explicit comparisons."),
                    _firstLineNumber);
        }
        return ret;
    }

    @Override
    public void notifyModelObservable(IObservable observable, IModelResolver resolver) {

        if (_discretization != null) {
            ((INamespaceDefinition) getNamespace()).synchronizeKnowledge(null);
            ((IClassificationDefinition) _discretization).setConceptSpace(observable, resolver);
            ((IClassificationDefinition) _discretization).initialize();
            if (!_discretization.isContiguousAndFinite()) {
                addError(new ThinklabValidationException(
                        "discretization has discontinuous and/or unbounded intervals"),
                        _discretization.getFirstLineNumber());
            }
        }

    }

    /*
     * -----------------------------------------------------------------------------------
     * accessor - it's always a mediator, either to another measurement or to a datasource
     * whose content was defined explicitly to conform to our semantics
     * -----------------------------------------------------------------------------------
     */
    public class ProportionAccessor extends MediatingAccessor {

        boolean _errorsPresent = false;

        public ProportionAccessor(List<IAction> actions, IMonitor monitor, boolean interpreting) {
            super(actions, monitor, interpreting);
            if (_discretization != null)
                ((org.integratedmodelling.thinklab.common.classification.Classification) _discretization)
                        .reset();

        }

        @Override
        public String toString() {
            return "[" + "percentage/proportion" + "]";
        }

        @Override
        public Object mediate(Object object) {

            double val = Double.NaN;

            if (object == null || (object instanceof Number && Double.isNaN(((Number) object).doubleValue())))
                return val;

            if (object instanceof IndexedCategoricalDistribution) {

                /*
                 * TODO try to mediate distributions
                 */
                val = ((IndexedCategoricalDistribution) object).getMean();
            } else if (object instanceof Number) {
                val = ((Number) object).doubleValue();
            } else {
                try {
                    val = object.toString().equals("NaN") ? Double.NaN : Double
                            .parseDouble(object.toString());
                } catch (Exception e) {
                    if (!_errorsPresent) {
                        _monitor.error("cannot interpret value: " + object + " as a number");
                        _errorsPresent = true;
                    }
                    return Double.NaN;
                }
            }

            return val;
        }
    }

    @Override
    public void setDiscretization(IClassification classification) {
        _discretization = classification;
    }

    @Override
    public IClassification getDiscretization() {
        return _discretization;
    }

    @Override
    public boolean isDiscrete() {
        return _discretization != null;
    }

    @Override
    protected void dumpIndented(PrintStream out, int indent) {

        String ind = MiscUtilities.spaces(indent);
        String in3 = MiscUtilities.spaces(3);

        out.print(ind + "percentage/proportion" + " " + dumpObservable());
        if (getDiscretization() != null) {
            out.println();
            out.println(ind + in3 + "discretized as");
            ((org.integratedmodelling.thinklab.common.classification.Classification) getDiscretization())
                    .dumpIndented(out, indent + 6);
        }
    }

    @Override
    public IStateAccessor getMediator(IObserver observer, IMonitor monitor) throws ThinklabException {

        IAccessor ret = getUserDefinedAccessor(monitor);
        if (ret != null)
            return (IStateAccessor) ret;

        return new ProportionAccessor(getActions(), monitor, false);
    }

    @Override
    public boolean canInterpretDirectly(IAccessor accessor) {
        return getActions().size() == 0 && _discretization == null;
    }

    @Override
    public IStateAccessor getInterpreter(IStateAccessor accessor, IMonitor monitor) {
        return new ProportionAccessor(getActions(), monitor, true);
    }

    @Override
    public String getSignature() {
        return "#percentage/proportion#" + (_discretization == null ? "" : _discretization.getConceptSpace());
    }

    @Override
    public String toString() {
        return "[proportion " + _observable.getType()
                + (_inherentSubjectType == null ? " " : ("of" + _inherentSubjectType + " ")) + "]";
    }

    @Override
    public IConcept getObservationType(INamespace namespace) {
        return _isProportion ? Env.c(NS.PROPORTION) : Env.c(NS.PERCENTAGE);
    }

    public double getMinimumValue() {
        return 0;
    }

    @Override
    public double getMaximumValue() {
        return _isProportion ? 1.0 : 100.0;
    }

    @Override
    public IConcept getComparisonConcept() {
        return _comparisonConcept;
    }

    @Override
    public boolean isIndirect() {
        return _isIndirect;
    }

    @Override
    public void setComparisonConcept(IConceptDefinition concept) {
        _comparisonConcept = Env.c(concept.getName());
    }

    @Override
    public void setIndirect(boolean b) {
        // TODO Auto-generated method stub
        _isIndirect = b;
    }
}
