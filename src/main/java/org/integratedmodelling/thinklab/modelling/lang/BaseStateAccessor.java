package org.integratedmodelling.thinklab.modelling.lang;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.metadata.IMetadata;
import org.integratedmodelling.thinklab.api.modelling.IDataset;
import org.integratedmodelling.thinklab.api.modelling.IModel;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.IObservingObject;
import org.integratedmodelling.thinklab.api.modelling.IState;
import org.integratedmodelling.thinklab.api.modelling.IStateAccessor;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.modelling.interfaces.IExtendedDataset;
import org.integratedmodelling.thinklab.modelling.interfaces.IModifiableState;
import org.integratedmodelling.thinklab.modelling.interfaces.IRawAccessor;
import org.integratedmodelling.thinklab.modelling.resolver.ProvenanceGraph;
import org.integratedmodelling.thinklab.modelling.states.DatasetBackedState;
import org.integratedmodelling.thinklab.modelling.states.IndexedObjectState;
import org.integratedmodelling.thinklab.modelling.states.State;

public abstract class BaseStateAccessor extends Accessor implements IStateAccessor {

    HashMap<String, IObserver> _outputs    = new HashMap<String, IObserver>();
    HashMap<String, IObserver> _inputs     = new HashMap<String, IObserver>();
    ArrayList<String>          _inputKeys  = new ArrayList<String>();
    ArrayList<String>          _outputKeys = new ArrayList<String>();

    private ArrayList<IState>  _states;

    /**
     * Return the input formal names.
     * 
     * @return
     */
    protected List<String> getInputKeys() {
        return _inputKeys;
    }

    /**
     * Return all the output formal names. First one will be the main observable, equal to
     * getName().
     * 
     * @return
     */
    protected List<String> getOutputKeys() {
        return _outputKeys;
    }

    /**
     * Return the observer for the passed output keys.
     * 
     * @param key
     * @return
     */
    protected IObserver getObserverForOutput(String key) {
        return _outputs.get(key);
    }

    /**
     * This one will return the observer for the passed input key.
     * 
     * @param key
     * @return
     */
    protected IObserver getObserverForInput(String key) {
        return _inputs.get(key);
    }

    /**
     * Use this one to intercept the expected outputs. 
     * @param observable
     * @param observer
     * @param key
     * @param isMainObservable
     * @throws ThinklabException
     */
    protected void notifyOutput(IObservable observable, IObserver observer, String key,
            boolean isMainObservable) throws ThinklabException {
        // reimplement
    }

    /**
     * Use this one to intercept the available dependencies when process() is called.
     * 
     */
    protected void notifyInput(IObservable observable, IObserver observer, String key, boolean isMediated)
            throws ThinklabException {
    }

    @Override
    public final void notifyExpectedOutput(IObservable observable, IObserver observer, String key)
            throws ThinklabException {
        _outputKeys.add(key);
        if (observer == null) {
            System.out.println("xccc");
        }
        IObserver oob = Observer.getRepresentativeObserver(observer);
        if (oob == null) {
            System.out.println("xccc");
        }
        _outputs.put(key, oob);
        notifyOutput(observable, oob, key, false);
    }

    @Override
    public final void notifyObserver(IObservable observable, IObserver observer) throws ThinklabException {
        _outputKeys.add(getName());
        IObserver oob = Observer.getRepresentativeObserver(observer);
        _outputs.put(getName(), oob);
        notifyOutput(observable, oob, getName(), true);
    }

    @Override
    public final void notifyDependency(IObservable observable, IObserver observer, String key,
            boolean isMediation) throws ThinklabException {
        _inputKeys.add(key);
        _inputs.put(key, Observer.getRepresentativeObserver(observer));
        notifyInput(observable, Observer.getRepresentativeObserver(observer), key, isMediation);

    }

    public Collection<IState> createStates(ISubject context, IModel model, IObserver obs,
            ProvenanceGraph provenance, IDataset dataset) {

        if (_outputs.size() == 0)
            return null;

        _states = new ArrayList<IState>();

        for (String key : _outputs.keySet()) {

            IObserver observer = _outputs.get(key);

            /*
             * label with the appropriate observable.
             */
            IObservingObject pkey = obs; /*((Observer<?>) observer).getTopLevelModel();
                                         if (pkey == null)
                                         pkey = observer; */

            IObservable c = pkey.getObservable();
            // if (((Observer<?>) observer).getModel() != null) {
            // c = ((Observer<?>) observer).getModel().getObservable();
            // }

            IState ret = dataset == null ? new IndexedObjectState(c, context, observer)
                    : new DatasetBackedState(c, context.getScale(), observer, (IExtendedDataset) dataset);

            String label = c.toString();

            if (this instanceof IRawAccessor) {
                label = ((IRawAccessor) this).getDatasourceLabel();
                ((State) ret).setRaw(true);
            }
            ((Metadata) (ret.getMetadata())).put(NS.DISPLAY_LABEL_PROPERTY, label);

            IMetadata md = provenance.collectMetadata(obs);
            ret.getMetadata().merge(md, false);

            _states.add(ret);
        }

        return _states;
    }

    public void updateStates(int stateIndex) {

        int i = 0;
        for (String key : _outputs.keySet()) {
            ((IModifiableState) (_states.get(i++))).setValue(stateIndex, getValue(key));
        }
    }
}
