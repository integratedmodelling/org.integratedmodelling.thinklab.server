package org.integratedmodelling.thinklab.modelling.lang;

import java.io.PrintStream;
import java.util.List;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.lang.IModelResolver;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.modelling.IAccessor;
import org.integratedmodelling.thinklab.api.modelling.IAction;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.IStateAccessor;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.api.modelling.parsing.INamespaceDefinition;
import org.integratedmodelling.thinklab.api.modelling.parsing.IPresenceObserverDefinition;
import org.integratedmodelling.thinklab.common.data.IndexedCategoricalDistribution;
import org.integratedmodelling.thinklab.common.utils.CamelCase;
import org.integratedmodelling.thinklab.common.utils.MiscUtilities;
import org.integratedmodelling.thinklab.common.utils.NameGenerator;
import org.integratedmodelling.thinklab.common.utils.NumberUtils;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.common.vocabulary.Observable;

public class Presence extends Observer<Presence> implements IPresenceObserverDefinition {

    boolean _indirect = true;

    public Presence() {
    }

    public Presence(IConcept type, INamespace namespace) {

        String id = NameGenerator.newName("prmd");
        setNamespace((INamespaceDefinition) namespace);
        IConcept otype = getObservedType(type);
        this._observable = new Observable(otype, getObservationType(namespace), CamelCase.toLowerCase(otype
                .getLocalName(), '-'));
        this._observableConcept = otype;
        this.setId(id);
    }

    @Override
    public Presence demote() {
        return this;
    }

    /*
     * -----------------------------------------------------------------------------------
     * accessor - it's always a mediator, either to another measurement or to a datasource
     * whose content was defined explicitly to conform to our semantics
     * -----------------------------------------------------------------------------------
     */
    public class PresenceAccessor extends MediatingAccessor {

        boolean _errorsPresent = false;

        public PresenceAccessor(List<IAction> actions, IMonitor monitor, boolean interpreting) {
            super(actions, monitor, interpreting);
        }

        @Override
        public String toString() {
            return "[presence of " + _inherentSubjectType + "]";
        }

        /*
         * this turns things into booleans for observers that get their input from datasources.
         * 
         * (non-Javadoc)
         * @see org.integratedmodelling.thinklab.modelling.lang.MediatingAccessor#getValue(java.lang.String)
         */
        @Override
        public Object getValue(String outputKey) {
            Object val = super.getValue(outputKey);
            return mediate(val);
        }

        @Override
        public Object mediate(Object object) {

            Object val = null;

            // not much to say
            if (object instanceof Boolean)
                return object;

            if (object == null || (object instanceof Number && Double.isNaN(((Number) object).doubleValue())))
                return null;

            if (object instanceof IndexedCategoricalDistribution) {

                /*
                 * TODO try to mediate distributions
                 */
                return !(NumberUtils.equal(((IndexedCategoricalDistribution) object).getMean(), 0));

            } else if (object instanceof Number) {
                return !(NumberUtils.equal(((Number) object).doubleValue(), 0));
            } else {
                try {
                    val = object.toString().equals("NaN") ? Double.NaN : Double
                            .parseDouble(object.toString());
                    return !(NumberUtils.equal(((Number) val).doubleValue(), 0));
                } catch (Exception e) {
                    if (!_errorsPresent) {
                        _monitor.error("cannot interpret value: " + object + " as a number");
                        _errorsPresent = true;
                    }
                    return null;
                }
            }
        }
    }

    @Override
    protected void dumpIndented(PrintStream out, int indent) {

        String ind = MiscUtilities.spaces(indent);
        out.print(ind + "presence " + dumpObservable());
    }

    @Override
    public IStateAccessor getMediator(IObserver observer, IMonitor monitor) throws ThinklabException {

        IAccessor ret = getUserDefinedAccessor(monitor);
        if (ret != null)
            return (IStateAccessor) ret;

        return new PresenceAccessor(getActions(), monitor, false);
    }

    // @Override
    // protected IObservable notifyObservable(Observable observable) {
    //
    // Observable ret = new Observable(observable);
    // ret.setObservationType(Thinklab.c(NS.IDENTIFICATION));
    // ret.setObservedType(Thinklab.c(NS.PRESENCE));
    // ret.setInherentType(observable.getType());
    //
    // return ret;
    // }

    @Override
    public IConcept getObservedType(IConcept concept) {

        IConcept ret = concept;
        if (_indirect) {
            ret = NS.makePresence(concept);
            if (ret == null) {
                addError(new ThinklabValidationException(concept
                        + ": presence is only assessed for things or agents. For presence qualities, use the direct form (without 'of') of the observer"),
                        _firstLineNumber);
                return concept;
            }
        }
        return ret;
    }

    @Override
    public boolean canInterpretDirectly(IAccessor accessor) {
        return false; // getActions().size() == 0;
    }

    @Override
    public IStateAccessor getInterpreter(IStateAccessor accessor, IMonitor monitor) {
        return new PresenceAccessor(getActions(), monitor, true);
    }

    @Override
    public String toString() {
        return "[presence of " + _inherentSubjectType + "]";
    }

    @Override
    public IConcept getObservationType(INamespace namespace) {
        return Thinklab.c(NS.PRESENCE);
    }

    @Override
    public void initialize(IModelResolver resolver) {
        super.initialize(resolver);
    }

    @Override
    public void notifyModelObservable(IObservable observable, IModelResolver resolver) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setIndirect(boolean b) {
        _indirect = b;
    }

}
