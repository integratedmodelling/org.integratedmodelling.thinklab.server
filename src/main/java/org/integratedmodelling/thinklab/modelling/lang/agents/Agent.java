package org.integratedmodelling.thinklab.modelling.lang.agents;

import java.util.Collection;

import org.integratedmodelling.thinklab.api.annotations.SubjectType;
import org.integratedmodelling.thinklab.api.modelling.IExtent;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.modelling.lang.Subject;

/**
 * Just a place holder in the inheritance tree for Agents.
 * 
 * @author luke
 * 
 */
@SubjectType(NS.BASE_AGENT)
public abstract class Agent extends Subject {

    public Agent(IObservable type, Collection<IExtent> extents, Object object, INamespace namespace,
            String name) {
        super(type, extents, object, namespace, name);
        // TODO Auto-generated constructor stub
    }
}
