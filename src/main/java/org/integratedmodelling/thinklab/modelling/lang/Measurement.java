package org.integratedmodelling.thinklab.modelling.lang;

import java.io.PrintStream;
import java.util.List;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.annotations.Concept;
import org.integratedmodelling.thinklab.api.annotations.Property;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.lang.IModelResolver;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.modelling.IAccessor;
import org.integratedmodelling.thinklab.api.modelling.IAction;
import org.integratedmodelling.thinklab.api.modelling.IClassification;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.IStateAccessor;
import org.integratedmodelling.thinklab.api.modelling.IUnit;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.api.modelling.parsing.IClassificationDefinition;
import org.integratedmodelling.thinklab.api.modelling.parsing.IMeasuringObserverDefinition;
import org.integratedmodelling.thinklab.api.modelling.parsing.INamespaceDefinition;
import org.integratedmodelling.thinklab.api.modelling.parsing.IUnitDefinition;
import org.integratedmodelling.thinklab.common.data.IndexedCategoricalDistribution;
import org.integratedmodelling.thinklab.common.utils.MiscUtilities;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.common.vocabulary.Unit;

@Concept(NS.MEASURING_OBSERVER)
public class Measurement extends Observer<Measurement> implements IMeasuringObserverDefinition {

    @Property(NS.HAS_UNIT_DEFINITION)
    IUnitDefinition _unitDefinition;

    @Property(NS.HAS_CLASSIFICATION)
    IClassification _discretization;

    IUnit           _unit;
    Throwable       _unitError;

    public IUnit getUnit() {
        return _unit;
    }

    @Override
    public void setUnit(IUnitDefinition unit) {
        _unitDefinition = unit;
    }

    @Override
    public Measurement demote() {
        return this;
    }

    // @Override
    // public void initialize() {
    //
    // try {
    // _unit = new Unit(_unitDefinition.getStringExpression());
    // } catch (Throwable e) {
    // _unitError = e;
    // }
    // // if (_discretization != null) {
    // // ((IClassificationDefinition)_discretization).initialize();
    // // }
    // }

    /*
     * -----------------------------------------------------------------------------------
     * accessor - it's always a mediator, either to another measurement or to a datasource
     * whose content was defined explicitly to conform to our semantics
     * -----------------------------------------------------------------------------------
     */
    public class MeasurementAccessor extends MediatingAccessor {

        Measurement other;
        boolean     _errorsPresent = false;

        public MeasurementAccessor(Measurement other, List<IAction> actions, IMonitor monitor,
                boolean interpreting) {
            super(actions, monitor, interpreting);
            this.other = other;

            if (_discretization != null)
                ((org.integratedmodelling.thinklab.common.classification.Classification) _discretization)
                        .reset();

        }

        @Override
        public String toString() {
            return "[measurement: " + _unit + "]";
        }

        public IUnit getUnit() {
            return _unit;
        }

        @Override
        public Object mediate(Object object) {

            double val = Double.NaN;

            if (object == null || (object instanceof Number && Double.isNaN(((Number) object).doubleValue())))
                return val;

            if (object instanceof IndexedCategoricalDistribution) {
                val = ((IndexedCategoricalDistribution) object).getMean();
            } else if (object instanceof Number) {
                val = ((Number) object).doubleValue();
            } else {
                try {
                    val = object.toString().equals("NaN") ? Double.NaN : Double
                            .parseDouble(object.toString());
                } catch (Exception e) {
                    if (!_errorsPresent) {
                        _monitor.error("cannot interpret value: " + object);
                        _errorsPresent = true;
                    }
                    return Double.NaN;
                }
            }

            return _unit.convert(val, other.getUnit());
        }
    }

    @Override
    public void setDiscretization(IClassification classification) {
        _discretization = classification;
    }

    @Override
    public IClassification getDiscretization() {
        return _discretization;
    }

    @Override
    public String toString() {
        return "[measurement " + _observable.getType()
                + (_inherentSubjectType == null ? " " : ("of" + _inherentSubjectType + " ")) + _unit + "]";
    }

    public boolean isDiscrete() {
        return _discretization != null;
    }

    @Override
    protected void dumpIndented(PrintStream out, int indent) {

        String ind = MiscUtilities.spaces(indent);
        String in3 = MiscUtilities.spaces(3);

        out.print(ind + "measure " + dumpObservable() + " in " + _unit);
        if (getDiscretization() != null) {
            out.println();
            out.println(ind + in3 + "discretized as");
            ((org.integratedmodelling.thinklab.common.classification.Classification) getDiscretization())
                    .dumpIndented(out, indent + 6);
        }
    }

    @Override
    public IStateAccessor getMediator(IObserver observer, IMonitor monitor) throws ThinklabException {

        observer = getRepresentativeObserver(observer);

        IAccessor ret = getUserDefinedAccessor(monitor);
        if (ret != null)
            return (IStateAccessor) ret;

        if (!(observer instanceof Measurement))
            throw new ThinklabValidationException("measurements can only mediate other measurements");

        if (getActions().size() == 0 && ((Measurement) observer).getUnit().equals(getUnit())) {
            return null;
        }

        return new MeasurementAccessor((Measurement) observer, getActions(), monitor, false);
    }

    @Override
    public IStateAccessor getInterpreter(IStateAccessor accessor, IMonitor monitor) {
        return new MeasurementAccessor(this, getActions(), monitor, true);
    }

    @Override
    public boolean canInterpretDirectly(IAccessor accessor) {
        return getActions().size() == 0 && _discretization == null;
    }

    @Override
    public String getSignature() {
        return "#measurement#" + _unit + (_discretization == null ? "" : _discretization.getConceptSpace());

    }

    @Override
    public void validate(IModelResolver resolver) {

        super.validate(resolver);
        if (_unitError != null) {
            resolver.onException(
                    new ThinklabValidationException("invalid unit: " + _unitDefinition.getStringExpression()),
                    this.getFirstLineNumber());
        }
        // try {
        // if (_discretization != null) {
        // /*
        // * TODO validate the continuity
        // */
        // }
        // } catch (ThinklabException e) {
        // resolver.onException(e, _discretization.getFirstLineNumber());
        // }
    }

    @Override
    public void notifyModelObservable(IObservable observable, IModelResolver resolver) {

        if (_discretization != null) {
            ((INamespaceDefinition) getNamespace()).synchronizeKnowledge(null);
            ((IClassificationDefinition) _discretization).setConceptSpace(observable, resolver);
            ((IClassificationDefinition) _discretization).initialize();
            if (!_discretization.isContiguousAndFinite()) {
                addError(new ThinklabValidationException(
                        "discretization has discontinuous and/or unbounded intervals"),
                        _discretization.getFirstLineNumber());
            }
        }

    }

    @Override
    public IConcept getObservationType(INamespace namespace) {

        /*
         * TODO specialize according to unit dimensions, and further
         * if there is a discretization
         */

        return Thinklab.c(NS.MEASUREMENT);
    }

    @Override
    public void initialize(IModelResolver resolver) {
        super.initialize(resolver);
        try {
            _unit = new Unit(_unitDefinition.getStringExpression());
        } catch (Throwable e) {
            _unitError = e;
            throw new ThinklabRuntimeException(e);
        }
        // if (_discretization != null) {
        // ((INamespaceDefinition)getNamespace()).synchronizeKnowledge(resolver);
        // ((IClassificationDefinition)_discretization).setConceptSpace(getObservableConcept(), resolver);
        // ((IClassificationDefinition)_discretization).initialize();
        // }

        ((Metadata) getMetadata())
                .put(NS.INTERNAL_DATA_LABEL_PROPERTY, _unitDefinition.getStringExpression());
    }

    @Override
    public double getMinimumValue() {
        return Double.NaN;
    }

    @Override
    public double getMaximumValue() {
        return Double.NaN;
    }

}
