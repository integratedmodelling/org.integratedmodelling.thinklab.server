package org.integratedmodelling.thinklab.modelling.lang.agents;

import org.integratedmodelling.thinklab.modelling.lang.Metadata;

public class ScaleMediatorMetadata extends Metadata {
    public enum ScaleMediatorTag {
        differentiable,
        differentiableArbitraryOrders,
        stepFunction,
        linear,
        planar,
        arbitraryDimensions,
        oneDimensional,
        twoDimensional,
        threeDimensional
    }

    /**
     * the main constructor. Tags the various scale interpolator features using the string version of the
     * ScaleMediatorTag ENUM.
     * 
     * calling ScaleMediatorMetadata.getBoolean() will return the intuitively appropriate result: Boolean.TRUE
     * for features that are present, or Boolean.FALSE for those that are not.
     * 
     * @param tags
     */
    public ScaleMediatorMetadata(ScaleMediatorTag... tags) {
        for (ScaleMediatorTag tag : tags) {
            put(tag.name(), Boolean.TRUE);
        }
    }

    /**
     * essentially the same as getBoolean(), but it returns a primitive boolean, and it takes a
     * ScaleMediatorTag so the caller doesn't have to convert it using .name() before calling. Just a simpler
     * interface.
     * 
     * @param key
     * @return
     */
    public boolean is(ScaleMediatorTag key) {
        return (Boolean) get(key.name());
    }
}
