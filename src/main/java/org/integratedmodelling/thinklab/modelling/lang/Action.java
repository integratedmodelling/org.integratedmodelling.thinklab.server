package org.integratedmodelling.thinklab.modelling.lang;

import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.ICondition;
import org.integratedmodelling.thinklab.api.knowledge.IExpression;
import org.integratedmodelling.thinklab.api.modelling.IAction;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.modelling.lang.expressions.GroovyExpression;

public class Action implements IAction {

    public IExpression action;
    public IExpression condition;
    public IConcept[]  domains;
    public String      target;
    public int         type;
    public String      namespaceId;
    boolean            isConditionNegated;

    @Override
    public String toString() {

        String ret = "";
        if (action != null) {
            ret += action;
        }
        if (condition != null) {
            ret += " if " + condition;
        }

        return ret;
    }

    public Action(String target, int type, IExpression action, IExpression condition, INamespace namespace,
            IConcept[] domains) {
        this.action = action;
        this.condition = condition;
        this.domains = domains;
        this.target = target;
        this.type = type;
        this.namespaceId = namespace == null ? null : namespace.getId();

        if (condition != null) {
            isConditionNegated = condition instanceof ICondition && ((ICondition) condition).isNegated();
        }
    }

    /**
     * The concept that triggers the action when the correspondent state
     * changes in the context. If null, this is an initialization action.
     * 
     * @return
     */
    @Override
    public IConcept[] getDomain() {
        return domains;
    }

    public void preprocess(Map<String, IObserver> inputs, HashMap<String, IObserver> outputs) {

        if (action instanceof GroovyExpression) {
            ((GroovyExpression) action).initialize(inputs, outputs);
        }

        if (condition instanceof GroovyExpression) {
            ((GroovyExpression) condition).initialize(inputs, outputs);
        }

    }
}
