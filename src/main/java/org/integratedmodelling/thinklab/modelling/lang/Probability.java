package org.integratedmodelling.thinklab.modelling.lang;

import java.io.PrintStream;
import java.util.List;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.lang.IModelResolver;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.modelling.IAccessor;
import org.integratedmodelling.thinklab.api.modelling.IAction;
import org.integratedmodelling.thinklab.api.modelling.IClassification;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.IStateAccessor;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.api.modelling.parsing.IClassificationDefinition;
import org.integratedmodelling.thinklab.api.modelling.parsing.INamespaceDefinition;
import org.integratedmodelling.thinklab.api.modelling.parsing.IProbabilityObserverDefinition;
import org.integratedmodelling.thinklab.common.configuration.Env;
import org.integratedmodelling.thinklab.common.data.IndexedCategoricalDistribution;
import org.integratedmodelling.thinklab.common.utils.MiscUtilities;
import org.integratedmodelling.thinklab.common.vocabulary.NS;

public class Probability extends Observer<Probability> implements IProbabilityObserverDefinition {

    IClassification _discretization;
    boolean         _isIndirect;

    @Override
    public Probability demote() {
        return this;
    }

    @Override
    public IConcept getObservedType(IConcept concept) {

        IConcept ret = concept;

        if (_isIndirect) {
            ret = NS.makeProbability(concept);
            if (ret == null) {
                addError(new ThinklabValidationException(concept
                        + ": probabilities are assigned only to events or processes. Use the direct form (without 'of') for observables that are already probabilities."),
                        _firstLineNumber);
                return concept;
            }
        } else if (!concept.is(Env.c(NS.PROBABILITY))) {
            addError(new ThinklabValidationException(concept
                    + ": the observable in this statement must be a probability. Use the indirect form (with 'of') for events or processes."),
                    _firstLineNumber);
        }
        return ret;
    }

    @Override
    public void notifyModelObservable(IObservable observable, IModelResolver resolver) {

        if (_discretization != null) {
            ((INamespaceDefinition) getNamespace()).synchronizeKnowledge(null);
            ((IClassificationDefinition) _discretization).setConceptSpace(observable, resolver);
            ((IClassificationDefinition) _discretization).initialize();
            if (!_discretization.isContiguousAndFinite()) {
                addError(new ThinklabValidationException(
                        "discretization has discontinuous and/or unbounded intervals"),
                        _discretization.getFirstLineNumber());
            }
        }

    }

    /*
     * -----------------------------------------------------------------------------------
     * accessor - it's always a mediator, either to another measurement or to a datasource
     * whose content was defined explicitly to conform to our semantics
     * -----------------------------------------------------------------------------------
     */
    public class ProbabilityAccessor extends MediatingAccessor {

        boolean _errorsPresent = false;

        public ProbabilityAccessor(List<IAction> actions, IMonitor monitor, boolean interpreting) {
            super(actions, monitor, interpreting);
            if (_discretization != null)
                ((org.integratedmodelling.thinklab.common.classification.Classification) _discretization)
                        .reset();

        }

        @Override
        public String toString() {
            return "[probability of " + _inherentSubjectType + "]";
        }

        @Override
        public Object mediate(Object object) {

            double val = Double.NaN;

            if (object == null || (object instanceof Number && Double.isNaN(((Number) object).doubleValue())))
                return val;

            if (object instanceof IndexedCategoricalDistribution) {

                /*
                 * TODO try to mediate distributions
                 */
                val = ((IndexedCategoricalDistribution) object).getMean();
            } else if (object instanceof Number) {
                val = ((Number) object).doubleValue();
            } else {
                try {
                    val = object.toString().equals("NaN") ? Double.NaN : Double
                            .parseDouble(object.toString());
                } catch (Exception e) {
                    if (!_errorsPresent) {
                        _monitor.error("cannot interpret value: " + object + " as a number");
                        _errorsPresent = true;
                    }
                    return Double.NaN;
                }
            }

            if (!Double.isNaN(val) && (val < 0 || val > 1)) {
                throw new ThinklabRuntimeException("probabilities cannot have a value outside of the 0-1 range");
            }

            return val;
        }
    }

    @Override
    public void setDiscretization(IClassification classification) {
        _discretization = classification;
    }

    @Override
    public IClassification getDiscretization() {
        return _discretization;
    }

    public boolean isDiscrete() {
        return _discretization != null;
    }

    @Override
    protected void dumpIndented(PrintStream out, int indent) {

        String ind = MiscUtilities.spaces(indent);
        String in3 = MiscUtilities.spaces(3);

        out.print(ind + "probability of " + dumpObservable());
        if (getDiscretization() != null) {
            out.println();
            out.println(ind + in3 + " discretized as");
            ((org.integratedmodelling.thinklab.common.classification.Classification) getDiscretization())
                    .dumpIndented(out, indent + 6);
        }
    }

    @Override
    public IStateAccessor getMediator(IObserver observer, IMonitor monitor) throws ThinklabException {

        IAccessor ret = getUserDefinedAccessor(monitor);
        if (ret != null)
            return (IStateAccessor) ret;

        return new ProbabilityAccessor(getActions(), monitor, false);
    }

    @Override
    public boolean canInterpretDirectly(IAccessor accessor) {
        return getActions().size() == 0 && _discretization == null;
    }

    @Override
    public IStateAccessor getInterpreter(IStateAccessor accessor, IMonitor monitor) {
        return new ProbabilityAccessor(getActions(), monitor, true);
    }

    @Override
    public String getSignature() {
        return "#probability#" + (_discretization == null ? "" : _discretization.getConceptSpace());
    }

    @Override
    public IConcept getObservationType(INamespace namespace) {
        return Thinklab.c(NS.PROBABILITY);
    }

    @Override
    public String toString() {
        return "[probability " + (_inherentSubjectType == null ? " " : (" of" + _inherentSubjectType + " "))
                + "]";
    }

    @Override
    public double getMinimumValue() {
        return 0;
    }

    @Override
    public double getMaximumValue() {
        return 1.0;
    }

    @Override
    public void setIndirect(boolean b) {
        _isIndirect = b;
    }
}
