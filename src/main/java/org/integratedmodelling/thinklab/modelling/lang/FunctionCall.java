package org.integratedmodelling.thinklab.modelling.lang;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.annotations.Concept;
import org.integratedmodelling.thinklab.api.annotations.Property;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IExpression;
import org.integratedmodelling.thinklab.api.lang.IModelResolver;
import org.integratedmodelling.thinklab.api.lang.IPrototype;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.listeners.IMonitorable;
import org.integratedmodelling.thinklab.api.modelling.parsing.IFunctionCall;
import org.integratedmodelling.thinklab.api.project.IProject;
import org.integratedmodelling.thinklab.common.utils.MiscUtilities;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.modelling.ModelManager;

@Concept(NS.FUNCTION_DEFINITION)
public class FunctionCall extends ModelObject<FunctionCall> implements IFunctionCall {

    @Property(NS.HAS_TYPE)
    String                  _type;
    @Property(NS.HAS_PARAMETERS)
    HashMap<String, Object> _parameters;
    @Property(NS.HAS_PROJECT_ID)
    String                  _projectId;

    IProject                _project;
    IMonitor                _monitor;

    @Override
    public void set(String type, Map<String, Object> parms) {
        _type = type;
        _parameters = new HashMap<String, Object>();
        if (parms != null)
            _parameters.putAll(parms);
    }

    @Override
    public FunctionCall demote() {
        return this;
    }

    @Override
    public Map<String, Object> getParameters() {
        return _parameters;
    }

    @Override
    public String getId() {
        return _type;
    }

    public void setProject(IProject project) {
        _project = project;
        _projectId = project.getId();
    }

    @Override
    public Object call(IConcept... context) throws ThinklabException {

        ModelManager mm = (ModelManager) Thinklab.get().getModelManager();
        IExpression exp = mm.getExpressionForFunctionCall(this);
        Object ret = null;

        /*
         * FIXME get the prototype and validate the parameters
         */

        /*
         * evaluate parameters if necessary
         */
        HashMap<String, Object> parms = new HashMap<String, Object>();
        if (_parameters != null) {
            for (String s : _parameters.keySet()) {
                Object val = _parameters.get(s);
                if (val instanceof FunctionCall) {
                    val = ((FunctionCall) val).call(context);
                }
                parms.put(s, val);
            }
        }

        if (exp != null) {
            exp.setProjectContext(_project);
            ret = exp.eval(parms, context);
        }

        if (ret instanceof IMonitorable) {
            ((IMonitorable) ret).setMonitor(_monitor);
        }

        return ret;
    }

    @Override
    protected void dumpIndented(PrintStream out, int indent) {

        out.print(MiscUtilities.spaces(indent) + _type + "(");

        int i = 0;
        for (String s : _parameters.keySet()) {

            if (!s.equals("__default"))
                out.print(s + " = ");
            out.print(ModelObject.dumpLiteral(_parameters.get(s)));
            out.print(i < (_parameters.size() - 1) ? ", " : "");
            i++;
        }
        out.print(")");

    }

    @Override
    public IPrototype getPrototype() {
        return Thinklab.get().getFunctionPrototype(getId());
    }

    /*
     * Defines the project when the thing comes out of a kbox. 
     * (non-Javadoc)
     * @see org.integratedmodelling.thinklab.modelling.lang.LanguageElement#initialize()
     */
    public void initializeFromStorage() {

        super.initializeFromStorage();

        if (_project == null && _projectId != null) {
            _project = Thinklab.get().getProject(_projectId);
        }
    }

    @Override
    public void initialize(IModelResolver resolver) {
        // TODO Auto-generated method stub

    }

}
