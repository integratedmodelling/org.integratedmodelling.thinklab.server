package org.integratedmodelling.thinklab.modelling.lang.datasources;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.integratedmodelling.collections.NumericInterval;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.metadata.IMetadata;
import org.integratedmodelling.thinklab.api.modelling.IAccessor;
import org.integratedmodelling.thinklab.api.modelling.IAction;
import org.integratedmodelling.thinklab.api.modelling.IDataSource;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.IScale;
import org.integratedmodelling.thinklab.modelling.lang.Metadata;
import org.integratedmodelling.thinklab.modelling.lang.Scale;
import org.integratedmodelling.thinklab.modelling.lang.StateAccessor;

/**
 * A datasource that returns the same object no matter what. Also works as an accessor so it can be parameterized
 * appropriately.
 * 
 * @author Ferd
 *
 */
public class RandomSelectDSAccessor extends StateAccessor implements IDataSource {

    //    private DistributionValue _state = null;
    private IMetadata _metadata = new Metadata();
    private Object[] _values;
    private Object[] _distribution;
    Random _rand = new Random();

    boolean isAccessor = false;

    public RandomSelectDSAccessor(List<IAction> actions, IMonitor monitor, Object[] values, Object[] dist) {
        super(actions, monitor);
        _values = values;
        _distribution = dist;
        isAccessor = true;
    }

    public RandomSelectDSAccessor(Object[] values, Object[] dist) {
        super(new ArrayList<IAction>(), null);
        _values = values;
        _distribution = dist;
    }

    @Override
    public void process(int stateIndex) throws ThinklabException {

        for (String s : getOutputKeys()) {
            setValue(s, draw());
        }
        super.process(stateIndex);
    }

    @Override
    public Object getValue(String outputKey) {
        return _parameters.containsKey(outputKey) ? _parameters.get(outputKey) : draw();
    }

    private Object draw() {

        double[] dd = new double[_values.length];
        double sum = 0.0;

        for (int i = 0; i < _values.length; i++) {
            double d = 0.0;
            if (!(_distribution[i] instanceof Number)) {
                Object o = this._parameters.get(_distribution[i].toString());
                if (!(o instanceof Number)) {
                    RuntimeException e = new ThinklabRuntimeException("cannot compute value of "
                            + _distribution[i] + ": " + o + " is not a number");
                    if (_monitor != null) {
                        _monitor.error(e);
                    }
                    throw e;
                }
                d = ((Number) o).doubleValue();
                if (Double.isNaN(d)) {
                    return null;
                }
            } else {
                d = ((Number) _distribution[i]).doubleValue();
            }

            sum += d;
            dd[i] = sum;
        }

        double thr = _rand.nextDouble() * sum;
        for (int i = 0; i < _values.length; i++) {
            if (thr <= dd[i]) {
                return processValue(_values[i]);
            }
        }

        return null;
    }

    private Object processValue(Object object) {

        if (object instanceof NumericInterval) {
            // choose randomly
            NumericInterval ni = (NumericInterval) object;
            object = ni.getMinimumValue()
                    + (_rand.nextDouble() * (ni.getMaximumValue() - ni.getMinimumValue()));
        }
        return object;
    }

    @Override
    public IAccessor getAccessor(IScale context, IObserver observer, IMonitor monitor)
            throws ThinklabException {
        return this;
    }

    @Override
    public IMetadata getMetadata() {
        return _metadata;
    }

    @Override
    public IScale getCoverage() {
        return new Scale();
    }

    public String toString() {
        /*
         * FIXME do better
         */
        return "[random.select]";
    }
}
