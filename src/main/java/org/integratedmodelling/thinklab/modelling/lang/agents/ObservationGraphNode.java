package org.integratedmodelling.thinklab.modelling.lang.agents;

import java.util.Collection;
import java.util.HashSet;

import org.integratedmodelling.thinklab.api.modelling.agents.IAgentState;
import org.integratedmodelling.thinklab.api.modelling.agents.IObservationGraphNode;
import org.integratedmodelling.thinklab.api.modelling.agents.IObservationTask;
import org.integratedmodelling.thinklab.api.modelling.agents.IScaleMediator;
import org.integratedmodelling.thinklab.api.modelling.scheduling.ITransition;

public class ObservationGraphNode implements IObservationGraphNode {

    private final IAgentState state;
    private final IObservationTask taskCreatingThisNodeState;
    private final ITransition transitionFromPreviousNodeState;
    private Collection<IScaleMediator> subscribers; // ScaleMediators which have actually READ this node's state

    // TODO think of how to clean this up over time. Once processing proceeds BEYOND the end time of an agent-state,
    // then the causal links from that agent-state could be deleted
    private final HashSet<IObservationTask> tasksCausedByThisNodeState = new HashSet<IObservationTask>();

    public ObservationGraphNode(IAgentState initialState, IObservationTask taskCreatingThisNodeState) {
        this(initialState, taskCreatingThisNodeState, null);
    }

    public ObservationGraphNode(IAgentState initialState, IObservationTask taskCreatingThisNodeState,
            ITransition transitionFromPreviousNodeState) {
        this.state = initialState;
        this.taskCreatingThisNodeState = taskCreatingThisNodeState;
        this.transitionFromPreviousNodeState = transitionFromPreviousNodeState;
    }

    @Override
    public IAgentState getAgentState() {
        return state;
    }

    @Override
    public ITransition getTransitionFromPrevious() {
        return transitionFromPreviousNodeState;
    }

    @Override
    public IObservationTask getTaskCreatingThisNodeState() {
        return taskCreatingThisNodeState;
    }

    @Override
    public void addTaskCausedByThisNodeState(IObservationTask task) {
        tasksCausedByThisNodeState.add(task);
    }

    @Override
    public Collection<IObservationTask> getTasksCausedByThisNodeState() {
        return tasksCausedByThisNodeState;
    }

    @Override
    public Collection<IScaleMediator> getSubscribers() {
        return subscribers;
    }

    @Override
    public String toString() {
        return state.toString();
    }

    @Override
    public boolean canCollideWithAnything() {
        return true;
    }
}
