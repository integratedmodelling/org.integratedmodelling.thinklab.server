package org.integratedmodelling.thinklab.modelling.lang.agents;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.modelling.IActiveSubject;
import org.integratedmodelling.thinklab.api.modelling.agents.ICollision;
import org.integratedmodelling.thinklab.api.modelling.agents.IObservationController;
import org.integratedmodelling.thinklab.api.modelling.agents.IObservationGraphNode;
import org.integratedmodelling.thinklab.api.modelling.scheduling.ITransition;
import org.integratedmodelling.thinklab.api.time.ITimeInstant;
import org.integratedmodelling.thinklab.debug.CallTracer;

public class ObservationTaskCollisionDetection extends ObservationTask {
    // default generated ID
    private static final long serialVersionUID = 1L;
    private final IObservationGraphNode node1;
    private final IObservationGraphNode node2;

    public ObservationTaskCollisionDetection(ITimeInstant observationTime, IObservationGraphNode node1,
            IObservationGraphNode node2, IObservationGraphNode parentNode, IObservationController controller) {
        // null SubjectObserver because it's actually not used during collision detection (Subjects do it directly)
        super(null, observationTime, parentNode, controller);
        this.node1 = node1;
        this.node2 = node2;
    }

    /**
     * NOTE: overriding run() also prevents NPE in superclass (where subjectObserver == null)
     */
    @Override
    public ITransition run() throws ThinklabException {
        CallTracer.indent("run()", this, node1, node2);
        // Necessary steps:
        // get the state distribution function for each agent which is valid @ observationTime
        // ask each agent to determine whether collision will happen
        ICollision collision1 = ((IActiveSubject) (node1.getAgentState().getSubject())).detectCollision(
                node1, node2);
        ICollision collision2 = ((IActiveSubject) (node1.getAgentState().getSubject())).detectCollision(
                node2, node1);
        // take the first-occurring collision (if any), because the earlier collision might influence the subsequent agent state(s)
        // and therefore the later collision would have to be re-computed anyway.
        ICollision collision;
        if (collision1 == null) {
            collision = collision2;
        } else if (collision2 == null) {
            collision = collision1;
        } else if (collision1.getCollisionTime().compareTo(collision1.getCollisionTime()) < 0) {
            // collision 1 happens earlier
            collision = collision1;
        } else {
            // collision 2 happens earlier
            collision = collision2;
        }

        if (collision != null) {
            controller.collide(node1, node2, collision);
        }

        CallTracer.unIndent();
        return null;
    }

    /**
     * Override this to prevent NPE in superclass (where subjectObserver == null)
     */
    @Override
    public String toString() {
        return getClass().getSimpleName() + " for " + node1 + " and " + node2 + " at " + observationTime;
    }
}
