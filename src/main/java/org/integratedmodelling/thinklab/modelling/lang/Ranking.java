package org.integratedmodelling.thinklab.modelling.lang;

import java.io.PrintStream;
import java.util.List;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.lang.RankingScale;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.lang.IModelResolver;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.modelling.IAccessor;
import org.integratedmodelling.thinklab.api.modelling.IAction;
import org.integratedmodelling.thinklab.api.modelling.IClassification;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.IStateAccessor;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.api.modelling.parsing.IClassificationDefinition;
import org.integratedmodelling.thinklab.api.modelling.parsing.INamespaceDefinition;
import org.integratedmodelling.thinklab.api.modelling.parsing.IRankingObserverDefinition;
import org.integratedmodelling.thinklab.common.data.IndexedCategoricalDistribution;
import org.integratedmodelling.thinklab.common.utils.MiscUtilities;
import org.integratedmodelling.thinklab.common.vocabulary.NS;

public class Ranking extends Observer<Ranking> implements IRankingObserverDefinition {

    final String[]  statements = { "rank", "encode", "select", "ratio", "proportion", "percentage" };
    RankingScale    _scale     = new RankingScale();
    IClassification _discretization;

    @Override
    public RankingScale getRankingScale() {
        return _scale;
    }

    @Override
    public void setScale(Number from, Number to) {
        _scale = new RankingScale(from, to);
    }

    @Override
    public Ranking demote() {
        return this;
    }

    @Override
    public void notifyModelObservable(IObservable observable, IModelResolver resolver) {

        if (_discretization != null) {
            ((INamespaceDefinition) getNamespace()).synchronizeKnowledge(null);
            ((IClassificationDefinition) _discretization).setConceptSpace(observable, resolver);
            ((IClassificationDefinition) _discretization).initialize();
            if (!_discretization.isContiguousAndFinite()) {
                addError(new ThinklabValidationException(
                        "discretization has discontinuous and/or unbounded intervals"),
                        _discretization.getFirstLineNumber());
            }
        }

    }

    /*
     * -----------------------------------------------------------------------------------
     * accessor - it's always a mediator, either to another measurement or to a datasource
     * whose content was defined explicitly to conform to our semantics
     * -----------------------------------------------------------------------------------
     */
    public class RankingAccessor extends MediatingAccessor {

        boolean _errorsPresent = false;

        public RankingAccessor(List<IAction> actions, IMonitor monitor, boolean interpreting) {
            super(actions, monitor, interpreting);
            if (_discretization != null)
                ((org.integratedmodelling.thinklab.common.classification.Classification) _discretization)
                        .reset();

        }

        @Override
        public String toString() {
            return "[ranking " + _scale + "]";
        }

        public RankingScale getRankingScale() {
            return _scale;
        }

        @Override
        public Object mediate(Object object) {

            double val = Double.NaN;

            if (object == null || (object instanceof Number && Double.isNaN(((Number) object).doubleValue())))
                return val;

            if (object instanceof IndexedCategoricalDistribution) {

                /*
                 * TODO try to mediate distributions
                 */
                val = ((IndexedCategoricalDistribution) object).getMean();
            } else if (object instanceof Number) {
                val = ((Number) object).doubleValue();
            } else {
                try {
                    val = object.toString().equals("NaN") ? Double.NaN : Double
                            .parseDouble(object.toString());
                } catch (Exception e) {
                    if (!_errorsPresent) {
                        _monitor.error("cannot interpret value: " + object + " as a number");
                        _errorsPresent = true;
                    }
                    return Double.NaN;
                }
            }
            
            if (getMediatedObserver() == null)
            	return val;

            return _scale.convert(val, ((Ranking) (getMediatedObserver()))._scale);
        }
    }

    @Override
    public void setDiscretization(IClassification classification) {
        _discretization = classification;
    }

    @Override
    public IClassification getDiscretization() {
        return _discretization;
    }

    @Override
    public boolean isDiscrete() {
        return _discretization != null;
    }

    @Override
    protected void dumpIndented(PrintStream out, int indent) {

        String ind = MiscUtilities.spaces(indent);
        String in3 = MiscUtilities.spaces(3);

        out.print(ind + " ranking " + dumpObservable());
        if (getDiscretization() != null) {
            out.println();
            out.println(ind + in3 + "discretized as");
            ((org.integratedmodelling.thinklab.common.classification.Classification) getDiscretization())
                    .dumpIndented(out, indent + 6);
        }
    }

    @Override
    public IStateAccessor getMediator(IObserver observer, IMonitor monitor) throws ThinklabException {

        IAccessor ret = getUserDefinedAccessor(monitor);
        if (ret != null)
            return (IStateAccessor) ret;

        return new RankingAccessor(getActions(), monitor, false);
    }

    @Override
    public boolean canInterpretDirectly(IAccessor accessor) {
        return getActions().size() == 0 && _discretization == null;
    }

    @Override
    public IStateAccessor getInterpreter(IStateAccessor accessor, IMonitor monitor) {
        return new RankingAccessor(getActions(), monitor, true);
    }

    @Override
    public String getSignature() {
        return "#ranking#" + (_scale == null ? "" : _scale)
                + (_discretization == null ? "" : _discretization.getConceptSpace());
    }

    @Override
    public IConcept getObservationType(INamespace namespace) {
        return Thinklab.c(NS.RANKING);
    }

    @Override
    public String toString() {
        return "[rank " + _observable.getType()
                + (_inherentSubjectType == null ? " " : ("of" + _inherentSubjectType + " ")) + "]";
    }

    @Override
    public double getMinimumValue() {
        return Double.NaN;
    }

    @Override
    public double getMaximumValue() {
        return Double.NaN;
    }

}
