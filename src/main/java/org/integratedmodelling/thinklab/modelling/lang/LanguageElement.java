package org.integratedmodelling.thinklab.modelling.lang;

import java.io.PrintStream;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.annotation.SemanticObject;
import org.integratedmodelling.thinklab.api.lang.IModelResolver;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.listeners.IMonitorable;
import org.integratedmodelling.thinklab.api.modelling.parsing.ILanguageDefinition;

public abstract class LanguageElement<T> extends SemanticObject<T> implements ILanguageDefinition,
        IMonitorable {

    int      _lastLineNumber  = 0;
    int      _firstLineNumber = 0;

    IMonitor _monitor;

    public abstract void dump(PrintStream out);

    @Override
    public void setLineNumbers(int startLine, int endLine) {
        _firstLineNumber = startLine;
        _lastLineNumber = endLine;
    }

    @Override
    public int getFirstLineNumber() {
        return _firstLineNumber;
    }

    @Override
    public int getLastLineNumber() {
        return _lastLineNumber;
    }

    // public void initialize() throws ThinklabException {
    // }

    public void validate(IModelResolver resolver) throws ThinklabException {
    }

    @Override
    public void setMonitor(IMonitor monitor) {
        _monitor = monitor;
    }

    @Override
    public IMonitor getMonitor() {
        return _monitor;
    }

}
