package org.integratedmodelling.thinklab.modelling.lang.datasources;

import java.util.ArrayList;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.thinklab.api.data.IAggregator;
import org.integratedmodelling.thinklab.api.data.IColumn;
import org.integratedmodelling.thinklab.api.data.ITable;
import org.integratedmodelling.thinklab.api.knowledge.IExpression;
import org.integratedmodelling.thinklab.api.modelling.IAction;
import org.integratedmodelling.thinklab.modelling.lang.StateAccessor;

public class DatatableAccessor extends StateAccessor {

    ITable _table;
    IExpression _rowSelector;
    IAggregator _operation;
    IColumn _column;
    Object _value;

    public DatatableAccessor(ArrayList<IAction> actions, ITable table, IExpression rowSelector,
            IAggregator operation, String column) throws ThinklabValidationException {

        super(actions, null);
        _table = table;
        _column = table.getColumn(column);
        _rowSelector = rowSelector;
        _operation = operation;
        if (_column == null) {
            throw new ThinklabValidationException("table column " + column + " does not exist");
        }
    }

    @Override
    public void process(int stateIndex) throws ThinklabException {

        /*
         * scan rows; if there is a selector, filter them through it; keep IDs of valid rows.
         * Use copy of _parameters plus all values of columns at each point.
         */
        if (_rowSelector == null) {
            // aggregation of all rows. Table is read-only so we only need to do this
            // once.
            if (_value == null) {
                _value = _operation.aggregate(_column.getValues());
            }
        } else {
            /*
             * apply aggregator to rows that match the filter.
             */
            if (_value == null || _parameters.size() > 0) {
                _value = _operation.aggregate(_table.lookup(_column.getName(), _rowSelector, _parameters));
            }
        }

        /*
         * set value to resulting aggregation
         */
        for (String s : getOutputKeys()) {
            setValue(s, _value);
        }

        super.process(stateIndex);
    }

}
