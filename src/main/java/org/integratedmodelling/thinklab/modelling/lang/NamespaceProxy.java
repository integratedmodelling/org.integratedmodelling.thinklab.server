//package org.integratedmodelling.thinklab.modelling.lang;
//
//import java.io.File;
//import java.util.Collection;
//import java.util.List;
//import java.util.Map;
//
//import org.integratedmodelling.collections.Pair;
//import org.integratedmodelling.collections.Triple;
//import org.integratedmodelling.exceptions.ThinklabException;
//import org.integratedmodelling.interpreter.ModelGenerator;
//import org.integratedmodelling.thinklab.Thinklab;
//import org.integratedmodelling.thinklab.api.knowledge.IAxiom;
//import org.integratedmodelling.thinklab.api.knowledge.IConcept;
//import org.integratedmodelling.thinklab.api.knowledge.IOntology;
//import org.integratedmodelling.thinklab.api.knowledge.IProperty;
//import org.integratedmodelling.thinklab.api.lang.IModelParser;
//import org.integratedmodelling.thinklab.api.lang.IModelResolver;
//import org.integratedmodelling.thinklab.api.listeners.IMonitor;
//import org.integratedmodelling.thinklab.api.metadata.IMetadata;
//import org.integratedmodelling.thinklab.api.modelling.IModelObject;
//import org.integratedmodelling.thinklab.api.modelling.IScale;
//import org.integratedmodelling.thinklab.api.modelling.parsing.IFunctionCall;
//import org.integratedmodelling.thinklab.api.modelling.parsing.IModelObjectDefinition;
//import org.integratedmodelling.thinklab.api.modelling.parsing.INamespaceDefinition;
//import org.integratedmodelling.thinklab.api.project.IProject;
//import org.integratedmodelling.thinklab.proxy.ModellingModule;
//
//import com.google.inject.Guice;
//import com.google.inject.Injector;
//
///**
// * A proxy object (wrapper) for a Namespace. This object should be generated during .tql file reading to
// * speed up Thinklab boot time. This Namespace object will not actually do any parsing until the information
// * is actually required. This is accomplished by setting a parser class element and calling parse() before any
// * operation that requires the file to be parsed.
// *
// * @author Luke
// *
// */
//public class NamespaceProxy extends Namespace {
//
//    private static Boolean       PROXY_THINGER_IS_ENABLED = null;
//
//    private INamespaceDefinition actual                   = null;
//    private final IModelResolver resolver;
//
//    public NamespaceProxy(String namespaceId, File file, IModelResolver resolver, IMonitor monitor) {
//        this.resolver = resolver;
//
//        this.setId(namespaceId);
//        this.setLocalFile(file);
//        this.setTimeStamp(file.lastModified());
//        this.setMonitor(monitor);
//
//        if (PROXY_THINGER_IS_ENABLED == null) {
//            PROXY_THINGER_IS_ENABLED = Boolean.parseBoolean(Thinklab.get().getProperties()
//                    .getProperty("lazy.namespace.loading", "false"));
//        }
//
//        if (!PROXY_THINGER_IS_ENABLED) {
//            parse();
//        }
//    }
//
//    /**
//     * The heart of the operation. This allows loading to happen on demand, rather than at boot time. This
//     * should be called before any operation that will read to or write from the actual Namespace, expecting
//     * that the file has already been parsed.
//     *
//     * This was shamelessly copied & pasted from ModelManager.java (in thinklab/)
//     */
//    private void parse() {
//        if (actual == null) {
//            Injector injector = Guice.createInjector(new ModellingModule());
//            IModelParser thinkqlParser = injector.getInstance(ModelGenerator.class);
//
//            try {
//                actual = (INamespaceDefinition) thinkqlParser.parse(getId(), _localFile.toString(), resolver,
//                        _monitor);
//                actual.setTimeStamp(_localFile.lastModified());
//                actual.setLocalFile(_localFile);
//            } catch (ThinklabException e) {
//                // TODO this could be really painful. It prevents any parsing errors from being detected
//                // automatically.
//                e.printStackTrace();
//            }
//        }
//    }
//
//    // ************ The wrapped methods ************
//    @Override
//    public Map<String, Object> getSymbolTable() {
//        parse();
//        return actual.getSymbolTable();
//    }
//
//    @Override
//    public IMetadata getMetadata() {
//        parse();
//        return actual.getMetadata();
//    }
//
//    /**
//     * Exec all axioms accumulated so far to actualize gathered knowledge.
//     * @throws ThinklabException
//     */
//    @Override
//    public void synchronizeKnowledge(IModelResolver resolver) {
//        parse();
//        actual.synchronizeKnowledge(resolver);
//    }
//
//    @Override
//    public List<IModelObject> getModelObjects() {
//        parse();
//        return actual.getModelObjects();
//    }
//
//    @Override
//    public void setOntology(IOntology ontology) {
//        parse();
//        actual.setOntology(ontology);
//    }
//
//    @Override
//    public IOntology getOntology() {
//        parse();
//        return actual.getOntology();
//    }
//
//    @Override
//    public void addAxiom(IAxiom axiom) {
//        parse();
//        actual.addAxiom(axiom);
//    }
//
//    @Override
//    public void setResourceUrl(String resourceUrl) {
//        parse();
//        actual.setResourceUrl(resourceUrl);
//    }
//
//    @Override
//    public void addModelObject(IModelObjectDefinition modelObject) {
//        parse();
//        actual.addModelObject(modelObject);
//    }
//
//    @Override
//    public void setProject(IProject project) {
//        parse();
//        actual.setProject(project);
//    }
//
//    @Override
//    public IConcept getConcept(String s) {
//        parse();
//        return actual.getConcept(s);
//    }
//
//    @Override
//    public IProperty getProperty(String s) {
//        parse();
//        return actual.getProperty(s);
//    }
//
//    @Override
//    public IProject getProject() {
//        parse();
//        return actual.getProject();
//    }
//
//    @Override
//    public IModelObject getModelObject(String mod) {
//        parse();
//        return actual.getModelObject(mod);
//    }
//
//    @Override
//    public String getResourceUrl() {
//        parse();
//        return actual.getResourceUrl();
//    }
//
//    @Override
//    public int getFirstLineNumber() {
//        parse();
//        return actual.getFirstLineNumber();
//    }
//
//    @Override
//    public int getLastLineNumber() {
//        parse();
//        return actual.getLastLineNumber();
//    }
//
//    @Override
//    public void setLineNumbers(int startLine, int endLine) {
//        parse();
//        actual.setLineNumbers(startLine, endLine);
//    }
//
//    @Override
//    public void setExpressionLanguage(String language) {
//        parse();
//        actual.setExpressionLanguage(language);
//    }
//
//    @Override
//    public String getExpressionLanguage() {
//        parse();
//        return actual.getExpressionLanguage();
//    }
//
//    @Override
//    public IScale getCoverage() {
//        parse();
//        return actual.getCoverage();
//    }
//
//    @Override
//    public boolean isInternal() {
//        parse();
//        return actual.isInternal();
//    }
//
//    @Override
//    public void setInternal(boolean b) {
//        parse();
//        actual.setInternal(b);
//    }
//
//    @Override
//    public void addCoveredExtent(IFunctionCall extent, IModelResolver resolver, int lineNumber) {
//        parse();
//        actual.addCoveredExtent(extent, resolver, lineNumber);
//    }
//
//    @Override
//    public void addWarning(String warning, int lineNumber) {
//        parse();
//        actual.addWarning(warning, lineNumber);
//    }
//
//    @Override
//    public void addError(int errorCode, String errorMessage, int lineNumber) {
//        parse();
//        actual.addError(errorCode, errorMessage, lineNumber);
//    }
//
//    @Override
//    public boolean hasErrors() {
//        parse();
//        return actual.hasErrors();
//    }
//
//    @Override
//    public boolean hasWarnings() {
//        parse();
//        return actual.hasWarnings();
//    }
//
//    @Override
//    public Collection<Triple<Integer, String, Integer>> getErrors() {
//        parse();
//        return actual.getErrors();
//    }
//
//    @Override
//    public Collection<Pair<String, Integer>> getWarnings() {
//        parse();
//        return actual.getWarnings();
//    }
//
//    @Override
//    public List<String> getTrainingNamespaces() {
//        parse();
//        return actual.getTrainingNamespaces();
//    }
//
//    @Override
//    public List<String> getLookupNamespaces() {
//        parse();
//        return actual.getLookupNamespaces();
//    }
//
//    @Override
//    public void addLookupNamespace(String tns) {
//        parse();
//        actual.addLookupNamespace(tns);
//    }
//
//    @Override
//    public void addTrainingNamespace(String tns) {
//        parse();
//        actual.addTrainingNamespace(tns);
//    }
//
//    @Override
//    public boolean isScenario() {
//        parse();
//        return actual.isScenario();
//    }
//
//    @Override
//    public void setScenario(boolean isScenario) {
//        parse();
//        actual.setScenario(isScenario);
//    }
//
//    @Override
//    public IMetadata getResolutionCriteria() {
//        parse();
//        return actual.getResolutionCriteria();
//    }
//
//    @Override
//    public void setResolutionCriteria(IMetadata metadata) {
//        parse();
//        actual.setResolutionCriteria(metadata);
//    }
//
//    @Override
//    public Collection<String> getDisjointNamespaces() {
//        parse();
//        return actual.getDisjointNamespaces();
//    }
//
//    @Override
//    public void addDisjointNamespace(String dn) {
//        parse();
//        actual.addDisjointNamespace(dn);
//    }
//
//    // @Override
//    // public void addImportedNamespace(String namespace) {
//    // parse();
//    // actual.addImportedNamespace(namespace);
//    // }
//    //
//    // @Override
//    // public Collection<INamespace> getImportedNamespaces() {
//    // parse();
//    // return actual.getImportedNamespaces();
//    // }
//    //
//    // @Override
//    // public boolean equals(Object obj) {
//    // parse();
//    // return actual.equals(obj);
//    // }
//    //
//    // @Override
//    // public int hashCode() {
//    // parse();
//    // return actual.hashCode();
//    // }
//
//    @Override
//    public boolean isPrivate() {
//        parse();
//        return actual.isPrivate();
//    }
//
//    @Override
//    public void setPrivate(boolean isPrivate) {
//        parse();
//        actual.setPrivate(isPrivate);
//    }
// }
