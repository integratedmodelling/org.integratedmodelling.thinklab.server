package org.integratedmodelling.thinklab.modelling.lang.agents;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.modelling.agents.IObservationController;
import org.integratedmodelling.thinklab.api.modelling.agents.IObservationTask;
import org.integratedmodelling.thinklab.api.modelling.agents.IObservationWorker;
import org.integratedmodelling.thinklab.api.modelling.scheduling.ITransition;
import org.integratedmodelling.thinklab.common.monitoring.Notification;
import org.integratedmodelling.thinklab.debug.CallTracer;

/**
 * A worker node which performs the tasks of observing agent-state intervals.
 *
 * Agents progress through time in a linear fashion, and their dependencies all point into the immediate past.
 * This will generate agent states for given time intervals based on these dependencies, which then allow
 * further observations to be made.
 *
 * @author luke
 *
 */
public class ObservationWorker implements IObservationWorker {

    private final IObservationController controller;
    private final IMonitor               monitor;
    private long                         contextId;

    public ObservationWorker(IObservationController controller, IMonitor monitor, long contextId) {
        this.controller = controller;
        this.monitor = monitor;
        this.contextId = contextId;
    }

    @Override
    public void run() throws ThinklabException {
        CallTracer.indent("run()", this);
        IObservationTask task = controller.getNext();
        while (task != null) {
            ITransition result = task.run();
            // null result will happen when ...well... nothing happens.
            // (like a collision detection task that doesn't detect anything)
            if (result != null) {
                controller.setResult(task, result);
                if (result.getAgentState() != null && result.getAgentState().getTimePeriod() != null) {
                    monitor.message(Notification.TRANSITION, contextId, monitor.getTaskId(),
                            result.getAgentState().getTimePeriod().getStart().getMillis(),
                            result.getAgentState().getTimePeriod().getEnd().getMillis());
                }
            }
            task = controller.getNext();
        }
        CallTracer.unIndent();
    }
}
