package org.integratedmodelling.thinklab.modelling.lang.agents;

import java.util.Collection;

import org.integratedmodelling.thinklab.api.annotations.SubjectType;
import org.integratedmodelling.thinklab.api.modelling.IExtent;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.common.vocabulary.NS;

/**
 * An Agent which can reason about itself and its environment (rather than simply reacting or applying rules,
 * as a Bayesian or Reactive agent would)
 * 
 * TODO what behaviors? A deliberative agent must be able to reason "through time", i.e. maintain state and
 * use history as a guide
 * 
 * @author luke
 * 
 */
@SubjectType(NS.DELIBERATIVE_AGENT)
public class AgentDeliberative extends Agent {

    public AgentDeliberative(IObservable type, Collection<IExtent> extents, Object object,
            INamespace namespace, String name) {
        super(type, extents, object, namespace, name);
        // TODO Auto-generated constructor stub
    }
}
