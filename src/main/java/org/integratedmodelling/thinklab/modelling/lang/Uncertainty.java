package org.integratedmodelling.thinklab.modelling.lang;

import java.io.PrintStream;
import java.util.List;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.lang.RankingScale;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.lang.IModelResolver;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.modelling.IAccessor;
import org.integratedmodelling.thinklab.api.modelling.IAction;
import org.integratedmodelling.thinklab.api.modelling.IClassification;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.IStateAccessor;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.api.modelling.parsing.IClassificationDefinition;
import org.integratedmodelling.thinklab.api.modelling.parsing.INamespaceDefinition;
import org.integratedmodelling.thinklab.api.modelling.parsing.IUncertaintyObserverDefinition;
import org.integratedmodelling.thinklab.common.configuration.Env;
import org.integratedmodelling.thinklab.common.data.IndexedCategoricalDistribution;
import org.integratedmodelling.thinklab.common.utils.MiscUtilities;
import org.integratedmodelling.thinklab.common.vocabulary.NS;

public class Uncertainty extends Observer<Uncertainty> implements IUncertaintyObserverDefinition {

    RankingScale    _scale      = new RankingScale();
    IClassification _discretization;
    boolean         _isIndirect = false;

    @Override
    public Uncertainty demote() {
        return this;
    }

    @Override
    public IConcept getObservedType(IConcept concept) {

        IConcept ret = concept;

        if (_isIndirect) {
            ret = NS.makeUncertainty(concept);
            if (ret == null) {
                addError(new ThinklabValidationException(concept
                        + ": uncertainties are assigned to observables. Use the direct form (without 'of') for observables that are already probabilities."),
                        _firstLineNumber);
                return concept;
            }
        } else if (!concept.is(Env.c(NS.UNCERTAINTY))) {
            addError(new ThinklabValidationException(concept
                    + ": the observable in this statement must be an uncertainty. Use the indirect form (with 'of') for the uncertainty of another observable."),
                    _firstLineNumber);
        }
        return ret;
    }

    @Override
    public void notifyModelObservable(IObservable observable, IModelResolver resolver) {

        if (_discretization != null) {
            ((INamespaceDefinition) getNamespace()).synchronizeKnowledge(null);
            ((IClassificationDefinition) _discretization).setConceptSpace(observable, resolver);
            ((IClassificationDefinition) _discretization).initialize();
            if (!_discretization.isContiguousAndFinite()) {
                addError(new ThinklabValidationException(
                        "discretization has discontinuous and/or unbounded intervals"),
                        _discretization.getFirstLineNumber());
            }
        }

    }

    /*
     * -----------------------------------------------------------------------------------
     * accessor - it's always a mediator, either to another measurement or to a datasource
     * whose content was defined explicitly to conform to our semantics
     * -----------------------------------------------------------------------------------
     */
    public class RankingAccessor extends MediatingAccessor {

        boolean _errorsPresent = false;

        public RankingAccessor(List<IAction> actions, IMonitor monitor, boolean interpreting) {
            super(actions, monitor, interpreting);
            if (_discretization != null)
                ((org.integratedmodelling.thinklab.common.classification.Classification) _discretization)
                        .reset();

        }

        @Override
        public String toString() {
            return "[uncertainty of " + _inherentSubjectType + "]";
        }

        public RankingScale getRankingScale() {
            return _scale;
        }

        @Override
        public Object mediate(Object object) {

            double val = Double.NaN;

            if (object == null || (object instanceof Number && Double.isNaN(((Number) object).doubleValue())))
                return val;

            if (object instanceof IndexedCategoricalDistribution) {

                /*
                 * TODO try to mediate distributions
                 */
                val = ((IndexedCategoricalDistribution) object).getMean();
            } else if (object instanceof Number) {
                val = ((Number) object).doubleValue();
            } else {
                try {
                    val = object.toString().equals("NaN") ? Double.NaN : Double
                            .parseDouble(object.toString());
                } catch (Exception e) {
                    if (!_errorsPresent) {
                        _monitor.error("cannot interpret value: " + object + " as a number");
                        _errorsPresent = true;
                    }
                    return Double.NaN;
                }
            }

            return _scale.convert(val, ((Uncertainty) (getMediatedObserver()))._scale);
        }
    }

    @Override
    public void setDiscretization(IClassification classification) {
        _discretization = classification;
    }

    @Override
    public IClassification getDiscretization() {
        return _discretization;
    }

    public boolean isDiscrete() {
        return _discretization != null;
    }

    @Override
    protected void dumpIndented(PrintStream out, int indent) {

        String ind = MiscUtilities.spaces(indent);
        String in3 = MiscUtilities.spaces(3);

        out.print(ind + "uncertainty of " + dumpObservable());
        if (getDiscretization() != null) {
            out.println();
            out.println(ind + in3 + " discretized as");
            ((org.integratedmodelling.thinklab.common.classification.Classification) getDiscretization())
                    .dumpIndented(out, indent + 6);
        }
    }

    @Override
    public IStateAccessor getMediator(IObserver observer, IMonitor monitor) throws ThinklabException {

        IAccessor ret = getUserDefinedAccessor(monitor);
        if (ret != null)
            return (IStateAccessor) ret;

        return new RankingAccessor(getActions(), monitor, false);
    }

    @Override
    public boolean canInterpretDirectly(IAccessor accessor) {
        return getActions().size() == 0 && _discretization == null;
    }

    @Override
    public IStateAccessor getInterpreter(IStateAccessor accessor, IMonitor monitor) {
        return new RankingAccessor(getActions(), monitor, true);
    }

    @Override
    public String getSignature() {
        return "#uncertainty#" + (_scale == null ? "" : _scale)
                + (_discretization == null ? "" : _discretization.getConceptSpace());
    }

    @Override
    public IConcept getObservationType(INamespace namespace) {
        return Thinklab.c(NS.UNCERTAINTY);
    }

    @Override
    public String toString() {
        return "[uncertainty " + (_inherentSubjectType == null ? " " : (" of" + _inherentSubjectType + " "))
                + "]";
    }

    @Override
    public double getMinimumValue() {
        return 0;
    }

    @Override
    public double getMaximumValue() {
        return Double.NaN;
    }

    @Override
    public void setIndirect(boolean b) {
        _isIndirect = b;
    }

}
