package org.integratedmodelling.thinklab.modelling.lang;

import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.annotations.Concept;
import org.integratedmodelling.thinklab.api.annotations.Property;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IExpression;
import org.integratedmodelling.thinklab.api.lang.IModelResolver;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.modelling.IAccessor;
import org.integratedmodelling.thinklab.api.modelling.IConditionalObserver;
import org.integratedmodelling.thinklab.api.modelling.IModel;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.IStateAccessor;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.api.modelling.parsing.IConceptDefinition;
import org.integratedmodelling.thinklab.api.modelling.parsing.IModelDefinition;
import org.integratedmodelling.thinklab.api.modelling.parsing.IModelObjectDefinition;
import org.integratedmodelling.thinklab.api.modelling.parsing.IObserverDefinition;
import org.integratedmodelling.thinklab.common.configuration.Env;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.common.vocabulary.Observable;

@Concept(NS.OBSERVER)
public abstract class Observer<T> extends ObservingObject<T> implements IObserverDefinition {

    @Property(NS.HAS_MEDIATED_OBSERVER)
    IModel                     _mediated          = null;

    @Property(NS.HAS_OBSERVABLE_CONCEPT)
    protected IConcept         _observableConcept = null;

    protected IModelDefinition _model;

    @Property(NS.HAS_OBSERVABLE)
    protected IObservable      _observable;

    @Override
    public boolean isIntegrated() {
        return _integrated;
    }

    @Override
    public void setId(String id) {
        super.setId(id);
        if (_mediated != null) {
            ((IModelObjectDefinition) _mediated).setId(id);
        }
    }

    @Override
    public IObserver getMediatedObserver() {
        return _mediated == null ? null : _mediated.getObserver();
    }

    @Override
    public IObserver train(ISubject context, IMonitor monitor) throws ThinklabException {
        return this;
    }

    @Override
    public void initialize(IModelResolver resolver) {

        if (_mediated != null) {

            /*
             * If we're mediating, we're reinterpreting the same concept under a different observation filter.
             */
            ((Model) _mediated).initialize(resolver);
            if (!_mediated.hasErrors()) {
                _observable = new Observable(_mediated.getObservable().getType(),
                        getObservationType(getNamespace()), _inherentSubjectType, _traitType, getId());
            }
        } else {
            _observable = new Observable(_observableConcept, getObservationType(getNamespace()),
                    _inherentSubjectType, _traitType, getId());
        }

        _observable = notifyObservable((Observable) _observable);

        alignDependencies(_observable, resolver);

    }

    protected IObservable notifyObservable(Observable observable) {
        return observable;
    }

    /*
     * return true if this observed doesn't change the semantics of the ultimate observer.
     */
    public boolean isNumericTransformation() {

        if (this instanceof Ranking || this instanceof Measurement || this instanceof Value) {
            return true;
        }

        if (this instanceof ConditionalObserver) {
            boolean ret = true;
            for (Pair<IModel, IExpression> o : ((ConditionalObserver) this).getModels()) {
                if (!((Observer<?>) (o.getFirst().getObserver())).isNumericTransformation()) {
                    ret = false;
                    break;
                }
            }
            return ret;
        }

        return false;
    }

    @Override
    public boolean hasAccessor() {
        return _accessorGenerator != null;
    }

    public boolean isDiscrete() {
        return false;
    }

    protected String dumpObservable() {

        if (_mediated != null) {
            return ((Observer<?>) _mediated).dump();
        }
        return getObservable().toString();
    }

    @Override
    public boolean canInterpretDirectly(IAccessor accessor) {
        return getActions().size() == 0;
    }

    @Override
    public IStateAccessor getComputingAccessor(IMonitor monitor) throws ThinklabException {
        IStateAccessor ret = (IStateAccessor) getUserDefinedAccessor(monitor);
        if (ret == null) {
            ret = new StateAccessor(this.getActions(), monitor);
        }
        return ret;
    }

    @Override
    public void addObservable(IModelDefinition mediated) {
        _mediated = mediated;
        _observable = new Observable(mediated.getObservable().getType(),
                getObservationType(getNamespace()), null, _traitType, null);
    }

    @Override
    public void addObservable(IConceptDefinition observable) {
        _observableConcept = getObservedType(Env.c(observable.getName()));
        _observable = new Observable(_observableConcept, getObservationType(getNamespace()), _inherentSubjectType, _traitType, null);
    }

    @Override
    public IConcept getObservedType(IConcept concept) {
        return concept;
    }

    @Override
    public IObservable getObservable() {
        return _observable;
    }

    @Override
    public void setModel(IModelDefinition model) {
        _model = model;
        if (_mediated != null) {
            ((Model) _mediated)._parent = _model;
        }
    }

    /**
     * This is to retrieve the model that provides the ultimate observable, so we can label datasets and
     * states appropriately.
     * 
     * @return
     */
    public IModel getTopLevelModel() {

        IModel ret = _model;
        while (((Model) ret)._parent != null) {
            ret = ((Model) ret)._parent;
        }

        return ret;
    }

    @Override
    public IModel getModel() {
        return _model;
    }

    //
    // public String getStateLabel() {
    //
    // return getObservableConcept().toString();
    //
    // // if (getModel() != null) {
    // //
    // // String ret = getModel().getId();
    // //
    // // return ret;
    // // }
    // // return CamelCase.toLowerCase(getObservableConcept().getLocalName(), '-');
    // }

    /**
     * utility to check observers in mediators - gives us the observer that has the semantics of the data,
     * being smart about conditional observers.
     * 
     * @param observer
     * @return
     */
    static public IObserver getRepresentativeObserver(IObserver observer) {
        return (observer instanceof IConditionalObserver) ? ((IConditionalObserver) observer)
                .getRepresentativeObserver() : observer;
    }

    @Override
    public void setTraitType(IConceptDefinition concept) {
        _traitType = Thinklab.c(concept.getName());
    }

}
