package org.integratedmodelling.thinklab.modelling.lang;

import java.io.PrintStream;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.annotations.Concept;
import org.integratedmodelling.thinklab.api.annotations.Property;
import org.integratedmodelling.thinklab.api.modelling.IExtent;
import org.integratedmodelling.thinklab.api.modelling.IUnit;
import org.integratedmodelling.thinklab.api.modelling.parsing.IUnitDefinition;
import org.integratedmodelling.thinklab.common.vocabulary.NS;

@Concept(NS.UNIT_DEFINITION)
public class UnitDefinition extends LanguageElement<UnitDefinition> implements IUnitDefinition {

    @Property(NS.HAS_EXPRESSION)
    String _expression;

    public UnitDefinition() {
    }

    public UnitDefinition(String unit) {
        _expression = unit;
    }

    @Override
    public void setExpression(String expression) {
        _expression = expression;
    }

    @Override
    public void dump(PrintStream out) {
        out.append(_expression);
    }

    @Override
    public UnitDefinition demote() {
        return this;
    }

    @Override
    public String getStringExpression() {
        return _expression;
    }

    @Override
    public double convert(double value, IUnit unit) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public boolean isRate() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public IUnit getTimeExtentUnit() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isLengthDensity() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public IUnit getLengthExtentUnit() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isArealDensity() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public IUnit getArealExtentUnit() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isVolumeDensity() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public IUnit getVolumeExtentUnit() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isUnitless() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isSpatialDensity(IExtent scale) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void parse(String string) throws ThinklabException {
        // TODO Auto-generated method stub

    }

    @Override
    public String asText() {
        // TODO Auto-generated method stub
        return null;
    }

}
