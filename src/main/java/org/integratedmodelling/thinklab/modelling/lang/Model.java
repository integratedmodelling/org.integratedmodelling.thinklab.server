package org.integratedmodelling.thinklab.modelling.lang;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.exceptions.ThinklabAuthorizationException;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.lang.Axiom;
import org.integratedmodelling.lang.LogicalConnector;
import org.integratedmodelling.lang.SemanticType;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.annotations.Property;
import org.integratedmodelling.thinklab.api.knowledge.IAxiom;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IExpression;
import org.integratedmodelling.thinklab.api.knowledge.IProperty;
import org.integratedmodelling.thinklab.api.knowledge.ISemanticObject;
import org.integratedmodelling.thinklab.api.lang.IModelResolver;
import org.integratedmodelling.thinklab.api.lang.IPrototype;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.listeners.IMonitorable;
import org.integratedmodelling.thinklab.api.metadata.IMetadata;
import org.integratedmodelling.thinklab.api.modelling.IAccessor;
import org.integratedmodelling.thinklab.api.modelling.IDataSource;
import org.integratedmodelling.thinklab.api.modelling.IExtent;
import org.integratedmodelling.thinklab.api.modelling.IModel;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.api.modelling.IObjectSource;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.IScale;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.api.modelling.parsing.IConceptDefinition;
import org.integratedmodelling.thinklab.api.modelling.parsing.IFunctionCall;
import org.integratedmodelling.thinklab.api.modelling.parsing.IModelDefinition;
import org.integratedmodelling.thinklab.api.modelling.parsing.INamespaceDefinition;
import org.integratedmodelling.thinklab.api.modelling.parsing.IObserverDefinition;
import org.integratedmodelling.thinklab.api.modelling.parsing.IPropertyDefinition;
import org.integratedmodelling.thinklab.common.configuration.Env;
import org.integratedmodelling.thinklab.common.utils.CamelCase;
import org.integratedmodelling.thinklab.common.utils.MiscUtilities;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.common.vocabulary.Observable;
import org.integratedmodelling.thinklab.interfaces.IStorageMetadataProvider;
import org.integratedmodelling.thinklab.modelling.lang.datasources.ConstantDataSource;
import org.integratedmodelling.thinklab.modelling.resolver.ModelData;
import org.integratedmodelling.thinklab.scripting.ModelFactory;

public class Model extends ObservingObject<Model> implements IModelDefinition, IStorageMetadataProvider {

    protected IObserver    _observer;
    ArrayList<IObservable> _observables        = new ArrayList<IObservable>();

    /*
     * The next only to reconstruct the observables after retrieval from DB. Having this as the model for the
     * first observable screws up the entifier otherwise.
     */
    ArrayList<IModel>      _obmodels           = new ArrayList<IModel>();
    IFunctionCall          _datasourceDefinition;
    Object                 _inlineState;
    ArrayList<String>      _trainingNamespaces = new ArrayList<String>();
    ArrayList<String>      _lookupNamespaces   = new ArrayList<String>();
    ArrayList<String>      _attributeNames     = new ArrayList<String>();
    IMetadata              _resolutionCriteria;
    ArrayList<IExtent>     _allowedCoverage    = new ArrayList<IExtent>();
    IDataSource            _datasource;

    /*
     * TODO this is so that observers of mediated models or conditionals can find the overall observable. Not
     * a good thing (leak potential) and should be substituted with a proper structural bookkeeping at the
     * namespace level.
     */
    IModel                 _parent             = null;

    private boolean        _initialized;
    private boolean        _merged             = false;
    private boolean        _isDependencyModel  = false;
    private boolean        _isInterpreting     = false;

    /*
     * data structure below support reification models. If the model is a reification model, the _datasource
     * will be a source of objects and the attribute table below is used to associate properties to each
     * object based on their attributes.
     */
    static public class AttributeTranslator {

        public String    attribute;
        public IModel    observer;
        public String    propertyId;

        public IProperty property;

        public void initializeFromStorage() {
            if (property == null && propertyId != null) {
                property = Thinklab.p(propertyId);
            }
        }
    }

    // @Property(NS.HAS_OBJECTSOURCE_DEFINITION)
    IFunctionCall                  _objectsourceDefinition;

    @Property(NS.IS_REIFICATION_MODEL)
    boolean                        _isReificationModel   = false;

    @Property(NS.HAS_INHERENT_SUBJECT_TYPE)
    IConcept                       _hasInherentSubjectType;

    IObjectSource                  _objectSource;

    @Property(NS.HAS_ATTRIBUTE_TRANSLATORS)
    ArrayList<AttributeTranslator> _attributeTranslators = new ArrayList<Model.AttributeTranslator>();

    @Override
    public String toString() {
        return "M" + _observables + ": " + _observer + " " + (_inlineState == null ? "" : "i")
                + (_datasourceDefinition == null ? "" : "d") + "}";
    }

    /*
     * ------------------------------------------------------------------------------ local methods
     * ------------------------------------------------------------------------------
     */

    public Model() {
    }

    /*
     * specialized constructor that will build one new model with a conditional composition of the passed
     * observers. Used only during model resolution.
     */
    public Model(ISemanticObject<?> observable, String id, INamespace namespace, ArrayList<IModel> models)
            throws ThinklabException {

        addObservable(observable.getDirectType(), id);
        setNamespace((INamespaceDefinition) namespace);

        ConditionalObserver observer = new ConditionalObserver(id, getNamespace());
        for (IModel m : models) {
            ((Model) m).setNamespace((INamespaceDefinition) getNamespace());
            observer.addModel(null, (IModelDefinition) m);
        }
        _observer = observer;
        _merged = true;

        // initialize();
    }

    /**
     * This is to retrieve the model that provides the ultimate observable(s), so we can label datasets and
     * states appropriately.
     * 
     * @return
     */
    public IModel getTopLevelModel() {

        IModel ret = this;
        while (((Model) ret)._parent != null) {
            ret = ((Model) ret)._parent;
        }

        return ret;
    }

    /*
     * specialized constructor that will build an identification with the passed dependencies and observables.
     * Used by the resolver when a parallel accessor is encountered, to compute all dependencies before its
     * process() method is called.
     */
    public Model(List<IObservable> observables, List<IDependency> dependencies, INamespace namespace) {
        _observables.addAll(observables);
        _dependencies.addAll(dependencies);
        setNamespace((INamespaceDefinition) namespace);
    }

    @Override
    public IScale getCoverage() throws ThinklabException {
        return getCoverage(null);
    }

    public IScale getCoverage(IMonitor monitor) throws ThinklabException {

        IScale ret = super.getCoverage();

        if (_datasource != null) {

            IMonitor itsm = null;
            if (_datasource instanceof IMonitorable && monitor != null) {
                itsm = ((IMonitorable) _datasource).getMonitor();
                ((IMonitorable) _datasource).setMonitor(monitor);
            }

            if (ret.getExtentCount() > 0) {
                System.out.println("");
            }

            ret = ret.merge(_datasource.getCoverage(), LogicalConnector.INTERSECTION, true);

            if (_datasource instanceof IMonitorable && monitor != null) {
                ((IMonitorable) _datasource).setMonitor(itsm);
            }
        } else if (_objectSource != null) {

            IMonitor itsm = null;
            if (_objectSource instanceof IMonitorable && monitor != null) {
                itsm = ((IMonitorable) _objectSource).getMonitor();
                ((IMonitorable) _objectSource).setMonitor(monitor);
            }

            if (ret.getExtentCount() > 0) {
                System.out.println("");
            }

            ret = ret.merge(_objectSource.getCoverage(), LogicalConnector.INTERSECTION, true);

            if (_objectSource instanceof IMonitorable && monitor != null) {
                ((IMonitorable) _objectSource).setMonitor(itsm);
            }
        }

        if (_observer != null) {
            ret = ret.merge(_observer.getCoverage(), LogicalConnector.INTERSECTION, true);
            /*
             * FIXME check that the merge results in usable extents. If not, monitor.
             */
            if (ret.isEmpty()) {
                if (monitor != null) {
                    monitor.error("merged spatial or temporal coverage of " + getName() + " is empty");
                }
                throw new ThinklabValidationException("empty coverage for " + getName());
            }
        }

        return ret;
    }

    static IModel promoteToModel(IObserver observer, IConcept observable, IModelResolver resolver, IMonitor monitor) {

        IModelDefinition ret = (IModelDefinition) resolver.newLanguageObject(IModel.class, monitor);

        ret.setLineNumbers(observer.getFirstLineNumber(), observer.getLastLineNumber());
        ret.setNamespace((INamespaceDefinition) observer.getNamespace());
        if (observable != null) {
            ((Model) ret).addObservable(observable, observer.getId());
        } else {
            ((Model) ret)._observables.add(observer.getObservable());
        }
        ret.setId(observer.getId());
        ret.setObserver((IObserverDefinition) observer);
        ret.initialize(resolver);
        return ret;
    }

    @Override
    public void setObserver(IObserverDefinition observer) {
        _observer = observer;
    }

    @Override
    public void addConditional(IModelDefinition observer, IExpression expression) {

        if (_observer == null) {
            _observer = new ConditionalObserver(getId(), getNamespace());
        } else if (!(_observer instanceof ConditionalObserver)) {
            throw new ThinklabRuntimeException("internal: observer already set in conditional model");
        }

        ((ConditionalObserver) _observer).addModel(expression, observer);
    }

    /**
     * Ensure that our metadata contain our extent and datasource information, plus some basic indicator
     * flags, so that we can get retrieved quickly without having to ask very complex queries.
     * @throws ThinklabException
     */
    @Override
    public void addStorageMetadata(IMetadata ret, IMonitor monitor) throws ThinklabException {

        ((Metadata) ret).put(NS.HAS_DIRECT_DATA, new Boolean(_datasource != null));
        ((Metadata) ret).put(NS.IS_IN_SCENARIO, new Boolean(getNamespace().isScenario()));
        ((Metadata) ret).put(NS.IS_PRIVATE, new Boolean(isPrivate()));
        ((Metadata) ret).put(NS.IS_DATA_MODEL, new Boolean(getObserver() != null));
        ((Metadata) ret).put(NS.IS_REIFICATION_MODEL, new Boolean(isReificationModel()));
        ((Metadata) ret).put(NS.HAS_DIRECT_OBJECTS, new Boolean(_objectsourceDefinition != null));

        /*
         * get storage metadata from both the datasource and the model
         */
        if (_observer instanceof IStorageMetadataProvider) {
            ((IStorageMetadataProvider) _observer).addStorageMetadata(ret, monitor);
        }
        if (_datasource instanceof IStorageMetadataProvider) {
            ((IStorageMetadataProvider) _datasource).addStorageMetadata(ret, monitor);
        }

        /*
         * compute extents including observer and datasource
         */
        int nOfMultipleExtents = 0;
        for (IExtent e : getCoverage(monitor)) {
            if (e instanceof IStorageMetadataProvider) {
                ((IStorageMetadataProvider) e).addStorageMetadata(ret, monitor);
                // if (e.getMultiplicity() > 1)
                nOfMultipleExtents++;
            }
        }

        ((Metadata) ret).put(NS.HAS_MULTIPLE_EXTENT_COUNT, nOfMultipleExtents);

    }

    @Override
    public void setInherentSubjectType(IConceptDefinition concept) {
        super.setInherentSubjectType(concept);
        for (IObservable obs : _observables) {
            ((Observable) obs).setInherentType(_inherentSubjectType);
        }
        if (_observer != null && _observer.getObservable() != null) {
            ((Observer<?>) _observer).setInherentSubjectType(concept);
            ((Observable) (_observer.getObservable())).setInherentType(_inherentSubjectType);
        }
    }

    /*
     * ------------------------------------------------------------------------------ public API
     * ------------------------------------------------------------------------------
     */
    @Override
    public IDataSource getDatasource() {
        return _datasource;
    }

    @Override
    public IObserver getObserver() {
        return _observer;
    }

    @Override
    public List<IObservable> getObservables() {
        return _observables;
    }

    @Override
    public Model demote() {
        return this;
    }

    @Override
    public void setDataSource(IDataSource datasource) {
        _datasource = datasource;
    }

    /*
     * ACHTUNG only use when a model is created directly and initialize() is not called.
     */
    public void addDependency(IDependency dep) {
        _dependencies.add(dep);
    }

    @Override
    public IAccessor getAccessor(IScale context, IMonitor monitor) throws ThinklabException {
        IAccessor ret = getUserDefinedAccessor(monitor);
        return ret;
    }

    @Override
    public boolean isResolved() {
        return (_datasourceDefinition != null || _datasource != null) && _dependencies.size() == 0;
    }

    @Override
    protected void dumpIndented(PrintStream out, int indent) {

        String ind = MiscUtilities.spaces(indent);

        out.print("model ");

        if (_datasourceDefinition != null) {

            ((FunctionCall) _datasourceDefinition).dumpIndented(out, indent);

        } else if (_inlineState != null) {

            out.print(ModelObject.dumpLiteral(_inlineState));

        } else {

            int i = 0;
            for (IObservable obs : _observables) {

                IConcept oc = obs.getType();
                out.print(oc.getConceptSpace().equals(getNamespace().getId()) ? oc.getLocalName() : oc
                        .toString() + " named " + obs.getFormalName());
                if (i < _observables.size() - 1)
                    out.print(",");
                i++;
            }
        }

        out.print(" named " + _namespaceId + ":" + getId());

        if (getObserver() != null) {
            out.print(" as\n");
            ((Observer<?>) getObserver()).dumpIndented(out, indent + 3);
        }

        for (int i = 0; i < _dependencies.size(); i++) {
            if (i == 0)
                out.print("\n" + ind + " observing\n");
            ((Dependency) _dependencies.get(i)).dumpIndented(out, indent + 6);
        }

        /*
         * TODO actions
         */

        if (_accessorGenerator != null) {
            out.print("\n" + ind + " using ");
            ((FunctionCall) _accessorGenerator).dumpIndented(out, indent);
        }

        /*
         * TODO metadata
         */

        out.print(";");
    }

    @Override
    public void addObservable(IConceptDefinition observationConcept, String formalName) {
        _obsDescs.add(new ObsDesc(observationConcept, formalName));
    }

    public void addObservable(IConcept observationConcept, String formalName) {
        _obsDescs.add(new ObsDesc(observationConcept, formalName));
    }

    @Override
    public void addObservable(IModel observationModel, String formalName) {
        _obsDescs.add(new ObsDesc(observationModel, formalName));
    }

    @Override
    public void addObservable(IFunctionCall function, String formalName) {
        _obsDescs.add(new ObsDesc(function, formalName));
    }

    @Override
    public void addInlineObservable(Object observable, String formalName) {
        ObsDesc od = new ObsDesc(observable);
        od.name = formalName;
        _obsDescs.add(od);
    }

    private boolean isSubjectAccessorFunction(IPrototype prototype) {
        for (IConcept c : prototype.getReturnTypes())
            if (c.is(Env.c(NS.SUBJECT_ACCESSOR)))
                return true;
        return false;
    }

    private void alignComputingModels(IModelResolver resolver) {

        /*
         * if we have a quality observable and an observer with a subject accessor, define a process to
         * compute the observable and move it to the second position. Model becomes a process model that
         * computes the observables given in first position.
         */

        if (_observer != null && ((Observer<?>) _observer)._accessorGenerator != null
                && ((Observer<?>) _observer)._accessorGenerator.getPrototype() != null
                && ((Observer<?>) _observer)._accessorGenerator.getPrototype().getReturnTypes() != null
                && isSubjectAccessorFunction(((Observer<?>) _observer)._accessorGenerator.getPrototype())
                && _obsDescs.size() > 0 && _obsDescs.get(0).concept != null) {

            /*
             * create and prepend process observable
             */
            IObservable observable = _observer.getObservable();
            String cId = observable.getLocalName() + "Computation";
            IConcept process = Thinklab.get().getConcept(observable.getConceptSpace() + ":" + cId);
            if (process == null) {
                process = Thinklab.get().getConcept(getNamespace().getId() + ":" + cId);
            }
            if (process /* still */== null) {

                ArrayList<IAxiom> axioms = new ArrayList<IAxiom>();
                axioms.add(Axiom
                        .ClassAssertion(cId, IConcept.CONCEPT_IS_COMPUTED | IConcept.CONCEPT_IS_LOCAL));
                axioms.add(Axiom.SubClass(NS.PROCESS, cId));
                getNamespace().getOntology().define(axioms);
                process = getNamespace().getOntology().getConcept(cId);
            }

            if (!process.is(Thinklab.c(NS.PROCESS))) {
                resolver.onException(new ThinklabValidationException(
                        "error trying to define a process to compute " + observable + ": concept " + process
                                + " exists and is not a process"), getFirstLineNumber());
            }

            /*
             * observer becomes a model; its dependencies are transferred to the main process model.
             */
            Observer<?> observer = (Observer<?>) _observer;
            _observer = null;
            _dependencies = observer._dependencies;
            observer._dependencies = new ArrayList<IDependency>();

            /*
             * remove accessor from observer and set it as model accessor
             */
            _accessorGenerator = observer._accessorGenerator;
            observer._accessorGenerator = null;

            /*
             * remove observer, make model out of it and set it as explanatory model for second observable
             */
            ArrayList<ObsDesc> obsDescs = new ArrayList<Model.ObsDesc>();
            obsDescs.add(new ObsDesc(process, getId()));
            obsDescs.add(new ObsDesc(
                    promoteToModel(observer, Thinklab.c(_obsDescs.get(0).concept), resolver, _monitor), observer
                            .getId()));
            for (int i = 1; i < _obsDescs.size(); i++) {
                obsDescs.add(_obsDescs.get(i));
            }
            _obsDescs = obsDescs;
        }
    }

    private void initializeObservables(IModelResolver resolver) {

        alignComputingModels(resolver);

        for (int i = 0; i < _obsDescs.size(); i++) {

            ObsDesc od = _obsDescs.get(i);

            if (od.concept != null) {

                /*
                 * model Concept; must be 1st; observationType is a child of the observable's or a new one if
                 * it's a subject.
                 */
                initializeConceptObservable(Env.c(od.concept), od.name, resolver, i);

            } else if (od.model != null) {

                /*
                 * must be 2nd+; observable is stated in model specs.
                 */
                initializeModelObservable(od.model, od.name, i);

            } else if (od.function != null) {

                Object o = null;
                try {
                    o = od.function.call(Thinklab.c(NS.DATASOURCE));
                } catch (ThinklabAuthorizationException e) {
                    resolver.onWarning(e.getMessage(), getFirstLineNumber());
                    _isInactive = true;
                } catch (ThinklabException e) {
                    resolver.onException(e, getFirstLineNumber());
                }
                
                if (o == null) {
                    addError(new ThinklabValidationException("function " + od.function.getId()
                            + " is unknown"), od.function.getFirstLineNumber());
                }

                if (o instanceof IDataSource) {
                    
                    initializeDatasourceObservable((IDataSource) o, od.name, resolver, i);
                    _datasourceDefinition = od.function;
                } else if (o instanceof IModel) {
                    initializeModelObservable((IModel) o, od.name, i);
                } else if (o != null) {
                    initializeInlineObservable(o, od.name, resolver, i);
                }
                /*
                 * NOTE: o == null is only acceptable in the client - and we should really create dummies for
                 * function returns, except we don't know if they're models or datasources.
                 */

            } else if (od.inlineValue != null) {
                initializeInlineObservable(od.inlineValue, od.name, resolver, i);
            }

        }

        /*
         * now the main observable is what the user defined. We want to substitute that with the observation
         * type unless we had a data or object source.
         */

    }

    private void initializeDatasourceObservable(IDataSource datasource, String name, IModelResolver resolver,
            int position) {

        /*
         * Anonymous: observation concept will be created from the observer, which must exist. Must be in 1st
         * position
         */
        if (position > 0) {
            addError(new ThinklabValidationException(
                    "data sources for annotation must be specified as the first observation"),
                    this.getFirstLineNumber());
        }

        if (_observer == null) {
            addError(new ThinklabValidationException(
                    "data sources for annotation require an observer to interpret them"),
                    this.getFirstLineNumber());
        }

        /*
         * observation type is the observer's
         */
        if (!hasErrors())
            _observables.add(new Observable(this, name));

        _datasource = datasource;
    }

    /**
     * Called with the STATED observable concept when it is stated. In practice it should only handle a single
     * concept stated in 1st position in the observable list, as all others should be models.
     * 
     * @param concept
     * @param name
     * @param axioms
     * @param resolver
     * @param position
     */
    private void initializeConceptObservable(IConcept concept, String name, IModelResolver resolver,
            int position) {

        /*
         * in 2nd+ position, the only bare concept allowed must have a quality model in the same namespace to
         * interpret it.
         */
        if (position > 0) {

            if (NS.isObject(concept)) {

                /*
                 * don't even try
                 */
                addError(new ThinklabValidationException(concept + ": secondary observables "
                        + "must be qualities and be annotated in the same namespace"),
                        this.getFirstLineNumber());

                return;
            }

            IModel representedModel = findObservableModel(concept, getFirstLineNumber());
            if (representedModel == null) {
                addError(new ThinklabValidationException(
                        ": cannot find a model in this namespace to interpret " + concept),
                        this.getFirstLineNumber());
            }

            if (!hasErrors()) {
                _observables.add(new Observable(concept, representedModel, name));
            }
            return;
        }

        /*
         * we're in 1st position. If we have no observer, we want an agent type; if the concept was just
         * defined, we make it a thing, otherwise we check and complain if it's not.
         */
        if (getObserver() == null) {

            /*
             * Observer is null: must be a valid subject type if position == 0; our observable type becomes an
             * identification of that subject.
             */
            if (!NS.isObject(concept)) {

                /*
                 * have we seen it before as a quality?
                 */
                if (concept.is(Env.c(NS.QUALITY))) {

                    /*
                     * yes - this concept really shouldn't be here
                     */
                    addError(new ThinklabValidationException("the observable " + concept
                            + " of a subject model " + "is a quality: it must be a process, thing or agent"),
                            this.getFirstLineNumber());

                } else {

                    /*
                     * No semantics: make it a thing. TODO see if it should be made an agent dependent on the
                     * model having actions
                     */
                    concept.getOntology()
                            .define(
                                    Collections.singletonList(Axiom.SubClass(NS.PHYSICAL_OBJECT, concept
                                            .toString())));

                }
            }

            // if we get here, we have a valid object concept; annotate as a direct observation.
            if (!hasErrors()) {
                _observables
                        .add(new Observable(concept, Env.c(NS.DIRECT_OBSERVATION), _inherentSubjectType, null));
            }

        } else {

            /*
             * we have an observer. Must be a quality or a process.
             */
            if (NS.isObject(concept)) {

                addError(new ThinklabValidationException("the observable " + concept + " of a data model "
                        + "is a thing or agent: it should be a quality"), this.getFirstLineNumber());

            } else {

                /*
                 * the stated concept is a delegate observation type for the observation semantics of the
                 * observer.
                 */
                _observables.add(new Observable(concept, this, name));

            }

        }
    }

    private void initializeModelObservable(IModel model, String name, int position) {

        /*
         * model is only accepted in 2nd position. We take the n-th observable from it.
         */
        if (position < 1) {
            addError(
                    new ThinklabValidationException(
                            "models in the observation list interpret additional observations, and can only be specified after the first observation"),
                    this.getFirstLineNumber());
        }

        if (model.getObserver() == null) {
            addError(
                    new ThinklabValidationException(
                            "models in the observation list are only for qualities; subject models should be stated only as dependencies"),
                    this.getFirstLineNumber());
        }

        if (name == null) {
            /*
             * we need a name here
             */
            name = CamelCase.toLowerCase(model.getObservable().getType().getLocalName(), '-');
        }

        if (!hasErrors())
            _observables.add(new Observable(model, name));

    }

    private void initializeInlineObservable(Object obj, String name, IModelResolver resolver, int position) {

        if (position > 0) {
            addError(new ThinklabValidationException(
                    "explicit values for annotation must be specified as the first observation"),
                    this.getFirstLineNumber());
        }

        if (_observer == null) {
            addError(new ThinklabValidationException(
                    "explicit values for annotation require an observer to interpret them"),
                    this.getFirstLineNumber());
        }

        _inlineState = obj;

        /*
         * observation type is the observer's
         */
        if (!hasErrors())
            _observables.add(new Observable(this, name));

        _datasource = new ConstantDataSource(_inlineState = obj);
    }

    private void initializeObserver(IModelResolver resolver) {

        if (_observer != null) {
            if (getId() != null) {
                ((Observer<?>) _observer).setId(getId());
            }
            ((Observer<?>) _observer).initialize(resolver);
        }
    }

    @Override
    public void validate(IModelResolver resolver) {

        super.validate(resolver);

        /*
         * validate observer
         */
        if (_observer != null)
            ((Observer<?>) _observer).validate(resolver);

    }

    public void initializeFromStorage() {

        super.initializeFromStorage();

        /*
         * models in all observers
         */
        for (int i = 0; i < _observables.size(); i++) {
            ((Observable) (_observables.get(i))).setModel(i == 0 ? this : _obmodels.get(i - 1));
        }

        /*
         * datasource function call
         */
        if (_datasourceDefinition != null)
            try {
                _datasource = (IDataSource) _datasourceDefinition.call();
                if (_datasource != null && _datasource instanceof IMonitorable) {
                    ((IMonitorable) _datasource).setMonitor(_monitor);
                }
            } catch (ThinklabException e) {
                throw new ThinklabRuntimeException(e);
            }

        /*
         * inline state
         */
        if (_inlineState != null)
            _datasource = new ConstantDataSource(_inlineState);

        if (_observer != null) {
            if (getId() != null) {
                ((Observer<?>) _observer).setId(getId());
            }
            ((IObserverDefinition) _observer).setModel(this);
        }

        /*
         * no need to validate, if this was stored it's already valid.
         */
    }

    @Override
    public void initialize(IModelResolver resolver) {

        if (_initialized)
            return;

        super.initialize(resolver);

        initializeObserver(resolver);

        if (_isInterpreting && _observer != null) {
            Observable oo = new Observable((Observable) (_observer.getObservable()));
            oo.setModel(this);
            _observables.add(oo);
            _traitType = ((Observer<?>) _observer)._traitType;
            _inherentSubjectType = ((Observer<?>) _observer)._inherentSubjectType;
        } else {
            initializeObservables(resolver);
        }

        if (_observer != null && _observer.hasErrors()) {
            _errors.addAll(((Observer<?>) _observer)._errors);
            _errorCount = _errors.size();
        }

        if (_observables.size() > 0) {
            alignDependencies(getObservable(), resolver);
            validate(resolver);
        }

        /*
         * store the models so we get written properly to the DB.
         */
        _obmodels.clear(); // should not be necessary
        for (int i = 1; i < _observables.size(); i++) {
            _obmodels.add(_observables.get(i).getModel());
        }

        /*
         * check errors accumulated at initialize() - by this or amy child class
         */
        for (Pair<Throwable, Integer> err : _errors) {
            resolver.onException(err.getFirst(), err.getSecond());
        }

        if (_observer != null) {
            ((IObserverDefinition) _observer).setModel(this);
        }

        if (_attributeTranslators.size() > 0) {
            /*
             * add anything we can infer, e.g. a presence observer
             */
            boolean hasPresence = false;
            for (AttributeTranslator a : _attributeTranslators) {

                if (a.observer != null) {
                    if (a.observer.getObservable().is(this.getObservable().getType())
                            && a.observer.getObserver() instanceof Presence) {
                        hasPresence = true;
                        break;
                    }
                }
            }
            if (!hasPresence) {
                Presence pobs = new Presence(this.getObservable().getType(), getNamespace());
                IModel pmod = promoteToModel(pobs, null, resolver, _monitor);
                /*
                 * TODO add the function that will generate the datasource from the objectsource.
                 */
                AttributeTranslator atra = new AttributeTranslator();
                atra.attribute = NS.PRESENCE;
                atra.observer = pmod;
                _attributeTranslators.add(atra);
            }
        }

        _initialized = true;

    }

    @Override
    public String createId() {

        String ret = null;
        if (_obsDescs.size() > 0) {
            ret = _obsDescs.get(0).createModelId();
        }
        if (ret == null && _observer != null) {
            ret = CamelCase.toLowerCase(_observer.getObservable().getLocalName(), '-');
        }

        if (ret == null) {
            ret = "unnamed-model";
        }
        return ret;
    }

    /*
     * meant to serialize easily; just holds the info until initialize() can deal with it.
     */
    class ObsDesc {

        String        name;

        IModel        model;
        String        concept;
        IFunctionCall function;
        Object        inlineValue;

        String createModelId() {

            if (model != null) {
                return ((Model) model).createId();
            } else if (concept != null) {
                return CamelCase.toLowerCase(new SemanticType(concept).getLocalName(), '-');
            }
            return null;
        }

        // for storage
        ObsDesc() {
        }

        ObsDesc(IModel model, String name) {
            this.model = model;
            this.name = name;
        }

        ObsDesc(IConceptDefinition concept, String name) {
            this.concept = concept.getName();
            this.name = name;
        }

        ObsDesc(IConcept concept, String name) {
            this.concept = concept.toString();
            this.name = name;
        }

        ObsDesc(IFunctionCall function, String name) {
            this.function = function;
            this.name = name;
        }

        ObsDesc(Object inlineValue) {
            this.inlineValue = inlineValue;
            // string is added later to avoid confusions with overloaded constructors.
        }
    }

    ArrayList<ObsDesc> _obsDescs    = new ArrayList<ObsDesc>();

    /*
     * only not-null in subject models, where it is set to the only concept mentioned in the specifications
     * (we have no observer) while our observable will be its DirectObservation.
     */
    IConcept           _subjectType = null;

    @Override
    public boolean hasErrors() {
        if (_observer != null && _observer.hasErrors())
            return true;
        return super.hasErrors();
    }

    @Override
    public void setObjectReificationData(IConceptDefinition concept, Object objectGenerator) {

        _isReificationModel = true;

        addObservable(concept, null);

        if (objectGenerator instanceof IFunctionCall) {
            _objectsourceDefinition = (IFunctionCall) objectGenerator;
        }
    }

    @Override
    public void addAttributeTranslator(String attributeId, IModel observer, IPropertyDefinition property) {

        AttributeTranslator at = new AttributeTranslator();

        at.attribute = attributeId;
        at.observer = observer;

        if (property != null) {
            at.property = Thinklab.p(property.getName());
            at.propertyId = property.getName();
        } else {
            _attributeNames.add(attributeId);
        }

        _attributeTranslators.add(at);
    }

    /**
     * helper method which allowed me to move createSubjects(IModel, IScale) to SubjectObserver.
     * 
     * -Luke
     * 
     * @return
     */
    public ArrayList<AttributeTranslator> getAttributeTranslators() {
        return _attributeTranslators;
    }

    @Override
    public boolean isReificationModel() {
        return _isReificationModel;
    }

    @Override
    public IObjectSource getObjectSource() {

        if (_objectSource == null && _objectsourceDefinition != null) {

            Object o;
            try {
                o = _objectsourceDefinition.call();
                if (o != null && o instanceof IMonitorable) {
                    ((IMonitorable) o).setMonitor(_monitor);
                }

            } catch (ThinklabException e) {
                throw new ThinklabRuntimeException(e);
            }
            if (!(o instanceof IObjectSource)) {
                throw new ThinklabRuntimeException("function " + _objectsourceDefinition.getId()
                        + " does not return a source of objects");
            }

            _objectSource = (IObjectSource) o;
        }

        return _objectSource;
    }

    @Override
    public IObservable getObservable() {
        return _observables.size() == 0 ? null : _observables.get(0);
    }

    @Override
    public void wrap(IObserverDefinition observer) {
        _observer = observer;
        _observables.add(observer.getObservable());
    }

    public void resetDependencies() {
        _dependencies.clear();
    }

    /*
     * true for models that result from a merge of conditional models that cover partial
     * extents, built using the specific constructor.
     */
    public boolean isMerged() {
        return _merged;
    }

    @Override
    public Collection<String> getObjectAttributes() {
        return _attributeNames;
    }

    @Override
    public IModel getAttributeObserver(String attribute) {

        IModel ret = null;

        if (attribute.equals(ModelData.PRESENCE_ATTRIBUTE)) {
            ret = ModelFactory.presence(getObservable(), getNamespace());
        } else {

            for (AttributeTranslator t : _attributeTranslators) {
                if (t.attribute.equals(attribute)) {
                    ret = t.observer;
                    break;
                }
            }
        }
        if (ret != null && getObjectSource() != null) {
            ((Model) ret)._datasource = getObjectSource().getDatasource(attribute);
        }

        return ret;
    }

    @Override
    public void setDependencyModel(boolean b) {
        _isDependencyModel = b;
    }

    @Override
    public void setInterpreter(boolean b) {
        _isInterpreting = b;
    }

}
