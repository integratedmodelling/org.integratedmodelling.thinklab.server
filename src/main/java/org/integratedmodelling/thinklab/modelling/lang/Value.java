package org.integratedmodelling.thinklab.modelling.lang;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.annotations.Concept;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.lang.IModelResolver;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.modelling.IClassification;
import org.integratedmodelling.thinklab.api.modelling.ICurrency;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.IStateAccessor;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.api.modelling.parsing.IClassificationDefinition;
import org.integratedmodelling.thinklab.api.modelling.parsing.INamespaceDefinition;
import org.integratedmodelling.thinklab.api.modelling.parsing.IValuingObserverDefinition;
import org.integratedmodelling.thinklab.common.vocabulary.NS;

@Concept(NS.VALUING_OBSERVER)
public class Value extends Observer<Value> implements IValuingObserverDefinition {

    IClassification _discretization;
    ICurrency _currency;

    @Override
    public Value demote() {
        return this;
    }

    public boolean isDiscrete() {
        return _discretization != null;
    }

    @Override
    public IClassification getDiscretization() {
        return _discretization;
    }

    @Override
    public void setDiscretization(IClassification classification) {
        _discretization = classification;
    }

    @Override
    public IStateAccessor getMediator(IObserver observer, IMonitor monitor) throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IStateAccessor getInterpreter(IStateAccessor accessor, IMonitor monitor) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getSignature() {
        return "#value#" +
        // TODO currency				(_scale == null ? "" : _scale) + 
                (_discretization == null ? "" : _discretization.getConceptSpace());
    }

    @Override
    public IConcept getObservationType(INamespace namespace) {
        // TODO specialize for currency, and further if there is a discretization
        return Thinklab.c(NS.VALUATION);
    }

    //
    //	@Override
    //	public void initialize(IResolver resolver) {
    //		super.initialize(resolver);
    //		if (_discretization != null) {
    //			((INamespaceDefinition)getNamespace()).synchronizeKnowledge(resolver);
    //			((IClassificationDefinition)_discretization).setConceptSpace(getObservableConcept(), resolver);
    //			((IClassificationDefinition)_discretization).initialize();
    //		}
    //	}

    @Override
    public void notifyModelObservable(IObservable observable, IModelResolver resolver) {

        if (_discretization != null) {
            ((INamespaceDefinition) getNamespace()).synchronizeKnowledge(null);
            ((IClassificationDefinition) _discretization).setConceptSpace(observable, resolver);
            ((IClassificationDefinition) _discretization).initialize();
            if (!_discretization.isContiguousAndFinite()) {
                addError(new ThinklabValidationException(
                        "discretization has discontinuous and/or unbounded intervals"),
                        _discretization.getFirstLineNumber());
            }
        }

    }

    @Override
    public void setCurrency(Object o) {
        // TODO Auto-generated method stub

    }

    @Override
    public ICurrency getCurrency() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public double getMinimumValue() {
        return Double.NaN;
    }

    @Override
    public double getMaximumValue() {
        return Double.NaN;
    }

}
