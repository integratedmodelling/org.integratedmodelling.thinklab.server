package org.integratedmodelling.thinklab.modelling.lang.agents;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Set;

import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabResourceNotFoundException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.modelling.IActiveSubject;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.api.modelling.agents.IAgentState;
import org.integratedmodelling.thinklab.api.modelling.agents.ICollision;
import org.integratedmodelling.thinklab.api.modelling.agents.IObservationController;
import org.integratedmodelling.thinklab.api.modelling.agents.IObservationGraphNode;
import org.integratedmodelling.thinklab.api.modelling.agents.IObservationGraphNodeSubscriber;
import org.integratedmodelling.thinklab.api.modelling.agents.IObservationTask;
import org.integratedmodelling.thinklab.api.modelling.scheduling.ITransition;
import org.integratedmodelling.thinklab.api.time.ITemporalSeries;
import org.integratedmodelling.thinklab.api.time.ITimeInstant;
import org.integratedmodelling.thinklab.api.time.ITimePeriod;
import org.integratedmodelling.thinklab.debug.CallTracer;
import org.integratedmodelling.thinklab.modelling.lang.agents.TemporalCausalGraph.InvalidationResult;
import org.integratedmodelling.thinklab.time.literals.PeriodValueUnboundedEnd;

public class ObservationController implements IObservationController {

    private final TemporalCausalGraph<ISubject, IObservationGraphNode> causalGraph;
    private final PriorityQueue<IObservationTask>                      taskQueue           = new PriorityQueue<IObservationTask>();
    private final Set<IObservationTask>                                currentlyProcessing = new HashSet<IObservationTask>();
    private final HashMap<ISubject, AgentDetail>                       agentDetails        = new HashMap<ISubject, AgentDetail>();

    private final IMonitor                                             monitor;

    // // TODO this might be needed if we have to create a SubjectObserver for newly created agents
    // private final List<String> scenarios;

    private final ITimeInstant                                         simulationEndTime;

    private class AgentDetail {
        ISubject                                               subject;
        ITimeInstant                                           birth;
        ITimeInstant                                           death       = null;
        ITimeInstant                                           lastKnownAliveTime;
        HashMap<IObservationGraphNodeSubscriber, ITimeInstant> subscribers = new HashMap<IObservationGraphNodeSubscriber, ITimeInstant>();

        public AgentDetail(ISubject subject, ITimePeriod initialTimePeriod) {
            this.subject = subject;
            birth = initialTimePeriod.getStart();
            lastKnownAliveTime = initialTimePeriod.getEnd();
        }
    }

    /**
     * represents the agent's status as of a specific point in time: is it alive? dead? has it been evaluated
     * yet?
     */
    public enum AgentStatus {
        nonExistent,
        evaluated,
        notYetEvaluated,
        dead
    }

    public ObservationController(TemporalCausalGraph<ISubject, IObservationGraphNode> causalGraph,
            IMonitor monitor, ITimeInstant simulationEndTime) {
        this.causalGraph = causalGraph;
        this.monitor = monitor;
        // this.scenarios = scenarios;
        this.simulationEndTime = simulationEndTime;
    }

    @Override
    public IObservationGraphNode createAgent(ISubject subject, IAgentState initialState,
            ITimePeriod initialTimePeriod, IObservationGraphNode parentNode, IObservationTask creationTask,
            boolean enqueueSubsequentTask) {

        IObservationGraphNode node = new ObservationGraphNode(initialState, creationTask);

        causalGraph.createAgent(subject, node, initialTimePeriod, parentNode);
        agentDetails.put(subject, new AgentDetail(subject, initialTimePeriod));

        // will the agent remain alive for another observation? (this code assumes yes)
        if (enqueueSubsequentTask) {
            IObservationTask subsequentTaskForThisAgent = new ObservationTask(subject,
                    initialTimePeriod.getEnd(), node, this);
            enqueueTask(subsequentTaskForThisAgent);
        }

        return node;
    }

    private void enqueueTask(IObservationTask task) {
        taskQueue.add(task);
        task.getParentNode().addTaskCausedByThisNodeState(task);
    }

    @Override
    public IObservationTask getNext() {
        IObservationTask task = taskQueue.poll();
        while (task != null && !task.isValid() && task.getObservationTime().compareTo(simulationEndTime) >= 0) {
            // poll until we find a task which has not been invalidated by a collision
            // (exits if the queue is empty)
            task = taskQueue.poll();
        }

        if (task != null) {
            // we found a valid task before the queue was empty, so mark it as 'working' and return it
            task.startedWorkAt(System.currentTimeMillis());
            currentlyProcessing.add(task);
        }

        monitor.debug("ObservationController is returning task " + CallTracer.detailedDescription(task));
        return task;
    }

    @Override
    public void setResult(IObservationTask task, ITransition result) throws ThinklabValidationException {
        // NOTE: result.task and task.result are already set

        CallTracer.indent("setResult()", this, task, result);

        // make sure we're dealing with the local task (so its state is accurate)
        task = getThreadlocalTaskObject(task);

        currentlyProcessing.remove(task);

        if (!task.isValid()) {
            // the task has been invalidated by a collision while it was processing, so don't do anything with
            // its result
            CallTracer
                    .msg("task was flagged as invalid while it was being processed. Returning without applying any changes.");
            CallTracer.unIndent();
            return;
        }

        // Sanity check - have any of the dependencies become invalid during observation?
        IObservationGraphNode dependencyNode;
        Collection<Pair<ITimeInstant, IObservationGraphNode>> dependencies = result
                .getObservationDependencies();
        for (Pair<ITimeInstant, IObservationGraphNode> dependency : dependencies) {
            dependencyNode = dependency.getSecond();

            // make sure we're dealing with a local graph node (so its state is accurate)
            dependencyNode = getThreadlocalGraphNodeObject(dependencyNode);

            // has the dependency been removed or shortened by an invalidation?
            if (dependencyNode == null
                    || !dependencyNode.getAgentState().getTimePeriod().contains(dependency.getFirst())) {
                // the dependency node has been removed, or it is no longer valid for the time which it was
                // originally queried.
                // re-evaluate the result which has been returned (without setting the current result)
                CallTracer.msg(dependencyNode == null ? "Found a null dependency." : "Dependency "
                        + dependencyNode + " has changed during processing.");
                CallTracer.msg("Re-enqueuing task without applying any changes...");
                CallTracer.unIndent();

                enqueueTask(task);
                return;
            }
        }

        ISubject agent = task.getSubject();
        AgentDetail agentDetail = agentDetails.get(agent); // null if agent doesn't exist yet
        IObservationGraphNode node;
        ITimePeriod timePeriod;

        if (result.agentSurvives()) {
            // set in the causal graph (also sets parent causal link with the agent's previous state)
            IAgentState agentState = result.getAgentState();
            timePeriod = agentState.getTimePeriod();
            ITimeInstant agentStateEndTime = timePeriod.getEnd();

            if (agentDetail == null) {
                // agent does not yet exist
                CallTracer.msg("Creating new agent " + agent + "...");
                node = createAgent(agent, agentState, timePeriod, task.getParentNode(), task, false);
                agentDetail = agentDetails.get(agent);
            } else {
                // agent exists - add a new node in its agent-state series
                CallTracer.msg("Creating new causal-graph node for agent " + agent + "...");
                node = new ObservationGraphNode(agentState, task, result);
            }

            // queue up collision detection tasks for all agent-states which overlap the one just created
            for (IObservationGraphNode overlappingNode : getOverlappingAgentStateNodes(node)) {
                if (overlappingNode.canCollideWithAnything()) { // returns false for agent-death nodes
                    IAgentState overlappingAgentState = overlappingNode.getAgentState();
                    // use getEnd() because collision detection happens at the END of the overlapping period
                    ITimeInstant endInstant = overlappingAgentState.getTimePeriod().getEnd();
                    if (agentStateEndTime.compareTo(endInstant) < 0) {
                        endInstant = agentStateEndTime;
                    }

                    ObservationTaskCollisionDetection collisionDetectionTask = new ObservationTaskCollisionDetection(
                            endInstant, node, overlappingNode, node, this);
                    enqueueTask(collisionDetectionTask);
                }
            }
        } else {
            ITimeInstant timeOfDeath = task.getObservationTime();
            timePeriod = new PeriodValueUnboundedEnd(timeOfDeath.getMillis());
            node = new ObservationGraphNodeAgentDeath(task, result);
            CallTracer.msg("Agent is pronounced dead at " + timeOfDeath + ".");
            agentDetail.death = timeOfDeath;
        }
        causalGraph.addStateNode(agent, node, timePeriod, task.getParentNode());

        // set any other *influential* links (i.e. what did the observer observe during observation?)
        for (Pair<ITimeInstant, IObservationGraphNode> dependency : dependencies) {
            causalGraph.addInfluentialRelationship(dependency.getSecond(), node);
        }

        // queue up the next tasks which are a result of this observation (does not include collision
        // detection).
        // possible outcomes of a task are:
        // next observation task for this agent
        // agent creation/destruction (messaging, childbirth, etc)
        // misc. observation tasks (we haven't limited the possible reasons)
        for (IObservationTask nextTask : result.getFurtherObservationTasks()) {
            nextTask.setParentNode(node);
            enqueueTask(nextTask);
        }

        // notify any listeners that this agent has changed
        notify(agent);

        CallTracer.unIndent();
    }

    /**
     * TODO in multi-threaded environments, use this method to convert an arbitrary task object into the
     * equivalent task which is "owned" by the observation controller, so that its state can be updated.
     * 
     * The most straightforward way to do this would be to implement task.hashCode() and task.equals(other)
     * and find the original in a hash index.
     * 
     * @param task
     * @return
     */
    protected IObservationTask getThreadlocalTaskObject(IObservationTask task) {
        return task;
    }

    /**
     * TODO this does the same as getThreadlocalTaskObject, but for graph nodes. TODO return null if the node
     * has been removed from the graph
     * 
     * @see ObservationController.getThreadlocalTaskObject()
     */
    private IObservationGraphNode getThreadlocalGraphNodeObject(IObservationGraphNode node) {
        return node;
    }

    private Collection<IObservationGraphNode> getOverlappingAgentStateNodes(IObservationGraphNode node) {
        Collection<IObservationGraphNode> overlaps = causalGraph.getOverlapping(node.getAgentState()
                .getTimePeriod());
        overlaps.remove(node); // above method will return the original agentState
        return overlaps;
    }

    @Override
    public void collide(IObservationGraphNode node1, IObservationGraphNode node2, ICollision collision)
            throws ThinklabException {
        collide(node1, collision);
        collide(node2, collision);
    }

    @Override
    public void collide(IObservationGraphNode node, ICollision collision) throws ThinklabException {
        ISubject subject = node.getAgentState().getSubject();
        boolean stateMustChange = ((IActiveSubject) subject)
                .doesThisCollisionAffectYou(node.getAgentState(), collision);

        if (stateMustChange) {
            ITimeInstant collisionTime = collision.getCollisionTime();

            invalidateChildTasksAndSubscriberCaches(node, collisionTime);

            // terminate the agent-state at the collision time
            InvalidationResult<IObservationGraphNode> statesToReEvaluate = causalGraph
                    .invalidate(node, collisionTime);

            // queue up the new observation which is now required.
            // All CAUSALLY dependent states and observation tasks which have been deleted will be re-computed
            // by this task.
            ObservationTaskCollisionHandling postCollisionTask = new ObservationTaskCollisionHandling(
                    agentDetails.get(subject).subject, collision, node, this);
            enqueueTask(postCollisionTask);

            // re-queue the INFLUENTIALLY dependent observations which have become necessary due to other
            // observations becoming invalid
            for (IObservationGraphNode state : statesToReEvaluate.getInfluential()) {
                invalidateChildTasksAndSubscriberCaches(state, collisionTime);
                enqueueTask(state.getTaskCreatingThisNodeState());
            }

            // CAUSALLY dependent observations should be deleted along with subsequent tasks, without
            // re-queuing an observation.
            for (IObservationGraphNode state : statesToReEvaluate.getCausal()) {
                invalidateChildTasksAndSubscriberCaches(state, collisionTime);
            }
        }
    }

    /**
     * invalidate observation tasks which were a product of the post-collision time period, and invalidate any
     * IScaleMediator objects which have cached the state in question.
     * 
     * @param agentState
     * @param collisionTime
     */
    private void invalidateChildTasksAndSubscriberCaches(IObservationGraphNode agentState,
            ITimeInstant collisionTime) {
        // NOTE: tasks can be in either the priority queue or the currently-processing queue
        for (IObservationTask task : agentState.getTasksCausedByThisNodeState()) {
            if (task.getObservationTime().compareTo(collisionTime) > 0) {
                // the task was created as a result of the invalid partition of the agent-state,
                // so invalidate it.
                // NOTE: don't remove it from any queues; this way is better for performance.
                task.setInvalid();
            }
        }

        // invalidate the ScaleMediator listeners' caches
        // TODO it is essential that the cache invalidations are CONFIRMED before adding any more tasks to the
        // queue
        // (this is trivially satisfied in single-threaded execution)
        for (IObservationGraphNodeSubscriber listener : agentState.getSubscribers()) {
            listener.invalidate(collisionTime);
        }
    }

    /**
     * 
     * @param agent
     * @param startTime set to null to start listening at the beginning.
     * @throws ThinklabResourceNotFoundException
     */
    @Override
    public void subscribe(IObservationGraphNodeSubscriber listener, ISubject agent, ITimeInstant startTime)
            throws ThinklabResourceNotFoundException {
        // register the subscriber (interpolator)
        AgentDetail agentDetail = agentDetails.get(agent);
        if (agentDetail == null) {
            throw new ThinklabResourceNotFoundException("Agent " + agent.toString());
        }
        agentDetail.subscribers.put(listener, startTime);

        // catch the listener up with what has been published since its start time
        notify(agent, listener);
    }

    private void notify(ISubject agent) {
        HashMap<IObservationGraphNodeSubscriber, ITimeInstant> listeners = agentDetails.get(agent).subscribers;
        if (listeners == null) {
            return;
        }
        for (IObservationGraphNodeSubscriber listener : listeners.keySet()) {
            notify(agent, listener);
        }
    }

    private void notify(ISubject agent, IObservationGraphNodeSubscriber listener) {
        ITemporalSeries<IObservationGraphNode> agentStateSeries = causalGraph.getAgentStateSeries(agent);
        HashMap<IObservationGraphNodeSubscriber, ITimeInstant> agentListenerUpdateTimes = agentDetails
                .get(agent).subscribers;
        ITimeInstant lastNotified = agentListenerUpdateTimes.get(listener);
        IObservationGraphNode node;
        if (lastNotified == null) {
            node = agentStateSeries.getFirst();
        } else {
            node = agentStateSeries.getFollowing(lastNotified);
        }

        // iterate through the agent states which start AFTER the last-notified agent state for this listener
        while (node != null) {
            listener.notify(node);
            lastNotified = node.getAgentState().getTimePeriod().getEnd();
            agentListenerUpdateTimes.put(listener, lastNotified);
            node = agentStateSeries.getFollowing(lastNotified);
        }
    }

    /**
     * is the agent alive? dead? non-existent?
     * 
     * @param agent
     * @param time
     * @return
     */
    public AgentStatus getAgentStatus(ISubject agent, ITimeInstant time) {
        AgentDetail agentDetail = agentDetails.get(agent);
        if (agentDetail == null || agentDetail.birth.compareTo(time) >= 0) {
            // agent not created, or is born after the time given
            return AgentStatus.nonExistent;
        }

        // agent was born before the time given
        if (agentDetail.lastKnownAliveTime.compareTo(time) >= 0) {
            return AgentStatus.evaluated;
        }

        if (agentDetail.death != null && agentDetail.death.compareTo(time) < 0) {
            return AgentStatus.dead;
        }

        return AgentStatus.notYetEvaluated;
    }
}
