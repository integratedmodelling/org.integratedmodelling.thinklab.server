package org.integratedmodelling.thinklab.modelling.lang.expressions;

import java.util.Map;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IExpression;
import org.integratedmodelling.thinklab.api.lang.IExpressionLanguageAdapter;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.api.modelling.parsing.IFunctionCall;
import org.integratedmodelling.thinklab.api.project.IProject;
import org.integratedmodelling.thinklab.modelling.lang.Namespace;

import com.google.common.collect.Sets;

public class GroovyLanguageAdapter implements IExpressionLanguageAdapter {

    Namespace _ns;

    public GroovyLanguageAdapter(Namespace namespace) {
        _ns = namespace;
    }

    @Override
    public IExpression getTrueExpression() {
        return new GroovyExpression("true");
    }

    public IExpression getInContextExpression(ISubject context) {
        return null;
    }

    @Override
    public IExpression getExpression(String expression, INamespace namespace, IConcept... domain) {
        return new GroovyExpression(expression, namespace, domain == null ? null : Sets.newHashSet(domain));
    }

    @Override
    public IExpression compileFunctionCall(IFunctionCall processFunctionCall) {
        return new ObjectExpression(processFunctionCall);
    }

    @Override
    public IExpression compileObject(Object processLiteral) {
        return new ObjectExpression(processLiteral);
    }

    /*
     * cheap trick to avoid lots of stupid switch statements.
     * FIXME this cheap trick seems to create a few disasters here and there.
     */
    class ObjectExpression implements IExpression {

        Object _o;

        public ObjectExpression(Object object) {
            _o = object;
        }

        @Override
        public Object eval(Map<String, Object> parameters, IConcept... context) throws ThinklabException {

            if (_o instanceof IFunctionCall) {

                /*
                 * TODO add contextual info to parameters
                 */
                return ((IFunctionCall) _o).call();
            }
            return _o;
        }

        @Override
        public void setProjectContext(IProject project) {
        }

        @Override
        public String toString() {
            return _o.toString();
        }

    }

}
