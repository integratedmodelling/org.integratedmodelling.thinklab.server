package org.integratedmodelling.thinklab.modelling.lang.agents;

import org.integratedmodelling.thinklab.api.modelling.agents.IAgentState;
import org.integratedmodelling.thinklab.api.modelling.agents.IObservationTask;

public class AgentStateGraphNode {
    private final IObservationTask task;
    private final IAgentState parentState;
    private final IAgentState result;

    public AgentStateGraphNode(IObservationTask task, IAgentState parentState, IAgentState result) {
        this.task = task;
        this.parentState = parentState;
        this.result = result;
    }

    public IObservationTask getTask() {
        return task;
    }

    public IAgentState getParentState() {
        return parentState;
    }

    public IAgentState getResult() {
        return result;
    }
}
