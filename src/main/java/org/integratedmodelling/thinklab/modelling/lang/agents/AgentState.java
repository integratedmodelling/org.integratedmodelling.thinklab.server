package org.integratedmodelling.thinklab.modelling.lang.agents;

import java.util.Map;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.knowledge.IProperty;
import org.integratedmodelling.thinklab.api.modelling.IObjectState;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.api.modelling.agents.IAgentState;
import org.integratedmodelling.thinklab.api.time.ITimeInstant;
import org.integratedmodelling.thinklab.api.time.ITimePeriod;
import org.integratedmodelling.thinklab.time.literals.PeriodValue;

/**
 * a temporary class to describe what is being called "agent-state" in the multiple-scale paper. It represents
 * a state (which can be a constant value, temporal function, or probability over either) valid for some time
 * period.
 *
 * @author luke
 *
 */
public class AgentState implements IAgentState {
    private final ISubject subject;
    private final ITimePeriod timePeriod;
    private final Map<IProperty, IObjectState> states;

    public AgentState(ISubject subject, ITimePeriod timePeriod, Map<IProperty, IObjectState> states) {
        this.subject = subject;
        this.timePeriod = timePeriod;
        this.states = states;
    }

    @Override
    public ISubject getSubject() {
        return subject;
    }

    @Override
    public ITimePeriod getTimePeriod() {
        return timePeriod;
    }

    @Override
    public Map<IProperty, IObjectState> getStates() {
        return states;
    }

    /**
     * see comments on IAgentState.terminateEarly()
     * @throws ThinklabException
     */
    @Override
    public IAgentState terminateEarly(ITimeInstant interruptTime) throws ThinklabException {
        ITimePeriod shorterTimePeriod = new PeriodValue(timePeriod.getStart().getMillis(),
                interruptTime.getMillis());
        AgentState result = new AgentState(subject, shorterTimePeriod, states);
        return result;
    }

    @Override
    public String toString() {
        return "AgentState[" + subject.toString() + " at " + timePeriod.toString() + "]";
    }
}
