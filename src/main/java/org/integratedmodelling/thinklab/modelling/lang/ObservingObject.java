package org.integratedmodelling.thinklab.modelling.lang;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabInternalErrorException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.lang.Axiom;
import org.integratedmodelling.lang.LogicalConnector;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.knowledge.IAxiom;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IExpression;
import org.integratedmodelling.thinklab.api.knowledge.IProperty;
import org.integratedmodelling.thinklab.api.knowledge.ISemanticObject;
import org.integratedmodelling.thinklab.api.lang.IList;
import org.integratedmodelling.thinklab.api.lang.IModelResolver;
import org.integratedmodelling.thinklab.api.lang.IPrototype;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.listeners.IMonitorable;
import org.integratedmodelling.thinklab.api.modelling.IAccessor;
import org.integratedmodelling.thinklab.api.modelling.IAction;
import org.integratedmodelling.thinklab.api.modelling.IExtent;
import org.integratedmodelling.thinklab.api.modelling.IModel;
import org.integratedmodelling.thinklab.api.modelling.IModelObject;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.api.modelling.IObservingObject;
import org.integratedmodelling.thinklab.api.modelling.IScale;
import org.integratedmodelling.thinklab.api.modelling.IState;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.api.modelling.parsing.IConceptDefinition;
import org.integratedmodelling.thinklab.api.modelling.parsing.IFunctionCall;
import org.integratedmodelling.thinklab.api.modelling.parsing.IObservingObjectDefinition;
import org.integratedmodelling.thinklab.api.modelling.parsing.IPropertyDefinition;
import org.integratedmodelling.thinklab.common.configuration.Env;
import org.integratedmodelling.thinklab.common.utils.CamelCase;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.common.vocabulary.Observable;

/* 
* @author Ferd
* 
*/
public abstract class ObservingObject<T> extends ModelObject<T> implements IObservingObject,
        IObservingObjectDefinition {

    ArrayList<IDependency>                 _dependencies        = new ArrayList<IDependency>();

    protected IFunctionCall                _accessorGenerator   = null;

    protected ArrayList<IAction>           _actions             = new ArrayList<IAction>();

    protected boolean                      _isComputed          = false;

    private final ArrayList<IFunctionCall> _extentConstraints   = new ArrayList<IFunctionCall>();
    private Scale                          _scaleCache          = null;

    protected boolean                      _integrated          = false;

    protected IScale                       _allowedContext      = null;

    ArrayList<IPropertyDefinition>         _propdefs            = new ArrayList<IPropertyDefinition>();

    protected IConcept                     _inherentSubjectType = null;

    protected IConcept                     _traitType           = null;

    public static class Dependency implements IDependency {

        // not in the model, we start with this and resolve it later at alignDependencies().
        Object      _obsObject;

        IObservable _observable;
        String      _formalName;
        IProperty   _property;
        boolean     _optional;
        boolean     _distribute;
        boolean     _generic;
        Object      _contextModel;
        int         _lineNumber = -1;
        int         _fLine, _lLine;

        public Dependency() {
        }

        public Dependency(Object cmodel, String formalName, IProperty property, boolean optional,
                boolean distribute, boolean generic, Object contextModel, int lineNumber) {
            this._obsObject = cmodel;
            this._formalName = formalName;
            this._property = property;
            this._optional = optional;
            this._generic = generic;
            this._distribute = distribute;
            this._lineNumber = lineNumber;
            this._contextModel = contextModel;
        }

        public Dependency(IObservable observable, String formalName, IProperty property, boolean optional,
                boolean distribute, Object contextModel) {
            this._observable = observable;
            this._formalName = formalName;
            this._property = property;
            this._optional = optional;
            this._generic = false;
            this._distribute = distribute;
            this._contextModel = contextModel;
        }

        @Override
        public String getFormalName() {
            return _formalName;
        }

        @Override
        public IObservable getObservable() {
            return _observable;
        }

        @Override
        public boolean isOptional() {
            return _optional;
        }

        @Override
        public IProperty getProperty() {
            return _property;
        }

        @Override
        public boolean isDistributed() {
            return _distribute;
        }

        @Override
        public boolean isGeneric() {
            return _generic;
        }

        @Override
        public Object getContextModel() {
            return _contextModel;
        }

        @Override
        public Object getWhereCondition() {
            return null;
        }

        public int getLineNumber() {
            return _lineNumber;
        }

        @Override
        public String toString() {
            return "D" + getObservable() + (_distribute ? "*" : "");
        }

        public void dumpIndented(PrintStream out, int i) {
            // TODO Auto-generated method stub

        }

        @Override
        public void setLineNumbers(int startLine, int endLine) {
            _fLine = startLine;
            _lLine = endLine;
        }

        @Override
        public int getFirstLineNumber() {
            return _fLine;
        }

        @Override
        public int getLastLineNumber() {
            return _lLine;
        }

        /**
         * If this dependency is generic, return the list of all dependencies the
         * observation semantics imply. Call ONLY if the _generic flag is there - no
         * check is done.
         * 
         * @return
         */
        public List<IDependency> concretize() {

            ArrayList<IDependency> ret = new ArrayList<IDependency>();

            int i = 1;
            for (IConcept c : NS.getConcreteDisjointChildren(_observable.getType())) {
                Observable obs = new Observable((Observable) _observable);
                obs.setType(c);
                ret.add(new Dependency(obs, _formalName + (i++), _property, true, _distribute, _contextModel));
            }

            return ret;
        }
    }

    /*
     * non-persistent fields
     */
    ArrayList<IList> _observableDefs = new ArrayList<IList>();

    @Override
    public List<IDependency> getDependencies() {
        return _dependencies;
    }

    // protected void addObservable(IList instance, String formalName, IModel observingModel) {
    //
    // if (_observableDefs.size() > 0) {
    // _observableModels.add(observingModel);
    // }
    //
    // try {
    // if (_observableCName == null)
    // _observableCName = instance.first().toString();
    // _observables.add(Thinklab.get().entify(instance));
    // if (formalName == null) {
    // formalName = CamelCase.toLowerCase(new SemanticType(_observableCName).getLocalName(), '-');
    // }
    //
    // _observableDefs.add(instance);
    // _observableNames.add(formalName);
    //
    // } catch (ThinklabException e) {
    // throw new ThinklabRuntimeException(e);
    // }
    // }

    @Override
    public IConcept addExtentConstraintFunction(IFunctionCall extentGenerator) {

        IPrototype prototype = extentGenerator.getPrototype();
        if (prototype == null)
            /*
             * happens with unregistered functions, that should flag a warning or an error.
             */
            return null;

        _extentConstraints.add(extentGenerator);
        IConcept[] c = extentGenerator.getPrototype().getReturnTypes();
        return c.length == 0 ? null : c[0];
    }

    @Override
    public boolean hasActionsFor(IConcept observable, IConcept domainConcept) {

        for (IAction a : _actions) {
            for (IConcept d : a.getDomain()) {
                if (d.is(domainConcept)) {

                    /*
                     * TODO check observable
                     */
                    return true;
                }
            }
        }

        return false;
    }

    public boolean hasActions() {
        return _actions.size() > 0;
    }

    @Override
    public void setAccessorGeneratorFunction(IFunctionCall function) {
        _accessorGenerator = function;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.integratedmodelling.thinklab.api.modelling.parsing.IObservingObjectDefinition#addAction(org.
     * integratedmodelling.thinklab.api.knowledge.IConcept, java.lang.String,
     * org.integratedmodelling.thinklab.api.modelling.parsing.IExpressionDefinition,
     * org.integratedmodelling.thinklab.api.modelling.parsing.IExpressionDefinition, boolean)
     */
    @Override
    public void addAction(String target, int type, IExpression action, IExpression condition,
            IConcept[] domains) {
        _actions.add(new Action(target, type, action, condition, getNamespace(), domains));
    }

    @Override
    public IScale getCoverage() throws ThinklabException {

        if (_allowedContext == null) {
            _allowedContext = getScale()
                    .merge(getNamespace().getCoverage(), LogicalConnector.INTERSECTION, true);
        }

        return _allowedContext;
    }

    @Override
    public IScale getScale() {
        if (_scaleCache == null) {
            _scaleCache = new Scale();
            for (IFunctionCall f : _extentConstraints) {
                try {
                    Object o = f.call();
                    if (!(o instanceof IExtent)) {
                        throw new ThinklabValidationException("coverage function " + f.getId()
                                + " does not return an extent");
                    }
                    _scaleCache.mergeExtent((IExtent) o, true);
                } catch (Exception e) {
                    throw new ThinklabRuntimeException(e);
                }
            }
        }
        return _scaleCache;
    }

    @Override
    public List<IAction> getActions() {
        return _actions;
    }

    public IAccessor getUserDefinedAccessor(IMonitor monitor) throws ThinklabException {

        IAccessor ret = null;
        if (_accessorGenerator != null) {
            Object ds = _accessorGenerator
                    .call(this instanceof IModel ? Thinklab.c(NS.SUBJECT_ACCESSOR) : Thinklab
                            .c(NS.STATE_ACCESSOR));
            if (!(ds instanceof IAccessor)) {
                throw new ThinklabValidationException("function " + _accessorGenerator.getId()
                        + " does not return an accessor");
            }
            if (ds instanceof IMonitorable) {
                ((IMonitorable) ds).setMonitor(monitor);
            }
            ret = (IAccessor) ds;
            for (IAction action : this._actions) {
                ((Accessor) ret).addAction(action);
            }
        }

        return ret;
    }

    @Override
    public void setComputed(boolean b) {
        _isComputed = b;
    }

    @Override
    public boolean isComputed() {
        return _isComputed;
    }

    // @Override
    // public void addDependency(Object cmodel, String formalName, IPropertyDefinition property, boolean
    // optional, boolean distribute,
    // Object contextGenerator,
    // Object whereCondition,
    // IResolver resolver, int lineNumber) {
    //
    // if (cmodel instanceof IConceptDefinition) {
    // IConcept c = Thinklab.get().getConcept(((IConceptDefinition)cmodel).getName());
    // if (c == null) {
    // addError(
    // new ThinklabValidationException(
    // "cannot define a dependency on bare" +
    // ((this instanceof Model && ((Model)this).getObserver() == null) ? ", non-subject" : "") +
    // " concept " +
    // ((IConceptDefinition)cmodel).getName() +
    // ": use a model" +
    // ((this instanceof Model && ((Model)this).getObserver() == null) ?
    // ", annotate it in this namespace or make it a subject" :
    // " or annotate it in this namespace")),
    // lineNumber);
    // return;
    // }
    // cmodel = c;
    // }
    //
    // for (IDependency d : _dependencies) {
    // if (d.getFormalName() != null && d.getFormalName().equals(formalName)) {
    // resolver.onException(new ThinklabValidationException(formalName +
    // ": cannot use a dependency name more than once"), lineNumber);
    // addError();
    // }
    // }
    //
    // _dependencies.add(new Dependency(cmodel, formalName,
    // (property == null ? null : Thinklab.get().getProperty(property.getName())),
    // optional, distribute, contextGenerator, lineNumber));
    // }

    @Override
    public void addDependency(Object cmodel, IConceptDefinition trait, IConceptDefinition subject,
            String formalName, IPropertyDefinition property,
            boolean optional, boolean distribute, boolean generic, Object contextGenerator, Object whereCondition,
            IModelResolver resolver, int lineNumber) {

        for (IDependency d : _dependencies) {
            if (((Dependency) d)._formalName != null && ((Dependency) d)._formalName.equals(formalName)) {
                resolver.onException(new ThinklabValidationException(formalName
                        + ": cannot use a dependency name more than once"), lineNumber);
                addError();
            }
        }

        IConcept oconc = null;
        if (cmodel instanceof IConceptDefinition) {
            oconc = Env.c(((IConceptDefinition) cmodel).getName());
        }

        /**
         * Special cases: a bare class is turned into a classification; a bare anything 'by' trait is set as 
         * a "classification by trait" observable.
         */
        if (trait != null) {
            cmodel = new Observable(
                    Thinklab.c(((IConceptDefinition) cmodel).getName()),
                    Thinklab.c(NS.INDIRECT_OBSERVATION),
                    (subject == null ? null : Thinklab.c(subject.getName())),
                    Thinklab.c(trait.getName()),
                    formalName);
        } else if (oconc != null && NS.isClass(oconc)) {
            cmodel = Model
                    .promoteToModel(new Classification(oconc, subject, this.getNamespace()), oconc, resolver, _monitor);
        }

        _dependencies
                .add(new Dependency(cmodel, formalName, (property == null ? null : Env.KM
                        .getProperty(property.getName())), optional, distribute, generic, contextGenerator, lineNumber));
    }

    /**
     * Find or create the property that would link an observable to a concept.
     * 
     * FIXME of course must use the semantics and a reasoner more than it does, and return null or throw
     * exceptions if anything inconsistent is attempted. When that is done, remove checks in the actual
     * observing functions that call this one.
     * 
     * FIXME generalize and move to the common package in the NS class.
     * 
     * @param observable
     * @param receiver
     * @param namespace
     * @return
     * @throws ThinklabException
     */
    public static IProperty getPropertyFor(Object observable, IObservable receiver, INamespace namespace)
            throws ThinklabException {

        IProperty property = null;

        IConcept c = null;
        if (observable instanceof IConcept)
            c = (IConcept) observable;
        else if (observable instanceof IObservable)
            c = ((IObservable) observable).getType();
        else if (observable instanceof IModel)
            c = ((IModel) observable).getObservable().getType();
        else if (observable instanceof ISubject)
            c = ((ISubject) observable).getDirectType();
        else if (observable instanceof ISemanticObject<?>)
            c = ((ISemanticObject<?>) observable).getDirectType();
        else if (observable instanceof IState)
            c = ((IState) observable).getDirectType();

        if (c != null && receiver.getType() != null) {

            /*
             * FIXME: if a model is taking subjects (say a railway) but discretizes it to an observation
             * it will MAKE the observation a quality. The attribute is for the OBSERVATION, not for the
             * observable. For now adjust things looking at the model first, but we need proper treatment
             * of the semantics.
             */
            boolean isQuality = NS.isQuality(c);
            boolean isQualitySpace = NS.isClass(c);

            if (observable instanceof IModel) {
                isQuality = ((IModel) observable).getObserver() != null;
                if (isQuality) {
                    isQualitySpace = !((Observer<?>) ((IModel) observable).getObserver())
                            .isNumericTransformation();
                }
            } else if (observable instanceof IState) {
                isQuality = true;
                /*
                 * FIXME should also check isQualitySpace, but if we get here we have already created or
                 * found the property before, so it's already there and the check after this will succeed
                 * without further intelligence.
                 */
            }

            /*
             * no range to speak of if we have a dataproperty, so just look for hasxxx and return it
             * if there.
             */
            if (isQuality && namespace.getOntology().getProperty("has" + c.getLocalName()) != null) {
                return namespace.getOntology().getProperty("has" + c.getLocalName());
            }

            /*
             * TODO should put them all away and raise an exception if
             * there is ambiguity.
             */
            for (IProperty p : receiver.getType().getAllProperties()) {

                if (receiver.getType().getPropertyRange(p).contains(c)) {
                    property = p;
                    break;
                }
            }

            if (property /* still */== null) {

                boolean isObject = !(isQuality || isQualitySpace);

                ArrayList<IAxiom> axioms = new ArrayList<IAxiom>();

                axioms.add(isObject ? Axiom.ObjectPropertyAssertion("has" + c.getLocalName()) : Axiom
                        .DataPropertyAssertion("has" + c.getLocalName()));
                axioms.add(isObject ? Axiom.ObjectPropertyRange("has" + c.getLocalName(), c.toString())
                        : Axiom.DataPropertyRange("has" + c.getLocalName(), c.toString()));
                axioms.add(isObject ? Axiom.ObjectPropertyDomain("has" + c.getLocalName(),
                        receiver.toString()) : Axiom.DataPropertyDomain("has" + c.getLocalName(),
                        receiver.toString()));

                namespace.getOntology().define(axioms);

                property = namespace.getOntology().getProperty("has" + c.getLocalName());

                // TODO turn to debug or remove
                Thinklab.get()
                        .logger()
                        .info((property.isObjectProperty() ? "object" : "data") + " property " + property
                                + " created for " + c);

            } else {

                // TODO turn to debug or remove
                Thinklab.get()
                        .logger()
                        .info((property.isObjectProperty() ? "object" : "data") + " property " + property
                                + " found for " + c);

            }
        } else {

            /*
             * huh?
             */
            throw new ThinklabInternalErrorException(
                    "internal error: adding dependency on something that is not semantic: " + observable);
        }

        return property;
    }

    protected void alignDependencies(IObservable mainObs, IModelResolver resolver) {

        for (IDependency d : getDependencies()) {

            /*
             * fix up and validate the context observer if one is provided. This is what goes after the 'at'
             * [each] spec.
             */
            if (d.getContextModel() instanceof IConceptDefinition) {
                ((Dependency) d)._contextModel = Env.c(d.getContextModel().toString());
            }

            IObservable ctxo = null;
            if (d.getContextModel() instanceof IConcept) {
                ctxo = new Observable((IConcept) d.getContextModel());
            } else if (d.getContextModel() instanceof IModel) {
                ctxo = ((IObservingObject) d.getContextModel()).getObservable();
            }

            boolean ctxOk = false;
            if (ctxo != null && !NS.isObject(ctxo)) {
                addError(new ThinklabValidationException("contextual observable " + ctxo
                        + " should be a thing or agent in order to be used here"), ((Dependency) d).getLineNumber());
            } else {
                ctxOk = true;
            }

            // get whatever observable this had.
            Object oobj = ((Dependency) d)._obsObject;
            if (oobj instanceof IConceptDefinition) {
                oobj = Env.c(((IConceptDefinition) oobj).getName());
            }

            /*
             * if the dependency names a concept, it must be an observation concept and resolve to a model,
             * which we set in the dependency. MAY NOT HAVE A MODEL IF THE OBSERVABLE IS AN AGENT CONCEPT.
             */
            if (oobj instanceof IObservable) {

                ((Dependency) d)._observable = (IObservable) oobj;

            } else if (oobj instanceof IConcept) {

                IConcept concept = (IConcept) oobj;
                IModel model = null;

                IObservable annotation = Observable.getObservable(concept);

                if (annotation == null && !NS.isObject(concept) && !ctxOk) {
                    addError(new ThinklabValidationException(
                            concept
                                    + ": bare concepts in the dependencies of this model "
                                    + "should "
                                    + ((this instanceof Model && ((Model) this).getObserver() == null) ? "be subjects or "
                                            : "")
                                    + "be annotated as observations " + "of qualities using models"), ((Dependency) d)
                            .getLineNumber());

                } else if (NS.isQuality(concept) && !ctxOk) {

                    /*
                     * an annotated quality - must have a model
                     */
                    model = findObservableModel(concept, ((Dependency) d)._lineNumber);

                } else if (NS.isObject(concept) && !NS.isObject(getObservable().getType())) {

                    /*
                     * dependency is on a subject but the model is a quality model
                     */
                    addError(new ThinklabValidationException(concept
                            + ": subject dependencies are only allowed in subject models"), ((Dependency) d).getLineNumber());

                } else if (annotation != null) {

                    /*
                     * previously annotated - get that
                     */
                    ((Dependency) d)._observable = annotation;
                } else if (NS.isObject(concept)) {

                    /*
                     * OK, you want an agent and you don't care about the model. Have it your way.
                     * 
                     * TODO/FIXME: the subject type was set to the main observable. Not right - should
                     * not lookup models that are restricted to it, particularly if the dependency is
                     * for a process, but we may want to give this a good look.
                     */
                    ((Dependency) d)._observable = new Observable(concept, Env.c(NS.DIRECT_OBSERVATION),
                            null, ((Dependency) d)._formalName);
                }

                if (model != null) {
                    ((Dependency) d)._observable = new Observable(model, ((Dependency) d)._formalName);
                }

                /*
                 * only case still not handled: bare concept and direct observation. If so, make the
                 * observable concept a subclass of the observed and move on.
                 */
                if (d.getObservable() == null
                        && mainObs.getObservationType().is(Thinklab.c(NS.DIRECT_OBSERVATION))) {
                    concept.getOntology().define(Collections.singleton(Axiom.SubClass(mainObs.getType()
                            .toString(), concept.getLocalName())));
                    ((Dependency) d)._observable = new Observable(concept, Thinklab.c(NS.DIRECT_OBSERVATION),
                            CamelCase.toLowerCase(concept.getLocalName(), '-'));
                }

            } else if (oobj instanceof IModel) {
                ((Dependency) d)._observable = new Observable(((IModel) oobj), ((Dependency) d)._formalName);
            }

            if (d.getObservable() == null) {
                addError(new ThinklabValidationException(
                        "don't know how to observe "
                                + oobj
                                + "; please provide an observer, define a model before this one, or make it a subject"), ((Dependency) d)._lineNumber);
                continue;
            }

            if (d.getProperty() == null && d.getObservable().getType() != null /* happens in error */) {
                try {
                    ((Dependency) d)._property = getPropertyFor(d.getObservable(), mainObs, getNamespace());
                } catch (ThinklabException e) {
                    resolver.onException(e, ((Dependency) d).getLineNumber());
                }
            }
        }

    }

    @Override
    public void validate(IModelResolver resolver) {

    }

    @Override
    public void initialize(IModelResolver resolver) {

        super.initialize(resolver);

        /*
         * we need it all
         */
        ((Namespace) _namespace).synchronizeKnowledge(resolver);
    }

    public boolean hasUserDefinedAccessor() {
        return _accessorGenerator != null;
    }

    /*
     * We want a model that represents the observation expressed by c.
     * 
     * @param c
     * 
     * @return
     */
    protected IModel findObservableModel(IConcept c, int lineNumber) {

        IModel ret = null;

        /*
         * we should find one and only one model in the namespace defined before this to model this
         * observable.
         */
        ArrayList<IModel> candidates = new ArrayList<IModel>();
        for (IModelObject o : getNamespace().getModelObjects()) {
            if (!o.isInactive() && o instanceof IModel) {

                Model m = (Model) o;
                if (!m.hasErrors() && m.getObservable().is(c))
                    candidates.add(m);
            }
        }

        if (candidates.size() == 0) {
            addError(new ThinklabValidationException("cannot find a model in the namespace to interpret " + c
                    + ": please provide one"), lineNumber);
        } else if (candidates.size() > 1) {
            addError(new ThinklabValidationException(
                    "too many alternative models found in the namespace to interpret " + c
                            + ": please indicate one explicitly"), lineNumber);

        } else {
            ret = candidates.get(0);
        }

        return ret;
    }

    @Override
    public void setIntegrated(boolean aggr) {
        _integrated = aggr;
    }

    @Override
    public void setInherentSubjectType(IConceptDefinition concept) {
        _inherentSubjectType = concept == null ? null : Env.c(concept.getName());
    }

    @Override
    public IConcept getInherentSubjectType() {
        return _inherentSubjectType;
    }

}
