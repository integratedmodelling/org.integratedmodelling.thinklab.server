package org.integratedmodelling.thinklab.modelling.lang;

import java.util.Map;

import org.integratedmodelling.thinklab.api.lang.IPrototype;
import org.integratedmodelling.thinklab.api.lang.IModelResolver;
import org.integratedmodelling.thinklab.api.modelling.parsing.IAnnotationDefinition;
import org.integratedmodelling.thinklab.api.project.IProject;

public class Annotation extends ModelObject<Annotation> implements IAnnotationDefinition {

    String _type;
    Map<String, Object> _parameters;
    IProject _project;

    @Override
    public Map<String, Object> getParameters() {
        return _parameters;
    }

    @Override
    public String getId() {
        return _type;
    }

    @Override
    public IPrototype getPrototype() {
        return null; // Env.MMANAGER.getFunctionPrototype(getId());
    }

    @Override
    public void initialize(IModelResolver resolver) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setParameters(Map<String, Object> parameters) {
        _parameters = parameters;
    }

    @Override
    public Annotation demote() {
        return this;
    }
}
