package org.integratedmodelling.thinklab.modelling.lang;

import java.util.HashMap;
import java.util.List;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.thinklab.api.knowledge.ICondition;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.modelling.IAction;
import org.integratedmodelling.thinklab.api.modelling.scheduling.ITransition;

public class StateAccessor extends BaseStateAccessor {

    protected HashMap<String, Object> _parameters = new HashMap<String, Object>();

    int _currentStateIndex = -1;

    boolean _preprocessed = false;
    boolean _errors = false;

    public StateAccessor(List<IAction> actions, IMonitor monitor) {
        if (actions != null) {
            for (IAction action : actions) {
                addAction(action);
            }
        }
        _monitor = monitor;
    }

    protected int getStateIndex() {
        return _currentStateIndex;
    }

    @Override
    public String toString() {

        return _actions.size() == 0 ? ""
                : ("[" + _actions.get(0).toString() + "]" + (_actions.size() > 1 ? " ..." : ""));
    }

    protected Object call(ITransition transition) throws ThinklabException {

        Object ret = null;

        for (IAction action : _actions) {

            if (((Action) action).domains == null && transition == null) {
                ret = doAction((Action) action, null);
            } /* TODO use domains vs. action type*/
        }

        _parameters.clear();

        return ret;
    }

    protected Object doAction(Action action, ITransition object) throws ThinklabException {

        if (action.condition != null) {
            Object o = action.condition.eval(_parameters);
            if (!(o instanceof Boolean))
                throw new ThinklabValidationException(
                        "condition in action does not evaluate to true or false");
            if (!((Boolean) o))
                return null;
        }

        return action.action == null ? null : action.action.eval(_parameters);
    }

    @Override
    public String getName() {
        return _name;
    }

    @Override
    public List<IAction> getActions() {
        return _actions;
    }

    protected synchronized void preprocessActions() {

        for (IAction a : _actions) {
            if (((Action) a).target == null) {
                ((Action) a).target = getName();
            }
            ((Action) a).preprocess(_inputs, _outputs);
        }
        _preprocessed = true;
    }

    /*
     * resolves to nothing when we have no actions.
     * (non-Javadoc)
     * @see org.integratedmodelling.thinklab.api.modelling.IStateAccessor#process(int)
     */
    @Override
    public void process(int stateIndex) throws ThinklabException {

        _currentStateIndex = stateIndex;

        boolean hasErrors = false;

        if (!_preprocessed) {
            preprocessActions();
        }

        /**
         * expose the monitor to the action
         * TODO expose everything else we may need
         */
        if (_monitor != null) {
            _parameters.put("_monitor", _monitor);
        }

        for (IAction action : _actions) {

            Action a = (Action) action;

            if (a.type == IAction.CHANGE) {

                String subject = a.target;
                if (checkCondition(a)) {
                    try {
                        Object res = a.action.eval(_parameters);
                        _parameters.put(subject, res);
                    } catch (Throwable e) {
                        // only report the first batch of errors
                        if (_monitor != null && !_errors) {
                            _monitor.error("error computing " + a.action + ": " + e.getMessage());
                            hasErrors = true;
                        }
                    }
                }
            } else if (a.type == IAction.DO) {

                if (checkCondition(a)) {
                    a.action.eval(_parameters);
                }

            } else if (a.type == IAction.INTEGRATE) {

                /*
                 * TODO needs to react to a time transition capable of getting to
                 * the previous value.
                 */
            }
        }

        if (hasErrors) {
            _errors = true;
        }
    }

    private boolean checkCondition(Action a) throws ThinklabException {

        boolean go = true;
        if (a.condition != null) {

            Object iif = null;
            try {
                iif = a.condition.eval(_parameters);
            } catch (Exception e) {
                // only report the first batch of errors
                if (_monitor != null && !_errors) {
                    _monitor.error("error computing " + a.action + ": " + e.getMessage());
                    _errors = true;
                }
            }
            if (!(iif instanceof Boolean)) {
                // only report the first batch of errors
                if (_monitor != null && !_errors) {
                    _monitor.error(new ThinklabValidationException("condition " + a.condition
                            + " does not return true/false"));
                    _errors = true;
                }
            }
            go = (Boolean) iif;
            if (a.condition instanceof ICondition && ((ICondition) (a.condition)).isNegated()) {
                go = !go;
            }
        }
        return go;
    }

    @Override
    public void setValue(String inputKey, Object value) {
        _parameters.put(inputKey, value);
    }

    @Override
    public Object getValue(String outputKey) {
        return _parameters.get(outputKey);
    }

    @Override
    public void reset() {
        _parameters.clear();
    }

}
