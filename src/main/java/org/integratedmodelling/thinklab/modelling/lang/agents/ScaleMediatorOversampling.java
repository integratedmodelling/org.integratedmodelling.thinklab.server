package org.integratedmodelling.thinklab.modelling.lang.agents;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import org.integratedmodelling.exceptions.ThinklabResourceNotFoundException;
import org.integratedmodelling.thinklab.api.knowledge.IProperty;
import org.integratedmodelling.thinklab.api.metadata.IMetadata;
import org.integratedmodelling.thinklab.api.modelling.IObjectState;
import org.integratedmodelling.thinklab.api.modelling.IScale;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.api.modelling.agents.IObservationGraphNode;
import org.integratedmodelling.thinklab.api.space.ISpatialExtent;
import org.integratedmodelling.thinklab.api.time.ITimeInstant;
import org.integratedmodelling.thinklab.modelling.lang.agents.ScaleMediatorMetadata.ScaleMediatorTag;
import org.integratedmodelling.thinklab.modelling.states.IndexedObjectState;

import com.infomatiq.jsi.Point;
import com.infomatiq.jsi.rtree.RTree;

/**
 * Linear translation can handle any arbitrary number of dimensions. Translations are done one dimension at a
 * time.
 * 
 * @author luke
 * 
 */
public class ScaleMediatorOversampling extends ScaleMediator {

    protected static final IMetadata metadata = new ScaleMediatorMetadata(ScaleMediatorTag.linear,
            ScaleMediatorTag.oneDimensional);

    private static final int RANDOM_SAMPLES_PER_CELL = 10;
    private static final Random random = new Random(System.currentTimeMillis());

    public ScaleMediatorOversampling(ISubject agent, ObservationController controller)
            throws ThinklabResourceNotFoundException {
        super(agent, controller);
    }

    @Override
    protected Map<IProperty, IObjectState> generateTargetScale(SubjectObservation observation,
            IScale targetScale) {
        Map<IProperty, IObjectState> result = new HashMap<IProperty, IObjectState>();

        Map<IProperty, IObjectState> observedStates = observation.agentStateNode.getAgentState().getStates();
        ISpatialExtent targetSpace = targetScale.getSpace();

        // find the overlapping observedStates for each extent in the target scale
        // and populate targetState with each Monte-Carlo-computed value
        IObjectState observedState;
        ISpatialExtent targetExtent;
        IProperty observedProperty;
        for (Entry<IProperty, IObjectState> observedPropertyState : observedStates.entrySet()) {
            observedProperty = observedPropertyState.getKey();
            observedState = observedPropertyState.getValue();
            RTree index = observation.getSpatialIndex(observedProperty);

            // TODO remove cast (see comments in StateFactory.getEmptyClone())
            IndexedObjectState targetState = (IndexedObjectState) StateFactory.getEmptyClone(observedState,
                    targetScale);

            int nearestRectangle;
            float randomX, randomY;

            for (int i = 0; i < targetSpace.getValueCount(); i++) {
                targetExtent = targetSpace.getExtent(i);

                /*
                 * TODO add polygon intelligence. This just uses the rectangular bounding box, and finds the
                 * "nearest" (which I'm sure means "nearest center"). This is fine for a Monte Carlo algorithm
                 * in general, but may run into trouble with strange boundary shapes, such as crescents or
                 * wholly surrounded regions.
                 */
                double sum = 0;
                for (int j = 0; j < RANDOM_SAMPLES_PER_CELL; j++) {
                    randomX = random.nextFloat() * (float) (targetExtent.getMaxX() - targetExtent.getMinX())
                            + (float) targetExtent.getMinX();
                    randomY = random.nextFloat() * (float) (targetExtent.getMaxY() - targetExtent.getMinY())
                            + (float) targetExtent.getMinY();
                    Point point = new Point(randomX, randomY);
                    nearestRectangle = index.nearest(point);
                    // TODO how to enforce that the object state holds scalar values?
                    sum += (Double) observedState.getValue(nearestRectangle);
                }

                Double monteCarloValue = sum / RANDOM_SAMPLES_PER_CELL;
                targetState.setValue(i, monteCarloValue); // won't work with non-scalar values (see comments above and @ getEmptyClone())
            }

            result.put(observedProperty, targetState);
        }

        observation.outputStates.put(targetScale, result);
        return result;
    }

    @Override
    public void invalidate(ITimeInstant interruptTime) {
        // TODO Auto-generated method stub

    }

    @Override
    public void notify(IObservationGraphNode node) {
        // TODO Auto-generated method stub

    }
}
