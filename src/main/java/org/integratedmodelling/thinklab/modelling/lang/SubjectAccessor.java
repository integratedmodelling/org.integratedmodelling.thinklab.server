package org.integratedmodelling.thinklab.modelling.lang;

import java.util.Collection;
import java.util.HashMap;

import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.modelling.IScale;
import org.integratedmodelling.thinklab.api.modelling.IState;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.api.modelling.ISubjectAccessor;

public abstract class SubjectAccessor extends Accessor implements ISubjectAccessor {

    protected HashMap<IConcept, IState> inputs = new HashMap<IConcept, IState>();
    protected ISubject                  _subject;
    protected IScale                    _scale;

    protected IState getInput(IConcept concept) {
        for (IConcept c : inputs.keySet()) {
            if (c.is(concept)) {
                return inputs.get(c);
            }
        }
        return null;
    }

    protected Collection<IConcept> getInputKeys() {
        return inputs.keySet();
    }

    // @Override
    // public IScale preinitialize(ISubject subject, ISubject context, IScale scale, IProperty property,
    // IMonitor monitor) throws ThinklabException {
    // this._monitor = monitor;
    // this._subject = subject;
    // this._scale = scale;
    // return scale;
    // }

    protected IState getInputState(IConcept observable) {
        return inputs.get(observable);
    }

    protected Collection<IState> getInputStates() {
        return inputs.values();
    }

    // @Override
    // public final void postinitialize(ISubject subject, ISubject context, IProperty property, IMonitor
    // monitor)
    // throws ThinklabException {
    // for (IState state : (subject instanceof IExtendedSubject ? ((IExtendedSubject) subject)
    // .getNonRawStates() : subject.getStates())) {
    // inputs.put(state.getDirectType(), state);
    // }
    // }
    //
    // @Override
    // public void initialize(ISubject subject, ISubject context, IProperty property, IMonitor monitor)
    // throws ThinklabException {
    // }

}
