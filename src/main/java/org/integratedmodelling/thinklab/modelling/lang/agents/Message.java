package org.integratedmodelling.thinklab.modelling.lang.agents;

import org.integratedmodelling.thinklab.api.modelling.agents.IAgentState;
import org.integratedmodelling.thinklab.api.time.ITimeInstant;

public class Message extends Collision {
    private final String message;

    public Message(ITimeInstant time, IAgentState senderState, IAgentState recipientState, String message) {
        super(time, senderState, recipientState);
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
