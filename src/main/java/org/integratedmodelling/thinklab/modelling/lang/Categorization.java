package org.integratedmodelling.thinklab.modelling.lang;

import java.util.Set;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.annotations.Concept;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.lang.IModelResolver;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.IStateAccessor;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.api.modelling.parsing.ICategorizingObserverDefinition;
import org.integratedmodelling.thinklab.common.vocabulary.NS;

@Concept(NS.CATEGORIZING_OBSERVER)
public class Categorization extends Observer<Categorization> implements ICategorizingObserverDefinition {

    Set<String> _dictionary;

    @Override
    public void setDictionary(Set<String> dictionary) {
        _dictionary = dictionary;
    }

    @Override
    public Set<String> getDictionary() {
        return _dictionary;
    }

    @Override
    public Categorization demote() {
        return this;
    }

    public boolean isDiscrete() {
        return true;
    }

    @Override
    public IStateAccessor getMediator(IObserver observer, IMonitor monitor) throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IStateAccessor getInterpreter(IStateAccessor accessor, IMonitor monitor) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getSignature() {
        return "#categorization#";
    }

    @Override
    public IConcept getObservationType(INamespace namespace) {
        /*
         * these can really be anything, and should only be used for
         * debugging.
         */
        return Thinklab.c(NS.INDIRECT_OBSERVATION);
    }

    @Override
    public void initialize(IModelResolver resolver) {
        super.initialize(resolver);
    }

    @Override
    public void notifyModelObservable(IObservable observable, IModelResolver resolver) {
        // TODO Auto-generated method stub

    }

}
