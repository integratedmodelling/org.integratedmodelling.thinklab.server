package org.integratedmodelling.thinklab.modelling.lang.agents;

import org.integratedmodelling.thinklab.api.modelling.agents.IObservationTask;
import org.integratedmodelling.thinklab.api.modelling.scheduling.ITransition;

public class ObservationGraphNodeAgentDeath extends ObservationGraphNode {

    public ObservationGraphNodeAgentDeath(IObservationTask taskCreatingThisNodeState, ITransition transition) {
        super(null, taskCreatingThisNodeState, transition);
    }

    @Override
    public boolean canCollideWithAnything() {
        return false;
    }
}
