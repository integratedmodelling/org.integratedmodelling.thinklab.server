package org.integratedmodelling.thinklab.modelling.lang;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.lang.Axiom;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.annotations.Concept;
import org.integratedmodelling.thinklab.api.annotations.Property;
import org.integratedmodelling.thinklab.api.knowledge.IAxiom;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.lang.IModelResolver;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.modelling.IAccessor;
import org.integratedmodelling.thinklab.api.modelling.IAction;
import org.integratedmodelling.thinklab.api.modelling.IClassification;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.IStateAccessor;
import org.integratedmodelling.thinklab.api.modelling.IUnit;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.api.modelling.parsing.IClassificationDefinition;
import org.integratedmodelling.thinklab.api.modelling.parsing.ICountingObserverDefinition;
import org.integratedmodelling.thinklab.api.modelling.parsing.INamespaceDefinition;
import org.integratedmodelling.thinklab.api.modelling.parsing.IUnitDefinition;
import org.integratedmodelling.thinklab.common.utils.MiscUtilities;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.common.vocabulary.Unit;

@Concept(NS.COUNTING_OBSERVER)
public class Count extends Observer<Count> implements ICountingObserverDefinition {

    @Property(NS.HAS_UNIT_DEFINITION)
    IUnitDefinition _unitDefinition;

    @Property(NS.HAS_CLASSIFICATION)
    IClassification _discretization;

    IUnit           _unit;
    Throwable       _unitError;

    public IUnit getUnit() {
        return _unit;
    }

    @Override
    public void setUnit(IUnitDefinition unit) {
        _unitDefinition = unit;
    }

    @Override
    public Count demote() {
        return this;
    }

    // @Override
    // public void initialize() {
    //
    // try {
    // if (_unitDefinition != null) {
    // _unit = new Unit("/" + (_unitDefinition.getStringExpression()));
    // }
    // } catch (Throwable e) {
    // _unitError = e;
    // }
    // if (_discretization != null) {
    // ((IClassificationDefinition) _discretization).initialize();
    // }
    // }

    /*
     * -----------------------------------------------------------------------------------
     * accessor - it's always a mediator, either to another measurement or to a datasource
     * whose content was defined explicitly to conform to our semantics
     * -----------------------------------------------------------------------------------
     */
    public class CountAccessor extends MediatingAccessor {

        IObserver other;

        public CountAccessor(IObserver other, List<IAction> actions, IMonitor monitor, boolean interpreting) {
            super(actions, monitor, interpreting);
            this.other = other;
        }

        @Override
        public String toString() {
            return "[count: " + _unit + "]";
        }

        public IUnit getUnit() {
            return _unit;
        }

        @Override
        public Object mediate(Object object) {

            if (object == null || (object instanceof Number && Double.isNaN(((Number) object).doubleValue())))
                return Double.NaN;

            if (!(other instanceof Count)) {
                return object;
            }

            return _unit == null ? object : _unit.convert(((Number) object).doubleValue(),
                    ((Count) other).getUnit());
        }
    }

    @Override
    public void setDiscretization(IClassification classification) {
        _discretization = classification;
    }

    @Override
    public IClassification getDiscretization() {
        return _discretization;
    }

    @Override
    public String toString() {
        return "[count: " + _unit + "]";
    }

    public boolean isDiscrete() {
        return _discretization != null;
    }

    @Override
    protected void dumpIndented(PrintStream out, int indent) {

        String ind = MiscUtilities.spaces(indent);
        String in3 = MiscUtilities.spaces(3);

        out.print(ind + "count " + dumpObservable() + (_unit == null ? "" : (" in " + _unit)));
        if (getDiscretization() != null) {
            out.println();
            out.println(ind + in3 + "discretized as");
            ((org.integratedmodelling.thinklab.common.classification.Classification) getDiscretization())
                    .dumpIndented(out, indent + 6);
        }
    }

    @Override
    public IStateAccessor getMediator(IObserver observer, IMonitor monitor) throws ThinklabException {

        observer = getRepresentativeObserver(observer);

        IAccessor ret = getUserDefinedAccessor(monitor);
        if (ret != null)
            return (IStateAccessor) ret;

        /*
         * FIXME this gets called when getInterpreter should be called
         */
        if (observer instanceof Count) {

            if (!observer.getObservable().is(getObservable()))
                throw new ThinklabValidationException(
                        "counts can only mediate other counts of the same thing");

            if ((((Count) observer).getUnit() == null && _unit != null)
                    || (((Count) observer).getUnit() != null && _unit == null))
                throw new ThinklabValidationException(
                        "counts with units can only mediate other counts with units");

            if (getActions().size() == 0
                    && ((((Count) observer).getUnit() == null && _unit == null) || ((Count) observer)
                            .getUnit().equals(getUnit()))) {
                return null;
            }
        }

        return new CountAccessor(observer, getActions(), monitor, false);
    }

    @Override
    public IStateAccessor getInterpreter(IStateAccessor accessor, IMonitor monitor) {
        return new CountAccessor(this, getActions(), monitor, true);
    }

    @Override
    public boolean canInterpretDirectly(IAccessor accessor) {
        return getActions().size() == 0 && _discretization == null;
    }

    @Override
    public String getSignature() {
        return "#count#" + _unit + (_discretization == null ? "" : _discretization.getConceptSpace());

    }

    @Override
    public void validate(IModelResolver resolver) {

        super.validate(resolver);
        if (_unitError != null) {
            resolver.onException(new ThinklabValidationException("invalid unit: " + _unitDefinition),
                    this.getFirstLineNumber());
        }
        // try {
        // if (_discretization != null) {
        // /*
        // * TODO validate the continuity
        // */
        // }
        // } catch (ThinklabException e) {
        // resolver.onException(e, _discretization.getFirstLineNumber());
        // }
    }

    @Override
    public void notifyModelObservable(IObservable observable, IModelResolver resolver) {

        if (_discretization != null) {
            ((INamespaceDefinition) getNamespace()).synchronizeKnowledge(null);
            ((IClassificationDefinition) _discretization).setConceptSpace(observable, resolver);
            ((IClassificationDefinition) _discretization).initialize();
            if (!_discretization.isContiguousAndFinite()) {
                addError(new ThinklabValidationException(
                        "discretization has discontinuous and/or unbounded intervals"),
                        _discretization.getFirstLineNumber());
            }
        }

    }

    @Override
    public IConcept getObservationType(INamespace namespace) {

        /*
         * TODO specialize according to unit dimensions, and further
         * if there is a discretization
         */

        return Thinklab.c(NS.COUNT);
    }

    @Override
    public IConcept getObservedType(IConcept concept) {
        IConcept ret = NS.makeCount(concept);
        if (ret == null) {
            addError(new ThinklabValidationException("only things or agents can be counted"),
                    _firstLineNumber);
            return concept;
        }
        return ret;
    }

    @Override
    public void initialize(IModelResolver resolver) {
        super.initialize(resolver);
        try {
            if (_unitDefinition != null) {
                _unit = new Unit("/" + _unitDefinition.getStringExpression());
            }
        } catch (Throwable e) {
            _unitError = e;
        }
        if (_discretization != null) {
            ((INamespaceDefinition) getNamespace()).synchronizeKnowledge(resolver);
            ((IClassificationDefinition) _discretization).setConceptSpace(getObservable(), resolver);
            ((IClassificationDefinition) _discretization).initialize();
        }
    }

    @Override
    public double getMinimumValue() {
        return 0;
    }

    @Override
    public double getMaximumValue() {
        return Double.NaN;
    }

}
