package org.integratedmodelling.thinklab.modelling.lang.agents;

import java.util.BitSet;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.collections.Triple;
import org.integratedmodelling.exceptions.ThinklabCircularDependencyException;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IProperty;
import org.integratedmodelling.thinklab.api.knowledge.ISemanticObject;
import org.integratedmodelling.thinklab.api.lang.IList;
import org.integratedmodelling.thinklab.api.metadata.IMetadata;
import org.integratedmodelling.thinklab.api.modelling.IExtent;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.IScale;
import org.integratedmodelling.thinklab.api.modelling.IScale.Index;
import org.integratedmodelling.thinklab.api.modelling.ITopologicallyComparable;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.api.space.ISpatialExtent;
import org.integratedmodelling.thinklab.api.time.ITemporalExtent;
import org.integratedmodelling.thinklab.api.time.ITemporalSeries;
import org.integratedmodelling.thinklab.api.time.ITimeInstant;
import org.integratedmodelling.thinklab.api.time.ITimePeriod;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.time.Time;
import org.integratedmodelling.thinklab.time.literals.PeriodValue;
import org.integratedmodelling.thinklab.time.literals.TimeValue;
import org.joda.time.Interval;

public class TemporalSeries<T> implements ITemporalSeries<T> {

    // a wrapper for the typed object, which also stores start/end time for which it's valid
    private class WrappedObject {
        final ITimePeriod timePeriod;
        final T item;

        public WrappedObject(final ITimePeriod timePeriod, final T item) {
            this.timePeriod = timePeriod;
            this.item = item;
        }
    }

    // a sorted collection which can query for "greatest prior"
    private final TreeMap<ITimeInstant, WrappedObject> collection = new TreeMap<ITimeInstant, WrappedObject>();

    @Override
    public T getFirst() {
        try {
            return collection.firstEntry().getValue().item;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public T getLast() {
        try {
            return collection.lastEntry().getValue().item;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public T getAtTime(ITimeInstant time) {
        return getWrappedObjectAtTime(time).item;
    }

    @Override
    public T getPrior(ITimeInstant time) {
        Entry<ITimeInstant, WrappedObject> entry = collection.lowerEntry(time);
        if (entry == null) {
            return null;
        }
        if (entry.getValue().timePeriod.contains(time)) {
            entry = collection.lowerEntry(entry.getKey());
        }
        if (entry == null) {
            return null;
        }
        return entry.getValue().item;
    }

    @Override
    public T getFollowing(ITimeInstant time) {
        Entry<ITimeInstant, WrappedObject> earliestNextEntry = collection.ceilingEntry(time);
        // NOTE: by exclusive-inclusive semantics, key might equal time, which is OK.

        if (earliestNextEntry == null) {
            // no intervals exist which start after the query time.
            return null;
        }
        return earliestNextEntry.getValue().item;
    }

    private WrappedObject getWrappedObjectAtTime(ITimeInstant time) {
        Entry<ITimeInstant, WrappedObject> latestPriorEntry = collection.lowerEntry(time);
        if (latestPriorEntry == null) {
            // no intervals exist which start before the query time.
            return null;
        }
        WrappedObject latestPrior = latestPriorEntry.getValue();
        if (!latestPrior.timePeriod.contains(time)) {
            // the latest-starting interval prior to the query time expires before the query time,
            // so there is no matching interval for this query time.
            return null;
        }
        // the interval contains the time, so return the enclosed value.
        return latestPrior;
    }

    @Override
    public void put(ITimeInstant start, ITimeInstant end, T item) throws ThinklabException {
        Interval interval = new Interval(start.getMillis(), end.getMillis());
        put(interval, item);
    }

    public void put(Interval interval, T item) throws ThinklabException {
        PeriodValue timePeriod = new PeriodValue(interval);
        put(timePeriod, item);
    }

    @Override
    public void put(ITimePeriod timePeriod, T item) {
        ITimeInstant endTime = new TimeValue(timePeriod.getEnd().getMillis());
        WrappedObject wrapper = new WrappedObject(timePeriod, item);

        // do any existing periods overlap with what is being written?
        Entry<ITimeInstant, WrappedObject> latestPriorEntry = collection.floorEntry(endTime);
        if (latestPriorEntry != null && latestPriorEntry.getValue().timePeriod.contains(endTime)) {
            // latest prior entry overlaps the end time, so we can't write the interval.
            throw new ThinklabRuntimeException("Cannot write the interval " + wrapper + " to TemporalScale "
                    + this + " because it overlaps with interval " + latestPriorEntry + ".");
        }

        ITimeInstant startTime = new TimeValue(timePeriod.getStart().getMillis());
        collection.put(startTime, wrapper);
    }

    @Override
    public void remove(ITimeInstant time) {
        WrappedObject wrappedObject = getWrappedObjectAtTime(time);
        collection.remove(wrappedObject.timePeriod.getStart());
    }

    @Override
    public ITimePeriod shorten(ITimeInstant spliceTime) throws ThinklabException {
        WrappedObject originalWrappedObject = getWrappedObjectAtTime(spliceTime);
        T item = originalWrappedObject.item;
        ITimePeriod originalTimePeriod = originalWrappedObject.timePeriod;

        // sanity check - new end time must be within original time period
        if (!originalTimePeriod.contains(spliceTime)) {
            throw new ThinklabValidationException("shorten() was called using the new end time "
                    + spliceTime.toString() + ", which is outside the original time period "
                    + originalTimePeriod.toString());
        }

        // create the new state. We don't have to validate for overlaps because we know we're shrinking the time period.
        ITimePeriod newTimePeriod = new PeriodValue(originalTimePeriod.getStart().getMillis(),
                spliceTime.getMillis());
        WrappedObject newWrappedObject = new WrappedObject(newTimePeriod, item);
        collection.remove(originalTimePeriod.getStart());
        collection.put(originalTimePeriod.getStart(), newWrappedObject);

        return newTimePeriod;
    }

    @Override
    public ITimePeriod bisect(ITimeInstant spliceTime, T object) throws ThinklabException {
        ITimeInstant endTime = getWrappedObjectAtTime(spliceTime).timePeriod.getEnd();
        shorten(spliceTime);
        PeriodValue newTimePeriod = new PeriodValue(spliceTime.getMillis(), endTime.getMillis());
        put(newTimePeriod, object);
        return newTimePeriod;
    }

    @Override
    public Collection<T> getOverlapping(ITimePeriod timePeriod) {
        Collection<T> result = new LinkedList<T>();

        // step forward through time finding all overlapping time periods
        Entry<ITimeInstant, WrappedObject> currentEntry = collection.floorEntry(timePeriod.getStart());
        WrappedObject wrappedObject;
        while (currentEntry != null) {
            // does the next wrappedObject time period overlap the one requested?
            wrappedObject = currentEntry.getValue();
            if (!wrappedObject.timePeriod.overlaps(timePeriod)) {
                break;
            }
            result.add(wrappedObject.item);
            currentEntry = collection.higherEntry(currentEntry.getKey());
        }

        return result;
    }

    /* THE ITemporalExtent METHODS */
    @Override
    public ITemporalExtent getExtent(int stateIndex) {
        Entry<ITimeInstant, WrappedObject> result = collection.firstEntry();
        ITimeInstant currentPosition = result.getKey();
        for (int i = 0; i < stateIndex && result != null; i++) {
            result = collection.higherEntry(currentPosition);
            currentPosition = result.getKey();
        }
        return result == null ? null : result.getValue().timePeriod;
    }

    @Override
    public ITimePeriod collapse() {
        ITimeInstant start = getStart();
        ITimeInstant end = getEnd();
        if (start == null || end == null) {
            return null;
        }
        return new PeriodValue(start.getMillis(), end.getMillis());
    }

    @Override
    public ITemporalExtent intersection(IExtent other) throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ITimeInstant getStart() {
        return collection.firstKey();
    }

    @Override
    public ITimeInstant getEnd() {
        Entry<ITimeInstant, WrappedObject> lastEntry = collection.lastEntry();
        if (lastEntry == null) {
            return null;
        }
        return lastEntry.getValue().timePeriod.getEnd();
    }

    @Override
    public IConcept getDomainConcept() {
        return Time.TIME_DOMAIN;
    }

    @Override
    public IProperty getDomainProperty() {
        return Thinklab.p(NS.TEMPORAL_EXTENT_PROPERTY);
    }

    @Override
    public IProperty getCoverageProperty() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isCovered(int stateIndex) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public IExtent merge(IExtent extent, boolean force) throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Pair<ITopologicallyComparable<?>, Double> checkCoverage(ITopologicallyComparable<?> obj)
            throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isConsistent() {
        return true;
    }

    @Override
    public boolean isEmpty() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public long getValueCount() {
        return collection.size();
    }

    @Override
    public IObserver getObserver() {
        return null;
    }

    @Override
    public boolean isSpatiallyDistributed() {
        return false;
    }

    @Override
    public boolean isTemporallyDistributed() {
        return getValueCount() > 1;
    }

    @Override
    public boolean isTemporal() {
        return true;
    }

    @Override
    public boolean isSpatial() {
        return false;
    }

    @Override
    public ISpatialExtent getSpace() {
        return null;
    }

    @Override
    public ITemporalExtent getTime() {
        return this;
    }

    @Override
    public double getCoveredExtent() {
        // TODO a double that can be used to compare objects
        return asDouble();
    }

    @Override
    public double asDouble() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public boolean asBoolean() {
        return false;
    }

    @Override
    public int asInteger() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public long asLong() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public float asFloat() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public String asString() {
        return toString(); // right?
    }

    @Override
    public INamespace getNamespace() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IList getSemantics() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Object demote() {
        return this; // maybe generate an irregular grid?
    }

    @Override
    public IConcept getDirectType() {
        return Time.TIME_DOMAIN;
    }

    @Override
    public boolean is(Object other) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public int getRelationshipsCount() {
        return 0;
    }

    @Override
    public int getRelationshipsCount(IProperty property) {
        return 0;
    }

    @Override
    public ISemanticObject<?> get(IProperty property) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<Pair<IProperty, ISemanticObject<?>>> getRelationships() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<Triple<IProperty, ISemanticObject<?>, Integer>> getCountedRelationships() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<ISemanticObject<?>> getRelationships(IProperty property) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isLiteral() {
        return true;
    }

    @Override
    public boolean isConcept() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isObject() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isCyclic() {
        return false;
    }

    @Override
    public boolean isValid() {
        return getValueCount() > 0;
    }

    @Override
    public List<ISemanticObject<?>> getSortedRelationships(IProperty property)
            throws ThinklabCircularDependencyException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IMetadata getMetadata() {
        return null;
    }

    @Override
    public IObservable getObservable() {
        return null;
    }

    @Override
    public long getMultiplicity() {
        return getValueCount();
    }

    @Override
    public IExtent union(IExtent other) throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean contains(IExtent o) throws ThinklabException {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean overlaps(IExtent o) throws ThinklabException {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean intersects(IExtent o) throws ThinklabException {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public ITopologicallyComparable<IExtent> union(ITopologicallyComparable<?> other)
            throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ITopologicallyComparable<IExtent> intersection(ITopologicallyComparable<?> other)
            throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IScale getScale() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Object getValue(int index) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Iterator<Object> iterator(Index index) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int[] getDimensionSizes(boolean rowFirst) {
        return new int[] { (int) getMultiplicity() };
    }

    @Override
    public int[] getDimensionOffsets(int linearOffset, boolean rowFirst) {
        return new int[] { linearOffset };
    }

    @Override
    public Class<?> getDataClass() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public BitSet getMask() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ITopologicallyComparable<?> getExtent() {
        // TODO Auto-generated method stub
        return null;
    }

}
