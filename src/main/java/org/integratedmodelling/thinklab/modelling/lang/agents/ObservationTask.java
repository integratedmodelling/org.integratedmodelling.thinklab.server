package org.integratedmodelling.thinklab.modelling.lang.agents;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.modelling.IActiveSubject;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.api.modelling.agents.IObservationController;
import org.integratedmodelling.thinklab.api.modelling.agents.IObservationGraphNode;
import org.integratedmodelling.thinklab.api.modelling.agents.IObservationTask;
import org.integratedmodelling.thinklab.api.modelling.scheduling.ITransition;
import org.integratedmodelling.thinklab.api.time.ITimeInstant;
import org.integratedmodelling.thinklab.debug.CallTracer;

public class ObservationTask implements IObservationTask {
    // default generated id
    private static final long              serialVersionUID = 1L;
    protected final ISubject               subject;
    protected final ITimeInstant           observationTime;
    private boolean                        valid            = true;
    private long                           startedAt;

    private IObservationGraphNode          parentNode       = null;
    protected final IObservationController controller;

    public ObservationTask(ISubject subject, ITimeInstant observationTime,
            IObservationController controller) {
        this(subject, observationTime, null, controller);
    }

    public ObservationTask(ISubject subject, ITimeInstant observationTime,
            IObservationGraphNode parentNode, IObservationController controller) {
        this.subject = subject;
        this.observationTime = observationTime;
        this.parentNode = parentNode;
        this.controller = controller;
    }

    /**
     * provide temporal ordering according to each observation START time, and by END time secondarily
     */
    @Override
    public int compareTo(IObservationTask other) {
        return observationTime.compareTo(other.getObservationTime());
    }

    /**
     * TODO need to take the subject's state forward in time from its current state(s) to its state(s) as of
     * the next time step (this includes finding out what that time step is)
     */
    @Override
    public ITransition run() throws ThinklabException {
        CallTracer.indent("run()", this);
        // tell the agent (via the subjectObserver) to make the next state transition
        // NOTE: this should also set the agent to its new state (won't be done explicitly anywhere else)
        ITransition transition = ((IActiveSubject) subject).performTemporalTransitionFromCurrentState();

        // add a furtherObservations item for the next time period, if the observer survives.
        if (transition.agentSurvives()) {
            IObservationTask subsequentTaskForThisAgent = new ObservationTask(subject, transition
                    .getTransitionTimePeriod().getEnd(), controller);
            transition.addFurtherObservationTask(subsequentTaskForThisAgent);
        }

        CallTracer.unIndent();
        return transition;
    }

    @Override
    public ITimeInstant getObservationTime() {
        return observationTime;
    }

    @Override
    public void startedWorkAt(long systemTimeMilliseconds) {
        startedAt = systemTimeMilliseconds;
    }

    @Override
    public long getStartTime() {
        return startedAt;
    }

    @Override
    public void setInvalid() {
        valid = false;
    }

    @Override
    public boolean isValid() {
        return valid;
    }

    @Override
    public IObservationGraphNode getParentNode() {
        return parentNode;
    }

    @Override
    public void setParentNode(IObservationGraphNode node) {
        this.parentNode = node;
    }

    @Override
    public ISubject getSubject() {
        return subject;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " for " + subject + " at " + observationTime;
    }
}
