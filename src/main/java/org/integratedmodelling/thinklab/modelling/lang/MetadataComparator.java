/*package org.integratedmodelling.thinklab.modelling.lang;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.integratedmodelling.thinklab.api.metadata.IMetadata;

*//**
* Comparators for ranking of models obtained from the KB.
* 
* @author Ferd
*
*/
/*
public enum MetadataComparator implements Comparator<IMetadata> {

 SEMANTIC_DISTANCE_CRITERION {
     public int compare(IMetadata o1, IMetadata o2) {
         //           return Integer.valueOf(o1.getId()).compareTo(o2.getId());
         return 0;
     }
 },

 IS_RESOLVED_CRITERION {
     public int compare(IMetadata o1, IMetadata o2) {
         //            return o1.getFullName().compareTo(o2.getFullName());
         return 0;
     }
 },

 DEFAULT_METADATA_CRITERION {
     public int compare(IMetadata o1, IMetadata o2) {
         //                return o1.getFullName().compareTo(o2.getFullName());
         return 0;
     }
 },

 HAS_COVERAGE_CRITERION {
     public int compare(IMetadata o1, IMetadata o2) {
         //           return o1.getFullName().compareTo(o2.getFullName());
         return 0;
     }
 };

 public static Comparator<IMetadata> descending(final Comparator<IMetadata> other) {
     return new Comparator<IMetadata>() {
         public int compare(IMetadata o1, IMetadata o2) {
             return -1 * other.compare(o1, o2);
         }
     };
 }

 public static Comparator<IMetadata> getComparator(final MetadataComparator... multipleOptions) {
     return new Comparator<IMetadata>() {
         public int compare(IMetadata o1, IMetadata o2) {
             for (MetadataComparator option : multipleOptions) {
                 int result = option.compare(o1, o2);
                 if (result != 0) {
                     return result;
                 }
             }
             return 0;
         }
     };
 }

 // example of use
 public static void main(String[] args) {
     List<Metadata> list = null;
     Collections.sort(list, descending(getComparator(IS_RESOLVED_CRITERION, SEMANTIC_DISTANCE_CRITERION)));
 }
}
*/
