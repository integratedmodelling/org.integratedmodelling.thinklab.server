package org.integratedmodelling.thinklab.modelling.lang;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.data.ITable;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IExpression;
import org.integratedmodelling.thinklab.api.lang.IPrototype;
import org.integratedmodelling.thinklab.api.modelling.IAction;
import org.integratedmodelling.thinklab.api.modelling.parsing.IFunctionCall;
import org.integratedmodelling.thinklab.api.project.IProject;

public class LookupTableAccessor extends StateAccessor implements IFunctionCall {

    ITable _table;
    List<String> _args = new ArrayList<String>();
    int _startLine, _endLine;
    IProject _project;
    int _columnIndex = -1;
    private boolean _reported;

    public LookupTableAccessor(ITable lookupTable, List<String> args) {

        super(new ArrayList<IAction>(), null);

        _table = lookupTable;

        for (int i = 0; i < args.size(); i++) {
            if (args.get(i).equals("?")) {
                _columnIndex = i;
            } else {
                _args.add(args.get(i));
            }
        }
        if (_columnIndex < 0) {
            _columnIndex = _args.size();
        }
    }

    @Override
    public void process(int stateIndex) throws ThinklabException {

        Object[] match = new Object[_args.size()];
        for (int i = 0; i < _args.size(); i++) {
            match[i] = _parameters.get(_args.get(i));
        }

        List<Object> val = null;
        try {
            val = _table.lookup(_columnIndex, match);
        } catch (Exception e) {
            if (!_reported) {
                _monitor.error(e);
                _reported = true;
            }
        }
        Object value = val == null ? null : (val.size() > 0 ? val.get(0) : null);

        /*
         * that's cool, man.
         */
        if (value instanceof IExpression) {
            value = ((IExpression) value).eval(_parameters, (IConcept[]) null);
        }

        for (String s : _outputKeys) {
            _parameters.put(s, value);
        }

        super.process(stateIndex);
    }

    @Override
    public void setLineNumbers(int startLine, int endLine) {
        _startLine = startLine;
        _endLine = endLine;
    }

    @Override
    public int getFirstLineNumber() {
        return _startLine;
    }

    @Override
    public int getLastLineNumber() {
        return _endLine;
    }

    @Override
    public void set(String id, Map<String, Object> parameters) {
        // TODO Auto-generated method stub

    }

    @Override
    public String getId() {
        return "lookup-table";
    }

    @Override
    public IPrototype getPrototype() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Map<String, Object> getParameters() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setProject(IProject project) {
        _project = project;
    }

    @Override
    public Object call(IConcept... context) throws ThinklabException {
        return new LookupTableAccessor(_table, _args);
    }

}
