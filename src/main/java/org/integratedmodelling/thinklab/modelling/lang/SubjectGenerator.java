package org.integratedmodelling.thinklab.modelling.lang;

import java.util.ArrayList;

import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.list.ReferenceList;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IProperty;
import org.integratedmodelling.thinklab.api.lang.IList;
import org.integratedmodelling.thinklab.api.lang.IModelResolver;
import org.integratedmodelling.thinklab.api.modelling.IExtent;
import org.integratedmodelling.thinklab.api.modelling.IModel;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.api.modelling.IScale;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.api.modelling.ISubjectGenerator;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.api.modelling.parsing.IFunctionCall;
import org.integratedmodelling.thinklab.api.modelling.parsing.IModelObjectDefinition;
import org.integratedmodelling.thinklab.api.modelling.parsing.IPropertyDefinition;
import org.integratedmodelling.thinklab.common.utils.CamelCase;
import org.integratedmodelling.thinklab.common.vocabulary.Observable;
import org.integratedmodelling.thinklab.modelling.resolver.SubjectFactory;

public class SubjectGenerator extends ModelObject<SubjectGenerator> implements ISubjectGenerator,
        IModelObjectDefinition {

    IList                                        observable;
    ArrayList<IFunctionCall>                     ogens             = new ArrayList<IFunctionCall>();
    ArrayList<Pair<IPropertyDefinition, IModel>> odeps             = new ArrayList<Pair<IPropertyDefinition, IModel>>();
    ArrayList<IExtent>                           predefinedExtents = new ArrayList<IExtent>();

    class DepModel {
        IProperty property;
        IModel    model;
    }

    // I don't think it's a good idea to script the models to use at the object level - we should just
    // peruse the resolution mechanism appropriately, but who knows.
    @Deprecated
    ArrayList<DepModel> mdeps = new ArrayList<DepModel>();

    public SubjectGenerator() {
    }

    public SubjectGenerator(IConcept concept, IExtent... scale) {
        observable = ReferenceList.list(concept);
        for (IExtent e : scale) {
            predefinedExtents.add(e);
        }
        setId(CamelCase.toLowerCase(concept.getLocalName(), '-'));
    }

    public ISubject createSubject() throws ThinklabException {

        ArrayList<Object> defs = new ArrayList<Object>(observable.toCollection());
        ArrayList<IExtent> extents = new ArrayList<IExtent>();
        for (IFunctionCall fc : ogens) {

            Object state = fc.call();

            if (!(state instanceof IExtent)) {
                throw new ThinklabValidationException(
                        "cannot initialize an object's scale with a non-abstract extent (e.g. space or time)");
            }

            extents.add((IExtent) state);
        }

        // it's usually either these or the functions, but you never know.
        for (IExtent e : predefinedExtents) {
            extents.add(e);
        }

        IConcept type = Thinklab.c(defs.get(0).toString());
        IObservable obs = new Observable(type);
        IScale scale = new Scale(extents.toArray(new IExtent[extents.size()]));

        Subject ret = SubjectFactory.getSubjectByMetadata(obs, this.getNamespace(), scale, getId());

        for (IProperty p : obs.getType().getAllProperties()) {
            INamespace ns = Thinklab.get().getNamespace(p.getConceptSpace());
            if (ns == null || ((Namespace) ns).isInternal()) {
                continue;
            }

            // TODO just using the first type. Handles the base cases, but if someone wants
            // to get fancy with semantics, they're going to be disappointed.
            if (p.isFunctional()) {
                for (IConcept target : obs.getType().getPropertyRange(p)) {
                    ret.addDependency(new Observable(target), p, true);
                    break;
                }
            }
        }

        return ret;

    }

    // @Override
    // public ISubjectObserver getSubjectObserver(INamespace namespace, IMonitor monitor)
    // throws ThinklabException {
    //
    // ArrayList<Object> defs = new ArrayList<Object>(observable.toCollection());
    // ArrayList<IExtent> extents = new ArrayList<IExtent>();
    // for (IFunctionCall fc : ogens) {
    //
    // Object state = fc.call();
    //
    // if (!(state instanceof IExtent)) {
    // throw new ThinklabValidationException(
    // "cannot initialize an object's scale with a non-abstract extent (e.g. space or time)");
    // }
    //
    // extents.add((IExtent) state);
    // }
    //
    // // it's usually either these or the functions, but you never know.
    // for (IExtent e : predefinedExtents) {
    // extents.add(e);
    // }
    //
    // IConcept type = Thinklab.c(defs.get(0).toString());
    // IObservable obs = new Observable(type);
    // IScale scale = new Scale(extents.toArray(new IExtent[extents.size()]));
    //
    // Subject subject = SubjectFactory.getSubjectByMetadata(obs, this.getNamespace(), scale, getId());
    // SubjectObserver ret = new SubjectObserver(subject, monitor);
    //
    // /*
    // * find all (non-core, i.e. user-defined) properties that apply to this type, and insert dependencies
    // * for each of them - mandatory if functional, optional otherwise.
    // *
    // * TODO the handling of observation vs. restrictions is imperfect and not easy to get right. FIXME
    // * this should go in the subject observer constructor, not here.
    // */
    // for (IProperty p : subject.getObservable().getType().getAllProperties()) {
    // INamespace ns = Thinklab.get().getNamespace(p.getConceptSpace());
    // if (ns == null || ((Namespace) ns).isInternal()) {
    // continue;
    // }
    //
    // // TODO just using the first type. Handles the base cases, but if someone wants
    // // to get fancy with semantics, they're going to be disappointed.
    // if (p.isFunctional()) {
    // for (IConcept target : subject.getObservable().getType().getPropertyRange(p)) {
    // ret.addDependency(new Observable(target), p, true);
    // break;
    // }
    // }
    // }
    //
    // return ret;
    // }

    // @Override
    // public ISubject observe(INamespace namespace, IMonitor monitor, List<String> scenarios)
    // throws ThinklabException {
    //
    // ISubjectObserver ret = getSubjectObserver(namespace, monitor);
    // ICoverage coverage = ret.initialize(scenarios);
    //
    // if (coverage.isEmpty()) {
    // monitor.error("observation of " + observable + " only covers " + coverage.getCoverage()
    // + " of context");
    // }
    //
    // return ret.run();
    //
    // }

    @Override
    public void setObservable(IList odef) {
        observable = odef;
    }

    @Override
    public void addObservationGeneratorFunction(IFunctionCall ff) {
        /*
         * TODO validation of return types
         */
        ogens.add(ff);
    }

    @Override
    public void addModelDependency(IPropertyDefinition property, IModel observer) {
        odeps.add(new Pair<IPropertyDefinition, IModel>(property, observer));
    }

    @Override
    public SubjectGenerator demote() {
        return this;
    }

    @Override
    public void initialize(IModelResolver resolver) {

        /*
         * check that 'propagate' status only applies to object properties
         */
        for (Pair<IPropertyDefinition, IModel> mm : odeps) {

            IProperty p = Thinklab.get().getProperty(mm.getFirst().getName());
            if (p == null) {
                addError(new ThinklabValidationException("internal error: property "
                        + mm.getFirst().getName() + " is unknown"), getFirstLineNumber());
                continue;
            }

            DepModel dp = new DepModel();
            dp.property = p;
            dp.model = mm.getSecond();

            mdeps.add(dp);
        }
    }

    /**
     * Overriding to prevent SemanticObject.toString() from failing on _semantics == null
     */
    @Override
    public String toString() {
        return "SG[" + _namespaceId + ":" + _id + "]";
    }
}
