package org.integratedmodelling.thinklab.modelling.lang;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.list.ReferenceList;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IProperty;
import org.integratedmodelling.thinklab.api.lang.IModelResolver;
import org.integratedmodelling.thinklab.api.lang.IReferenceList;
import org.integratedmodelling.thinklab.api.modelling.IActiveSubject;
import org.integratedmodelling.thinklab.api.modelling.ICoverage;
import org.integratedmodelling.thinklab.api.modelling.IDataset;
import org.integratedmodelling.thinklab.api.modelling.IExtent;
import org.integratedmodelling.thinklab.api.modelling.IModel;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.api.modelling.IObjectState;
import org.integratedmodelling.thinklab.api.modelling.IObservation;
import org.integratedmodelling.thinklab.api.modelling.IObservingObject.IDependency;
import org.integratedmodelling.thinklab.api.modelling.IProcess;
import org.integratedmodelling.thinklab.api.modelling.IScale;
import org.integratedmodelling.thinklab.api.modelling.IState;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.api.modelling.agents.IAgentState;
import org.integratedmodelling.thinklab.api.modelling.agents.ICollision;
import org.integratedmodelling.thinklab.api.modelling.agents.IObservationController;
import org.integratedmodelling.thinklab.api.modelling.agents.IObservationGraphNode;
import org.integratedmodelling.thinklab.api.modelling.agents.IObservationWorker;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.api.modelling.resolution.IResolutionStrategy;
import org.integratedmodelling.thinklab.api.modelling.scheduling.ITransition;
import org.integratedmodelling.thinklab.api.time.ITemporalExtent;
import org.integratedmodelling.thinklab.api.time.ITimePeriod;
import org.integratedmodelling.thinklab.common.utils.NameGenerator;
import org.integratedmodelling.thinklab.common.vocabulary.Observable;
import org.integratedmodelling.thinklab.debug.CallTracer;
import org.integratedmodelling.thinklab.modelling.datasets.CFdataset;
import org.integratedmodelling.thinklab.modelling.interfaces.IExtendedSubject;
import org.integratedmodelling.thinklab.modelling.lang.ObservingObject.Dependency;
import org.integratedmodelling.thinklab.modelling.lang.agents.AgentState;
import org.integratedmodelling.thinklab.modelling.lang.agents.ObservationController;
import org.integratedmodelling.thinklab.modelling.lang.agents.ObservationWorker;
import org.integratedmodelling.thinklab.modelling.lang.agents.TemporalCausalGraph;
import org.integratedmodelling.thinklab.modelling.resolver.Coverage;
import org.integratedmodelling.thinklab.modelling.resolver.ResolutionStrategy;
import org.integratedmodelling.thinklab.modelling.resolver.Resolver;
import org.integratedmodelling.thinklab.modelling.scheduling.TemporalTransition;
import org.integratedmodelling.thinklab.modelling.states.State;

public class Subject extends ModelObject<Object> implements IExtendedSubject, Cloneable {

    Object                                                       _object                    = null;
    Scale                                                        _scale                     = null;
    IObservable                                                  _observable                = null;
    /*
     * internal ID for observation paths
     */
    protected String                                             _internalID                = NameGenerator
                                                                                                    .shortUUID();

    ArrayList<Pair<IProperty, IState>>                           _states                    = new ArrayList<Pair<IProperty, IState>>();
    ArrayList<Pair<IProperty, ISubject>>                         _subjects                  = new ArrayList<Pair<IProperty, ISubject>>();
    ArrayList<Pair<IProperty, IProcess>>                         _processes                 = new ArrayList<Pair<IProperty, IProcess>>();
    ArrayList<IExtent>                                           _extents                   = new ArrayList<IExtent>();

    boolean                                                      _initialized               = false;

    // set from Context at each new resolution.
    long                                                         _contextId                 = -1;

    // resolution may set this
    IModel                                                       _model                     = null;
    IDataset                                                     _backingDataset            = null;

    // index value of _scale.getTime().getExtent(i) corresponding to agent's temporal location
    private int                                                  currentTemporalExtentIndex = 0;
    private ITimePeriod                                          currentTimePeriodCache     = null;
    private TemporalCausalGraph<ISubject, IObservationGraphNode> causalGraph;

    private ResolutionStrategy                                   _resolutionStrategy        = new ResolutionStrategy();

    // ArrayList<Triple<IObservable, IProperty, Boolean>> _depData = new ArrayList<Triple<IObservable,
    // IProperty, Boolean>>();

    ArrayList<IDependency>                                       _dependencies              = new ArrayList<IDependency>();

    @Override
    public IObservable getObservable() {
        return _observable;
    }

    public Subject(IObservable type, Collection<IExtent> extents, Object object, INamespace namespace,
            String name) {
        _semantics = ReferenceList.list(type.getType());
        _observable = type;
        for (IExtent e : extents) {
            _extents.add(e);
        }
        _object = object;
        _namespace = namespace;
        setId(name);
    }

    public Subject(IReferenceList semantics, Collection<IExtent> extents, Object object,
            INamespace namespace, String name) {
        _semantics = semantics;
        IConcept type = Thinklab.c(semantics.first().toString());
        _observable = new Observable(type);
        for (IExtent e : extents) {
            _extents.add(e);
        }
        _object = object;
        _namespace = namespace;
        setId(name);
    }

    @Override
    public Collection<IState> getStates() {
        ArrayList<IState> ret = new ArrayList<IState>();
        for (Pair<IProperty, IState> pd : _states) {
            ret.add(pd.getSecond());
        }
        return ret;
    }

    @Override
    public Collection<IState> getNonRawStates() {

        HashMap<IConcept, IState> ret = new HashMap<IConcept, IState>();
        for (Pair<IProperty, IState> pd : _states) {

            IConcept key = pd.getSecond().getDirectType();

            /*
             * only substitute an existing one if the one in the result set is raw. When only a raw one is
             * available, use it.
             */
            if (ret.containsKey(key)) {
                if (((State) (ret.get(key))).isRaw()) {
                    ret.put(key, pd.getSecond());
                }
            } else {
                ret.put(key, pd.getSecond());
            }
        }
        return ret.values();

    }

    @Override
    public Map<IProperty, IObjectState> getObjectStateCopy() {
        HashMap<IProperty, IObjectState> result = new HashMap<IProperty, IObjectState>();
        IState state;
        for (Pair<IProperty, IState> pd : _states) {
            state = pd.getSecond();
            if (state instanceof IObjectState) {
                result.put(pd.getFirst(), (IObjectState) state);
            }
        }
        return result;
    }

    @Override
    public Object demote() {
        if (_object == null) {
            try {
                _object = Thinklab.get().instantiate(_semantics);
            } catch (ThinklabException e) {
                throw new ThinklabRuntimeException(e);
            }
        }
        return _object;
    }

    @Override
    public IScale getScale() {

        if (_scale == null) {
            _scale = new Scale();
            for (IExtent extent : _extents) {
                try {
                    _scale.mergeExtent(extent, true);
                } catch (ThinklabException e) {
                    // shouldn't happen
                    throw new ThinklabRuntimeException(e);
                }
            }
        }
        return _scale;
    }

    @Override
    public void addState(IState s) throws ThinklabException {
        CallTracer.msg("addState(" + s + ")", this);
        IProperty p = ObservingObject.getPropertyFor(s, getObservable(), getNamespace());
        _states.add(new Pair<IProperty, IState>(p, s));
    }

    /**
     * Re-evaluate states given the new temporal location of this agent. The agent has access to the current
     * states during execution of this method because they have not yet changed. The agent is expected to
     * update the state values and set them in the result.
     *
     * this method is intended to be overridden by subclasses as appropriate.
     * @param timePeriod
     *
     * @return
     */
    @Override
    public ITransition reEvaluateStates(ITimePeriod timePeriod) {
        // TODO either call resolve() somehow, or mimic its behavior
        CallTracer.indent("reEvaluateStates()", this, timePeriod);
        Map<IProperty, IObjectState> currentStates = getObjectStateCopy();

        IAgentState agentState = new AgentState(this, timePeriod, currentStates);

        // TODO this is the suburban version: nothing ever happens.
        ITransition noTransition = new TemporalTransition(agentState, true);

        CallTracer.unIndent();
        return noTransition;
    }

    @Override
    public ISubject clone() {

        Subject ret = new Subject(getObservable(), _extents, _object, _namespace, _id);
        ret._states.addAll(_states);
        ret._subjects.addAll(_subjects);

        return ret;
    }

    @Override
    public void addSubject(ISubject subject) throws ThinklabException {

        IProperty property = ObservingObject.getPropertyFor(subject, this.getObservable(), this
                .getNamespace());
        CallTracer.indent("addSubject()", this, subject, property);
        _subjects.add(new Pair<IProperty, ISubject>(property, subject));
        CallTracer.unIndent();
    }

    @Override
    public Collection<ISubject> getSubjects() {
        ArrayList<ISubject> ret = new ArrayList<ISubject>();
        for (Pair<IProperty, ISubject> pd : _subjects) {
            ret.add(pd.getSecond());
        }
        return ret;
    }

    public IObservation get(String path) {

        if (path.startsWith("/")) {
            path = path.substring(1);
        }
        String[] s = path.split("\\/");
        IObservation ret = this;

        for (String id : s) {

            for (IState state : getStates()) {
                // states have no children
                if (((State) state).getInternalId().equals(id))
                    return state;
            }
            for (ISubject subject : getSubjects()) {
                if (((Subject) subject)._internalID.equals(id))
                    ret = subject;
            }
        }

        return ret;
    }

    @Override
    public void initialize(IModelResolver resolver) {
        // TODO Auto-generated method stub
    }

    @Override
    public void removeStates() {
        _states.clear();
    }

    @Override
    public String toString() {
        return "S" + getObservable();
    }

    @Override
    public ICollision detectCollision(IObservationGraphNode myAgentState,
            IObservationGraphNode otherAgentState) {
        // TODO not going to implement this. Collision detection is extremely difficult.
        return null;
    }

    @Override
    public boolean doesThisCollisionAffectYou(IAgentState agentState, ICollision collision) {
        // TODO not going to implement this. Collision detection is extremely difficult.
        return false;
    }

    public String getInternalID() {
        return _internalID;
    }

    @Override
    public void setScale(IScale s) {
        _scale = (Scale) s;
    }

    @Override
    public void contextualize() throws ThinklabException {

        if (!_initialized) {
            if (initialize(new ArrayList<String>()).isEmpty())
                return;
        }

        CallTracer.indent("run()", this);
        ITemporalExtent time = _scale.getTime();
        if (time == null) {
            // the subject doesn't proceed through time
            CallTracer.msg("scale for root subject is atemporal");
        } else {
            CallTracer.msg("scale for root subject contains time dimension.");

            CallTracer.msg("creating ObservationController...");
            causalGraph = new TemporalCausalGraph<ISubject, IObservationGraphNode>();
            IObservationController controller = new ObservationController(causalGraph, _monitor,
                    time.getEnd());

            // add the root subject (and its children) to the controller's causal graph
            // insert the FIRST time period of the temporal extent
            ITimePeriod simulationTimePeriod = time.collapse();
            addSubjectToObservationGraph(this, simulationTimePeriod, controller, null);

            // TODO for now, only create one observation worker for single-threaded demonstration
            IObservationWorker worker = new ObservationWorker(controller, _monitor, _contextId);
            worker.run();
            // TODO should this return the controller? or root subject?
        }
    }

    public void addDependency(Observable observable, IProperty p, boolean b) {
        _dependencies
                .add(new Dependency(observable, observable.getFormalName(), p, b, false, null));

    }

    @Override
    public ICoverage observe(IObservable observable, Collection<String> scenarios, boolean isOptional)
            throws ThinklabException {

        ICoverage ret = Coverage.EMPTY;
        IProperty property = ObservingObject
                .getPropertyFor(observable, this.getObservable(), this.getNamespace());

        boolean distribute = observable.getModel() != null && observable.getModel().isReificationModel();

        if (observable.getInherentType() != null && !_observable.getType().is(observable.getInherentType())) {
            // TODO needs another strategy limited to user "drop" actions - this would prevent legitimate
            // observations.
            // throw new ThinklabValidationException("this observable requires a "
            // + observable.getInherentType() + " as context");
        }

        _dependencies
                .add(new Dependency(observable, observable.getFormalName(), property, isOptional, distribute, null));

        if (_initialized) {
            try {
                ret = resolve(scenarios);
            } catch (Exception e) {
                _monitor.error(e);
            } finally {
                _dependencies.clear();
            }
        }

        return ret;
    }

    public ICoverage initialize(Collection<String> scenarios) throws ThinklabException {

        ICoverage ret = Coverage.EMPTY;
        try {
            ret = resolve(scenarios);
        } finally {
            _dependencies.clear();
        }
        _initialized = true;
        return ret;
    }

    private ICoverage resolve(Collection<String> scenarios) throws ThinklabException {

        Resolver resolver = new Resolver();
        return resolver.resolve(this, scenarios, _monitor);
    }

    public List<IDependency> getDependencies() {
        return _dependencies;
    }

    public IDataset requireBackingDataset(long contextId) throws ThinklabException {

        if (_backingDataset == null) {
            _backingDataset = new CFdataset(this, _monitor, contextId);
        }
        return _backingDataset;
    }

    /**
     * Recursively add the subject and all its dependencies to the observation graph (which is contained in
     * the controller parameter). Keep a list of what's been added already, to speed up performance, avoid
     * infinite recursion, and avoid duplicating the initial observation tasks for the agents.
     * 
     * @param subjectObserver
     * @param simulationTimePeriod
     * @param controller
     * @param alreadyAdded
     */
    private void addSubjectToObservationGraph(ISubject subject,
            ITimePeriod simulationTimePeriod, IObservationController controller,
            HashSet<ISubject> alreadyAdded) {
        CallTracer.indent("addSubjectToObservationGraph()", this, subject, simulationTimePeriod);

        if (alreadyAdded == null) {
            alreadyAdded = new HashSet<ISubject>();
        }
        if (alreadyAdded.contains(subject)) {
            CallTracer.msg("subject has already been added! returning without doing any work.");
        } else {
            alreadyAdded.add(subject);

            // recursively add each other subject to the controller's causal graph
            // NOTE: do this first so that the observation queue causes dependencies to be observed before
            // dependents.
            // This would work either way, but this way doesn't cause sleep/wait at the start of an
            // observation.
            CallTracer.msg("adding subject's children...");
            for (ISubject child : subject.getSubjects()) {
                addSubjectToObservationGraph(child, simulationTimePeriod, controller, alreadyAdded);
            }

            CallTracer.msg("adding subject...");
            ITimePeriod agentCreationTimePeriod = simulationTimePeriod;
            ITemporalExtent subjectTime = subject.getScale().getTime();
            if (subjectTime != null) {
                // agent has its own temporal scale, so don't inherit the default. Start with extent 0.
                agentCreationTimePeriod = subjectTime.getExtent(0).collapse();
            }
            AgentState initialState = new AgentState(subject, agentCreationTimePeriod,
                    ((IActiveSubject) subject).getObjectStateCopy());
            controller.createAgent(subject, initialState, agentCreationTimePeriod, null, null, true);
        }
        CallTracer.unIndent();
    }

    /**
     * Default implementation for Subjects (non-agents) without any explicit view of time. This means that
     * they can be re-evaluated over time by repeating the same observations, but they do not perform any
     * logic or application of rules based on states or state transitions.
     */
    @Override
    public ITransition performTemporalTransitionFromCurrentState() {
        ITransition result;
        CallTracer.indent("performTemporalTransitionFromCurrentState()", this);

        // set the clock forward
        if (moveToNextTimePeriod() == null) {
            // we went beyond the agent's last temporal extent, so the agent dies
            result = new TemporalTransition(null, false);
        } else {
            // TODO re-evaluate states by repeating the observations.
            result = reEvaluateStates(getCurrentTimePeriod());
        }

        CallTracer.unIndent();
        return result;
    }

    private ITimePeriod moveToNextTimePeriod() {

        if (getCurrentTimePeriod() == null) {
            // if we're already beyond the valid index numbers, then don't increment any more.
            return null;
        }

        // set the clock forward & invalidate the cache
        currentTemporalExtentIndex++;
        currentTimePeriodCache = null;

        // reload the cache and validate that we're still within the subject's temporal scale
        ITimePeriod result = getCurrentTimePeriod();
        if (result == null) {
            // TODO either extend the scale or the agent must die
            // currently, the agent dies by default
        }

        return result;
    }

    /**
     * get the TemporalExtent (which is also a TimePeriod) of the agent's current temporal location. Will
     * return null if the index has gone beyond the agent's temporal scale.
     * 
     * @return
     */
    private ITimePeriod getCurrentTimePeriod() {
        if (currentTimePeriodCache == null) {
            ITemporalExtent time = _scale.getTime();
            if (time != null) {
                if (currentTemporalExtentIndex >= time.getMultiplicity())
                    return null;
                ITemporalExtent extent = time.getExtent(currentTemporalExtentIndex);
                currentTimePeriodCache = extent.collapse();
            }
        }
        return currentTimePeriodCache;
    }

    public TemporalCausalGraph<ISubject, IObservationGraphNode> getCausalGraph() {
        return causalGraph;
    }

    public void setContextId(long contextId) {
        // TODO Auto-generated method stub
        _contextId = contextId;
    }

    public long getContextId() {
        return _contextId;
    }

    public IDataset getBackingDataset() {
        return _backingDataset;
    }

    @Override
    public IResolutionStrategy getResolutionStrategy() {
        return _resolutionStrategy;
    }

    public boolean isInitialized() {
        return _initialized;
    }

    @Override
    public Collection<IProcess> getProcesses() {
        ArrayList<IProcess> ret = new ArrayList<IProcess>();
        for (Pair<IProperty, IProcess> pd : _processes) {
            ret.add(pd.getSecond());
        }
        return ret;
    }

}
