package org.integratedmodelling.thinklab.modelling.lang.agents;

import java.util.Map;

import org.integratedmodelling.exceptions.ThinklabResourceNotFoundException;
import org.integratedmodelling.thinklab.api.knowledge.IProperty;
import org.integratedmodelling.thinklab.api.metadata.IMetadata;
import org.integratedmodelling.thinklab.api.modelling.IObjectState;
import org.integratedmodelling.thinklab.api.modelling.IScale;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.api.modelling.agents.IObservationGraphNode;
import org.integratedmodelling.thinklab.api.time.ITimeInstant;
import org.integratedmodelling.thinklab.modelling.lang.agents.ScaleMediatorMetadata.ScaleMediatorTag;

/**
 * Nearest Neighbor translation can handle any arbitrary number of dimensions.
 * 
 * @author luke
 * 
 */
public class ScaleMediatorNearestNeighbor extends ScaleMediator {

    protected static final IMetadata metadata = new ScaleMediatorMetadata(
            ScaleMediatorTag.arbitraryDimensions, ScaleMediatorTag.stepFunction);

    public ScaleMediatorNearestNeighbor(ISubject agent, ObservationController controller)
            throws ThinklabResourceNotFoundException {
        super(agent, controller);
    }

    @Override
    protected Map<IProperty, IObjectState> generateTargetScale(SubjectObservation observation,
            IScale targetScale) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void invalidate(ITimeInstant interruptTime) {
        // TODO Auto-generated method stub

    }

    @Override
    public void notify(IObservationGraphNode node) {
        // TODO Auto-generated method stub

    }
}
