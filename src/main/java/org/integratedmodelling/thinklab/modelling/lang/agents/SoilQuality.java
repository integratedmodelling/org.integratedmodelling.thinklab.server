package org.integratedmodelling.thinklab.modelling.lang.agents;

import java.util.Collection;
import org.integratedmodelling.thinklab.api.modelling.IExtent;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;

/**
 * To be used in the Proof of Concept for the Master Thesis (Scenario 1: Soil Erosion)
 *
 * @author luke
 */
public class SoilQuality extends AgentDeliberative {

    // mimics the constructor used by SubjectObserver.createDependentSubjects():
    //    Subject(observable, ((Scale) scale).getExtents(), null, namespace, name)
    public SoilQuality(IObservable type, Collection<IExtent> extents, Object object, INamespace namespace,
            String name) {
        super(type, extents, object, namespace, name);
    }
}
