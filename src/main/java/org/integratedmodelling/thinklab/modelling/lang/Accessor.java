package org.integratedmodelling.thinklab.modelling.lang;

import java.util.ArrayList;
import java.util.List;

import org.integratedmodelling.common.HashableObject;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.listeners.IMonitorable;
import org.integratedmodelling.thinklab.api.modelling.IAccessor;
import org.integratedmodelling.thinklab.api.modelling.IAction;

public abstract class Accessor extends HashableObject implements IAccessor, IMonitorable {

    protected String _name;
    protected ArrayList<IAction> _actions = new ArrayList<IAction>();
    protected IMonitor _monitor;

    public void addAction(IAction action) {
        if (((Action) action).target == null)
            ((Action) action).target = getName();
        _actions.add(action);
    }

    @Override
    public void setMonitor(IMonitor monitor) {
        _monitor = monitor;
    }

    public IMonitor getMonitor() {
        return _monitor;
    }

    public void setName(String name) {
        _name = name;
    }

    @Override
    public String getName() {
        return _name;
    }

    @Override
    public List<IAction> getActions() {
        return _actions;
    }

}
