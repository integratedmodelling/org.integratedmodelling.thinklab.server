package org.integratedmodelling.thinklab.modelling.lang;

import java.io.PrintStream;
import java.util.List;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.lang.IModelResolver;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.modelling.IAccessor;
import org.integratedmodelling.thinklab.api.modelling.IAction;
import org.integratedmodelling.thinklab.api.modelling.IClassification;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.IStateAccessor;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.api.modelling.parsing.IClassificationDefinition;
import org.integratedmodelling.thinklab.api.modelling.parsing.IClassifyingObserverDefinition;
import org.integratedmodelling.thinklab.api.modelling.parsing.IConceptDefinition;
import org.integratedmodelling.thinklab.api.modelling.parsing.INamespaceDefinition;
import org.integratedmodelling.thinklab.common.configuration.Env;
import org.integratedmodelling.thinklab.common.utils.CamelCase;
import org.integratedmodelling.thinklab.common.utils.MiscUtilities;
import org.integratedmodelling.thinklab.common.vocabulary.NS;

public class Classification extends Observer<Classification> implements IClassifyingObserverDefinition {

    IClassification _classification;
    String          _metadataProperty;
    boolean         _isDiscretization;

    public Classification() {
    }

    public Classification(IConcept classObservable, IConceptDefinition subject, INamespace namespace) {
        this.setInherentSubjectType(subject);
        this.setNamespace((INamespaceDefinition) namespace);
        this.addObservable(new ConceptObject((INamespaceDefinition) Env.MMANAGER.getNamespace(classObservable
                .getConceptSpace()), classObservable
                .getLocalName()));
        this.setId(CamelCase.toLowerCase(classObservable.getLocalName(), '-'));
    }

    @Override
    public IClassification getClassification() {
        return _classification;
    }

    @Override
    public Classification demote() {
        return this;
    }

    @Override
    public void setClassification(IClassification classification) {
        _classification = classification;
    }

    @Override
    public void initialize(IModelResolver resolver) {

        super.initialize(resolver);

        ((INamespaceDefinition) getNamespace()).synchronizeKnowledge(null);

        if (_metadataProperty != null) {
            _classification = NS.createClassificationFromMetadata(getObservable(), _metadataProperty);
            // } else if (_classification != null) {
            // ((IClassificationDefinition)_classification).setConceptSpace(getObservableConcept(), resolver);
            // if (getMediatedObserver() != null && getMediatedObserver() instanceof IRankingObserver &&
            // ((IRankingObserver)(getMediatedObserver())).getType() == IRankingObserver.BINARY_CODING) {
            // ((IClassificationDefinition)_classification).setBooleanRanking(true);
            // }
            // ((IClassificationDefinition)_classification).initialize();
        } else {
            /*
             * TODO should only be admitted in a dependency.
             * it's for reference in a dependency: will link to another but we need to avoid ambiguity.
             */
        }
    }

    @Override
    public void notifyModelObservable(IObservable observable, IModelResolver resolver) {

        if (_classification != null) {
            ((INamespaceDefinition) getNamespace()).synchronizeKnowledge(null);
            ((IClassificationDefinition) _classification).setConceptSpace(observable, resolver);
            ((IClassificationDefinition) _classification).initialize();
        }

    }

    /*
     * -----------------------------------------------------------------------------------
     * accessor - it's always a mediator, either to another measurement or to a datasource
     * whose content was defined explicitly to conform to our semantics
     * -----------------------------------------------------------------------------------
     */
    public class ClassificationAccessor extends MediatingAccessor {

        public ClassificationAccessor(List<IAction> actions, IMonitor monitor) {
            super(actions, monitor, false);
            if (_classification != null)
                ((org.integratedmodelling.thinklab.common.classification.Classification) _classification)
                        .reset();
        }

        @Override
        public String toString() {
            return "[classification: "
                    + (_classification == null ? "null" : _classification.getConceptSpace()) + "]";
        }

        @Override
        public Object mediate(Object object) {
            return _classification.classify(object);
        }
    }

    @Override
    public String toString() {
        return "[classification: " + (_classification == null ? "null" : _classification.getConceptSpace())
                + "]";
    }

    public boolean isDiscrete() {
        return true;
    }

    @Override
    public void setMetadataEncodingProperty(String metadataProperty) {
        _metadataProperty = metadataProperty;
    }

    @Override
    protected void dumpIndented(PrintStream out, int indent) {

        String ind = MiscUtilities.spaces(indent);
        String in3 = MiscUtilities.spaces(3);

        out.print(ind + "classify " + getObservable());
        out.println();
        out.println(ind + in3 + "discretized as");
        ((org.integratedmodelling.thinklab.common.classification.Classification) _classification)
                .dumpIndented(out, indent + 6);
    }

    @Override
    public IStateAccessor getMediator(IObserver observer, IMonitor monitor) throws ThinklabException {

        IAccessor ret = getUserDefinedAccessor(monitor);
        if (ret != null)
            return (IStateAccessor) ret;

        if (observer instanceof Classification
                && observer.getActions().size() == 0
                && (_classification == null || _classification
                        .isIdentical(((Classification) observer)._classification)))
            return null;

        return new ClassificationAccessor(getActions(), monitor);
    }

    @Override
    public boolean canInterpretDirectly(IAccessor accessor) {
        /*
         * classifications must always be seen through, data
         * don't come classified, unless we are an empty 'classify' statement
         * which just bridges to whatever classification it's asked to mediate.
         */
        if (accessor instanceof ClassificationAccessor && _classification == null)
            return true;

        return false;
    }

    @Override
    public IStateAccessor getInterpreter(IStateAccessor accessor, IMonitor monitor) {
        if (_classification == null)
            return accessor;
        return new ClassificationAccessor(getActions(), monitor);
    }

    @Override
    public String getSignature() {
        return "#classification#" + (_classification == null ? "NULL" : _classification.getConceptSpace())
                + "#";
    }

    @Override
    public IConcept getObservationType(INamespace namespace) {
        return Thinklab.c(NS.CLASSIFICATION);
    }

    @Override
    public void setIsDiscretization(boolean b) {
        _isDiscretization = b;
    }

    @Override
    public IConcept getObservedType(IConcept concept) {
        return NS.makeClassification(concept);
    }

}
