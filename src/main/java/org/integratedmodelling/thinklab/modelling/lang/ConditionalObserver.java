package org.integratedmodelling.thinklab.modelling.lang;

import java.util.ArrayList;
import java.util.List;

import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.lang.LogicalConnector;
import org.integratedmodelling.thinklab.api.annotations.Concept;
import org.integratedmodelling.thinklab.api.annotations.Property;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.ICondition;
import org.integratedmodelling.thinklab.api.knowledge.IExpression;
import org.integratedmodelling.thinklab.api.lang.IModelResolver;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.modelling.IAccessor;
import org.integratedmodelling.thinklab.api.modelling.IAction;
import org.integratedmodelling.thinklab.api.modelling.IModel;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.IScale;
import org.integratedmodelling.thinklab.api.modelling.IStateAccessor;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.api.modelling.parsing.IConditionalObserverDefinition;
import org.integratedmodelling.thinklab.api.modelling.parsing.IModelDefinition;
import org.integratedmodelling.thinklab.api.modelling.parsing.INamespaceDefinition;
import org.integratedmodelling.thinklab.api.modelling.parsing.IObserverDefinition;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.modelling.interfaces.ISwitchingAccessor;
import org.integratedmodelling.thinklab.modelling.lang.expressions.GroovyExpression;

@Concept(NS.CONDITIONAL_OBSERVER)
public class ConditionalObserver extends Observer<ConditionalObserver> implements
        IConditionalObserverDefinition {

    // flag to only report script errors once.
    boolean _reported = false;

    @Property(NS.HAS_OBSERVERS)
    ArrayList<Pair<IModel, IExpression>> _observers = new ArrayList<Pair<IModel, IExpression>>();

    public class ConditionalAccessor extends StateAccessor implements ISwitchingAccessor {

        public ConditionalAccessor(List<IAction> actions, IMonitor monitor) {
            super(actions, monitor);
        }

        boolean _preprocessed = false;

        @Override
        public void process(int stateIndex) throws ThinklabException {
        }

        @Override
        public boolean getConditionValue(int condition) throws ThinklabException {

            if (!_preprocessed) {
                preprocess();
            }

            IExpression cond = _observers.get(condition).getSecond();
            try {
                Object iif = cond.eval(_parameters);
                if (!(iif instanceof Boolean)) {
                    _monitor.error(new ThinklabValidationException(
                            "condition in observer does not evaluate to true or false"));
                }
                if (cond instanceof ICondition && ((ICondition) cond).isNegated()) {
                    iif = !((Boolean) iif);
                }
                return (Boolean) iif;
            } catch (Exception e) {
                if (!_reported) {
                    _reported = true;
                    throw new ThinklabValidationException(e);
                }
            }
            return false;
        }

        private synchronized void preprocess() {
            _preprocessed = true;
            for (Pair<IModel, IExpression> zio : _observers) {
                if (zio.getSecond() instanceof GroovyExpression) {
                    ((GroovyExpression) zio.getSecond()).initialize(_inputs, _outputs);
                }
            }
        }

        @Override
        public int getConditionsCount() {
            return _observers.size();
        }

        @Override
        public boolean isNullCondition(int condition) {
            return _observers.get(condition).getSecond() == null;
        }

        @Override
        public String toString() {
            return "switch " + getName();
        }

        @Override
        public void notifyConditionalAccessor(IAccessor accessor, int conditionIndex) {
        }

    }

    public ConditionalObserver() {
    }

    public ConditionalObserver(String id, INamespace namespace) {
        setId(id);
        setNamespace((INamespaceDefinition) namespace);
    }

    public void addObserver(IExpression expression, IModelDefinition observer) {
        observer.setId(getId());
        _observers.add(new Pair<IModel, IExpression>(observer, (IExpression) expression));
    }

    public List<Pair<IModel, IExpression>> getObservers() {
        return _observers;
    }

    @Override
    public ConditionalObserver demote() {
        return this;
    }

    @Override
    public void setId(String id) {
        super.setId(id);
        for (Pair<IModel, IExpression> od : _observers) {
            ((IModelDefinition) (od.getFirst())).setId(id);
            ((IObserverDefinition) (od.getFirst().getObserver())).setId(id);
        }
    }

    @Override
    public void initialize(IModelResolver resolver) {

        super.initialize(resolver);
        for (Pair<IModel, IExpression> oo : _observers) {
            ((Model) (oo.getFirst())).initialize(resolver);
        }
    }

    @Override
    public void validate(IModelResolver resolver) {

        super.validate(resolver);

        /*
         * ensure all models are identical.
         * TODO this should use the ObservationType and the reasoner, but for
         * now we don't automatically mediate compatible observers, so we require
         * exact match.
         */
        String observerSignature = null;
        for (Pair<IModel, IExpression> zio : _observers) {

            ((Model) (zio.getFirst())).validate(resolver);

            if (observerSignature == null) {
                observerSignature = ((Observer<?>) (zio.getFirst().getObserver())).getSignature();
            } else {
                String os = ((Observer<?>) (zio.getFirst().getObserver())).getSignature();
                if (!observerSignature.equals(os)) {
                    resolver.onException(new ThinklabValidationException(
                            "the observers in a conditional statement are not compatible"), zio.getFirst()
                            .getFirstLineNumber());
                    addError();
                }
            }

        }

    }

    @Override
    public void notifyModelObservable(IObservable observable, IModelResolver resolver) {
        for (Pair<IModel, IExpression> oo : _observers) {
            ((IObserverDefinition) (oo.getFirst().getObserver())).notifyModelObservable(observable, resolver);
        }
    }

    @Override
    public IStateAccessor getMediator(IObserver observer, IMonitor monitor) throws ThinklabException {
        return new ConditionalAccessor(getActions(), monitor);
    }

    @Override
    public IStateAccessor getInterpreter(IStateAccessor accessor, IMonitor monitor) {
        return new ConditionalAccessor(getActions(), monitor);
    }

    @Override
    public IScale getCoverage() throws ThinklabException {

        IScale ret = super.getCoverage();
        for (Pair<IModel, IExpression> zio : _observers) {
            ret = ret.merge(zio.getFirst().getCoverage(), LogicalConnector.UNION, true);
        }
        return ret;
    }

    @Override
    public List<Pair<IModel, IExpression>> getModels() {
        return _observers;
    }

    @Override
    public void addModel(IExpression condition, IModelDefinition observer) {
        _observers.add(new Pair<IModel, IExpression>(observer, condition));
    }

    @Override
    public IObserver getRepresentativeObserver() {

        IObserver ret = this;
        do {
            ret = ((ConditionalObserver) ret)._observers.get(0).getFirst().getObserver();
        } while (ret instanceof ConditionalObserver);

        return ret;
    }

    @Override
    public String getSignature() {

        String ret = "#conditional#";
        for (Pair<IModel, IExpression> zio : _observers) {
            ret += ((Observer<?>) (zio.getFirst().getObserver())).getSignature();
        }
        return ret;
    }

    @Override
    public IConcept getObservationType(INamespace namespace) {
        return getRepresentativeObserver().getObservationType(null);
    }

    @Override
    public IObservable getObservable() {
        return getRepresentativeObserver().getObservable();
    }

    @Override
    public void setModel(IModelDefinition model) {
        super.setModel(model);
        for (Pair<IModel, IExpression> zio : _observers) {
            ((Observer<?>) (zio.getFirst().getObserver())).setModel(model);
        }
    }

    @Override
    public String toString() {
        return "[conditional switch]";
    }

}
