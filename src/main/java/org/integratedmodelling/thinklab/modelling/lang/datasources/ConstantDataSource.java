package org.integratedmodelling.thinklab.modelling.lang.datasources;

import java.util.ArrayList;
import java.util.List;

import org.integratedmodelling.common.HashableObject;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.metadata.IMetadata;
import org.integratedmodelling.thinklab.api.modelling.IAccessor;
import org.integratedmodelling.thinklab.api.modelling.IAction;
import org.integratedmodelling.thinklab.api.modelling.IDataSource;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.IScale;
import org.integratedmodelling.thinklab.modelling.interfaces.IRawAccessor;
import org.integratedmodelling.thinklab.modelling.lang.Metadata;
import org.integratedmodelling.thinklab.modelling.lang.Scale;
import org.integratedmodelling.thinklab.modelling.lang.StateAccessor;

/**
 * A datasource that returns the same object no matter what.
 * 
 * @author Ferd
 * 
 */
public class ConstantDataSource extends HashableObject implements IDataSource {

    private Object    _state    = null;
    private IMetadata _metadata = new Metadata();

    class ConstantAccessor extends StateAccessor implements IRawAccessor {

        public ConstantAccessor(List<IAction> actions, IMonitor monitor) {
            super(actions, monitor);
        }

        @Override
        public Object getValue(String outputKey) {
            return _state;
        }

        @Override
        public String getDatasourceLabel() {
            return "[constant: " + _state + "]";
        }

        @Override
        public String toString() {
            return _state == null ? "null" : _state.toString();
        }

        /*
         * TODO override createStates() to create a simple state without storage.
         */
    }

    public ConstantDataSource(Object state) {
        _state = state;
    }

    @Override
    public IAccessor getAccessor(IScale context, IObserver observer, IMonitor monitor)
            throws ThinklabException {
        return new ConstantAccessor(new ArrayList<IAction>(), monitor);
    }

    @Override
    public IMetadata getMetadata() {
        return _metadata;
    }

    @Override
    public IScale getCoverage() {
        return new Scale();
    }

    public String toString() {
        return _state.toString();
    }

}
