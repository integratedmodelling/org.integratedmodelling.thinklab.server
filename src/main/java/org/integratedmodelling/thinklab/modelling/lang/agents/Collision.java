package org.integratedmodelling.thinklab.modelling.lang.agents;

import org.integratedmodelling.thinklab.api.modelling.agents.IAgentState;
import org.integratedmodelling.thinklab.api.modelling.agents.ICollision;
import org.integratedmodelling.thinklab.api.time.ITimeInstant;

/**
 * this represents an interruption of an agent-state which is caused by some
 * outside force. Agents change their own states by their own decisions; collisions cause states to change
 * without agents' decisions.
 * 
 * Also, collisions do not have to respect the colliding agents' schedules. They can happen at any time, and
 * so they have the ability to alter the agents' temporal scales.
 * 
 * Sometimes the causingAgent/impactedAgent don't matter and sometimes they do. When they do, the "causing"
 * agent is the one taking action. For instance, if the collision represents a message being sent, the
 * causingAgent is the sender and the impactedAgent is the recipient.
 * 
 * @author luke
 * 
 */
public class Collision implements ICollision {
    private final ITimeInstant collisionTime;
    private final IAgentState causingAgentState;
    private final IAgentState impactedAgentState;

    public Collision(ITimeInstant collisionTime, IAgentState causingAgentState, IAgentState impactedAgentState) {
        this.collisionTime = collisionTime;
        this.causingAgentState = causingAgentState;
        this.impactedAgentState = impactedAgentState;
    }

    @Override
    public ITimeInstant getCollisionTime() {
        return collisionTime;
    }

    @Override
    public IAgentState getCausingAgent() {
        return causingAgentState;
    }

    @Override
    public IAgentState getImpactedAgent() {
        return impactedAgentState;
    }
}
