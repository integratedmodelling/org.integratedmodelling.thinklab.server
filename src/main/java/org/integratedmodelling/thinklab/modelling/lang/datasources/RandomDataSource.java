package org.integratedmodelling.thinklab.modelling.lang.datasources;

import java.util.ArrayList;
import java.util.List;

import org.integratedmodelling.common.HashableObject;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.metadata.IMetadata;
import org.integratedmodelling.thinklab.api.modelling.IAccessor;
import org.integratedmodelling.thinklab.api.modelling.IAction;
import org.integratedmodelling.thinklab.api.modelling.IDataSource;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.IScale;
import org.integratedmodelling.thinklab.modelling.interfaces.IRawAccessor;
import org.integratedmodelling.thinklab.modelling.lang.Metadata;
import org.integratedmodelling.thinklab.modelling.lang.Scale;
import org.integratedmodelling.thinklab.modelling.lang.StateAccessor;
import org.integratedmodelling.thinklab.modelling.random.DistributionValue;

/**
 * A datasource that returns the same object no matter what.
 * 
 * @author Ferd
 *
 */
public class RandomDataSource extends HashableObject implements IDataSource {

    private DistributionValue _state = null;
    private IMetadata _metadata = new Metadata();

    class RandomAccessor extends StateAccessor implements IRawAccessor {

        public RandomAccessor(List<IAction> actions, IMonitor monitor) {
            super(actions, monitor);
        }

        @Override
        public Object getValue(String outputKey) {
            return _state.draw();
        }

        @Override
        public String getDatasourceLabel() {
            return "[random from: " + _state.getName() + "]";
        }
    }

    public RandomDataSource(String distribution, double... parameters) {
        _state = new DistributionValue(distribution, parameters);
    }

    @Override
    public IAccessor getAccessor(IScale context, IObserver observer, IMonitor monitor)
            throws ThinklabException {
        return new RandomAccessor(new ArrayList<IAction>(), monitor);
    }

    @Override
    public IMetadata getMetadata() {
        return _metadata;
    }

    @Override
    public IScale getCoverage() {
        return new Scale();
    }

    public String toString() {
        return _state.toString();
    }

}
