package org.integratedmodelling.thinklab.modelling.lang.agents;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.api.modelling.agents.ICollision;
import org.integratedmodelling.thinklab.api.modelling.agents.IObservationController;
import org.integratedmodelling.thinklab.api.modelling.agents.IObservationGraphNode;
import org.integratedmodelling.thinklab.api.modelling.scheduling.ITransition;

public class ObservationTaskCollisionHandling extends ObservationTask {

    // default generated ID
    private static final long serialVersionUID = 1L;
    private final ICollision  collision;

    public ObservationTaskCollisionHandling(ISubject subject, ICollision collision,
            IObservationGraphNode parentAgentState, IObservationController controller) {
        super(subject, collision.getCollisionTime(), parentAgentState, controller);
        this.collision = collision;
    }

    @Override
    public ITransition run() throws ThinklabException {
        // TODO terminate agent-state at collision time
        // TODO recursively invalidate dependent agent-states
        // TODO create new tasks to re-observe invalidated agent-states (influential relationships only)
        // TODO create new tasks to observe the collided agents
        return null;
    }

    public ICollision getCollision() {
        return collision;
    }
}
