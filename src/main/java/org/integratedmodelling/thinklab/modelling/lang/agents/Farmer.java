package org.integratedmodelling.thinklab.modelling.lang.agents;

import java.util.Collection;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.annotations.SubjectType;
import org.integratedmodelling.thinklab.api.modelling.IExtent;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.api.modelling.IState;
import org.integratedmodelling.thinklab.api.modelling.agents.IAgentState;
import org.integratedmodelling.thinklab.api.modelling.agents.ICollision;
import org.integratedmodelling.thinklab.api.modelling.agents.IObservationGraphNode;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;

/**
 * To be used in the Proof of Concept for the Master Thesis (Scenario 1: Soil Erosion)
 *
 * TODO move to test packages
 * @author luke
 */
@SubjectType("thinklab.agents.sandbox.agents:Farmer")
public class Farmer extends AgentDeliberative {

    // mimics the constructor used by SubjectObserver.createDependentSubjects():
    // Subject(observable, ((Scale) scale).getExtents(), null, namespace, name)
    public Farmer(IObservable type, Collection<IExtent> extents, Object object, INamespace namespace,
            String name) {
        super(type, extents, object, namespace, name);
    }

    @Override
    public void addState(IState s) throws ThinklabException {
        // TODO Auto-generated method stub
        super.addState(s);
    }

    @Override
    public ICollision detectCollision(IObservationGraphNode myAgentState,
            IObservationGraphNode otherAgentState) {
        // TODO Auto-generated method stub
        return super.detectCollision(myAgentState, otherAgentState);
    }

    @Override
    public boolean doesThisCollisionAffectYou(IAgentState agentState, ICollision collision) {
        // TODO Auto-generated method stub
        return super.doesThisCollisionAffectYou(agentState, collision);
    }

}
