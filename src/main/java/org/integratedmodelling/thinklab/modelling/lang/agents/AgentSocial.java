package org.integratedmodelling.thinklab.modelling.lang.agents;

import java.util.Collection;

import org.integratedmodelling.thinklab.api.annotations.SubjectType;
import org.integratedmodelling.thinklab.api.modelling.IExtent;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.common.vocabulary.NS;

@SubjectType(NS.SOCIAL_AGENT)
public class AgentSocial extends AgentDeliberative {

    public AgentSocial(IObservable type, Collection<IExtent> extents, Object object, INamespace namespace,
            String name) {
        super(type, extents, object, namespace, name);
        // TODO Auto-generated constructor stub
    }
}
