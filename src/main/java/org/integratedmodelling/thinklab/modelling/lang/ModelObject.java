package org.integratedmodelling.thinklab.modelling.lang;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Observer;

import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabInternalErrorException;
import org.integratedmodelling.lang.Axiom;
import org.integratedmodelling.lang.SemanticType;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.annotations.Concept;
import org.integratedmodelling.thinklab.api.annotations.Property;
import org.integratedmodelling.thinklab.api.knowledge.IAxiom;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IProperty;
import org.integratedmodelling.thinklab.api.knowledge.ISemanticObject;
import org.integratedmodelling.thinklab.api.lang.IModelResolver;
import org.integratedmodelling.thinklab.api.metadata.IMetadata;
import org.integratedmodelling.thinklab.api.modelling.IAnnotation;
import org.integratedmodelling.thinklab.api.modelling.IModel;
import org.integratedmodelling.thinklab.api.modelling.IModelObject;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.api.modelling.IState;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.api.modelling.parsing.IMetadataDefinition;
import org.integratedmodelling.thinklab.api.modelling.parsing.IModelObjectDefinition;
import org.integratedmodelling.thinklab.api.modelling.parsing.INamespaceDefinition;
import org.integratedmodelling.thinklab.common.vocabulary.NS;

@Concept(NS.MODEL_OBJECT)
public abstract class ModelObject<T> extends LanguageElement<T> implements IModelObject,
        IModelObjectDefinition {

    @Property(NS.HAS_ID)
    String     _id;

    @Property(NS.HAS_NAMESPACE_ID)
    String     _namespaceId;

    boolean    _isPrivate  = false;
    boolean    _isInactive = false;

    /*
     * no @Property
     * store without namespace to avoid chain effect of trying to store the whole thing
     * for each stored object. We only store the namespace ID to be able to retrieve
     * objects by namespace.
     */
    INamespace _namespace;

    /*
     * no @Property
     * don't store metadata as our Kbox implementation floats them to the
     * top object level for easier searching.
     */
    IMetadata  _metadata   = new Metadata();

    protected void dumpIndented(PrintStream out, int indent) {
    }

    @Override
    public boolean isPrivate() {
        return _isPrivate;
    }

    @Override
    public void setPrivate(boolean b) {
        _isPrivate = b;
    }

    @Override
    public boolean isInactive() {
        return _isInactive || _namespace.isInactive();
    }

    @Override
    public void setInactive(boolean b) {
        _isInactive = b;
    }

    public void initializeFromStorage() {

        if (_namespace == null && _namespaceId != null) {
            _namespace = Thinklab.get().getNamespace(_namespaceId);
        }
    }

    @Override
    public void initialize(IModelResolver resolver) {
    }

    @Override
    public void dump(PrintStream out) {

        dumpIndented(out, 0);
        /*
         * dump metadata
         */
        Metadata md = (Metadata) getMetadata();
        if (md._data.size() > 0) {
            out.println("  with metadata {");
            for (String s : md._data.keySet()) {
                out.println("   " + s + " " + md.getString(s));
            }
            out.println("  }");
        }
    }

    public String dump() {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream out = new PrintStream(baos);
        dump(out);
        out.flush();
        return baos.toString();
    }

    @Override
    public String getName() {
        return _namespaceId + "." + _id;
    }

    @Override
    public IMetadata getMetadata() {
        return _metadata;
    }

    @Override
    public void setNamespace(INamespaceDefinition namespace) {
        _namespace = (INamespace) namespace;
        _namespaceId = namespace.getId();
    }

    @Override
    public void setMetadata(IMetadataDefinition metadata) {
        _metadata = (IMetadata) metadata;
    }

    @Override
    public INamespace getNamespace() {
        return _namespace;
    }

    @Override
    public String getId() {
        return _id;
    }

    @Override
    public void setId(String id) {

        if (SemanticType.validate(id)) {
            SemanticType st = new SemanticType(id);
            _namespace = Thinklab.get().getNamespace(st.getConceptSpace());
            _id = st.getLocalName();
        } else {
            _id = id;
        }
    }

    /**
     * This one is an initializer that is called by the resolver ONLY on the top-level models 
     * and subject generators defined in the specifications. Does nothing except in IModels, where it
     * sets up the information needed for proper storage and retrieval of observers.
     */
    public void initializeTopLevelObject() {

    }

    /**
     * Simple dumper for POD objects
     * @param object
     * @return
     */
    public static String dumpLiteral(Object object) {

        if (object instanceof String) {
            return "\"" + object + "\"";
        } else if (object instanceof ModelObject<?>) {
            return ((ModelObject<?>) object).dump();
        }

        return object.toString();
    }

    /*
     * to add an error when we deal with the error messages separately
     */
    protected void addError() {
        _errorCount++;
    }

    @Override
    public Throwable addError(Throwable error, int lineNumber) {
        _errors.add(new Pair<Throwable, Integer>(error, lineNumber));
        return error;
    }

    @Override
    public boolean hasErrors() {
        return _errors.size() > 0 || _errorCount > 0;
    }

    /*
     * to accumulate errors in initialize() and report them later to validate().
     */
    protected ArrayList<Pair<Throwable, Integer>> _errors = new ArrayList<Pair<Throwable, Integer>>();

    @Override
    public void addAnnotation(IAnnotation annotation) {
        _annotations.add(annotation);
    }

    @Override
    public Collection<IAnnotation> getAnnotations() {
        return _annotations;
    }

    ArrayList<IAnnotation> _annotations = new ArrayList<IAnnotation>();

    protected int          _errorCount  = 0;
}
