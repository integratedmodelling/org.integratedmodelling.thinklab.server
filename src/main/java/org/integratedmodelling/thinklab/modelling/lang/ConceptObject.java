package org.integratedmodelling.thinklab.modelling.lang;

import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.annotations.Concept;
import org.integratedmodelling.thinklab.api.lang.IModelResolver;
import org.integratedmodelling.thinklab.api.modelling.parsing.IConceptDefinition;
import org.integratedmodelling.thinklab.api.modelling.parsing.INamespaceDefinition;
import org.integratedmodelling.thinklab.common.vocabulary.NS;

@Concept(NS.CONCEPT_DEFINITION)
public class ConceptObject extends ModelObject<ConceptObject> implements IConceptDefinition {

    public ConceptObject() {
    }

    public ConceptObject(INamespaceDefinition namespace, String id) {
        setNamespace(namespace);
        setId(id);
    }

    @Override
    public ConceptObject demote() {
        return this;
    }

    @Override
    public String getName() {

        /*
         * namespace == null only happens in error, but let it through so
         * we don't get a null pointer exception, and we report the error 
         * anyway.
         */
        String ns = "UNDEFINED";
        if (getNamespace() != null)
            ns = getNamespace().getId();
        else
            Thinklab.get().logger().error("ZIO CALAMARO.");

        return ns + ":" + _id;
    }

    @Override
    public String toString() {
        return getNamespace() == null ? _id : getName();
    }

    @Override
    public void initialize(IModelResolver resolver) {
        // TODO Auto-generated method stub

    }

}
