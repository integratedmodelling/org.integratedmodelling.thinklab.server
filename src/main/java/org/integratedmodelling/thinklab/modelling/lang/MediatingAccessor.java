package org.integratedmodelling.thinklab.modelling.lang;

import java.util.List;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.modelling.IAction;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;

public abstract class MediatingAccessor extends StateAccessor {

    /*
     * if true, this accessor is used only to interpret data, so it should
     * not mediate.
     */
    boolean _interpreting;

    public MediatingAccessor(List<IAction> actions, IMonitor monitor, boolean isInterpreting) {
        super(actions, monitor);
        _interpreting = isInterpreting;
    }

    IObserver _mediatedObserver = null;

    protected abstract Object mediate(Object object);

    @Override
    public void notifyInput(IObservable observable, IObserver observer, String key, boolean isMediation)
            throws ThinklabException {
        if (isMediation) {
            _mediatedObserver = observer;
        }
    }

    @Override
    public Object getValue(String outputKey) {
        return (!_interpreting && outputKey.equals(getName())) ? mediate(super.getValue(outputKey)) : super
                .getValue(outputKey);
    }

    protected IObserver getMediatedObserver() {
        return _mediatedObserver;
    }

}
