package org.integratedmodelling.thinklab.modelling.lang.agents;

import java.util.Map;
import java.util.Map.Entry;

import org.integratedmodelling.exceptions.ThinklabResourceNotFoundException;
import org.integratedmodelling.thinklab.api.knowledge.IProperty;
import org.integratedmodelling.thinklab.api.metadata.IMetadata;
import org.integratedmodelling.thinklab.api.modelling.IObjectState;
import org.integratedmodelling.thinklab.api.modelling.IScale;
import org.integratedmodelling.thinklab.api.modelling.IState;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.api.modelling.agents.IObservationGraphNode;
import org.integratedmodelling.thinklab.api.space.ISpatialExtent;
import org.integratedmodelling.thinklab.api.time.ITimeInstant;
import org.integratedmodelling.thinklab.modelling.lang.agents.ScaleMediatorMetadata.ScaleMediatorTag;

/**
 * Linear translation can handle any arbitrary number of dimensions. Translations are done one dimension at a
 * time.
 * 
 * @author luke
 * 
 */
public class ScaleMediatorLinear extends ScaleMediator {

    protected static final IMetadata metadata = new ScaleMediatorMetadata(ScaleMediatorTag.linear,
            ScaleMediatorTag.oneDimensional);

    public ScaleMediatorLinear(ISubject agent, ObservationController controller)
            throws ThinklabResourceNotFoundException {
        super(agent, controller);
    }

    @Override
    protected Map<IProperty, IObjectState> generateTargetScale(SubjectObservation observation,
            IScale targetScale) {
        Map<IProperty, IObjectState> result = null;

        Map<IProperty, IObjectState> propertyStates = observation.agentStateNode.getAgentState().getStates();
        ISpatialExtent targetSpace = targetScale.getSpace();

        IProperty observedProperty;
        IState observedState;
        ISpatialExtent observedSpace;
        for (Entry<IProperty, IObjectState> propertyState : propertyStates.entrySet()) {
            observedProperty = propertyState.getKey();
            observedState = propertyState.getValue();
            IState targetState = StateFactory.getEmptyClone(observedState, targetScale);

            /*
             *  TODO
             * 
             *  for (extent : space.extents)
             *  index(extent) by extent.envelope
             * 
             *  (probably do this for the target scale as well?)
             * 
             *  triangulate observed extents via indexed values
             *  (this is the intermediate representation)
             * 
             *  TODO is this a "triangulation" interpolator? (linear is 1-D equivalent)
             */
        }

        observation.outputStates.put(targetScale, result);
        return result;
    }

    @Override
    public void invalidate(ITimeInstant interruptTime) {
        // TODO Auto-generated method stub

    }

    @Override
    public void notify(IObservationGraphNode node) {
        // TODO Auto-generated method stub

    }
}
