package org.integratedmodelling.thinklab.modelling.interfaces;

import org.integratedmodelling.thinklab.api.modelling.IDataset;
import org.integratedmodelling.thinklab.api.modelling.IState;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;

/**
 * R/W dataset with "live" storage of one time slice at a time. Used as a backing store for 
 * subject states during contextualization.
 * 
 * @author Ferd
 *
 */
public interface IExtendedDataset extends IDataset {

    public interface Var {

        /**
         * 
         * @param index the FULL scale offset, including the time
         * @param value
         */
        void setValue(int index, Object value);

        /**
         * 
         * @param index
         * @return
         */
        Object getValue(int index);

        /**
         * Set values from double array, converting appropriately or throwing a
         * runtime error if non-numeric.
         * @param data
         */
        void setFromDouble(double[] data);

        /**
         * Get number of values in CURRENT view.
         * @return
         */
        long getValueCount();

    }

    /**
     * Get a Var object to do I/O on the passed time slice. Var must exist in dataset. Returns null
     * if not found.
     * 
     * @param observable
     * @return
     */
    Var getVar(IObservable observable, int timeSlice);

    /**
     * Get a Var object to do I/O on the passed time slice, creating as necessary. Should be implemented so that
     * random access does not have large performance costs.
     * 
     * @param observable
     * @return
     */
    Var getVar(IState observable, int timeSlice);
}
