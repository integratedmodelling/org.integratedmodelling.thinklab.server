package org.integratedmodelling.thinklab.modelling.interfaces;

import org.integratedmodelling.thinklab.api.modelling.IState;

public interface IModifiableState extends IState {

    /**
     * Set the value corresponding to the scale index given.
     * 
     * @param index
     * @param value
     */
    void setValue(int index, Object value);

    /**
     * Redefined in disk-backed datasets; called at the end of each temporal cycle (or init)
     * to notify that current data setting is over and the next setValue() will be on another
     * time slice. 
     * 
     * @param tSlice the time slice we've just finished writing. Passed -1 after initialization.
     */
    public void flush(int tSlice);
}
