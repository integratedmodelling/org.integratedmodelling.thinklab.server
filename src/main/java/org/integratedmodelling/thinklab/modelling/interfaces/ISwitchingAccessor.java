package org.integratedmodelling.thinklab.modelling.interfaces;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.modelling.IAccessor;
import org.integratedmodelling.thinklab.api.modelling.IStateAccessor;

/*
 * Switching accessor, used by a ConditionalAccessor to allow the VM to determine which
 * observer in a switch should be given control to.
 * 
 * TODO this doesn't need to be a IStateAccessor - all it needs from it is setValue, 
 * but I'm not sure what happens in the compiler if I do it now.
 */
public interface ISwitchingAccessor extends IStateAccessor {

    /**
     * Return the number of conditional observers we're handling.
     * @return
     */
    public int getConditionsCount();

    /**
     * True if the condition with the given index is null - meaning we're just
     * asking to stop at the first non-nodata result.
     * 
     * @param condition
     * @return
     */
    public boolean isNullCondition(int condition);

    /**
     * Compute the condition indexed by the passed index and return its 
     * result, which must be a boolean.
     * 
     * @param condition
     * @return
     * @throws ThinklabException 
     */
    public boolean getConditionValue(int condition) throws ThinklabException;

    /**
     * Notify the accessor that will be executed for the given condition index.
     * @param accessor
     * @param conditionIndex
     */
    public void notifyConditionalAccessor(IAccessor accessor, int conditionIndex);
}
