package org.integratedmodelling.thinklab.modelling.interfaces;

/*
 * interface to tag accessors that provide access to raw datasources. Provides
 * a label to add to a state's description.
 */
public interface IRawAccessor {

    /*
     * Only for display and naming of states.
     */
    public String getDatasourceLabel();
}
