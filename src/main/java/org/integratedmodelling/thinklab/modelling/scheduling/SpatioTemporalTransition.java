package org.integratedmodelling.thinklab.modelling.scheduling;

import java.util.Collection;
import java.util.Map;

import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.thinklab.api.knowledge.IProperty;
import org.integratedmodelling.thinklab.api.modelling.IObjectState;
import org.integratedmodelling.thinklab.api.modelling.agents.IAgentState;
import org.integratedmodelling.thinklab.api.modelling.agents.IObservationGraphNode;
import org.integratedmodelling.thinklab.api.modelling.agents.IObservationTask;
import org.integratedmodelling.thinklab.api.modelling.scheduling.ITransition;
import org.integratedmodelling.thinklab.api.time.ITimeInstant;
import org.integratedmodelling.thinklab.api.time.ITimePeriod;

/**
 * For use with agents that can move around. This transition is used to express motion through
 * both space and time.
 *
 * @author luke
 *
 */
public class SpatioTemporalTransition implements ITransition {

    @Override
    public ITimePeriod getTransitionTimePeriod() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean agentSurvives() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public IAgentState getAgentState() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Collection<IObservationTask> getFurtherObservationTasks() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void addFurtherObservationTask(IObservationTask subsequentTaskForThisAgent) {
        // TODO Auto-generated method stub

    }

    @Override
    public Collection<Pair<ITimeInstant, IObservationGraphNode>> getObservationDependencies() {
        // TODO Auto-generated method stub
        return null;
    }
}
