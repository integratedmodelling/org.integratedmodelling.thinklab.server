package org.integratedmodelling.thinklab.modelling.scheduling;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Map;

import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.thinklab.api.knowledge.IProperty;
import org.integratedmodelling.thinklab.api.modelling.IObjectState;
import org.integratedmodelling.thinklab.api.modelling.agents.IAgentState;
import org.integratedmodelling.thinklab.api.modelling.agents.IObservationGraphNode;
import org.integratedmodelling.thinklab.api.modelling.agents.IObservationTask;
import org.integratedmodelling.thinklab.api.modelling.scheduling.ITransition;
import org.integratedmodelling.thinklab.api.time.ITimeInstant;
import org.integratedmodelling.thinklab.api.time.ITimePeriod;

/**
 * This class is used to express simple state changes over time, without any concept of an "agent in motion".
 * Assumes that the agent (ISubject) is either spatially distributed or aspatial in a way that doesn't change
 * with time.
 *
 * @author luke
 *
 */
public class TemporalTransition implements ITransition {

    private final IAgentState                                           agentState;

    private final boolean                                               agentSurvives;
    private final Collection<Pair<ITimeInstant, IObservationGraphNode>> observationDependencies = new LinkedList<Pair<ITimeInstant, IObservationGraphNode>>();
    private final Collection<IObservationTask>                          furtherObservationTasks = new LinkedList<IObservationTask>();

    public TemporalTransition(IAgentState agentState, boolean agentSurvives) {
        this.agentState = agentState;
        this.agentSurvives = agentSurvives;
    }

    @Override
    public ITimePeriod getTransitionTimePeriod() {
        return agentState.getTimePeriod();
    }

    @Override
    public IAgentState getAgentState() {
        return agentState;
    }

    @Override
    public boolean agentSurvives() {
        return agentSurvives;
    }

    @Override
    public void addFurtherObservationTask(IObservationTask task) {
        furtherObservationTasks.add(task);
    }

    @Override
    public Collection<IObservationTask> getFurtherObservationTasks() {
        return furtherObservationTasks;
    }

    public void addObservationDependency(ITimeInstant time, IObservationGraphNode node) {
        Pair<ITimeInstant, IObservationGraphNode> pair = new Pair<ITimeInstant, IObservationGraphNode>(time,
                node);
        observationDependencies.add(pair);
    }

    @Override
    public Collection<Pair<ITimeInstant, IObservationGraphNode>> getObservationDependencies() {
        return observationDependencies;
    }

    @Override
    public String toString() {

        if (agentState == null || agentState.getTimePeriod() == null) {
            return "|";
        }

        return agentState.getTimePeriod().getStart().getMillis() + "|"
                + agentState.getTimePeriod().getEnd().getMillis();

    }
}
