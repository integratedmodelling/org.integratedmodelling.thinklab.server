//package org.integratedmodelling.thinklab.modelling.datasets;
//
//import java.awt.Image;
//import java.awt.image.BufferedImage;
//import java.io.File;
//import java.io.IOException;
//import java.util.HashSet;
//
//import org.apache.commons.io.FileUtils;
//import org.integratedmodelling.collections.ContextIndex;
//import org.integratedmodelling.collections.Pair;
//import org.integratedmodelling.common.utils.image.ImageUtil;
//import org.integratedmodelling.exceptions.ThinklabException;
//import org.integratedmodelling.exceptions.ThinklabIOException;
//import org.integratedmodelling.thinklab.api.metadata.IMetadata;
//import org.integratedmodelling.thinklab.api.modelling.IDataset;
//import org.integratedmodelling.thinklab.api.modelling.IExtent;
//import org.integratedmodelling.thinklab.api.modelling.IState;
//import org.integratedmodelling.thinklab.api.modelling.ISubject;
//import org.integratedmodelling.thinklab.geospace.extents.SpaceExtent;
//import org.integratedmodelling.thinklab.geospace.literals.ShapeValue;
//import org.integratedmodelling.thinklab.modelling.compiler.ResolvedSubject;
//import org.integratedmodelling.thinklab.visualization.geospace.GeoImageFactory;
//import org.integratedmodelling.thinklab.visualization.old.DisplayAdapter;
//import org.integratedmodelling.thinklab.visualization.old.VisualizationFactory;
//
//import com.thoughtworks.xstream.XStream;
//import com.thoughtworks.xstream.io.xml.StaxDriver;
//
///**
// * A dataset residing in a directory on the filesystem, with an index, pre-visualized objects, a full NetCDF
// * for grids and any other non-grid data in subdirectories. Index is a serialized ContextIndex
// * bean from the API library.
// * 
// * File structure after persist() is called:
// * 
// * <main dir>
// *      index.xml // use to reconstruct the ContextIndex bean
// *
// * 		thumbnails/
// * 			th_n.png*
// *
// *      images/
// *          im_n.xxx*
// *
// *      data.nc  // netcdf file with all grid data
// *      
// *      data/ xxx  // any additional non-grid data file (e.g. shapefiles), indexed in netcdf attributes.
// *      
// * @author Ferd
// *
// */
//public class FileDataset implements IDataset {
//
//	public static final int DEFAULT_THUMBNAIL_WIDTH = 360;
//	public static final int DEFAULT_THUMBNAIL_HEIGHT = 180;
//	public static final int DEFAULT_IMAGE_WIDTH = 600;
//	public static final int DEFAULT_IMAGE_HEIGHT = 600;
//
//	ISubject _context;
//	ContextIndex _index = new ContextIndex();
//	
//	HashSet<String> _dIds = new HashSet<String>();
//	
//	int _tboxX = 40, _tboxY = 40;
//	int _iboxX = 580, _iboxY = 580;
//	
//	public FileDataset() {
//	}
//	
//	public FileDataset(ISubject context) {
//		setContext(context);
//	}
//
//	public void setThumbnailBoxSize(int x, int y) {
//		_tboxX = x;
//		_tboxY = y;
//	}
//	
//	public void setImageBoxSize(int x, int y) {
//		_iboxX = x;
//		_iboxY = y;
//	}
//	
//	public static interface StateDescriptor {
//		String getName();
//		String getObservableDefinition();
//	}
//	
//	@Override
//	public void setContext(ISubject context)  {
//		_context = context;
//	}
//
//	@Override
//	public ISubject getContext() {
//		return _context;
//	}
//
//	public ContextIndex getIndex() {
//		return _index;
//	}
//	
//	@Override
//	public String persist(String location) throws ThinklabException {
//		
//		if (_context == null)
//			return null;
//		
//		_index.setMultiplicity(_context.getScale().getMultiplicity());
//		
//		File locDir = new File(location);
//		locDir.mkdirs();
//		
//		new File(locDir + File.separator + "images").mkdirs();
//		new File(locDir + File.separator + "thumbnails").mkdirs();
//		new File(locDir + File.separator + "icons").mkdirs();
//		
//		/*
//		 * save any provenance and run info in the index.
//		 */
//		_index.setModelGraph(_context.getMetadata().getString(IMetadata.MODEL_ACCESSOR_GRAPH));
//		_index.setWorkflowDOTGraph(_context.getMetadata().getString(IMetadata.MODEL_WORKFLOW_GRAPH));
//		
//		/*
//		 * create the NetCDF for anything that can be stored in it.
//		 * TODO this should not whine about non-grid data, but return
//		 * a list of states that could not be serialized in it.
//		 * 
//		 * TODO switch to CFdataset when it's ready.
//		 */
//		if (_context.getStates().size() > 0) {
//			new NetCDFDataset(_context).write(locDir + File.separator + "data.nc");
//			_index.setDataFile("data.nc");
//		}
//		
//		/*
//		 * serialize extents
//		 */
//		SpaceExtent space = (SpaceExtent) _context.getScale().getSpace();
//		if (space.isGrid()) {
//			
//			ShapeValue shape = space.getShape();
//			Pair<Integer, Integer> pst = GeoImageFactory.getPlotSize(_tboxX, _tboxY, 
//					space.getGrid().getXCells(), space.getGrid().getYCells());
//			Pair<Integer, Integer> psi = GeoImageFactory.getPlotSize(_iboxX, _iboxY, 
//					space.getGrid().getXCells(), space.getGrid().getYCells());
//			Pair<Integer, Integer> psk = GeoImageFactory.getPlotSize(16, 16, 
//					space.getGrid().getXCells(), space.getGrid().getYCells());
//
//			/*
//			 * TODO use a raster image function that paints the actual grid
//			 */
//			BufferedImage thumbnail = 
//					GeoImageFactory.get().getImagery(shape.getEnvelope(), shape, pst.getFirst(), pst.getSecond(), 0);
//			
//			ImageUtil.saveImage(
//					thumbnail,
//					locDir + File.separator + "thumbnails" + File.separator + "space.png");
//		
//			/*
//			 * make an icon out of that
//			 */
//			Image image = new BufferedImage(psk.getFirst(), psk.getSecond(), BufferedImage.TYPE_INT_RGB);
//			image.getGraphics().drawImage(thumbnail, 0, 0, 16, 16, null);
//			ImageUtil.saveImage(
//					GeoImageFactory.get().getImagery(shape.getEnvelope(), shape, psi.getFirst(), psi.getSecond(), 0),
//					locDir + File.separator + "icons" + File.separator + "space.png");
//
//			ImageUtil.saveImage(
//					GeoImageFactory.get().getImagery(shape.getEnvelope(), shape, psi.getFirst(), psi.getSecond(), 0),
//					locDir + File.separator + "images" + File.separator + "space.png");
//			
//			_index.addExtent(
//					ContextIndex.SPACE_ID, space.getMultiplicity(), 
//					"thumbnails" + File.separator + "space.png", 
//					"images" + File.separator + "space.png",
//					"Spatial coverage");
//			
//		} /* TODO other space extents */
//		
//		IExtent time = _context.getScale().getTime();
//		if (time != null && time.getMultiplicity() > 1) {
//		
//			/*
//			 * TODO create image of time grid if multiple - should be easy.
//			 */
//		}
//		
//		for (final IState s : ((ResolvedSubject)_context).getStates()) {
//			
//			/*
//			 * produce images, set name in index
//			 */
//			DisplayAdapter da = VisualizationFactory.get().getDisplayAdapter(s, _context);
//			
//			/*
//			 * TODO make an icon too - not easy with the current call logics.
//			 */
//			String thf = da.getMediaFile(new File(locDir + File.separator + "thumbnails"), _tboxX, _tboxY);
//			String imf = da.getMediaFile(new File(locDir + File.separator + "images"), _iboxX, _iboxY);
//			
//			if (thf != null && imf != null)
//				_index.addState(
//						getDisplayID(s), 
//						"thumbnails" + File.separator + thf, 
//						"images" + File.separator + imf, 
//						getDisplayLabel(s), 
//						s);
//		}
//		
//		/*
//		 * Persist all subjects as independent datasets in subdirs.
//		 */
//		int i = 0;
//		for (ISubject subject : _context.getSubjects()) {
//			
//			String id = subject.getId();
//			if (id == null) {
//				id = "subject_" + i;
//			}
//			
//			/*
//			 * TBC this will create geovisualizations in each subdir - not a problem
//			 * if images are cached (i.e. the extents are the same) but a potential
//			 * performance hit otherwise.
//			 */
//			FileDataset fd = new FileDataset(subject);
//			fd.persist(location + File.separator + id);
//			
//			/*
//			 * TODO add property
//			 */
//			_index.addSubject(id, "");
//			
//			i++;
//		}
//		
//		/*
//		 * serialize index to xml
//		 */
//		XStream xstream = new XStream(new StaxDriver());
//		String xml = xstream.toXML(_index);
//		try {
//			FileUtils.writeStringToFile(new File(locDir + File.separator + "index.xml"), xml);
//		} catch (IOException e1) {
//			throw new ThinklabIOException(e1);
//		}
//		
//				
//		return location;
//	}
//
//	@Override
//	public void restore(String location) throws ThinklabException {
//		// TODO Auto-generated method stub
//
//	}
//
//	public String getDisplayID(IState observable) {
//		
//		String cnc = observable.getDirectType().toString();
//		String ret = cnc;
//		int n = 0;
//		
//		while (_dIds.contains(ret)) {
//			ret = cnc + (n++);
//		}
//		
//		_dIds.add(ret);
//		
//		return ret; 
//	}
//
//	public String getDisplayLabel(IState observable) {
//		
//		String label =
//				observable.getMetadata().getString(IMetadata.DC_LABEL);
//		return 
//			label == null ? 
//				observable.getDirectType().getLocalName().toString() :
//				label;
//	}
//	
//}
