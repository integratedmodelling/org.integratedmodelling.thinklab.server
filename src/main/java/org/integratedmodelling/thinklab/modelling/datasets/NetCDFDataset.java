package org.integratedmodelling.thinklab.modelling.datasets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.collections.Triple;
import org.integratedmodelling.exceptions.ThinklabCircularDependencyException;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabIOException;
import org.integratedmodelling.exceptions.ThinklabUnsupportedOperationException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IProperty;
import org.integratedmodelling.thinklab.api.knowledge.ISemanticObject;
import org.integratedmodelling.thinklab.api.lang.IList;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.metadata.IMetadata;
import org.integratedmodelling.thinklab.api.modelling.ICoverage;
import org.integratedmodelling.thinklab.api.modelling.IDataset;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.api.modelling.IProcess;
import org.integratedmodelling.thinklab.api.modelling.IScale;
import org.integratedmodelling.thinklab.api.modelling.IState;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.api.modelling.resolution.IResolutionStrategy;
import org.integratedmodelling.thinklab.geospace.extents.Grid;
import org.integratedmodelling.thinklab.geospace.extents.SpaceExtent;
import org.integratedmodelling.thinklab.geospace.interfaces.IGridMask;
import org.integratedmodelling.thinklab.time.extents.RegularTemporalGrid;
import org.integratedmodelling.thinklab.visualization.VisualizationFactory;

import ucar.ma2.ArrayDouble;
import ucar.ma2.DataType;
import ucar.ma2.Index;
import ucar.nc2.Dimension;
import ucar.nc2.NetcdfFileWriteable;

public class NetCDFDataset implements IDataset {

    public NetCDFDataset() {
    }

    /**
     * For direct API use
     * @param context
     * @throws ThinklabException
     */
    public NetCDFDataset(ISubject context) throws ThinklabException {
        setContext(context);
    }

    // @Override
    public void setContext(ISubject context) throws ThinklabException {
        this.context = context;
    }

    // @Override
    public ISubject getContext() {
        return context;
    }

    @Override
    public String persist(String location) throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void restore(String location) throws ThinklabException {
        // TODO Auto-generated method stub
    }

    Grid                            space        = null;
    RegularTemporalGrid             time         = null;

    Map<String, IState>             auxVariables = new Hashtable<String, IState>();
    ISubject                        context      = null;
    HashSet<String>                 varnames     = new HashSet<String>();

    /*
     * container for variables to write
     */
    ArrayList<Pair<String, String>> attributes;

    public void write(String filename) throws ThinklabException {

        SpaceExtent rgrid = (SpaceExtent) context.getScale().getSpace();
        if (rgrid != null && rgrid.isGrid()) {
            this.space = rgrid.getGrid();
        } else {
            throw new ThinklabUnsupportedOperationException(
                    "only raster grid data are supported in NetCDF exporter for now");
        }

        Dimension latDim = null;
        Dimension lonDim = null;
        Dimension timDim = null;

        HashMap<IState, double[]> dataCatalog = new HashMap<IState, double[]>();

        ArrayList<Dimension> spdims = new ArrayList<Dimension>();

        if (!filename.endsWith(".nc"))
            filename += ".nc";

        NetcdfFileWriteable ncfile;
        try {
            ncfile = NetcdfFileWriteable.createNew(filename, false);
        } catch (IOException e1) {
            throw new ThinklabIOException(e1);
        }

        /*
         * add dimensions
         */
        if (time != null) {
            // unimplemented for now
        }

        if (space != null) {

            latDim = ncfile.addDimension("lat", space.getYCells());
            lonDim = ncfile.addDimension("lon", space.getXCells());
            spdims.add(latDim);
            spdims.add(lonDim);

            /* add latitude and longitude as variables */
            ncfile.addVariable("lat", DataType.DOUBLE, new Dimension[] { latDim });
            ncfile.addVariableAttribute("lat", "units", "degrees_north");
            ncfile.addVariableAttribute("lat", "long_name", "latitude");
            /* add latitude and longitude as variables */
            ncfile.addVariable("lon", DataType.DOUBLE, new Dimension[] { lonDim });
            ncfile.addVariableAttribute("lon", "units", "degrees_east");
            ncfile.addVariableAttribute("lon", "long_name", "longitude");
        }

        varnames.clear();

        for (IState state : context.getStates()) {

            // TODO implement the rest

            if (spdims.size() == 2) {
                // we have space only
                String varname = getVarname(state);
                if (varnames.contains(varname))
                    continue;

                ncfile.addVariable(varname, DataType.FLOAT, new Dimension[] { latDim, lonDim });

                double[] dio = VisualizationFactory.get().getStateDataAsNumbers(state);
                dataCatalog.put(state, dio);
                varnames.add(varname);

                // TODO if var is a measurement, add units attribute - this is a stupid stub
                if (varname.equals("Altitude")) {
                    ncfile.addVariableAttribute("Altitude", "units", "meters");
                }
            }
        }

        /*
         * create the file before we add variables
         */
        try {
            ncfile.create();
        } catch (IOException e) {
            throw new ThinklabIOException(e);
        }

        /*
         * TODO write time data
         */

        /*
         * lat and lon data if any
         */
        IGridMask mask = null;

        if (space != null) {

            mask = space.getActivationLayer();

            ArrayDouble alat = new ArrayDouble.D1(latDim.getLength());
            Index ind1 = alat.getIndex();
            double xcn = space.getSouth() + space.getNSResolution() * 0.5;
            for (int i = 0; i < latDim.getLength(); i++) {
                alat.setDouble(ind1.set(i), xcn);
                xcn += space.getNSResolution();
            }

            ArrayDouble alon = new ArrayDouble.D1(lonDim.getLength());
            Index ind2 = alon.getIndex();
            xcn = space.getWest() + space.getEWResolution() * 0.5;
            for (int i = 0; i < lonDim.getLength(); i++) {
                alon.setDouble(ind2.set(i), xcn);
                // use NSres instead of EWres as they may differ slightly. This will
                // skew results if the cell is not square. This way import to ARC will
                // work.
                xcn += space.getNSResolution();
            }

            try {
                ncfile.write("lat", alat);
                ncfile.write("lon", alon);
            } catch (Exception e) {
                throw new ThinklabIOException(e);
            }
        }

        for (IState state : context.getStates()) {

            if (state.getValueCount() != space.getXCells() * space.getYCells()) {
                Thinklab.get()
                        .logger()
                        .error("state of " + state + " has " + state.getValueCount()
                                + " multiplicity when context expects "
                                + (space.getXCells() * space.getYCells()) + ": results not stored");
                continue;
            }

            varnames.clear();

            // TODO implement the rest
            if (spdims.size() == 2) {

                // we have space only
                String varname = getVarname(state);

                if (varnames.contains(varname))
                    continue;

                varnames.add(varname);

                ArrayDouble data = new ArrayDouble.D2(latDim.getLength(), lonDim.getLength());
                Index ind = data.getIndex();
                double[] dd = dataCatalog.get(state);

                // this can now happen for stuff like categories, eventually it will be removed
                if (dd == null)
                    continue;

                int i = 0;
                for (int lat = 0; lat < latDim.getLength(); lat++) {
                    for (int lon = 0; lon < lonDim.getLength(); lon++) {

                        Index index = ind.set(lat, lon);

                        double val = dd[i];

                        if (mask != null) {
                            int[] xy = space.getXYCoordinates(i);
                            if (!mask.isActive(xy[0], xy[1])) {
                                val = Double.NaN;
                            }
                        }

                        data.setFloat(index, (float) val);
                        i++;
                    }
                }
                try {
                    ncfile.write(varname, data);
                } catch (Exception e) {
                    throw new ThinklabIOException(e);
                }
            }
        }

        for (String varname : auxVariables.keySet()) {

            if (spdims.size() == 2) {

                ArrayDouble data = new ArrayDouble.D2(latDim.getLength(), lonDim.getLength());
                Index ind = data.getIndex();

                double[] dd = VisualizationFactory.get().getStateDataAsNumbers(auxVariables.get(varname));
                int i = 0;
                for (int lat = 0; lat < latDim.getLength(); lat++) {
                    for (int lon = 0; lon < lonDim.getLength(); lon++) {

                        double val = dd[i];
                        if (mask != null) {
                            int[] xy = space.getXYCoordinates(i);
                            if (!mask.isActive(xy[0], xy[1]))
                                val = Double.NaN;
                        }

                        data.setFloat(ind.set(lat, lon), (float) val);
                        i++;
                    }
                }

                try {
                    ncfile.write(varname, data);
                } catch (Exception e) {
                    throw new ThinklabIOException(e);
                }
            }
        }

        try {
            ncfile.close();
        } catch (IOException e) {
            throw new ThinklabIOException(e);
        }
    }

    /*
     * Extract the proper name for a state. TODO this needs a lot of work. The metadata from the model and
     * concept should be considered.
     * 
     * just recognize some concepts that have special meaning for the netcdf CF convention
     */
    private String getVarname(IState state) {

        IConcept obs = state.getDirectType();

        String ret = obs.getLocalName() + "_" + obs.getConceptSpace();
        ret = ret.replace('.', '_');
        /**
         * FIXME this must check for the common SWEET concept, not the IM one.
         */
        if (obs.is(Thinklab.c("im.geo:ElevationSeaLevel"))) {
            ret = "Altitude";
        }
        return ret;
    }

    @Override
    public Collection<IState> getStates() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Collection<ISubject> getSubjects() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getId() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IMetadata getMetadata() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IObservable getObservable() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IScale getScale() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public INamespace getNamespace() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IList getSemantics() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Object demote() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IConcept getDirectType() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean is(Object other) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public int getRelationshipsCount() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int getRelationshipsCount(IProperty property) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public ISemanticObject<?> get(IProperty property) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<Pair<IProperty, ISemanticObject<?>>> getRelationships() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<Triple<IProperty, ISemanticObject<?>, Integer>> getCountedRelationships() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<ISemanticObject<?>> getRelationships(IProperty property) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isLiteral() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isConcept() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isObject() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isCyclic() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isValid() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public List<ISemanticObject<?>> getSortedRelationships(IProperty property)
            throws ThinklabCircularDependencyException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void dispose() {
        // TODO Auto-generated method stub

    }

    @Override
    public ICoverage observe(IObservable observable, Collection<String> scenarios, boolean isOptional)
            throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ICoverage initialize(Collection<String> scenarios) throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void contextualize() throws ThinklabException {
        // TODO Auto-generated method stub

    }

    @Override
    public void setMonitor(IMonitor monitor) {
        // TODO Auto-generated method stub

    }

    @Override
    public IMonitor getMonitor() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IResolutionStrategy getResolutionStrategy() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Collection<IProcess> getProcesses() {
        // TODO Auto-generated method stub
        return null;
    }

}
