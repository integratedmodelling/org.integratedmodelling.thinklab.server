package org.integratedmodelling.thinklab.modelling.datasets;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.collections.BidiMap;
import org.apache.commons.collections.bidimap.DualHashBidiMap;
import org.apache.commons.io.FileUtils;
import org.integratedmodelling.collections.MultidimensionalCursor;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.collections.Triple;
import org.integratedmodelling.exceptions.ThinklabCircularDependencyException;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabIOException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IProperty;
import org.integratedmodelling.thinklab.api.knowledge.ISemanticObject;
import org.integratedmodelling.thinklab.api.lang.IList;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.metadata.IMetadata;
import org.integratedmodelling.thinklab.api.modelling.IClassifyingObserver;
import org.integratedmodelling.thinklab.api.modelling.ICoverage;
import org.integratedmodelling.thinklab.api.modelling.IExtent;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.api.modelling.INumericObserver;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.IPresenceObserver;
import org.integratedmodelling.thinklab.api.modelling.IProcess;
import org.integratedmodelling.thinklab.api.modelling.IScale;
import org.integratedmodelling.thinklab.api.modelling.IState;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.api.modelling.resolution.IResolutionStrategy;
import org.integratedmodelling.thinklab.api.runtime.ISession;
import org.integratedmodelling.thinklab.api.space.ISpatialExtent;
import org.integratedmodelling.thinklab.api.time.ITemporalExtent;
import org.integratedmodelling.thinklab.geospace.GeoNS;
import org.integratedmodelling.thinklab.geospace.Geospace;
import org.integratedmodelling.thinklab.geospace.extents.Grid;
import org.integratedmodelling.thinklab.geospace.extents.SpaceExtent;
import org.integratedmodelling.thinklab.modelling.interfaces.IExtendedDataset;
import org.integratedmodelling.thinklab.modelling.lang.Scale;
import org.integratedmodelling.thinklab.modelling.monitoring.Monitor;
import org.integratedmodelling.thinklab.time.Time;
import org.integratedmodelling.thinklab.time.extents.RegularTemporalGrid;

import ucar.ma2.Array;
import ucar.ma2.ArrayDouble;
import ucar.ma2.DataType;
import ucar.ma2.Index;
import ucar.nc2.Attribute;
import ucar.nc2.Dimension;
import ucar.nc2.Group;
import ucar.nc2.NetcdfFileWriter;
import ucar.nc2.NetcdfFileWriter.Version;
import ucar.nc2.Variable;

/**
 * Wrapper over a NetCDF to use for storage as well as for caching of temporal simulations in a
 * DatasetBackedState.
 */
public class CFdataset implements IExtendedDataset {

    private static final String CONVENTIONS_ATTRIBUTE = "Conventions";
    private static final String CREATION_TIME         = "CreationTime";

    private static final String CONVENTION            = "CF-1.6";

    private ISubject            _subject;
    private IMonitor            _monitor;
    private File                _file;
    private NetcdfFileWriter    _ncfile;
    private boolean             _open                 = false;
    private long                _timeStateCount       = 0;
    private Group               _group                = null;
    private long                _sliceMultiplicity;
    boolean                     _readOnly             = true;
    MultidimensionalCursor      _cursor               = null;
    MultidimensionalCursor      _timelessCursor       = null;
    private int                 _tIndex               = -1;
    private int                 _sIndex               = -1;

    /*
     * dimensions result from the initial serialization of scale
     */
    List<Dimension>             _dimensions           = null;

    HashMap<IObservable, Var>   _vars                 = new HashMap<IObservable, Var>();

    /**
     * Live storage is much more economical than before.
     * @author Ferd
     * 
     */
    class CFVariable implements Var {

        final private int DOUBLE             = 1;
        final private int INT                = 2;
        final private int LONG               = 3;
        final private int INT_IDX            = 4;
        final private int BOOLEAN            = 5;

        int               _objIdx            = Integer.MIN_VALUE + 1;
        int               _dType             = DOUBLE;
        double[]          _dData             = null;
        long[]            _lData             = null;
        int[]             _iData             = null;
        BidiMap           _dMap              = null;
        BitSet            _bData             = null;
        BitSet            _bNullData         = null;
        boolean           dirty              = true;
        private long      _currentTIndex     = -1;                   // initialization

        IObservable       _observable;
        IObserver         _observer;
        private Variable  _ncvar;
        private Array     _array;
        private boolean   _flushed;

        private int       _lastOffset        = -1;
        private int       _lastFlushedOffset = -1;

        // used on "live" subjects
        public CFVariable(IObservable o, IObserver observer) {

            _observable = o;
            _observer = observer;

            if (observer instanceof IPresenceObserver) {
                _dType = BOOLEAN;
                _bData = new BitSet((int) _sliceMultiplicity);
                _bNullData = new BitSet((int) _sliceMultiplicity);
            } else if (observer instanceof IClassifyingObserver) {
                _dType = INT_IDX;
            } else if (observer instanceof INumericObserver
                    && ((INumericObserver) observer).getDiscretization() != null) {

                _dType = INT_IDX;
                /*
                 * TODO store discretization and prepare for remapping; if it's probabilities, must store the
                 * distributions and prepare to switch to full storage of objects if the capacity is exceeded.
                 */
            }

            if (_dType == INT_IDX) {
                _dMap = new DualHashBidiMap();
                _iData = new int[(int) _sliceMultiplicity];
            } else if (_dType == DOUBLE) {
                _dData = new double[(int) _sliceMultiplicity];
            } else if (_dType == INT) {
                _iData = new int[(int) _sliceMultiplicity];
            } else if (_dType == LONG) {
                _lData = new long[(int) _sliceMultiplicity];
            }
        }

        // // used on read-only files; reconstruct observer semantics
        // public CFVariable(IObservable o) {
        // // TODO
        // }

        @Override
        protected void finalize() throws Throwable {
            close();
            super.finalize();
        }

        void load(long timeSlice) {

            if (timeSlice != _currentTIndex) {

                if (dirty) {
                    /*
                     * TODO flush to disk
                     */
                }

                if (timeSlice != (_currentTIndex + 1)) {
                    // TODO check - we probably want to use different temporary storage for
                    // this, so we keep the previous state
                    zero();
                }

            }

        }

        private void zero() {
            // TODO set all live storage to 0. Called only at non-natural time
            // transitions.
        }

        @Override
        public void setValue(int index, Object value) {

            if (_lastOffset < index)
                _lastOffset = index;

            if (_readOnly) {
                throw new ThinklabRuntimeException("cannot write on a read-only dataset");
            }

            if (value instanceof Double && Double.isNaN((Double) value)) {
                value = null;
            }

            int tslice = _tIndex < 0 ? -1 : _cursor.getElementIndexes(index)[_tIndex];
            if (_currentTIndex != tslice) {
                load(tslice);
            }
            int offset = _timelessCursor == null ? index : _timelessCursor.getElementOffset(tail(_cursor
                    .getElementIndexes(index)));
            switch (_dType) {

            case DOUBLE:
                _dData[offset] = value == null ? Double.NaN : ((Number) value).doubleValue();
                break;
            case INT:
                _iData[offset] = value == null ? Integer.MIN_VALUE : ((Number) value).intValue();
                break;
            case LONG:
                _lData[offset] = value == null ? Long.MIN_VALUE : ((Number) value).longValue();
                break;
            case INT_IDX:
                Integer idx = Integer.MIN_VALUE;
                if (value != null) {
                    idx = (Integer) _dMap.getKey(value);
                    if (idx == null) {
                        idx = ++_objIdx;
                        _dMap.put(idx, value);
                    }
                }
                _iData[offset] = idx == null ? Integer.MIN_VALUE : idx;
                break;
            case BOOLEAN:
                _bNullData.set(offset, value == null);
                _bData.set(offset, value instanceof Boolean ? ((Boolean) value)
                        : (value != null && !(value instanceof Number && ((Number) value).doubleValue() == 0)));
                break;
            }
        }

        @Override
        public Object getValue(int index) {

            Object ret = null;
            int tslice = _tIndex < 0 ? -1 : _cursor.getElementIndexes(index)[_tIndex];
            if (_currentTIndex != tslice) {
                load(tslice);
            }

            int offset = _timelessCursor == null ? index : _timelessCursor.getElementOffset(tail(_cursor
                    .getElementIndexes(index)));

            switch (_dType) {
            case DOUBLE:
                ret = _dData[offset];
                if (Double.isNaN((Double) ret))
                    ret = null;
                break;
            case INT:
                ret = _iData[offset];
                if ((Integer) ret == Integer.MIN_VALUE)
                    ret = null;
                break;
            case LONG:
                ret = _lData[offset];
                if ((Long) ret == Long.MIN_VALUE)
                    ret = null;
                break;
            case INT_IDX:
                ret = _dMap.get(_iData[offset]);
                break;
            case BOOLEAN:
                ret = _bNullData.get(offset) ? null : _bData.get(offset);
                break;
            }

            return ret;
        }

        public double[] asDouble() {

            if (_dData != null)
                return _dData;
            /*
             * TODO
             */
            return null;
        }

        @Override
        public void setFromDouble(double[] data) {
            // TODO fill in
            switch (_dType) {
            case DOUBLE:
                _dData = Arrays.copyOf(data, data.length);
                break;
            }
        }

        public void flush() throws ThinklabException {

            defineMode(true);

            if (_flushed) {
                return;
            }

            // int[] idx = _cursor.getExtents();
            ArrayList<Integer> indexes = new ArrayList<Integer>();
            for (IExtent ext : _subject.getScale()) {
                for (int ds : ext.getDimensionSizes(true)) {
                    indexes.add(ds);
                }
            }

            String varname = getVarname(_observable);
            if (_ncvar == null) {

                // dio cane
                int[] idxx = new int[indexes.size()];
                for (int i = 0; i < indexes.size(); i++) {
                    idxx[i] = indexes.get(i);
                }

                _ncvar = _ncfile.addVariable(null, varname, getDataType(), _dimensions);
                _array = Array.factory(getDataType(), idxx);

                /*
                 * TODO add metadata and categories if necessary
                 */
            }

            int cnt = 0;
            Index index = _array.getIndex();

            for (int n = _lastFlushedOffset + 1; n < _lastOffset; n++) {

                // get index for this offset
                int[] extentOffsets = _cursor.getElementIndexes(n);
                int[] dimensionOffsets = new int[_dimensions.size()];

                int eIdx = 0, dIdx = 0;
                boolean nodata = false;
                for (IExtent ext : _subject.getScale()) {
                    for (int o : ext.getDimensionOffsets(extentOffsets[eIdx], true)) {
                        dimensionOffsets[dIdx++] = o;
                    }
                    if (nodata) {
                        nodata = !ext.isCovered(extentOffsets[eIdx]);
                    }
                    eIdx++;
                }

                // TODO only write a slice
                int sliceOfs = _timelessCursor == null ? n : _timelessCursor
                        .getElementOffset(tail(extentOffsets));

                if (_dData != null) {
                    _array.setDouble(index.set(dimensionOffsets), nodata ? Double.NaN : _dData[sliceOfs]);
                } else if (_iData != null) {
                    _array.setInt(index.set(dimensionOffsets), nodata ? 0 : _iData[sliceOfs]);
                } else if (_lData != null) {
                    _array.setLong(index.set(dimensionOffsets), nodata ? 0l : _iData[sliceOfs]);
                }

                cnt++;
            }

            defineMode(false);

            try {
                _ncfile.write(_ncvar, /* TODO add origin for slice, */_array);
            } catch (Exception e) {
                throw new ThinklabIOException(e);
            }

            _lastFlushedOffset += cnt;
            _flushed = true;
        }

        private DataType getDataType() {

            DataType ret = DataType.DOUBLE;

            switch (_dType) {
            case INT:
            case INT_IDX:
                ret = DataType.INT;
                break;
            case LONG:
                ret = DataType.LONG;
                break;
            case BOOLEAN:
                ret = DataType.BOOLEAN;
                break;
            }

            return ret;
        }

        @Override
        public long getValueCount() {
            return _sliceMultiplicity;
        }
    }

    public CFdataset(File file) throws ThinklabException {
        _file = file;
        open();
    }

    public int[] tail(int[] array) {
        int[] ret = new int[array.length - 1];
        for (int i = 1; i < array.length; i++) {
            ret[i - 1] = array[i];
        }
        return ret;
    }

    private void open() throws ThinklabIOException {

        try {
            if (_file.exists()) {
                _ncfile = NetcdfFileWriter.openExisting(_file.toString());
            } else {
                _ncfile = NetcdfFileWriter.createNew(Version.netcdf3, _file.toString());
                _ncfile.create();
            }
        } catch (IOException e) {
            throw new ThinklabIOException(e);
        }

        _open = true;
    }

    public CFdataset(ISubject subject, IMonitor monitor, long contextId) throws ThinklabException {

        /*
         * create the NetCDF. Take session area from monitor in subject, or create in temp workspace if none.
         * 
         * TODO explore groups for the subject hierarchy. Unfortunately the Java API is different for writing
         * and reading, which is a bummer if we need to do both at the same time.
         */
        ISession session = null;
        _monitor = monitor;
        _subject = subject;

        if (_monitor != null && _monitor instanceof Monitor) {
            session = ((Monitor) _monitor).getSession();
        }

        if (session != null) {
            _file = Thinklab.get().getTempArea(session.getWorkspace());
            _file = new File(_file + File.separator + contextId);
            _file.mkdirs();
        } else {
            _file = Thinklab.get().getTempArea("output");
        }

        File dir = _file;
        _file = new File(_file + File.separator + makeFileNameFromSubject(subject));
        _file.deleteOnExit();
        dir.deleteOnExit();

        // if the file exists, being this a temporary one, we have reset the context and recreated the
        // dataset, so we must delete it.
        if (_file.exists()) {
            FileUtils.deleteQuietly(_file);
        }

        open();
        writeGlobalAttributes(subject);

        _readOnly = false;
    }

    private String makeFileNameFromSubject(ISubject subject) {
        return subject.getId() + ".nc";
    }

    public void close() throws ThinklabException {

        if (_open) {

            flush();
            _open = false;
            try {
                _ncfile.close();
            } catch (IOException e) {
                throw new ThinklabIOException(e);
            }
        }
    }

    /**
     * Write the current states to their time slice and move the time pointer to the next unless we're already
     * there.
     * 
     * @throws ThinklabException
     */
    private void flush() throws ThinklabException {

        for (Var v : _vars.values()) {
            ((CFVariable) v).flush();
        }
    }

    private void defineMode(boolean b) throws ThinklabException {
        try {
            _ncfile.setRedefineMode(b);
        } catch (IOException e) {
            throw new ThinklabIOException(e);
        }
    }

    private void writeGlobalAttributes(ISubject subject) throws ThinklabException {

        /*
         * TODO write index with all dimensions.
         */

        defineMode(true);
        _ncfile.addGroupAttribute(null, new Attribute(CONVENTIONS_ATTRIBUTE, CONVENTION));
        // FLOAT due to LONG raising an exception at runtime.
        _ncfile.addGroupAttribute(null, new Attribute(CREATION_TIME, new Float(new Date().getTime())));
        defineMode(false);

        IScale scale = subject.getScale();

        _cursor = ((Scale) scale).getCursor();
        /*
         * if we have the timeless cursor, we have >1 dimensionality in other extents; otherwise each slice
         * has dimensionality 1.
         */
        if (scale.getTime() != null && scale.getExtentCount() > 1
                && scale.getMultiplicity() > scale.getTime().getMultiplicity()) {

        }

        /*
         * TODO substitute with proper scale serialization
         */
        serializeScale(scale);

        /*
         * store all metadata as POD. TODO make it smarter at some point.
         */
        for (String key : subject.getMetadata().getKeys()) {

            Object o = subject.getMetadata().get(key);

            if (o instanceof Number) {
                _ncfile.addGroupAttribute(_group, new Attribute(key, (Number) o));
            } else if (o instanceof String) {
                _ncfile.addGroupAttribute(_group, new Attribute(key, (String) o));
            } else {
                _ncfile.addGroupAttribute(_group, new Attribute(key, o.toString()));
            }
        }

        try {
            _ncfile.setRedefineMode(false);
        } catch (IOException e) {
            throw new ThinklabIOException(e);
        }

    }

    private void serializeScale(IScale scale) throws ThinklabException {

        /*
         * write dimensions details
         */
        ArrayList<Dimension> dimensions = new ArrayList<Dimension>();

        _sliceMultiplicity = 1L;
        int n = 0;
        for (IExtent e : scale) {
            if (e.getDomainConcept().equals(Time.TIME_DOMAIN)) {
                serializeTime(scale.getTime(), dimensions);
                _timeStateCount = e.getMultiplicity();
                _tIndex = n;
            } else if (e.getDomainConcept().equals(Geospace.get().SpatialDomain())) {
                serializeSpace(scale.getSpace(), dimensions);
                _sliceMultiplicity *= e.getMultiplicity();
                _sIndex = n;
            } else {
                serializeExtent(e, dimensions);
                _sliceMultiplicity *= e.getMultiplicity();
            }
            n++;
        }

        if (_sliceMultiplicity > Integer.MAX_VALUE) {
            throw new ThinklabRuntimeException("cannot store states of dimensionality larger than "
                    + Integer.MAX_VALUE);
        }

        _dimensions = dimensions;
    }

    private void serializeTime(ITemporalExtent time, List<Dimension> dims) {
        // TODO Auto-generated method stub

    }

    private void serializeExtent(IExtent e, List<Dimension> dims) {
        // TODO Auto-generated method stub

    }

    private void serializeSpace(ISpatialExtent spaceExt, List<Dimension> dims) throws ThinklabException {

        SpaceExtent space = (SpaceExtent) spaceExt;

        /*
         * TODO write bounding box, shape, total multiplicity, other statistics
         */

        if (space.getGrid() != null) {

            defineMode(true);

            Grid grid = space.getGrid();

            Dimension latDim = _ncfile.addDimension(_group, "lat", grid.getYCells());
            Dimension lonDim = _ncfile.addDimension(_group, "lon", grid.getXCells());

            /* add latitude and longitude as variables */
            Variable latitude = _ncfile.addVariable(_group, "lat", DataType.DOUBLE, Collections
                    .singletonList(latDim));
            _ncfile.addVariableAttribute(latitude, new Attribute("units", "degrees_north"));
            _ncfile.addVariableAttribute(latitude, new Attribute("long_name", "latitude"));

            /* add latitude and longitude as variables */
            Variable longitude = _ncfile.addVariable(_group, "lon", DataType.DOUBLE, Collections
                    .singletonList(lonDim));
            _ncfile.addVariableAttribute(longitude, new Attribute("units", "degrees_east"));
            _ncfile.addVariableAttribute(longitude, new Attribute("long_name", "longitude"));

            double latDelta = (grid.getEast() - grid.getWest()) / (double) grid.getXCells();
            double lonDelta = (grid.getNorth() - grid.getSouth()) / (double) grid.getYCells();

            ArrayDouble alat = new ArrayDouble.D1(latDim.getLength());
            Index ind1 = alat.getIndex();
            double xcn = grid.getSouth();
            for (int i = 0; i < latDim.getLength(); i++) {
                alat.setDouble(ind1.set(i), xcn);
                xcn += latDelta;
            }

            ArrayDouble alon = new ArrayDouble.D1(lonDim.getLength());
            Index ind2 = alon.getIndex();
            xcn = grid.getWest();
            for (int i = 0; i < lonDim.getLength(); i++) {
                alon.setDouble(ind2.set(i), xcn);
                xcn += lonDelta;
            }

            /*
             * TODO write mask
             */
            defineMode(false);

            try {
                _ncfile.write(latitude, alat);
                _ncfile.write(longitude, alon);
            } catch (Exception e) {
                throw new ThinklabIOException(e);
            }

            dims.add(latDim);
            dims.add(lonDim);

        } // TODO else write features if any
    }

    Grid                            space = null;
    RegularTemporalGrid             time  = null;

    /*
     * container for variables to write
     */
    ArrayList<Pair<String, String>> attributes;

    // public void write(String filename) throws ThinklabException {
    //
    // Dimension latDim = null;
    // Dimension lonDim = null;
    // Dimension timDim = null;
    //
    // HashMap<IState, double[]> dataCatalog = new HashMap<IState, double[]>();
    //
    // ArrayList<Dimension> spdims = new ArrayList<Dimension>();
    //
    // if (!filename.endsWith(".nc")) {
    // filename += ".nc";
    // }
    //
    // NetcdfFileWriteable ncfile;
    // try {
    // ncfile = NetcdfFileWriteable.createNew(filename, false);
    // } catch (IOException e1) {
    // throw new ThinklabIOException(e1);
    // }
    //
    // /*
    // * add dimensions
    // */
    // if (time != null) {
    // // unimplemented for now
    // }
    //
    // if (space != null) {
    //
    // latDim = ncfile.addDimension("lat", space.getYCells());
    // lonDim = ncfile.addDimension("lon", space.getXCells());
    // spdims.add(latDim);
    // spdims.add(lonDim);
    //
    // /* add latitude and longitude as variables */
    // ncfile.addVariable("lat", DataType.DOUBLE, new Dimension[] { latDim });
    // ncfile.addVariableAttribute("lat", "units", "degrees_north");
    // ncfile.addVariableAttribute("lat", "long_name", "latitude");
    //
    // /* add latitude and longitude as variables */
    // ncfile.addVariable("lon", DataType.DOUBLE, new Dimension[] { lonDim });
    // ncfile.addVariableAttribute("lon", "units", "degrees_east");
    // ncfile.addVariableAttribute("lon", "long_name", "longitude");
    // }
    //
    // varnames.clear();
    //
    // for (IState state : ((SubjectObserver) context).getSubject().getStates()) {
    //
    // // TODO implement the rest
    //
    // if (spdims.size() == 2) {
    // // we have space only
    // String varname = getVarname(state);
    // if (varnames.contains(varname)) {
    // continue;
    // }
    //
    // ncfile.addVariable(varname, DataType.FLOAT, new Dimension[] { latDim, lonDim });
    //
    // double[] dio = VisualizationFactory.get().getStateDataAsNumbers(state);
    // dataCatalog.put(state, dio);
    // varnames.add(varname);
    //
    // // TODO if var is a measurement, add units attribute - this is a stupid stub
    // if (varname.equals("Altitude")) {
    // ncfile.addVariableAttribute("Altitude", "units", "meters");
    // }
    // }
    // }
    //
    // /*
    // * create the file before we add variables
    // */
    // try {
    // ncfile.create();
    // } catch (IOException e) {
    // throw new ThinklabIOException(e);
    // }
    //
    // /*
    // * TODO write time data
    // */
    //
    // /*
    // * lat and lon data if any
    // */
    // IGridMask mask = null;
    //
    // if (space != null) {
    //
    // mask = space.getActivationLayer();
    //
    // ArrayDouble alat = new ArrayDouble.D1(latDim.getLength());
    // Index ind1 = alat.getIndex();
    // double xcn = space.getSouth() + space.getNSResolution() * 0.5;
    // for (int i = 0; i < latDim.getLength(); i++) {
    // alat.setDouble(ind1.set(i), xcn);
    // xcn += space.getNSResolution();
    // }
    //
    // ArrayDouble alon = new ArrayDouble.D1(lonDim.getLength());
    // Index ind2 = alon.getIndex();
    // xcn = space.getWest() + space.getEWResolution() * 0.5;
    // for (int i = 0; i < lonDim.getLength(); i++) {
    // alon.setDouble(ind2.set(i), xcn);
    // // use NSres instead of EWres as they may differ slightly. This will
    // // skew results if the cell is not square. This way import to ARC will
    // // work.
    // xcn += space.getNSResolution();
    // }
    //
    // try {
    // ncfile.write("lat", alat);
    // ncfile.write("lon", alon);
    // } catch (Exception e) {
    // throw new ThinklabIOException(e);
    // }
    // }
    //
    // for (IState state : ((SubjectObserver) context).getSubject().getStates()) {
    //
    // if (state.getValueCount() != space.getXCells() * space.getYCells()) {
    // Thinklab.get()
    // .logger()
    // .error("state of " + state + " has " + state.getValueCount()
    // + " multiplicity when context expects "
    // + (space.getXCells() * space.getYCells()) + ": results not stored");
    // continue;
    // }
    //
    // varnames.clear();
    //
    // // TODO implement the rest
    // if (spdims.size() == 2) {
    //
    // // we have space only
    // String varname = getVarname(state);
    //
    // if (varnames.contains(varname)) {
    // continue;
    // }
    //
    // varnames.add(varname);
    //
    // ArrayDouble data = new ArrayDouble.D2(latDim.getLength(), lonDim.getLength());
    // Index ind = data.getIndex();
    // double[] dd = dataCatalog.get(state);
    //
    // // this can now happen for stuff like categories, eventually it will be removed
    // if (dd == null) {
    // continue;
    // }
    //
    // int i = 0;
    // for (int lat = 0; lat < latDim.getLength(); lat++) {
    // for (int lon = 0; lon < lonDim.getLength(); lon++) {
    //
    // Index index = ind.set(lat, lon);
    //
    // double val = dd[i];
    //
    // if (mask != null) {
    // int[] xy = space.getXYCoordinates(i);
    // if (!mask.isActive(xy[0], xy[1])) {
    // val = Double.NaN;
    // }
    // }
    //
    // data.setFloat(index, (float) val);
    // i++;
    // }
    // }
    // try {
    // ncfile.write(varname, data);
    // } catch (Exception e) {
    // throw new ThinklabIOException(e);
    // }
    // }
    // }
    //
    // for (String varname : auxVariables.keySet()) {
    //
    // if (spdims.size() == 2) {
    //
    // ArrayDouble data = new ArrayDouble.D2(latDim.getLength(), lonDim.getLength());
    // Index ind = data.getIndex();
    //
    // double[] dd = VisualizationFactory.get().getStateDataAsNumbers(auxVariables.get(varname));
    // int i = 0;
    // for (int lat = 0; lat < latDim.getLength(); lat++) {
    // for (int lon = 0; lon < lonDim.getLength(); lon++) {
    //
    // double val = dd[i];
    // if (mask != null) {
    // int[] xy = space.getXYCoordinates(i);
    // if (!mask.isActive(xy[0], xy[1])) {
    // val = Double.NaN;
    // }
    // }
    //
    // data.setFloat(ind.set(lat, lon), (float) val);
    // i++;
    // }
    // }
    //
    // try {
    // ncfile.write(varname, data);
    // } catch (Exception e) {
    // throw new ThinklabIOException(e);
    // }
    // }
    // }
    //
    // try {
    // ncfile.close();
    // } catch (IOException e) {
    // throw new ThinklabIOException(e);
    // }
    // }

    /*
     * Extract the proper name for a state. TODO this needs a lot of work. The metadata from the model and
     * concept should be considered.
     * 
     * just recognize some concepts that have special meaning for the netcdf CF convention
     */
    private String getVarname(IObservable state) {

        GeoNS.synchronize();

        IConcept obs = state.getType();

        String ret = obs.getLocalName() + "_" + obs.getConceptSpace();
        ret = ret.replace('.', '_');

        if (GeoNS.ELEVATION != null && obs.is(GeoNS.ELEVATION)) {
            ret = "Altitude";
        }
        return ret;
    }

    @Override
    public Collection<IState> getStates() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Collection<ISubject> getSubjects() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getId() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IMetadata getMetadata() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IObservable getObservable() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IScale getScale() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public INamespace getNamespace() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Var getVar(IObservable observable, int timeSlice) {

        Var ret = _vars.get(observable);
        if (ret == null) {
            ret = createVarFromStorage(observable);
        }

        ((CFVariable) ret).load(timeSlice);

        return ret;
    }

    private Var createVarFromStorage(IObservable observable) {
        // TODO Auto-generated method stub
        return null;
    }

    public Var getVar(IState state, int timeSlice) {

        Var ret = _vars.get(state.getObservable());
        if (ret == null) {
            ret = new CFVariable(state.getObservable(), state.getObserver());
            _vars.put(state.getObservable(), ret);
        }

        ((CFVariable) ret).load(timeSlice);

        return ret;
    }

    // ISemanticObject methods
    // FROM THIS POINT ON, THE METHODS ARE NOT EXPECTED TO WORK ON A PERSISTED OBJECT
    // FIXME not sure we need the ISemanticObject hierarchy but I think we should keep it for now (and improve
    // the API for it)
    // TODO implement the obvious ones, add inappropriate operation exceptions for the non-obvious

    @Override
    public IList getSemantics() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Object demote() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IConcept getDirectType() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean is(Object other) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public int getRelationshipsCount() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int getRelationshipsCount(IProperty property) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public ISemanticObject<?> get(IProperty property) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<Pair<IProperty, ISemanticObject<?>>> getRelationships() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<Triple<IProperty, ISemanticObject<?>, Integer>> getCountedRelationships() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<ISemanticObject<?>> getRelationships(IProperty property) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isLiteral() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isConcept() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isObject() {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public boolean isCyclic() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isValid() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public List<ISemanticObject<?>> getSortedRelationships(IProperty property)
            throws ThinklabCircularDependencyException {
        // TODO Auto-generated method stub
        return null;
    }

    // @Override
    // public void setContext(ISubject context) throws ThinklabException {
    // // TODO to be removed
    //
    // }
    //
    // @Override
    // public ISubject getContext() {
    // // TODO to be removed
    // return null;
    // }

    @Override
    public String persist(String location) throws ThinklabException {

        if (!location.endsWith(".nc")) {
            location = location + ".nc";
        }
        close();
        try {
            FileUtils.copyFile(_file, new File(location));
        } catch (IOException e) {
            throw new ThinklabIOException(e);
        }
        return null;
    }

    @Override
    public void restore(String location) throws ThinklabException {
        _file = new File(location);
        open();
    }

    @Override
    public void dispose() {
        try {
            close();
            FileUtils.deleteQuietly(_file);
        } catch (Exception e) {
            // throw new ThinklabRuntimeException(e);
        }
    }

    @Override
    public ICoverage observe(IObservable observable, Collection<String> scenarios, boolean isOptional)
            throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ICoverage initialize(Collection<String> scenarios) throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void contextualize() throws ThinklabException {
        // TODO Auto-generated method stub

    }

    @Override
    public void setMonitor(IMonitor monitor) {
        // TODO Auto-generated method stub

    }

    @Override
    public IMonitor getMonitor() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IResolutionStrategy getResolutionStrategy() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Collection<IProcess> getProcesses() {
        // TODO Auto-generated method stub
        return null;
    }

}
