package org.integratedmodelling.thinklab.modelling.functions.random;

import java.util.Map;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IExpression;
import org.integratedmodelling.thinklab.api.project.IProject;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.interfaces.annotations.Function;
import org.integratedmodelling.thinklab.modelling.lang.datasources.RandomDataSource;

@Function(id = "rand.poisson", parameterNames = { "lambda" }, returnTypes = { NS.DATASOURCE })
public class POISSON implements IExpression {

    IProject _project;

    @Override
    public Object eval(Map<String, Object> parameters, IConcept... context) throws ThinklabException {

        double lambda = 0.0;
        if (parameters.containsKey("lambda")) {
            lambda = Double.parseDouble(parameters.get("lambda").toString());
        }

        return new RandomDataSource("PoissonDist", lambda);
    }

    @Override
    public void setProjectContext(IProject project) {
        _project = project;
    }

}
