package org.integratedmodelling.thinklab.modelling.functions.random;

import java.util.Map;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IExpression;
import org.integratedmodelling.thinklab.api.project.IProject;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.interfaces.annotations.Function;
import org.integratedmodelling.thinklab.modelling.lang.datasources.RandomDataSource;

@Function(id = "rand.uniform", parameterNames = { "min", "max" }, returnTypes = { NS.DATASOURCE })
public class UNIFORM implements IExpression {

    IProject _project;

    @Override
    public Object eval(Map<String, Object> parameters, IConcept... context) throws ThinklabException {

        double min = 0.0;
        double max = 1.0;
        if (parameters.containsKey("min")) {
            min = Double.parseDouble(parameters.get("min").toString());
        }
        if (parameters.containsKey("max")) {
            max = Double.parseDouble(parameters.get("max").toString());
        }

        return new RandomDataSource("UniformDist", min, max);
    }

    @Override
    public void setProjectContext(IProject project) {
        _project = project;
    }

}
