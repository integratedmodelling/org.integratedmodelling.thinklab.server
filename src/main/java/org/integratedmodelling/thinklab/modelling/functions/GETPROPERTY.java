package org.integratedmodelling.thinklab.modelling.functions;

import java.util.Map;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IExpression;
import org.integratedmodelling.thinklab.api.modelling.parsing.IFunctionCall;
import org.integratedmodelling.thinklab.api.project.IProject;
import org.integratedmodelling.thinklab.common.ThinklabProperties;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.interfaces.annotations.Function;

@Function(
        id = "get-property",
        parameterNames = { IFunctionCall.DEFAULT_PARAMETER_NAME, "default", "id" },
        returnTypes = { NS.TEXT })
public class GETPROPERTY implements IExpression {

    IProject _project;

    @Override
    public Object eval(Map<String, Object> parameters, IConcept... context) throws ThinklabException {

        if (parameters.containsKey(IFunctionCall.DEFAULT_PARAMETER_NAME)) {
            return ThinklabProperties.getPropertyAsArray(parameters.get(IFunctionCall.DEFAULT_PARAMETER_NAME)
                    .toString(), _project);
        }

        Object ret = Thinklab.get().getProperties().getProperty(parameters.get("id").toString());
        if (ret == null)
            ret = parameters.get("default");

        return ret;
    }

    @Override
    public void setProjectContext(IProject project) {
        _project = project;
    }

}
