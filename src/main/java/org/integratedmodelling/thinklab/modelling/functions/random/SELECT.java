package org.integratedmodelling.thinklab.modelling.functions.random;

import java.util.ArrayList;
import java.util.Map;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IExpression;
import org.integratedmodelling.thinklab.api.lang.IList;
import org.integratedmodelling.thinklab.api.modelling.IAction;
import org.integratedmodelling.thinklab.api.project.IProject;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.interfaces.annotations.Function;
import org.integratedmodelling.thinklab.modelling.lang.datasources.RandomSelectDSAccessor;

@Function(id = "rand.select", parameterNames = { "values", "distribution" }, returnTypes = {
        NS.DATASOURCE,
        NS.STATE_ACCESSOR })
public class SELECT implements IExpression {

    IProject _project;

    @Override
    public Object eval(Map<String, Object> parameters, IConcept... context) throws ThinklabException {

        Object[] distribution = null;
        Object[] values = null;

        if (parameters.containsKey("values")) {
            Object _vals = parameters.get("values");
            if (!(_vals instanceof IList)) {
                throw new ThinklabValidationException("values in a rand.select function must be a list");
            }

            Object[] vals = new Object[((IList) _vals).length()];
            int i = 0;
            for (Object o : ((IList) _vals)) {
                vals[i] = o;
                i++;
            }
            values = vals;
        } else {
            throw new ThinklabValidationException("rand.select must contains a 'values' list to choose from");
        }

        if (parameters.containsKey("distribution")) {
            Object _dist = parameters.get("distribution");
            if (!(_dist instanceof IList)) {
                throw new ThinklabValidationException(
                        "distribution in a rand.select function must be a list of floating point numbers");
            }

            int i = 0;
            Object[] vals = new Object[((IList) _dist).length()];
            for (Object o : ((IList) _dist)) {
                if (context != null && context[0].is(Thinklab.c(NS.DATASOURCE)) && !(o instanceof Number)) {
                    throw new ThinklabValidationException(
                            "distribution in a rand.select datasource must be a list of floating point numbers");
                }
                vals[i] = o;
                i++;
            }

            distribution = vals;
        }

        /*
         * uniform dist if not specified
         */
        if (distribution == null) {
            distribution = new Object[values.length];
            for (int i = 0; i < distribution.length; i++) {
                distribution[i] = 1.0 / (double) distribution.length;
            }
        }

        if (distribution.length != values.length) {
            throw new ThinklabValidationException(
                    "distribution and values in rand.select must have the same number of items");
        }

        if (context == null || context[0].is(Thinklab.c(NS.DATASOURCE)))
            // datasource
            return new RandomSelectDSAccessor(values, distribution);

        // accessor
        return new RandomSelectDSAccessor(new ArrayList<IAction>(), null, values, distribution);
    }

    @Override
    public void setProjectContext(IProject project) {
        _project = project;
    }

}
