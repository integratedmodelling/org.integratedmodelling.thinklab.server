package org.integratedmodelling.thinklab.modelling.functions;

import java.util.Map;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IExpression;
import org.integratedmodelling.thinklab.api.project.IProject;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.interfaces.annotations.Function;
import org.integratedmodelling.thinklab.modelling.monitoring.StatisticsAccessor;

@Function(id = "stat.summary", parameterNames = {}, returnTypes = { NS.SUBJECT_ACCESSOR })
public class STATISTICS implements IExpression {

    IProject _project;

    @Override
    public Object eval(Map<String, Object> parameters, IConcept... context) throws ThinklabException {
        return new StatisticsAccessor();
    }

    @Override
    public void setProjectContext(IProject project) {
        _project = project;
    }

}
