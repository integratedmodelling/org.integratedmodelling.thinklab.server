package org.integratedmodelling.thinklab.modelling.functions;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.thinklab.api.data.IAggregator;
import org.integratedmodelling.thinklab.api.data.ITable;
import org.integratedmodelling.thinklab.api.data.ITableSet;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IExpression;
import org.integratedmodelling.thinklab.api.modelling.IAction;
import org.integratedmodelling.thinklab.api.project.IProject;
import org.integratedmodelling.thinklab.common.data.TableFactory;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.interfaces.annotations.Function;
import org.integratedmodelling.thinklab.modelling.lang.datasources.DatatableAccessor;
import org.integratedmodelling.thinklab.modelling.lang.expressions.GroovyExpression;

@Function(
        id = "data.tabular",
        parameterNames = { "file", "sheet", "column", "row-selector", "aggregation" },
        returnTypes = { NS.STATE_ACCESSOR })
public class DATATABLE implements IExpression {

    private IProject project;

    @Override
    public Object eval(Map<String, Object> parameters, IConcept... context) throws ThinklabException {

        ITable table = null;
        IExpression rowSelector = null;
        IAggregator operation = null;
        String column = null;

        if (parameters.containsKey("file")) {
            File file = new File((project == null ? "" : (project.getLoadPath() + File.separator))
                    + parameters.get("file"));
            ITableSet tableSet = TableFactory.open(file);

            if (tableSet != null) {
                String sheet = null;
                if (parameters.containsKey("table-name")) {
                    sheet = parameters.get("table-name").toString();
                }
                if (sheet != null) {
                    table = tableSet.getTable(sheet);
                } else {
                    Collection<ITable> tables = tableSet.getTables();
                    if (tables.size() > 0) {
                        table = tables.iterator().next();
                    }
                }

                if (table == null) {
                    throw new ThinklabValidationException("data.tabular: cannot find "
                            + (sheet == null ? "default" : sheet) + " table");
                }

            }
        }
        if (parameters.containsKey("column")) {
            column = parameters.get("column").toString();
        }
        if (parameters.containsKey("row-selector")) {
            rowSelector = new GroovyExpression(parameters.get("row-selector").toString());
        }
        if (parameters.containsKey("aggregation")) {
            String op = parameters.get("aggregation").toString();
            operation = TableFactory.getAggregator(op);
            if (operation == null) {
                throw new ThinklabValidationException("data.tabular: cannot find operation " + op);
            }
        }

        // average is the default aggregation
        if (operation == null) {
            operation = TableFactory.getAggregator("mean");
        }

        return new DatatableAccessor(new ArrayList<IAction>(), table, rowSelector, operation, column);
    }

    @Override
    public void setProjectContext(IProject project) {
        this.project = project;
    }

}
