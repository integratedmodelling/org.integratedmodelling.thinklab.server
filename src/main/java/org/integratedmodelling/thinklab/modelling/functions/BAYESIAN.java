package org.integratedmodelling.thinklab.modelling.functions;

import java.util.ArrayList;
import java.util.Map;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IExpression;
import org.integratedmodelling.thinklab.api.modelling.IAction;
import org.integratedmodelling.thinklab.api.project.IProject;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.interfaces.annotations.Function;
import org.integratedmodelling.thinklab.modelling.bayes.BayesianAccessor;

@Function(id = "bayesian", parameterNames = { "import", "method" }, returnTypes = { NS.STATE_ACCESSOR })
public class BAYESIAN implements IExpression {

    private IProject project;

    @Override
    public Object eval(Map<String, Object> parameters, IConcept... context) throws ThinklabException {

        if (project == null || !parameters.containsKey("import"))
            return null;

        return new BayesianAccessor(new ArrayList<IAction>(), parameters.get("import").toString(),
                project.getLoadPath());
    }

    @Override
    public void setProjectContext(IProject project) {
        this.project = project;
    }

}
