package org.integratedmodelling.thinklab.modelling.functions.random;

import java.util.Map;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IExpression;
import org.integratedmodelling.thinklab.api.project.IProject;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.interfaces.annotations.Function;
import org.integratedmodelling.thinklab.modelling.lang.datasources.RandomDataSource;

@Function(id = "rand.normal", parameterNames = { "mean", "std" }, returnTypes = { NS.DATASOURCE })
public class NORMAL implements IExpression {

    IProject _project;

    @Override
    public Object eval(Map<String, Object> parameters, IConcept... context) throws ThinklabException {

        double mean = 0.0;
        double std = 1.0;
        if (parameters.containsKey("mean")) {
            mean = Double.parseDouble(parameters.get("mean").toString());
        }
        if (parameters.containsKey("std")) {
            std = Double.parseDouble(parameters.get("std").toString());
        }

        return new RandomDataSource("NormalDist", mean, std);
    }

    @Override
    public void setProjectContext(IProject project) {
        _project = project;
    }

}
