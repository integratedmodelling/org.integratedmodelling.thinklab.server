package org.integratedmodelling.thinklab.modelling.resolver;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.knowledge.ISemanticObject;
import org.integratedmodelling.thinklab.api.knowledge.query.IQuery;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.modelling.IDataSource;
import org.integratedmodelling.thinklab.api.modelling.IModel;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.api.modelling.resolution.IModelPrioritizer;
import org.integratedmodelling.thinklab.api.modelling.resolution.IObservationKbox;
import org.integratedmodelling.thinklab.api.modelling.resolution.IResolutionContext;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.debug.DataRecorder;
import org.integratedmodelling.thinklab.kbox.neo4j.NeoKBox;
import org.integratedmodelling.thinklab.modelling.lang.Model;
import org.integratedmodelling.thinklab.modelling.lang.Namespace;
import org.integratedmodelling.thinklab.query.Queries;

/**
 * kbox with additional methods to store, retrieve and rank model data.
 */
public class ObservationKbox extends NeoKBox implements IObservationKbox {

    public ObservationKbox(String url) {
        super(url);
    }

    /**
     * If object is a model, store not only data for the model itself but also for any
     * dereifying models associated. 
     */
    @Override
    public synchronized long store(Object o, IMonitor monitor) throws ThinklabException {

        ArrayList<Object> toStore = new ArrayList<Object>();

        if (o instanceof IModel) {
            toStore.add((new ModelData((Model) o, monitor)));

            for (String attr : ((IModel) o).getObjectAttributes()) {
                toStore.add(new ModelData((Model) o, attr, monitor));
            }

            /*
             * TODO may check a flag to ensure the model is worth storing after analysis in ModelData constructor.
             */
            if (((IModel) o).getObjectSource() instanceof IDataSource) {
                // presence model
                toStore.add(new ModelData((Model) o, ModelData.PRESENCE_ATTRIBUTE, monitor));
            }

        } else {
            toStore.add(o);
        }

        long ret = -1;
        for (Object obj : toStore) {
            long r = super.store(obj, monitor);
            if (ret < 0)
                ret = r;
        }

        return ret;
    }

    /**
     * Pass the timestamp of a namespace to check if its objects need to be stored.
     * If the stored namespace record does not exist or has a timestamp older than the
     * passed one, remove all objects that belong to it and return true. Does not store
     * a new namespace record - this should be done when this has returned true and there
     * were no errors.
     * 
     * Returns: 0 if no need to refresh, 1 if it must be entirely refreshed and every model and
     * namespace record is removed from the kbox, and 2 if the models without errors need to be
     * checked again (they may be in or not).
     * 
     * @param namespaceId
     * @param time
     * @throws ThinklabException 
     */
    public int removeNamespaceIfOlder(String namespaceId, long timestamp) throws ThinklabException {

        IQuery query = Queries.select(NS.NAMESPACE).restrict(NS.HAS_ID, Queries.is(namespaceId));
        List<ISemanticObject<?>> res = this.query(query);
        long dbTimestamp = 0;
        Namespace namespace = null;

        if (res.size() > 0) {
            namespace = (Namespace) res.get(0);
            dbTimestamp = namespace.getTimeStamp();
        }

        /*
         * if we have stored something and we are younger than the stored ns, remove all models coming
         * from it so we can add our new ones.
         */
        if (timestamp > dbTimestamp) {

            if (dbTimestamp > 0) {
                this.removeAll(Queries.select(NS.MODEL_DATA).restrict(NS.HAS_NAMESPACE_ID,
                        Queries.is(namespaceId)));
                this.removeAll(Queries.select(NS.NAMESPACE).restrict(NS.HAS_ID, Queries.is(namespaceId)));
            }

            DataRecorder.debug("Refresh " + namespaceId + ": stored  " + new Date(dbTimestamp) + " < "
                    + new Date(timestamp));

            return 1;
        }

        /*
         * if we have not changed the source file but models had errors when stored, return the
         * conservative mode so we can check model by model and only store those that are no longer in
         * error due to external reasons.
         */
        if (namespace != null && namespace.hasErrors()) {
            return 2;
        }

        return 0;
    }

    @Override
    public ObservationQueryResult query(IObservable observable, IResolutionContext context)
            throws ThinklabException {

        IModelPrioritizer<ModelData> prioritizer = new ModelPrioritizer(context);
        ObservationQueryResult ret = new ObservationQueryResult(prioritizer);

        /*
         * TODO get all connected servers from user model (to be passed as a parameter) and dispatch a query/add thread to each; if there is one
         * server or more, wait for all threads to finish or fail.
         */
        for (ISemanticObject<?> o : this.query(ObservationQueries.contextQuery(observable, context))) {

            if (o == null)
                continue;

            ModelData md = (ModelData) o.demote();

            /*
             * TODO if there is a namespace whitelist/blacklist, filter
             */
            md.ranks = prioritizer.computeCriteria(md, context);
            ret.addModelData(md);
        }

        return ret;
    }

}
