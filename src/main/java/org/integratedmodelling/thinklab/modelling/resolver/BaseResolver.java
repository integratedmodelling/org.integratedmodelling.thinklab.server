package org.integratedmodelling.thinklab.modelling.resolver;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.integratedmodelling.collections.Triple;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabInternalErrorException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.thinklab.api.knowledge.IProperty;
import org.integratedmodelling.thinklab.api.modelling.IModel;
import org.integratedmodelling.thinklab.api.modelling.IObservingObject;
import org.integratedmodelling.thinklab.api.modelling.IObservingObject.IDependency;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.api.modelling.resolution.IResolutionContext;
import org.integratedmodelling.thinklab.api.modelling.resolution.ISubjectResolver;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.modelling.lang.Model;
import org.integratedmodelling.thinklab.modelling.lang.ObservingObject;
import org.integratedmodelling.thinklab.modelling.lang.ObservingObject.Dependency;
import org.integratedmodelling.thinklab.modelling.lang.Subject;

/**
 * Provides all basic resolver functionalities except the actual resolve() methods. Separated 
 * only to keep Resolver clean and readable and still have the full resolver logics in it (without
 * separating in artificial subclasses as it was earlier).
 * 
 * @author ferdinando.villa
 *
 */
public abstract class BaseResolver implements ISubjectResolver {

    // holds externally inserted dependency data
    ArrayList<Triple<IObservable, IProperty, Boolean>> _depData = new ArrayList<Triple<IObservable, IProperty, Boolean>>();

    /**
     * Add a dependency to be observed in the context of the subject we are modeling.
     * just note down the data, then create the dependencies in context at resolution time.
     * 
     * @param observable
     * @param distribute
     * @param property
     * @throws ThinklabException
     */
    public void addDependency(IObservable observable, IProperty property, boolean optional)
            throws ThinklabException {
        _depData.add(new Triple<IObservable, IProperty, Boolean>(observable, property, optional));
    }

    /*
     * get the externally defined dependencies, ensuring they're OK for the context we're in.
     */
    protected List<IDependency> getExternalDependencies(IResolutionContext context) throws ThinklabException {
        ArrayList<IDependency> ret = new ArrayList<IObservingObject.IDependency>();

        IObservable s = context.getSubject().getObservable();

        for (Triple<IObservable, IProperty, Boolean> dd : _depData) {

            IObservable observable = dd.getFirst();
            IProperty property = dd.getSecond();
            boolean optional = dd.getThird();

            boolean distribute = observable.getModel() != null && observable.getModel().isReificationModel();

            if (observable.getInherentType() != null && !s.getType().is(observable.getInherentType())) {
                context.getMonitor().error(new ThinklabValidationException("this object requires a "
                        + observable.getInherentType() + " as context"));
                continue;
            }

            /*
             * if the dependency is on an abstract concept, we observe all the disjoint children concepts of it.
             */
            ArrayList<IObservable> ob = new ArrayList<IObservable>();
            // if (observable.getModel() == null && observable.getType().isAbstract()) {
            // for (IConcept c : observable.getType().getDisjointConcreteChildren()) {
            // ob.add(new Observable(c));
            // }
            // } else {
            ob.add(observable);
            // }

            for (IObservable o : ob) {

                /*
                 * we MUST have a property. This is the key that links the observable to the parent.
                 */
                if (property == null) {
                    property = ObservingObject.getPropertyFor(o, s, context.getNamespace());
                }

                ret.add(new Dependency(o, o.getFormalName(), property, optional, distribute, null));
            }

        }

        return ret;
    }

    /**
     * Group together the dependencies of the passed object (model or observer) into groups to be resolved in
     * sequence. This will collect all consecutive data dependencies in one group and leave process and agent
     * dependencies by themselves.
     * 
     * @throws ThinklabException
     */
    public List<List<IDependency>> groupDependencies(Object observer, IResolutionContext context)
            throws ThinklabException {

        List<List<IDependency>> ret = new ArrayList<List<IDependency>>();
        List<IDependency> deps = null;

        /*
         * external dependencies are for the root subject
         */
        if (observer instanceof Subject) {
            deps = ((Subject) observer).getDependencies();
        } else if (observer instanceof IObservingObject) {
            deps = new ArrayList<IObservingObject.IDependency>(((IObservingObject) observer).getDependencies());
        }

        if (deps == null || deps.size() == 0) {
            return ret;
        }

        ArrayList<IDependency> last = null;
        for (IDependency d : deps) {

            for (IDependency dep : getConcreteDependencies(d)) {
                IObservable obs = dep.getObservable();

                if (NS.isProcess(obs)) {
                    if (last != null) {
                        ret.add(last);
                    }
                    ret.add(Collections.singletonList(dep));
                    last = null;
                } else if (NS.isQuality(obs)) {
                    if (last == null) {
                        last = new ArrayList<IObservingObject.IDependency>();
                    }
                    last.add(dep);
                } else if (NS.isObject(obs)) {
                    if (last != null) {
                        ret.add(last);
                    }
                    ret.add(Collections.singletonList(dep));
                    last = null;
                } else {

                    /*
                     * shouldn't happen unless the ontologies are fubar
                     */
                    throw new ThinklabInternalErrorException(obs
                            + " cannot be categorized as quality, agent or process");
                }
            }
        }

        if (last != null) {
            ret.add(last);
        }

        return ret;
    }

    private List<IDependency> getConcreteDependencies(IDependency dependency) {
        return dependency.isGeneric() ? ((Dependency) dependency).concretize() : Collections
                .singletonList(dependency);
    }

    // helper
    public static List<Dependency> getDataDependencies(IModel model) {

        List<Dependency> ret = new ArrayList<Dependency>();

        for (IDependency d : model.getDependencies()) {
            if (d.getProperty().isLiteralProperty()) {
                ret.add((Dependency) d);
            }
        }

        return ret;
    }

    /**
     * Create a model by merging all the passed ones into a conditional model that
     * uses the call order for selection. If any model is a merged model, unwrap
     * it and merge the result again in a flat list.
     * 
     * @param models
     * @return
     * @throws ThinklabException
     */
    public static IModel mergeModels(IModel... models) throws ThinklabException {

        ArrayList<IModel> mods = new ArrayList<IModel>();

        for (IModel m : models) {
            if (((Model) m).isMerged()) {

            }
        }

        // IModel model = new Model(SemanticObject.newInstance(concept.getType()), CamelCase.toLowerCase(
        // concept.getLocalName(), '-'), subjectObserver.getSubject().getNamespace(), models);
        // ProvenanceNode obs = ctx.modelGraph.add(model.getObserver());
        //
        // int cidx = 0;
        // for (ProvenanceNode node : conditions) {
        // DependencyEdge de = new DependencyEdge(DependencyEdge.CONDITIONAL_DEPENDENCY, "", concept);
        // de.conditionIndex = cidx++;
        // ctx.modelGraph.addEdge(node, obs, de);
        // }
        //
        // ProvenanceNode ret = ctx.modelGraph.add(model);
        // ret.coverage = coverage;
        //
        // ctx.modelGraph.addEdge(
        // obs,
        // ret,
        // new DependencyEdge(DependencyEdge.DEFINE_STATE, "", subjectObserver.getObservable(model
        // .getObserver())));
        return null;
    }

}
