package org.integratedmodelling.thinklab.modelling.resolver.obsolete;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.modelling.ICoverage;
import org.integratedmodelling.thinklab.api.modelling.IModel;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.debug.CallTracer;
import org.integratedmodelling.thinklab.modelling.resolver.Coverage;
import org.integratedmodelling.thinklab.modelling.resolver.ProvenanceGraph;
import org.integratedmodelling.thinklab.modelling.resolver.ResolutionContext;
import org.integratedmodelling.thinklab.modelling.resolver.ProvenanceGraph.ProvenanceNode;

public class ConceptResolver extends AbstractResolver {

    private final IObservable concept;
    private final ProvenanceNode root;
    private final ResolutionContext ctx;

    public ConceptResolver(SubjectObserver subjectObserver, IObservable concept, ProvenanceNode root,
            ResolutionContext ctx, IMonitor monitor) {
        super(subjectObserver, monitor);
        this.concept = concept;
        this.root = root;
        this.ctx = ctx;
    }

    @Override
    public ICoverage resolve(Collection<String> scenarios) throws ThinklabException {

        CallTracer.indent("resolveConcept()", this, concept, root);

        // DataRecorder.debug("Resolving " + concept + " with observer " + ctx.observer);

        ProvenanceNode node = findResolved(ctx.getObservableClosure(concept));
        if (node != null) {

            /**
             * when looking for data not found previously, the node is in the cache but not in the graph -
             * which throws a "no such vertex in graph" exception instead of a proper one
             */
            if (!ctx.modelGraph.containsVertex(node)) {
                return null;
            }

            ctx.modelGraph.addEdge(node, root, ctx.link);
            CallTracer.msg("findResolved() found non-null ProvenanceNode");
            CallTracer.unIndent();
            return node.coverage;
        }

        Set<IObservable> observables = ctx.getObservableClosure(concept);
        IModel ret = findStateFor(observables, ctx);
        if (ret != null) {
            CallTracer.msg("findKnownDatasources() found non-null IModel");
            CallTracer.unIndent();
            AbstractResolver r = new DatasourceResolver(subjectObserver, ret.getDatasource(), root, ret, ctx,
                    monitor);
            return r.resolve(scenarios);
        }

        Pair<Coverage, List<Pair<SubjectObserver, Coverage>>> matching = findModels(concept, ctx);

        if (matching == null) {
            CallTracer.msg("findDataModels() returned null");
            CallTracer.unIndent();
            return null;
        }

        CallTracer.msg("findDataModels() returned something meaningful. Computing coverage...");
        Coverage coverage = matching.getFirst();
        if (coverage.getCoverage() >= subjectObserver.MIN_MODEL_COVERAGE) {
            CallTracer.msg("coverage adequate. calling accept()...");
            accept(concept, coverage, matching.getSecond(), root, ctx);
        } else {
            CallTracer.msg("coverage was inadequate.");
        }

        CallTracer.unIndent();
        return coverage;

    }

    //
    //    /**
    //     * TODO use this as a model for a single findModel to be located in AbstractResolver, used for all
    //     * types of observables. 
    //     * 
    //     * Process observables should probably use a single model to avoid deconstruction
    //     * of dependents, but subject models may well use more than one to generate subjects in a context so
    //     * the process should be the same. Also, the sandbox resolver should be used for all of these, not
    //     * just for data.
    //     * 
    //     * @param observables
    //     * @param scale
    //     * @param ctx
    //     * @return the model (possibly multiple) that observes any of the passed concepts in the context, and its
    //     * coverage. If no model is found, model is null and coverage is 0.
    //     * @throws ThinklabException
    //     */
    //    private Pair<Coverage, List<Pair<SubjectObserver, Coverage>>> findDataModels(IObservable observable,
    //            ResolutionContext ctx) throws ThinklabException {
    //
    //        Set<IObservable> observables = ctx.getObservableClosure(observable);
    //
    //        /*
    //         * this can only happen during proactive resolution attempts called from within the loop below.
    //         */
    //        if (checkInfiniteRecursion(observable) || (monitor != null && monitor.isStopped())) {
    //            return null;
    //        }
    //
    //        //        // to be substituted with a simple loop on the result of
    //        //        // obsKbox.query(observable, getResolutionCriteria(scenarios)
    //        //
    //        //        ModelQuery query = new ModelQuery(observables, Thinklab.get().requireKbox(Thinklab.DEFAULT_KBOX),
    //        //                ctx.scale, ctx.observer, ctx.model == null ? null : ctx.model.getNamespace(), getResolutionCriteria(scenarios), scenarios, ctx.nsWhitelist,
    //        //                ctx.nsBlacklist, monitor);
    //        //
    //        //        /*
    //        //         * define query. That's a big query strategy. Start with the whole strategy within the scenarios if we
    //        //         * have them, then repeat without scenarios.
    //        //         * 
    //        //         * Strategy for data models is:
    //        //         * 
    //        //         * 1. resolved data models with specific context mentioned; 2. unresolved data models with specific
    //        //         * context; 3. subject models with specific context; 4,5,6: like 1,2,3 but without specific context.
    //        //         * 
    //        //         * Namespace whitelist and blacklist are handled automatically within ModelQuery, as is the
    //        //         * metadata-driven ranking.
    //        //         */
    //        //        // if (_scenarios != null && _scenarios.size() > 0) {
    //        //        //
    //        //        // query.add(ModelQuery.IN_SCENARIOS | ModelQuery.DATA_MODELS |
    //        //        // ModelQuery.RESOLVED_MODELS | ModelQuery.CONTEXT_SPECIFIC);
    //        //        //
    //        //        // query.add(ModelQuery.IN_SCENARIOS | ModelQuery.DATA_MODELS | ModelQuery.UNRESOLVED_MODELS |
    //        //        // ModelQuery.CONTEXT_SPECIFIC);
    //        //        //
    //        //        // query.add(ModelQuery.IN_SCENARIOS | ModelQuery.PROCESS_MODELS |ModelQuery.CONTEXT_SPECIFIC);
    //        //        //
    //        //        // query.add(ModelQuery.IN_SCENARIOS | ModelQuery.DATA_MODELS |
    //        //        // ModelQuery.RESOLVED_MODELS | ModelQuery.CONTEXT_INDEPENDENT);
    //        //        //
    //        //        // query.add(ModelQuery.IN_SCENARIOS | ModelQuery.DATA_MODELS |
    //        //        // ModelQuery.UNRESOLVED_MODELS | ModelQuery.CONTEXT_INDEPENDENT);
    //        //        //
    //        //        // query.add(ModelQuery.IN_SCENARIOS | ModelQuery.PROCESS_MODELS | ModelQuery.CONTEXT_INDEPENDENT);
    //        //        // }
    //        //
    //        //        /*
    //        //         * TODO the following should substitute all the queries above and below except the last. Query groups
    //        //         * with nested loops where the following are added in order:
    //        //         * 
    //        //         * for each of in scenario, in namespace, not in namespace for each of resolved, unresolved for each
    //        //         * group of context specific, context independent data models inherent to subject OR to dependency
    //        //         * (observer) if specified (data models inherent to nothing - if dependency (observer) is not inherent
    //        //         * OR subject models whose subjects can produce the inherency info - only if dependency is inherent)
    //        //         * process models
    //        //         */
    //        //
    //        //        int[] scenarioFlags = (scenarios != null && scenarios.size() > 0) ? new int[] {
    //        //                ModelQuery.IN_SCENARIOS,
    //        //                ModelQuery.NOT_IN_SCENARIOS | ModelQuery.IN_DEFAULT_NAMESPACE,
    //        //                ModelQuery.NOT_IN_SCENARIOS }
    //        //                : new int[] {
    //        //                        ModelQuery.NOT_IN_SCENARIOS | ModelQuery.IN_DEFAULT_NAMESPACE,
    //        //                        ModelQuery.NOT_IN_SCENARIOS };
    //        //
    //        //        for (int sc_flag : scenarioFlags) {
    //        //            for (int rs_flag : new int[] { ModelQuery.RESOLVED_MODELS, ModelQuery.UNRESOLVED_MODELS }) {
    //        //                for (int ct_flag : new int[] { ModelQuery.CONTEXT_SPECIFIC, ModelQuery.CONTEXT_INDEPENDENT }) {
    //        //
    //        //                    // data models inherent to : observer if specified, subject otherwise;
    //        //                    query.add(sc_flag | rs_flag | ct_flag | ModelQuery.DATA_MODELS
    //        //                            | ModelQuery.INHERENT_MODELS);
    //        //
    //        //                    // base query:
    //        //                    // if (obs is inherent)
    //        //                    // subject models with appropriate attributes (ALWAYS ok for presence);
    //        //                    // else
    //        //                    // data models inherent to nothing;
    //        //                    query.add(sc_flag | rs_flag | ct_flag | ModelQuery.DATA_MODELS);
    //        //                }
    //        //            }
    //        //        }
    //        //
    //        //        /*
    //        //         * add search only in SAME namespace of context first; then add PUBLIC_MODELS to all subsequent
    //        //         * queries.
    //        //         */
    //        //        // query.add(ModelQuery.NOT_IN_SCENARIOS | ModelQuery.DATA_MODELS | ModelQuery.IN_DEFAULT_NAMESPACE |
    //        //        // ModelQuery.RESOLVED_MODELS | ModelQuery.CONTEXT_SPECIFIC);
    //        //        //
    //        //        // query.add(ModelQuery.NOT_IN_SCENARIOS | ModelQuery.DATA_MODELS | ModelQuery.IN_DEFAULT_NAMESPACE |
    //        //        // ModelQuery.UNRESOLVED_MODELS | ModelQuery.CONTEXT_SPECIFIC);
    //        //        //
    //        //        // query.add(ModelQuery.NOT_IN_SCENARIOS | ModelQuery.PROCESS_MODELS | ModelQuery.IN_DEFAULT_NAMESPACE
    //        //        // |
    //        //        // ModelQuery.CONTEXT_SPECIFIC);
    //        //        //
    //        //        // query.add(ModelQuery.NOT_IN_SCENARIOS | ModelQuery.DATA_MODELS | ModelQuery.IN_DEFAULT_NAMESPACE |
    //        //        // ModelQuery.RESOLVED_MODELS | ModelQuery.CONTEXT_INDEPENDENT);
    //        //        //
    //        //        // query.add(ModelQuery.NOT_IN_SCENARIOS | ModelQuery.DATA_MODELS | ModelQuery.IN_DEFAULT_NAMESPACE |
    //        //        // ModelQuery.UNRESOLVED_MODELS | ModelQuery.CONTEXT_INDEPENDENT);
    //        //        //
    //        //        // query.add(ModelQuery.NOT_IN_SCENARIOS | ModelQuery.DATA_MODELS |
    //        //        // ModelQuery.RESOLVED_MODELS | ModelQuery.CONTEXT_SPECIFIC);
    //        //        //
    //        //        // query.add(ModelQuery.NOT_IN_SCENARIOS | ModelQuery.DATA_MODELS |
    //        //        // ModelQuery.UNRESOLVED_MODELS | ModelQuery.CONTEXT_SPECIFIC);
    //        //        //
    //        //        // query.add(ModelQuery.NOT_IN_SCENARIOS | ModelQuery.PROCESS_MODELS |
    //        //        // ModelQuery.CONTEXT_SPECIFIC);
    //        //        //
    //        //        // query.add(ModelQuery.NOT_IN_SCENARIOS | ModelQuery.DATA_MODELS |
    //        //        // ModelQuery.RESOLVED_MODELS | ModelQuery.CONTEXT_INDEPENDENT);
    //        //        //
    //        //        // query.add(ModelQuery.NOT_IN_SCENARIOS | ModelQuery.DATA_MODELS |
    //        //        // ModelQuery.UNRESOLVED_MODELS | ModelQuery.CONTEXT_INDEPENDENT);
    //        //
    //        //        // the very last choice
    //        //        query.add(ModelQuery.NOT_IN_SCENARIOS | ModelQuery.PROCESS_MODELS | ModelQuery.CONTEXT_INDEPENDENT);
    //
    //        /*
    //         * perform one query at a time and accumulate resolvable models until the model set has enough
    //         * coverage of the context.
    //         */
    //        Coverage coverage = new Coverage(ctx.scale);
    //        ArrayList<Pair<SubjectObserver, Coverage>> accepted = new ArrayList<Pair<SubjectObserver, Coverage>>();
    //        IObservationKbox kbox = Thinklab.get().requireKbox(Thinklab.DEFAULT_KBOX);
    //
    //        for (IModel m : kbox.query(observable, ctx)) {
    //
    //            SubjectObserver sandbox = subjectObserver.getSandboxObserver(m, observables, ctx.optional);
    //            Coverage mcov = (Coverage) sandbox.resolve(ctx.getScenarios());
    //
    //            /*
    //             * only accept models that contribute enough ADDITIONAL coverage - which cannot happen unless
    //             * they have a minimum coverage of as much context.
    //             */
    //            if (mcov != null && mcov.getCoverage() >= subjectObserver.MIN_MODEL_COVERAGE) {
    //
    //                double pcov = coverage.getCoverage();
    //                Coverage ncov = coverage.or(mcov, subjectObserver.MIN_MODEL_COVERAGE);
    //                if (ncov.getCoverage() - pcov >= subjectObserver.MIN_MODEL_COVERAGE) {
    //                    accepted.add(new Pair<SubjectObserver, Coverage>(sandbox, mcov));
    //                    coverage = ncov;
    //                    if (monitor != null) {
    //                        monitor.info(
    //                                m.getName()
    //                                        + (accepted.size() == 1 ? (" covers "
    //                                                + StringUtils.percent(ncov.getCoverage() - pcov) + " of " + observable
    //                                                .getType()) : (" adds "
    //                                                + StringUtils.percent(ncov.getCoverage() - pcov)
    //                                                + " coverage of " + observable.getType())), null);
    //                    }
    //                }
    //                if (coverage.getCoverage() >= subjectObserver.REQUIRED_COVERAGE) {
    //                    break;
    //                }
    //            }
    //        }
    //
    //        if (monitor != null && accepted.size() == 0) {
    //            if (coverage.getCoverage() > 0.00001) {
    //                monitor.info(
    //                        "concept " + observable.getType() + " has insufficient coverage in this context",
    //                        null);
    //            } else {
    //                // FIXME redundant? Clearly it is if it's part of a dependency.
    //                monitor.info("concept " + observable.getType() + " cannot be observed in this context", null);
    //            }
    //        }
    //
    //        return new Pair<Coverage, List<Pair<SubjectObserver, Coverage>>>(coverage, accepted);
    //    }

}
