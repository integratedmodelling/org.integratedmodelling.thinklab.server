package org.integratedmodelling.thinklab.modelling.resolver.obsolete;

import java.util.Collection;

import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.modelling.IActiveSubject;
import org.integratedmodelling.thinklab.api.modelling.ICoverage;
import org.integratedmodelling.thinklab.api.modelling.IModel;
import org.integratedmodelling.thinklab.api.modelling.IObservingObject.IDependency;
import org.integratedmodelling.thinklab.api.modelling.IState;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.debug.CallTracer;
import org.integratedmodelling.thinklab.modelling.lang.Subject;
import org.integratedmodelling.thinklab.modelling.resolver.ResolutionContext;

public class ProcessResolver extends AbstractResolver {

    private final ResolutionContext ctx;
    private ICoverage ret;
    private final IDependency dependency;

    public ProcessResolver(SubjectObserver subjectObserver, ResolutionContext ctx, ICoverage ret,
            IDependency dependency, IMonitor monitor) {
        super(subjectObserver, monitor);
        this.ctx = ctx;
        this.ret = ret;
        this.dependency = dependency;
    }

    @Override
    public ICoverage resolve(Collection<String> scenarios) throws ThinklabException {

        CallTracer.indent("resolveProcesses()", this, ctx, ctx.getScale(), dependency);

        ISubject rootSubject = subjectObserver.getSubject();
        ISubject dependentSubject;
        SubjectObserver dependentSubjectObserver;
        ICoverage dcov;

        for (Pair<SubjectObserver, IModel> ds : createDependentSubjects(dependency, ctx)) {

            dependentSubjectObserver = ds.getFirst();
            dependentSubject = dependentSubjectObserver.getSubject();
            dcov = dependentSubjectObserver.resolve(ds.getSecond(), subjectObserver,
                    dependency.getProperty(), true, ctx);
            // IModel subjectModel, SubjectObserver context, IProperty pcontext, boolean initializeOnly,
            // ResolutionContext ctx

            // FIXME should processes have full coverage?
            if (dcov != null && dcov.getCoverage() > subjectObserver.MIN_MODEL_COVERAGE) {
                ret.and(dcov);
                ((IActiveSubject) rootSubject).addSubject(dependentSubjectObserver, dependency.getProperty());
                subjectObserver.getSubjects().add(dependentSubjectObserver);
                if (monitor != null) {
                    monitor.info("process " + dependentSubject.getId() + " resolved", null);
                }
            } else {
                ret = dcov;
                if (monitor != null) {
                    monitor.error(addError("error resolving dependent process "
                            + ((Subject) dependentSubject).getId()));
                }
            }

            /*
             * TODO - what to do with the resolution strategy? Should probably insert the workflow from the
             * subject
             */

            /*
             * these are processes, so transfer the states to the object if necessary and remove them from the
             * process itself.
             */
            for (IState s : dependentSubject.getStates()) {
                ((IActiveSubject) rootSubject).addState(s);
            }
            ((IActiveSubject) dependentSubject).removeStates();
        }

        CallTracer.unIndent();
        return ret;
    }

}
