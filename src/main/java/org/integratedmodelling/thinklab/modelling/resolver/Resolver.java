package org.integratedmodelling.thinklab.modelling.resolver;

import java.util.Collection;
import java.util.List;

import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.lang.LogicalConnector;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.knowledge.IExpression;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.modelling.IActiveSubject;
import org.integratedmodelling.thinklab.api.modelling.IConditionalObserver;
import org.integratedmodelling.thinklab.api.modelling.ICoverage;
import org.integratedmodelling.thinklab.api.modelling.IModel;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.IObservingObject.IDependency;
import org.integratedmodelling.thinklab.api.modelling.IScale;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.api.modelling.ISubjectAccessor;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.api.modelling.resolution.IObservationKbox;
import org.integratedmodelling.thinklab.api.modelling.resolution.IProvenance;
import org.integratedmodelling.thinklab.api.modelling.resolution.IResolutionContext;
import org.integratedmodelling.thinklab.api.modelling.resolution.ISubjectResolver;
import org.integratedmodelling.thinklab.common.monitoring.Notification;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.modelling.lang.Subject;

public class Resolver extends BaseResolver implements ISubjectResolver {

    @Override
    public ICoverage resolve(ISubject subject, Collection<String> scenarios, IMonitor monitor)
            throws ThinklabException {

        ResolutionContext rc = ResolutionContext.root(monitor, scenarios);
        return resolve(subject, rc.forSubject(subject));
    }

    private ICoverage resolve(IModel model, ResolutionContext context) throws ThinklabException {

        if (context.isUnsatisfiable)
            return Coverage.EMPTY;

        IScale scale = context.getScale();
        ISubjectAccessor accessor = (ISubjectAccessor) model.getAccessor(scale, context.getMonitor());

        // run pre-initializer method if any, and reset scale that may have been modified
        if (accessor != null) {
            accessor.notifyModel(model);
            for (IObservable observable : model.getObservables()) {
                if (NS.isQuality(observable)) {
                    accessor.notifyExpectedOutput(observable, observable.getObserver(),
                            observable.getFormalName());
                }
            }
        }

        if (context.monitor.hasErrors()) {
            return Coverage.EMPTY;
        }

        if (model.getObserver() != null) {
            if (resolve(model.getObserver(), context.forObserver(model.getObserver(), model.getDatasource()))
                    .isEmpty()) {
                return Coverage.EMPTY;
            }
        }

        /*
         * resolve all model dependencies, contextualizing at every group if the model is
         * a subject model. This will call finish() on the context passed.
         */
        ICoverage ret = resolveDependencies(model, context, NS.isObject(model.getObservable()));

        if (model.getObjectSource() != null) {
            context.forObjectSource(model.getObjectSource()).finish();
        }

        if (!context.monitor.hasErrors()) {

            if (accessor != null) {
                accessor.process(context.getSubject(), context.getContextSubject(),
                        context.getPropertyContext(), context.getMonitor());
            }
        }

        return ret;

    }

    /*
     * resolves dependencies for a model, subject or observer.
     */
    private ICoverage resolveDependencies(Object observation, ResolutionContext context, boolean execute)
            throws ThinklabException {

        if (context.isUnsatisfiable)
            return Coverage.EMPTY;

        ICoverage ret = Coverage.FULL(context.getScale());

        for (List<IDependency> dg : groupDependencies(observation, context)) {

            ResolutionContext cd = context.forGroup(LogicalConnector.INTERSECTION);
            ret = cd.getCoverage();
            for (IDependency d : dg) {
                if (resolve(d, cd.forDependency(d)).isEmpty()) {
                    return Coverage.EMPTY;
                }
            }

            ret = cd.finish();

            // run if requested. Object generation should have created provenance steps although at the moment
            // this doesn't happen.
            if (execute && ret.isRelevant() && !contextualize(cd)) {
                return Coverage.EMPTY;
            }
        }

        return context.finish();
    }

    /*
     * If the resolution so far has resulted in a non-empty workflow, run it and reset the context for
     * further resolution.
     */
    private boolean contextualize(ResolutionContext context) throws ThinklabException {

        ISubject subject = context.getSubject();
        IProvenance provenance = context.getProvenance();
        IModel model = context.getModel();
        IMonitor monitor = context.getMonitor();

        if (provenance.isEmpty()) {
            return true;
        }

        monitor.message(Notification.PROVENANCE_GRAPH, context.modelGraph.visualize().toString());
        Workflow workflow = new Workflow(subject, model, provenance, monitor, context.getCoverage(), ((Subject) subject)
                .getContextId());
        monitor.message(Notification.WORKFLOW_GRAPH, workflow.visualize().toString());

        ((ResolutionStrategy) (subject.getResolutionStrategy())).addStep(provenance, workflow);

        // initialization transition = null
        boolean ret = workflow.run(null);

        /*
         * if the action has generated subjects, resolve them in our context, then 
         * add them to the context.
         */
        for (ISubject s : workflow.getGeneratedSubjects()) {
            ICoverage su = resolve(s, context.forSubject(subject));
            if (su.isRelevant()) {
                ((IActiveSubject) subject).addSubject(s);
            }
        }

        context.reset();

        return ret;
    }

    private ICoverage resolve(IObserver observer, ResolutionContext context) throws ThinklabException {

        if (context.isUnsatisfiable)
            return Coverage.EMPTY;

        if (observer.getMediatedObserver() != null) {
            if (resolve(observer.getMediatedObserver(),
                    context.forMediatedObserver(observer.getMediatedObserver())).isEmpty()) {
                return Coverage.EMPTY;
            }
        } else if (observer instanceof IConditionalObserver) {

            int i = 0;
            ResolutionContext cd = context.forGroup(LogicalConnector.UNION);
            for (Pair<IModel, IExpression> observedModelPair : ((IConditionalObserver) observer).getModels()) {
                IModel model = observedModelPair.getFirst();
                IExpression expression = observedModelPair.getSecond();
                resolve(model, cd.forCondition(expression, i++));
            }
            cd.finish();

            /*
             * model dependencies are only relevant when we have conditional observers; we 
             * link them to the observer.
             */
            IResolutionContext dc = context.forGroup(LogicalConnector.INTERSECTION);
            for (IDependency d : context.model.getDependencies()) {
                if (resolve(d, (ResolutionContext) dc.forDependency(d)).isEmpty())
                    break;
            }
            dc.finish();

        } else if (!observer.hasAccessor() && !observer.isComputed()) {
            /*
             * not mediating, not computing and with no user-defined accessor: if we have been passed a
             * datasource to interpret do that, otherwise we must resolve our final observer externally.
             */
            if (context.datasource != null) {
                if (context.forDatasource(context.datasource).finish().isEmpty())
                    return Coverage.EMPTY;
            } else {
                IObservable observable = observer.getObservable();
                if (resolve(observable, context.forObservable(observable)).isEmpty())
                    return Coverage.EMPTY;
            }
        }

        if (observer.getDependencies().size() > 0) {
            IResolutionContext dc = context.forGroup(LogicalConnector.INTERSECTION);
            for (IDependency d : observer.getDependencies()) {
                if (resolve(d, (ResolutionContext) dc.forDependency(d)).isEmpty())
                    break;
            }
            dc.finish();
        }

        return context.finish();
    }

    private ICoverage resolve(ISubject subject, ResolutionContext context) throws ThinklabException {

        if (context.isUnsatisfiable)
            return Coverage.EMPTY;

        if (!((Subject) subject).isInitialized()) {

            ICoverage cov = resolve(subject.getObservable(), context.forObservable(subject.getObservable()));

            if (cov.isEmpty()) {
                context.getMonitor().error("cannot resolve subject " + subject.getId());
                return cov;
            }
        }

        return resolveDependencies(subject, context, true);
    }

    private ICoverage resolve(IDependency dependency, ResolutionContext context) throws ThinklabException {

        if (context.isUnsatisfiable)
            return Coverage.EMPTY;

        IObservable observable = dependency.getObservable();
        ICoverage cov = null;

        if (dependency.getContextModel() != null) {
            // TODO - resolve the context model, then set the result in the context for the
            // following resolution. The next step will necessarily be an object resolution.
        }

        if (observable.getModel() != null) {
            cov = resolve(observable.getModel(), context.forModel(observable.getModel()));
        } else {
            cov = resolve(observable, context.forObservable(observable));
        }

        if (cov.isEmpty() && !context.isOptional()) {
            context.getMonitor().error("mandatory dependency on " + observable.getType() + " is unresolved");
            context.coverage = Coverage.EMPTY;
        }

        return context.finish();
    }

    /*
     * this will be passed a context.forObservable(), which automatically only merges in models that cover
     * enough more context to be relevant, and merges them into one conditional model at finish().
     */
    private ICoverage resolve(IObservable observable, ResolutionContext context) throws ThinklabException {

        if (context.isUnsatisfiable)
            return Coverage.EMPTY;

        IObservationKbox kbox = Thinklab.get().requireKbox(Thinklab.DEFAULT_KBOX);
        ICoverage cov = Coverage.EMPTY;

        if (observable.getModel() != null) {
            cov = resolve(observable.getModel(), context.forModel(observable.getModel()));
        } else {

            IModel previous = context.getModelFor(observable);
            if (previous != null) {
                cov = resolve(previous, context.forModel(previous));
            } else {

                /*
                 * if we're resolving a subject, coverage is full if we don't find any model. Any other
                 * situation, including processes and events, must have a model.
                 */
                cov = NS.isThing(observable) ? Coverage.FULL(context.getScale()) : Coverage.EMPTY;

                /*
                 * lookup model for this observable. ObservationKbox will do all the
                 * ranking and sorting as long as the iterator is used. 
                 */
                for (IModel m : kbox.query(observable, context)) {

                    if (m == null)
                        continue;
                    if (context.isResolving(m))
                        continue;
                    cov = resolve(m, context.forModel(m));
                    if (cov.isComplete()) {
                        return context.finish();
                    }
                }
            }
        }

        return cov;
    }

}
