package org.integratedmodelling.thinklab.modelling.resolver;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.metadata.IMetadata;
import org.integratedmodelling.thinklab.api.modelling.IAccessor;
import org.integratedmodelling.thinklab.api.modelling.IAction;
import org.integratedmodelling.thinklab.api.modelling.IAnnotation;
import org.integratedmodelling.thinklab.api.modelling.IDataSource;
import org.integratedmodelling.thinklab.api.modelling.IModel;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.api.modelling.IObjectSource;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.IScale;
import org.integratedmodelling.thinklab.api.modelling.IState;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.modelling.lang.StateAccessor;
import org.integratedmodelling.thinklab.modelling.states.State;

/**
 * Wraps a previously computed IState into a IModel that can be used to resolve a dependency for something
 * that was computed earlier.
 * 
 * @author Ferd
 * 
 */
public class StateModel implements IModel {

    IObservable _observable;
    IState      _state;
    IDataSource _datasource;

    class StateDatasource implements IDataSource {

        class StateDSAccessor extends StateAccessor {

            int _idx = 0;

            public StateDSAccessor(IMonitor monitor) {
                super(new ArrayList<IAction>(), monitor);
            }

            @Override
            public void process(int stateIndex) throws ThinklabException {
                _idx = stateIndex;
            }

            @Override
            public Object getValue(String outputKey) {
                return _state.getValue(_idx);
            }
        }

        @Override
        public IMetadata getMetadata() {
            return _state.getMetadata();
        }

        @Override
        public IAccessor getAccessor(IScale context, IObserver contextObserver, IMonitor monitor)
                throws ThinklabException {
            return new StateDSAccessor(monitor);
        }

        @Override
        public IScale getCoverage() {
            return _state.getScale();
        }

        public String toString() {
            return "Previous observation of " + _state.getObservable().getType();
        }

    }

    public StateModel(IObservable o, IState s) {
        _observable = o;
        _state = s;
        _datasource = new StateDatasource();
    }

    @Override
    public List<IDependency> getDependencies() {
        return new ArrayList<IDependency>();
    }

    @Override
    public boolean hasActionsFor(IConcept observable, IConcept domainConcept) {
        return false;
    }

    @Override
    public IScale getCoverage() throws ThinklabException {
        return _state.getScale();
    }

    @Override
    public List<IAction> getActions() {
        return new ArrayList<IAction>();
    }

    @Override
    public boolean isComputed() {
        return false;
    }

    @Override
    public IObservable getObservable() {
        return _observable;
    }

    @Override
    public IScale getScale() {
        return _state.getScale();
    }

    @Override
    public String getId() {
        return ((State) _state).getInternalId();
    }

    @Override
    public boolean isPrivate() {
        return false;
    }

    @Override
    public boolean isInactive() {
        return false;
    }

    @Override
    public Collection<IAnnotation> getAnnotations() {
        return new ArrayList<IAnnotation>();
    }

    @Override
    public void setLineNumbers(int startLine, int endLine) {
    }

    @Override
    public int getFirstLineNumber() {
        return 0;
    }

    @Override
    public int getLastLineNumber() {
        return 0;
    }

    @Override
    public INamespace getNamespace() {
        return _state.getNamespace();
    }

    @Override
    public String getName() {
        return "previous observation";
    }

    @Override
    public IMetadata getMetadata() {
        return _state.getMetadata();
    }

    @Override
    public List<IObservable> getObservables() {
        return Collections.singletonList(_observable);
    }

    @Override
    public IDataSource getDatasource() {
        return _datasource;
    }

    @Override
    public IObjectSource getObjectSource() {
        return null;
    }

    @Override
    public IObserver getObserver() {
        return _state.getObserver();
    }

    @Override
    public boolean isResolved() {
        return true;
    }

    @Override
    public IAccessor getAccessor(IScale context, IMonitor monitor) throws ThinklabException {

        /*
         * the accessor is returned by the datasource
         */
        return null;
    }

    @Override
    public boolean hasErrors() {
        return false;
    }

    @Override
    public boolean isReificationModel() {
        return false;
    }

    @Override
    public IConcept getInherentSubjectType() {
        return _observable.getInherentType();
    }

    @Override
    public Collection<String> getObjectAttributes() {
        // wil never be called, but you never know
        return new ArrayList<String>();
    }

    @Override
    public IModel getAttributeObserver(String attribute) {
        // wil never be called
        return null;
    }

}
