package org.integratedmodelling.thinklab.modelling.resolver;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

import org.integratedmodelling.collections.NumericInterval;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.query.IQuery;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.api.modelling.resolution.IResolutionContext;
import org.integratedmodelling.thinklab.api.space.ISpatialExtent;
import org.integratedmodelling.thinklab.api.time.ITemporalExtent;
import org.integratedmodelling.thinklab.common.ConceptPair;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.debug.DataRecorder;
import org.integratedmodelling.thinklab.query.Queries;
import org.integratedmodelling.thinklab.query.Query;

/**
 * Query builder for complicated observation queries on ModelData objects.
 * 
 * @author ferdinando.villa
 *
 */
public class ObservationQueries {

    /**
     * Query helper for scope query: given a dominant namespace and an optional
     * list of scenarios, return a restriction for model data to select models in
     * that namespace, models in any of those scenarios, or public models not in scenarios.
     * 
     * @param md
     * @param base
     * @param value
     * @return
     */
    public static IQuery scopeQuery(String namespaceId, Collection<String> scenarios) {

        return Queries.or(
                Queries.restriction(NS.HAS_NAMESPACE_ID, Queries.is(namespaceId)),
                ((scenarios == null || scenarios.size() == 0) ? null : Queries.and(
                        Queries.restriction(NS.IS_IN_SCENARIO, Queries.is(true)),
                        Queries.restriction(NS.HAS_NAMESPACE_ID, Queries.in(scenarios.toArray())))), Queries
                        .and(Queries.no(Queries.restriction(NS.HAS_NAMESPACE_ID, Queries.is(namespaceId))),
                                Queries.restriction(NS.IS_PRIVATE, Queries.is(false)),
                                Queries.restriction(NS.IS_IN_SCENARIO, Queries.is(false))));
    }

    // /**
    // * Select those that have a particular value of a trait or don't have that trait at all.
    // * @param type
    // * @param value
    // * @return
    // */
    // public static IQuery traitQuery(IConcept type, IConcept value) {
    // return Queries.or(
    // Queries.no(Queries.has(NS.HAS_TRAIT)),
    // Queries.restriction(
    // NS.HAS_TRAIT,
    // Queries.select(NS.CONCEPT_PAIR).restrict(NS.HAS_FIRST_FIELD,
    // Queries.no(Queries.is(type)))),
    // Queries.restriction(NS.HAS_TRAIT,
    // Queries.select(NS.CONCEPT_PAIR).restrict(NS.HAS_FIRST_FIELD, Queries.is(type))
    // .restrict(NS.HAS_SECOND_FIELD, Queries.is(value))));
    // }

    /**
     * The root query that matches the passed observable in the passed context. Results need to
     * be ranked using a IModelPrioritizer.
     * 
     * @param observable
     * @param context
     * @return
     */
    public static IQuery contextQuery(IObservable observable, IResolutionContext context) {

        Query ret = Queries.select(NS.MODEL_DATA)
                .restrict(scopeQuery(context.getNamespace().getId(), context.getScenarios()))
                .restrict(observableQuery(context, observable));

        if (((ResolutionContext) context).scale.getSpace() != null) {
            ret = ret.restrict(spatialQuery(((ResolutionContext) context).scale.getSpace()));
        }
        if (((ResolutionContext) context).scale.getTime() != null) {
            ret = ret.restrict(temporalQuery(((ResolutionContext) context).scale.getTime()));
        }

        DataRecorder.debug(ret.toString());

        return ret;
    }

//    /**
//     * Match the traits requested by a context. If no traits are requested, matches must have
//     * no traits. Otherwise, models with no traits will also match, and
//     * will be ranked lower than those with one or more matching traits.
//     * 
//     * Not used in model search - the traits are matched by possible observable types, set in
//     * observableQuery.
//     * 
//     * @param context
//     * @return
//     */
//    public static IQuery traitQuery(IResolutionContext context) {
//
//        if (((ResolutionContext) context).traits == null || ((ResolutionContext) context).traits.size() == 0)
//            return Queries.no(Queries.has(NS.HAS_TRAIT));
//
//        /*
//         * TODO/FIXME: make any identity traits mandatory (those that inherit from NS.IDENTITY_TRAIT)
//         */
//
//        ArrayList<Query> qs = new ArrayList<Query>();
//        qs.add(Queries.no(Queries.has(NS.HAS_TRAIT)));
//
//        for (Pair<IConcept, IConcept> t : ((ResolutionContext) context).traits) {
//
//            if (NS.isIdentity(t.getFirst())) {
//
//                /*
//                 * identities are matched mandatorily
//                 */
//                qs.add((Query) Queries.restriction(
//                        NS.HAS_TRAIT,
//                        Queries.select(NS.CONCEPT_PAIR)
//                                .restrict(NS.HAS_FIRST_FIELD, Queries.is(t.getFirst()))
//                                .restrict(NS.HAS_SECOND_FIELD, Queries.is(t.getSecond()))));
//
//            } else {
//
//                qs.add(Queries.or(
//                        Queries.restriction(
//                                NS.HAS_TRAIT,
//                                Queries.select(NS.CONCEPT_PAIR).restrict(NS.HAS_FIRST_FIELD,
//                                        Queries.no(Queries.is(t.getFirst())))),
//                        Queries.restriction(
//                                NS.HAS_TRAIT,
//                                Queries.select(NS.CONCEPT_PAIR)
//                                        .restrict(NS.HAS_FIRST_FIELD, Queries.is(t.getFirst()))
//                                        .restrict(NS.HAS_SECOND_FIELD, Queries.is(t.getSecond())))));
//            }
//        }
//
//        return qs.size() == 1 ? qs.get(0) : Queries.or(qs.toArray(new Query[qs.size()]));
//    }

    /**
     * The query for a particular observable matches the observable concept exactly and the
     * observation and (if any) inherent type using the semantic closure. If an inherent type
     * is not present in the observable, the subject type in the context is used and the 
     * query will also contain those models that are not inherent to anything.
     * 
     * The observable query also includes attribute traits from the context.
     * 
     * @param context
     * @param observable
     * @return
     */
    public static IQuery observableQuery(IResolutionContext context, IObservable observable) {

        IConcept inherent = observable.getInherentType(); // TODO type of subject we're in
        IConcept trait = observable.getTraitType();

        if (inherent == null) {
            inherent = context.getSubject().getObservable().getType();
        }
        
        ArrayList<IQuery> conds = new ArrayList<IQuery>();
        
        IConcept main = observable.getType();
        try {
            Set<IConcept> compatibleObservables = NS.getCompatibleObservables(main, context.getTraits());

            if (compatibleObservables.size() == 1) {
                conds.add(Queries.restriction(NS.HAS_TYPE, Queries.isOnly(main)));
            } else {
                ArrayList<IQuery> ots = new ArrayList<IQuery>();
                for (IConcept oo : compatibleObservables) {
                    ots.add(Queries.restriction(NS.HAS_TYPE, Queries.isOnly(oo)));
                }
                conds.add(Queries.or(ots));
            }
        
        } catch (ThinklabValidationException e) {
            throw new ThinklabRuntimeException(e);
        }



        conds.add(Queries.restriction(NS.HAS_OBSERVATION_TYPE, Queries.is(observable.getObservationType())));

        if (inherent != null) {

            /*
             * inherency is compulsory if specified in the observable, otherwise we can also get
             * what is not inherent to anything.
             */
            conds.add(
                    observable.getInherentType() == null ?
                            Queries.or(Queries.no(Queries.has(NS.HAS_INHERENT_SUBJECT_TYPE)),
                                    Queries.restriction(NS.HAS_INHERENT_SUBJECT_TYPE, Queries
                                            .isObservable(inherent)))
                            :
                            Queries.restriction(NS.HAS_INHERENT_SUBJECT_TYPE, Queries.isObservable(inherent)));
        } else {
            conds.add(Queries.no(Queries.has(NS.HAS_INHERENT_SUBJECT_TYPE)));
        }

        if (trait != null) {
            /*
             * trait is compulsory for the observable; traits taken from the context are
             * evaluated at ranking.
             */
            conds.add(Queries.restriction(NS.HAS_TRAIT_TYPE, Queries.is(trait)));
        } else {
            conds.add(Queries.no(Queries.has(NS.HAS_TRAIT_TYPE)));
        }

        return Queries.restriction(
                NS.HAS_OBSERVABLE,
                Queries.selectOnly(NS.OBSERVABLE).restrict(
                        Queries.and(conds.toArray(new IQuery[conds.size()]))));
    }

    /**
     * Match a spatial extent by intersection. 
     * 
     * @param space
     * @return
     */
    public static IQuery spatialQuery(ISpatialExtent space) {

        return Queries.or(Queries.no(Queries.has(NS.SPATIAL_EXTENT_PROPERTY)),
                Queries.restriction(NS.SPATIAL_EXTENT_PROPERTY, Queries.intersects(space.getExtent())));
    }

    /**
     * Match a temporal extent by overlap. 
     * 
     * @param time
     * @return
     */
    public static IQuery temporalQuery(ITemporalExtent time) {
        NumericInterval ni = (NumericInterval) time.getExtent();
        return null;
    }

    /**
     * Inquiry on ModelData:
     * 
     * true if the trait is not present or present only in that incarnation; false only if trait
     * is there but not with that value.
     *
     * @param md
     * @param base
     * @param value
     * @return
     */
    public static boolean containsOnlyTrait(ModelData md, IConcept base, IConcept value) {
        if (md.traits != null) {
            for (ConceptPair cp : md.traits) {
                if (cp.getFirst().equals(base))
                    return cp.getSecond().equals(value);
            }
        }
        return true;
    }

    /**
     * Inquiry on ModelData: check if the base trait is there and has the passed value
     * 
     * @param md
     * @param base
     * @param value
     * @return
     */
    public static boolean containsTrait(ModelData md, IConcept base, IConcept value) {
        if (md.traits != null) {
            for (ConceptPair cp : md.traits) {
                if (cp.getFirst().equals(base) && cp.getSecond().equals(value))
                    return true;
            }
        }
        return false;
    }

    /**
     * Check if one of the observables is as indicated. 
     * @param md
     * @param type main type (not null)
     * @param otype observation type (may be null)
     * @param itype inherent type (may be null)
     * @return
     */
    public static boolean hasObservable(ModelData md, IConcept type, IConcept otype, IConcept itype) {

        for (org.integratedmodelling.thinklab.modelling.resolver.ModelData.Observable obs : md.observables) {

            if (!obs.mainType.is(type))
                continue;
            if (otype != null && !obs.obsType.is(otype))
                continue;
            if (itype != null && !obs.subjType.is(itype))
                continue;

            return true;
        }

        return false;
    }
}
