package org.integratedmodelling.thinklab.modelling.resolver.obsolete;

import java.util.Collection;
import java.util.List;

import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.knowledge.IExpression;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.modelling.IConditionalObserver;
import org.integratedmodelling.thinklab.api.modelling.ICoverage;
import org.integratedmodelling.thinklab.api.modelling.IDataSource;
import org.integratedmodelling.thinklab.api.modelling.IModel;
import org.integratedmodelling.thinklab.api.modelling.IObservingObject.IDependency;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.modelling.lang.Observer;
import org.integratedmodelling.thinklab.modelling.lang.ObservingObject.Dependency;
import org.integratedmodelling.thinklab.modelling.resolver.Coverage;
import org.integratedmodelling.thinklab.modelling.resolver.ProvenanceGraph;
import org.integratedmodelling.thinklab.modelling.resolver.ResolutionContext;
import org.integratedmodelling.thinklab.modelling.resolver.ProvenanceGraph.ProvenanceNode;

public class ObserverResolver extends AbstractResolver {

    private final Observer<?> observer;
    private final IDataSource modelDatasource;
    private final List<Dependency> modelDependencies;
    private final ProvenanceNode rootProvenanceNode;
    private final ResolutionContext resolutionContext;

    public ObserverResolver(SubjectObserver subjectObserver, Observer<?> observer,
            IDataSource modelDatasource, List<Dependency> modelDependencies,
            ProvenanceNode rootProvenanceNode, ResolutionContext resolutionContext, IMonitor monitor) {
        super(subjectObserver, monitor);
        this.observer = observer;
        this.modelDatasource = modelDatasource;
        this.modelDependencies = modelDependencies;
        this.rootProvenanceNode = rootProvenanceNode;
        this.resolutionContext = resolutionContext;
    }

    @Override
    public ICoverage resolve(Collection<String> scenarios) throws ThinklabException {

        ProvenanceNode node = subjectObserver.getNode(observer, resolutionContext);
        ICoverage coverage = null;
        ICoverage dcov;
        AbstractResolver r;

        if (observer.getMediatedObserver() != null) {

            r = new ObserverResolver(subjectObserver, (Observer<?>) observer.getMediatedObserver(),
                    modelDatasource, modelDependencies, node, resolutionContext.mediate(subjectObserver
                            .getObservable(observer.getMediatedObserver())), monitor);
            coverage = r.resolve(scenarios);

            if (coverage == null || coverage.getCoverage() < subjectObserver.MIN_MODEL_COVERAGE) {
                return coverage;
            }

        } else if (observer instanceof IConditionalObserver) {

            List<Pair<IModel, IExpression>> observedModels = ((IConditionalObserver) observer).getModels();

            IModel observedModel;
            IExpression observedModelExpression;
            Observer<?> observedModelObserver;
            IDataSource conditionalObserverDatasource;
            List<Dependency> conditionalObserverDataDependencies;
            IObservable observedObservable;
            ResolutionContext conditionalDependency;

            int i = 0;
            for (Pair<IModel, IExpression> observedModelPair : observedModels) {
                observedModel = observedModelPair.getFirst();
                observedModelExpression = observedModelPair.getSecond();

                observedModelObserver = (Observer<?>) observedModel.getObserver();
                conditionalObserverDatasource = observedModel.getDatasource();
                conditionalObserverDataDependencies = subjectObserver.getDataDependencies(observedModel);
                observedObservable = subjectObserver.getObservable(observedModel);

                conditionalDependency = resolutionContext.conditionalDependency(observedModel,
                        observedObservable, observedModelExpression, i++);

                r = new ObserverResolver(subjectObserver, observedModelObserver,
                        conditionalObserverDatasource, conditionalObserverDataDependencies, node,
                        conditionalDependency, monitor);
                dcov = r.resolve(scenarios);

                /*
                 * these are in OR, so the total coverage is the largest coverage; throw away the full
                 * coverage we started with and accumulate coverages.
                 */
                coverage = coverage == null ? dcov : coverage.or(dcov, subjectObserver.MIN_MODEL_COVERAGE);
            }

            /*
             * these are in AND and only happen when the observer is conditional anyway.
             * 
             * We compute the coverage for the mandatory dependencies separately so we can proceed anyway if
             * the mandatory are covered.
             */
            for (IDependency d : modelDependencies) {
                r = new DependencyResolver(subjectObserver, d, node, resolutionContext.dependency(d), monitor);
                dcov = r.resolve(scenarios);

                if (dcov == null || dcov.getCoverage() < subjectObserver.MIN_MODEL_COVERAGE) {
                    String err = "dependency " + d.getFormalName() + " in " + observer.getName()
                            + " cannot be resolved in this context";

                    if (d.isOptional()) {
                        subjectObserver._monitor.warn("optional " + err);
                    } else if (!resolutionContext.optional) {
                        subjectObserver._monitor.error(addError("mandatory " + err));
                    }
                }

                coverage = coverage == null ? dcov : coverage.and(dcov);
            }

            if (coverage == null || coverage.getCoverage() < subjectObserver.MIN_MODEL_COVERAGE) {
                return coverage;
            }

        } else if (!observer.hasAccessor() && !observer.isComputed()) {
            /*
             * not mediating, not computing and with no user-defined accessor: if we have been passed a
             * datasource to interpret do that, otherwise we must resolve our final observer in the kbox.
             */
            IObservable observable = subjectObserver.getObservable(observer);
            if (modelDatasource != null) {
                ResolutionContext newContext;
                if (isObserverTrivial(observer)) {
                    newContext = resolutionContext.interpretAs(observable);
                } else {
                    newContext = resolutionContext.mediate(observable);
                }
                r = new DatasourceResolver(subjectObserver, modelDatasource, node, observer.getModel(),
                        newContext, monitor);
                coverage = r.resolve(scenarios);
            } else {
                /*
                 * resolve the observable we're being linked with, which is the observer's final observable,
                 * and mediate it. Workflow will compile as interpret_as if the mediation is trivial.
                 */
                ResolutionContext subContext = resolutionContext.observe(observer);
                r = new ConceptResolver(subjectObserver, observable, node, subContext, monitor);
                coverage = r.resolve(scenarios);
            }

            if (coverage == null || coverage.getCoverage() < subjectObserver.MIN_MODEL_COVERAGE) {
                return coverage;
            }
        }

        /*
         * We compute the coverage for the mandatory dependencies; optional dependencies are handled
         * separately for reporting and inclusion but do not contribute to the return value.
         */
        Coverage mcoverage = null;
        for (IDependency d : observer.getDependencies()) {
            r = new DependencyResolver(subjectObserver, d, node, resolutionContext.dependency(d), monitor);
            dcov = r.resolve(scenarios);

            if (subjectObserver._monitor != null
                    && (dcov == null || dcov.getCoverage() < subjectObserver.MIN_MODEL_COVERAGE)) {
                String err = "dependency " + d.getFormalName() + " in " + observer.getName()
                        + " cannot be resolved in this context";
                if (d.isOptional()) {
                    subjectObserver._monitor.warn("optional " + err);
                } else if (!resolutionContext.optional) {
                    subjectObserver._monitor.error(addError("mandatory " + err));
                }
            }

            if (!d.isOptional()) {
                mcoverage = (Coverage) (mcoverage == null ? dcov : (coverage == null ? null : coverage
                        .and(dcov)));
            }
        }

        if (mcoverage != null) {
            coverage = coverage == null ? mcoverage : coverage.and(mcoverage);
        } else if (coverage == null) {
            /*
             * both are null, meaning no deps or all optional -> model is in.
             */
            coverage = new Coverage(resolutionContext.scale, 1.0);
        }

        if (coverage.getCoverage() >= subjectObserver.MIN_MODEL_COVERAGE) {
            resolutionContext.modelGraph.addEdge(node, rootProvenanceNode, resolutionContext.link);
        }

        node.setCoverage(coverage);

        return coverage;
    }

}
