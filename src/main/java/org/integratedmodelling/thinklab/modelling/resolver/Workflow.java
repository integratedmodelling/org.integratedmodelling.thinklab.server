package org.integratedmodelling.thinklab.modelling.resolver;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.HashableObject;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.configuration.IConfiguration;
import org.integratedmodelling.thinklab.api.knowledge.IExpression;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.modelling.IAccessor;
import org.integratedmodelling.thinklab.api.modelling.IActiveSubject;
import org.integratedmodelling.thinklab.api.modelling.IConditionalObserver;
import org.integratedmodelling.thinklab.api.modelling.ICoverage;
import org.integratedmodelling.thinklab.api.modelling.IDataset;
import org.integratedmodelling.thinklab.api.modelling.IModel;
import org.integratedmodelling.thinklab.api.modelling.IObjectSource;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.IScale;
import org.integratedmodelling.thinklab.api.modelling.IState;
import org.integratedmodelling.thinklab.api.modelling.IStateAccessor;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.api.modelling.resolution.IProvenance;
import org.integratedmodelling.thinklab.api.modelling.resolution.IWorkflow;
import org.integratedmodelling.thinklab.api.modelling.scheduling.ITransition;
import org.integratedmodelling.thinklab.common.data.Edge;
import org.integratedmodelling.thinklab.common.visualization.GraphVisualization;
import org.integratedmodelling.thinklab.common.visualization.GraphVisualization.GraphAdapter;
import org.integratedmodelling.thinklab.debug.CallTracer;
import org.integratedmodelling.thinklab.modelling.interfaces.ISwitchingAccessor;
import org.integratedmodelling.thinklab.modelling.lang.Accessor;
import org.integratedmodelling.thinklab.modelling.lang.Classification;
import org.integratedmodelling.thinklab.modelling.lang.Observer;
import org.integratedmodelling.thinklab.modelling.lang.Scale;
import org.integratedmodelling.thinklab.modelling.lang.StateAccessor;
import org.integratedmodelling.thinklab.modelling.lang.Subject;
import org.integratedmodelling.thinklab.modelling.resolver.ProvenanceGraph.DependencyEdge;
import org.integratedmodelling.thinklab.modelling.resolver.ProvenanceGraph.ProvenanceNode;
import org.integratedmodelling.thinklab.modelling.resolver.Workflow.DataPath;
import org.integratedmodelling.thinklab.modelling.resolver.Workflow.ProcessingStep;
import org.integratedmodelling.thinklab.modelling.resolver.Workflow.ProcessingStep.Cond;
import org.integratedmodelling.thinklab.modelling.resolver.Workflow.ProcessingStep.OutputAction;
import org.integratedmodelling.utils.graph.GraphViz;
import org.integratedmodelling.utils.graph.GraphViz.NodePropertiesProvider;
import org.jgrapht.alg.CycleDetector;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.traverse.TopologicalOrderIterator;

/**
 * An acyclic dataflow built from the ProvenanceGraph of a resolved model.
 * 
 * Iterates the processing steps in order of dependency.
 * 
 * @author Ferd
 */
public class Workflow extends DefaultDirectedGraph<ProcessingStep, DataPath> 
    implements Iterable<ProcessingStep>, IWorkflow {

    private static final long                      serialVersionUID        = 2348908259284218130L;

    public static final int                        INITIALIZATION_SCHEDULE = 0;
    public static final int                        TIME_SCHEDULE           = 1;

    private int                                    lastRegister            = 0;

    ISubject                                       _subject;
    IModel                                         _rootModel;
    ProvenanceGraph                                _modelGraph;
    IScale                                         _scale;
    IMonitor                                       _monitor;
    ArrayList<ProcessingStep>                      _entryPoints            = new ArrayList<ProcessingStep>();
    long                                           _contextId;

    // the overall coverage of the observation we're performing. Used to pass to the states.
    final ICoverage                                _coverage;

    HashMap<ProvenanceNode, ProcessingStep>        steps                   = new HashMap<ProvenanceNode, ProcessingStep>();

    // workflows with object sources are part of object or process dependency chains only, so they should have
    // JUST that. These are collected here and interrogated in one operation at initialization.
    private ArrayList<Pair<IModel, IObjectSource>> _objectCreationSteps    = new ArrayList<Pair<IModel, IObjectSource>>();

    private final Collection<IState>               _states                 = new ArrayList<IState>();
    private Object[]                               _registers              = null;
    private List<ISubject>                         _subjects               = new ArrayList<ISubject>();

    /*
     * flags that decide which states to generate. Default is only directly resolved nodes and
     * those that have dependencies. Not used at the moment (I doubt anyone will take the trouble
     * of customizing this). Complete checkStorage() to support fully.
     */
    boolean                                        storeRaw                = Boolean
                                                                                   .parseBoolean(Thinklab
                                                                                           .get()
                                                                                           .getProperties()
                                                                                           .getProperty(IConfiguration.STORE_RAW_DATA_PROPERTY, "false"));
    boolean                                        storeIntermediate       = Boolean
                                                                                   .parseBoolean(Thinklab
                                                                                           .get()
                                                                                           .getProperties()
                                                                                           .getProperty(IConfiguration.STORE_INTERMEDIATE_DATA_PROPERTY, "true"));
    boolean                                        storeConditional        = Boolean
                                                                                   .parseBoolean(Thinklab
                                                                                           .get()
                                                                                           .getProperties()
                                                                                           .getProperty(IConfiguration.STORE_CONDITIONAL_DATA_PROPERTY, "false"));
    boolean                                        storeMediated           = Boolean
                                                                                   .parseBoolean(Thinklab
                                                                                           .get()
                                                                                           .getProperties()
                                                                                           .getProperty(IConfiguration.STORE_MEDIATED_DATA_PROPERTY, "false"));

    public Workflow(ISubject subject, IModel rootModel, IProvenance modelGraph, IMonitor monitor,
            ICoverage coverage, long contextId) throws ThinklabException {
        super(DataPath.class);
        _rootModel = rootModel;
        _modelGraph = (ProvenanceGraph) modelGraph;
        _subject = subject;
        _scale = subject.getScale();
        _monitor = monitor;
        _coverage = coverage;
        _contextId = contextId;
        compile();
        optimize();
    }

    private void optimize() {

        /*
         * TODO more checks. For now just removes precomputed nodes if they're not involved in anything.
         */
        ArrayList<ProcessingStep> trivials = new ArrayList<Workflow.ProcessingStep>();
        for (ProcessingStep s : this) {
            if (s.model instanceof StateModel && this.outgoingEdgesOf(s).size() == 0
                    && this.incomingEdgesOf(s).size() == 0) {
                trivials.add(s);
            }
        }

        for (ProcessingStep s : trivials) {
            this.removeVertex(s);
        }
    }

    @Override
    public boolean addEdge(ProcessingStep stepFrom, ProcessingStep stepTo, DataPath edge) {
        // CallTracer.indent("addEdge()", this, stepFrom, stepTo, edge);
        /*
         * every edge except dependency has no formal name set in, and the accessors may not either.
         */
        if (stepFrom.accessor.getName() == null && stepFrom.observer != null) {
            ((Accessor) (stepFrom.accessor)).setName(stepFrom.observer.getId());
        }
        if (stepTo.accessor.getName() == null) {
            ((Accessor) (stepTo.accessor)).setName(stepTo.observer.getId());
        }
        if (edge.formalName == null || edge.formalName.isEmpty()) {
            edge.formalName = stepTo.accessor.getName();
        }

        // CallTracer.unIndent();
        return super.addEdge(stepFrom, stepTo, edge);
    }

    private void compile() throws ThinklabException {
        /*
         * should return null when called at this level.
         */
        compileInternal(_modelGraph.get(_rootModel), null, _scale, _modelGraph);

        // /*
        // * since now, we may be compiling a partial workflow just for one observation, so the
        // * workflow may not contain the whole provenance up to the subject. Also, the model
        // * for the subject is now optional. So this may happen.
        // */
        // if (_entryPoints.isEmpty()) {
        //
        for (ProcessingStep s : this) {
            Set<DataPath> oe = outgoingEdgesOf(s);
            if (oe.size() == 0) {
                _entryPoints.add(s);
            }
        }
    }

    private ProcessingStep newStep(IAccessor accessor, IObserver observer, ProvenanceNode node, ProvenanceGraph graph) {

        ProcessingStep ret = new ProcessingStep(accessor, observer);
        addVertex(ret);
        ret.model = node.model;
        ret.observer = observer;
        steps.put(node, ret);
        ret.stored = checkStorage(node, graph);
        return ret;
    }

    /**
     * We only get datasource and observer nodes - those that can produce a value. The
     * datasource ones we store iif we want raw data. The observer ones only if they
     * are the target of a resolution from a model and they define the state of
     * another.
     * 
     * @param node
     * @param graph
     * @return
     */
    private boolean checkStorage(ProvenanceNode node, ProvenanceGraph graph) {

        /*
         * optional = one of the optional sources for a conditional accessor
         */
        boolean isRaw = false;
        boolean hasDirectDependency = false;
        boolean definesState = false;
        boolean wasResolved = false;
        boolean ret = true;
        boolean interpretAs = false;

        for (DependencyEdge d : graph.outgoingEdgesOf(node)) {
            if (d.type == DependencyEdge.INTERPRET_AS) {
                isRaw = true;
            }
            if (d.type == DependencyEdge.DEFINE_STATE) {
                // sends input to model
                definesState = true;
            }
        }

        for (DependencyEdge d : graph.incomingEdgesOf(node)) {
            if (d.type == DependencyEdge.DEPENDENCY && d.conditionIndex < 0) {
                hasDirectDependency = true;
            }
            if (d.type == DependencyEdge.RESOLVES) {
                // was resolved, i.e. doesn't just interpret a datasource.
                wasResolved = true;
            }
            if (d.type == DependencyEdge.INTERPRET_AS) {
                // interprets a datasource.
                interpretAs = true;
            }
        }

        ret = (definesState /* && (wasResolved || interpretAs)*/) /* || hasDirectDependency */
                || (storeRaw && isRaw);

        /*
         * whatever happened, we don't want to store previous states again.
         */
        if (node.model instanceof StateModel) {
            ret = false;
        }

        return ret;

    }

    private ProcessingStep compileInternal(ProvenanceNode node, IObserver contextObserver, IScale context,
            ProvenanceGraph provenanceGraph) throws ThinklabException {

        CallTracer.indent("compileInternal()", this, node, contextObserver, context);
        ProcessingStep result = null;

        // FIXME patch - may happen if one dep is a process or subject.
        if (node == null)
            return null;
        
        if (node.objectsource != null) {
            _objectCreationSteps.add(new Pair<IModel, IObjectSource>(node.model, node.objectsource));
        } else if (steps.containsKey(node)) {
            CallTracer.msg("node has already been processed. Returning previous ProcessingStep...");
            result = steps.get(node);
        } else if (node.datasource != null) {
            /*
             * we add the observer and model later, from the observer accessor upstream.
             */
            CallTracer
                    .msg("node has already designated a datasource. Generating step from datasource.getAccessor()...");
            // result = newStep(node.datasource.getAccessor(context, contextObserver, _monitor), null, node);
            result = newStep(node.datasource.getAccessor(context, node.observer, _monitor), node.observer,
                    node, provenanceGraph);
        } else if (provenanceGraph.incomingEdgesOf(node).size() > 0) {

            CallTracer.msg("node does not exist and has no datasource defined but has inputs...");

            IObserver observer = node.getObserver();
            IModel model = node.model;

            for (ProvenanceGraph.DependencyEdge edge : provenanceGraph.incomingEdgesOf(node)) {

                IAccessor accessor = null;
                boolean childIsEntryPoint = false;
                ProcessingStep child = compileInternal(edge.getSourceNode(), node.observer, context,
                        provenanceGraph);

                if (child == null) {
                    /*
                     * should only happen when we don't have a datasource at the end of the line, which is not a usable workflow, 
                     * with unresolved nodes that should not get here at all. For now
                     * leave it because it does happen - although I need to understand why.
                     */
                    // throw new ThinklabInternalErrorException("dependency " + edge.formalName
                    // + " unresolved");
                    continue;
                }

                CallTracer.msg("switching on edge.type: " + edge.type);

                switch (edge.type) {
                case DependencyEdge.DEPENDENCY:
                case DependencyEdge.CONDITIONAL_DEPENDENCY:

                    childIsEntryPoint = node.model != null && node.model.getObserver() == null;

                    if (childIsEntryPoint) {
                        // CallTracer.msg("node.model has no observer. adding entry point for child: " +
                        // child);
                        // _entryPoints.add(child);
                    } else {

                        if (result == null) {
                            result = newStep(node.getAccessor(context, _monitor), observer, node, provenanceGraph);
                        }
                        CallTracer.msg("adding dependency edge for child: " + child);
                        addEdge(child, result, new DataPath(edge));
                    }
                    break;

                case DependencyEdge.MEDIATE_TO:
                case DependencyEdge.RESOLVES:

                    /*
                     * we must have an observer to get here. Get the observer from the child and call
                     * getMediator(); if the result is null (no mediation needed) we fall through to using the
                     * child.
                     */
                    if (child.observer != null)
                        accessor = node.observer.getMediator(child.observer, _monitor);

                    if (accessor != null) {

                        /*
                         * If we do mediate, the mediated gets the same name as our own accessor.
                         */
                        CallTracer.msg("got mediated dependency using accessor: " + accessor);
                        result = newStep(accessor, observer, node, provenanceGraph);
                        addEdge(child, result, new DataPath(edge));
                        break;
                    }
                    // else fall through

                case DependencyEdge.INTERPRET_AS:

                    if (!observer.canInterpretDirectly(child.accessor)) {
                        /*
                         * we are reinterpreting a datasource value through actions. Just put in a bare
                         * accessor with nothing but the actions.
                         */
                        CallTracer.msg("cannot interpret child directly. Creating interpreter...");
                        result = newStep(observer.getInterpreter((IStateAccessor) child.accessor, _monitor),
                                observer, node, provenanceGraph);
                        addEdge(child, result, new DataPath(edge));
                        break;
                    }
                    // else fall through

                case DependencyEdge.DEFINE_STATE:
                    CallTracer.msg("using normal pass-through dependency...");
                    result = child;
                    break;
                }

                if (child.observer == null)
                    child.observer = observer;
                if (child.model == null)
                    child.model = model;

            }

            /*
             * only store states for non-branch accessors TODO shouldn't store state if implied in conditional
             */
            if (result != null && node.model != null && node.getObserver() != null) {
                notifyObservables(node, result);
            }

        } else if (node.observer != null && ((Observer<?>) (node.observer)).hasUserDefinedAccessor()) {
            /*
             * we add the observer and model later, from the observer accessor upstream.
             */
            CallTracer.msg("node has no dependencies or datasource but produces data through an accessor.");
            result = newStep(((Observer<?>) (node.observer)).getUserDefinedAccessor(_monitor), node.observer,
                    node, provenanceGraph);
        }

        if (result != null && result.accessor != null && result.accessor.getName() == null
                && result.observer != null) {
            // happens when there is only one node. FIXME should be written cleanly, one day.
            ((Accessor) (result.accessor)).setName(result.observer.getId());
        }

        CallTracer.msg("result: " + result);
        CallTracer.unIndent();
        return result;
    }

    private void notifyObservables(ProvenanceNode node, ProcessingStep step) {

        ArrayList<IObservable> observables = new ArrayList<IObservable>();
        ArrayList<String> observableNames = new ArrayList<String>();
        ArrayList<IObserver> observers = new ArrayList<IObserver>();

        /*
         * use the top model to get at the intended observables.
         */
        IModel tmodel = node.model; // ((Model)(node.model)).getTopLevelModel();

        for (int i = 0; i < tmodel.getObservables().size(); i++) {

            IObservable observable = tmodel.getObservables().get(i);
            String name = i == 0 ? step.accessor.getName() : observable.getFormalName();
            IObserver observer = observable.getObserver();

            observables.add(observable);
            observableNames.add(name);
            observers.add(observer);
        }

        if (observables.size() > 0) {
            step.observables = observables;
            step.observableNames = observableNames;
            step.observers = observers;
        }
    }

    public class DataPath extends Edge {

        /*
         * each data path is for ONE observable and between two accessors. The same accessor may appear as the
         * target of more than one path. Each accessor has a name which it is known to itself with, and is the
         * ID of the model it comes from or it represents (in the case of datasource accessors). Each path has
         * a formal name that the other accessor is known to it with. Paths act as name translators when
         * connections are made. When an accessor reinterprets a datasource (which only happens with actions
         * or mediations) the path has the same formal name as the target accessor's. Otherwise the dependency
         * name is used as the formal name.
         */
        public DataPath(DependencyEdge edge) {

            isMediation = edge.type == DependencyEdge.MEDIATE_TO;

            if ((isConditional = conditionIndex >= 0)) {
                conditionIndex = edge.conditionIndex;
            }

            formalName = edge.formalName;
            observable = edge.observable;
            register = lastRegister++;
        }

        private static final long serialVersionUID = 2366743581134478147L;

        public boolean            isMediation      = false;
        public String             formalName       = null;
        public IObservable        observable       = null;
        public boolean            isConditional;
        public int                conditionIndex   = -1;

        @Override
        public String toString() {
            return getSource() + " -- " + (isMediation ? "m/" : "")
                    + (isConditional ? ("c/" + conditionIndex) + "/" : "") + formalName + " --> "
                    + getTarget();
        }

        public IStateAccessor getSourceAccessor() {
            return (IStateAccessor) ((ProcessingStep) (this.getSource())).accessor;
        }

        public IStateAccessor getTargetAccessor() {
            return (IStateAccessor) ((ProcessingStep) (this.getTarget())).accessor;
        }

        public ProcessingStep getSourceStep() {
            return (ProcessingStep) (this.getSource());
        }

        public ProcessingStep getTargetStep() {
            return (ProcessingStep) (this.getTarget());
        }

        /*
         * each dependency gets its own register automatically
         */
        public int register = -1;

        @Override
        public boolean equals(Object edge) {
            return edge instanceof DataPath && this.getSource().equals(((DataPath) edge).getSource())
                    && this.getTarget().equals(((DataPath) edge).getTarget())
                    && isMediation == ((DataPath) edge).isMediation;
        }
    }

    /*
     * compilation element - the workflow is made of these. It holds the accessor (the processing step) and
     * the observer that provides its observation semantics. Observable is only set in it when we want to
     * create a state.
     */
    public static class ProcessingStep extends HashableObject  {

        public IModel model;

        public ProcessingStep(IAccessor accessor, IObserver observer) {
            this.accessor = accessor;
            this.observer = observer;
        }

        public IObserver observer;
        public IAccessor accessor;

        @Override
        public String toString() {
            return "<" + accessor.getName() + ">";
        }

        public boolean isConditional;

        /*
         * all actions to be done as output after accessor was called - set to register if >= 0, save to state
         * with given name if state != null.
         */
        static class OutputAction {
            String formalName;
            int    register = -1;
        }

        ArrayList<OutputAction> outputs = new ArrayList<OutputAction>();

        /*
         * if conditional, this structure tells us how to process the switch instead of calling process().
         */
        static class Cond {
            int            index;
            IExpression    expression;
            ProcessingStep target;
            String         formalName;
        }

        List<Cond>                conditionals = null;

        /*
         * TODO this would be lots simpler and faster if we used concepts, but that creates endless
         * repercussions at the moment.
         */
        public List<IObservable>  observables;
        public List<String>       observableNames;
        public List<IObserver>    observers;

        public boolean            stored;
        public Collection<IState> states;
    }

    @Override
    public Iterator<ProcessingStep> iterator() {

        CycleDetector<ProcessingStep, DataPath> cycleDetector = new CycleDetector<ProcessingStep, DataPath>(
                this);

        if (cycleDetector.detectCycles()) {

            Iterator<ProcessingStep> iterator;
            Set<ProcessingStep> cycleVertices;
            Set<ProcessingStep> subCycle;
            ProcessingStep cycle;

            // TODO leave but report an internal error and
            // check what is happening. The point here is that we should
            // distinguish cycles that are semantic contradiction from those
            // that can be seen as representational artifacts once temporal dynamics is
            // factored in.
            Thinklab.get().logger().warn("Cycles detected in workflow.");

            // Get all vertices involved in cycles.
            cycleVertices = cycleDetector.findCycles();

            // Loop through vertices trying to find disjoint cycles.
            while (!cycleVertices.isEmpty()) {

                // Get a vertex involved in a cycle.
                iterator = cycleVertices.iterator();
                cycle = iterator.next();

                // Get all vertices involved with this vertex.
                subCycle = cycleDetector.findCyclesContainingVertex(cycle);
                for (ProcessingStep sub : subCycle) {
                    // Remove vertex so that this cycle is not encountered
                    // again.
                    // cycleVertices.remove(sub);
                }
            }
        }

        return new TopologicalOrderIterator<Workflow.ProcessingStep, Workflow.DataPath>(this);
    }

    public Collection<IState> getStates() throws ThinklabException {
        return _states;
    }

    public String dump() {

        GraphViz ziz = new GraphViz();
        ziz.loadGraph(this, new NodePropertiesProvider<ProcessingStep, DataPath>() {

            @Override
            public int getNodeWidth(ProcessingStep o) {
                return 40;
            }

            @Override
            public String getNodeId(ProcessingStep ps) {
                return ps.accessor + " (" + ps.hashCode() + ")";
            }

            @Override
            public int getNodeHeight(ProcessingStep o) {
                return 20;
            }

            @Override
            public String getNodeShape(ProcessingStep o) {
                return o.accessor instanceof ISwitchingAccessor ? DIAMOND
                        : (o.accessor.getActions().size() > 0 ? COMPONENT : BOX);
            }

            @Override
            public String getEdgeColor(DataPath e) {
                DataPath de = e;
                // TODO put a switch here will you
                return
                // de.isInitialization ? "red" :
                (de.isMediation ? "blue" : (de.isConditional ? "yellow" : "black"));
            }

            @Override
            public String getEdgeLabel(DataPath e) {
                return (e.observable == null ? "" : e.observable.getLocalName())
                        + (e.isConditional ? (" #" + e.conditionIndex) : "");
            }

        }, false);

        return ziz.getDotSource();
    }

    /**
     * Run the workflow once per context state, before time exists, using pull strategy. This produce initial
     * states for all states. Schedules are computed after this has run from 0 to context multiplicity, driven
     * by observe() in ResolvedSubject.
     * 
     * If anything bad happens, use logging in the session/subject to communicate it and return false instead
     * of throwing exceptions.
     * 
     */
    public boolean initialize(int contextIndex) {

        if (this.vertexSet().isEmpty())
            return true;

        HashSet<ProcessingStep> computed = new HashSet<ProcessingStep>();
        for (ProcessingStep entry : _entryPoints) {
            if (!compute(entry, contextIndex, computed))
                return false;
        }
        return true;
    }

    /**
     * Run the workflow for the passed transition, already determined by the scheduler to be relevant for it.
     * 
     * If anything bad happens, use logging in the session/subject to communicate it and return false instead
     * of throwing exceptions.
     */
    @Override
    public boolean run(ITransition transition) {

        if (this.vertexSet().isEmpty())
            return true;

        if (transition == null) {

            IScale scale = ((Scale) _scale).getNonTemporallyDynamicScale();

            /*
             * at the beginning is fine because we process dependencies in groups, so it's either
             * this part or the next.
             */
            for (Pair<IModel, IObjectSource> os : _objectCreationSteps) {
                try {
                    for (ISubject subj : SubjectFactory.createSubjects(os.getFirst(), os.getSecond(), _scale)) {
                        // create them; let the resolver resolve them and add them to the root subject.
                        _subjects.add(subj);
                    }
                } catch (ThinklabException e) {
                    _monitor.error(e);
                }
            }

            // initialize for all context states. The simple loop is fine here.
            if (preprocess()) {

                _monitor.info("initializing " + scale.getMultiplicity() + " states", null);

                // main initialization loop.
                // TODO parallelize. With the current time/space, we should be able to just use
                // Parallel.for, but the issue is whether the external accessors are reentrant
                // or not.
                for (int i = 0; i < scale.getMultiplicity(); i++) {
                    if (_monitor.isStopped()) {
                        _monitor.warn("initialization interrupted by user");
                        return false;
                    }
                    initialize(i);
                }

                /*
                 * set the states in the subject and it's all done.
                 */
                try {
                    for (IState s : getStates()) {
                        ((IActiveSubject) _subject).addState(s);
                    }
                } catch (ThinklabException e) {
                    _monitor.error(e);
                    return false;
                }
            }
        } else {

            HashSet<ProcessingStep> computed = new HashSet<ProcessingStep>();
            for (ProcessingStep entry : _entryPoints) {
                if (!compute(entry, transition, computed))
                    return false;
            }
        }

        return true;
    }

    /*
     * called once before initialize() and possibly run() are called. Call every notifier in accessors and
     * communicate all relevant contextual information. If anything bad happens, use logging in the
     * session/subject to communicate it and return false instead of throwing exceptions.
     */
    public boolean preprocess() {

        if (this.vertexSet().isEmpty())
            return true;

        HashSet<ProcessingStep> computed = new HashSet<ProcessingStep>();
        HashSet<IAccessor> visualized = new HashSet<IAccessor>();
        for (ProcessingStep entry : _entryPoints) {
            if (!notify(entry, computed, visualized))
                return false;
        }

        _registers = new Object[lastRegister];

        /*
         * create states for anything we want stored.
         */
        for (ProcessingStep step : vertexSet()) {
            defineStates(step);
        }

        if (_monitor != null) {
            // no need to continue
            if (_monitor.hasErrors()) {
                return false;
            }
            _monitor.debug("preprocessing finished");
        }

        return true;
    }

    private boolean notify(ProcessingStep step, HashSet<ProcessingStep> computed,
            HashSet<IAccessor> visualized) {

        if (computed.contains(step))
            return true;
        computed.add(step);

        IStateAccessor acc = (IStateAccessor) step.accessor;
        step.isConditional = acc instanceof ISwitchingAccessor;

        try {
            acc.notifyObserver(step.observer.getObservable(), step.observer);

            IModel tmodel = ((Observer<?>) (step.observer)).getTopLevelModel();
            if (tmodel.getObservables().size() > 1) {
                for (int i = 1; i < tmodel.getObservables().size(); i++) {
                    IObservable oob = tmodel.getObservables().get(i);
                    OutputAction out = new OutputAction();
                    out.formalName = oob.getFormalName();
                    acc.notifyExpectedOutput(oob, oob.getObserver(), oob.getFormalName());
                    step.outputs.add(out);
                }
            }

        } catch (Exception e) {
            return onException(e);
        }

        /*
         * notify all inputs
         */
        for (DataPath d : this.incomingEdgesOf(step)) {

            ProcessingStep src = this.getEdgeSource(d);

            notify(src, computed, visualized);

            try {
                if (d.isConditional) {

                    ((ISwitchingAccessor) acc).notifyConditionalAccessor(src.accessor, d.conditionIndex);

                    Cond cond = new Cond();
                    cond.target = src;
                    cond.index = d.conditionIndex;
                    cond.formalName = src.accessor.getName();

                    if (step.conditionals == null) {
                        step.conditionals = new ArrayList<Cond>();
                    }
                    step.conditionals.add(cond);

                } else {
                    acc.notifyDependency(d.observable, getRepresentativeObserver(src.observer),
                            (d.formalName == null ? src.accessor.getName() : d.formalName), d.isMediation);
                }
            } catch (ThinklabException e) {
                return onException(e);
            }
        }

        /*
         * record all outputs needed by other steps
         */
        for (DataPath d : this.outgoingEdgesOf(step)) {

            OutputAction out = new OutputAction();
            out.register = d.register;
            out.formalName = step.accessor.getName();
            step.outputs.add(out);
        }

        /*
         * extract and store any explicit observables that we haven't already used as part of the model
         * dataflow. This can only happen if the accessor is a computing one.
         * 
         * FIXME/CHECK - is this ever called? See same logics above.
         */
        // if (step.observables != null) {
        // for (int i = 0; i < step.observables.size(); i++) {
        //
        // IObservable o = step.observables.get(i);
        //
        // OutputAction out = new OutputAction();
        // out.formalName = step.observableNames.get(i);
        // IObserver observer = step.observers.get(i);
        //
        // try {
        // ((IStateAccessor) (step.accessor)).notifyExpectedOutput(o,
        // getRepresentativeObserver(observer), out.formalName);
        // } catch (ThinklabException e) {
        // onException(e);
        // }
        //
        // step.outputs.add(out);
        // }
        // }

        /*
         * if we have conditionals, sort them in order of definition.
         */
        if (step.conditionals != null) {
            Collections.sort(step.conditionals, new Comparator<Cond>() {
                @Override
                public int compare(Cond o1, Cond o2) {
                    return o1.index - o2.index;
                }
            });
        }

        return true;
    }

    private void defineStates(ProcessingStep step) {

        // force storage if we only have this step.
        if (this.vertexSet().size() == 1 || _entryPoints.contains(step)) {
            step.stored = true;
        }

        if (step.stored) {

            IDataset ds = null;
            try {
                ds = ((Subject) _subject).requireBackingDataset(_contextId);
            } catch (ThinklabException e) {
                _monitor.error("I/O error: cannot create backing dataset for subject " + _subject.getId());
                return;
            }

            /**
             * This is the one that we actually asked for in a dependency, not the one
             * that interprets whatever we linked it to. One problem is that for bare classifications,
             * it is an incomplete classification, so for now we patch it as seen below.
             */
            IObserver actualObserver = step.observers.get(0);

            /**
             * FIXME this should be set at resolution when the classifications are matched, and the
             * following should be unnecessary. Matching classifications should check the types and
             * when we grow up, mediate them.
             */
            if (actualObserver instanceof Classification
                    && ((Classification) actualObserver).getClassification() == null) {
                actualObserver = step.observer;
            }

            step.states = ((StateAccessor) (step.accessor))
                    .createStates(_subject, step.model, actualObserver,
                            _modelGraph, ds);
            if (step.states != null) {
                for (IState state : step.states) {
                    _states.add(state);
                }
            }
        }
    }

    private IObserver getRepresentativeObserver(IObserver observer) {
        return observer instanceof IConditionalObserver ? ((IConditionalObserver) observer)
                .getRepresentativeObserver() : observer;
    }

    private boolean onException(Exception e) {
        _monitor.error(e);
        return false;
    }

    private boolean compute(ProcessingStep step, int contextIndex, HashSet<ProcessingStep> computed) {

        if (computed.contains(step))
            return true;

        computed.add(step);

        ((IStateAccessor) (step.accessor)).reset();

        /*
         * compute all non-conditional connections and retrieve the observable
         */
        for (DataPath in : this.incomingEdgesOf(step)) {

            if (in.isConditional)
                continue;

            if (compute(in.getSourceStep(), contextIndex, computed)) {
                ((IStateAccessor) (step.accessor)).setValue(in.formalName, _registers[in.register]);
            }
        }

        /*
         * process
         */
        try {
            if (step.isConditional) {

                /*
                 * ask accessor to compute each condition in order.
                 */
                ISwitchingAccessor switcher = (ISwitchingAccessor) step.accessor;
                String id = step.accessor.getName();
                boolean ok = false;
                Object value = null;
                for (Cond cond : step.conditionals) {

                    if (switcher.isNullCondition(cond.index)) {

                        /*
                         * compute the accessor, get the value and stop when it's not null. If so have the
                         * step update all other states and break the loop.
                         */
                        if (compute(cond.target, contextIndex, computed)) {
                            value = ((IStateAccessor) (cond.target.accessor)).getValue(cond.formalName);
                            ok = value != null;
                        }

                    } else if (switcher.getConditionValue(cond.index)) {
                        if (compute(cond.target, contextIndex, computed)) {
                            value = ((IStateAccessor) (cond.target.accessor)).getValue(cond.formalName);
                        }
                        ok = true;
                    }

                    if (ok) {

                        /*
                         * TODO properly handle side effects of the winning accessor.
                         */
                        switcher.setValue(id, value);
                        break;
                    }

                }

            } else {
                ((IStateAccessor) (step.accessor)).process(contextIndex);
            }
        } catch (ThinklabException e) {
            return onException(e);
        }

        /*
         * do what we need to do with the output
         */
        for (OutputAction action : step.outputs) {

            Object out = ((IStateAccessor) (step.accessor)).getValue(action.formalName);
            if (action.register >= 0) {
                _registers[action.register] = out;
            }
        }

        if (step.stored) {
            ((StateAccessor) (step.accessor)).updateStates(contextIndex);
        }

        return true;
    }

    private boolean compute(ProcessingStep step, ITransition contextIndex, HashSet<ProcessingStep> computed) {

        if (computed.contains(step))
            return true;

        computed.add(step);

        /*
         * compute all connections and retrieve the observable
         */

        /*
         * 
         */

        return true;
    }

    public GraphVisualization visualize() {

        /*
         * FIXME add total coverage from _coverage
         */
        GraphVisualization ret = new GraphVisualization();
        ret.adapt(this, new GraphAdapter<ProcessingStep, DataPath>() {

            @Override
            public String getNodeType(ProcessingStep o) {
                return "accessor";
            }

            @Override
            public String getNodeId(ProcessingStep o) {
                return "" + o.hashCode();
            }

            @Override
            public String getNodeLabel(ProcessingStep o) {
                return o.accessor.toString();
            }

            @Override
            public String getNodeDescription(ProcessingStep o) {
                // TODO Auto-generated method stub
                return null;
            }

            @Override
            public String getEdgeType(DataPath o) {
                // TODO Auto-generated method stub
                return null;
            }

            @Override
            public String getEdgeId(DataPath o) {
                return "" + o.hashCode();
            }

            @Override
            public String getEdgeLabel(DataPath de) {
                return (de.observable == null ? "" : de.observable.getLocalName())
                        + (de.isConditional ? (" #" + de.conditionIndex) : "");
            }

            @Override
            public String getEdgeDescription(DataPath o) {
                // TODO Auto-generated method stub
                return null;
            }
        });

        return ret;
    }

    public List<ISubject> getGeneratedSubjects() {
        return _subjects;
    }
}
