package org.integratedmodelling.thinklab.modelling.resolver;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.collections.Triple;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.knowledge.IProperty;
import org.integratedmodelling.thinklab.api.knowledge.ISemanticObject;
import org.integratedmodelling.thinklab.api.knowledge.kbox.IKbox;
import org.integratedmodelling.thinklab.api.knowledge.query.IQuery;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.listeners.INotification;
import org.integratedmodelling.thinklab.api.metadata.IMetadata;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.IScale;
import org.integratedmodelling.thinklab.api.modelling.ITopologicallyComparable;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.debug.DataRecorder;
import org.integratedmodelling.thinklab.kbox.KBoxResult;
import org.integratedmodelling.thinklab.modelling.lang.Scale;
import org.integratedmodelling.thinklab.query.Queries;
import org.integratedmodelling.thinklab.query.Query;

/**
 * Specialized query strategy for models to be resolved. Can be configured with multiple queries. Can
 * iterate over the result of each query in order of priority.
 * 
 * @author ferdinando.villa
 * @deprecated switch to ObservationKbox and its ranking methods
 */
public class ModelQuery implements Iterable<List<ISemanticObject<?>>> {

    IKbox _kbox;
    IMetadata _priorities;
    IScale _scale;
    Set<IObservable> _observables;
    Collection<String> _scenarios;
    Collection<String> _nsWhitelist;
    Collection<String> _nsBlacklist;
    IMonitor _monitor;
    IObserver _observer;

    /*
     * the different queries to try, most important first. Added by next()
     */
    ArrayList<IQuery> _queries = new ArrayList<IQuery>();
    ArrayList<String> _qdescs = new ArrayList<String>();

    private INamespace _defaultNamespace;

    private Comparator<IMetadata> _prioritizer;

    public static final String DEBUG_ID = "MODELQUERY";

    /*
     * flags
     */
    public static final int DATA_MODELS = 0x0001;
    public static final int PROCESS_MODELS = 0x0002;
    public static final int RESOLVED_MODELS = 0x0004;
    public static final int UNRESOLVED_MODELS = 0x0008;
    public static final int CONTEXT_SPECIFIC = 0x0010;
    public static final int CONTEXT_INDEPENDENT = 0x0020;
    public static final int IN_DEFAULT_NAMESPACE = 0x0040;
    public static final int DO_NOT_CHECK_NAMESPACES = 0x0080;
    public static final int NOT_IN_SCENARIOS = 0x0100;
    public static final int IN_SCENARIOS = 0x0200;
    public static final int OBJECT_MODELS = 0x0400;
    public static final int INHERENT_MODELS = 0x0800;

    class It implements Iterator<List<ISemanticObject<?>>> {

        int _idx = 0;

        @Override
        public boolean hasNext() {
            return _idx < _queries.size();
        }

        @Override
        public List<ISemanticObject<?>> next() {

            List<ISemanticObject<?>> ret = new ArrayList<ISemanticObject<?>>();
            KBoxResult result = null;
            try {
                IQuery q = _queries.get(_idx++);
                result = (KBoxResult) _kbox.query(q, _prioritizer, _monitor);
                if (Thinklab.get().getNotificationLevel() == INotification.DEBUG) {
                    DataRecorder.get().add(
                            new Triple<IQuery, KBoxResult, String>(q, result, _qdescs.get(_idx - 1)),
                            DEBUG_ID, false);
                }
            } catch (ThinklabException e) {
                throw new ThinklabRuntimeException(e);
            }

            if (result.size() > 1) {

                /*
                 * sort by metadata and return. 
                 */
                if (_priorities != null) {
                    for (int i = 0; i < result.size(); i++) {
                        result.setScore(i, computeScore(((KBoxResult) result).getMetadata(i)));
                    }
                }

                return result;

            } else if (result.size() > 0) {
                ret.add(result.get(0));
            }

            return ret;
        }

        /**
         * Return the string description of the current query (call only after next() returned
         * a value).
         * 
         * @return
         */
        public String getQueryDesc() {
            return _qdescs.get(_idx - 1);
        }

        @Override
        public void remove() {
        }

    }

    public ModelQuery(Set<IObservable> observables, IKbox kbox, IScale scale, IObserver observer,
            INamespace defaultNamespace, IMetadata priorities, Collection<String> scenarios,
            Collection<String> nsWhitelist, Collection<String> nsBlacklist, IMonitor monitor) {

        _observables = observables;
        _kbox = kbox;
        _scale = scale;
        _priorities = priorities;
        _nsWhitelist = nsWhitelist;
        _nsBlacklist = nsBlacklist;
        _scenarios = scenarios;
        _defaultNamespace = defaultNamespace;
        //        _prioritizer = new ModelPrioritizer(priorities);
        _monitor = monitor;
        _observer = observer;
    }

    /**
     * Select the objects that match all extents in the query, and have no other multiple extents
     * to match.
     * 
     * @param query
     * @return
     * @throws ThinklabException 
     */
    public IQuery matchScale(IQuery query) throws ThinklabException {

        if (_scale == null)
            return query;

        List<Pair<IProperty, ITopologicallyComparable<?>>> coverageProperties = ((Scale) _scale)
                .getCoverageProperties(_monitor);

        int nExt = 0;
        for (Pair<IProperty, ITopologicallyComparable<?>> cp : coverageProperties) {
            nExt++;
            query = query.restrict(cp.getFirst(), Queries.intersects(cp.getSecond()));
        }

        return ((Query) query).restrict(NS.HAS_MULTIPLE_EXTENT_COUNT, Queries.compare(nExt, Queries.LE));
    }

    /**
     * NOTE: only meaningful AFTER the scale has been checked.
     * @param query
     * @return
     * @throws ThinklabException 
     */
    public IQuery noScale(IQuery query) throws ThinklabException {
        return ((Query) query).restrict(NS.HAS_MULTIPLE_EXTENT_COUNT, Queries.is(0));
    }

    public IQuery getBaseQuery(Set<IObservable> observables, int flags, Object... args)
            throws ThinklabException {

        Query ret = Queries.select(Thinklab.c(NS.MODEL));

        if (observables.size() == 1) {
            IObservable c = observables.iterator().next();
            ret = ret.restrict(NS.HAS_OBSERVABLE, getObservableQuery(c, flags));
        } else {

            ArrayList<IQuery> oq = new ArrayList<IQuery>();
            for (IObservable c : observables) {
                oq.add(Queries.restriction(NS.HAS_OBSERVABLE, getObservableQuery(c, flags)));
            }
            ret = ret.restrict(Queries.or(oq.toArray(new IQuery[oq.size()])));
        }

        /**
         * if it's something 'of' something that we're looking for, ensure the observer
         * is of the same type and is inherent to the intended subject.
         */
        //		if (_observer != null && _observer.getInherentSubjectType() != null) {
        //			ret = ret.restrict(
        //					NS.HAS_OBSERVER,
        //					Queries.select(((Observer<?>)_observer).getDirectType()).
        //						restrict(NS.DEREIFIES_TYPE, Queries.selectAnyOf(_observer.getInherentSubjectType())));
        //		}

        if ((flags & DATA_MODELS) != 0) {
            ret = ret.restrict(NS.IS_DATA_MODEL, Queries.is(true));
        } else if ((flags & PROCESS_MODELS) != 0) {
            ret = ret.restrict(NS.IS_DATA_MODEL, Queries.is(false));
        }

        if ((flags & RESOLVED_MODELS) != 0) {
            ret = ret.restrict(NS.HAS_DIRECT_DATA, Queries.is(true));
        } else if ((flags & UNRESOLVED_MODELS) != 0) {
            ret = ret.restrict(NS.HAS_DIRECT_DATA, Queries.is(false));
        }

        if ((flags & IN_DEFAULT_NAMESPACE) == 0) {
            ret = ret.restrict(NS.IS_PRIVATE, Queries.is(false));
        }

        if ((flags & IN_SCENARIOS) != 0) {
            ret = ret.restrict(NS.IS_IN_SCENARIO, Queries.is(true));
        } else if ((flags & NOT_IN_SCENARIOS) != 0) {
            ret = ret.restrict(NS.IS_IN_SCENARIO, Queries.is(false));
        }

        if ((flags & CONTEXT_SPECIFIC) != 0) {
            ret = (Query) matchScale(ret);
        } else if ((flags & CONTEXT_INDEPENDENT) != 0) {
            ret = (Query) noScale(ret);
        }

        /*
         * scenario list takes over namespace whitelist
         */
        if (((flags & IN_SCENARIOS) != 0) && _scenarios != null) {

            if (_scenarios.size() == 1) {
                ret = ret.restrict(NS.HAS_NAMESPACE_ID, Queries.is(_scenarios.iterator().next()));
            } else {

                ArrayList<IQuery> oq = new ArrayList<IQuery>();
                for (Object c : _scenarios) {
                    oq.add(Queries.restriction(NS.HAS_NAMESPACE_ID, Queries.is(c)));
                }
                ret = ret.restrict(Queries.or(oq.toArray(new IQuery[oq.size()])));
            }
        }

        if (((flags & IN_SCENARIOS) == 0) && (flags & IN_DEFAULT_NAMESPACE) != 0 && _defaultNamespace != null) {

            ret = ret.restrict(NS.HAS_NAMESPACE_ID, Queries.is(_defaultNamespace.getId()));

        } else if (_nsWhitelist != null) {

            if (_nsWhitelist.size() == 1) {
                ret = ret.restrict(Queries.restriction(NS.HAS_NAMESPACE_ID,
                        Queries.is(_nsWhitelist.iterator().next())));
            } else {

                ArrayList<IQuery> oq = new ArrayList<IQuery>();
                for (Object c : _nsWhitelist) {
                    oq.add(Queries.restriction(NS.HAS_NAMESPACE_ID, Queries.is(c)));
                }
                ret = ret.restrict(Queries.or(oq.toArray(new IQuery[oq.size()])));
            }
        }

        if (_nsBlacklist != null) {

            if (_nsBlacklist.size() == 1) {
                ret = ret.restrict(Queries.no(Queries.restriction(NS.HAS_NAMESPACE_ID,
                        Queries.is(_nsBlacklist.iterator().next()))));
            } else {

                ArrayList<IQuery> oq = new ArrayList<IQuery>();
                for (Object c : _nsBlacklist) {
                    oq.add(Queries.no(Queries.restriction(NS.HAS_NAMESPACE_ID, Queries.is(c))));
                }
                ret = ret.restrict(Queries.and(oq.toArray(new IQuery[oq.size()])));
            }
        }

        return ret;
    }

    private IQuery getObservableQuery(IObservable c, int flags) {

        Query ret = Queries.select(NS.OBSERVABLE).restrict(NS.HAS_OBSERVABLE_CONCEPT,
                Queries.select(c.getType()));

        /*
         * Add compatible observation types. Still some work to do on the semantics of
         * compatible observations - CHECK make sure everything's OK.
         */
        if (c.getObservationType() != null) {
            ret = ret.restrict(NS.HAS_OBSERVATION_TYPE,
                    Queries.select(c.getObservationType()).setFlags(Query.USE_SEMANTIC_CLOSURE));
        }

        /*
         * inherency only if required and appropriate. FIXME: switch to looking for it in the observable. This will
         * work but it's a side effect of a redundancy that must be eliminated.
         */
        if ((flags & INHERENT_MODELS) != 0 && c.getInherentType() != null) {
            ret = ret.restrict(NS.HAS_INHERENT_SUBJECT_TYPE,
                    Queries.select(c.getInherentType()).setFlags(Query.USE_SEMANTIC_CLOSURE));
        }

        // if not required, it's not that we don't care - we want models that are not inherent.
        if ((flags & INHERENT_MODELS) == 0) {
            //			ret = ret.restrict(Queries.no(
            //					NS.HAS_INHERENT_SUBJECT_TYPE, 
            //					Queries.select(c.getInherentType()).setFlags(Query.USE_SEMANTIC_CLOSURE));
        }

        return ret;
    }

    /**
     * Add a query as specified. Arguments need only be passed on queries that 
     * have them - currently only the namespace criteria. Returns self to be 
     * used with a fluent idiom.
     * 
     * @param flags
     * @param arguments
     * @throws ThinklabException 
     */
    public ModelQuery add(int flags, Object... args) throws ThinklabException {
        IQuery q = getBaseQuery(_observables, flags, args);
        //		((Query)q).setFlags(Query.USE_SEMANTIC_CLOSURE);
        _queries.add(q);
        _qdescs.add(describeFlags(flags));
        return this;
    }

    private String describeFlags(int flags) {

        String ret = "";

        if ((flags & DATA_MODELS) != 0)
            ret += (ret.isEmpty() ? "" : " ") + "data";
        if ((flags & PROCESS_MODELS) != 0)
            ret += (ret.isEmpty() ? "" : " ") + "process";
        if ((flags & RESOLVED_MODELS) != 0)
            ret += (ret.isEmpty() ? "" : " ") + "resolved";
        if ((flags & UNRESOLVED_MODELS) != 0)
            ret += (ret.isEmpty() ? "" : " ") + "unresolved";
        if ((flags & CONTEXT_SPECIFIC) != 0)
            ret += (ret.isEmpty() ? "" : " ") + "context";
        if ((flags & CONTEXT_INDEPENDENT) != 0)
            ret += (ret.isEmpty() ? "" : " ") + "nocontext";
        if ((flags & IN_DEFAULT_NAMESPACE) != 0)
            ret += (ret.isEmpty() ? "" : " ") + "in-namespace";
        if ((flags & DO_NOT_CHECK_NAMESPACES) != 0)
            ret += "";
        if ((flags & NOT_IN_SCENARIOS) != 0)
            ret += "";
        if ((flags & IN_SCENARIOS) != 0)
            ret += (ret.isEmpty() ? "" : " ") + "in-scenario";
        if ((flags & OBJECT_MODELS) != 0)
            ret += (ret.isEmpty() ? "" : " ") + "objects";
        if ((flags & INHERENT_MODELS) != 0)
            ret += (ret.isEmpty() ? "" : " ") + "inherent";

        return "(" + ret + ")";
    }

    /**
     * Returns the models that match the query configuration
     * established before calling find(). If there is more than one result, they
     * will be sorted so that the "best" alternative comes first. 
     * 
     * The models returned here may not be resolvable, so this is not used
     * in the model resolver - the iterator behavior is used instead until
     * something can be resolved.
     * 
     * @return
     * @throws ThinklabException 
     */
    public List<ISemanticObject<?>> find() throws ThinklabException {

        List<ISemanticObject<?>> ret = new ArrayList<ISemanticObject<?>>();

        for (IQuery q : _queries) {

            KBoxResult result = (KBoxResult) _kbox.query(q, _prioritizer, _monitor);

            if (result.size() > 1) {
                return result;
            } else if (result.size() > 0) {
                ret.add(result.get(0));
            }

            if (ret.size() > 0)
                return ret;
        }

        /*
         * empty return
         */
        return ret;

    }

    private double computeScore(IMetadata metadata) {

        return 1.0;
    }

    public void dump() {

        int i = 0;
        for (IQuery q : _queries) {
            System.out.println(i++ + ". " + q);
        }
    }

    @Override
    public Iterator<List<ISemanticObject<?>>> iterator() {
        return new It();
    }

}
