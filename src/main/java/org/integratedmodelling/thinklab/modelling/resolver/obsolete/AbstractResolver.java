package org.integratedmodelling.thinklab.modelling.resolver.obsolete;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.annotation.SemanticObject;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.modelling.IExtent;
import org.integratedmodelling.thinklab.api.modelling.IModel;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.api.modelling.IObjectSource;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.IObservingObject.IDependency;
import org.integratedmodelling.thinklab.api.modelling.IReifiableObject;
import org.integratedmodelling.thinklab.api.modelling.IScale;
import org.integratedmodelling.thinklab.api.modelling.IState;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.api.modelling.resolution.IObservationKbox;
import org.integratedmodelling.thinklab.api.modelling.resolution.ISubjectResolver;
import org.integratedmodelling.thinklab.api.time.ITemporalExtent;
import org.integratedmodelling.thinklab.common.utils.StringUtils;
import org.integratedmodelling.thinklab.debug.CallTracer;
import org.integratedmodelling.thinklab.modelling.lang.Measurement;
import org.integratedmodelling.thinklab.modelling.lang.Metadata;
import org.integratedmodelling.thinklab.modelling.lang.Model;
import org.integratedmodelling.thinklab.modelling.lang.Model.AttributeTranslator;
import org.integratedmodelling.thinklab.modelling.lang.Ranking;
import org.integratedmodelling.thinklab.modelling.lang.Scale;
import org.integratedmodelling.thinklab.modelling.lang.Subject;
import org.integratedmodelling.thinklab.modelling.lang.Value;
import org.integratedmodelling.thinklab.modelling.resolver.Coverage;
import org.integratedmodelling.thinklab.modelling.resolver.ProvenanceGraph;
import org.integratedmodelling.thinklab.modelling.resolver.ResolutionContext;
import org.integratedmodelling.thinklab.modelling.resolver.StateModel;
import org.integratedmodelling.thinklab.modelling.resolver.SubjectFactory;
import org.integratedmodelling.thinklab.modelling.resolver.ProvenanceGraph.DependencyEdge;
import org.integratedmodelling.thinklab.modelling.resolver.ProvenanceGraph.ProvenanceNode;
import org.integratedmodelling.thinklab.modelling.states.ConstObjectState;
import org.integratedmodelling.thinklab.time.Time;
import org.integratedmodelling.thinklab.time.extents.TimeHelper;
import org.integratedmodelling.utils.CamelCase;

public abstract class AbstractResolver implements ISubjectResolver {

    protected final SubjectObserver subjectObserver;
    protected final IMonitor monitor;

    public AbstractResolver(SubjectObserver subjectObserver, IMonitor monitor) {
        this.subjectObserver = subjectObserver;
        this.monitor = monitor;
    }

    String addError(String string) {
        subjectObserver._errors.add(string);
        return string;
    }

    /**
     * Return true if the observer interprets data from a datasource directly - i.e. does not need any
     * transformation to interpret them. This usually applies to numeric ones, so that's what we check for.
     * 
     * @param observer
     * @return
     */
    boolean isObserverTrivial(IObserver observer) {
        return observer instanceof Measurement || observer instanceof Ranking || observer instanceof Value;
    }

    /*
     * this check is only performed in the "mock" resolution process, called on temporary subjects from within
     * findDataModels. These subjects have the beingResolved has set to non-null. This ensures that only
     * non-recursive dependencies are accepted; then another (actual) resolution is done to add the model
     * chain to the provenance graph, and the subsequent dependencies take it from there.
     */
    boolean checkInfiniteRecursion(IObservable observableBeingResolved) {
        return (subjectObserver.isBeingResolved(observableBeingResolved));
    }

    /*
     * Find observable in models that were resolved from this same contextualizer.
     * 
     * @param observables
     * 
     * @return
     */
    ProvenanceNode findResolved(Set<IObservable> observables) {

        for (IObservable c : observables) {
            if (subjectObserver.models.containsKey(c)) {
                return subjectObserver.models.get(c);
            }
        }
        return null;
    }

    /*
     * find observable in models that come from adapting pre-existing states to datasources.
     */
    IModel findStateFor(Set<IObservable> observables, ResolutionContext ctx) {

        for (IObservable o : observables) {

            for (IState s : subjectObserver.getSubject().getStates()) {
                if (s.getObservable().canResolve(o)) {
                    if (monitor != null) {
                        monitor.info("using previously computed state for " + o.getType(), null);
                    }
                    return new StateModel(o, s);
                }
            }

        }
        return null;
    }

    void accept(IObservable observable, Coverage coverage, List<Pair<SubjectObserver, Coverage>> list,
            ProvenanceNode root, ResolutionContext ctx) throws ThinklabException {

        CallTracer.msg("ACCEPT(" + observable + ", " + root + ")", this);

        List<ProvenanceNode> nodes = new ArrayList<ProvenanceNode>();
        for (Pair<SubjectObserver, Coverage> s : list) {
            mergeProvenance(s.getFirst(), ctx);
            nodes.add(s.getFirst().getResolutionContext().modelGraph.get(s.getFirst()._model));
        }

        if (nodes.size() == 1) {
            ctx.modelGraph.addEdge(nodes.get(0), root, ctx.link);
            subjectObserver.models.put(observable, nodes.get(0));
        } else {
            ProvenanceNode nn = makeModel(observable, coverage, list, ctx, nodes);
            ctx.modelGraph.addEdge(nn, root, ctx.link);
            subjectObserver.models.put(observable, nn);
        }
    }

    private void mergeProvenance(SubjectObserver s, ResolutionContext ctx) {

        for (ProvenanceNode node : s.getResolutionContext().modelGraph.vertexSet()) {
            ctx.modelGraph.addVertex(node);
        }
        for (DependencyEdge edge : s.getResolutionContext().modelGraph.edgeSet()) {
            if (!ctx.modelGraph.containsEdge(edge)) {
                ctx.modelGraph.addEdge(edge.getSourceNode(), edge.getTargetNode(), edge);
            }
        }
        subjectObserver.models.putAll(s.models);
    }

    private ProvenanceNode makeModel(IObservable concept, Coverage coverage,
            List<Pair<SubjectObserver, Coverage>> list, ResolutionContext ctx, List<ProvenanceNode> conditions)
            throws ThinklabException {

        ArrayList<IModel> models = new ArrayList<IModel>();
        for (Pair<SubjectObserver, Coverage> s : list) {
            models.add(s.getFirst()._model);
        }
        IModel model = new Model(SemanticObject.newInstance(concept.getType()), CamelCase.toLowerCase(
                concept.getLocalName(), '-'), subjectObserver.getSubject().getNamespace(), models);
        ProvenanceNode obs = ctx.modelGraph.add(model.getObserver());

        int cidx = 0;
        for (ProvenanceNode node : conditions) {
            DependencyEdge de = new DependencyEdge(DependencyEdge.CONDITIONAL_DEPENDENCY, "", concept);
            de.conditionIndex = cidx++;
            ctx.modelGraph.addEdge(node, obs, de);
        }

        ProvenanceNode ret = ctx.modelGraph.add(model);
        ret.coverage = coverage;

        ctx.modelGraph.addEdge(
                obs,
                ret,
                new DependencyEdge(DependencyEdge.DEFINE_STATE, "", subjectObserver.getObservable(model
                        .getObserver())));
        return ret;
    }

    /**
     * TODO use this as a model for a single findModel to be located in AbstractResolver, used for all
     * types of observables. 
     * 
     * Process observables should probably use a single model to avoid deconstruction
     * of dependents, but subject models may well use more than one to generate subjects in a context so
     * the process should be the same. Also, the sandbox resolver should be used for all of these, not
     * just for data.
     * 
     * @param observables
     * @param scale
     * @param ctx
     * @return the model (possibly multiple) that observes any of the passed concepts in the context, and its
     * coverage. If no model is found, model is null and coverage is 0.
     * @throws ThinklabException
     */
    protected Pair<Coverage, List<Pair<SubjectObserver, Coverage>>> findModels(IObservable observable,
            ResolutionContext ctx) throws ThinklabException {

        Set<IObservable> observables = ctx.getObservableClosure(observable);

        /*
         * this can only happen during proactive resolution attempts called from within the loop below.
         */
        if (checkInfiniteRecursion(observable) || (monitor != null && monitor.isStopped())) {
            return null;
        }

        /*
         * perform one query at a time and accumulate resolvable models until the model set has enough
         * coverage of the context.
         */
        Coverage coverage = new Coverage(ctx.scale);
        ArrayList<Pair<SubjectObserver, Coverage>> accepted = new ArrayList<Pair<SubjectObserver, Coverage>>();
        IObservationKbox kbox = Thinklab.get().requireKbox(Thinklab.DEFAULT_KBOX);

        for (IModel m : kbox.query(observable, ctx)) {

            SubjectObserver sandbox = subjectObserver.getSandboxObserver(m, observables, ctx.optional);
            Coverage mcov = (Coverage) sandbox.resolve(ctx.getScenarios());

            /*
             * only accept models that contribute enough ADDITIONAL coverage - which cannot happen unless
             * they have a minimum coverage of as much context.
             */
            if (mcov != null && mcov.getCoverage() >= subjectObserver.MIN_MODEL_COVERAGE) {

                double pcov = coverage.getCoverage();
                Coverage ncov = coverage.or(mcov, subjectObserver.MIN_MODEL_COVERAGE);
                if (ncov.getCoverage() - pcov >= subjectObserver.MIN_MODEL_COVERAGE) {
                    accepted.add(new Pair<SubjectObserver, Coverage>(sandbox, mcov));
                    coverage = ncov;
                    if (monitor != null) {
                        monitor.info(
                                m.getName()
                                        + (accepted.size() == 1 ? (" covers "
                                                + StringUtils.percent(ncov.getCoverage() - pcov) + " of " + observable
                                                .getType()) : (" adds "
                                                + StringUtils.percent(ncov.getCoverage() - pcov)
                                                + " coverage of " + observable.getType())), null);
                    }
                }
                if (coverage.getCoverage() >= subjectObserver.REQUIRED_COVERAGE) {
                    break;
                }
            }
        }

        if (monitor != null && accepted.size() == 0) {
            if (coverage.getCoverage() > 0.00001) {
                monitor.info(
                        "concept " + observable.getType() + " has insufficient coverage in this context",
                        null);
            } else {
                // FIXME redundant? Clearly it is if it's part of a dependency.
                monitor.info("concept " + observable.getType() + " cannot be observed in this context", null);
            }
        }

        return new Pair<Coverage, List<Pair<SubjectObserver, Coverage>>>(coverage, accepted);
    }

    Collection<Pair<SubjectObserver, IModel>> createDependentSubjects(IDependency dependency,
            ResolutionContext ctx) throws ThinklabException {
        CallTracer.indent("createDependentSubjects()", this, dependency);

        ArrayList<Pair<SubjectObserver, IModel>> ret = new ArrayList<Pair<SubjectObserver, IModel>>();

        ISubject rootSubject = subjectObserver.getSubject();
        INamespace namespace = rootSubject.getNamespace();
        String name = dependency.getFormalName();
        if (name == null) {
            name = dependency.getObservable().getFormalName();
        }
        if (name == null) {
            name = CamelCase.toLowerCase(dependency.getObservable().getType().getLocalName(), '-');
        }

        if (dependency.getContextModel() != null) {

            /*
             * TODO check if this is really the way. The logics below should take care of all situations.
             * 
             * compute context model
             */
        }

        IObservable observable = subjectObserver.getObservable(dependency);

        IModel observableModel = observable.getModel();
        if (dependency.isDistributed()) {
            CallTracer.msg("creating distributed subjects for each cell in the dependency scale...");

            IModel rModel = observableModel;

            if (rModel != null && !rModel.isReificationModel()) {
                throw new ThinklabValidationException(
                        "cannot use a non-reifying model to resolve a distributed object dependency");
            }

            if (rModel == null) {
                rModel = subjectObserver.findSubjectModel(observable.getType(), ctx, true);
            }

            if (rModel /* still */== null) {
                rModel = subjectObserver.createDefaultSubjectModel(observable);
            }

            /*
             * run it man
             */
            for (ISubject s : createSubjects(rModel, rootSubject.getScale())) {
                ret.add(new Pair<SubjectObserver, IModel>(new SubjectObserver((Subject) s, monitor, rModel,
                        subjectObserver._refNamespace), rModel));
            }

            /*
             * TODO the rest when the model has no object source: create the agents and resolve them using the
             * upper-level subject as context. Not sure whether this is subsumed in Model.createObjects or we
             * need another strategy here.
             */

        } else {
            // not distributed. create one subject for the whole context.
            CallTracer.msg("creating one subject to satisfy the entire scale...");

            // update the temporal extent to use the new subject's resolution
            // TODO this should really be done for all extents... how to generalize?
            Scale combinedScale = new Scale();
            List<IExtent> extents = combinedScale.getExtents();
            for (IExtent parentExtent : ctx.getScale()) {
                if (parentExtent.getDomainConcept().equals(Time.TIME_DOMAIN) && observableModel != null) {
                    // get the observable time scale constrained to the envelope of the root time scale
                    IScale modelScale = observableModel.getScale();
                    ITemporalExtent modelScaleTimeExtent = modelScale.getTime();
                    ITemporalExtent newSubjectTime = TimeHelper.getConstrainedTemporalExtent(
                            modelScaleTimeExtent, (ITemporalExtent) parentExtent);
                    extents.add(newSubjectTime);
                } else {
                    // non-temporal extent. just add it without modifying.
                    extents.add(parentExtent);
                }
            }

            // TODO if we have computed a context model, initialize scale from that.
            // use passed scale here, the default situation
            Subject subject = SubjectFactory.getSubjectByMetadata(observable, namespace, combinedScale, name);

            IModel model = dependency.getObservable().getModel();
            if (model == null) {
                model = subjectObserver.findSubjectModel(subject, ctx, dependency.isDistributed());
            }

            if (model != null) {
                ret.add(new Pair<SubjectObserver, IModel>(new SubjectObserver(subject, monitor, model,
                        subjectObserver._refNamespace), model));
            }
        }

        CallTracer.unIndent();
        return ret;
    }

    Collection<Pair<SubjectObserver, IModel>> createDependentProcesses(IDependency dependency,
            IScale parentScale) throws ThinklabException {

        CallTracer.indent("createDependentProcesses()", this, dependency);

        ArrayList<Pair<SubjectObserver, IModel>> ret = new ArrayList<Pair<SubjectObserver, IModel>>();

        ISubject rootSubject = subjectObserver.getSubject();
        INamespace namespace = rootSubject.getNamespace();
        String name = dependency.getFormalName();
        if (name == null) {
            name = dependency.getObservable().getFormalName();
        }
        if (name == null) {
            name = CamelCase.toLowerCase(dependency.getObservable().getType().getLocalName(), '-');
        }

        IObservable observable = subjectObserver.getObservable(dependency);

        IModel observableModel = observable.getModel();
        // not distributed. create one subject for the whole context.
        CallTracer.msg("creating one process to satisfy the entire scale...");

        // update the temporal extent to use the new subject's resolution
        // TODO this should really be done for all extents... how to generalize?
        Scale combinedScale = new Scale();
        List<IExtent> extents = combinedScale.getExtents();
        for (IExtent parentExtent : parentScale) {
            if (parentExtent.getDomainConcept().equals(Time.TIME_DOMAIN) && observableModel != null) {
                // get the observable time scale constrained to the envelope of the root time scale
                IScale modelScale = observableModel.getScale();
                ITemporalExtent modelScaleTimeExtent = modelScale.getTime();
                ITemporalExtent newSubjectTime = TimeHelper.getConstrainedTemporalExtent(
                        modelScaleTimeExtent, (ITemporalExtent) parentExtent);
                extents.add(newSubjectTime);
            } else {
                // non-temporal extent. just add it without modifying.
                extents.add(parentExtent);
            }
        }

        // TODO if we have computed a context model, initialize scale from that.
        // use passed scale here, the default situation
        Subject subject = SubjectFactory.getSubjectByMetadata(observable, namespace, combinedScale, name);

        IModel model = dependency.getObservable().getModel();
        if (model == null) {
            model = subjectObserver.findProcessModel(subject, parentScale);
        }

        if (model != null) {
            ret.add(new Pair<SubjectObserver, IModel>(new SubjectObserver(subject, monitor, model,
                    subjectObserver._refNamespace), model));
        }

        CallTracer.unIndent();
        return ret;
    }

    private List<ISubject> createSubjects(IModel model, IScale scale) throws ThinklabException {
        ArrayList<ISubject> ret = new ArrayList<ISubject>();
        IObjectSource objectSource = model.getObjectSource();

        if (objectSource != null) {
            int i = 1;
            for (IReifiableObject o : objectSource.getObjects(scale)) {
                String name = model.getId() + "-" + i++;
                Subject subject = SubjectFactory.getSubjectByMetadata(model.getObservableConcept(),
                        model.getNamespace(), scale, name);

                for (AttributeTranslator at : ((Model) model).getAttributeTranslators()) {
                    Object att = o.getAttributeValue(at.attribute, at.observer);
                    if (at.observer == null) {
                        ((Metadata) (subject.getMetadata())).put(at.property.toString(), att);
                    } else {
                        subject.addState(new ConstObjectState(at.observer.getObservableConcept(), subject,
                                at.observer, att));
                    }
                }

                ret.add(subject);
            }
        }

        return ret;
    }
}
