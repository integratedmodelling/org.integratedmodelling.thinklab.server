package org.integratedmodelling.thinklab.modelling.resolver.obsolete;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabInternalErrorException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IProperty;
import org.integratedmodelling.thinklab.api.knowledge.ISemanticObject;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.modelling.IActiveSubject;
import org.integratedmodelling.thinklab.api.modelling.ICoverage;
import org.integratedmodelling.thinklab.api.modelling.IDataset;
import org.integratedmodelling.thinklab.api.modelling.IModel;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.IObservingObject;
import org.integratedmodelling.thinklab.api.modelling.IObservingObject.IDependency;
import org.integratedmodelling.thinklab.api.modelling.IScale;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.api.modelling.ISubjectAccessor;
import org.integratedmodelling.thinklab.api.modelling.ISubjectObserver;
import org.integratedmodelling.thinklab.api.modelling.agents.IObservationController;
import org.integratedmodelling.thinklab.api.modelling.agents.IObservationGraphNode;
import org.integratedmodelling.thinklab.api.modelling.agents.IObservationWorker;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.api.modelling.resolution.IResolutionStrategy;
import org.integratedmodelling.thinklab.api.modelling.resolution.ISubjectResolver;
import org.integratedmodelling.thinklab.api.modelling.scheduling.ITransition;
import org.integratedmodelling.thinklab.api.time.ITemporalExtent;
import org.integratedmodelling.thinklab.api.time.ITimePeriod;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.common.vocabulary.Observable;
import org.integratedmodelling.thinklab.debug.CallTracer;
import org.integratedmodelling.thinklab.modelling.datasets.CFdataset;
import org.integratedmodelling.thinklab.modelling.lang.Model;
import org.integratedmodelling.thinklab.modelling.lang.ModelObject;
import org.integratedmodelling.thinklab.modelling.lang.Observer;
import org.integratedmodelling.thinklab.modelling.lang.ObservingObject;
import org.integratedmodelling.thinklab.modelling.lang.ObservingObject.Dependency;
import org.integratedmodelling.thinklab.modelling.lang.Scale;
import org.integratedmodelling.thinklab.modelling.lang.Subject;
import org.integratedmodelling.thinklab.modelling.lang.agents.AgentState;
import org.integratedmodelling.thinklab.modelling.lang.agents.ObservationController;
import org.integratedmodelling.thinklab.modelling.lang.agents.ObservationWorker;
import org.integratedmodelling.thinklab.modelling.lang.agents.TemporalCausalGraph;
import org.integratedmodelling.thinklab.modelling.resolver.Coverage;
import org.integratedmodelling.thinklab.modelling.resolver.ProvenanceGraph;
import org.integratedmodelling.thinklab.modelling.resolver.ProvenanceGraph.ProvenanceNode;
import org.integratedmodelling.thinklab.modelling.resolver.ResolutionStrategy;
import org.integratedmodelling.thinklab.modelling.scheduling.TemporalTransition;
import org.integratedmodelling.utils.CamelCase;

public class SubjectObserver implements ISubjectObserver {

    /*
     * the obvious fields: the subject we have resolved, the context of resolution (will be null in the root
     * subject) and the model chosen to create everything (never null).
     */
    Subject _root;
    SubjectObserver _context;
    IModel _model;
    IDataset _dataset;

    // ID of the context we're in, if any.
    long _contextId = -1;

    // this is only saved because the provenance in it may be passed to invoking contexts for
    // merging after a successful attempt at resolution.
    private ResolutionContext _resolutionContext;

    /*
     * the whole set of agents, including the top one. Should become a graph that we scan in order of
     * dependency.
     */
    ArrayList<SubjectObserver> _subjects = new ArrayList<SubjectObserver>();

    /*
     * expected coverage of the context by states after resolution.
     */
    double _dataCoverage;

    /*
     * do not return a model unless its coverage is greater than this. Default 0.
     */
    double MIN_MODEL_COVERAGE = 0.01;

    /*
     * default: we accept models if they cover at least 25% of the whole context
     */
    public static double MIN_TOTAL_COVERAGE = 0.25;

    /*
     * default: we stop adding models when we cover at least 95% of the whole context.
     */
    double REQUIRED_COVERAGE = 0.95;

    /*
     * additional dependencies for the model that will be built. These may be set from outside (cf. open world
     * assumption for observed objects).
     */
    ArrayList<IDependency> _dependencies = new ArrayList<IDependency>();

    Collection<String> _errors = new ArrayList<String>();

    /*
     * model cache for models that have resolved the passed ones
     */
    HashMap<IObservable, ProvenanceNode> models = new HashMap<IObservable, ProvenanceNode>();

    /*
     * cache for models that adapt previously computed states to datasources
     */
    HashMap<IConcept, IModel> datasources = new HashMap<IConcept, IModel>();

    /*
     * true when we're testing resolution of a model to decide if it's a candidate for resolving a dependency.
     * This is only set to true in secondary observers created by createSandboxObserver(), called within
     * concept resolvers.
     */
    boolean _isSandbox = false;

    /*
     * used to track concept being resolved to avoid infinite recursion. It's only not null in sandbox models
     * created inside findDataModels().
     */
    private HashSet<IObservable> beingResolved = null;

    /*
     * resolution strategy adopted, initially empty.
     */
    ResolutionStrategy _resolutionStrategy = new ResolutionStrategy();

    /*
     * true only when the subject is being used to test resolvability within an optional dependency.
     */
    boolean _optional = false;

    /*
     * may be null
     */
    IMonitor _monitor;

    /*
     * normally true, determines whether the subject accessor initializers are called; can be set/reset from
     * outside when that's indesirable, typically when the observer is being called upon by the initializers
     * themselves.
     */
    private boolean _initializeSubject = true;

    //    private ResolutionContext _resolutionContext;

    // time-oriented fields for observing a subject as it progresses through its life span
    private int currentTemporalExtentIndex = 0; // index
                                                // value
                                                // of
                                                // _scale.getTime().getExtent(i)
                                                // corresponding
                                                // to
                                                // agent's
                                                // temporal
                                                // location
    private ITimePeriod currentTimePeriodCache = null;
    private TemporalCausalGraph<ISubject, IObservationGraphNode> causalGraph;

    /*
     * original namespace of reference for subject resolution - usually the namespace of the
     * original observation (subject generator) or subject model that initiated the resolution.
     */
    INamespace _refNamespace;

    /*
     * special constructor: copy the current resolution cache from original, use candidate to resolve its
     * observables, and use to resolve the result (with monitoring disabled) to see if resolution works with
     * this candidate. If resolve() returns true, use merge(the new subject) to set the dependencies from the
     * tentative new subject.
     * 
     * Pattern should be
     * 
     * IModel model = get candidate.... ResolvedSubject test = new ResolvedSubject(this, model); if
     * (test.resolve()) accept(test);
     */
    private SubjectObserver(SubjectObserver original, IModel candidate, Set<IObservable> beingResolved,
            boolean isOptional) {

        _monitor = original._monitor;
        _root = original._root;
        _context = original._context;
        _dataCoverage = original._dataCoverage;
        _model = candidate;
        // _dependencies.addAll(original._dependencies);
        _errors = original._errors;
        _subjects.addAll(original._subjects);
        _optional = true;

        for (IObservable m : original.models.keySet()) {
            ProvenanceNode model = original.models.get(m);
            models.put(m, model);
        }

        models.putAll(original.models);
        datasources.putAll(original.datasources);

        if (original.beingResolved == null) {
            this.beingResolved = new HashSet<IObservable>();
        } else {
            this.beingResolved = original.beingResolved;
        }

        this.beingResolved.addAll(beingResolved);
    }

    /*
     * special constructor: take all states in the original subject and put them in a special resolution cache
     * as resolved datasource models, so they can be used to resolve any new dependency. Do not copy the
     * original dependencies, which were already resolved.
     */
    SubjectObserver(SubjectObserver original) {

        _monitor = original._monitor;
        _root = original._root;
        _context = original._context;
        _dataCoverage = original._dataCoverage;
        _model = original._model;
        _errors = original._errors;
        _refNamespace = original._refNamespace;

        for (IObservable m : original.models.keySet()) {
            ProvenanceNode model = original.models.get(m);
            models.put(m, model);
        }

        _subjects.addAll(original._subjects);
    }

    public SubjectObserver(ISubject subject, IMonitor monitor, INamespace refNamespace) {
        _root = (Subject) subject;
        _monitor = monitor;
        _refNamespace = refNamespace;
    }

    public SubjectObserver(Subject subject, IMonitor monitor, IModel model, INamespace refNamespace) {
        this(subject, monitor, refNamespace);
        _model = model;
    }

    /**
     * Add a dependency to be observed in the context of the subject we are modeling.
     * 
     * @param observable
     * @param distribute
     * @param property
     * @throws ThinklabException
     */
    public void addDependency(IObservable observable, IProperty property, boolean optional)
            throws ThinklabException {

        IObservable s = _root.getObservable();

        boolean distribute = observable.getModel() != null && observable.getModel().isReificationModel();

        /*
         * if the dependency is on an abstract concept, we observe all the disjoint children concepts of it.
         */
        ArrayList<IObservable> ob = new ArrayList<IObservable>();
        if (observable.getModel() == null && observable.getType().isAbstract()) {
            for (IConcept c : observable.getType().getDisjointConcreteChildren()) {
                ob.add(new Observable(c));
            }
        } else {
            ob.add(observable);
        }

        for (IObservable o : ob) {

            /*
             * we MUST have a property. This is the key that links the observable to the parent.
             */
            if (property == null) {
                property = ModelObject.getPropertyFor(o, s, _root.getNamespace());
            }

            _dependencies.add(new Dependency(o, o.getFormalName(), property, optional, distribute, null));
        }
    }

    /**
     * Set the threshold of coverage to consider a model for inclusion. The default is 5%.
     * 
     * @param c threshold as a proportion (0 to 1)
     */
    public void setMinimumModelCoverage(double c) {
        MIN_MODEL_COVERAGE = c;
    }

    public void setMinimumTotalCoverage(double c) {
        MIN_TOTAL_COVERAGE = c;
    }

    /**
     * Set the threshold of coverage to stop looking for other models to cover a context. Default is 95%.
     * 
     * @param c threshold as a proportion (0 to 1)
     */
    public void setSatisfactoryTotalCoverage(double c) {
        REQUIRED_COVERAGE = c;
    }

    //
    //    @Deprecated
    //    IModel findProcessModel(ISubject observable, IScale context) throws ThinklabException {
    //        List<Pair<IProperty, ITopologicallyComparable<?>>> coverageProperties = ((Scale) context)
    //                .getCoverageProperties(_monitor);
    //        //
    //        //        /*
    //        //         * the result set will be sorted by semantic distance first, so that items within the same namespace
    //        //         * have priority etc. The remaining criteria are taken from the metadata and may depend on the
    //        //         * namespace.
    //        //         */
    //        //        MultipleQueryResult result = new MultipleQueryResult(
    //        //                MetadataComparator.getComparator(MetadataComparator.SEMANTIC_DISTANCE_CRITERION,
    //        //                        MetadataComparator.DEFAULT_METADATA_CRITERION));
    //        //
    //        //        /*
    //        //         * try those with specific coverage of our context first.
    //        //         */
    //        //        IQuery query = Queries.select(NS.MODEL).restrict(
    //        //                NS.HAS_OBSERVABLE,
    //        //                Queries.select(NS.OBSERVABLE).restrict(NS.HAS_OBSERVABLE_CONCEPT,
    //        //                        Queries.select(observable.getDirectType())));
    //        //
    //        //        for (Pair<IProperty, ITopologicallyComparable<?>> cp : coverageProperties) {
    //        //            query = query.restrict(cp.getFirst(), Queries.covers(cp.getSecond()));
    //        //        }
    //        //
    //        //        result.add(Thinklab.get().requireKbox(Thinklab.DEFAULT_KBOX).query(query));
    //        //
    //        //        /*
    //        //         * try those without any coverage only if no models that are specific to this context are found.
    //        //         */
    //        //        if (result.size() == 0) {
    //        //
    //        //            query = Queries.select(NS.MODEL).restrict(
    //        //                    NS.HAS_OBSERVABLE,
    //        //                    Queries.select(NS.OBSERVABLE).restrict(NS.HAS_OBSERVABLE_CONCEPT,
    //        //                            Queries.select(observable.getDirectType())));
    //        //
    //        //            result.add(Thinklab.get().requireKbox(Thinklab.DEFAULT_KBOX).query(query));
    //        //        }
    //        //
    //        //        if (result.size() > 0) {
    //        //            IModel ret = (IModel) result.get(0);
    //        //            if (_monitor != null) {
    //        //                _monitor.info("using process model " + ret.getName() + " for " + observable, null);
    //        //            }
    //        //            return ret;
    //        //
    //        //        }
    //
    //        return null;
    //
    //    }
    //
    //    @Deprecated
    //    IModel findSubjectModel(ISubject observable, ResolutionContext ctx, boolean isDistributed)
    //            throws ThinklabException {
    //
    //        IModel ret = findSubjectModel(observable.getDirectType(), ctx, isDistributed);
    //
    //        // do not create a dumb subject if it's a process we're looking for - processes are expected to
    //        // not be dumb.
    //        if (ret == null && !NS.isProcess(observable.getDirectType())) {
    //            if (_monitor != null) {
    //                _monitor.info("creating default model for " + observable.getDirectType(), null);
    //            }
    //            ret = createDefaultSubjectModel(observable.getObservable());
    //        }
    //        return ret;
    //    }
    //
    //    /**
    //     * TODO incorporate rationale of choice and number of alternatives in resulting provenance graph.
    //     * 
    //     * @param observable
    //     * @param context
    //     * @param scenarios
    //     * @return
    //     * @throws ThinklabException
    //     * @deprecated slated for removal after testing of the proper one.
    //     */
    //    IModel findSubjectModel(IConcept observable, ResolutionContext context, boolean isDistributed)
    //            throws ThinklabException {
    //        //
    //        //        List<Pair<IProperty, ITopologicallyComparable<?>>> coverageProperties = ((Scale) (context.getScale()))
    //        //                .getCoverageProperties(_monitor);
    //        //
    //        //        /*
    //        //         * the result set will be sorted by semantic distance first, so that items within the same namespace
    //        //         * have priority etc. The remaining criteria are taken from the metadata and may depend on the
    //        //         * namespace.
    //        //         */
    //        //        MultipleQueryResult result = new MultipleQueryResult(
    //        //                MetadataComparator.getComparator(MetadataComparator.SEMANTIC_DISTANCE_CRITERION,
    //        //                        MetadataComparator.DEFAULT_METADATA_CRITERION));
    //        //
    //        //        /*
    //        //         * try those with specific coverage of our context first.
    //        //         */
    //        //        IQuery query = Queries.select(NS.MODEL)
    //        //                .restrict(
    //        //                        NS.HAS_OBSERVABLE,
    //        //                        Queries.select(NS.OBSERVABLE).restrict(NS.HAS_OBSERVABLE_CONCEPT,
    //        //                                Queries.select(observable)));
    //        //
    //        //        if (isDistributed) {
    //        //            query = query.restrict(Thinklab.p(NS.IS_REIFICATION_MODEL), Queries.is(true));
    //        //        }
    //        //
    //        //        for (Pair<IProperty, ITopologicallyComparable<?>> cp : coverageProperties) {
    //        //            query = query.restrict(cp.getFirst(), Queries.covers(cp.getSecond()));
    //        //        }
    //        //
    //        //        result.add(Thinklab.get().requireKbox(Thinklab.DEFAULT_KBOX).query(query));
    //        //
    //        //        /*
    //        //         * try those without any coverage only if no models that are specific to this context are found.
    //        //         */
    //        //        if (result.size() == 0) {
    //        //
    //        //            query = Queries.select(NS.MODEL).restrict(
    //        //                    NS.HAS_OBSERVABLE,
    //        //                    Queries.select(NS.OBSERVABLE).restrict(NS.HAS_OBSERVABLE_CONCEPT,
    //        //                            Queries.select(observable)));
    //        //
    //        //            // TODO fix this - this would pick models for a different coverage too.
    //        //            // for (Pair<IProperty, ITopologicallyComparable<?>> cp : coverageProperties) {
    //        //            // query = query.restrict(Queries.no(Queries.has(cp.getFirst())));
    //        //            // }
    //        //
    //        //            result.add(Thinklab.get().requireKbox(Thinklab.DEFAULT_KBOX).query(query));
    //        //        }
    //        //
    //        //        if (result.size() > 0) {
    //        //            IModel ret = (IModel) result.get(0);
    //        //            if (_monitor != null) {
    //        //                _monitor.info("using model " + ret.getName() + " for " + observable, null);
    //        //            }
    //        //            return ret;
    //        //
    //        //        }
    //
    //        return null;
    //    }

    /**
     * Create default subject model for passed concept; if passed a non-null subject, pre-instantiate only the
     * functional properties that are not already observed, otherwise do all of them.
     * 
     * @param oobs
     * @param subject
     * @return
     * @throws ThinklabException
     */
    public IModel createDefaultSubjectModel(IObservable subject) throws ThinklabException {

        IModel model = null;

        /*
         * subject without any model associated: create default model from the concept adding dependencies for
         * its functional properties.
         */
        ArrayList<IDependency> toObserve = new ArrayList<IDependency>();
        for (IProperty p : subject.getType().getAllProperties()) {
            if (p.isFunctional()) {
                IConcept target = subject.getType().getPropertyRange(p).iterator().next();
                IDependency dep = new ObservingObject.Dependency(

                // ANY observation! FIXME - put DIRECT_OBSERVATION if it's a subject concept
                        new Observable(target, Thinklab.c(NS.INDIRECT_OBSERVATION), null, ""),
                        CamelCase.toLowerCase(target.getLocalName(), '-'), p, true, false, null);
                toObserve.add(dep);
            }
        }

        /*
         * we make a model even if nothing is there, just observing to the same subject.
         */
        ISubject rootSubject = _root;
        ArrayList<IObservable> sj = new ArrayList<IObservable>();
        sj.add(new Observable(subject.getType(), Thinklab.c(NS.DIRECT_OBSERVATION), rootSubject
                .getObservable().getType(), ""));
        model = new Model(sj, toObserve, rootSubject.getNamespace());
        ((Model) model).setId(rootSubject.getId());

        /*
         * add any dependency we have set beforehand unless they are there already and set the resolving model
         * to the one we were asked to
         * 
         * TODO remove - pass these ephemeral dependencies directly to the resolver.
         */
        for (IDependency dep : _dependencies) {
            ((Model) model).addDependency(dep);
        }

        return model;

    }

    // private IModel createDefaultSubjectModel(ISubject subject) throws ThinklabException {
    //
    // IModel model = null;
    //
    // /*
    // * subject without any model associated: create default model from the concept adding dependencies for
    // * its functional properties.
    // */
    // ArrayList<IDependency> toObserve = new ArrayList<IDependency>();
    // for (IProperty p : subject.getDirectType().getAllProperties()) {
    // if (p.isFunctional() && (subject != null && subject.get(p) == null)) {
    // IConcept target = subject.getDirectType().getPropertyRange(p).iterator().next();
    // IDependency dep = new ObservingObject.Dependency(new Observable(target,
    // Thinklab.c(NS.DIRECT_OBSERVATION), _root.getObservable().getType(), ""),
    // CamelCase.toLowerCase(target.getLocalName(), '-'), p, true, false, null);
    // toObserve.add(dep);
    // }
    // }
    //
    // /*
    // * we make a model even if nothing is there, just observing to the same subject.
    // */
    // ArrayList<IObservable> ob = new ArrayList<IObservable>();
    // ob.add(subject.getObservable());
    // model = new Model(ob, toObserve, _root.getNamespace());
    // ((Model) model).setId(_root.getId());
    //
    // /*
    // * add any dependency we have set beforehand unless they are there already and set the resolving model
    // * to the one we were asked to
    // */
    // for (IDependency dep : _dependencies) {
    // ((Model) model).addDependency(dep);
    // }
    //
    // return model;
    // }
    //
    /*
     * get a new or old node for the object in the given context. If the context is a conditional branch and
     * the node has been seen in an unconditional context or another branch, make the context unconditional
     * from that node down so it gets compiled in the main code.
     */
    public ProvenanceNode getNode(Object key, ResolutionContext ctx) {
        ProvenanceNode ret = ctx.modelGraph.add(key);
        return ret;
    }

    /**
     * Group together the dependencies of the passed object (model or observer) into groups to be resolved in
     * sequence. This will collect all consecutive data dependencies in one group and leave process and agent
     * dependencies by themselves. The first member of each returned pair is either NS.QUALITY, NS.PROCESS or
     * NS.THING to identify the group. * @return
     * 
     * @throws ThinklabException
     */
    public List<Pair<String, List<IDependency>>> groupDependencies(IObservingObject observer,
            List<IDependency> extraDependencies) throws ThinklabException {

        List<Pair<String, List<IDependency>>> ret = new ArrayList<Pair<String, List<IDependency>>>();

        ArrayList<IDependency> deps = new ArrayList<IObservingObject.IDependency>(observer.getDependencies());
        if (!isSandbox()) {
            deps.addAll(extraDependencies);
        }

        ArrayList<IDependency> last = null;
        for (IDependency dep : deps) {

            IObservable obs = getObservable(dep);

            if (NS.isProcess(obs)) {
                if (last != null) {
                    ret.add(new Pair<String, List<IDependency>>(NS.QUALITY, last));
                }
                ret.add(new Pair<String, List<IDependency>>(NS.PROCESS, Collections.singletonList(dep)));
                last = null;
            } else if (NS.isQuality(obs)) {
                if (last == null) {
                    last = new ArrayList<IObservingObject.IDependency>();
                }
                last.add(dep);
            } else if (NS.isObject(obs)) {
                if (last != null) {
                    ret.add(new Pair<String, List<IDependency>>(NS.QUALITY, last));
                }
                ret.add(new Pair<String, List<IDependency>>(NS.THING, Collections.singletonList(dep)));
                last = null;
            } else {

                /*
                 * shouldn't happen
                 */
                throw new ThinklabInternalErrorException(obs
                        + " cannot be categorized as quality, agent or process");
            }
        }

        if (last != null) {
            ret.add(new Pair<String, List<IDependency>>(NS.QUALITY, last));
        }

        return ret;
    }

    public IObservable getObservable(Object o) {

        if (o instanceof IObservable) {
            return (IObservable) o;
        }
        if (o instanceof IObserver) {
            return ((IObserver) o).getObservableConcept();
        }
        if (o instanceof IModel) {
            return ((IModel) o).getObservableConcept();
        }
        if (o instanceof Dependency) {
            return ((Dependency) o).getObservable();
        }
        if (o instanceof ISemanticObject<?>) {
            // hmmm FIXME this shouldn't happen any more (and will throw an exception)
            return (IObservable) ((ISemanticObject<?>) o).getDirectType();
        }
        return null;
    }

    // ----- resolver starts here -----
    /**
     * Resolve the subject to models and return a fully initialized subject hierarchy.
     * 
     * @throws ThinklabException
     */
    @Override
    public ICoverage resolve(Collection<String> scenarios) throws ThinklabException {

        CallTracer.indent("resolve()", this);
        Scale scale = (Scale) _root.getScale();

        _resolutionContext = ResolutionContext.forSubject(this, scenarios);
        if (_model == null) {
            _model = findSubjectModel(_root, _resolutionContext, false);
        }

        // resolve all dependencies to models and initialize all data dependencies and subjects.
        ICoverage ret = resolve(_model, _context, null, false, _resolutionContext.forSubjectModel(_model));

        CallTracer.unIndent();
        return ret == null ? new Coverage(scale, 0.0) : ret;
    }

    /**
     * Get a provenance graph with all the models already resolved in it. This is passed around to the
     * subordinate resolvers by the main resolve().
     * 
     * @return
     */
    ProvenanceGraph getProvenanceGraph() {

        ProvenanceGraph ret = new ProvenanceGraph(_monitor);
        for (IObservable m : models.keySet()) {
            ProvenanceNode model = models.get(m);
            ret.addVertex(model);
        }

        return ret;
    }

    /**
     * Entry point in the resolver after a subject model has been identified.
     */
    ICoverage resolve(IModel subjectModel, SubjectObserver context, IProperty pcontext,
            boolean initializeOnly, ResolutionContext ctx) throws ThinklabException {
        CallTracer.indent("resolve()", this, subjectModel, context, pcontext);

        IScale scale = _root.getScale();
        ISubject contextSubject = null;
        ISubjectAccessor accessor = (ISubjectAccessor) subjectModel.getAccessor(scale, _monitor);

        // run pre-initializer method if any, and reset scale that may have been modified
        if (accessor != null && _initializeSubject) {
            accessor.notifyModel(subjectModel);
            for (IObservable observable : subjectModel.getObservables()) {
                if (observable.is(Thinklab.c(NS.QUALITY))) {
                    accessor.notifyExpectedOutput(observable, observable.getObserver(),
                            observable.getFormalName());
                }
            }
            accessor.preinitialize(this, contextSubject, pcontext, _monitor);
            scale = _root.getScale();
        }

        /*
         * NOW we can build a backing dataset for the states - we didn't know the final scale until this point
         * - unless we already have one.
         */
        if (_dataset == null && beingResolved == null && _contextId >= 0)
            _dataset = new CFdataset(_root, _monitor, _contextId);

        if (_monitor.hasErrors() || _errors.size() != 0) {
            CallTracer.msg("found errors.");
            CallTracer.unIndent();
            return null;
        }

        // group all model dependencies according to type; resolve as appropriate for the dependency type.
        ProvenanceNode node = getNode(_model, ctx);
        ICoverage ret = new Coverage(ctx.scale, 1.0);
        ISubjectResolver r = null;
        if (_model.getObserver() != null) {
            /*
             * datasource and model dependencies are linked to the observer in the provenance graph, while
             * they are syntactically defined in the model.
             */
            r = new ObserverResolver(this, (Observer<?>) _model.getObserver(), _model.getDatasource(),
                    getDataDependencies(_model), node, ctx.defineState(_model), _monitor);
            ret = r.resolve(ctx.getScenarios());
        }
        // TODO there should be an else here, as no models with observers can have their own deps

        for (Pair<String, List<IDependency>> dg : groupDependencies(_model, _dependencies)) {
            String dependencyType = dg.getFirst();
            List<IDependency> dependencies = dg.getSecond();
            if (dependencyType.equals(NS.QUALITY)) {
                r = new StateResolver(this, _model, node, dependencies, ctx.newDependencyGroup(), _monitor,
                        _dataset);
            } else if (dependencyType.equals(NS.PROCESS)) {
                r = new ProcessResolver(this, ctx, ret, dependencies.get(0), _monitor);
            } else if (dependencyType.equals(NS.THING)) {
                r = new SubjectResolver(this, ctx, ret, dependencies.get(0), _monitor);
            }
            ret = r.resolve(ctx.getScenarios());
        }

        if (_errors.size() == 0 && !_monitor.hasErrors() && ret != null
                && ret.getCoverage() >= MIN_MODEL_COVERAGE) {
            // no errors happened, so finish processing.
            if (accessor != null && _initializeSubject) {
                // have the accessor perform data gathering if there is one
                accessor.initialize(_root, contextSubject, pcontext, _monitor);
                accessor.postinitialize(_root, contextSubject, pcontext, _monitor);
                // TODO/FIXME currently process() is not called if initialization is suppressed - probably
                // OK as is, but may require more thought.
                accessor.process(_root, contextSubject, pcontext, _monitor);
            }
            node.setCoverage(ret);

            // TODO what about models with infinite time scales?
        }

        CallTracer.unIndent();
        return ret;
    }

    @Override
    public ISubject run() throws ThinklabException {
        CallTracer.indent("run()", this);
        ITemporalExtent time = _root.getScale().getTime();
        if (time == null) {
            // the subject doesn't proceed through time
            CallTracer.msg("scale for root subject is atemporal");
        } else {
            CallTracer.msg("scale for root subject contains time dimension.");

            CallTracer.msg("creating ObservationController...");
            causalGraph = new TemporalCausalGraph<ISubject, IObservationGraphNode>();
            IObservationController controller = new ObservationController(causalGraph, _monitor,
                    time.getEnd());

            // add the root subject (and its children) to the controller's causal graph
            // insert the FIRST time period of the temporal extent
            ITimePeriod simulationTimePeriod = time.collapse();
            addSubjectToObservationGraph(this, simulationTimePeriod, controller, null);

            // TODO for now, only create one observation worker for single-threaded demonstration
            IObservationWorker worker = new ObservationWorker(controller);
            worker.run();
            // TODO should this return the controller? or root subject?
        }

        return _root;
    }

    /**
     * Recursively add the subject and all its dependencies to the observation graph (which is contained in
     * the controller parameter). Keep a list of what's been added already, to speed up performance, avoid
     * infinite recursion, and avoid duplicating the initial observation tasks for the agents.
     * 
     * @param subjectObserver
     * @param simulationTimePeriod
     * @param controller
     * @param alreadyAdded
     */
    private void addSubjectToObservationGraph(ISubjectObserver subjectObserver,
            ITimePeriod simulationTimePeriod, IObservationController controller,
            HashSet<ISubject> alreadyAdded) {
        CallTracer.indent("addSubjectToObservationGraph()", this, subjectObserver, simulationTimePeriod);
        ISubject subject = subjectObserver.getSubject();

        if (alreadyAdded == null) {
            alreadyAdded = new HashSet<ISubject>();
        }
        if (alreadyAdded.contains(subject)) {
            CallTracer.msg("subject has already been added! returning without doing any work.");
        } else {
            alreadyAdded.add(subject);

            // recursively add each other subject to the controller's causal graph
            // NOTE: do this first so that the observation queue causes dependencies to be observed before
            // dependents.
            // This would work either way, but this way doesn't cause sleep/wait at the start of an
            // observation.
            CallTracer.msg("adding subject's children...");
            for (ISubject child : subject.getSubjects()) {
                SubjectObserver childObserver = new SubjectObserver(child, _monitor, _refNamespace);
                addSubjectToObservationGraph(childObserver, simulationTimePeriod, controller, alreadyAdded);
            }

            CallTracer.msg("adding subject...");
            ITimePeriod agentCreationTimePeriod = simulationTimePeriod;
            ITemporalExtent subjectTime = subject.getScale().getTime();
            if (subjectTime != null) {
                // agent has its own temporal scale, so don't inherit the default. Start with extent 0.
                agentCreationTimePeriod = subjectTime.getExtent(0).collapse();
            }
            AgentState initialState = new AgentState(subject, agentCreationTimePeriod,
                    ((IActiveSubject) subject).getObjectStateCopy());
            controller.createAgent(subjectObserver, initialState, agentCreationTimePeriod, null, null, true);
        }
        CallTracer.unIndent();
    }

    /**
     * Default implementation for Subjects (non-agents) without any explicit view of time. This means that
     * they can be re-evaluated over time by repeating the same observations, but they do not perform any
     * logic or application of rules based on states or state transitions.
     */
    @Override
    public ITransition performTemporalTransitionFromCurrentState() {
        ITransition result;
        CallTracer.indent("performTemporalTransitionFromCurrentState()", this);

        // set the clock forward
        if (moveToNextTimePeriod() == null) {
            // we went beyond the agent's last temporal extent, so the agent dies
            result = new TemporalTransition(null, false);
        } else {
            // TODO re-evaluate states by repeating the observations.
            result = _root.reEvaluateStates(getCurrentTimePeriod());
        }

        CallTracer.unIndent();
        return result;
    }

    private ITimePeriod moveToNextTimePeriod() {
        if (getCurrentTimePeriod() == null) {
            // if we're already beyond the valid index numbers, then don't increment any more.
            return null;
        }

        // set the clock forward & invalidate the cache
        currentTemporalExtentIndex++;
        currentTimePeriodCache = null;

        // reload the cache and validate that we're still within the subject's temporal scale
        ITimePeriod result = getCurrentTimePeriod();
        if (result == null) {
            // TODO either extend the scale or the agent must die
            // currently, the agent dies by default
        }

        return result;
    }

    /**
     * get the TemporalExtent (which is also a TimePeriod) of the agent's current temporal location. Will
     * return null if the index has gone beyond the agent's temporal scale.
     * 
     * @return
     */
    private ITimePeriod getCurrentTimePeriod() {
        if (currentTimePeriodCache == null) {
            try {
                // FV ok it's caught, but I loathe NPE thrown and caught as if there's no tomorrow.
                ITemporalExtent time = _root.getScale().getTime();
                if (time != null) {
                    ITemporalExtent extent = time.getExtent(currentTemporalExtentIndex);
                    currentTimePeriodCache = extent.collapse();
                }
            } catch (Exception e) {
                // went beyond the agent's range. return null.
            }
        }
        return currentTimePeriodCache;
    }

    public List<Dependency> getDataDependencies(IModel model) {

        List<Dependency> ret = new ArrayList<Dependency>();

        for (IDependency d : model.getDependencies()) {
            if (d.getProperty().isLiteralProperty()) {
                ret.add((Dependency) d);
            }
        }

        return ret;
    }

    @Override
    public ISubjectObserver addObservation(IObservable observable, IProperty property, IMonitor monitor)
            throws ThinklabException {

        CallTracer.indent("addObservation()", this, observable, property);

        ((Observable) observable).setInherentType(_root.getObservable().getType());

        SubjectObserver ret = new SubjectObserver(this);
        ret.addDependency(observable, property, false);

        CallTracer.unIndent();
        return ret;
    }

    @Override
    public ISubject getSubject() {
        return _root;
    }

    public IScale getScale() {
        return _root.getScale();
    }

    public String getName() {
        return _root.getName();
    }

    @Override
    public String toString() {
        return "SO[" + _root.getId() + " (" + _root.getDirectType() + ")]";
    }

    public List<SubjectObserver> getSubjects() {
        return _subjects;
    }

    @Override
    public IResolutionStrategy getResolutionStrategy() {
        return _resolutionStrategy;
    }

    public IDataset getDataset() {
        return _dataset;
    }

    //    /*
    //     * the context of our resolution. Will be null if resolve() has not been called. Contains the provenance
    //     * graph and the model cache.
    //     */
    //    ResolutionContext getResolutionContext() {
    //        return _resolutionContext;
    //    }

    public boolean isBeingResolved(IObservable observableBeingResolved) {
        return (beingResolved != null && beingResolved.contains(observableBeingResolved));
    }

    public IMonitor getMonitor() {
        return _monitor;
    }

    public TemporalCausalGraph<ISubject, IObservationGraphNode> getCausalGraph() {
        return causalGraph;
    }

    public boolean enableSubjectAccessorInitialization(boolean init) {
        boolean ret = _initializeSubject;
        _initializeSubject = init;
        return ret;
    }

    public void setContextId(long contextId) {
        _contextId = contextId;
    }

    public void setMonitor(IMonitor monitor) {
        _monitor = monitor;
    }

    public void reset() {
        /*
         * Called when we start over with the same root subject, by observing it anew. For now, just flush the
         * dataset
         */
        if (_dataset != null) {
            _dataset.dispose();
        }
        _dataset = null;
    }

    public SubjectObserver getSandboxObserver(IModel m, Set<IObservable> observables, boolean optional) {
        SubjectObserver ret = new SubjectObserver(this, m, observables, optional);
        ret._isSandbox = true;
        return ret;
    }

    public boolean isSandbox() {
        return _isSandbox;
    }

    /**
     * Original namespace for root observation (subject or subject model). This sets the original
     * ranking priorities and white/blacklists for model resolution.
     * @return
     */
    public INamespace getReferenceNamespace() {
        return _refNamespace;
    }

    public ResolutionContext getResolutionContext() {
        return _resolutionContext;
    }
}
