package org.integratedmodelling.thinklab.modelling.resolver.obsolete;

import java.util.Collection;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.modelling.ICoverage;
import org.integratedmodelling.thinklab.api.modelling.IObservingObject.IDependency;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.debug.CallTracer;
import org.integratedmodelling.thinklab.modelling.lang.Observer;
import org.integratedmodelling.thinklab.modelling.resolver.ProvenanceGraph;
import org.integratedmodelling.thinklab.modelling.resolver.ResolutionContext;
import org.integratedmodelling.thinklab.modelling.resolver.ProvenanceGraph.ProvenanceNode;

public class DependencyResolver extends AbstractResolver {

    private final IDependency d;
    private final ProvenanceNode root;
    private ResolutionContext ctx;

    public DependencyResolver(SubjectObserver subjectObserver, IDependency d, ProvenanceNode root,
            ResolutionContext ctx, IMonitor monitor) {
        super(subjectObserver, monitor);
        this.d = d;
        this.root = root;
        this.ctx = ctx;
    }

    @Override
    public ICoverage resolve(Collection<String> scenarios) throws ThinklabException {
        CallTracer.indent("resolveDependency()", this, d, root);

        // FIXME now it only has an observable - must resolve it according to the content.
        IObservable observable = d.getObservable();
        AbstractResolver r;
        ICoverage result;

        if (observable.getObserver() != null) {
            ResolutionContext switchedContext = ctx.switchTo(subjectObserver.getObservable(d),
                    d.getProperty(), observable.getModel());
            r = new ObserverResolver(subjectObserver, (Observer<?>) observable.getObserver(), observable
                    .getModel().getDatasource(), subjectObserver.getDataDependencies(observable.getModel()),
                    root, switchedContext, monitor);
            result = r.resolve(scenarios);
        } else {
            ctx = ctx.switchTo(subjectObserver.getObservable(d), d.getProperty(), null);

            r = new ConceptResolver(subjectObserver, observable, root, ctx, monitor);
            result = r.resolve(scenarios);
            CallTracer.unIndent();
        }
        return result;
    }
}
