package org.integratedmodelling.thinklab.modelling.resolver.obsolete;

import java.util.Collection;

import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.knowledge.IProperty;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.modelling.IActiveSubject;
import org.integratedmodelling.thinklab.api.modelling.ICoverage;
import org.integratedmodelling.thinklab.api.modelling.IModel;
import org.integratedmodelling.thinklab.api.modelling.IObservingObject.IDependency;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.debug.CallTracer;
import org.integratedmodelling.thinklab.modelling.lang.Subject;
import org.integratedmodelling.thinklab.modelling.resolver.ResolutionContext;

public class SubjectResolver extends AbstractResolver {

    private final ICoverage coverage;
    private final IDependency dependency;
    private final ResolutionContext context;

    public SubjectResolver(SubjectObserver subjectObserver, ResolutionContext ctx, ICoverage coverage,
            IDependency dependency, IMonitor monitor) throws ThinklabException {
        super(subjectObserver, monitor);
        this.coverage = coverage;
        this.dependency = dependency;
        this.context = ctx;
    }

    @Override
    public ICoverage resolve(Collection<String> scenarios) throws ThinklabException {
        CallTracer.indent("resolve()", this, dependency, context.getScale());

        IProperty dependencyProperty = dependency.getProperty();
        ISubject rootSubject = subjectObserver.getSubject();
        SubjectObserver dependentSubjectObserver;

        CallTracer.indent("iterating over results of createDependentSubjects()...");
        Collection<Pair<SubjectObserver, IModel>> dependentSubjects = createDependentSubjects(dependency,
                context);

        for (Pair<SubjectObserver, IModel> ds : dependentSubjects) {
            dependentSubjectObserver = ds.getFirst();
            IModel subjectModel = ds.getSecond();
            CallTracer.indent("for() loop iteration", this, dependentSubjectObserver, subjectModel);
            /*
             * TODO/FIXME: this resolves the agent model independent of the context. If we want the existing
             * states to be data sources for the agents, we must put them in the new context's resolution
             * cache. This way each agent will observe their dependencies independently, potentially running
             * complex models but not constrained by the scale of the root subject.
             */
            //            ResolutionContext ctx = new ResolutionContext(subjectObserver, subjectModel,
            //                    dependentSubjectObserver.getScale(), dependentSubjectObserver.getProvenanceGraph(),
            //                    scenarios);

            ICoverage dcov = dependentSubjectObserver.resolve(subjectModel, subjectObserver,
                    dependencyProperty, true,
                    context.forDependentSubject(dependentSubjectObserver, subjectModel));

            if (dcov != null && dcov.getCoverage() > subjectObserver.MIN_MODEL_COVERAGE) {
                CallTracer
                        .msg("got adequate coverage. adding subject and intersecting with current coverage...");

                ((IActiveSubject) rootSubject).addSubject(dependentSubjectObserver, dependencyProperty);
                subjectObserver.getSubjects().add(dependentSubjectObserver);
                if (monitor != null) {
                    monitor.info("agent " + dependentSubjectObserver.getSubject().getId() + " created in "
                            + rootSubject.getId(), null);
                }

            } else if (monitor != null) {
                CallTracer.msg("got null or inadequate coverage. logging error...");

                monitor.error(addError("error resolving dependent agent "
                        + ((Subject) (dependentSubjectObserver.getSubject())).getId()));
            }
            CallTracer.unIndent();
        }
        CallTracer.unIndent();

        /*
         * TODO - what to do with the resolution strategy? Should probably insert the workflow from the
         * subjects
         */
        CallTracer.unIndent();
        return coverage;
    }
}
