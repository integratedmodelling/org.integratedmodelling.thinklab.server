package org.integratedmodelling.thinklab.modelling.resolver;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;

import org.integratedmodelling.collections.ImmutableList;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.listeners.INotification;
import org.integratedmodelling.thinklab.api.modelling.IModel;
import org.integratedmodelling.thinklab.api.modelling.IModelObject;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.api.modelling.resolution.IModelPrioritizer;
import org.integratedmodelling.thinklab.common.utils.StringUtils;
import org.integratedmodelling.thinklab.debug.DataRecorder;

/**
 * Result of a query. The get(n) method returns a model in order of insertion; the iterator returns
 * RANKED models. Model retrieval is lazy and the collection only holds a small amount of ranked metadata 
 * until a model is wanted. The model retriever should (eventually) be able to integrate models from 
 * any server, local or remote, creating the final models based on the server ID in model data.
 * 
 * @author ferdinando.villa
 *
 */
public class ObservationQueryResult extends ImmutableList<IModel> {

    IModelPrioritizer<ModelData> _comparator;
    ArrayList<ModelData>         _modelData = new ArrayList<ModelData>();
    boolean                      _sorted    = false;

    public class It implements Iterator<IModel> {

        Iterator<ModelData> _it;

        It() {
            if (!_sorted) {
                Collections.sort(_modelData, _comparator);
                _sorted = true;

                // add ranking metadata to provenance. TODO we may want to condition this to an option.
                addMetadata();

                // if debugging, print out the ranked alternatives.
                if (Thinklab.get().getNotificationLevel() == INotification.DEBUG) {
                    DataRecorder.debug("---- SCORES ------");
                    int n = 1;
                    for (ModelData md : _modelData) {
                        DataRecorder.debug(describeRanks(md, 2, n++));
                    }
                    DataRecorder.debug("------------------");
                }
            }
            _it = _modelData.iterator();
        }

        @Override
        public boolean hasNext() {
            return _it.hasNext();
        }

        @Override
        public IModel next() {
            return getModel(_it.next());
        }

        @Override
        public void remove() {
            throw new ThinklabRuntimeException("remove() in ObservationQueryResult iterator is unsupported");
        }
    }

    private IModel getModel(ModelData md) {

        IModelObject ret = null;

        if (md == null)
            return null;
        
        if (md.serverId != null) {
            // TODO - locate the server in session/auth data and issue client call, load remote projects
            // if necessary. After the call, the object should be available locally.
        }

        INamespace ns = Thinklab.get().getNamespace(md.namespaceId);
        if (ns != null)
            ret = ns.getModelObject(md.id);
        if (!(ret instanceof IModel))
            return null;

        if (md.dereifyingAttribute != null) {
            ret = ((IModel) ret).getAttributeObserver(md.dereifyingAttribute);
        }

        return (IModel) ret;
    }

    public void addMetadata() {
        // TODO Auto-generated method stub

    }

    public String describeRanks(ModelData md, int indent, int n) {

        String ret = "";
        String filler = StringUtils.spaces(indent);

        ret += filler + StringUtils.rightPad(n + ".", 4) + md.name + "\n";
        Map<String, Object> ranks = _comparator.getRanks(md);
        for (String s : _comparator.listCriteria()) {
            ret += filler + "  " + StringUtils.rightPad(s, 25) + " "
                    + ranks.get(s) + "\n";
        }

        return ret;
    }

    public ObservationQueryResult(IModelPrioritizer<ModelData> prioritizer) {
        _comparator = prioritizer;
    }

    @Override
    public boolean contains(Object arg0) {
        throw new ThinklabRuntimeException("contains() in ObservationQueryResult is unsupported");
    }

    @Override
    public IModel get(int arg0) {
        return getModel(_modelData.get(arg0));
    }

    @Override
    public Iterator<IModel> iterator() {
        return new It();
    }

    @Override
    public int size() {
        return _modelData.size();
    }

    @Override
    public Object[] toArray() {
        throw new ThinklabRuntimeException("toArray() in ObservationQueryResult is unsupported");
    }

    @Override
    public <T> T[] toArray(T[] arg0) {
        throw new ThinklabRuntimeException("toArray() in ObservationQueryResult is unsupported");
    }

    public void addModelData(ModelData md) {
        _modelData.add(md);
        _sorted = false;
    }

}
