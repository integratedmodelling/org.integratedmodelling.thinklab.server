package org.integratedmodelling.thinklab.modelling.resolver.obsolete;

import java.util.Collection;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.modelling.ICoverage;
import org.integratedmodelling.thinklab.api.modelling.IDataSource;
import org.integratedmodelling.thinklab.api.modelling.IModel;
import org.integratedmodelling.thinklab.debug.CallTracer;
import org.integratedmodelling.thinklab.modelling.resolver.Coverage;
import org.integratedmodelling.thinklab.modelling.resolver.ProvenanceGraph;
import org.integratedmodelling.thinklab.modelling.resolver.ResolutionContext;
import org.integratedmodelling.thinklab.modelling.resolver.ProvenanceGraph.ProvenanceNode;

public class DatasourceResolver extends AbstractResolver {

    private final IDataSource datasource;
    private final ProvenanceNode root;
    private final ResolutionContext resolutionContext;
    private final IModel model;

    public DatasourceResolver(SubjectObserver subjectObserver, IDataSource datasource, ProvenanceNode root,
            IModel model, ResolutionContext resolutionContext, IMonitor monitor) {
        super(subjectObserver, monitor);
        this.datasource = datasource;
        this.root = root;
        this.resolutionContext = resolutionContext;
        this.model = model;
    }

    @Override
    public ICoverage resolve(Collection<String> scenarios) throws ThinklabException {
        CallTracer.indent("resolve()", this, datasource, root);

        ProvenanceNode node = subjectObserver.getNode(datasource, resolutionContext);
        ICoverage coverage = new Coverage(resolutionContext.scale, 1.0);
        ICoverage dcoverag = new Coverage(datasource.getCoverage(), 1.0);

        coverage = dcoverag.and(coverage);

        resolutionContext.modelGraph.addEdge(node, root, resolutionContext.link);
        node.setCoverage(coverage);
        node.model = model;
        node.observer = model.getObserver();

        CallTracer.unIndent();
        return coverage;
    }

}
