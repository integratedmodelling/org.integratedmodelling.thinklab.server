package org.integratedmodelling.thinklab.modelling.resolver;

import java.util.ArrayList;

import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.collections.Triple;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.knowledge.IProperty;
import org.integratedmodelling.thinklab.api.modelling.ICoverage;
import org.integratedmodelling.thinklab.api.modelling.IExtent;
import org.integratedmodelling.thinklab.api.modelling.IScale;
import org.integratedmodelling.thinklab.api.modelling.ITopologicallyComparable;
import org.integratedmodelling.thinklab.modelling.lang.Scale;

public class Coverage implements ICoverage {

    /*
     * do not accept a model unless its coverage is greater than this.
     */
    private static double        MIN_MODEL_COVERAGE    = 0.01;

    /*
     * default: we accept models if they cover at least an additional 20% of the whole context
     */
    private static double        MIN_TOTAL_COVERAGE    = 0.20;

    /*
     * default: we stop adding models when we cover at least 95% of the whole context.
     */
    private static double        MIN_REQUIRED_COVERAGE = 0.95;

    IScale                       _scale;
    double                       _coverage             = 0.0;

    public static final Coverage EMPTY                 = new Coverage(null, 0.0);

    public static ICoverage FULL(IScale scale) {
        return new Coverage(scale, 1.0);
    }

    /*
     * the extents are initialized from the scale and change by either setting the
     * coverage's metadata or by and/or-ing with another coverage.
     */
    ArrayList<Triple<IExtent, ITopologicallyComparable<?>, Double>> _current = new ArrayList<Triple<IExtent, ITopologicallyComparable<?>, Double>>();

    /**
     * Create coverage initialized at 0
     * @param scale
     */
    public Coverage(IScale scale) {
        this(scale, 0.0);
    }

    @Override
    public boolean isEmpty() {
        return _coverage < MIN_MODEL_COVERAGE;
    }

    @Override
    public boolean isRelevant() {
        return _coverage > MIN_TOTAL_COVERAGE;
    }

    @Override
    public boolean isComplete() {
        return _coverage >= MIN_REQUIRED_COVERAGE;
    }

    public static void setMinimumModelCoverage(double d) {
        MIN_TOTAL_COVERAGE = d;
    }

    public static void setMinimumTotalCoverage(double d) {
        MIN_MODEL_COVERAGE = d;
    }

    public static void setSufficientTotalCoverage(double d) {
        MIN_REQUIRED_COVERAGE = d;
    }

    /**
     * Create the coverage of scale 'toCover' created by adding scale
     * 'covering'.
     * 
     * @param toCover
     * @param covering
     * @throws ThinklabException 
     */
    public Coverage(IScale toCover, IScale covering) throws ThinklabException {
        this(toCover);
        double co = 1.0;
        for (Triple<IExtent, ITopologicallyComparable<?>, Double> tc : _current) {
            IExtent its = ((Scale) covering).getExtent(tc.getFirst().getDomainConcept());
            if (its != null) {
                Pair<ITopologicallyComparable<?>, Double> topo = tc.getFirst().checkCoverage(its);
                tc.setSecond(topo.getFirst());
                tc.setThird(topo.getSecond());
                if (topo.getSecond() < co)
                    co = topo.getSecond();
            }
        }
        _coverage = co;

    }

    /**
     * Create coverage with predefined value
     * @param scale
     * @param coverage
     */
    public Coverage(IScale scale, double coverage) {
        _scale = scale;
        if (_scale != null) {
            for (IExtent e : ((Scale) _scale).getExtents()) {
                _current.add(new Triple<IExtent, ITopologicallyComparable<?>, Double>(e, null, coverage));
            }
        }
        _coverage = coverage;
    }

    public Coverage(ArrayList<Triple<IExtent, ITopologicallyComparable<?>, Double>> sav) {
        _current = sav;
        for (Triple<IExtent, ITopologicallyComparable<?>, Double> tc : _current) {
            if (tc.getThird() > _coverage) {
                _coverage = tc.getThird();
            }
        }
    }

    private Coverage(Coverage coverage) {

        for (Triple<IExtent, ITopologicallyComparable<?>, Double> tc : coverage._current) {
            _current.add(new Triple<IExtent, ITopologicallyComparable<?>, Double>(tc));
        }
        _coverage = coverage._coverage;
        _scale = coverage._scale;
    }

    /*
     * AND the coverage with the passed one. If there is no difference in the result, 
     * return this; otherwise return a new coverage.
     */
    @Override
    public Coverage and(ICoverage coverage) throws ThinklabException {

        if (coverage == null || coverage.getCoverage().equals(0)) {

            /*
             * empty coverage
             */
            return new Coverage(_scale);

        } else {

            /*
             * work on a copy
             */
            ArrayList<Triple<IExtent, ITopologicallyComparable<?>, Double>> sav = new ArrayList<Triple<IExtent, ITopologicallyComparable<?>, Double>>();
            for (Triple<IExtent, ITopologicallyComparable<?>, Double> tc : _current) {
                sav.add(new Triple<IExtent, ITopologicallyComparable<?>, Double>(tc));
            }
            if (andCoverage(((Coverage) coverage)._current, sav) != 0)
                return new Coverage(sav);
        }

        return this;

    }

    /*
     * OR the coverage with the passed one, only accepting the passed one as a contributor if
     * it determines a net improvement in coverage of at least minAcceptedImprovement. If the
     * coverage is not accepted, return this (getCoverage() will return the same value); otherwise
     * return a new coverage.
     */
    public Coverage or(ICoverage coverage, double minIncrementAccepted) throws ThinklabException {

        if (coverage != null) {
            if (((Coverage) coverage)._coverage == 1.0) {
                return new Coverage((Coverage) coverage);
            } else {

                /*
                 * work on a copy
                 */
                ArrayList<Triple<IExtent, ITopologicallyComparable<?>, Double>> sav = new ArrayList<Triple<IExtent, ITopologicallyComparable<?>, Double>>();
                for (Triple<IExtent, ITopologicallyComparable<?>, Double> tc : _current) {
                    sav.add(new Triple<IExtent, ITopologicallyComparable<?>, Double>(tc));
                }
                double cov = orCoverage(((Coverage) coverage)._current, sav);
                if (cov >= minIncrementAccepted) {
                    return new Coverage(sav);
                }
            }

        }

        return this;
    }

    /*
     * OR the coverage with the passed one, only accepting the passed one as a contributor if
     * it determines a net improvement in coverage of at least minAcceptedImprovement. If the
     * coverage is not accepted, return this (getCoverage() will return the same value); otherwise
     * return a new coverage.
     */
    public Coverage orIfRelevant(ICoverage coverage) throws ThinklabException {
        return or(coverage, MIN_TOTAL_COVERAGE);
    }

    /*
     * OR the coverage with the passed one, only accepting the passed one as a contributor if
     * it determines a net improvement in coverage of at least minAcceptedImprovement. If the
     * coverage is not accepted, return this (getCoverage() will return the same value); otherwise
     * return a new coverage.
     */
    @Override
    public Coverage or(ICoverage coverage) throws ThinklabException {
        return or(coverage, 0);
    }

    private double andCoverage(ArrayList<Triple<IExtent, ITopologicallyComparable<?>, Double>> md,
            ArrayList<Triple<IExtent, ITopologicallyComparable<?>, Double>> coverage)
            throws ThinklabException {

        double best = 0.0;

        for (int i = 0; i < coverage.size(); i++) {

            Triple<IExtent, ITopologicallyComparable<?>, Double> tc = coverage.get(i);
            IProperty p = tc.getFirst().getCoverageProperty();
            ITopologicallyComparable<?> cov = null;
            for (Triple<IExtent, ITopologicallyComparable<?>, Double> oc : md)
                if (oc.getFirst().getCoverageProperty().equals(p))
                    cov = oc.getSecond();

            /* 
             * If the model does not have the extent or it's empty, no cookie anyway.
             */
            if (cov == null) {
                coverage.get(i).setSecond(null);
                coverage.get(i).setThird(0.0);
            } else {

                /*
                 * if we had no previous value, the AND can only return nothing.
                 */
                ITopologicallyComparable<?> prev = tc.getSecond();
                if (prev != null) {

                    Pair<ITopologicallyComparable<?>, Double> newcov = tc.getFirst().checkCoverage(cov);
                    if (newcov.getSecond() > 0.0) {

                        /*
                         * intersect them
                         */
                        ITopologicallyComparable<?> ncov = cov.intersection(newcov.getFirst());

                        double change = ncov.getCoveredExtent() / newcov.getFirst().getCoveredExtent();

                        coverage.get(i).setSecond(ncov);
                        coverage.get(i).setThird(change);

                        if (best < change)
                            best = change;

                    } else {
                        coverage.get(i).setSecond(null);
                        coverage.get(i).setThird(0.0);
                    }
                }
            }
        }

        /*
         * return highest change among all extents.
         */
        return best;

    }

    Triple<IExtent, ITopologicallyComparable<?>, Double> getRecord(IExtent ext) {
        for (Triple<IExtent, ITopologicallyComparable<?>, Double> tc : _current) {
            if (tc.getFirst().getCoverageProperty().equals(ext.getCoverageProperty())) {
                return tc;
            }
        }
        return null;
    }

    /*
     * add the metadata to the extents in coverage using the passed logical connector (AND or OR). Return the 
     * highest coverage change obtained in the process. 
     * 
     */
    private double orCoverage(ArrayList<Triple<IExtent, ITopologicallyComparable<?>, Double>> md,
            ArrayList<Triple<IExtent, ITopologicallyComparable<?>, Double>> coverage)
            throws ThinklabException {

        double improvement = 0.0;
        double best = 0.0;

        for (int i = 0; i < coverage.size(); i++) {

            Triple<IExtent, ITopologicallyComparable<?>, Double> tc = coverage.get(i);
            IProperty p = tc.getFirst().getCoverageProperty();

            ITopologicallyComparable<?> cov = null;
            for (Triple<IExtent, ITopologicallyComparable<?>, Double> oc : md)
                if (oc.getFirst().getCoverageProperty().equals(p))
                    cov = oc.getSecond();

            /* 
             * If the model does not have the extent, leave coverage at 1. 
             * This is a second choice model that with the search strategy in ModelQuery should only
             * show up here if no models with all extents were found.
             */
            if (cov != null) {

                /*
                 * if we had a previous value, merge it with the current and try that;
                 * otherwise use the new one alone
                 */
                ITopologicallyComparable<?> prev = tc.getSecond();
                if (prev != null) {
                    cov = prev.union(cov);
                }
                Pair<ITopologicallyComparable<?>, Double> newcov = tc.getFirst().checkCoverage(cov);
                improvement = newcov.getSecond() - tc.getThird();
                cov = newcov.getFirst();

                /*
                 * if enough improvement, make new extents current
                 */
                if (improvement != 0) {
                    coverage.get(i).setSecond(cov);
                    coverage.get(i).setThird(newcov.getSecond());

                    if (best < improvement)
                        best = improvement;
                }
            }
        }

        /*
         * return highest change among all extents.
         */
        return best;
    }

    @Override
    public Double getCoverage() {
        return _coverage;
    }

    @Override
    public String toString() {
        return "coverage (" + _current.size() + " ext) = " + _coverage;
    }

}
