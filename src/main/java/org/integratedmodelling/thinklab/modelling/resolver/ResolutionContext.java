package org.integratedmodelling.thinklab.modelling.resolver;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.lang.LogicalConnector;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IExpression;
import org.integratedmodelling.thinklab.api.knowledge.IProperty;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.modelling.ICoverage;
import org.integratedmodelling.thinklab.api.modelling.IDataSource;
import org.integratedmodelling.thinklab.api.modelling.IModel;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.api.modelling.IObjectSource;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.IObservingObject.IDependency;
import org.integratedmodelling.thinklab.api.modelling.IScale;
import org.integratedmodelling.thinklab.api.modelling.IState;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.api.modelling.resolution.IProvenance;
import org.integratedmodelling.thinklab.api.modelling.resolution.IResolutionContext;
import org.integratedmodelling.thinklab.api.modelling.resolution.IResolutionStrategy;
import org.integratedmodelling.thinklab.api.time.ITemporalExtent;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.debug.CallTracer;
import org.integratedmodelling.thinklab.modelling.lang.Metadata;
import org.integratedmodelling.thinklab.modelling.lang.Scale;
import org.integratedmodelling.thinklab.modelling.resolver.ProvenanceGraph.DependencyEdge;
import org.integratedmodelling.thinklab.modelling.resolver.ProvenanceGraph.ProvenanceNode;

/*
 * resolution context of object X contains a scale, the link X will be target of in the provenance diagram,
 * and optionally the semantic context of the resolution, i.e. the property that represents the link and if
 * that isn't null, the concept it links to in that capacity.
 * 
 * TODO rename all the methods that return sub-contexts to something sensible and address any redundancy or
 * stupidity in them.
 */
public class ResolutionContext implements IResolutionContext {

    enum Type {
        ROOT,
        SUBJECT,
        OBSERVABLE,
        MODEL,
        OBSERVER,
        DEPENDENCY,
        CONDITIONAL_DEPENDENCY,
        DATASOURCE,
        OBJECTSOURCE,
        GROUP
    };

    Type                         type;
    ISubject                     subject;
    IObservable                  observable;
    IProperty                    property;
    IObserver                    observer;
    IModel                       model;
    IDataSource                  datasource;
    IObjectSource                objectsource;
    ISubject                     contextSubject;
    IDependency                  dependency;
    // mandatorily not null
    IMonitor                     monitor;

    // the scale we keep harmonizing as we resolve; exclude time given that we resolve for initialization
    IScale                       scale;

    // the time scale, if any, so we can tune the model retrieval for currency and coverage.
    ITemporalExtent              time;

    // the link we will use to link to root if resolved
    DependencyEdge               link;
    // the node we get for our target when resolution is accepted by the resolver, and the root node for
    // our children contexts.

    ProvenanceNode               node;
    // provenance graph that gets built along the resolution chain, passed around to children
    ProvenanceGraph              modelGraph;

    // TODO fill these in
    Collection<String>           nsWhitelist        = null;
    Collection<String>           nsBlacklist        = null;

    // scenarios are defined externally on the root context and passed around unmodified
    HashSet<String>              scenarios          = null;

    // attributes extracted from the context, used when the kbox is consulted for ranking of options.
    Set<IConcept>                attributes         = null;

    // the namespace of reference for the context.
    INamespace                   namespace;

    // these are not dealt with directly, but the resolver may add metadata, in which
    // case they're passed on to the provenance nodes when they're built.
    Metadata                     metadata           = null;

    /*
     * this should only be true when we're resolving a conditional branch. It will color all nodes as
     * conditional unless they have been seen in an unconditional branch too.
     */
    boolean                      isOptional;

    /*
     * A mismatch between attribute semantics may make a context unsatisfiable, which prunes
     * a resolution subtree instantly.
     */
    boolean                      isUnsatisfiable    = false;

    /*
     * connector for multiple coverage merges in resolution of dependencies. Default is
     * AND, but some contexts (namely conditional dependencies) will use OR. 
     */
    LogicalConnector             connector          = LogicalConnector.INTERSECTION;

    /*
     *  model cache, passed around; holds node->model correspondences as they're accepted.
     *  We hold all the resolved observables with the respective model and coverage.
     */
    HashMap<IObservable, IModel> models;
    /*
     * coverage cache for models. We just pass this around.
     */
    HashMap<IModel, ICoverage>   coverages;

    /*
     * overall coverage, updated by the resolver after each step.
     */
    Coverage                     coverage;

    /*
     * our parent node which will accept() us if the merged coverage is sufficient.
     */
    ResolutionContext            parent;

    /*
     * if we're representing a conditional dependency, the condition that comes with it (may be 
     * null even for a conditional dependency).
     */
    IExpression                  condition;

    /*
     * if we're representing a conditional dependency, the order of definition of the
     * condition.
     */
    int                          conditionIndex     = -1;

    /*
     * cache of model IDs being resolved to avoid infinite loops when a model matches
     * its own observable.
     */
    HashSet<String>              resolving          = new HashSet<String>();

    /*
     * The final product of the resolution is a resolution strategy that will be
     * executed to initialize the subject if the resulting coverage is sufficient.
     */
    ResolutionStrategy           resolutionStrategy = new ResolutionStrategy();

    /*
     * ONLY for "child" contexts - automatically sets the parent context and
     * prepares a new provenance graph, to be merged with the parent's at finish() if coverage
     * is sufficient. Copies all common info and leave everything else undefined.
     * 
     * Constructor using this must set the link and add the provenance node as 
     * the context's node, then link it as necessary. Each context can only have
     * one root node.
     */
    private ResolutionContext(ResolutionContext ctx, Type type) {

        CallTracer.indent("" + type);

        this.models = new HashMap<IObservable, IModel>(ctx.models);

        if (ctx.attributes != null) {
            this.attributes = new HashSet<IConcept>(ctx.attributes);
        }

        this.modelGraph = new ProvenanceGraph(ctx.monitor);
        this.scenarios = ctx.scenarios;
        this.nsWhitelist = ctx.nsWhitelist;
        this.nsBlacklist = ctx.nsBlacklist;
        this.scale = ctx.scale;
        this.monitor = ctx.monitor;
        this.namespace = ctx.namespace;
        // this.tasks = ctx.tasks;
        this.coverages = ctx.coverages;
        this.isOptional = ctx.isOptional;
        this.resolving.addAll(ctx.resolving);
        this.time = ctx.time;

        // the model stays unless redefined later
        this.model = ctx.model;
        this.parent = ctx;
        this.type = type;
    }

    /**
     * Call after resolution of this context to merge in results into the father context. If the
     * merged coverage is acceptable, merge provenance and model cache and link the root node to
     * the father's.
     * 
     * @param ctx
     * @return
     * @throws ThinklabException 
     */
    @Override
    public ICoverage finish() throws ThinklabException {

        if (coverage == null) {
            // we're a group with no members or something that hasn't accepted or resolved anything.
            // Also happens for previous model observers. Needs checking.
            coverage = type.equals(Type.MODEL) || type.equals(Type.SUBJECT) || type.equals(Type.OBSERVER) ? new Coverage(scale, 1.0)
                    : Coverage.EMPTY;
        }

        if (coverage.isEmpty()) {
            // an empty optional dependency is OK, doesn't alter the parent's coverage
            if (isOptional) {
                if (dependency == null) {
                    // monitor.warn("there are unsatisfied optional dependencies");
                } else {
                    monitor.warn("optional dependency on " + dependency.getObservable().getType()
                            + " is unsatisfied");
                }
                CallTracer.unIndent();
                return coverage;
            }
            CallTracer.unIndent();
            return coverage;
        }

        CallTracer.unIndent();
        return parent.accept(this);

    }

    /**
     * Root resolution context. All others should be created using public methods.
     * @param subject
     * @param scenarios
     * @return
     */
    public static ResolutionContext root(IMonitor monitor, Collection<String> scenarios) {
        ResolutionContext ret = new ResolutionContext(monitor, scenarios);
        ret.type = Type.ROOT;
        return ret;
    }

    public ResolutionContext(IMonitor monitor, Collection<String> scenarios) {
        this.models = new HashMap<IObservable, IModel>();
        this.coverages = new HashMap<IModel, ICoverage>();
        this.monitor = monitor;
        this.scenarios = new HashSet<String>();
        for (String s : scenarios) {
            this.scenarios.add(s);
        }
        this.modelGraph = new ProvenanceGraph(monitor);
        // this.tasks = new ArrayList<IResolutionTask>();
    }

    @Override
    public Set<IConcept> getObservableClosure(IObservable observable) throws ThinklabException {

        Set<IConcept> observables = new HashSet<IConcept>();
        if (this.observable != null && this.property != null) {
            for (IConcept cc : this.observable.getType().getPropertyRange(this.property)) {
                observables.add(cc);
            }
        } else {
            observables.add(observable.getType());
        }

        if (observables.size() == 0) {
            observables.add(observable.getType());
        }

        return observables;
    }

    @Override
    public IScale getScale() {
        return scale;
    }

    @Override
    public Collection<String> getScenarios() {
        return scenarios;
    }

    @Override
    public IModel getModel() {
        return model;
    }

    @Override
    public INamespace getNamespace() {
        return namespace;
    }

    @Override
    public void setMonitor(IMonitor monitor) {
        this.monitor = monitor;
    }

    @Override
    public IMonitor getMonitor() {
        return monitor;
    }

    @Override
    public ISubject getSubject() {
        return subject;
    }

    @Override
    public IProperty getPropertyContext() {
        return property;
    }

    @Override
    public void forceScale(IScale scale) {
        this.scale = scale;
    }

    @Override
    public IProvenance getProvenance() {
        return modelGraph;
    }

    @Override
    public ICoverage getCoverage() {
        return coverage;
    }

    @Override
    public ISubject getContextSubject() {
        return contextSubject;
    }

    @Override
    public void setMetadata(String key, Object value) {
        if (metadata == null) {
            metadata = new Metadata();
        }
        metadata.put(key, value);
    }

    @Override
    public ResolutionContext forObservable(IObservable observable) {

        ResolutionContext ret = new ResolutionContext(this, Type.OBSERVABLE);
        ret.subject = subject;
        ret.observable = observable;
        ret.attributes = mergeTraits(this.attributes, observable);

        trace(ret);

        return ret;
    }

    /*
     * add any traits that weren't there already, update those that were there. If any
     * traits are incompatible, set the context to unsatisfiable.
     */
    private Set<IConcept> mergeTraits(Set<IConcept> oldt, IObservable observable) {

        Set<IConcept> ret = new HashSet<IConcept>();
        try {

            Pair<IConcept, Collection<IConcept>> ast = NS.separateAttributes(observable.getType());
            for (IConcept attribute : ast.getSecond()) {
                IConcept baset = NS.getBaseParentTrait(attribute);
                if (oldt != null) {
                    for (IConcept c : oldt) {
                        if (!c.equals(attribute) && c.is(baset)) {
                            isUnsatisfiable = true;
                            return ret;
                        }
                    }
                }
                /*
                 * it gets here only if oldt didn't have it or had exactly the same.
                 */
                ret.add(attribute);
            }

            /*
             * anything not added that was in oldt is safe for addition at this point
             */
            if (oldt == null) {
                return ret.isEmpty() ? null : ret;
            }
            ret.addAll(oldt);

        } catch (ThinklabValidationException e) {

            // better empty than wrong
            ret.clear();
        }

        return ret;
    }

    @Override
    public ResolutionContext forDependency(IDependency dependency) {
        ResolutionContext ret = new ResolutionContext(this, Type.DEPENDENCY);
        ret.subject = subject;
        ret.model = this.model;
        ret.observer = this.observer;
        ret.datasource = this.datasource;
        ret.dependency = dependency;
        ret.property = dependency.getProperty();
        ret.isOptional = this.isOptional || dependency.isOptional();

        // no link - this will collect node and link from downstream and pass them on to the parent.

        trace(ret);

        return ret;
    }

    private static void trace(ResolutionContext ret) {

        if (ret.node != null) {
            CallTracer.msg("N: " + ret.node);
        }
        if (ret.link != null) {
            CallTracer.msg("L: " + ret.link.describeType());
        }

    }

    private boolean isObserverContext() {
        if (this.type.equals(Type.OBSERVER))
            return true;
        if (this.type.equals(Type.MODEL) || this.type.equals(Type.DEPENDENCY))
            return false;
        return parent.isObserverContext();
    }

    @Override
    public ResolutionContext forModel(IModel model) {

        ResolutionContext ret = new ResolutionContext(this, Type.MODEL);
        ret.subject = subject;
        ret.model = model;
        ret.namespace = model.getNamespace();
        ret.node = ret.modelGraph.getNode(model);
        ret.attributes = mergeTraits(this.attributes, model.getObservable());
        ret.resolving.add(model.getName());

        // if data model, link to parent is "provides"; otherwise we are an entry point and we don't want a
        // link. Dependency must be completed upstream by adding formal name and conditions if any.
        if (model.getObserver() != null) {
            ret.link = new DependencyEdge(isObserverContext() ? DependencyEdge.RESOLVES
                    : DependencyEdge.DEPENDENCY, "", model.getObservable());
        }

        trace(ret);

        return ret;
    }

    @Override
    public ResolutionContext forSubject(ISubject subject) {

        ResolutionContext ret = new ResolutionContext(this, Type.SUBJECT);

        // null in root context
        ret.contextSubject = this.subject;
        ret.subject = subject;
        ret.scale = ((Scale) (subject.getScale())).getNonTemporallyDynamicScale();
        ret.time = subject.getScale().getTime();
        ret.namespace = subject.getNamespace();
        ret.attributes = mergeTraits(this.attributes, subject.getObservable());

        // whatever is in there, we reuse; use the observer's observable, which is what was
        // matched, not the dependency's.
        for (IState s : subject.getStates()) {
            ret.models.put(s.getObserver().getObservable(), new StateModel(s.getObservable(), s));
        }

        // no node - we use the subject model as the entry point for the workflow.

        trace(ret);

        return ret;
    }

    @Override
    public ResolutionContext forObserver(IObserver observer, IDataSource modelDatasource) {
        ResolutionContext ret = new ResolutionContext(this, Type.OBSERVER);
        ret.subject = subject;
        ret.model = this.model;
        ret.observer = observer;
        ret.datasource = modelDatasource;
        ret.node = ret.modelGraph.getNode(observer);
        ret.attributes = mergeTraits(this.attributes, observer.getObservable());

        // no need for coverage, observer always has a datasource or a mediated observer.
        // observers define the state of models.
        ret.link = new DependencyEdge(DependencyEdge.DEFINE_STATE, "", observer.getObservable());

        trace(ret);

        return ret;
    }

    @Override
    public ResolutionContext forDatasource(IDataSource datasource) throws ThinklabException {

        ResolutionContext ret = new ResolutionContext(this, Type.DATASOURCE);
        ret.subject = subject;
        ret.datasource = datasource;
        ret.model = this.model;
        ret.observer = this.observer;

        ret.node = ret.modelGraph.getNode(datasource);
        // non-conditional node with no further resolution, so it doesn't go through finish() and
        // gets added here.
        ret.modelGraph.add(ret.node);

        // datasources are interpreted by observers.
        ret.link = new DependencyEdge(DependencyEdge.INTERPRET_AS, "", observer.getObservable());

        // initial coverage is the intersection of the datasource with the scale.
        ret.coverage = new Coverage(scale, 1.0);
        ret.coverage = ret.coverage.and(new Coverage(datasource.getCoverage()));

        trace(ret);

        return ret;
    }

    @Override
    public ResolutionContext forObjectSource(IObjectSource objectsource) throws ThinklabException {

        final ResolutionContext ret = new ResolutionContext(this, Type.OBJECTSOURCE);
        ret.subject = subject;
        ret.objectsource = objectsource;
        ret.model = model;
        ret.observer = observer;

        ret.node = ret.modelGraph.getNode(objectsource);
        ret.node.model = model;

        // non-conditional node with no further resolution, so it doesn't go through finish() and
        // gets added here.
        ret.modelGraph.add(ret.node);

        // coverage is the intersection of the datasource with the scale.
        ret.coverage = new Coverage(scale);
        ret.coverage = ret.coverage.and(new Coverage(objectsource.getCoverage()));

        trace(ret);

        return ret;
    }

    @Override
    public ResolutionContext forMediatedObserver(IObserver observer) {
        ResolutionContext ret = new ResolutionContext(this, Type.OBSERVER);
        ret.subject = subject;
        ret.model = model;
        ret.observer = observer;
        ret.datasource = datasource;
        ret.link = new DependencyEdge(DependencyEdge.MEDIATE_TO, "", observer.getObservable());
        ret.observable = observer.getObservable();
        ret.node = ret.modelGraph.getNode(observer);

        trace(ret);

        return ret;
    }

    @Override
    public ResolutionContext forCondition(IExpression expression, int n) {

        /*
         * FIXME - this should merely color the downstream dependency link with a
         * conditional flavor and pass the expression and index, otherwise works like 
         * a dependency, so it should have no link and node. and the condition/index
         * should be in the context.
         */
        ResolutionContext ret = new ResolutionContext(this, Type.CONDITIONAL_DEPENDENCY);
        ret.subject = subject;
        ret.model = model;
        ret.observer = observer;
        ret.condition = expression;
        ret.conditionIndex = n;

        trace(ret);

        return ret;
    }

    @Override
    public ResolutionContext forGroup(LogicalConnector connector) {
        ResolutionContext ret = new ResolutionContext(this, Type.GROUP);
        ret.subject = subject;
        ret.connector = connector;
        ret.coverage = (connector.equals(LogicalConnector.INTERSECTION) ? new Coverage(ret.scale, 1.0)
                : Coverage.EMPTY);

        // act as a worker for the parent, so take its node to link to each child that satisfies
        // the rule for group inclusion.
        ret.node = this.node;

        // will use the child link to the parent node; we have no link of our own, but will link
        // the parent's to the child's at each accept()

        trace(ret);

        return ret;
    }

    @Override
    public IModel getModelFor(IObservable observable) {

        /*
         * TODO/CHECK
         * this will not match, e.g., an indirect observation with a measurement - which may be a problem
         * although usually only for manually observed concepts.
         */

        return models.get(observable);
    }

    @Override
    public ICoverage accept(IResolutionContext chld) throws ThinklabException {

        ResolutionContext child = (ResolutionContext) chld;
        boolean noOp = false;

        boolean linkToParentSubject = this.type == Type.MODEL && NS.isProcess(this.model.getObservable());

        ProvenanceNode source = linkToParentSubject ? getParentNode() : this.node;
        ProvenanceNode target = child.node;
        DependencyEdge dlink = linkToParentSubject ? new DependencyEdge(DependencyEdge.DEPENDENCY, "", child.observable)
                : child.link;

        /*
         * determine coverage
         */
        if (type.equals(Type.OBSERVABLE)) {

            /*
             * child can only be a model
             */
            if (coverage == null) {

                coverage = child.coverage;

                // may have been seen before
                if (!models.containsValue(child.model)) {

                    monitor.info(child.model.getName() + " satisfies "
                            + NumberFormat.getPercentInstance().format(coverage.getCoverage()) + " of "
                            + this.observable.getType(), null);

                    /*
                     * store in cache
                     */
                    models.put(this.observable, child.model);
                    coverages.put(child.model, coverage);
                }

            } else {

                /*
                 * the child is a resolved model. Check if it adds enough coverage to be useful; if not, do nothing.
                 */
                double tcov = coverage.getCoverage();
                Coverage cv = coverage.orIfRelevant(child.coverage);
                double additional = cv.getCoverage() - tcov;
                if (additional > 0) {

                    monitor.info(
                            child.model.getName() + " adds "
                                    + NumberFormat.getPercentInstance().format(additional) + " of "
                                    + this.observable.getType(), null);

                    coverage = cv;
                    // link up the model into a common one.
                    if (node == null) {
                        node = child.node;
                    } else {
                        /*
                         * TODO
                         * substitute the model in the node with the merged model for the
                         * additional coverage.
                         */
                    }

                    /*
                     * TODO store merged model in cache in lieu of previous
                     * store in cache
                     */

                } else {
                    noOp = true;
                }

            }

        } else if (type.equals(Type.GROUP)) {

            /*
             * merge according to the connector we are using
             */
            if (connector.equals(LogicalConnector.INTERSECTION)) {
                coverage = coverage.and(child.coverage);
            } else {
                coverage = coverage.or(child.coverage);
            }

        } else {
            coverage = child.coverage;
        }

        /*
         * merge the child's context if we're using it.
         */
        if (!coverage.isEmpty()) {

            modelGraph.merge(child.modelGraph);
            models.putAll(child.models);
            resolutionStrategy.merge(child.resolutionStrategy);

            if (!noOp) {
                if (source != null) {
                    modelGraph.add(source);
                }
                if (source != null && target != null && dlink != null) {
                    modelGraph.link(target, source, dlink);
                    CallTracer.msg("Linked " + dlink.describeType());
                }

                /*
                 * if we're in a group and we have all the coverage we need, we also want to merge the found models with our own
                 * cache, so the next dependency can find them.
                 * 
                 * HMMM may not work - what happens to the next in line for the same observable in a union? Must be done when coverage is sufficient
                 */
                // if (parent != null && parent.type.equals(Type.GROUP)) {
                // parent.models.putAll(models);
                // }
            }

            /*
             * float these if we're just a linkpoint to the parent.
             */
            if (node == null && link == null) {

                node = child.node;
                link = child.link;

                /*
                 * "color" the link if our context contains anything relevant to it.
                 */
                if (link != null) {
                    if (dependency != null) {
                        link.formalName = dependency.getFormalName();
                        link.property = dependency.getProperty();
                    }
                    link.condition = condition;
                    link.conditionIndex = conditionIndex;
                }
            }

            /*
             * inherit the model if we have resolved a new one in the child
             */
            if (model == null) {
                model = child.model;
            }

            /*
             * float the scale (only to the root context) or harmonize it with 
             */
            if (scale == null) {
                scale = child.scale;
            } else {

                /*
                 * TODO harmonize the common scale with that of the object we have just resolved.
                 */
                scale = scale.harmonize(child.scale);
            }
        }

        /*
         * if we've been given metadata by the resolver, transfer them to
         * the node we're handling.
         */
        if (metadata != null && node != null) {
            node.getMetadata().merge(metadata, true);
        }

        return coverage;

    }

    /**
     * Get the first parent node available. Used when we must connect a process model's dependencies to
     * the subject they inhere to.
     * 
     * @return
     */
    private ProvenanceNode getParentNode() {
        ResolutionContext ctx = parent;
        while (ctx != null && ctx.node == null) {
            ctx = ctx.parent;
        }
        return ctx == null ? null : ctx.node;
    }

    public boolean isRoot() {
        return type.equals(Type.ROOT);
    }

    public String toString() {
        return "{" + type + " # " + (coverage == null ? "null" : coverage.getCoverage()) + " # " + subject
                + "/" + model + "/" + observer + "}";
    }

    public boolean isRootSubjectObservable() {
        return type.equals(Type.OBSERVABLE) && parent.type.equals(Type.SUBJECT)
                && parent.parent.type.equals(Type.ROOT);
    }

    @Override
    public Set<IConcept> getTraits() {
        // don't return null, make it easy for your friends.
        return attributes == null ? new HashSet<IConcept>() : attributes;
    }

    /*
     * resolve the current context with the model passed, which is already in the
     * provenance graph.
     */
    public ICoverage resolve(IModel model) throws ThinklabException {
        coverage = (Coverage) coverages.get(model);
        modelGraph.add(modelGraph.getNode(model));
        return finish();
    }

    @Override
    public boolean isOptional() {
        return isOptional;
    }

    public boolean isResolving(IModel m) {
        return resolving.contains(m.getName());
    }

    public void reset() {

        for (ResolutionContext c = this; c != null; c = c.parent) {
            c.modelGraph = new ProvenanceGraph(monitor);
            for (IState s : subject.getStates()) {
                c.models.put(s.getObserver().getObservable(), new StateModel(s.getObservable(), s));
            }
        }
    }

    @Override
    public IResolutionStrategy getResolutionStrategy() {
        return resolutionStrategy;
    }
}
