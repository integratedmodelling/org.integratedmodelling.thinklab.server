package org.integratedmodelling.thinklab.modelling.resolver;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.integratedmodelling.thinklab.api.modelling.ICoverage;
import org.integratedmodelling.thinklab.api.modelling.IModel;
import org.integratedmodelling.thinklab.api.modelling.resolution.IProvenance;
import org.integratedmodelling.thinklab.api.modelling.resolution.IResolutionStrategy;
import org.integratedmodelling.thinklab.api.modelling.resolution.IWorkflow;
import org.integratedmodelling.thinklab.api.modelling.scheduling.ITransition;

public class ResolutionStrategy implements IResolutionStrategy {

    List<Step> _steps = new ArrayList<Step>();

    public static class ResolutionStep implements Step {

        IProvenance provenance;
        IWorkflow   workflow;
        IModel      model;
        ICoverage   coverage;

        public ResolutionStep(IProvenance provenance, IWorkflow workflow) {
            this.provenance = provenance;
            this.workflow = workflow;
        }

        public ResolutionStep(IModel model) {
            this.model = model;
        }

        @Override
        public IModel getModel() {
            return model;
        }

        @Override
        public IProvenance getProvenance(int step) {
            return provenance;
        }

        @Override
        public IWorkflow getWorkflow(int step) {
            return workflow;
        }

        @Override
        public ICoverage getCoverage() {
            return coverage;
        }

    }

    @Override
    public int getStepCount() {
        return _steps.size();
    }

    // @Override
    // public IProvenance getProvenance(int step) {
    // return _steps.get(step).getFirst();
    // }
    //
    // @Override
    // public IWorkflow getWorkflow(int step) {
    // return _steps.get(step).getSecond();
    // }

    public void addStep(IProvenance provenance, IWorkflow workflow) {
        _steps.add(new ResolutionStep(provenance, workflow));
    }

    public void addStep(IModel model) {
        _steps.add(new ResolutionStep(model));
    }

    // public GraphVisualization visualizeProvenance(int step) {
    //
    // ProvenanceGraph pg = (ProvenanceGraph) getProvenance(step);
    // return pg.vertexSet().size() > 1 ? pg.visualize() : null;
    // }
    //
    // public GraphVisualization visualizeWorkflow(int step) {
    //
    // Workflow wf = (Workflow) getWorkflow(step);
    // return wf.vertexSet().size() > 1 ? wf.visualize() : null;
    // }
    //
    // public List<Pair<GraphVisualization, GraphVisualization>> visualize() {
    //
    // List<Pair<GraphVisualization, GraphVisualization>> ret = new ArrayList<Pair<GraphVisualization,
    // GraphVisualization>>();
    //
    // int i = 1;
    // for (Pair<IProvenance, IWorkflow> zio : _steps) {
    // GraphVisualization pg = ((ProvenanceGraph) (zio.getFirst())).visualize();
    // GraphVisualization wg = ((Workflow) (zio.getSecond())).visualize();
    // pg.name = "Provenance #" + i;
    // wg.name = "Dataflow #" + i;
    // ret.add(new Pair<GraphVisualization, GraphVisualization>(pg, wg));
    // i++;
    // }
    //
    // return ret;
    // }

    @Override
    public Iterator<Step> iterator() {
        return _steps.iterator();
    }

    @Override
    public Step getStep(int i) {
        return _steps.get(i);
    }

    @Override
    public boolean execute(ITransition transition) {

        for (Step s : _steps) {

            // if there is a workflow, run it

            // if there are object dependencies, run them

            // if there is an accessor, run it

        }

        return false;
    }

    public void merge(ResolutionStrategy strategy) {
        for (Step s : strategy) {
            _steps.add(s);
        }
    }
}
