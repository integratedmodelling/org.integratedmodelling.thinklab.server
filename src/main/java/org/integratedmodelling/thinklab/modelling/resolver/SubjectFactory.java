package org.integratedmodelling.thinklab.modelling.resolver;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabInternalErrorException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IProperty;
import org.integratedmodelling.thinklab.api.modelling.IModel;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.api.modelling.IObjectSource;
import org.integratedmodelling.thinklab.api.modelling.IObservingObject.IDependency;
import org.integratedmodelling.thinklab.api.modelling.IReifiableObject;
import org.integratedmodelling.thinklab.api.modelling.IScale;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.common.vocabulary.Observable;
import org.integratedmodelling.thinklab.modelling.lang.Metadata;
import org.integratedmodelling.thinklab.modelling.lang.Model;
import org.integratedmodelling.thinklab.modelling.lang.Model.AttributeTranslator;
import org.integratedmodelling.thinklab.modelling.lang.ObservingObject;
import org.integratedmodelling.thinklab.modelling.lang.Scale;
import org.integratedmodelling.thinklab.modelling.lang.Subject;
import org.integratedmodelling.thinklab.modelling.states.ConstObjectState;
import org.integratedmodelling.utils.CamelCase;

/**
 * Reimplemented to use the annotation-driven mechanism used for other things in Thinklab. Just tag
 * the subject class to associate with a concept with the annotation @SubjectType(NS.CONCEPT_TO_ASSOCIATE).
 * This will register the class with the Model Manager and the class will be associated intelligently
 * (meaning, the reasoner will be used so derived concept will be associated to the class that is 
 * semantically closest).
 *
 * TODO this could (should?) use the default ISemanticObject mechanism in Thinklab (works exactly the same and adds
 * bi-directional extraction/injection of formal semantics from/to Java objects) but this way it's 
 * simpler and likely works out of the box.
 * 
 * @author Luke
 * @author Ferd
 */
public class SubjectFactory {

    /**
     * The main entry point.
     *
     * @param observable
     * @param namespace
     * @param scale
     * @param newSubjectId
     * @return
     * @throws ThinklabException
     */
    public static Subject getSubjectByMetadata(IObservable observable, INamespace namespace, IScale scale,
            String newSubjectId) throws ThinklabException {

        Subject result;
        Constructor<?> constructor;

        Class<?> agentClass = Thinklab.get().getSubjectClass(observable.getType());

        try {
            constructor = agentClass.getConstructor(IObservable.class, Collection.class, Object.class,
                    INamespace.class, String.class);
        } catch (Exception e) {
            throw new ThinklabInternalErrorException("No viable constructor found for Java class '"
                    + agentClass.getCanonicalName() + "' for agent type '" + observable.getFormalName() + "'");
        }

        try {
            result = (Subject) constructor.newInstance(observable, ((Scale) scale).getExtents(), null,
                    namespace, newSubjectId);
        } catch (Exception e) {
            throw new ThinklabRuntimeException("Unable to generate new instance of Java class '"
                    + agentClass.getCanonicalName() + "' for agent type '" + observable.getFormalName() + "'");
        }

        return result;

    }

    // /**
    // * Create default subject model for passed concept; if passed a non-null subject, pre-instantiate only
    // the
    // * functional properties that are not already observed, otherwise do all of them.
    // *
    // * @param oobs
    // * @param subject
    // * @return
    // * @throws ThinklabException
    // */
    // public static IModel createDefaultSubjectModel(ISubject subject, ISubject rootSubject)
    // throws ThinklabException {
    //
    // IModel model = null;
    //
    // /*
    // * subject without any model associated: create default model from the concept adding dependencies for
    // * its functional properties.
    // */
    // ArrayList<IDependency> toObserve = new ArrayList<IDependency>();
    // for (IProperty p : subject.getObservable().getType().getAllProperties()) {
    // if (p.isFunctional()) {
    // IConcept target = subject.getObservable().getType().getPropertyRange(p).iterator().next();
    // IDependency dep = new ObservingObject.Dependency(
    // new Observable(target,
    // (NS.isObject(target) ? Thinklab.c(NS.DIRECT_OBSERVATION) : Thinklab.c(NS.INDIRECT_OBSERVATION)),
    // null, ""),
    // CamelCase.toLowerCase(target.getLocalName(), '-'), p, true, false, null);
    // toObserve.add(dep);
    // }
    // }
    //
    // /*
    // * we make a model even if nothing is there, just observing to the same subject.
    // */
    // ArrayList<IObservable> sj = new ArrayList<IObservable>();
    // sj.add(new Observable(subject.getObservable().getType(), Thinklab.c(NS.DIRECT_OBSERVATION),
    // rootSubject == null ? null : rootSubject.getObservable().getType(), ""));
    // model = new Model(sj, toObserve, rootSubject == null ? subject.getNamespace()
    // : rootSubject.getNamespace());
    // ((Model) model).setId(subject.getId());
    //
    // return model;
    //
    // }

    /**
     * Create all subjects from an object source in a given scale.
     * 
     * @param model
     * @param scale
     * @return
     * @throws ThinklabException
     */
    public static List<ISubject> createSubjects(IModel model, IObjectSource objectSource, IScale scale)
            throws ThinklabException {
        ArrayList<ISubject> ret = new ArrayList<ISubject>();

        if (objectSource != null) {
            int i = 1;
            for (IReifiableObject o : objectSource.getObjects(scale)) {
                String name = model.getId() + "-" + i++;
                Subject subject = SubjectFactory.getSubjectByMetadata(model.getObservable(),
                        model.getNamespace(), scale, name);

                for (AttributeTranslator at : ((Model) model).getAttributeTranslators()) {
                    Object att = o.getAttributeValue(at.attribute, at.observer.getObserver());
                    if (at.observer == null) {
                        ((Metadata) (subject.getMetadata())).put(at.property.toString(), att);
                    } else {
                        subject.addState(new ConstObjectState(at.observer.getObservable(), subject,
                                at.observer.getObserver(), att));
                    }
                }

                ret.add(subject);
            }
        }

        return ret;
    }
}
