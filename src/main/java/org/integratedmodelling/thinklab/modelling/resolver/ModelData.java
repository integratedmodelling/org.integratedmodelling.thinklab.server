package org.integratedmodelling.thinklab.modelling.resolver;

import java.util.ArrayList;
import java.util.Map;

import org.integratedmodelling.collections.NumericInterval;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.HashableObject;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.lang.IRemoteSerializable;
import org.integratedmodelling.thinklab.api.annotations.Concept;
import org.integratedmodelling.thinklab.api.annotations.Property;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.metadata.IMetadata;
import org.integratedmodelling.thinklab.api.modelling.IClassification;
import org.integratedmodelling.thinklab.api.modelling.IModel;
import org.integratedmodelling.thinklab.api.modelling.INumericObserver;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.IScale;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.common.ConceptPair;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.geospace.Geospace;
import org.integratedmodelling.thinklab.geospace.literals.ShapeValue;
import org.integratedmodelling.thinklab.interfaces.IStorageMetadataProvider;
import org.integratedmodelling.thinklab.modelling.lang.Metadata;
import org.integratedmodelling.thinklab.modelling.lang.Model;

/*
 * the structure stored in an ObservationKbox for each model instead of storing the model itself. 
 * Facilitates and speeds up query and ranking, allowing ModelPrioritizer to obtain 
 * the contextual ranking as fast as possible. The actual objects are then loaded from their 
 * authoritative definitions. It can also be assigned a server ID so that the models can be
 * queried and loaded from remote servers.
 */
@Concept(NS.MODEL_DATA)
public class ModelData extends HashableObject implements IStorageMetadataProvider, IRemoteSerializable {

    /*
     * customized for storage; uses per-object hashing to avoid messing with the 
     * caching in kbox.
     */
    @Concept(NS.OBSERVABLE)
    public static class Observable extends HashableObject {

        @Property(NS.HAS_TYPE)
        public IConcept mainType   = null;
        @Property(NS.HAS_OBSERVATION_TYPE)
        public IConcept obsType    = null;
        @Property(NS.HAS_SUBJECT_TYPE)
        public IConcept subjType   = null;
        @Property(NS.HAS_TRAIT_TYPE)
        public IConcept traiType   = null;
        @Property(NS.HAS_FORMAL_NAME)
        public String   formalName = null;

        public Observable() {
        }

        Observable(ModelData data, IObserver observer, IObservable obs) {
            this.mainType = obs.getType();
            this.obsType = obs.getObservationType();
            this.subjType = obs.getInherentType();
            this.traiType = data.checkDiscretization(observer, obs.getTraitType());
            this.formalName = obs.getFormalName();
        }

        /*
         * for debugging
         */
        public boolean is(IObservable concept) {
            boolean conceptOK = mainType.is(concept.getType());
            boolean obsOK = obsType.is(concept.getObservationType());
            boolean inhOK = (subjType == null && concept.getInherentType() == null)
                    || (subjType != null && concept.getInherentType() != null && subjType.is(concept
                            .getInherentType()));
            return conceptOK && obsOK && inhOK;
        }
    }

    // if this is set as the dereification attribute, dereification is presence/absence
    public static final String    PRESENCE_ATTRIBUTE = "__PRESENCE__";

    public Map<String, Object>    ranks              = null;

    /*
     * null serverId means that the object comes from this server. It is set externally after a query,
     * depending on what context the query was triggered in. A REST command that performs a kbox query
     * will typically pass the serverId it wants the retrieved objects to be tagged with.
     */
    public String                 serverId           = null;

    /*
     * Observables. The type/oType/iType below is for the primary observable and it's
     * redundant - may be removed, they allow to perform queries on the primary observable
     * easily, but for now we don't use them.
     */
    @Property(NS.HAS_OBSERVABLE)
    public ArrayList<Observable>  observables        = new ArrayList<Observable>();

    // stored
    @Property(NS.HAS_ID)
    public String                 id;
    @Property(NS.HAS_NAME)
    public String                 name;
    @Property(NS.HAS_NAMESPACE_ID)
    public String                 namespaceId;
    @Property(NS.HAS_PROJECT_ID)
    public String                 projectId;
    @Property(NS.HAS_TRAIT)
    public ArrayList<ConceptPair> traits;
    @Property(NS.HAS_TYPE)
    public IConcept               type;
    @Property(NS.HAS_OBSERVATION_TYPE)
    public IConcept               oType;
    @Property(NS.HAS_INHERENT_SUBJECT_TYPE)
    public IConcept               iType;
    @Property(NS.IS_PRIVATE)
    public boolean                isPrivate;
    @Property(NS.HAS_TRAIT_TYPE)
    public IConcept               tType;
    @Property(NS.IS_DATA_MODEL)
    public boolean                isResolved;
    @Property(NS.IS_COMPUTED)
    public boolean                isComputed;
    @Property(NS.IS_REIFICATION_MODEL)
    public boolean                isReification;
    @Property(NS.IS_IN_SCENARIO)
    public boolean                isInScenario;
    @Property(NS.HAS_DIRECT_OBJECTS)
    public boolean                hasDirectObjects;
    @Property(NS.HAS_DIRECT_DATA)
    public boolean                hasDirectData;
    @Property(NS.SPATIAL_EXTENT_PROPERTY)
    public ShapeValue             spaceExtent;
    @Property(NS.HAS_METADATA)
    public Metadata               metadata           = new Metadata();

    /*
     * I don't really want to build a Lucene index for intervals that supports intersection with partially
     * defined intervals, and using a LineString is not an option because Neo4j only supports one spatial
     * property per object, so we store the temporal boundaries, if any, with two longs, and the day we 
     * need non-continuous time intervals we'll reconsider. 
     */
    @Property(NS.HAS_START_TIME)
    public long                   timeStart          = -1;
    @Property(NS.HAS_END_TIME)
    public long                   timeEnd            = -1;

    /*
     * The following two may be true even when there is no space/time extent or multiplicity, and
     * mean that the object is aware of the respective domain (so it requires it in contextualization) but
     * doesn't have full scale information. They're always true if there is a scale for space/time, but
     * the contrary isn't true. Objects that are nonspatial and/or nontemporal can be used in a spatial/temporal
     * context, but objects that are spatial and/or temporal are given precedence, and will make a 
     * nonspatial/nontemporal context spatial/temporal. These are used to compute the domain specificity
     * criterion.
     */
    @Property(NS.IS_SPATIAL)
    public boolean                isSpatial;
    @Property(NS.IS_TEMPORAL)
    public boolean                isTemporal;

    /*
     * these allow to compute metrics of resolution given the intersection size of each domain and to check
     * if there are more domains besides space and time. They should only be relied upon in resolved models with
     * data, as computed models may not know their full scale until contextualized.
     */
    @Property(NS.HAS_TIME_MULTIPLICITY)
    public long                   timeMultiplicity   = 0;
    @Property(NS.HAS_SPACE_MULTIPLICITY)
    public long                   spaceMultiplicity  = 0;
    @Property(NS.HAS_SCALE_MULTIPLICITY)
    public long                   scaleMultiplicity  = 0;
    @Property(NS.DEREIFYING_ATTRIBUTE)
    public String                 dereifyingAttribute;
    
    @Property(NS.HAS_DISCRETE_LEVELS_COUNT)
    public int discreteLevelsCount = 0;

    // for the instantiator - do not remove
    public ModelData() {
    }

    // painkiller method
    public boolean isTemporal() {
        return !(timeStart < 0 && timeEnd < 0);
    }

    public ModelData(Model o, IMonitor monitor) throws ThinklabException {

        id = o.getId();
        name = o.getName();
        metadata = (Metadata) o.getMetadata();
        namespaceId = o.getNamespace().getId();
        projectId = o.getNamespace().getProject().getId();

        IScale scale = o.getCoverage();
        if (scale.getSpace() != null) {
            spaceExtent = (ShapeValue) scale.getSpace().getExtent();
            // may be null when we just say 'over space'.
            if (spaceExtent != null) {
                spaceExtent = spaceExtent.transform(Geospace.get().getDefaultCRS());
                spaceMultiplicity = scale.getSpace().getMultiplicity();
            }
            isSpatial = true;
        }
        if (scale.getTime() != null) {
            NumericInterval timeExtent = (NumericInterval) scale.getTime().getExtent();
            if (timeExtent != null) {
                if (!timeExtent.isLeftInfinite()) {
                    timeStart = (long) timeExtent.getMinimumValue();
                }
                if (!timeExtent.isRightInfinite()) {
                    timeEnd = (long) timeExtent.getMaximumValue();
                }
                timeMultiplicity = scale.getTime().getMultiplicity();
            }
            isTemporal = true;
        }

        for (int i = 0; i < o.getObservables().size(); i++) {
            IObservable obs = o.getObservables().get(i);
            // if (i == 0 && o.getObserver() != null) {
            // obs = o.getObserver().getObservable();
            // }
            observables.add(new Observable(this, o.getObserver(), obs));
        }

        /*
         * the type of the MAIN observable. We use the observer types (if any) for
         * matching, but we extract the traits from this one, so that any of our
         * model's observables will match them.
         */
        IObservable obs = o.getObservable();
        // if (o.getObserver() != null) {
        // obs = o.getObserver().getObservable();
        // }

        type = obs.getType();
        oType = obs.getObservationType();
        iType = obs.getInherentType();
        tType = checkDiscretization(o.getObserver(), obs.getTraitType());
        
        for (Pair<IConcept, IConcept> tt : NS.getTraits(o.getObservable().getType())) {
            if (traits == null) {
                traits = new ArrayList<ConceptPair>();
            }
            traits.add(new ConceptPair(tt.getFirst(), tt.getSecond()));
        }

        isPrivate = o.isPrivate();
        isComputed = o.isComputed();
        isInScenario = o.getNamespace().isScenario();
        isReification = o.isReificationModel();
        isResolved = o.isResolved();
        hasDirectData = o.getDatasource() != null;
        hasDirectObjects = o.getObjectSource() != null;

    }

    /**
     * If the passed observer is a numeric observer that discretizes the
     * value, do not use the trait type but keep a count of the highest number of levels 
     * to indicate that the data have lower precision. We can use that for
     * ranking.
     * 
     * @param observer
     * @param traitType
     * @return
     */
     IConcept checkDiscretization(IObserver observer, IConcept traitType) {

        if (observer == null || traitType == null) {
            return traitType;
        }
        
        if (observer instanceof INumericObserver) {
            IClassification zc = ((INumericObserver)observer).getDiscretization();
            if (zc != null) {
                if (this.discreteLevelsCount < zc.getClassifiers().size()) {
                    this.discreteLevelsCount = zc.getClassifiers().size();
                }
                traitType = null;
            }
        }

        return traitType;
    }

    public ModelData(Model model, String dereifyingAttribute, IMonitor monitor) throws ThinklabException {

        this(model, monitor);

        IModel observer = model.getAttributeObserver(dereifyingAttribute);

        /*
         * reset observable
         */
        this.type = observer.getObservable().getType();
        this.oType = observer.getObservable().getObservationType();
        this.iType = observer.getObservable().getInherentType();
        this.tType = checkDiscretization(observer.getObserver(), observer.getObservable().getTraitType());

        for (Pair<IConcept, IConcept> tt : NS.getTraits(type)) {
            if (traits == null) {
                traits = new ArrayList<ConceptPair>();
            }
            traits.add(new ConceptPair(tt.getFirst(), tt.getSecond()));
        }
        this.observables.clear();
        this.observables.add(new Observable(this, observer.getObserver(), observer.getObservable()));

        this.isReification = false;
        this.hasDirectData = true;
        this.hasDirectObjects = false;

        /*
         * add attribute
         */
        this.dereifyingAttribute = dereifyingAttribute;
    }

    @Override
    public IMetadata getMetadata() {
        return metadata;
    }

    @Override
    public void addStorageMetadata(IMetadata metadata, IMonitor monitor) throws ThinklabException {
        metadata.merge(this.metadata, false);
    }

    @Override
    public String toString() {

        String ret = name + "\nMain observable:\n";
        ret += " <" + type + " " + oType + " " + (iType == null ? "N/A" : iType)
                + (tType == null ? " N/A" : (" " + tType)) + ">\n   ";

        ret += "All observables:\n";

        for (Observable ob : observables) {
            ret += " <" + ob.mainType + " " + ob.obsType + " " + (ob.subjType == null ? "N/A" : ob.subjType)
                    + (ob.traiType == null ? " N/A" : (" " + ob.traiType)) + ">\n   ";
        }

        ret += isTemporal() ? "[" + (timeStart < 0 ? "*" : "" + timeStart) + ","
                + (timeEnd < 0 ? "*" : "" + timeEnd) + "] (#" + timeMultiplicity + ")" : "NoTime";
        ret += " ";
        ret += spaceExtent != null ? spaceExtent + " (#" + spaceMultiplicity + ")" : "NoSpace";
        ret += "\n   ";
        if (traits != null) {
            for (int i = 0; i < traits.size(); i++) {
                ret += "T<" + traits.get(i).getFirst() + " = " + traits.get(i).getSecond() + ">\n   ";
            }
        }
        ret += isPrivate ? "private " : "public ";
        ret += isComputed ? "computed " : "notComputed ";
        ret += isInScenario ? "inScenario " : "notInScenario ";
        ret += isReification ? "reification " : "notReification ";
        ret += hasDirectData ? "directData " : "noDirectData ";
        ret += hasDirectObjects ? "directObjects " : "noDirectObjects ";

        return ret;
    }

    @Override
    public Object adapt() {
        return ranks;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        return obj instanceof ModelData && name.equals(((ModelData) obj).name);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return name.hashCode();
    }

    public ArrayList<ConceptPair> getTraits() {
        // no nulls, cleaner code.
        return traits == null ? new ArrayList<ConceptPair>() : traits;
    }

}
