package org.integratedmodelling.thinklab.modelling.resolver.obsolete;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IExpression;
import org.integratedmodelling.thinklab.api.knowledge.IProperty;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.modelling.ICoverage;
import org.integratedmodelling.thinklab.api.modelling.IDataSource;
import org.integratedmodelling.thinklab.api.modelling.IModel;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.api.modelling.IObjectSource;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.IObservingObject.IDependency;
import org.integratedmodelling.thinklab.api.modelling.IScale;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.api.modelling.resolution.IProvenance;
import org.integratedmodelling.thinklab.api.modelling.resolution.IResolutionContext;
import org.integratedmodelling.thinklab.common.vocabulary.Observable;
import org.integratedmodelling.thinklab.modelling.resolver.ProvenanceGraph.DependencyEdge;
import org.integratedmodelling.thinklab.modelling.resolver.ProvenanceGraph.ProvenanceNode;

/*
 * resolution context of object X contains a scale, the link X will be target of in the provenance diagram,
 * and optionally the semantic context of the resolution, i.e. the property that represents the link and if
 * that isn't null, the concept it links to in that capacity.
 * 
 * TODO rename all the methods that return sub-contexts to something sensible and address any redundancy or
 * stupidity in them.
 */
class ResolutionContext implements IResolutionContext {

    ISubject subject;
    IObservable concept;
    IProperty property;
    IObserver observer;
    IModel model;

    // mandatorily not null
    IMonitor monitor;

    // the scale we keep harmonizing as we resolve 
    IScale scale;
    // the link we will use to link to root if resolved
    DependencyEdge link;
    // the node we get for our target when resolution is accepted by the resolver, and the root node for
    // our children contexts.
    ProvenanceNode node;
    // provenance graph that gets built along the resolution chain, passed around to children
    ProvenanceGraph modelGraph;

    // true only when we're using this context to accept a model that has been already
    // deemed resolvable. When this is true, we don't try to pre-resolve it for acceptance.
    boolean preResolved = false;

    // TODO fill these in
    Collection<String> nsWhitelist = null;
    Collection<String> nsBlacklist = null;

    // scenarios are defined externally on the root context and passed around unmodified 
    Collection<String> scenarios = null;

    // traits extracted from our observable, used when the kbox is consulted for ranking of options.
    Collection<Pair<IConcept, IConcept>> traits = null;

    /*
     * this should only be true when we're resolving a conditional branch. It will color all nodes as
     * conditional unless they have been seen in an unconditional branch too.
     */
    boolean isConditional;
    boolean optional;

    // only true in the root context, the one that admits default models and requires just a single one for the subject.
    boolean isRoot = false;

    // model cache, passed around; holds node->model correspondences as they're accepted.
    HashMap<ProvenanceNode, IModel> models;

    /*
     * overall coverage, updated by the resolver after each step.
     */
    ICoverage coverage;

    ResolutionContext parent;

    /*
     * ONLY for "child" contexts - automatically sets the parent context and
     * prepares a new provenance graph, to be merged with the parent's at finish() if coverage
     * is sufficient. Copies all common info and leave everything else undefined.
     * 
     * Constructor using this must set the link and add the provenance node as 
     * the context's node, then link it as necessary. Each context can only have
     * one root node.
     */
    private ResolutionContext(ResolutionContext ctx) {

        this.models = new HashMap<ProvenanceGraph.ProvenanceNode, IModel>(ctx.models);
        this.modelGraph = new ProvenanceGraph(ctx.monitor);
        this.scenarios = ctx.scenarios;
        this.nsWhitelist = ctx.nsWhitelist;
        this.nsBlacklist = ctx.nsBlacklist;
        this.scale = ctx.scale;
        this.monitor = ctx.monitor;
        this.preResolved = ctx.preResolved;
        this.coverage = ctx.coverage;
        this.parent = ctx;
    }

    /**
     * Call after resolution of this context to merge in results into the father context. If the
     * merged coverage is acceptable, merge provenance and model cache and link the root node to
     * the father's.
     * 
     * @param ctx
     * @return
     * @throws ThinklabException 
     */
    @Override
    public ICoverage finish() throws ThinklabException {

        if (parent == null)
            return coverage;

        parent.coverage = parent.coverage == null ? coverage : parent.coverage.and(coverage);

        if (!parent.coverage.isEmpty()) {
            parent.modelGraph.merge(modelGraph);
            parent.models.putAll(models);
            if (parent.node != null) {
                parent.modelGraph.addEdge(parent.node, node, link);
            }

            /*
             * TODO harmonize the common scale with that of the object we have just resolved.
             */

        }

        return parent.coverage;
    }

    /**
     * Root resolution context. All others should be created using public methods.
     * @param subject
     * @param scenarios
     * @return
     */
    public static ResolutionContext forSubject(ISubject subject, IMonitor monitor,
            Collection<String> scenarios) {
        ResolutionContext ret = new ResolutionContext(subject, monitor, scenarios);
        ret.isRoot = true;
        ret.models = new HashMap<ProvenanceGraph.ProvenanceNode, IModel>();
        return ret;
    }

    //
    //    public ResolutionContext forSubjectModel(IModel model) {
    //        return new ResolutionContext(this.subjectObserver, model, this.scale, this.modelGraph, this.scenarios);
    //    }
    //
    //    public ResolutionContext forDependentSubject(ISubjectObserver observer, IModel subjectModel) {
    //        ResolutionContext ret = new ResolutionContext();
    //        ret.subjectObserver = observer;
    //        ret.model = subjectModel;
    //        ret.scale = observer.getScale();
    //        ret.modelGraph = modelGraph;
    //        ret.concept = observer.getObservable(model);
    //        ret.scenarios = scenarios;
    //        return ret;
    //    }
    //
    //    private ResolutionContext(SubjectObserver subjectObserver, Collection<String> scenarios) {
    //        this.subjectObserver = subjectObserver;
    //        this.scale = subjectObserver.getScale();
    //
    //        /*
    //         * create initial provenance graph, adding the vertices already known by the observer if
    //         * any were predefined.
    //         */
    //        this.modelGraph = new ProvenanceGraph(subjectObserver.getMonitor());
    //        for (IObservable m : subjectObserver.models.keySet()) {
    //            ProvenanceNode model = subjectObserver.models.get(m);
    //            this.modelGraph.addVertex(model);
    //        }
    //
    //        this.concept = subjectObserver.getObservable(model);
    //        this.scenarios = scenarios;
    //    }
    //
    //    private ResolutionContext(SubjectObserver subjectObserver, IModel model, IScale scale,
    //            ProvenanceGraph modelGraph, Collection<String> scenarios) {
    //        this.subjectObserver = subjectObserver;
    //        this.model = model;
    //        this.scale = scale;
    //        this.modelGraph = modelGraph;
    //        this.concept = subjectObserver.getObservable(model);
    //        this.scenarios = scenarios;
    //    }

    public ResolutionContext(ISubject subject, IMonitor monitor, Collection<String> scenarios) {
        this.subject = subject;
        this.monitor = monitor;
        this.scenarios = scenarios;
        this.modelGraph = new ProvenanceGraph(monitor);
    }

    public ResolutionContext resolved() {

        ResolutionContext ret = new ResolutionContext(this);
        ret.preResolved = true;
        return ret;
    }

    public ResolutionContext defineState(IModel model) {
        ResolutionContext ret = new ResolutionContext(this);
        ret.model = model;
        ret.link = new DependencyEdge(DependencyEdge.DEFINE_STATE, "", subjectObserver.getObservable(model
                .getObserver()));
        ret.observer = model.getObserver();
        return ret;
    }

    public ResolutionContext mediate(IObservable iConcept) {
        ResolutionContext ret = new ResolutionContext(this);
        ret.link = new DependencyEdge(DependencyEdge.MEDIATE_TO, "", iConcept);
        ret.concept = iConcept;
        return ret;
    }

    public ResolutionContext observe(IObserver observer) {
        ResolutionContext ret = new ResolutionContext(this);
        ret.link = new DependencyEdge(DependencyEdge.MEDIATE_TO, "", observer.getObservableConcept());
        ret.concept = observer.getObservableConcept();
        ret.observer = observer;
        return ret;
    }

    public ResolutionContext conditionalDependency(IModel model, IObservable observable,
            IExpression condition, int index) {
        ResolutionContext ret = new ResolutionContext(this);
        DependencyEdge link = new DependencyEdge(DependencyEdge.CONDITIONAL_DEPENDENCY, "", observable);
        link.condition = condition;
        link.conditionIndex = index;
        ret.link = link;
        ret.concept = observable;
        ret.model = model;
        ret.observer = model.getObserver();

        return ret;
    }

    public ResolutionContext interpretAs(IObservable iConcept) {
        ResolutionContext ret = new ResolutionContext(this);
        ret.link = new DependencyEdge(DependencyEdge.INTERPRET_AS, "", iConcept);
        ret.concept = iConcept;
        return ret;
    }

    /*
     * change semantic context preserving links and entry point status. Only change model context if this
     * comes from a model dependency.
     */
    public ResolutionContext switchTo(IObservable observable, IProperty property, IModel model) {
        ResolutionContext ret = new ResolutionContext(this);
        ret.concept = observable;
        ret.property = property;
        ret.link = this.link;

        if (model != null) {
            ret.model = model;
        }
        return ret;
    }

    /*
     * a direct data dependency for a subject. FIXME check redundancy with dependency()
     */
    public ResolutionContext entryPoint(IDependency dependency) {

        ResolutionContext ret = new ResolutionContext(this);
        DependencyEdge link = new DependencyEdge(DependencyEdge.DEPENDENCY, dependency.getFormalName(),
                dependency.getObservable());
        link.property = ret.property = dependency.getProperty();
        ret.concept = dependency.getObservable();
        ret.link = link;
        ret.optional = dependency.isOptional() || parent.optional;
        return ret;
    }

    public ResolutionContext dependency(IDependency dependency) {
        ResolutionContext ret = new ResolutionContext(this);
        DependencyEdge link = new DependencyEdge(DependencyEdge.DEPENDENCY, dependency.getFormalName(),
                dependency.getObservable());
        link.property = ret.property = dependency.getProperty();
        ret.concept = dependency.getObservable();
        ret.link = link;
        ret.model = dependency.getObservable().getModel();
        ret.optional = this.optional || dependency.isOptional();

        return ret;
    }

    public Set<IObservable> getObservableClosure(IObservable observable) throws ThinklabException {

        Set<IObservable> observables = new HashSet<IObservable>();
        if (this.concept != null && this.property != null) {
            for (IConcept cc : this.concept.getType().getPropertyRange(this.property)) {
                observables.add(new Observable(cc, observable.getObservationType(), observable
                        .getInherentType(), observable.getFormalName()));
            }
        } else {
            observables.add(observable);
        }

        if (observables.size() == 0) {
            observables.add(observable);
        }

        return observables;
    }

    /*
     * get a context to resolve the next group of data dependencies. Will need to use all precomputed states
     * available.
     */
    //    public ResolutionContext newDependencyGroup() {
    //        ResolutionContext ret = new ResolutionContext(this);
    //        ret.modelGraph = new ProvenanceGraph(subjectObserver._monitor);
    //        /*
    //         * TODO publish all existing states
    //         */
    //        return ret;
    //    }

    @Override
    public IScale getScale() {
        return scale;
    }

    @Override
    public Collection<String> getScenarios() {
        return scenarios;
    }

    @Override
    public IModel getModel() {
        return model;
    }

    @Override
    public INamespace getNamespace() {

        if (model != null)
            return model.getNamespace();
        return subject.getNamespace();
    }

    @Override
    public void setMonitor(IMonitor monitor) {
        this.monitor = monitor;
    }

    @Override
    public IMonitor getMonitor() {
        return monitor;
    }

    @Override
    public ISubject getSubject() {
        return subject;
    }

    @Override
    public IProperty getPropertyContext() {
        return property;
    }

    @Override
    public void forceScale(IScale scale) {
        this.scale = scale;
    }

    @Override
    public IProvenance getProvenance() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ICoverage getCoverage() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IResolutionContext forObservable(IObservable observable) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IResolutionContext forDependency(IDependency dependency) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IResolutionContext forModel(IModel model) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IResolutionContext forSubject(ISubject subject) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IResolutionContext forObserver(IObserver observer) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IResolutionContext forDatasource(IDataSource datasource) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IResolutionContext forObjectSource(IObjectSource objectsource) {
        // TODO Auto-generated method stub
        return null;
    }

}
