package org.integratedmodelling.thinklab.modelling.resolver.obsolete;

import java.util.Collection;
import java.util.List;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.modelling.IActiveSubject;
import org.integratedmodelling.thinklab.api.modelling.ICoverage;
import org.integratedmodelling.thinklab.api.modelling.IDataset;
import org.integratedmodelling.thinklab.api.modelling.IModel;
import org.integratedmodelling.thinklab.api.modelling.IObservingObject.IDependency;
import org.integratedmodelling.thinklab.api.modelling.IState;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.common.monitoring.Notification;
import org.integratedmodelling.thinklab.common.utils.StringUtils;
import org.integratedmodelling.thinklab.debug.CallTracer;
import org.integratedmodelling.thinklab.modelling.resolver.Coverage;
import org.integratedmodelling.thinklab.modelling.resolver.ProvenanceGraph;
import org.integratedmodelling.thinklab.modelling.resolver.ResolutionContext;
import org.integratedmodelling.thinklab.modelling.resolver.Workflow;
import org.integratedmodelling.thinklab.modelling.resolver.ProvenanceGraph.ProvenanceNode;

public class StateResolver extends AbstractResolver {

    private final IModel model;
    private final ProvenanceNode root;
    private final List<IDependency> dependencies;
    private final ResolutionContext ctx;
    private final IDataset dataset;

    // ICoverage resolveStates(IModel model, ProvenanceNode root, List<IDependency> dependencies,
    // ResolutionContext ctx) throws ThinklabException {
    public StateResolver(SubjectObserver subjectObserver, IModel model, ProvenanceNode root,
            List<IDependency> dependencies, ResolutionContext ctx, IMonitor monitor, IDataset dataset) {
        super(subjectObserver, monitor);
        this.model = model;
        this.root = root;
        this.dependencies = dependencies;
        this.ctx = ctx;
        this.dataset = dataset;
    }

    /**
     * resolve dependency group to workflow and run if coverage is sufficient. Record provenance and workflow.
     * 
     * @param model
     * @param root
     * @param dependencies
     * @param ctx
     * @return
     * @throws ThinklabException
     */
    @Override
    public ICoverage resolve(Collection<String> scenarios) throws ThinklabException {

        CallTracer.indent("resolveStates()", this, model, root, dependencies);

        ProvenanceNode node = subjectObserver.getNode(model, ctx);
        Coverage coverage = null;

        // if there are no data dependencies, the coverage is 1.
        if (dependencies.size() == 0) {
            CallTracer.msg("no data dependencies (dependencies.size() == 0)");
            coverage = new Coverage(ctx.scale, 1.0);
        }

        // resolve data dependencies of subject model. These correspond to the entry points for the workflow.
        Coverage mcoverage = null;
        DependencyResolver r;
        ICoverage dcov;
        for (IDependency d : dependencies) {
            CallTracer.msg("dependency: " + d.toString());

            ResolutionContext rc = ctx.dependency(d); // HMMM suspicious logics. 
            ResolutionContext entryPoint = rc.entryPoint(d);
            r = new DependencyResolver(subjectObserver, d, node, entryPoint, monitor);
            dcov = r.resolve(scenarios);

            if (dcov == null || dcov.getCoverage() < subjectObserver.MIN_MODEL_COVERAGE) {
                CallTracer.msg("dependency has null or inadequate coverage.");
                /*
                 * user-injected dependencies have no formal name; if these are unresolved it's because of
                 * downstream dependencies, so we don't report those
                 */
                if (d.getFormalName() != null) {

                    String err = "dependency " + d.getFormalName() + " in " + model.getName()
                            + " cannot be observed in this context";

                    if (d.isOptional()) {
                        if (monitor != null) {
                            monitor.warn("optional " + err);
                        }
                    } else if (!ctx.optional) {
                        if (monitor != null) {
                            monitor.error(addError("mandatory " + err));
                        }
                    }
                }
            }

            if (!d.isOptional()) {
                mcoverage = (Coverage) (mcoverage == null ? dcov : (coverage == null ? null : coverage
                        .and(dcov)));
            }
        }

        if (mcoverage != null) {
            coverage = (Coverage) (coverage == null ? mcoverage : coverage.and(mcoverage));
        } else if (coverage == null) {
            // both are null, meaning no deps or all optional -> model is in.
            coverage = new Coverage(ctx.scale, 1.0);
        }

        // if (root != null && coverage != null && coverage.getCoverage() >= MIN_MODEL_COVERAGE) {
        // ctx.modelGraph.addEdge(node, root, ctx.link);
        // }

        node.setCoverage(coverage);

        // if we're only being used to check for resolution, we have nothing else to do
        if (subjectObserver.isSandbox()) {
            CallTracer.unIndent();
            return coverage;
        }

        /*
         * cleanup from unsuccessful attempts made in resolution and unused states. If nothing is left, we had
         * all optional dependencies which were not resolved.
         */
        ctx.modelGraph.cleanup();

        if (ctx.modelGraph.vertexSet().size() == 0) {
            CallTracer.unIndent();
            return coverage;
        }

        // store the graph before we do anything potentially dangerous later.
        if (monitor != null) {
            monitor.message(Notification.PROVENANCE_GRAPH, ctx.modelGraph.visualize().toString());
        }

        /*
         * merge scale into definitive representation before states are computed
         * 
         * NO - this is still a scale, we're just initializing. THEN we create the agents and run a schedule
         * if we have time in the context.
         * 
         * FIXME merge within the ResolutionContext we're using.
         */
        // scale = mergeScale(scale);

        // compile model graph into dataflow and run it to initialize all states.
        // ACHTUNG: if the model graph contains only statemodels, the workflow should be empty.
        ISubject rootSubject = subjectObserver.getSubject();
        Workflow workflow = new Workflow(rootSubject, model, ctx.modelGraph, monitor, dataset);

        // notify provenance and workflow
        subjectObserver.getResolutionStrategy().addStep(ctx.modelGraph, workflow);

        // notify the workflow graph for posterity.
        if (monitor != null) {
            monitor.message(Notification.WORKFLOW_GRAPH, workflow.visualize().toString());
        }

        // initialize for all context states. The simple loop is fine here.
        if (workflow.preprocess()) {
            if (workflow.vertexSet().size() > 0) {
                if (monitor != null) {
                    monitor.info("computing " + ctx.scale.getMultiplicity() + " initial states", null);
                }

                // main initialization loop.
                // TODO parallelize. With the current time/space, we should be able to just use
                // Parallel.for, but the issue is whether the external accessors are reentrant
                // or not.
                for (int i = 0; i < ctx.scale.getMultiplicity(); i++) {
                    if (monitor != null && monitor.isStopped()) {
                        monitor.warn("interrupted by user");

                        CallTracer.unIndent();
                        return null;
                    }

                    workflow.initialize(i);
                }

                if (monitor != null) {
                    monitor.info("data coverage is " + StringUtils.percent(coverage.getCoverage()), null);
                }

                for (IState s : workflow.getStates()) {
                    ((IActiveSubject) rootSubject).addState(s);
                }
            }
        } else {
            coverage = null;
        }

        CallTracer.unIndent();
        return coverage;
    }
}
