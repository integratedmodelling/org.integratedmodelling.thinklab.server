package org.integratedmodelling.thinklab.modelling.bayes;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.collections.Triple;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.modelling.IAction;
import org.integratedmodelling.thinklab.api.modelling.IClassification;
import org.integratedmodelling.thinklab.api.modelling.IClassifyingObserver;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.IPresenceObserver;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.common.data.IndexedCategoricalDistribution;
import org.integratedmodelling.thinklab.common.utils.CamelCase;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.interfaces.bayes.IBayesianInference;
import org.integratedmodelling.thinklab.interfaces.bayes.IBayesianNetwork;
import org.integratedmodelling.thinklab.modelling.lang.ConditionalObserver;
import org.integratedmodelling.thinklab.modelling.lang.Measurement;
import org.integratedmodelling.thinklab.modelling.lang.Ranking;
import org.integratedmodelling.thinklab.modelling.lang.StateAccessor;
import org.integratedmodelling.thinklab.modelling.lang.Value;

import com.google.common.collect.Sets;

public class BayesianAccessor extends StateAccessor {

    IBayesianNetwork                                _network;
    String                                          importFile;
    File                                            workspace;
    IBayesianInference                              _inference;

    HashSet<String>                                 _nodeIds               = new HashSet<String>();
    HashMap<String, String>                         _key2node              = new HashMap<String, String>();
    HashMap<IConcept, String>                       _concept2node          = new HashMap<IConcept, String>();
    HashMap<String, List<String>>                   _outputKeys            = new HashMap<String, List<String>>();
    HashMap<String, IClassification>                _outputClassifications = new HashMap<String, IClassification>();
    // keep keys of presence/absence observers with the node ID and the outcome IDs corresponding to true and
    // false.
    HashMap<String, Triple<String, String, String>> _presenceKeys          = new HashMap<String, Triple<String, String, String>>();
    Set<String>                                     _warningKeys           = new HashSet<String>();
    ArrayList<Pair<String, String>>                 _evidence              = new ArrayList<Pair<String, String>>();

    class UncertaintyDesc {
        public UncertaintyDesc(IConcept inherentType) {
            this.observed = inherentType;
        }

        // the key for the correspondent BN node - starts null
        String      nodeKey;
        // the observable for the uncertainty
        IObservable observable;
        // the concept we're looking at
        IConcept    observed;
    }

    // matches the key we use for the uncertainty state to the node we're measuring the
    // uncertainty for.
    HashMap<String, UncertaintyDesc> _uncertainties = new HashMap<String, BayesianAccessor.UncertaintyDesc>();

    boolean                          _resolved      = false;

    public BayesianAccessor(List<IAction> actions, String importFile, File workspace)
            throws ThinklabException {
        super(actions, null);
        this.importFile = importFile;
        this.workspace = workspace;
        initialize();
    }

    public void initialize() throws ThinklabException {

        _network = BayesianFactory.get().createBayesianNetwork(workspace + File.separator + importFile);

        _nodeIds = Sets.newHashSet(_network.getAllNodeIds());
        _inference = _network.getInference();
    }

    @Override
    public void notifyInput(IObservable observable, IObserver observer, String key, boolean isMediation)
            throws ThinklabException {

        /*
         * check if it's a presence/absence; special treatment if so.
         */
        if (observer instanceof IPresenceObserver) {

            String nodeId = findMatchingNodeID(observable.getType(), observer, key);
            if (nodeId != null) {

                String present = null, absent = null;

                for (String s : _network.getOutcomeIds(nodeId)) {
                    if (s.endsWith("Present")) {
                        present = s;
                    }
                    if (s.endsWith("Absent")) {
                        absent = s;
                    }
                }

                if (present == null || absent == null) {
                    _monitor.error("cannot establish outcomes for presence/absence of " + nodeId);
                } else {
                    _presenceKeys.put(key, new Triple<String, String, String>(nodeId, present, absent));
                }
            }
            return;
        }

        /*
         * if the observer is not discretized in some way, raise a ruckus and leave.
         */
        // if (!((Observer<?>)observer).isDiscrete()) {
        IClassification classif = getClassification(observer);
        if (classif == null) {
            _monitor.error("cannot obtain discretized values from observation of " + observable);
            return;
        }

        /*
         * find a matching node and set it in key2node dictionary.
         * TODO if not found, ignore for now - will want to warn later
         */
        String nodeId = findMatchingNodeID(classif.getConceptSpace(), observer, key);

        if (nodeId == null)
            return;

        /*
         * validate state IDs against concepts
         */
        String notFound = "";
        for (String s : _network.getOutcomeIds(nodeId)) {
            boolean match = false;
            for (IConcept c : classif.getConceptOrder()) {
                if ((match = c.getLocalName().equals(s))) {
                    break;
                }
            }
            if (!match) {
                notFound += (notFound.isEmpty() ? "" : ", ") + s;
            }
        }

        if (!notFound.isEmpty()) {
            _monitor.error("cannot match subclasses of " + observable.getType()
                    + " to outcomes of bayesian node " + nodeId);
        }

    }

    @Override
    public void notifyOutput(IObservable observable, IObserver observer, String key, boolean isMain)
            throws ThinklabException {

        if (observable.getObservationType().is(Thinklab.c(NS.UNCERTAINTY))) {
            /*
             * prepare to handle uncertainty; we match concepts to nodes when we
             * have all nodes.
             */
            _uncertainties.put(key, new UncertaintyDesc(observable.getInherentType()));
            return;
        }

        IClassification cls = getClassification(observer);

        if (cls == null) {
            _monitor.error("bayesian: observed values of " + key + " are not discretized");
            return;
        }

        /*
         * find node matching observable. TODO handle classifications by trait properly.
         */
        String nodeId = findMatchingNodeID(cls.getConceptSpace(), observer, key);

        if (nodeId == null) {
            _monitor.error("bayesian: cannot find a node to match output " + cls.getConceptSpace() + " ("
                    + key + ")");
            return;
        }

        /*
         * store classification as key to generate distributions later
         */
        _outputClassifications.put(nodeId, cls);

        /*
         * build ID order to interpret classification into outcomes
         */
        List<String> outcomeOrder = new ArrayList<String>();

        String missing = "";
        HashSet<String> outcomes = Sets.newHashSet(_network.getOutcomeIds(nodeId));
        for (IConcept c : cls.getConceptOrder()) {
            String id = c.getLocalName();
            if (!outcomes.contains(id)) {
                missing += (missing.isEmpty() ? "" : ", ") + id;
            }
            outcomeOrder.add(id);
        }

        if (!missing.isEmpty()) {
            _monitor.error("bayesian: cannot find outcome(s): " + missing + " in node " + nodeId
                    + " to match observable " + observable.getLocalName() + " (" + key + ")");
            return;
        }

        /*
         * record key for evidence matching
         */
        _outputKeys.put(nodeId, outcomeOrder);
    }

    private String findMatchingNodeID(IConcept observable, IObserver observer, String key) {
        String ret = findMatchingNodeID(observable, observer, key, false);
        return ret == null ? findMatchingNodeID(observable, observer, key, true) : ret;
    }

    private String findMatchingNodeID(IConcept observable, IObserver observer, String key, boolean removeTraits) {

        if (removeTraits) {
            Pair<IConcept, Collection<IConcept>> btr;
            try {
                btr = NS.separateAttributes(observable);
            } catch (ThinklabValidationException e) {
                return null;
            }
            observable = btr.getFirst();
        }

        /*
         * find a matching node. 
         * TODO if not found, ignore for now - will want to warn later
         */
        String nodeId = null;
        String humpKey = CamelCase.toUpperCamelCase(key, '-');
        if (_nodeIds.contains(key)) {
            nodeId = key;
        } else if (_nodeIds.contains(humpKey)) {
            nodeId = humpKey;
        } else if (_nodeIds.contains(observable.getLocalName())) {
            nodeId = observable.getLocalName();
        } else {
            String tryThat = observable.toString().replace(':', '-');
            if (_nodeIds.contains(tryThat))
                nodeId = tryThat;
        }

        /*
         * TODO CHECK INHERENT SUBJECT TYPE & PRESENCE
         */

        /*
         * try the observer if we still have no match.
         */
        if (nodeId == null) {
            observable = observer.getObservable().getType();
            if (removeTraits) {
                observable = NS.getBaseObservable(observable);
            }
            if (_nodeIds.contains(observable.getLocalName())) {
                nodeId = observable.getLocalName();
            }
        }

        if (nodeId != null) {
            _key2node.put(key, nodeId);
            _concept2node.put(observable, nodeId);
        }

        return nodeId;
    }

    private IClassification getClassification(IObserver observer) {

        IClassification ret = null;
        if (observer instanceof IClassifyingObserver) {
            ret = ((IClassifyingObserver) observer).getClassification();
        } else if (observer instanceof Measurement) {
            ret = ((Measurement) observer).getDiscretization();
        } else if (observer instanceof Ranking) {
            ret = ((Ranking) observer).getDiscretization();
        } else if (observer instanceof Value) {
            ret = ((Value) observer).getDiscretization();
        } else if (observer instanceof ConditionalObserver) {

            /*
             * classifications must be the same if present, so just get the first
             * FIXME/CHECK: should use getRepresentativeObserver although I don't think it
             * makes a difference now. No test case so postponing.
             */
            ret = getClassification(((ConditionalObserver) observer).getModels().get(0).getFirst()
                    .getObserver());
        }

        return ret;
    }

    @Override
    public void process(int stateIndex) throws ThinklabException {

        if (!_resolved) {
            resolveUncertaintyRefs();
        }

        _inference.clearEvidence();

        /*
         * submit evidence
         */
        for (Pair<String, String> zio : _evidence) {
            _inference.setEvidence(zio.getFirst(), zio.getSecond());
        }

        /*
         * run inference
         */
        _inference.run();

    }

    /*
     * runs once before the first process(). Resolves all uncertainty keys
     * to the correspondent concept.
     */
    private void resolveUncertaintyRefs() throws ThinklabValidationException {
        _resolved = true;

        for (UncertaintyDesc u : _uncertainties.values()) {
            u.nodeKey = _concept2node.get(u.observed);
            if (u.nodeKey == null) {
                throw new ThinklabValidationException("cannot find concept " + u.observed
                        + " for uncertainty computation");
            }
        }
    }

    @Override
    public void setValue(String inputKey, Object value) {

        if (_presenceKeys.containsKey(inputKey)) {

            if (!(value instanceof Boolean) && value != null) {
                throw new ThinklabRuntimeException("internal: presence value not a boolean for " + inputKey);
            }

            Triple<String, String, String> ik = _presenceKeys.get(inputKey);
            if (value != null) {
                _evidence.add(new Pair<String, String>(ik.getFirst(), (Boolean) value ? ik.getSecond() : ik
                        .getThird()));
            }
            return;
        }

        String nodeId = _key2node.get(inputKey);

        if (nodeId == null && !_warningKeys.contains(nodeId)) {
            _monitor.warn("model dependency " + inputKey + " cannot be matched to any Bayesian node");
            _warningKeys.add(nodeId);
        }

        /*
         * silent if value is not a concept for any reason, but 
         * behaves nicely on nodata.
         */
        if (nodeId != null && value instanceof IConcept) {
            _evidence.add(new Pair<String, String>(nodeId, ((IConcept) value).getLocalName()));
        }
    }

    @Override
    public Object getValue(String outputKey) {

        String nodeId = null;
        boolean isUncertainty = false;

        if (_uncertainties.containsKey(outputKey)) {
            nodeId = _uncertainties.get(outputKey).nodeKey;
            isUncertainty = true;
        } else {
            nodeId = _key2node.get(outputKey);
        }

        List<String> okey = _outputKeys.get(nodeId);

        /*
         * happens after initialization gave errors - which should interrupt the
         * process, but if not (e.g. parallelizing), this avoids a NPE.
         */
        if (nodeId == null || okey == null)
            return null;

        double[] data = new double[okey.size()];
        int i = 0;
        for (String outcome : okey) {
            data[i++] = _inference.getMarginal(nodeId, outcome);
        }

        Object ret = new IndexedCategoricalDistribution(data, _outputClassifications.get(nodeId)
                .getDistributionBreakpoints());

        if (isUncertainty) {
            ret = ((IndexedCategoricalDistribution) ret).getUncertainty();
        }

        return ret;
    }

    @Override
    public void reset() {
        _evidence.clear();
    }

    @Override
    public String toString() {
        return "Bayesian network " + (_network == null ? "" : _network.getName());
    }
}
