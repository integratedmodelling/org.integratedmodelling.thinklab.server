package org.integratedmodelling.thinklab.modelling.states;

import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.modelling.interfaces.IModifiableState;

public abstract class IndexedState extends State implements IModifiableState {

    protected Class<?> _valueClass = null;
    protected boolean _heterogeneous;

    public IndexedState(int multiplicity) {
        super(null);
        value = new Object[multiplicity];
    }

    public IndexedState(IObservable observable) {
        super(observable);
    }

    public IndexedState(IObservable observable, int multiplicity) {
        super(observable);
        value = new Object[multiplicity];
    }
    
    public void setValue(Object[] value) {
        this.value = value;
        this.value = value;
        for (Object o : value) {
            if (o != null) {
                if (_valueClass == null) {
                    _valueClass = o.getClass();
                }
                break;
            }
        }
    }

    public IndexedState(IObservable observable, Object[] value) {
        super(observable);
        setValue(value);
    }

    // special case of the above so we can use a POD array
    public IndexedState(IObservable observable, double[] value) {
        super(observable);
        this.value = value;
        for (Object o : value) {
            if (o != null) {
                if (_valueClass == null) {
                    _valueClass = o.getClass();
                }
                break;
            }
        }
    }

    @Override
    public void setValue(int index, Object val) {
        if (val != null && _valueClass == null) {
            _valueClass = val.getClass();
        }

        if (!_heterogeneous && val != null && _valueClass != null && !val.getClass().equals(_valueClass)) {
            _heterogeneous = true;
        }

        getValue()[index] = val;
    }

    protected Object[] getValue() {
        // just a convenient cast method. use this in subclasses to ensure consistent read behavior.
        return (Object[]) value;
    }

    @Override
    public Object getValue(int contextIndex) {
        return getValue()[contextIndex];
    }

    @Override
    public long getValueCount() {
        return getValue().length;
    }

    @Override
    public Class<?> getDataClass() {
        return _valueClass;
    }

    public boolean isHeterogeneous() {
        return _heterogeneous;
    }
}
