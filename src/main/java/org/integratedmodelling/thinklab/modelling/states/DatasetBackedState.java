package org.integratedmodelling.thinklab.modelling.states;

import org.integratedmodelling.thinklab.api.modelling.IObjectState;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.IScale;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.api.space.ISpatialExtent;
import org.integratedmodelling.thinklab.api.time.ITemporalExtent;
import org.integratedmodelling.thinklab.modelling.interfaces.IExtendedDataset;
import org.integratedmodelling.thinklab.modelling.interfaces.IExtendedDataset.Var;
import org.integratedmodelling.thinklab.modelling.interfaces.IModifiableState;

/**
 * Indexes everything except floating point numbers. Keeps one active timestep at a time; flushes to a NetCDF
 * as soon as a new one is recorded. Wants to be written at incremental T steps as natural - any other use is either
 * an error or a performance nightmare.
 * @author Ferd
 *
 */
public class DatasetBackedState extends IndexedState implements IModifiableState, IObjectState {

    IObserver _observer;
    IScale _scale;
    IExtendedDataset _dataset;
    Var _var;

    /**
     * Passed a dataset for storage. Dataset must have been created with the same scale of
     * the containing subject.
     * 
     * @param observable
     * @param scale
     * @param observer
     * @param dataset
     */
    public DatasetBackedState(IObservable observable, IScale scale, IObserver observer,
            IExtendedDataset dataset) {
        super(observable);
        _scale = scale;
        _observer = observer;
        _dataset = dataset;
        _var = _dataset.getVar(this, -1);
    }

    
    
    @Override
    public void setValue(Object[] value) {
        for (int i = 0; i < value.length; i++) {
            setValue(i, value[i]);
        }
    }

    /* (non-Javadoc)
     * @see org.integratedmodelling.thinklab.modelling.states.IndexedState#setValue(int, java.lang.Object)
     */
    @Override
    public void setValue(int index, Object val) {

        if (val != null && _valueClass == null) {
            _valueClass = val.getClass();
        }
        if (!_heterogeneous && val != null && _valueClass != null && !val.getClass().equals(_valueClass)) {
            _heterogeneous = true;
        }
        _var.setValue(index, val);
    }

    /* (non-Javadoc)
     * @see org.integratedmodelling.thinklab.modelling.states.IndexedState#getValue(int)
     */
    @Override
    public Object getValue(int contextIndex) {
        return _var.getValue(contextIndex);
    }

    @Override
    public long getValueCount() {
        return _var.getValueCount();
    }

    public DatasetBackedState(double[] data, IObservable observable, ISubject context,
            IExtendedDataset dataset) {
        super(observable);
        _scale = context.getScale();
        _observer = observable.getObserver();
        _dataset = dataset;
        _var = _dataset.getVar(this, -1);
        _var.setFromDouble(data);
    }

    @Override
    public boolean isSpatiallyDistributed() {
        return _scale.getSpace() != null && _scale.getSpace().getMultiplicity() > 1;
    }

    @Override
    public boolean isTemporallyDistributed() {
        return _scale.getTime() != null && _scale.getTime().getMultiplicity() > 1;
    }

    @Override
    public boolean isSpatial() {
        return _scale.getSpace() != null;
    }

    @Override
    public boolean isTemporal() {
        return _scale.getTime() != null;
    }

    @Override
    public ISpatialExtent getSpace() {
        return _scale.getSpace();
    }

    @Override
    public ITemporalExtent getTime() {
        return _scale.getTime();
    }

    @Override
    public IObserver getObserver() {
        return _observer;
    }

    @Override
    public IScale getScale() {
        return _scale;
    }

    @Override
    public void flush(int tSlice) {
        // TODO Auto-generated method stub

    }

}
