package org.integratedmodelling.thinklab.modelling.states;

import org.integratedmodelling.thinklab.api.modelling.IObjectState;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.IScale;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.api.space.ISpatialExtent;
import org.integratedmodelling.thinklab.api.time.ITemporalExtent;
import org.integratedmodelling.thinklab.modelling.interfaces.IModifiableState;
import org.integratedmodelling.thinklab.modelling.lang.ConditionalObserver;

public class IndexedObjectState extends IndexedState implements IModifiableState, IObjectState {

    IObserver _observer;
    IScale _scale;

    public IndexedObjectState(IObservable observable, IScale scale, IObserver observer) {
        super(observable, (int) scale.getMultiplicity());
        _scale = scale;
        _observer = observer;
    }

    /*
     * FIXME make this private or remove in favor of the one below it.
     */
    public IndexedObjectState(IObservable observable, ISubject context, IObserver observer) {
        this(observable, context.getScale(), observer);

        /*
         * TODO this should be redundant now - remove when sure
         */
        if (_observer instanceof ConditionalObserver) {
            _observer = ((ConditionalObserver) _observer).getRepresentativeObserver();
        }
    }

    public IndexedObjectState(IObservable observable, ISubject context) {
        this(observable, context, observable.getObserver());
    }

    /*
     * FIXME make this private or remove in favor of the one below
     */
    public IndexedObjectState(Object[] data, IObservable observable, ISubject context, IObserver observer) {
        super(observable, data);
        _scale = context.getScale();
        _observer = observer;
    }

    public IndexedObjectState(double[] data, IObservable observable, ISubject context, IObserver observer) {
        super(observable, data);
        _scale = context.getScale();
        _observer = observer;
    }

    public IndexedObjectState(Object[] data, IObservable observable, ISubject context) {
        this(data, observable, context, observable.getObserver());
    }

    public IndexedObjectState(double[] data, IObservable observable, ISubject context) {
        this(data, observable, context, observable.getObserver());
    }

    @Override
    public boolean isSpatiallyDistributed() {
        return _scale.getSpace() != null && _scale.getSpace().getMultiplicity() > 1;
    }

    @Override
    public boolean isTemporallyDistributed() {
        return _scale.getTime() != null && _scale.getTime().getMultiplicity() > 1;
    }

    @Override
    public boolean isSpatial() {
        return _scale.getSpace() != null;
    }

    @Override
    public boolean isTemporal() {
        return _scale.getTime() != null;
    }

    @Override
    public ISpatialExtent getSpace() {
        return _scale.getSpace();
    }

    @Override
    public ITemporalExtent getTime() {
        return _scale.getTime();
    }

    @Override
    public IObserver getObserver() {
        return _observer;
    }

    @Override
    public IScale getScale() {
        return _scale;
    }

    @Override
    public void flush(int tSlice) {
        // TODO Auto-generated method stub

    }

}
