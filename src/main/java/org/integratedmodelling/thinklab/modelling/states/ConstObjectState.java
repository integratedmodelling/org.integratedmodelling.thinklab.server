package org.integratedmodelling.thinklab.modelling.states;

import java.util.Iterator;

import org.integratedmodelling.thinklab.api.modelling.IConstantState;
import org.integratedmodelling.thinklab.api.modelling.IObjectState;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.IScale;
import org.integratedmodelling.thinklab.api.modelling.IScale.Index;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.api.space.ISpatialExtent;
import org.integratedmodelling.thinklab.api.time.ITemporalExtent;
import org.integratedmodelling.thinklab.modelling.interfaces.IModifiableState;
import org.integratedmodelling.thinklab.modelling.lang.ConditionalObserver;

public class ConstObjectState extends State implements IModifiableState, IObjectState, IConstantState {

    IObserver _observer;
    IScale _scale;
    Object _prototype;
    boolean _heterogeneous;

    public ConstObjectState(IObservable observable, ISubject context, IObserver observer, Object value) {
        this(observable, context.getScale(), observer, value);
    }

    public ConstObjectState(IObservable observable, IScale scale, IObserver observer, Object value) {
        super(observable);

        concept = observable.getType();
        _scale = scale;
        _observer = observer;
        _prototype = value;

        /*
         * TODO this should be redundant now - remove when sure
         */
        if (_observer instanceof ConditionalObserver) {
            _observer = ((ConditionalObserver) _observer).getRepresentativeObserver();
        }
    }

    @Override
    public long getValueCount() {
        return _scale.getMultiplicity();
    }

    @Override
    public boolean isSpatiallyDistributed() {
        return _scale.getSpace() != null && _scale.getSpace().getMultiplicity() > 1;
    }

    @Override
    public boolean isTemporallyDistributed() {
        return _scale.getTime() != null && _scale.getTime().getMultiplicity() > 1;
    }

    @Override
    public boolean isSpatial() {
        return _scale.getSpace() != null;
    }

    @Override
    public boolean isTemporal() {
        return _scale.getTime() != null;
    }

    @Override
    public ISpatialExtent getSpace() {
        return _scale.getSpace();
    }

    @Override
    public ITemporalExtent getTime() {
        return _scale.getTime();
    }

    @Override
    public void setValue(int index, Object val) {
        _prototype = val;
    }

    @Override
    public Object getValue(int contextIndex) {
        return _prototype;
    }

    @Override
    public IObserver getObserver() {
        return _observer;
    }

    public boolean isHeterogeneous() {
        return _heterogeneous;
    }

    @Override
    public Class<?> getDataClass() {
        return _prototype == null ? null : _prototype.getClass();
    }

    @Override
    public IScale getScale() {
        return _scale;
    }

    @Override
    public void flush(int tSlice) {
        // TODO Auto-generated method stub

    }

    @Override
    public Iterator<Object> iterator(Index index) {
        // TODO Auto-generated method stub
        return null;
    }

}
