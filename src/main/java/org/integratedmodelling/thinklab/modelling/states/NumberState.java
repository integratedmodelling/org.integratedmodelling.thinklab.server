//package org.integratedmodelling.thinklab.modelling.states;
//
//import org.integratedmodelling.exceptions.ThinklabException;
//import org.integratedmodelling.thinklab.api.knowledge.ISemanticObject;
//import org.integratedmodelling.thinklab.api.modelling.IExtent;
//import org.integratedmodelling.thinklab.api.modelling.IObserver;
//import org.integratedmodelling.thinklab.api.modelling.IScale;
//import org.integratedmodelling.thinklab.api.modelling.ISubject;
//import org.integratedmodelling.thinklab.modelling.interfaces.IModifiableState;
//import org.integratedmodelling.thinklab.modelling.lang.ConditionalObserver;
//
//public class NumberState extends State implements IModifiableState {
//
//	IObserver _observer;
//	IScale _scale; 
//	
//	public NumberState(ISemanticObject<?> observable, ISubject context, IObserver observer) {
//		
//		concept = observable.getDirectType();
//		_scale = context.getScale();
//		_observer = observer;		
//		/*
//		 * TODO this should be redundant now - remove when sure
//		 */
//		if (_observer instanceof ConditionalObserver) {
//			_observer = ((ConditionalObserver)_observer).getRepresentativeObserver();
//		}
//
//		value = new double[(int)_scale.getMultiplicity()];
//	}
//	
//	@Override
//	public Object getValue(int index) {
//		return ((double[])value)[index];
//	}
//	
//	@Override
//	public Object demote() {
//		return ((double[])value);
//	}
//
//	@Override
//	public double[] getDataAsDoubles() throws ThinklabException {
//		return ((double[])value);
//	}
//
//	@Override
//	public double getDoubleValue(int index) throws ThinklabException {
//		return ((double[])value)[index];
//	}
//
//	@Override
//	public int getValueCount() {
//		return ((double[])value).length;
//	}
//
//	@Override
//	public boolean isSpatiallyDistributed() {
//		return _scale.getSpace() != null && _scale.getSpace().getMultiplicity() > 1;
//	}
//
//	@Override
//	public boolean isTemporallyDistributed() {
//		return _scale.getTime() != null && _scale.getTime().getMultiplicity() > 1;
//	}
//	
//	@Override
//	public boolean isSpatial() {
//		return _scale.getSpace() != null;
//	}
//
//	@Override
//	public boolean isTemporal() {
//		return _scale.getTime() != null;
//	}
//	
//	@Override
//	public IExtent getSpace() {
//		return _scale.getSpace();
//	}
//
//	@Override
//	public IExtent getTime() {
//		return _scale.getTime();
//	}
//
//	@Override
//	public void setValue(int index, Object val) {
//
//		if (val instanceof Number)
//			((double[])value)[index] = ((Number)val).doubleValue();
//		else 
//			((double[])value)[index] = Double.NaN;
//	}
//
//	@Override
//	public String toString() {
//		return "[" + getDirectType() + ": numeric(size = " + ((double[])value).length + ")]";
//	}
//
//	@Override
//	public IObserver getObserver() {
//		return _observer;
//	}
//	
//}
