package org.integratedmodelling.thinklab.modelling.states;

import java.io.File;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.lang.IRemoteSerializable;
import org.integratedmodelling.thinklab.annotation.SemanticLiteral;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.metadata.IMetadata;
import org.integratedmodelling.thinklab.api.modelling.IAccessor;
import org.integratedmodelling.thinklab.api.modelling.IAction;
import org.integratedmodelling.thinklab.api.modelling.IMeasuringObserver;
import org.integratedmodelling.thinklab.api.modelling.IScale.Index;
import org.integratedmodelling.thinklab.api.modelling.IState;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.common.data.IndexedCategoricalDistribution;
import org.integratedmodelling.thinklab.common.utils.NameGenerator;
import org.integratedmodelling.thinklab.common.vocabulary.Observable;
import org.integratedmodelling.thinklab.geospace.Geospace;
import org.integratedmodelling.thinklab.geospace.extents.SpaceExtent;
import org.integratedmodelling.thinklab.modelling.lang.Metadata;
import org.integratedmodelling.thinklab.modelling.lang.StateAccessor;
import org.integratedmodelling.thinklab.time.Time;

import com.ibm.icu.text.NumberFormat;

/**
 * Base class for states, taking care of semantic object business and simplified accessor interface.
 * 
 * @author Ferd
 * 
 */
public abstract class State extends SemanticLiteral<Object> implements IState, IRemoteSerializable {

    protected Metadata    _metadata;
    protected boolean     _isRaw      = false;
    protected IObservable _observable = null;

    class It implements Iterator<Object> {

        Iterator<Integer> _sit;

        It(Iterator<Integer> sit) {
            _sit = sit;
        }

        @Override
        public boolean hasNext() {
            return _sit.hasNext();
        }

        @Override
        public Object next() {
            return getValue(_sit.next());
        }

        @Override
        public void remove() {
        }

    }

    /*
     * unique ID for observation paths
     */
    protected String _internalID = NameGenerator.shortUUID();

    protected State(IObservable observable) {
        super(observable == null ? null : observable.getType(), null);
        _observable = observable;
    }

    public String getInternalId() {
        return _internalID;
    }

    @Override
    public IObservable getObservable() {
        return _observable;
    }

    public abstract Object getValue(int index);

    class TStateAccessor extends StateAccessor {

        int _idx;

        /*
         * give it the ID of the model that produced it. If not, things will get out of whack when using as a
         * dependency in computing accessors.
         * 
         * NOTE: the field used must be the same that Bytecode.createState() is setting.
         */
        TStateAccessor(ArrayList<IAction> actions, IMonitor monitor) {
            super(actions, monitor);
            String id = getMetadata().getString(IMetadata.DC_LABEL);
            if (id != null) {
                setName(id);
            }
        }

        @Override
        public void process(int stateIndex) throws ThinklabException {
            _idx = stateIndex;
        }

        @Override
        public Object getValue(String outputKey) {
            return State.this.getValue(_idx);
        }
    }

    @Override
    public IMetadata getMetadata() {

        if (_metadata == null) {
            _metadata = new Metadata();
        }
        return _metadata;
    }

    public IAccessor getAccessor(IMonitor monitor) {
        return new TStateAccessor(new ArrayList<IAction>(), monitor);
    }

    public abstract Class<?> getDataClass();

    public void setRaw(boolean b) {
        _isRaw = b;
    }

    /**
     * True if this state comes from a raw accessor, meaning it has not been interpreted by a model and there
     * may be a non-raw one for the same concept which should be used instead.
     * @return
     */
    public boolean isRaw() {
        return _isRaw;
    }

    @Override
    public Object adapt() {

        Map<String, Object> ret = new HashMap<String, Object>();
        if (getObserver() instanceof IRemoteSerializable) {
            ret.put("observer", ((IRemoteSerializable) getObserver()).adapt());
        }
        ret.put("observable", ((Observable) getObservable()).adapt());
        ret.put("internal-id", _internalID);

        // ColorMap colormap = (ColorMap) VisualizationFactory.getColormap(this);
        // Histogram histogram = VisualizationFactory.getHistogram(this, 10);
        //
        // if (histogram != null) {
        // ret.put("histogram", histogram.toString());
        // }
        // if (colormap != null) {
        // ret.put("colormap", colormap.toString());
        // }
        return ret;
    }

    @Override
    public Iterator<Object> iterator(Index index) {
        return new It(index.iterator());
    }

    public void persist(File file) throws ThinklabException {

        if (isSpatiallyDistributed()) {
            Geospace.get().persistState(this, file);
        } else if (isTemporallyDistributed()) {
            Time.get().persistState(this, file);
        }

    }

    @Override
    public BitSet getMask() {
        if (isSpatiallyDistributed() && !isTemporallyDistributed()) {
            SpaceExtent space = (SpaceExtent) getSpace();
            return space.getMask();
        }
        return null;
    }

    /**
     * Returns a serializable descriptor for the n-th state.
     * 
     * @param idx
     * @return
     */
    public Map<?, ?> describeValue(int idx) {

        HashMap<String, Object> ret = new HashMap<String, Object>();

        Object o = this.getValue(idx);
        if (o == null || (o instanceof Double && Double.isNaN((Double) o))) {
            o = "No data";
        } else if (o instanceof IndexedCategoricalDistribution) {
            ret.put("distribution", ((IndexedCategoricalDistribution) o).getData());
            ret.put("ranges", ((IndexedCategoricalDistribution) o).getRanges());
            ret.put("uncertainty", ((IndexedCategoricalDistribution) o).getUncertainty());
            o = "m=" +
                    NumberFormat.getInstance().format(((IndexedCategoricalDistribution) o).getMean())
                    + ", s=" +
                    NumberFormat.getInstance().format(((IndexedCategoricalDistribution) o).getUncertainty());
        } else if (o instanceof Boolean) {
            o = getDirectType().getLocalName() + ((Boolean) o ? " present" : " absent");
        } else if (o instanceof IConcept) {
            o = ((IConcept) o).getLocalName();
        } else {
            o = o.toString();
        }

        if (getObserver() instanceof IMeasuringObserver) {
            o = o + " " + ((IMeasuringObserver) getObserver()).getUnit();
        }

        ret.put("value", o);
        return ret;
    }
}
