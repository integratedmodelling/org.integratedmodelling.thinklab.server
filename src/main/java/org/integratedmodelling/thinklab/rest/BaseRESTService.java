/**
 * Copyright 2011 The ARIES Consortium (http://www.ariesonline.org) and
 * www.integratedmodelling.org. 

   This file is part of Thinklab.

   Thinklab is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published
   by the Free Software Foundation, either version 3 of the License,
   or (at your option) any later version.

   Thinklab is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Thinklab.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.integratedmodelling.thinklab.rest;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabIOException;
import org.integratedmodelling.exceptions.ThinklabInternalErrorException;
import org.integratedmodelling.exceptions.ThinklabResourceNotFoundException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.list.Escape;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.knowledge.ISemanticObject;
import org.integratedmodelling.thinklab.api.lang.IList;
import org.integratedmodelling.thinklab.api.runtime.IServer;
import org.integratedmodelling.thinklab.api.runtime.ISession;
import org.integratedmodelling.thinklab.api.runtime.IUser;
import org.integratedmodelling.thinklab.common.utils.MiscUtilities;
import org.integratedmodelling.thinklab.common.utils.URLUtils;
import org.integratedmodelling.thinklab.rest.interfaces.IRESTHandler;
import org.integratedmodelling.thinklab.rest.interfaces.IStatelessService;
import org.json.JSONException;
import org.json.JSONObject;
import org.restlet.data.CharacterSet;
import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.data.Method;
import org.restlet.data.Parameter;
import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.representation.Variant;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;

/**
 * Default resource handler always responds JSON, with fields pointing to results or 
 * further resource URNs.
 * 
 * The handler methods should return one of the wrap() functions.
 * 
 * @author Ferdinando
 *
 */
public abstract class BaseRESTService extends ServerResource implements IRESTHandler {

    // result types
    // FIXME - probably vastly obsolete
    static public final int                 VOID         = 0;
    static public final int                 INT          = 1;
    static public final int                 DOUBLE       = 2;
    static public final int                 TEXT         = 3;
    static public final int                 URN          = 4;
    static public final int                 INTS         = 5;
    static public final int                 DOUBLES      = 6;
    static public final int                 TEXTS        = 7;
    static public final int                 URNS         = 8;

    // this one means "you're on your own figuring it out" and is only used for internal
    // hand-shaking commands where the result structure is known to the client.
    static public final int                 OBJECT       = 9;
    static public final int                 LIST         = 10;

    ArrayList<String>                       _context     = new ArrayList<String>();
    HashMap<String, String>                 _query       = new HashMap<String, String>();
    String                                  _MIME        = null;
    Date                                    start        = null;
    int                                     resultStatus = IServer.OK;
    private ResultHolder                    rh           = new ResultHolder();

    private ArrayList<Pair<String, String>> _downloads   = new ArrayList<Pair<String, String>>();

    String                                  error        = null, info = null, warn = null;

    boolean                                 _processed   = false;

    static RESTTaskScheduler                _scheduler   = null;

    public BaseRESTService() {
        final Set<Method> allowedMethods = new HashSet<Method>();
        allowedMethods.add(Method.POST);
        getVariants().add(new Variant(MediaType.TEXT_ALL));
        setAnnotated(true);
        setNegotiated(false);
        setAllowedMethods(allowedMethods);
    }

    /**
     * Redefine and skip the Restlet annotations. 
     * FIXME have this return objects and ignore JSON; handle JSON translation transparently.
     * 
     * @param entity
     * @return
     * @throws Exception 
     */
    public abstract Representation service(Representation entity) throws Exception;

    @Override
    protected Representation post(Representation entity) throws ResourceException {

        Representation ret = null;
        try {
            ret = service(entity);
        } catch (Exception e) {
            fail(e);
        }
        getResponse().setEntity(ret == null ? wrap() : ret);
        return entity;
    }

    /**
     * Call this one to ensure that a restricted command is allowed for the
     * current user. The actual session user is checked against the requested
     * privilege. If RESTManager returns true to allowPrivilegedLocalConnection
     * (currently always false) any connection coming from localhost is allowed
     * privileged access.
     * 
     * @param concept the user role required for the command. Must resolve to a 
     *        valid concept.
     * @throws ThinklabException if the user is not allowed to run the command or 
     * 		   is undefined
     */
    protected boolean checkPrivileges(String neededRole) throws ThinklabException {

        String domain = getRequest().getResourceRef().getHostDomain();
        boolean isLocal = (domain != null && (domain.equals("127.0.0.1") || domain.equals("localhost")));

        if (isLocal && RESTManager.get().allowPrivilegedLocalConnections())
            return true;

        if (getSession() == null) {
            fail("no user privileges for command");
            return false;
        }

        IUser user = getSession().getUserModel().getUser();
        if (user == null || !user.getRoles().contains(neededRole)) {
            fail("not enough user privileges for command");
            return false;
        }
        return true;
    }

    protected static RESTTaskScheduler getScheduler() {

        if (_scheduler == null) {
            int ntasks = Integer.parseInt(Thinklab.get().getProperties()
                    .getProperty(RESTTaskScheduler.N_TASKS_PROPERTY, "8"));
            _scheduler = new RESTTaskScheduler(ntasks);
            _scheduler.start();
        }
        return _scheduler;
    }

    public String getServiceName() {
        return getRequest().getOriginalRef().getLastSegment();
    }

    /*
     * TODO pass command for info, record time, track listeners for task-specific logging
     */
    public Representation enqueue(final Thread thread) {

        getScheduler().enqueue(thread);

        JSONObject ret = new JSONObject();
        try {
            ret.put("__taskid", thread.getId() + "");
            ret.put("__status", IServer.SCHEDULED);
        } catch (JSONException e) {
            // come on
        }
        return new JsonRepresentation(ret);
    }

    protected void addDownload(String handle, String filename) {
        _downloads.add(new Pair<String, String>(filename, handle));
    }

    /**
     * Takes the session from the session parameter, which must be in all
     * services that require an active session to operate. If the service is tagged as being able
     * to work without one, it will be allowed to return null without 
     * error, which of course the service must be ready to handle.
     * 
     * @throws ThinklabInternalErrorException
     */
    public ISession getSession() throws ThinklabException {

        String id = getArgument("session");

        if (id == null && !(this instanceof IStatelessService))
            throw new ThinklabInternalErrorException("REST command did not specify required session ID");

        return RESTManager.get().getSession(id);
    }

    /**
     * Return the file correspondent to the handle previously returned by 
     * getFileName() and supposedly uploaded by a client.
     * 
     * @param argument
     * @return
     * @throws ThinklabException 
     */
    protected File getFileForHandle(String handle, boolean mustExist) throws ThinklabException {

        File ret = new File(Thinklab.get().getScratchArea() + File.separator + "rest/tmp" + File.separator
                + handle);

        if (mustExist && !ret.exists())
            throw new ThinklabResourceNotFoundException(handle);

        return ret;
    }

    /**
     * Return a file path and "handle" for a file that will be created and returned to the 
     * client to retrieve through receive(handle).

     * @param fileName the file the user wants us to create
     * @param session current session
     * @return pair<file, handle> - create file in File, return handle to client using 
     * 		   addDownload(handle, fileName)
     * @throws ThinklabException
     */
    public static Pair<File, String> getFileName(String fileName, String prefixDir) throws ThinklabException {

        Pair<File, String> ret = null;
        File sdir = new File(Thinklab.get().getScratchArea() + File.separator + "rest/tmp" + File.separator
                + prefixDir);
        sdir.mkdirs();

        String ext = MiscUtilities.getFileExtension(fileName);
        ext = (ext == null || ext.isEmpty()) ? ".tmp" : ("." + ext);
        try {
            File out = File.createTempFile("upl", ext, sdir);
            String handle = prefixDir + File.separator + MiscUtilities.getFileName(out.toString());
            ret = new Pair<File, String>(out, handle);
        } catch (IOException e) {
            throw new ThinklabIOException(e);
        }

        return ret;
    }

    /**
     * Publish given file in default publish directory
     * 
     * @param first
     * @param _publish2
     * @throws ThinklabIOException 
     */
    protected void publish(File file, String publish) throws ThinklabIOException {

        String pdir = System.getenv("THINKLAB_PUBLISH_DIRECTORY");
        if (pdir == null)
            pdir = "./publish";
        File out = new File(pdir + File.separator + publish);
        File dir = MiscUtilities.getPath(out.toString());
        dir.mkdirs();
        URLUtils.copy(file, out);
    }

    /**
     * Return the elements of the request path after the service identifier, in the same
     * order they have in the URL.
     * 
     * @return
     * @throws ThinklabException
     */
    public List<String> getRequestPath() throws ThinklabException {

        if (!_processed)
            processRequest();
        return _context;
    }

    /**
     * Get a map of all query arguments, no matter what method was used in the request.
     * 
     * @return
     * @throws ThinklabException
     */
    public HashMap<String, String> getArguments() throws ThinklabException {

        if (!_processed)
            processRequest();
        return _query;
    }

    public String getArgument(String id, String defvalue) throws ThinklabException {
        String ret = getArgument(id);
        return ret == null ? defvalue : ret;
    }

    public String getArgument(String id) throws ThinklabException {
        return getArguments().get(id);
    }

    public String requireArgument(String id) throws ThinklabException {
        String ret = getArguments().get(id);
        if (ret == null)
            throw new ThinklabResourceNotFoundException("required argument " + id
                    + " not passed in REST request");
        return ret;
    }

    // only used in CheckWaiting for now - set the result object directly
    protected void setResult(ResultHolder result) {
        rh = result;
    }

    protected void keepWaiting(String taskId) {
        put("__taskid", taskId);
        resultStatus = IServer.SCHEDULED;
    }

    /**
     * Return the string correspondent to the MIME type that was selected by the URL
     * extension. Will return null if no extension was used.
     * 
     * @return
     */
    protected String getMIMEType() {
        if (!_processed)
            processRequest();
        return _MIME;
    }

    private void processRequest() {

        Form form = getRequest().getResourceRef().getQueryAsForm();
        for (Parameter parameter : form) {
            _query.put(parameter.getName(), Escape.fromURL(parameter.getValue()));
        }

        _processed = true;
    }

    @Override
    protected void doInit() throws ResourceException {
        // TODO Auto-generated method stub
        super.doInit();
        start = new Date();
    }

    @Override
    protected void doRelease() throws ResourceException {

        Date date = new Date();

        Representation r = getResponseEntity();

        if (r instanceof JsonRepresentation) {
            try {

                ((JsonRepresentation) r).getJsonObject().put("__elapsed",
                        ((float) (date.getTime() - start.getTime())) / 1000.0f);

                ((JsonRepresentation) r).getJsonObject().put("__endTime", date.getTime());

            } catch (JSONException e) {
                throw new ResourceException(e);
            }
        }

        super.doRelease();
    }

    /**
     * Return this when you have a JSON object of your own
     * 
     * @param jsonObject
     * @return
     */
    protected JsonRepresentation wrap(JSONObject jsonObject) {
        JsonRepresentation jr = new JsonRepresentation(jsonObject);
        jr.setCharacterSet(CharacterSet.UTF_8);
        return jr;
    }

    /**
     * If this is used, "return wrap()" should be the last call in your handler function. Any 
     * data set through this one or setResult will be automatically returned in a JSON object.
     * 
     * @param key
     * @param o
     */
    protected void put(String key, Object... o) {
        rh.put(key, o);
    }

    public void setResult(Object o) {
        if (o instanceof IList)
            rh.setList((IList) o);
        rh.setResult(o);
    }

    protected void setResult(int... iResult) {
        rh.setResult(iResult);
    }

    protected void setResult(double... dResult) {
        rh.setResult(dResult);
    }

    protected void setResult(String... tResult) {
        rh.setResult(tResult);
    }

    protected void addResult(String rURN, String rMIME) {
        rh.addResult(rURN, rMIME);
    }

    protected void fail() {
        resultStatus = IServer.ERROR;
        Thinklab.get().logger().error(this);
    }

    protected void fail(String message) {
        resultStatus = IServer.ERROR;
        error = message;
        Thinklab.get().logger().error(message);
    }

    protected void fail(Throwable e) {
        resultStatus = IServer.ERROR;
        error = e.getMessage();
        rh.put(IServer.EXCEPTION_CLASS, e.getClass().getCanonicalName());
        rh.put(IServer.STACK_TRACE, MiscUtilities.getStackTrace(e));
        Thinklab.get().logger().error(this, e);
    }

    protected void warn(String s) {
        warn = s;
        Thinklab.get().logger().warn(s);
    }

    protected void info(String s) {
        info = s;
        Thinklab.get().logger().info(s);
    }

    /**
     * Return this if you have used any of the put() or setResult() functions. Will create and 
     * wrap a suitable JSON object automatically.
     * @return
     */
    protected JsonRepresentation wrap() {

        JSONObject jsonObject = new JSONObject();
        rh.toJSON(jsonObject);

        try {

            jsonObject.put("__status", resultStatus);

            if (warn != null) {
                jsonObject.put("__warn", warn);
            }
            if (info != null) {
                jsonObject.put("__info", info);
            }
            if (error != null) {
                jsonObject.put("__error", error);
            }

            if (_downloads.size() > 0) {
                Object[] oj = new Object[_downloads.size()];
                int i = 0;
                for (Pair<String, String> dl : _downloads) {
                    oj[i++] = new String[] { dl.getFirst(), dl.getSecond() };
                }
                jsonObject.put("__downloads", oj);
            }

        } catch (JSONException e) {
            throw new ThinklabRuntimeException(e);
        }

        JsonRepresentation jr = new JsonRepresentation(jsonObject);
        jr.setCharacterSet(CharacterSet.UTF_8);
        return jr;
    }
}
