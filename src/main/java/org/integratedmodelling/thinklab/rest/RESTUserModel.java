/**
 * Copyright 2011 The ARIES Consortium (http://www.ariesonline.org) and
 * www.integratedmodelling.org. 

   This file is part of Thinklab.

   Thinklab is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published
   by the Free Software Foundation, either version 3 of the License,
   or (at your option) any later version.

   Thinklab is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Thinklab.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.integratedmodelling.thinklab.rest;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Properties;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.interpreter.ModelGenerator;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.knowledge.ISemanticObject;
import org.integratedmodelling.thinklab.api.lang.IModelResolver;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.runtime.ISession;
import org.integratedmodelling.thinklab.api.runtime.IUser;
import org.integratedmodelling.thinklab.api.runtime.IUserModel;
import org.integratedmodelling.thinklab.modelling.ModelManager;
import org.integratedmodelling.thinklab.modelling.monitoring.Monitor;
import org.integratedmodelling.thinklab.proxy.ModellingModule;
import org.integratedmodelling.thinklab.runtime.Session;

import com.google.inject.Guice;
import com.google.inject.Injector;

/**
 * Contains a monitor for base (non task-related) operations.
 * 
 * @author Ferd
 *
 */
public class RESTUserModel implements IUserModel {

    ISession session = null;
    IMonitor monitor;
    IUser    user;

    public RESTUserModel(HashMap<String, String> arguments, IUser user, Session session) {
        this.user = user;
        this.session = session;
        this.monitor = new Monitor(-1, session);
    }

    @Override
    public InputStream getInputStream() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public PrintStream getOutputStream() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void initialize(ISession session) {
        this.session = session;
    }

    public IUser getUser() {
        return user;
    }

    ModelGenerator _mg       = null;
    IModelResolver _resolver = null;

    public ModelGenerator getModelGenerator() {

        if (_mg == null) {
            Injector injector = Guice.createInjector(new ModellingModule());
            _mg = injector.getInstance(ModelGenerator.class);
        }
        return _mg;
    }

    public IMonitor getMonitor() {
        return monitor;
    }

    public IModelResolver getResolver(int taskId) {
        _resolver = ((ModelManager) Thinklab.get().getModelManager()).getInteractiveResolver(session
                .getUserModel().getInputStream(), session.getUserModel().getOutputStream());
        return _resolver;
    }

}
