package org.integratedmodelling.thinklab.rest;

import org.integratedmodelling.exceptions.ThinklabAuthorizationException;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.runtime.ISession;
import org.integratedmodelling.thinklab.api.runtime.IUser;
import org.integratedmodelling.thinklab.command.Command;
import org.integratedmodelling.thinklab.interfaces.commands.ICommandHandler;
import org.integratedmodelling.thinklab.rest.interfaces.IStatelessService;

/**
 * Abstract base class for commands that require no session ID but the ID of a previously
 * authorized user. Will intercept the default execute() and defer to another with the user
 * as a parameter instead of the session.
 * 
 * @author Ferd
 *
 */
public abstract class AuthorizedCommand implements ICommandHandler, IStatelessService {

    @Override
    public Object execute(Command command, ISession session) throws ThinklabException {
        
        IUser user = null;
        String skey = command.getArgumentAsString("key");
        
        if (skey != null) {
            user = RESTManager.get().checkAuthorization(skey);
        }

        if (user == null) {
            throw new ThinklabAuthorizationException("command requires previous authorization");
        }
        
        return execute(command, user);
    }

    protected abstract Object execute(Command command, IUser user) throws ThinklabException;

}
