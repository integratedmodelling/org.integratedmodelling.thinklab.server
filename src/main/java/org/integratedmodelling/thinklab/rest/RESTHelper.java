package org.integratedmodelling.thinklab.rest;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.integratedmodelling.auth.data.Datarecord;
import org.integratedmodelling.thinklab.api.project.IProject;
import org.integratedmodelling.thinklab.api.runtime.IServer;
import org.integratedmodelling.thinklab.api.runtime.IUser;
import org.integratedmodelling.thinklab.api.runtime.IUserAssets;
import org.integratedmodelling.thinklab.common.utils.Escape;

import us.monoid.json.JSONObject;
import us.monoid.web.Resty;

public class RESTHelper {

    public static String getURLForPrimaryServerCommand(String serverURL, String command, Object... arguments) {

        String url = serverURL + "/" + command;
        if (arguments != null && arguments.length > 0) {
            for (int i = 0; i < arguments.length; i++)
                url += (i == 0 ? "?" : "&") + Escape.forURL(arguments[i].toString()) + "="
                        + Escape.forURL(arguments[++i].toString());
        }
        return url;
    }

    @SuppressWarnings("unchecked")
    public static <T> T getAsset(String urn, IUser user, Class<T> cls) {

        if (cls.equals(Datarecord.class)) {

            String url =
                    getURLForPrimaryServerCommand(user.getServerURL(), "get-asset",
                            "asset", urn,
                            "type", IUserAssets.TYPE_DATA_RECORD,
                            "key", user.getSecurityKey());
            try {
                JSONObject ret = new Resty().json(url).object();
                Map<String, Object> data = new HashMap<String, Object>();

                if (ret.get("urn") == null)
                    return null;

                for (Iterator<String> it = ret.keys(); it.hasNext();) {
                    String k = it.next();
                    data.put(k, ret.get(k));
                }
                return (T) new Datarecord(data);

            } catch (Exception e) {
                return null;
            }

        } else if (cls.equals(IProject.class)) {
            // File temp = File.createTempFile("project", "zip");
            // String url =
            // ((User) _user).getURLForPrimaryServerCommand("get-asset",
            // "asset", id,
            // "type", IUserAssets.TYPE_PROJECT,
            // "key", _user.getSecurityKey());
            //
            // if (URLUtils.copy(new URL(url), temp) > 0) {
            // FolderZiper.unzip(temp, deployDir);
            // }
            //
            // /*
            // * create timestamp file
            // */
            // FileUtils.writeStringToFile(tst, remoteTimestamp + "");
        }

        return null;
    }
}
