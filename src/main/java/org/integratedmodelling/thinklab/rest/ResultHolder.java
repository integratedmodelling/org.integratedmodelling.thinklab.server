/**
 * Copyright 2011 The ARIES Consortium (http://www.ariesonline.org) and
 * www.integratedmodelling.org. 

   This file is part of Thinklab.

   Thinklab is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published
   by the Free Software Foundation, either version 3 of the License,
   or (at your option) any later version.

   Thinklab is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Thinklab.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.integratedmodelling.thinklab.rest;

import java.util.ArrayList;
import java.util.HashMap;

import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.thinklab.api.lang.IList;
import org.integratedmodelling.utils.ScrewyJSONUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Utility class that can be set with a result and translate it into a JSON object.
 * 
 * @author ferdinando.villa
 *
 */
public class ResultHolder {

    HashMap<String, Object> _parameters = new HashMap<String, Object>();
    int resultType = BaseRESTService.VOID;

    // result holders. Could be more efficient and a lot messier.
    ArrayList<Integer> _intResult = null;
    ArrayList<Double> _dblResult = null;
    ArrayList<String> _txtResult = null;
    Object _objResult = null;
    ArrayList<String> _urnResult = null;
    ArrayList<String> _urnTypes = null;
    private IList _lstResult;

    /**
     * If this is used, "return wrap()" should be the last call in your handler function. Any 
     * data set through this one or setResult will be automatically returned in a JSON object.
     * 
     * @param key
     * @param o
     */
    public void put(String key, Object... o) {
        if (o == null || o.length == 0)
            _parameters.put(key, "nil");
        else if (o.length == 1)
            _parameters.put(key, o[0]);
        else {

            /*
             * if a tree node, do our tree thing
             * TODO when we know what it is, of course.
             */

           
            /*
             * else make a JSONArray
             */
            try {
                JSONArray ja = (JSONArray) ScrewyJSONUtils.wrap(o);
                _parameters.put(key, ja);
            } catch (JSONException e) {
                throw new ThinklabRuntimeException(e);
            }
        }
    }

    public void setResult(int... iResult) {
        _intResult = new ArrayList<Integer>();
        for (int i : iResult)
            _intResult.add(i);
        resultType = _intResult.size() > 1 ? BaseRESTService.INTS : BaseRESTService.INT;
    }

    public void setResult(double... dResult) {
        _dblResult = new ArrayList<Double>();
        for (double i : dResult)
            _dblResult.add(i);
        resultType = _dblResult.size() > 1 ? BaseRESTService.DOUBLES : BaseRESTService.DOUBLE;
    }

    public void setResult(Object o) {
        _objResult = o;
        resultType = BaseRESTService.OBJECT;
    }

    public void setResult(String... tResult) {
        _txtResult = new ArrayList<String>();
        for (String i : tResult)
            _txtResult.add(i);
        resultType = _txtResult.size() > 1 ? BaseRESTService.TEXTS : BaseRESTService.TEXT;
    }

    public void addResult(String rURN, String rMIME) {

        if (_urnResult == null) {
            _urnResult = new ArrayList<String>();
            _urnTypes = new ArrayList<String>();
        }

        _urnResult.add(rURN);
        _urnTypes.add(rMIME);

        resultType = _urnResult.size() > 1 ? BaseRESTService.URNS : BaseRESTService.URN;
    }

    public void toJSON(JSONObject jsonObject) {

        /*
         * TODO put any result; add type if indirect (URN)
         */
        try {

            jsonObject.put("__type", resultType);

            switch (resultType) {
            case BaseRESTService.DOUBLE:
                jsonObject.put("__result", _dblResult.size() == 0 ? JSONObject.NULL : _dblResult.get(0));
                break;
            case BaseRESTService.INT:
                jsonObject.put("__result", _intResult.size() == 0 ? JSONObject.NULL : _intResult.get(0));
                break;
            case BaseRESTService.TEXT:
                jsonObject.put("__result", _txtResult.size() == 0 ? JSONObject.NULL : _txtResult.get(0));
                break;
            case BaseRESTService.URN:
                jsonObject.put("__result", _urnResult.size() == 0 ? JSONObject.NULL : _urnResult.get(0));
                break;
            case BaseRESTService.DOUBLES:
                jsonObject.put("__result", _dblResult);
                break;
            case BaseRESTService.INTS:
                jsonObject.put("__result", _intResult);
                break;
            case BaseRESTService.TEXTS:
                jsonObject.put("__result", _txtResult);
                break;
            case BaseRESTService.URNS:
                jsonObject.put("__result", _urnResult);
                break;
            case BaseRESTService.OBJECT:
                jsonObject.put("__result", _objResult);
                break;
            case BaseRESTService.LIST:
                // TODO check appropriate representations
                jsonObject.put("__result", _lstResult.toString());
                break;
            }

            /*
             * put any fields
             */
            for (String s : _parameters.keySet()) {
                jsonObject.put(s, _parameters.get(s));
            }
        } catch (JSONException e) {
            throw new ThinklabRuntimeException(e);
        }

    }

    public void setList(IList o) {
        _lstResult = o;
        resultType = BaseRESTService.LIST;
    }
}
