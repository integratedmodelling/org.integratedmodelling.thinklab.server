package org.integratedmodelling.thinklab.rest.interfaces;

/**
 * Tag interface to classify those services that don't need a verified session ID in their
 * parameters - typically ping and authorize. (Yes, I know that all REST sessions are stateless by definition).
 * 
 * TODO it's probably more elegant (given the REST definition) to tag the services that DO require
 * a session, but it's more work and all thinklab services except a couple require a session.
 * 
 * @author Ferd
 *
 */
public interface IStatelessService {

}
