package org.integratedmodelling.thinklab.rest;

import java.io.File;

import org.restlet.data.MediaType;

/**
 * Simple wrapper for a file. If this is returned by a REST command, the file is automatically
 * dispatched for download in the servlet response.
 * 
 * @author Ferd
 *
 */
public class FileMedia {

    public File file;
    public MediaType type;
    
    public FileMedia(File file, MediaType type) {
        this.file = file;
        this.type = type;
    }
}
