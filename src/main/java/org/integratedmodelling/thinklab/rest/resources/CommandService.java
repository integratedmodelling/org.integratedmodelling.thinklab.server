/**
 * Copyright 2011 The ARIES Consortium (http://www.ariesonline.org) and
 * www.integratedmodelling.org. 

   This file is part of Thinklab.

   Thinklab is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published
   by the Free Software Foundation, either version 3 of the License,
   or (at your option) any later version.

   Thinklab is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Thinklab.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.integratedmodelling.thinklab.rest.resources;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabIOException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.lang.IRemoteSerializable;
import org.integratedmodelling.thinklab.api.lang.ICommand;
import org.integratedmodelling.thinklab.api.lang.IPrototype;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.modelling.visualization.IImageMedia;
import org.integratedmodelling.thinklab.api.modelling.visualization.IMedia;
import org.integratedmodelling.thinklab.command.Command;
import org.integratedmodelling.thinklab.command.CommandDeclaration;
import org.integratedmodelling.thinklab.command.CommandManager;
import org.integratedmodelling.thinklab.modelling.monitoring.Monitor;
import org.integratedmodelling.thinklab.rest.BaseRESTService;
import org.integratedmodelling.thinklab.rest.FileMedia;
import org.integratedmodelling.utils.ScrewyJSONUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;
import org.restlet.data.CharacterSet;
import org.restlet.data.Disposition;
import org.restlet.data.MediaType;
import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.ByteArrayRepresentation;
import org.restlet.representation.FileRepresentation;
import org.restlet.representation.Representation;
import org.restlet.resource.Get;

/**
 * Execute a language statement and optionally produce a visualization and/or dataset
 * for the results.
 * 
 * @author ferdinando.villa
 *
 */
public class CommandService extends BaseRESTService {

    private IPrototype getCommandPrototype() throws ThinklabException {
        return CommandManager.get().requireDeclarationForCommand(getServiceName());
    }

    public CommandService() {
    }

    class RunThread extends Thread {

    }

    @Get
    public Representation service(Representation entity) {

        try {
            IPrototype prototype = getCommandPrototype();
            ICommand command = validateCommand(prototype, new Monitor(-1, prototype.requiresSession() ? getSession()
                    : null));
            try {
                /*
                 * exec command and prepare result according to type.
                 */
                return processResult(CommandManager.get()
                        .submitCommand(command, prototype.requiresSession() ? getSession() : null),
                        command.getMonitor());

            } catch (Exception e) {
                /*
                 * just report, then let the outer try/catch handle it
                 */
                command.getMonitor().error(e.getMessage());
                throw e;
            }
        } catch (Exception e) {
            fail(e);
        }

        return wrap();
    }

    /**
     * TODO this should be moved to the session, although the current design makes it difficult. Showing or
     * transmitting whatever command result to the requester we got should be the session's problem.
     * 
     * @param result
     * @param monitor
     * @return
     * @throws ThinklabException
     */
    private Representation processResult(Object result, IMonitor monitor) throws ThinklabException {

        JsonRepresentation ret = null;

        try {

            if (result instanceof IMedia) {

                if (result instanceof IImageMedia) {
                    BufferedImage image = null;
                    Image img = ((IImageMedia) result).getImage();

                    if (img instanceof BufferedImage) {
                        image = (BufferedImage) img;

                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        try {
                            ImageIO.write(image, "png", baos);
                        } catch (IOException e) {
                            throw new ThinklabIOException(e);
                        }
                        // TODO generalize
                        return new ByteArrayRepresentation(baos.toByteArray(), new MediaType("image/png"));
                    } else {
                        this.fail();
                    }
                }
            
            } else if (result instanceof BufferedImage) {
                
                BufferedImage image = (BufferedImage) result;

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                try {
                    ImageIO.write(image, "png", baos);
                } catch (IOException e) {
                    throw new ThinklabIOException(e);
                }
                // TODO generalize
                return new ByteArrayRepresentation(baos.toByteArray(), new MediaType("image/png"));
                
            } else if (result instanceof FileMedia) {
                
                FileMedia fm = (FileMedia)result;
                FileRepresentation rep = new FileRepresentation(fm.file, fm.type);
                Disposition disp = new Disposition(Disposition.TYPE_ATTACHMENT);
                disp.setFilename(fm.file.getName());
                disp.setSize(fm.file.length());
                rep.setDisposition(disp);
                return rep;
            
            } else if (result instanceof IRemoteSerializable) {
                
                ret = new JsonRepresentation((JSONObject)ScrewyJSONUtils.wrap(((IRemoteSerializable) result).adapt()));
            
            } else if (result instanceof Map<?,?>) { 
                
                ret = new JsonRepresentation((JSONObject)ScrewyJSONUtils.wrap(result));
                
            } else if (result instanceof List<?>) {
                
                ret = new JsonRepresentation((JSONArray)ScrewyJSONUtils.wrap(result));
            
            } else if (result instanceof String) {

                ret = new JsonRepresentation(new JSONStringer().value(result));

            } else if (result instanceof Number) {

                ret = new JsonRepresentation(new JSONStringer().value(result));

            }

        } catch (JSONException e) {
            // come on, it's a map.
        }

        if (ret != null) {
            ret.setCharacterSet(CharacterSet.UTF_8);
        }

        return ret == null ? wrap() : ret;
    }

    private ICommand validateCommand(IPrototype commandPrototype, IMonitor monitor) throws ThinklabException {

        // FIXME adjourn to modernized thinklab-common type when ready
        CommandDeclaration declaration = (CommandDeclaration) commandPrototype;
        Command cmd = new Command(declaration);

        for (String arg : declaration.getMandatoryArgumentNames()) {
            Object o = getArgument(arg);
            if (o == null) {
                throw new ThinklabValidationException("service " + declaration.getId()
                        + " requires argument " + arg);
            }
            cmd.addArgument(arg, o);
        }

        for (String arg : declaration.getOptionalArgumentNames()) {

            Object o = getArgument(arg);

            if (o != null) {
                cmd.addArgument(arg, o);
            }
        }
        for (String arg : declaration.getOptionNames()) {

            Object o = getArgument(arg);
            if (o != null) {
                cmd.addOption(arg, o);
            }
        }

        cmd.validate();
        cmd.setMonitor(monitor);

        return cmd;
    }

}
