/**
 * Copyright 2011 The ARIES Consortium (http://www.ariesonline.org) and
 * www.integratedmodelling.org. 

   This file is part of Thinklab.

   Thinklab is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published
   by the Free Software Foundation, either version 3 of the License,
   or (at your option) any later version.

   Thinklab is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Thinklab.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.integratedmodelling.thinklab.rest.resources;

import java.io.File;

import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.exceptions.ThinklabResourceNotFoundException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.project.IProject;
import org.integratedmodelling.thinklab.common.project.Project;
import org.integratedmodelling.thinklab.common.utils.FolderZiper;
import org.integratedmodelling.thinklab.common.utils.MiscUtilities;
import org.integratedmodelling.thinklab.rest.BaseRESTService;
import org.integratedmodelling.thinklab.rest.RESTUserModel;
import org.restlet.representation.Representation;
import org.restlet.resource.Get;

/**
 * Manages thinklab projects at the server side (deploy, undeploy, update) using 
 * archive files sent by the client.
 * 	
 * 
 * @author ferdinando.villa
 *
 */
public class ProjectService extends BaseRESTService {

    @Get
    public Representation service(Representation entity) {

        try {
            if (!checkPrivileges("ADMIN"))
                return wrap();

            String cmd = getArgument("cmd");
            String pluginId = getArgument("plugin");
            RESTUserModel um = (RESTUserModel) (getSession().getUserModel());

            if (cmd.equals("deploy")) {

                File archive = this.getFileForHandle(getArgument("handle"), true);
                Thinklab.get().logger().info("archive " + archive + " received for project " + pluginId);
                Thinklab.get().deployProject(pluginId, archive.toString(), um.getMonitor());

            } else if (cmd.equals("undeploy")) {

                Thinklab.get().undeployProject(pluginId);

            } else if (cmd.equals("pack")) {

                /*
                 * make an archive from the project and return the handle
                 */
                IProject tp = Thinklab.get().getProject(pluginId);
                if (tp == null)
                    throw new ThinklabResourceNotFoundException("project " + pluginId + " does not exist");

                Pair<File, String> fname = this.getFileName("project.zip", getSession().getWorkspace());
                new FolderZiper(".git").zipFolder(((Project) tp).getLoadPath().toString(), fname.getFirst()
                        .toString());
                put("handle", fname.getSecond());

            } else if (cmd.equals("register")) {

                /*
                 * notify that a project is waiting in a particular directory. Use instead of deploy in embedded
                 * servers where the server filesystem is available to the client, or for special purposes.
                 * Admits several directories separated by commas.
                 */
                for (String s : getArgument("directory").split(",")) {

                    File file = new File(s);
                    if (!file.exists() || !file.isDirectory())
                        throw new ThinklabResourceNotFoundException("directory " + file
                                + " not found on filesystem");

                    String projectId = MiscUtilities.getFileName(s);
                    Thinklab.get().logger().info("registering project " + projectId + " from " + file);
                    Thinklab.get().registerProject(file);
                }

            } else if (cmd.equals("load")) {

                if (pluginId == null) {
                    Thinklab.get().load(false, um.getMonitor());
                } else {

                    IProject project = Thinklab.get().getProject(pluginId);
                    if (project == null) {
                        throw new ThinklabResourceNotFoundException("project " + pluginId + " not registered");
                    }

                    boolean nochange = getArgument("reload", "true").equals("false");

                    if (nochange && ((Project) project).isLoaded()) {
                        Thinklab.get().logger().info("project " + pluginId + " unchanged");
                    } else {
                        Thinklab.get().logger().info("loading project " + pluginId);
                        Thinklab.get().loadProject(pluginId, um.getMonitor());
                    }
                }
            }

        } catch (Exception e) {
            fail(e);
        }

        return wrap();
    }

}
