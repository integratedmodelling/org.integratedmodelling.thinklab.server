/**
 * Copyright 2011 The ARIES Consortium (http://www.ariesonline.org) and
 * www.integratedmodelling.org. 

   This file is part of Thinklab.

   Thinklab is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published
   by the Free Software Foundation, either version 3 of the License,
   or (at your option) any later version.

   Thinklab is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Thinklab.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.integratedmodelling.thinklab.rest.resources;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.listeners.INotification;
import org.integratedmodelling.thinklab.api.runtime.IServer;
import org.integratedmodelling.thinklab.common.monitoring.Notification;
import org.integratedmodelling.thinklab.rest.BaseRESTService;
import org.integratedmodelling.thinklab.runtime.Session;
import org.json.JSONObject;
import org.restlet.data.CharacterSet;
import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.resource.Get;

/**
 * Retrieve notifications from running tasks, optionally clearing the log and starting from a given position.
 * 
 * If argument "send"(message) is passed along with taskId, locate the task monitor and if found, send the
 * message.
 * 
 * TODO improve: should return server status and short capabilities, plus status of all tasks for a session if
 * the session ID is passed.
 * 
 * @author ferdinando.villa
 *
 */
public class GetNotificationsService extends BaseRESTService {

    public GetNotificationsService() {
        super();
        this.getLogger().setLevel(Level.WARNING);
    }

    @Get
    public Representation service(Representation entity) {

        JSONObject oret = new JSONObject();
        Runtime runtime = Runtime.getRuntime();

        try {
            oret.put(IServer.VERSION_STRING, Thinklab.get().getVersion());
            oret.put(IServer.BOOT_TIME_MS, Thinklab.get().getBootTime());
            oret.put(IServer.TOTAL_MEMORY_MB, (runtime.totalMemory() / 1048576l));
            oret.put(IServer.FREE_MEMORY_MB, (runtime.freeMemory() / 1048576l));
            oret.put(IServer.AVAILABLE_PROCESSORS, runtime.availableProcessors());
            oret.put(IServer.IS_LOCKED, Thinklab.get().isLocked() ? "true" : "false");
            oret.put("status", IServer.OK);

            /*
             * time running
             */
            long bt = new Date().getTime() - Thinklab.get().getBootTime();
            String ut = String.format(
                    "%02d:%02d min",
                    TimeUnit.MILLISECONDS.toMinutes(bt),
                    TimeUnit.MILLISECONDS.toSeconds(bt)
                            - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(bt)));
            oret.put(IServer.UPTIME_TEXT, ut);

            boolean clear = Boolean.parseBoolean(getArgument("clear", "false"));
            int start = Integer.parseInt(getArgument("start", "0"));
            String message = getArgument("send");

            if (getSession() != null) {

                if (message != null) {
                    int taskId = Integer.parseInt(getArgument("taskId", "-1"));
                    IMonitor monitor = ((Session) getSession()).getTaskMonitor(taskId);
                    if (monitor != null) {
                        monitor.send(message);
                    }

                } else {

                    List<INotification> notifications = getSession().getNotifications(clear);

                    if (start > 0)
                        notifications = notifications.subList(start, notifications.size());

                    ArrayList<Object> ndata = new ArrayList<Object>();
                    for (INotification n : notifications) {
                        ndata.add(((Notification) n).adapt());
                    }

                    oret.put("notifications", ndata);
                }
            }

        } catch (Exception e) {
            fail(e);
        }

        JsonRepresentation ret = new JsonRepresentation(oret);
        ret.setCharacterSet(CharacterSet.UTF_8);

        return ret;

    }

}
