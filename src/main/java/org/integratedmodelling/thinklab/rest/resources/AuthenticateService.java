/**
 * Copyright 2011 The ARIES Consortium (http://www.ariesonline.org) and
 * www.integratedmodelling.org. 

   This file is part of Thinklab.

   Thinklab is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published
   by the Free Software Foundation, either version 3 of the License,
   or (at your option) any later version.

   Thinklab is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Thinklab.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.integratedmodelling.thinklab.rest.resources;

import org.integratedmodelling.auth.User;
import org.integratedmodelling.exceptions.ThinklabAuthenticationException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.runtime.ISession;
import org.integratedmodelling.thinklab.api.runtime.IUser;
import org.integratedmodelling.thinklab.rest.BaseRESTService;
import org.integratedmodelling.thinklab.rest.RESTManager;
import org.integratedmodelling.thinklab.rest.interfaces.IStatelessService;
import org.restlet.representation.Representation;
import org.restlet.resource.Get;

/**
 * Creates a session for the calling service and returns a descriptor 
 * containing the session URN. Must be called by clients before doing 
 * anything except server status requests, and almost all requests must
 * contain the session ID returned by this service.
 * 
 * If the request already contains a valid session URN, simply verify its validity and 
 * return session status.
 * 
 * @author ferdinando.villa
 * 
 */
public class AuthenticateService extends BaseRESTService implements IStatelessService {

    @Get
    public Representation service(Representation entity) {

        try {

            String sess = this.getArgument("session");
            if (sess != null) {
                ISession s = RESTManager.get().getSession(sess);
                if (s != null) {
                    info("session is already established");
                } else {
                    fail("session is already active but does not exist");
                }
                return wrap();
            }

            String unam = this.getArgument("user");
            String pass = this.getArgument("password");
            String pser = null;
            if (this.getArgument("primaryserver") != null
                    && !this.getArgument("primaryserver").equals("NONE"))
                pser = this.getArgument("primaryserver");

            // start anonymous
            IUser user = User.getAnonymous();

            if (unam != null) {

                boolean preauthenticated = this.getArgument("preauthenticated") != null
                        && this.getArgument("preauthenticated").equals("true");

                if (preauthenticated) {

                    /*
                     * this is hit on embedded servers only.
                     */
                    ((User) user).define(unam, pass, pser);

                    /*
                     * lock the server
                     */
                    Thinklab.get().lock(user, true);

                } else {

                    user = User.retrieve(unam);
                    if (user == null || pass == null || !user.getSecurityKey().equals(pass)) {
                        throw new ThinklabAuthenticationException("failed to authenticate user " + user);
                    }
                }
            }

            ISession session = RESTManager.get().createRESTSession(this.getArguments(), user);
            put("session", session.getID());
            put("locked", Thinklab.get().isLocked() ? "true" : "false");

        } catch (Exception e) {
            fail(e);
        }

        return wrap();
    }
}
