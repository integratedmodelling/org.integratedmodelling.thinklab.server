package org.integratedmodelling.thinklab.mca.promethee.intensity;

/**
 *
 * @author Edwin Boaz Soenaryo
 */
public interface IHFunction {

    public double getHValue(double difference);

}
