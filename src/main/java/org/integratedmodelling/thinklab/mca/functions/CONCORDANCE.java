package org.integratedmodelling.thinklab.mca.functions;

import java.util.Map;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IExpression;
import org.integratedmodelling.thinklab.api.project.IProject;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.interfaces.annotations.Function;

@Function(
        id = "mca.concordance",
        parameterNames = { "criteria", "weights", "method" },
        returnTypes = { NS.STATE_ACCESSOR })
public class CONCORDANCE implements IExpression {

    private IProject project;

    @Override
    public Object eval(Map<String, Object> parameters, IConcept... context) throws ThinklabException {

        return null;
    }

    @Override
    public void setProjectContext(IProject project) {
        this.project = project;
    }

}
