package org.integratedmodelling.thinklab.plugins.hydrology.lisflood;

import java.util.Date;
import java.util.Random;

import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.common.utils.parallel.Parallel;
import org.integratedmodelling.thinklab.common.utils.parallel.ParallelOpInt;

/**
 * Java port of CAESAR-LISFLOOD by Tom Coulthard & c. Port based on version 1.2x of 10/10/2012.
 * GPL 2.0.
 * 
 * Factors out the GUI and is parameterized by a single instance of {@link FloodInput}. All
 * I/O substituted by a IState for the DEM and a Weather for weather data, also provided through
 * FloodInput.
 * 
 * @author Ferd
 *
 */
public class FloodModel {

    /*
     * all form parameters are here
     */
    FloodInput _input;
    IMonitor _monitor;

    public static double magnifyValue = 0;
    public static int updateClick = 0;
    private double[] zoomFactor = { .25, .33, .50, .66, .80, 1, 1.25, 1.5, 2.0, 2.5, 3.0 };
    private double[] contrastFactor = { 1, 1.2, 1.4, 1.6, 1.8, 2.0, 2.2, 2.4, 2.6, 2.8, 3 };
    private double contrastMultiplier = 0;
    public int imageCount = 1;
    public int imageCount2 = 1;
    int coordinateDone = 0;
    double urfinalLati, urfinalLongi, llfinalLati, llfinalLongi, yurcorner, xurcorner = 0;
    public String kml = "";
    public String KML_FILE_NAME = "animation\\animation.kml";
    int save_time2, save_interval2 = 0;
    public String startDate, kmlTime;
    public Date googleTime;
    public String[] DateArray;
    public String[] DateArray2;

    // toms global variables
    final float g = 9.81F;
    final float kappa = 0.4F;
    double water_depth_erosion_threshold = 0.01;
    int input_time_step = 60;
    int number_of_points = 0;
    double globalsediq = 0;
    double time_1 = 1;
    double save_time = 0;
    double creep_time = 1;
    double creep_time2 = 1;
    double soil_erosion_time = 1;

    // int tot_number_of_tracer_points=0;
    int input_type_flag = 0; // 0 is water input from points, 1 is input from
                             // hydrograph or rainfall file.
    double failureangle = 45;
    double saveinterval = 1000;
    int counter = 0;
    boolean googleoutputflag = false;
    double waterinput = 0;
    double waterOut = 0;
    double in_out_difference = 0;
    double mannings = 0.04;

    int xmax, ymax;
    double xll, yll;
    int maxiterations;
    int maxcycle;
    final int ACTIVE_FACTOR = 1;
    final int TRUE = 1;
    final int FALSE = 0;
    double ERODEFACTOR = 0.05;
    double DX = 5;
    double root = 7.07;

    int LIMIT = 1;
    double MIN_Q = 0.01;
    double CREEP_RATE = 0.0025;
    double SOIL_RATE = 0.0025;
    double active = 0.2;
    int G_MAX = 10;
    double lateral_constant = 0.0000002;
    int grain_array_tot = 1;

    double time_factor = 1;
    double j = 0.000000001; // start of hydrological model paramneters
    double jo = 0.000000001;
    double j_mean;
    double old_j_mean = 0;
    double new_j_mean = 0;
    double M = 0.005;
    double baseflow = 0.00000005; // end of hyd model variables usually
                                  // 0.0000005 changed 2/11/05

    /*
     * ACHTUNG this static thing is going to mess things up
     */
    public static double cycle = 0;
    double rain_factor = 1;
    double sediQ = 0;
    double grow_grass_time = 0;

    double output_file_save_interval = 60;
    double min_time_step = 0;
    double vegTauCrit = 100;
    public static int graphics_scale = 2; // value that controls the number of
                                          // bmp pixels per model pixel for
                                          // the output images.
    int max_time_step = 0;
    int dune_mult = 5;
    double dune_time = 1;
    double max_vel = 5;
    double sand_out = 0;
    double maxdepth = 10;
    double courant_number = 0.7;
    double erode_call = 0;
    double erode_mult = 1;
    double lateralcounter = 1;
    double edgeslope = 0.001;
    double bed_proportion = 0.01;
    double veg_lat_restriction = 0.1;
    double lateral_cross_channel_smoothing = 0.0001;
    double froude_limit = 0.8;
    double recirculate_proportion = 1;

    double Csuspmax = 0.05; // max concentration of SS allowed in a cell
                            // (proportion)
    double hflow_threshold = 0.001;

    // KAtharine
    int variable_m_value_flag = 0;

    // grain size variables - the sizes
    double d1 = 0.0005;
    double d2 = 0.001;
    double d3 = 0.002;
    double d4 = 0.004;
    double d5 = 0.008;
    double d6 = 0.016;
    double d7 = 0.032;
    double d8 = 0.064;
    double d9 = 0.128;

    // grain size proportions for each fraction... as a proportion of 1.

    double d1prop = 0.144;
    double d2prop = 0.022;
    double d3prop = 0.019;
    double d4prop = 0.029;
    double d5prop = 0.068;
    double d6prop = 0.146;
    double d7prop = 0.22;
    double d8prop = 0.231;
    double d9prop = 0.121;

    // Gez
    double previous;
    int hours = 0;
    double new_cycle = 0;
    double old_cycle = 0;
    double tx = 60;
    double Tx = 0;
    double tlastcalc = 0;
    double Qs_step = 0;
    double Qs_hour = 0;
    double Qs_over = 0;
    double Qs_last = 0;
    double Qw_newvol = 0;
    double Qw_oldvol = 0;
    double Qw_lastvol = 0;
    double Qw_stepvol = 0;
    double Qw_hourvol = 0;
    double Qw_hour = 0;
    double Qw_overvol = 0;
    double temptotal = 0;
    double old_sediq = 0;
    double[] sum_grain, sum_grain2, old_sum_grain, old_sum_grain2, Qg_step, Qg_step2, Qg_hour, Qg_hour2,
            Qg_over, Qg_over2, Qg_last, Qg_last2;
    String CATCH_FILE = "catchment.dat";
    // end gez

    // toms global arrays
    double[][] elev, bedrock, init_elevs, water_depth, area, tempcreep, Tau, Vel, qx, qy, qxs, qys,
    /* dune arrays */area_depth, sand, elev2, sand2, grain;
    int[][] index, cross_scan, down_scan;
    boolean[][] inputpointsarray;
    int[] catchment_input_x_coord, catchment_input_y_coord;

    double[][][] vel_dir;
    double[][][] strata;
    double[] hourly_rain_data, hourly_m_value, temp_grain;
    double[][] hydrograph, dischargeinput;
    double[][][] inputfile;
    int[][] inpoints;
    double[][][] veg;
    double[][] edge, edge2; // TJC 27/1/05 array for edges
    double[] old_j_mean_store;
    double[][] climate_data;

    // MJ global vars
    double[] fallVelocity;
    boolean[] isSuspended;
    double[][] Vsusptot;
    int[] deltaX = new int[] { 0, 0, 1, 1, 1, 0, -1, -1, -1 };
    int[] deltaY = new int[] { 0, -1, -1, 0, 1, 1, 1, 0, -1 };
    int nActualGridCells;
    double Jw_newvol = 0.0;
    double Jw_oldvol = 0.0;
    double Jw_lastvol = 0.0;
    double Jw_stepvol = 0.0;
    double Jw_hourvol = 0.0;
    double Jw_hour = 0.0;
    double Jw_overvol = 0.0;
    double k_evap = 0.0;

    // JOE global vars
    String[] inputheader; // Read from ASCII DEM <JOE 20050605>
    double[][] slopeAnalysis; // Initially calculated in percent slope, coverted
                              // to radians
    double[][] aspect; // Radians
    double[][] hillshade; // 0 to 255
    double hue = 360.0; // Ranges between 0 and 360 degrees
    double sat = 0.90; // Ranges between 0 and 1.0 (where 1 is 100%)
    double val = 1.0; // Ranges between 0 and 1.0 (where 1 is 100%)
    double red = 0.0;
    double green = 0.0;
    double blue = 0.0;

    // siberia submodel parameters
    double m1 = 1.70;
    double n1 = 0.69;
    double Beta3 = 0.000186;
    double m3 = 0.79;
    double Beta1 = 1067;

    // sedi tpt flags
    int einstein = 0;
    int wilcock = 0;

    int div_inputs = 1;
    double rain_data_time_step = 60; // time step for rain data - default is 60.

    // lisflood caesar adaptation globals
    int catchment_input_counter = 0;

    // JMW Vars
    String basetext = "CAESAR - Lisflood 1.2x (27/6/2011)";
    String cfgname = null; // Config file name
    String workdir = "c:\\program files\\Caesar\\work\\";

    public FloodModel(FloodInput input) {
        _input = input;
    }

    public void run() {

        /********* locals ***********/
        double tempflow = baseflow;
        double ince = cycle + 60;

        time_1 = 1;

        calc_J(1.0);

        save_time = cycle;
        creep_time = cycle;
        creep_time2 = cycle;
        soil_erosion_time = cycle;
        time_1 = cycle;

        //slide_5();
        get_area();

        get_catchment_input_points();

        // TODO info (running)
        time_factor = 1;

        /*
         * launch main simulation driver
         */
        erodedepo();
    }

    void catchment_water_input_and_hydrology(double local_time_factor) {
        waterinput += j_mean * nActualGridCells * DX * DX;

        for (int z = 1; z <= catchment_input_counter; z++) {
            int x = catchment_input_x_coord[z];
            int y = catchment_input_y_coord[z];
            double water_add_amt = (j_mean * nActualGridCells) / (catchment_input_counter)
                    * local_time_factor;
            if (water_add_amt > ERODEFACTOR)
                water_add_amt = ERODEFACTOR;
            water_depth[x][y] += water_add_amt;
            if ((j_mean * nActualGridCells) / (catchment_input_counter) * local_time_factor > ERODEFACTOR)
                _monitor.info("C add= "
                        + ((j_mean * nActualGridCells) / (catchment_input_counter) * local_time_factor), null);
        }

        // if the input type flag is 1 then the discharge is input from the hydrograph...
        if (cycle >= time_1) {
            do {
                time_1++;
                calc_J(time_1);
                if (time_factor > max_time_step && new_j_mean > (0.2 / (xmax * ymax * DX * DX)))//stops code going too fast when there is actual flow in the channels greater than 0.2cu.
                {

                    cycle = time_1 + (max_time_step / 60);
                    time_factor = max_time_step;
                }
            } while (time_1 < cycle);
        }

        calchydrograph(time_1 - cycle);

        // next line is a dirty fix, so that j_mean can be read directly in from the rainfall file, if the check box is ticked
        // that allows you to calc j_mean direct from a hydrograph.
        if (_input.READ_DISCHARGE_FROM_RAINFALL_FILE)
            j_mean = ((hourly_rain_data[(int) (cycle / rain_data_time_step)]) / Math.pow(DX, 2))
                    / nActualGridCells;

        if (j_mean >= baseflow) {
            baseflow = baseflow * 3;
            //Console.Write("going up.. \n");
            get_area();
            get_catchment_input_points();
        }

        if (baseflow > (j_mean * 3) && baseflow > 0.0000001) {
            baseflow = j_mean * 1.25;
            //Console.Write("going down.. \n");
            get_area();
            get_catchment_input_points();
        }
    }

    void reach_water_and_sediment_input(double local_time_factor) {
        Double[] remove_from_temp_grain = new Double[G_MAX + 2];

        if (_input.REACH_MODE) {
            for (int n = 0; n <= number_of_points - 1; n++) {
                int tempn;

                int x = inpoints[n][0];
                int y = inpoints[n][1];

                double adding_factor = 1;
                // tot up total to be added - and if its greater than number of cells and erode_limit
                // then reduce what can be added via a factor. 
                double added_tot = 0;

                if (water_depth[x][y] > water_depth_erosion_threshold) {
                    for (tempn = 5; tempn <= 13; tempn++) {
                        added_tot += (Math.abs(inputfile[n][(int) (cycle / input_time_step)][tempn]))
                                / div_inputs / (DX * DX) / (input_time_step * 60) * time_factor;
                    }

                    // then multiply by the recirculation factor..
                    if (added_tot / number_of_points > ERODEFACTOR * 0.75)
                        adding_factor = (ERODEFACTOR * 0.75) / (added_tot / number_of_points);

                    if (index[x][y] == -9999)
                        addGS(x, y);
                    for (tempn = 5; tempn <= 13; tempn++) {
                        double amount_to_add = adding_factor
                                * (Math.abs(inputfile[n][(int) (cycle / input_time_step)][tempn]))
                                / div_inputs / (DX * DX) / (input_time_step * 60) * time_factor;
                        if (isSuspended[tempn - 4]) {
                            Vsusptot[x][y] += amount_to_add;
                        } else {
                            grain[index[x][y]][tempn - 4] += amount_to_add;
                            elev[x][y] += amount_to_add;
                        }

                    }
                }
            }
        }

        // add in recirculating sediment.
        if (_input.COMPUTE_RECIRCULATION && _input.REACH_MODE) {
            int tempn;
            double adding_factor = 1;
            // tot up total to be added - and if its greater than number of cells and erode_limit
            // then reduce what can be added via a factor. 
            double added_tot = 0;

            for (tempn = 5; tempn <= 13; tempn++) {
                added_tot += temp_grain[tempn - 4];
                remove_from_temp_grain[tempn - 4] = 0.0; // quick way of emptying this variable
            }

            // then multiply by the recirculation factor..
            if (added_tot / number_of_points > ERODEFACTOR * 0.75)
                adding_factor = (ERODEFACTOR * 0.75) / (added_tot / number_of_points);

            for (int n = 0; n <= number_of_points - 1; n++) {
                int x = inpoints[n][0];
                int y = inpoints[n][1];

                if (water_depth[x][y] > water_depth_erosion_threshold) {

                    for (tempn = 5; tempn <= 13; tempn++) {
                        if (index[x][y] == -9999)
                            addGS(x, y);
                        if (isSuspended[tempn - 4]) {
                            Vsusptot[x][y] += ((temp_grain[tempn - 4] * adding_factor) / number_of_points);
                            remove_from_temp_grain[tempn - 4] += ((temp_grain[tempn - 4] * adding_factor) / number_of_points);
                        } else {
                            grain[index[x][y]][tempn - 4] += ((temp_grain[tempn - 4] * adding_factor) / number_of_points);
                            elev[x][y] += ((temp_grain[tempn - 4] * adding_factor) / number_of_points);
                            remove_from_temp_grain[tempn - 4] += ((temp_grain[tempn - 4] * adding_factor) / number_of_points);
                        }

                    }
                }
            }

        }

        for (int n = 1; n <= G_MAX; n++) {
            temp_grain[n] -= remove_from_temp_grain[n];
        }

        for (int n = 0; n <= number_of_points - 1; n++) {
            int x = inpoints[n][0];
            int y = inpoints[n][1];
            double interpolated_input1 = inputfile[n][(int) (cycle / input_time_step)][1];
            double interpolated_input2 = inputfile[n][(int) (cycle / input_time_step) + 1][1];
            double proportion_between_time1and2 = ((((int) (cycle / input_time_step) + 1) * input_time_step) - cycle)
                    / input_time_step;

            double input = interpolated_input1
                    + ((interpolated_input2 - interpolated_input1) * (1 - proportion_between_time1and2));

            //j_mean = old_j_mean + (((new_j_mean - old_j_mean) / 2) * (2 - time));

            waterinput += (input / div_inputs);

            // trial adding SS line
            // if(counter<500)Vsusptot[x+5, y] = 0.1;
            water_depth[x][y] += (input / div_inputs) / (DX * DX) * local_time_factor;
            // also have to add suspended sediment here..
            // from file

        }

    }

    void calchydrograph(double time) {
        j_mean = old_j_mean + (((new_j_mean - old_j_mean) / 2) * (2 - time));
    }

    void get_catchment_input_points() {
        catchment_input_counter = 1;
        for (int x = 1; x <= xmax; x++) {
            for (int y = 1; y <= ymax; y++) {
                if ((area[x][y] * baseflow * DX * DX) > MIN_Q) {
                    catchment_input_x_coord[catchment_input_counter] = x;
                    catchment_input_y_coord[catchment_input_counter] = y;
                    catchment_input_counter++;
                }
            }
        }
    }

    private void calc_J(double cycle) {

        double local_rain_fall_rate;
        /** in m/second **/
        double local_time_step = 60;
        /** in secs */
        old_j_mean = new_j_mean;
        jo = j;

        /* Get The M Value From File If One Is Specified */
        if (variable_m_value_flag == 1) {
            M = hourly_m_value[1 + (int) (cycle / rain_data_time_step)];
        }

        local_rain_fall_rate = 0;
        if (hourly_rain_data[(int) (cycle / rain_data_time_step)] > 0) {
            local_rain_fall_rate = rain_factor
                    * ((hourly_rain_data[(int) (cycle / rain_data_time_step)] / 1000) / 3600);
            /** divide by 1000 to make m/hr, then by 3600 for m/sec */
        }

        if (local_rain_fall_rate == 0) {
            j = jo / (1 + ((jo * local_time_step) / M));
            new_j_mean = M / local_time_step * Math.log(1 + ((jo * local_time_step) / M));
        }

        if (local_rain_fall_rate > 0) {
            j = local_rain_fall_rate
                    / (((local_rain_fall_rate - jo) / jo)
                            * Math.exp((0 - local_rain_fall_rate) * local_time_step / M) + 1);
            new_j_mean = (M / local_time_step)
                    * Math.log(((local_rain_fall_rate - jo) + jo
                            * Math.exp((local_rain_fall_rate * local_time_step) / M))
                            / local_rain_fall_rate);
        }
        if (new_j_mean < 0)
            new_j_mean = 0;
        /**printf("new J mean = %f\n",(new_j_mean*1000));*/

    }

    // seems to be the main driver after initialization and data load
    void erodedepo() {

        int y;
        do {

            // set of lines commented out to instigate uplift at set times (if needed)
            // line to do uplift at set time....
            //if (cycle > (1440 * 18250) && elev[1, 1] < 100)
            //{
            //    for (int x = 1; x <= 300; x++)
            //    {
            //        for (y = 1; y <= ymax; y++)
            //        {
            //            elev[x, y] += 10;//Convert.ToInt32(textBox6.Text);
            //        }
            //    }
            //    slide_5();
            //    slide_5();
            //    elev[1, 1] = 100;
            //}

            // Gez code: set previous cycle = to cycle
            previous = cycle;
            old_cycle = cycle % output_file_save_interval;
            // end gez code

            //
            // section below workign out time step.
            //
            double input_output_difference = Math.abs(waterinput - waterOut);

            // all dealing with calculating model time step
            if (maxdepth <= 0.1)
                maxdepth = 0.1;
            if (time_factor < min_time_step)
                time_factor = min_time_step;
            if (time_factor < (courant_number * (DX / Math.sqrt(9.8 * (maxdepth)))))
                time_factor = (courant_number * (DX / Math.sqrt(9.8 * (maxdepth))));
            if (input_output_difference > in_out_difference
                    && time_factor > (courant_number * (DX / Math.sqrt(9.8 * (maxdepth)))))
                time_factor = courant_number * (DX / Math.sqrt(9.8 * (maxdepth)));
            double local_time_factor = time_factor;
            if (local_time_factor > (courant_number * (DX / Math.sqrt(9.8 * (maxdepth)))))
                local_time_factor = courant_number * (DX / Math.sqrt(9.8 * (maxdepth)));

            // code to incrememtn counters. Counter is model iterations, cycle is the actual (modelled reality) time
            counter++;
            cycle += time_factor / 60;

            // Gez code
            new_cycle = cycle % output_file_save_interval;
            // end gez code

            ////			This whole section below deals with the water inputs. A separate part for point inputs and 
            ////		    a different bit for gradual inputs from the whole of the catchment. (reach or catchment)
            // first zero counter to tally up water inputs
            waterinput = 0;

            // reach mode water inputs
            if (_input.REACH_MODE)
                reach_water_and_sediment_input(local_time_factor);

            // catchment mode water inputs
            if (_input.CATCHMENT_MODE)
                catchment_water_input_and_hydrology(local_time_factor);

            // route water and update flow depths

            qroute();
            depth_update();

            // check scan area every 5 iters.. maybe re-visit for reach mode if it causes too much backing up of sed. see code commented below nex if..
            if (Math.IEEEremainder(counter, 5) == 0) {
                scan_area();
            }

            // carry out erosion

            if (!_input.COMPUTE_FLOW_ONLY) {
                if (counter >= erode_call) {
                    erode_mult = (int) (ERODEFACTOR / erode(erode_mult));
                    if (erode_mult < 1)
                        erode_mult = 1;
                    if (erode_mult > 5)
                        erode_mult = 5;
                    erode_call = counter + erode_mult;
                    //this.tempdata1.Text = Convert.ToString(erode_mult);
                }
                //
                // carry out lateral erosion
                //
                if (_input.COMPUTE_LATERAL_EROSION && counter >= lateralcounter) {
                    lateral3();
                    lateralcounter = counter + (50 * erode_mult);
                }
            }

            //
            // work out water coming out..
            // and zero water depths in edges..
            //

            double temptot = 0;
            for (y = 1; y <= ymax; y++) {
                if (water_depth[xmax][y] > 0) {
                    temptot += water_depth[xmax][y] * DX * DX / local_time_factor;
                    // and zero water depths at RH end
                    water_depth[xmax][y] = 0;
                }
                if (water_depth[1][y] > 0) {
                    temptot += water_depth[1][y] * DX * DX / local_time_factor;
                    // and zero water depths at RH end
                    water_depth[1][y] = 0;
                }
            }
            for (int x = 1; x <= xmax; x++) {
                if (water_depth[x][1] > 0) {
                    temptot += water_depth[x][1] * DX * DX / local_time_factor;
                    // and zero water depths at RH end
                    water_depth[x][1] = 0;
                }
                if (water_depth[x][ymax] > 0) {
                    temptot += water_depth[x][ymax] * DX * DX / local_time_factor;
                    // and zero water depths at RH end
                    water_depth[x][ymax] = 0;
                }
            }
            waterOut = temptot;

            // then carry out soil creep. 
            //- also growing grass... 
            // and dunes...
            // slide_3 is looking at landslides only in the 'scanned' area
            // slide_4 is to do with dunes
            // slide_5 is landslides everywhere..

            if (!_input.COMPUTE_FLOW_ONLY) {
                // carry out local landslides every X iterations...
                if (Math.IEEEremainder(counter, 10) == 0) {
                    slide_3();
                }
                // soil creep - here every 10 days
                if (cycle > creep_time) {
                    // updating counter creep_time
                    creep_time += 14400;
                    // now calling soil creep function
                    creep(0.028);//(0.019);//(0.083);
                }
                //// doing sand dunes part

                if (_input.COMPUTE_DUNES == true && cycle > dune_time) {
                    dune1(0);
                    dune_time += _input.TIMESTEPS_BETWEEN_DUNE_CALLS;
                }

                //
                // now calling soil erosion function  - also checks if soil erosion rate is > 0 
                // to prevent it calling it if not required.
                // then soil erosion time
                //

                if (SOIL_RATE > 0 && cycle > soil_erosion_time) {
                    get_area(); // gets drainage area before doing soil erosion - as used in the calcs. therefore good to have accurate/fresh drianage area
                    soil_erosion_time += 1440;// do soil erosion daily
                    if (_input.EROSION_DEPENDS_ON_J_MEAN == true)
                        SOIL_RATE = ((0.768383841078 * j_mean + 0.000001979564));
                    soilerosion(0.0028);
                }
            }

            // other slope things.. done every day.
            if (cycle > creep_time2) {
                // updating counter creep_time2
                creep_time2 += 1440;// daily
                evaporate(1440);
                slide_5();
                // calling siberia model (if wanted)
                if (_input.USE_SIBERIA_SUBMODEL == true) {
                    get_area();
                    siberia(0.002739); // the value passed is the time step (in years)
                }
                // next line does grass growing, must change it if changes from monthly update
                if (grow_grass_time > 0)
                    grow_grass(1 / (grow_grass_time * 365));
            }

            // Gez
            temptotal = temptot;

            // call output routine
            // only if iteration by iteration output is required.
            if (_input.GENERATE_TIMESERIES_OUTPUT) // MJ 20/01/05
            {
                output_data();
            }
            // end gez code

            //Google Earth Animation outputs data to file.
            if ((_input.GENERATE_GOOGLE_EARTH_ANIMATION) && (cycle >= save_time2))
                googleoutputflag = true;

            // save data & draw graphics every specified interval. Passes the cycle whuch becomes the file extension
            if (cycle >= save_time && /* uniquefilecheck.Checked == true */false) {
                save_data_and_draw_graphics();
                save_time += saveinterval;
            }
            // if its at the end of the run kill the program
        } while (counter < maxiterations && cycle < maxcycle * 60);

        // TODO monitor.info(finished)

        // end of main loop!!
    }

    void output_data() {
        int n;

        // Qw (m3) value at timestep cycle (current
        // Qw_newvol+=temptotal*((cycle-previous)*output_file_save_interval); // replaced by line below MJ 25/01/05
        Qw_newvol += temptotal * ((cycle - previous) * 60); // 60 secs per min
        Jw_newvol += (j_mean * DX * DX * nActualGridCells) * ((cycle - previous) * 60);

        //Catch all time steps that pass one or more hour marks 
        if ((new_cycle < old_cycle) || (cycle - previous >= output_file_save_interval)) {
            while ((tx > previous) && (cycle >= tx)) {
                hours++;

                // Step1: Calculate hourly total sediment Q (m3)
                Qs_step = globalsediq - old_sediq;
                Qs_over = Qs_step * ((cycle - tx) / (cycle - tlastcalc));
                Qs_hour = Qs_step - Qs_over + Qs_last;

                // reset Qs_last and old_sediq for large time steps
                if (cycle >= tx + output_file_save_interval) {
                    Qs_last = 0;
                    old_sediq = globalsediq - Qs_over;
                }

                // reset Qs_last and old_sediq for small time steps
                if (cycle < tx + output_file_save_interval) {
                    Qs_last = Qs_over;
                    old_sediq = globalsediq;
                }

                // Step 2: Calculate grain size Qgs, also calculate contaminated amounts
                for (n = 1; n <= G_MAX - 1; n++) {
                    // calculate timestep Qgs
                    Qg_step[n] = sum_grain[n] - old_sum_grain[n];
                    Qg_step2[n] = sum_grain2[n] - old_sum_grain2[n];
                    // Interpolate Qgs beyond time tx
                    Qg_over[n] = Qg_step[n] * ((cycle - tx) / (cycle - tlastcalc));
                    Qg_over2[n] = Qg_step2[n] * ((cycle - tx) / (cycle - tlastcalc));
                    // and calculate hourly Qgs
                    Qg_hour[n] = Qg_step[n] - Qg_over[n] + Qg_last[n];
                    Qg_hour2[n] = Qg_step2[n] - Qg_over2[n] + Qg_last2[n];
                    // Reset Qg_last[n] and old_sum_grain[n]for large time steps
                    if (cycle >= tx + output_file_save_interval) {
                        Qg_last[n] = 0;
                        Qg_last2[n] = 0;
                        old_sum_grain[n] = sum_grain[n] - Qg_over[n];
                        old_sum_grain2[n] = sum_grain2[n] - Qg_over2[n];
                    }
                    // Reset Qg_last[n] and old_sum_grain[n] for small time steps
                    if (cycle < tx + output_file_save_interval) {
                        Qg_last[n] = Qg_over[n];
                        Qg_last2[n] = Qg_over2[n];
                        old_sum_grain[n] = sum_grain[n];
                        old_sum_grain2[n] = sum_grain2[n];
                    }
                }

                // Step 3: Calculate hourly mean water discharge
                // Qw_overvol = temptotal*((cycle-tx)*output_file_save_interval); // replaced by line below MJ 25/01/05
                Qw_overvol = temptotal * ((cycle - tx) * 60); // 60 secs per min
                Qw_stepvol = Qw_newvol - Qw_oldvol;
                Qw_hourvol = Qw_stepvol - Qw_overvol + Qw_lastvol;
                Qw_hour = Qw_hourvol / (60 * output_file_save_interval); // convert hourly water volume to cumecs

                // same for Jw (j_mean contribution)  MJ 14/03/05
                Jw_overvol = (j_mean * DX * DX * nActualGridCells) * ((cycle - tx) * 60); // fixed MJ 29/03/05
                Jw_stepvol = Jw_newvol - Jw_oldvol;
                Jw_hourvol = Jw_stepvol - Jw_overvol + Jw_lastvol;
                Jw_hour = Jw_hourvol / (60 * output_file_save_interval);

                // reset Qw_lastvol and Qw_oldvol for large time steps
                if (cycle >= tx + output_file_save_interval) {
                    Qw_lastvol = 0;
                    Qw_oldvol = Qw_newvol - Qw_overvol;

                    // same for Jw (j_mean contribution)  MJ 14/03/05
                    Jw_lastvol = 0;
                    Jw_oldvol = Jw_newvol - Jw_overvol;
                }

                // reset Qw_lastvol and Qw_oldvol for small time steps
                if (cycle < tx + output_file_save_interval) {
                    Qw_lastvol = Qw_overvol;
                    Qw_oldvol = Qw_newvol;

                    // same for Jw (j_mean contribution)  MJ 14/03/05
                    Jw_lastvol = Jw_overvol;
                    Jw_oldvol = Jw_newvol;
                }

                Tx = tx;
                tx = Tx + output_file_save_interval;

                // Step 4: Output hourly data to file (format for reach model input)
                // changed MJ 18/01/05
                // TODO output - see what we want to achieve here
                //				SString output = string.Format("{0}",hours);
                //				output = output	+ string.Format(" {0:F6}",Qw_hour);
                //				output = output	+ string.Format(" {0:F6}",Jw_hour);
                //                if (SiberiaBox.Checked == true)
                //                {
                //                    double tomsedi =0;
                //                    for (int x = 1; x <= xmax; x++)
                //                    {
                //                        for (int y = 1; y <= ymax; y++)
                //                        {
                //                            if (elev[x, y] > -9999)
                //                            {
                //                                tomsedi += (init_elevs[x, y] - elev[x, y]) * DX * DX;
                //                            }
                //                        }
                //                    }
                //                    output = output + string.Format(" {0:F6}", tomsedi);
                //                }
                //                else
                //                {
                //                    output = output + string.Format(" {0:F6}", sand_out);
                //                    sand_out = 0;
                //                }
                //				output = output	+ string.Format(" {0:F10}",Qs_hour);
                //				for (n=1;n<=G_MAX-1;n++)
                //				{
                //					output = output + string.Format(" {0:F10}",Qg_hour[n]);
                //					//output = output+" "+Qg_hour[n];
                //				}
                //				
                //
                //				StreamWriter sw = File.AppendText(CATCH_FILE);
                //				sw.WriteLine(output);
                //				sw.Close();

            }
            tlastcalc = cycle;
        }
    }

    void save_data_and_draw_graphics() {
        /*
         * TODO determine requested outputs and adapt output routines
         */
        if (true)
            save_data(1, Math.abs(cycle)); // save waterdepths
        if (true)
            save_data(2, Math.abs(cycle)); // save elevdiff
        if (true)
            save_data(3, Math.abs(cycle)); // save elevations
        if (true)
            save_data(4, Math.abs(cycle)); // save grainsize
        if (true)
            save_data(15, Math.abs(cycle)); // save d50 top layer
        if (true)
            save_data(16, Math.abs(cycle)); // save velocity			<JOE 20050605>
        if (true)
            save_data(17, Math.abs(cycle)); // save soil saturation	<JOE 20050605

    }

    private void save_data(int i, double abs) {
        // TODO Auto-generated method stub

    }

    void evaporate(double time) {
        double _evap_amount = k_evap * (time / 1440);
        if (_evap_amount > ERODEFACTOR)
            _evap_amount = ERODEFACTOR;

        final double evap_amount = _evap_amount;

        Parallel.For(1, ymax, new ParallelOpInt() {

            @Override
            public void run(int y) {
                int inc = 1;
                while (down_scan[y][inc] > 0) {
                    int x = down_scan[y][inc];
                    inc++;
                    // removes water in rate of mm per day..
                    if (water_depth[x][y] > 0) {
                        water_depth[x][y] -= evap_amount;
                        if (water_depth[x][y] < 0)
                            water_depth[x][y] = 0;
                    }
                }

            }
        });
    }

    //	void load_data()
    //	{
    //		int x,y=1,z,xcounter,x1=0,y1=0,n;			
    //		String input;
    //		double tttt=0.00;
    //		
    //		// load dem
    //
    //		string FILE_NAME = this.openfiletextbox.Text;
    //		if (!File.Exists(FILE_NAME)) 
    //		{
    //			MessageBox.Show("No such DEM data file..");
    //			return;
    //		}
    //
    //		StreamReader sr = File.OpenText(FILE_NAME);
    //
    //		//read headers
    //		for(z=1;z<=6;z++)
    //		{
    //			input=sr.ReadLine();
    //		}
    //		y=1;
    //
    //		while ((input=sr.ReadLine())!=null) 
    //		{
    //			string[] lineArray;
    //			lineArray = input.Split(new char[]{' '});
    //			xcounter=1;
    //			for (x = 0; x<=(lineArray.Length-1); x++)
    //			{
    //				
    //				if(lineArray[x]!=""&&xcounter<=xmax)
    //				{
    //					tttt=double.Parse(lineArray[x]);
    //					elev[xcounter,y]=tttt;
    //					//if(xcounter==1)elev[xcounter,y]+=4;
    //					init_elevs[xcounter,y]=elev[xcounter,y];
    //					xcounter++;
    //				}
    //			}
    //			y++;
    //
    //		}
    //		sr.Close();
    //
    //
    //		if(this.graindataloadbox.Text!="null")
    //		{
    //			FILE_NAME = this.graindataloadbox.Text;
    //			if(FILE_NAME!="null")
    //			{
    //				StreamReader gr = File.OpenText(FILE_NAME);
    //				y=1;
    //				grain_array_tot=0;
    //				while ((input=gr.ReadLine())!=null) 
    //				{
    //					string[] lineArray;
    //					lineArray = input.Split(new char[]{' '});
    //					xcounter=1;
    //					grain_array_tot++;
    //					for (x = 0; x<=(lineArray.Length-1); x++)
    //					{
    //						if(lineArray[x]!="")
    //						{
    //							if(xcounter==1)x1=int.Parse(lineArray[x]);
    //							if(xcounter==2)y1=int.Parse(lineArray[x]);
    //                            if (x1 > xmax) x1 = xmax; // lines to prevent adding grain if clipping the grid and excellss on the grain grid is left
    //                            if (y1 > ymax) y1 = ymax; //
    //
    //                            if (xcounter == 3) index[x1, y1] = grain_array_tot;// int.Parse(lineArray[x]);
    //
    //							for(n=0;n<=G_MAX;n++)
    //							{
    //								if(xcounter==4+n)
    //								{
    //									grain[grain_array_tot,n]=double.Parse(lineArray[x]);
    //								}
    //							}
    //
    //							for(z=0;z<=9;z++)
    //							{
    //								for(n=0;n<=(G_MAX-2);n++)
    //								{
    //									if(xcounter==(4+G_MAX+n+1)+((z)*9))
    //									{
    //										strata[grain_array_tot,z,n]=double.Parse(lineArray[x]);
    //									}
    //								}
    //							}
    //
    //							xcounter++;
    //						}
    //					}
    //				}
    //				gr.Close();
    //			}
    //		}
    //		
    //
    //		FILE_NAME = this.bedrockbox.Text;
    //		if(FILE_NAME!="null")
    //		{
    //
    //			StreamReader gr = File.OpenText(FILE_NAME);
    //			//read headers
    //			for(z=1;z<=6;z++)
    //			{
    //				input=gr.ReadLine();
    //			}
    //			y=1;
    //
    //			while ((input=gr.ReadLine())!=null) 
    //			{
    //                string[] lineArray;
    //                lineArray = input.Split(new char[] { ' ' });
    //                xcounter = 1;
    //                for (x = 0; x <= (lineArray.Length - 1); x++)
    //                {
    //
    //                    if (lineArray[x] != "" && xcounter <= xmax)
    //                    {
    //                        tttt = double.Parse(lineArray[x]);
    //                        bedrock[xcounter, y] = tttt;
    //                        xcounter++;
    //                    }
    //                }
    //                y++;
    //
    //			}
    //			gr.Close();
    //		}
    //
    //
    //        try
    //        {
    //
    //
    //            FILE_NAME = this.raindataloadbox.Text;
    //            if (FILE_NAME != "null")
    //            {
    //                input_type_flag = 1;
    //                int inc = 1;
    //                //				Random R =new Random();
    //                StreamReader gr = File.OpenText(FILE_NAME);
    //                while ((input = gr.ReadLine()) != null)
    //                {
    //
    //                    hourly_rain_data[inc] = double.Parse(input);
    //
    //                    //					// adding random rain bit.
    //                    //
    //                    //					if(hourly_rain_data[inc]>0)
    //                    //					{
    //                    //						hourly_rain_data[inc]=(R.Next(1,30));
    //                    //					}
    //
    //                    //Console.WriteLine(hourly_rain_data[inc]);
    //                    //					string[] lineArray;
    //                    //					lineArray = input.Split(new char[]{' '});
    //                    //					for (x = 0; x<(lineArray.Length-1); x++)
    //                    //					{
    //                    //						if(lineArray[x]!="")
    //                    //						{
    //                    //							hourly_rain_data[inc]=double.Parse(lineArray[x]);
    //                    //							Console.WriteLine("loading raindata \n");
    //                    //							Console.WriteLine(lineArray[x]);
    //                    //						}
    //                    //					}
    //                    inc++;
    //                }
    //                gr.Close();
    //            }
    //
    //        }
    //
    //        catch (Exception)
    //        {
    //            MessageBox.Show("There was some type of error loading the input data from the rain data file CAESAR may continue to function but may not be correct");
    //        }
    //
    //        try
    //        {
    //            FILE_NAME = this.mvalueloadbox.Text;
    //            if (FILE_NAME != "null")
    //            {
    //                variable_m_value_flag = 1; // sets flag for variable M value to 1 (true)
    //                int inc = 1;
    //                StreamReader gr = File.OpenText(FILE_NAME);
    //                while ((input = gr.ReadLine()) != null)
    //                {
    //                    hourly_m_value[inc] = double.Parse(input);
    //                    inc++;
    //                }
    //                gr.Close();
    //            }
    //        }
    //        catch (Exception)
    //        {
    //            MessageBox.Show("There was some type of error loading the m value data from the m value data file CAESAR may continue to function but may not be correct");
    //        }
    //        //try
    //        //{
    //        //    //single mine input "supermine", up to nine grainsizes
    //        //    if (mine_checkBox.Checked)
    //        //    {
    //        //        //mine_inpoints[0, 0] = Convert.ToInt32(mineX_textBox.Text); //input x coordrinate 
    //        //        //mine_inpoints[0, 1] = Convert.ToInt32(mineY_textBox.Text); //input y coordinate
    //        //        FILE_NAME = this.mine_input_textBox.Text;
    //        //        if (FILE_NAME != "null")
    //        //        {
    //        //            int lineinc = 1;//number of hours of contamination
    //        //            StreamReader gr = File.OpenText(FILE_NAME);
    //        //            while ((input = gr.ReadLine()) != null)
    //        //            {
    //        //                int inc = 1;//grainsizes 1-9, only two necessary now
    //
    //        //                string[] lineArray;
    //        //                lineArray = input.Split(new char[] { ' ' });
    //        //                for (x = 1; x < (lineArray.Length); x++)
    //        //                {
    //        //                    if (lineArray[x] != "")
    //        //                    {
    //        //                        //if (inc == 1) mineinput[lineinc, inc] = double.Parse(lineArray[x]);//grainsize1
    //        //                        //if (inc == 2) mineinput[lineinc, inc] = double.Parse(lineArray[x]);//grainsize2
    //        //                        //if (inc == 3) mineinput[lineinc, inc] = double.Parse(lineArray[x]);//grainsize3
    //        //                        //if (inc == 4) mineinput[lineinc, inc] = double.Parse(lineArray[x]);//grainsize4
    //        //                        //if (inc == 5) mineinput[lineinc, inc] = double.Parse(lineArray[x]);//grainsize5
    //        //                        //if (inc == 6) mineinput[lineinc, inc] = double.Parse(lineArray[x]);//grainsize6
    //        //                        //if (inc == 7) mineinput[lineinc, inc] = double.Parse(lineArray[x]);//grainsize7
    //        //                        //if (inc == 8) mineinput[lineinc, inc] = double.Parse(lineArray[x]);//grainsize8
    //        //                        //if (inc == 9) mineinput[lineinc, inc] = double.Parse(lineArray[x]);//grainsize9
    //        //                        inc++;
    //        //                    }
    //        //                }
    //        //                lineinc++;
    //        //            }
    //        //            gr.Close();
    //        //        }
    //        //    }
    //        //}
    //        //catch (Exception ex)
    //        //{
    //        //    MessageBox.Show("There was some type of error loading the contaminant input CAESAR may continue to function but may not be correct");
    //        //}
    //        int tempx=0;
    //
    //		try
    //		{
    //			if(inbox1.Checked)
    //			{
    //				FILE_NAME = this.infile1.Text;
    //				if(FILE_NAME!="null")
    //				{
    //					input_type_flag=0;
    //					StreamReader gr = File.OpenText(FILE_NAME);
    //					while ((input=gr.ReadLine())!=null) 
    //					{
    //						string[] lineArray;
    //						lineArray = input.Split(new char[]{' '});
    //						for (x = 0; x<=(lineArray.Length-1); x++)
    //						{
    //							if(lineArray[x]!="")
    //							{
    //								tempx=int.Parse(lineArray[0]);
    //								inputfile[0,tempx,x]=double.Parse(lineArray[x]);
    //							}
    //						}
    //					}
    //					gr.Close();
    //				}
    //
    //			}
    //        }catch(Exception)
    //		{
    //			MessageBox.Show("There was some type of error loading the input data from "+FILE_NAME+" at line "+Convert.ToString(tempx)+", CAESAR will continue to function but may not be correct");
    //		}
    //
    //        try{
    //			if(inbox2.Checked)
    //			{
    //				FILE_NAME = this.infile2.Text;
    //				if(FILE_NAME!="null")
    //				{
    //					input_type_flag=0;
    //					StreamReader gr = File.OpenText(FILE_NAME);
    //					while ((input=gr.ReadLine())!=null) 
    //					{
    //						string[] lineArray;
    //						lineArray = input.Split(new char[]{' '});
    //						for (x = 0; x<=(lineArray.Length-1); x++)
    //						{
    //							if(lineArray[x]!="")
    //							{
    //								tempx=int.Parse(lineArray[0]);
    //								inputfile[1,tempx,x]=double.Parse(lineArray[x]);
    //							}
    //						}
    //					}
    //					gr.Close();
    //				}
    //			}
    //        }
    //        catch (Exception)
    //        {
    //            MessageBox.Show("There was some type of error loading the input data from " + FILE_NAME + " at line " + Convert.ToString(tempx) + ", CAESAR will continue to function but may not be correct");
    //        }
    //
    //        try{
    //			if(inbox3.Checked)
    //			{
    //				FILE_NAME = this.infile3.Text;
    //				if(FILE_NAME!="null")
    //				{
    //					input_type_flag=0;
    //					StreamReader gr = File.OpenText(FILE_NAME);
    //					while ((input=gr.ReadLine())!=null) 
    //					{
    //						string[] lineArray;
    //						lineArray = input.Split(new char[]{' '});
    //						for (x = 0; x<=(lineArray.Length-1); x++)
    //						{
    //							if(lineArray[x]!="")
    //							{
    //								tempx=int.Parse(lineArray[0]);
    //								inputfile[2,tempx,x]=double.Parse(lineArray[x]);
    //							}
    //						}
    //					}
    //					gr.Close();
    //				}
    //			}
    //        }
    //        catch (Exception)
    //        {
    //            MessageBox.Show("There was some type of error loading the input data from " + FILE_NAME + " at line " + Convert.ToString(tempx) + ", CAESAR will continue to function but may not be correct");
    //        }
    //        try{
    //			if(inbox4.Checked)
    //			{
    //				FILE_NAME = this.infile4.Text;
    //				if(FILE_NAME!="null")
    //				{
    //					input_type_flag=0;
    //					StreamReader gr = File.OpenText(FILE_NAME);
    //					while ((input=gr.ReadLine())!=null) 
    //					{
    //						string[] lineArray;
    //						lineArray = input.Split(new char[]{' '});
    //						for (x = 0; x<=(lineArray.Length-1); x++)
    //						{
    //							if(lineArray[x]!="")
    //							{
    //								tempx=int.Parse(lineArray[0]);
    //								inputfile[3,tempx,x]=double.Parse(lineArray[x]);
    //							}
    //						}
    //					}
    //					gr.Close();
    //				}
    //			}
    //        }
    //        catch (Exception)
    //        {
    //            MessageBox.Show("There was some type of error loading the input data from " + FILE_NAME + " at line " + Convert.ToString(tempx) + ", CAESAR will continue to function but may not be correct");
    //        }
    //
    //        try{
    //			if(inbox5.Checked)
    //			{
    //				FILE_NAME = this.infile5.Text;
    //				if(FILE_NAME!="null")
    //				{
    //					input_type_flag=0;
    //					StreamReader gr = File.OpenText(FILE_NAME);
    //					while ((input=gr.ReadLine())!=null) 
    //					{
    //						string[] lineArray;
    //						lineArray = input.Split(new char[]{' '});
    //						for (x = 0; x<=(lineArray.Length-1); x++)
    //						{
    //							if(lineArray[x]!="")
    //							{
    //								tempx=int.Parse(lineArray[0]);
    //								inputfile[4,tempx,x]=double.Parse(lineArray[x]);
    //							}
    //						}
    //					}
    //					gr.Close();
    //				}
    //			}
    //        }
    //        catch (Exception)
    //        {
    //            MessageBox.Show("There was some type of error loading the input data from " + FILE_NAME + " at line " + Convert.ToString(tempx) + ", CAESAR will continue to function but may not be correct");
    //        }
    //        try{
    //
    //			if(inbox6.Checked)
    //			{
    //				FILE_NAME = this.infile6.Text;
    //				if(FILE_NAME!="null")
    //				{
    //					input_type_flag=0;
    //					StreamReader gr = File.OpenText(FILE_NAME);
    //					while ((input=gr.ReadLine())!=null) 
    //					{
    //						string[] lineArray;
    //						lineArray = input.Split(new char[]{' '});
    //						for (x = 0; x<=(lineArray.Length-1); x++)
    //						{
    //							if(lineArray[x]!="")
    //							{
    //								tempx=int.Parse(lineArray[0]);
    //								inputfile[5,tempx,x]=double.Parse(lineArray[x]);
    //							}
    //						}
    //					}
    //					gr.Close();
    //				}
    //			}
    //        }
    //        catch (Exception)
    //        {
    //            MessageBox.Show("There was some type of error loading the input data from " + FILE_NAME + " at line " + Convert.ToString(tempx) + ", CAESAR will continue to function but may not be correct");
    //        }
    //        try{
    //			if(inbox7.Checked)
    //			{
    //				FILE_NAME = this.infile7.Text;
    //				if(FILE_NAME!="null")
    //				{
    //					input_type_flag=0;
    //					StreamReader gr = File.OpenText(FILE_NAME);
    //					while ((input=gr.ReadLine())!=null) 
    //					{
    //						string[] lineArray;
    //						lineArray = input.Split(new char[]{' '});
    //						for (x = 0; x<=(lineArray.Length-1); x++)
    //						{
    //							if(lineArray[x]!="")
    //							{
    //								tempx=int.Parse(lineArray[0]);
    //								inputfile[6,tempx,x]=double.Parse(lineArray[x]);
    //							}
    //						}
    //					}
    //					gr.Close();
    //				}
    //			}
    //        }
    //        catch (Exception)
    //        {
    //            MessageBox.Show("There was some type of error loading the input data from " + FILE_NAME + " at line " + Convert.ToString(tempx) + ", CAESAR will continue to function but may not be correct");
    //        }
    //
    //        try{
    //			if(inbox8.Checked)
    //			{
    //				FILE_NAME = this.infile8.Text;
    //				if(FILE_NAME!="null")
    //				{
    //					input_type_flag=0;
    //					StreamReader gr = File.OpenText(FILE_NAME);
    //					while ((input=gr.ReadLine())!=null) 
    //					{
    //						string[] lineArray;
    //						lineArray = input.Split(new char[]{' '});
    //						for (x = 0; x<=(lineArray.Length-1); x++)
    //						{
    //							if(lineArray[x]!="")
    //							{
    //								tempx=int.Parse(lineArray[0]);
    //								inputfile[7,tempx,x]=double.Parse(lineArray[x]);
    //							}
    //						}
    //					}
    //					gr.Close();
    //				}
    //			}
    //		}
    //        catch(Exception)
    //		{
    //			MessageBox.Show("There was some type of error loading the input data from "+FILE_NAME+" at line "+Convert.ToString(tempx)+", CAESAR will continue to function but may not be correct");
    //		}
    //
    //		          			// MessageBox.Show(Convert.ToString(input_type_flag));
    //		this.InfoStatusPanel.Text="Loaded: type = " + input_type_flag.ToString();        // MJ 02/02/05			
    //
    //	}

    double d50(int index1) {
        int z, n, i;
        double active_thickness = 0;
        double Dfifty = 0, max = 0, min = 0;
        double[] cum_tot;
        cum_tot = new double[20];

        for (n = 1; n <= G_MAX; n++) {
            for (z = 0; z <= (0); z++) {
                active_thickness += (grain[index1][n]);
                cum_tot[n] += active_thickness;
            }
        }

        i = 1;
        while (cum_tot[i] < (active_thickness * 0.5) && i <= 9) {
            i++;
        }

        if (i == 1) {
            min = Math.log(d1);
            max = Math.log(d1);
        }
        if (i == 2) {
            min = Math.log(d1);
            max = Math.log(d2);
        }
        if (i == 3) {
            min = Math.log(d2);
            max = Math.log(d3);
        }
        if (i == 4) {
            min = Math.log(d3);
            max = Math.log(d4);
        }
        if (i == 5) {
            min = Math.log(d4);
            max = Math.log(d5);
        }
        if (i == 6) {
            min = Math.log(d5);
            max = Math.log(d6);
        }
        if (i == 7) {
            min = Math.log(d6);
            max = Math.log(d7);
        }
        if (i == 8) {
            min = Math.log(d7);
            max = Math.log(d8);
        }
        if (i == 9) {
            min = Math.log(d8);
            max = Math.log(d9);
        }
        //if(i==9){min=Math.Log(d8);max=Math.Log(d9);}

        Dfifty = Math.exp(max
                - ((max - min) * ((cum_tot[i] - (active_thickness * 0.5)) / (cum_tot[i] - cum_tot[i - 1]))));
        if (active_thickness < 0.0000001)
            Dfifty = 0;
        return Dfifty;
    }

    double max_bed_slope2(int x, int y) {

        double slope = 0;
        int slopetot = 0;
        double slopemax = 0;

        if (elev[x][y] > elev[x][y - 1]) {
            slope = Math.pow((elev[x][y] - elev[x][y - 1]) / DX, 1);
            if (slope > slopemax)
                slopemax = slope;
            slopetot++;
        }
        if (elev[x][y] > elev[x + 1][y - 1]) {
            slope = Math.pow((elev[x][y] - elev[x + 1][y - 1]) / root, 1);
            if (slope > slopemax)
                slopemax = slope;
            slopetot++;
        }
        if (elev[x][y] > elev[x + 1][y]) {
            slope = Math.pow((elev[x][y] - elev[x + 1][y]) / DX, 1);
            if (slope > slopemax)
                slopemax = slope;
            slopetot++;
        }
        if (elev[x][y] > elev[x + 1][y + 1]) {
            slope = Math.pow((elev[x][y] - elev[x + 1][y + 1]) / root, 1);
            if (slope > slopemax)
                slopemax = slope;
            slopetot++;
        }
        if (elev[x][y] > elev[x][y + 1]) {
            slope = Math.pow((elev[x][y] - elev[x][y + 1]) / DX, 1);
            if (slope > slopemax)
                slopemax = slope;
            slopetot++;
        }
        if (elev[x][y] > elev[x - 1][y + 1]) {
            slope = Math.pow((elev[x][y] - elev[x - 1][y + 1]) / root, 1);
            if (slope > slopemax)
                slopemax = slope;
            slopetot++;
        }
        if (elev[x][y] > elev[x - 1][y]) {
            slope = Math.pow((elev[x][y] - elev[x - 1][y]) / DX, 1);
            if (slope > slopemax)
                slopemax = slope;
            slopetot++;
        }
        if (elev[x][y] > elev[x - 1][y - 1]) {
            slope = Math.pow((elev[x][y] - elev[x - 1][y - 1]) / root, 1);
            if (slope > slopemax)
                slopemax = slope;
            slopetot++;
        }
        //if (slope > 0) slope = (slope / slopetot);

        //return(slopemax);
        //if(slopemax<0.001)slopemax=0.001;
        //if (slopemax > 0.01) slopemax = 0.01;
        return slopemax;
    }

    void addGS(int x, int y) {
        // needs lock statement to stop two being added at the same time...
        synchronized (this) {
            int n, q;
            grain_array_tot++;
            index[x][y] = grain_array_tot;

            grain[grain_array_tot][0] = 0;
            grain[grain_array_tot][1] = active * d1prop;
            grain[grain_array_tot][2] = active * d2prop;//+(active*d1prop);
            grain[grain_array_tot][3] = active * d3prop;
            grain[grain_array_tot][4] = active * d4prop;
            grain[grain_array_tot][5] = active * d5prop;
            grain[grain_array_tot][6] = active * d6prop;
            grain[grain_array_tot][7] = active * d7prop;
            grain[grain_array_tot][8] = active * d8prop;
            grain[grain_array_tot][9] = active * d9prop;
            grain[grain_array_tot][10] = 0;

            for (n = 0; n <= 9; n++) {
                strata[grain_array_tot][n][0] = (active) * d1prop;
                strata[grain_array_tot][n][1] = (active * d2prop);
                strata[grain_array_tot][n][2] = (active) * d3prop;
                strata[grain_array_tot][n][3] = (active) * d4prop;
                strata[grain_array_tot][n][4] = (active) * d5prop;
                strata[grain_array_tot][n][5] = (active) * d6prop;
                strata[grain_array_tot][n][6] = (active) * d7prop;
                strata[grain_array_tot][n][7] = (active) * d8prop;
                strata[grain_array_tot][n][8] = (active) * d9prop;

                if (elev[x][y] - (active * (n + 1)) < (bedrock[x][y] - active)) {
                    for (q = 0; q <= (G_MAX - 2); q++) {
                        strata[grain_array_tot][n][q] = 0;
                    }
                }
            }

            sort_active(x, y);

        }
    }

    void sort_active(int x, int y) {
        int xyindex;
        double total;
        double amount;
        double coeff;
        int n, z;

        if (index[x][y] == -9999)
            addGS(x, y); // should not be necessary
        xyindex = index[x][y];

        total = 0.0;
        for (n = 0; n <= G_MAX; n++) {

            total += grain[xyindex][n];

        }

        if (total > (active * 1.5)) // depositing - create new strata layer and remove bottom one..
        {
            // start from bottom
            // remove bottom active layer
            // then move all from layer above into one below, up to the top layer
            for (z = 9; z >= 1; z--) {
                for (n = 0; n <= G_MAX - 2; n++) {

                    strata[xyindex][z][n] = strata[xyindex][z - 1][n];

                }
            }

            // then remove strata thickness from grain - and add to top strata layer
            coeff = active / total;
            for (n = 1; n <= (G_MAX - 1); n++) {
                if ((grain[xyindex][n] > 0.0)) {

                    amount = coeff * (grain[xyindex][n]);
                    strata[xyindex][0][n - 1] = amount;
                    grain[xyindex][n] -= amount;

                }
            }
        }

        if (total < (active / 4)) // eroding - eat into existing strata layer & create new one at bottom
        {
            // Start at top
            // Add top strata to grain
            for (n = 1; n <= (G_MAX - 1); n++) {
                grain[xyindex][n] += strata[xyindex][0][n - 1];
            }

            // then from top down add lower strata into upper
            for (z = 0; z <= 8; z++) {
                for (n = 0; n <= G_MAX - 2; n++) {
                    strata[xyindex][z][n] = strata[xyindex][z + 1][n];
                }
            }

            // add new layer at the bottom
            amount = active;
            z = 9;
            strata[xyindex][z][0] = amount * d1prop; // 0.0;
            strata[xyindex][z][1] = amount * d2prop; // + amount*d1prop;
            strata[xyindex][z][2] = amount * d3prop;
            strata[xyindex][z][3] = amount * d4prop;
            strata[xyindex][z][4] = amount * d5prop;
            strata[xyindex][z][5] = amount * d6prop;
            strata[xyindex][z][6] = amount * d7prop;
            strata[xyindex][z][7] = amount * d8prop;
            strata[xyindex][z][8] = amount * d9prop;
        }

    }

    double sand_fraction(int index1) {

        int n;
        double active_thickness = 0;
        double sand_total = 0;
        for (n = 1; n <= G_MAX; n++) {
            active_thickness += (grain[index1][n]);
        }

        for (n = 1; n <= 2; n++) // number of sand fractions...
        {
            sand_total += (grain[index1][n]);
        }

        if (active_thickness < 0.0001) {
            return (0.0);
        } else {
            return (sand_total / active_thickness);
        }

    }

    // calc roughtness on Srickler relationship
    double strickler(int x, int y) {
        double temp = 0;
        double temp_d50 = 1;

        if (index[x][y] != -9999)
            temp_d50 = d50(index[x][y]);
        temp = 0.034 * Math.pow(temp_d50, 0.167);
        //Console.WriteLine(Convert.ToString(strickler(x,y)));
        temp += 0.012;
        return temp;
    }

    void grow_grass(double amount3) {

        int x, y;
        for (x = 1; x <= xmax; x++) {
            for (y = 1; y <= ymax; y++) {
                //first check if veg is at 0.. not sure if this is needed now..
                if (veg[x][y][0] == 0) {
                    veg[x][y][0] = elev[x][y];
                }

                // first check if under water or not..
                if (water_depth[x][y] < water_depth_erosion_threshold) {
                    // if not then it
                    // now adds to the amount of veg there.. 
                    veg[x][y][1] += amount3;
                    if (veg[x][y][1] > 1)
                        veg[x][y][1] = 1;
                }

                // check if veg below elev! if so raise to elev
                if (veg[x][y][0] < elev[x][y]) // raises the veg level if it gets buried.. but only by a certain amount (0.001m day...)
                {
                    veg[x][y][0] += 0.001; // this is an arbitrary amount..
                    if (veg[x][y][0] > elev[x][y])
                        veg[x][y][0] = elev[x][y];
                }

                // check if veg above elev! if so lower to elev
                if (veg[x][y][0] > elev[x][y]) {
                    veg[x][y][0] -= 0.001; // this is an arbitrary amount..
                    if (veg[x][y][0] < elev[x][y])
                        veg[x][y][0] = elev[x][y];
                }

                // trying to see now, if veg is above elev - so if lateral erosion happens - more than 0.1m the veg gets wiped..

                if (veg[x][y][0] - elev[x][y] > 0.1) {
                    veg[x][y][1] = 0;
                    veg[x][y][0] = elev[x][y];
                }

                // now see if veg under water - if so let it die back a bit..
                if (water_depth[x][y] >= water_depth_erosion_threshold && veg[x][y][1] > 0) {
                    veg[x][y][1] -= (amount3 / 2);
                    if (veg[x][y][1] < 0) {
                        veg[x][y][1] = 0;
                        veg[x][y][0] = elev[x][y]; // resets elev if veg amt is 0
                    }
                }

                // also if it is under sediment - then dies back a bit too...
                if (veg[x][y][0] < elev[x][y]) {
                    veg[x][y][1] -= (amount3 / 2);
                    if (veg[x][y][1] < 0) {
                        veg[x][y][1] = 0;
                        veg[x][y][0] = elev[x][y]; // resets elev if veg amt is 0
                    }

                }

                // but if it is under sediment, has died back to nearly 0 (0.05) then it resets the elevation 
                // to the surface elev.
                if (veg[x][y][0] < elev[x][y] && veg[x][y][1] < 0.05) {
                    veg[x][y][0] = elev[x][y];
                }

            }
        }
    }

    void creep(double time) {

        /** creep rate is 10*-2 * slope per year, so inputs time jump in years*/
        /** very important differnece here is that slide_GS() is called only if
        	BOTH cells are not -9999 therfore if both have grainsize then do additions.
        	this is to stop the progressive spread of selected cells upslope */

        int x, y;
        double temp;

        for (x = 1; x <= xmax; x++) {
            for (y = 1; y <= ymax; y++) {
                tempcreep[x][y] = 0;
            }
        }

        for (x = 2; x < xmax; x++) {
            for (y = 2; y < ymax; y++) {
                if (elev[x][y] > bedrock[x][y]) {
                    if (elev[x][y - 1] < elev[x][y] && elev[x][y - 1] > 0) {
                        temp = ((elev[x][y] - elev[x][y - 1]) / DX) * CREEP_RATE * time / DX;
                        if ((elev[x][y] - temp) < bedrock[x][y])
                            temp = elev[x][y] - bedrock[x][y];
                        if (temp < 0)
                            temp = 0;
                        tempcreep[x][y] -= temp;
                        tempcreep[x][y - 1] += temp;
                        if (index[x][y] != -9999) {
                            slide_GS(x, y, temp, x, y - 1);
                        }
                    }
                    if (elev[x + 1][y - 1] < elev[x][y] && elev[x + 1][y - 1] > 0) {
                        temp = ((elev[x][y] - elev[x + 1][y - 1]) / root) * CREEP_RATE * time / root;
                        if ((elev[x][y] - temp) < bedrock[x][y])
                            temp = elev[x][y] - bedrock[x][y];
                        if (temp < 0)
                            temp = 0;
                        tempcreep[x][y] -= temp;
                        tempcreep[x + 1][y - 1] += temp;
                        if (index[x][y] != -9999) {
                            slide_GS(x, y, temp, x + 1, y - 1);
                        }

                    }
                    if (elev[x + 1][y] < elev[x][y] && elev[x + 1][y] > 0) {
                        temp = ((elev[x][y] - elev[x + 1][y]) / DX) * CREEP_RATE * time / DX;
                        if ((elev[x][y] - temp) < bedrock[x][y])
                            temp = elev[x][y] - bedrock[x][y];
                        if (temp < 0)
                            temp = 0;
                        tempcreep[x][y] -= temp;
                        tempcreep[x + 1][y] += temp;
                        if (index[x][y] != -9999) {
                            slide_GS(x, y, temp, x + 1, y);
                        }
                    }
                    if (elev[x + 1][y + 1] < elev[x][y] && elev[x + 1][y + 1] > 0) {
                        temp = ((elev[x][y] - elev[x + 1][y + 1]) / root) * CREEP_RATE * time / root;
                        if ((elev[x][y] - temp) < bedrock[x][y])
                            temp = elev[x][y] - bedrock[x][y];
                        if (temp < 0)
                            temp = 0;
                        tempcreep[x][y] -= temp;
                        tempcreep[x + 1][y + 1] += temp;
                        if (index[x][y] != -9999) {
                            slide_GS(x, y, temp, x + 1, y + 1);
                        }
                    }
                    if (elev[x][y + 1] < elev[x][y] && elev[x][y + 1] > 0) {
                        temp = ((elev[x][y] - elev[x][y + 1]) / DX) * CREEP_RATE * time / DX;
                        if ((elev[x][y] - temp) < bedrock[x][y])
                            temp = elev[x][y] - bedrock[x][y];
                        if (temp < 0)
                            temp = 0;
                        tempcreep[x][y] -= temp;
                        tempcreep[x][y + 1] += temp;
                        if (index[x][y] != -9999) {
                            slide_GS(x, y, temp, x, y + 1);
                        }
                    }
                    if (elev[x - 1][y + 1] < elev[x][y] && elev[x - 1][y + 1] > 0) {
                        temp = ((elev[x][y] - elev[x - 1][y + 1]) / root) * CREEP_RATE * time / root;
                        if ((elev[x][y] - temp) < bedrock[x][y])
                            temp = elev[x][y] - bedrock[x][y];
                        if (temp < 0)
                            temp = 0;
                        tempcreep[x][y] -= temp;
                        tempcreep[x - 1][y + 1] += temp;
                        if (index[x][y] != -9999) {
                            slide_GS(x, y, temp, x - 1, y + 1);
                        }
                    }
                    if (elev[x - 1][y] < elev[x][y] && elev[x - 1][y] > 0) {
                        temp = ((elev[x][y] - elev[x - 1][y]) / DX) * CREEP_RATE * time / DX;
                        if ((elev[x][y] - temp) < bedrock[x][y])
                            temp = elev[x][y] - bedrock[x][y];
                        if (temp < 0)
                            temp = 0;
                        tempcreep[x][y] -= temp;
                        tempcreep[x - 1][y] += temp;
                        if (index[x][y] != -9999) {
                            slide_GS(x, y, temp, x - 1, y);
                        }
                    }
                    if (elev[x - 1][y - 1] < elev[x][y] && elev[x - 1][y - 1] > 0) {
                        temp = ((elev[x][y] - elev[x - 1][y - 1]) / root) * CREEP_RATE * time / root;
                        if ((elev[x][y] - temp) < bedrock[x][y])
                            temp = elev[x][y] - bedrock[x][y];
                        if (temp < 0)
                            temp = 0;
                        tempcreep[x][y] -= temp;
                        tempcreep[x - 1][y - 1] += temp;
                        if (index[x][y] != -9999) {
                            slide_GS(x, y, temp, x - 1, y - 1);
                        }
                    }
                }
            }
        }

        for (x = 1; x <= xmax; x++) {
            for (y = 1; y <= ymax; y++) {
                elev[x][y] += tempcreep[x][y];
            }
        }

    }

    void slide_GS(int x, int y, double amount, int x2, int y2) {

        /** Ok, heres how it works, x and y are ones material moved from,
          x2 and y2 are ones material moved to...
          amd amount is the amount shifted. */

        int n;
        double total = 0;

        // do only for cells where both have grainsize..

        if (index[x][y] != -9999 && index[x2][y2] != -9999) {
            for (n = 1; n <= (G_MAX - 1); n++) {
                if (grain[index[x][y]][n] > 0)
                    total += grain[index[x][y]][n];
            }

            if (amount > total) {
                grain[index[x2][y2]][1] += (amount - total) * d1prop;
                grain[index[x2][y2]][2] += (amount - total) * d2prop;
                grain[index[x2][y2]][3] += (amount - total) * d3prop;
                grain[index[x2][y2]][4] += (amount - total) * d4prop;
                grain[index[x2][y2]][5] += (amount - total) * d5prop;
                grain[index[x2][y2]][6] += (amount - total) * d6prop;
                grain[index[x2][y2]][7] += (amount - total) * d7prop;
                grain[index[x2][y2]][8] += (amount - total) * d8prop;
                grain[index[x2][y2]][9] += (amount - total) * d9prop;

                amount = total;
            }

            if (total > 0) {
                for (n = 1; n <= (G_MAX - 1); n++) {
                    double transferamt = amount * (grain[index[x][y]][n] / total);
                    grain[index[x2][y2]][n] += transferamt;
                    grain[index[x][y]][n] -= transferamt;
                    if (grain[index[x][y]][n] < 0)
                        grain[index[x][y]][n] = 0;
                }

            }

            /* then to set active layer to correct depth before erosion, */
            sort_active(x, y);
            sort_active(x2, y2);
        }

        //now do for cells where only recieving cells have grainsize
        // just adds amount to reviving cells of normal..
        if (index[x][y] == -9999 && index[x2][y2] != -9999) {
            grain[index[x2][y2]][1] += (amount) * d1prop;
            grain[index[x2][y2]][2] += (amount) * d2prop;
            grain[index[x2][y2]][3] += (amount) * d3prop;
            grain[index[x2][y2]][4] += (amount) * d4prop;
            grain[index[x2][y2]][5] += (amount) * d5prop;
            grain[index[x2][y2]][6] += (amount) * d6prop;
            grain[index[x2][y2]][7] += (amount) * d7prop;
            grain[index[x2][y2]][8] += (amount) * d8prop;
            grain[index[x2][y2]][9] += (amount) * d9prop;

            /* then to set active layer to correct depth before erosion, */
            sort_active(x2, y2);
        }

        // now for cells whre dontaing cell has grainsize
        if (index[x][y] != -9999 && index[x2][y2] == -9999) {

            addGS(x2, y2); // add grainsize array for recieving cell..

            if (amount > active) {

                grain[index[x2][y2]][1] += (amount - active) * d1prop;
                grain[index[x2][y2]][2] += (amount - active) * d2prop;
                grain[index[x2][y2]][3] += (amount - active) * d3prop;
                grain[index[x2][y2]][4] += (amount - active) * d4prop;
                grain[index[x2][y2]][5] += (amount - active) * d5prop;
                grain[index[x2][y2]][6] += (amount - active) * d6prop;
                grain[index[x2][y2]][7] += (amount - active) * d7prop;
                grain[index[x2][y2]][8] += (amount - active) * d8prop;
                grain[index[x2][y2]][9] += (amount - active) * d9prop;

                amount = active;
            }

            for (n = 1; n <= (G_MAX - 1); n++) {
                if (grain[index[x][y]][n] > 0.000)
                    total += grain[index[x][y]][n];
            }

            for (n = 1; n <= (G_MAX - 1); n++) {
                if (total > 0) {
                    grain[index[x2][y2]][n] += amount * (grain[index[x][y]][n] / total);
                    if (grain[index[x][y]][n] > 0.0001)
                        grain[index[x][y]][n] -= amount * (grain[index[x][y]][n] / total);
                    if (grain[index[x][y]][n] < 0)
                        grain[index[x][y]][n] = 0;
                }

            }

            /* then to set active layer to correct depth before erosion, */
            sort_active(x, y);
            sort_active(x2, y2);
        }

    }

    double erode(final double mult_factor) {
        final double rho = 1000.0;
        final double gravity = 9.8;
        final Double[][][] sr, sl, su, sd, sne, sse, snw, ssw;
        final double[][] ss = new double[xmax + 2][ymax + 2];
        ;

        // ugly, but so delicious.
        final double[] tempbmax = new double[] { 0.0 };

        // temp arrays that control how much sediment goes up,. righ left down and into suspended sediment.
        sr = new Double[xmax + 2][ymax + 2][10];
        sl = new Double[xmax + 2][ymax + 2][10];
        su = new Double[xmax + 2][ymax + 2][10];
        sd = new Double[xmax + 2][ymax + 2][10];

        sne = new Double[xmax + 2][ymax + 2][10];
        sse = new Double[xmax + 2][ymax + 2][10];
        snw = new Double[xmax + 2][ymax + 2][10];
        ssw = new Double[xmax + 2][ymax + 2][10];

        Double[] gtot2 = new Double[20];

        for (int n = 0; n <= G_MAX; n++) {
            gtot2[n] = 0.0;
        }

        //
        // first add in any sediment from file....
        //
        time_factor = time_factor * 1.5;
        if (time_factor > max_time_step)
            time_factor = max_time_step;

        int counter2 = 0;
        do {
            counter2++;
            Parallel.For(1, ymax, new ParallelOpInt() {

                public void run(int y) {

                    int inc = 1;
                    while (down_scan[y][inc] > 0) {
                        int x = down_scan[y][inc];
                        inc++;

                        // zero vels.
                        Vel[x][y] = 0;

                        if (water_depth[x][y] > water_depth_erosion_threshold && x < xmax
                                && elev[x][y] > bedrock[x][y]) {
                            double temptot2 = 0;
                            double veltot = 0;
                            double vel = 0;
                            double qtot = 0;
                            double tau = 0;
                            double tauvel = 0;
                            double velnum = 0;
                            Double[] temp_dist, tempdir;
                            tempdir = new Double[11]; // array that holds velocity directions temp - so they dont have to be calculated again
                            temp_dist = new Double[11]; // array that holds amount to be removed from cell in each grainsize

                            // zero ss array
                            ss[x][y] = 0.0;

                            for (int n = 1; n <= 9; n++) {
                                sr[x][y][n] = 0.0;
                                sl[x][y][n] = 0.0;
                                su[x][y][n] = 0.0;
                                sd[x][y][n] = 0.0;
                                temp_dist[n] = 0.0;
                            }

                            // check to see if index for that cell...
                            if (index[x][y] == -9999)
                                addGS(x, y);

                            // now tot up velocity directions, velocities and edge directions.
                            for (int p = 1; p <= 8; p += 2) {
                                int x2 = x + deltaX[p];
                                int y2 = y + deltaY[p];
                                if (water_depth[x2][y2] > water_depth_erosion_threshold) {

                                    if (edge[x][y] > edge[x2][y2]) {

                                        temptot2 += (edge[x][y] - edge[x2][y2]);

                                    }

                                    if (vel_dir[x][y][p] > 0) {
                                        // first work out velocities in each direction (for sedi distribution)
                                        vel = vel_dir[x][y][p];
                                        if (vel > max_vel) {
                                            // TODO monitor warn
                                            // this.tempStatusPanel.Text = "vel too high = " + Convert.ToString(vel);
                                            vel = max_vel; // if vel too high cut it
                                        }
                                        tempdir[p] = vel * vel;
                                        veltot += tempdir[p];

                                        velnum++;
                                        qtot += (vel * vel);
                                    }
                                }
                            }

                            if (qtot > 0) {
                                vel = (Math.sqrt(qtot));
                                Vel[x][y] = vel;
                                if (vel > max_vel)
                                    vel = max_vel; // if vel too high cut it
                                double ci = 9.81 * (mannings * mannings) * Math.pow(water_depth[x][y], -0.33);

                                tauvel = 1000 * ci * vel * vel;

                            }

                            tau = tauvel;
                            Tau[x][y] = tau;

                            // taufactor controls how much goes to lateral or to routed by velocity.. 
                            //double taufactor = 1 -(Math.Pow(edge2[x, y],1.1) * 2);
                            //if (taufactor < 0.7) taufactor = 0.7;
                            double taufactor = 0.75;

                            // now do some erosion

                            if (tau > 0) {
                                double d_50 = 0;
                                double Fs = 0, Di = 0, tau_ri = 0, U_star, Wi_star;
                                if (wilcock == 1) {
                                    d_50 = d50(index[x][y]);
                                    if (d_50 < d1)
                                        d_50 = d1;
                                    Fs = sand_fraction(index[x][y]);
                                }

                                ////

                                for (int n = 1; n <= 9; n++) {
                                    switch (n) {
                                    case 1:
                                        Di = d1;
                                        break;
                                    case 2:
                                        Di = d2;
                                        break;
                                    case 3:
                                        Di = d3;
                                        break;
                                    case 4:
                                        Di = d4;
                                        break;
                                    case 5:
                                        Di = d5;
                                        break;
                                    case 6:
                                        Di = d6;
                                        break;
                                    case 7:
                                        Di = d7;
                                        break;
                                    case 8:
                                        Di = d8;
                                        break;
                                    case 9:
                                        Di = d9;
                                        break;
                                    }

                                    // Wilcock and Crowe/Curran

                                    if (wilcock == 1) {
                                        tau_ri = (0.021 + (0.015 * Math.exp(-20 * Fs)))
                                                * (rho * gravity * d_50)
                                                * Math.pow((Di / d_50),
                                                        (0.67 / (1 + Math.exp(1.5 - (Di / d_50)))));
                                        U_star = Math.pow(tau / rho, 0.5);
                                        if ((tau / tau_ri) < 1.35) {
                                            Wi_star = 0.002 * Math.pow(tau / tau_ri, 7.5);

                                        } else {
                                            Wi_star = 14 * Math.pow(
                                                    1 - (0.894 / Math.pow(tau / tau_ri, 0.5)), 4.5);
                                        }
                                        //maybe should divide by DX as well..
                                        temp_dist[n] = mult_factor
                                                * time_factor
                                                * ((Fi(index[x][y], n) * Math.pow(U_star, 3)) / ((2.65 - 1) * gravity))
                                                * Wi_star / DX;
                                    }
                                    if (einstein == 1) {
                                        // maybe should divide by DX as well.. 
                                        temp_dist[n] = mult_factor
                                                * time_factor
                                                * (40 * Math.pow((1 / (((2250 - 1000) * Di) / (tau / 9.8))),
                                                        3))
                                                / Math.sqrt(1000 / ((2250 - 1000) * 9.8 * Math.pow(Di, 3)))
                                                / DX;
                                    }

                                    if (temp_dist[n] < 0.0000000000001)
                                        temp_dist[n] = 0.0;

                                    //
                                }

                                // Vegetation parts:
                                // veg components

                                // here to remove from veg layer..
                                if ((veg[x][y][1] > 0) && (tau > vegTauCrit)) {
                                    // now to remove from veg layer..
                                    veg[x][y][1] -= time_factor * Math.pow(tau - vegTauCrit, 0.5) * 0.00001;
                                    if (veg[x][y][1] < 0)
                                        veg[x][y][1] = 0;
                                }

                                // now to determine if movement should be restricted due to veg...
                                if (veg[x][y][1] > 0.25) {
                                    double temptot = 0;
                                    double elevdiff = 0;
                                    // adds up total of material to be removed.. including calculating how much is there to be moved
                                    for (int n = 1; n <= 9; n++)
                                        temptot += temp_dist[n];

                                    // now checks if this removed from the cell would put it below the veg layer..
                                    if (elev[x][y] - temptot <= veg[x][y][0]) {
                                        // now remove from proportion that can be eroded..
                                        // we can do this as we have the prop (in temptot) that is there to be eroded.
                                        elevdiff = elev[x][y] - veg[x][y][0];
                                        for (int n = 1; n <= 9; n++) {
                                            temp_dist[n] = elevdiff * (temp_dist[n] / temptot);
                                            if (elev[x][y] <= veg[x][y][0])
                                                temp_dist[n] = 0.0;
                                        }
                                    }

                                }

                                // now reduce temp_dist[n] if greater than whats already there...
                                // also adding up amount eroded..
                                double temptot1 = 0;
                                for (int n = 1; n <= 9; n++) {

                                    // first check to see that theres not too little sediment in a cell to be entrained
                                    if (temp_dist[n] > grain[index[x][y]][n])
                                        temp_dist[n] = grain[index[x][y]][n];

                                    // then check to see if this would make SS levels too high.. and if so reduce
                                    //
                                    if (n == 1 && isSuspended[n]) {
                                        if ((temp_dist[n] + Vsusptot[x][y]) / water_depth[x][y] > Csuspmax) {
                                            //work out max amount of sediment that can be there (waterdepth * csuspmax) then subtract whats already there
                                            // (Vsusptot) to leave what can be entrained. Check if < 0 after.
                                            temp_dist[n] = (water_depth[x][y] * Csuspmax) - Vsusptot[x][y];

                                        }
                                        ;
                                    }

                                    if (temp_dist[n] < 0)
                                        temp_dist[n] = 0.0;

                                    // nwo placed here speeding up reduction of erode repeats.
                                    temptot1 += temp_dist[n];

                                }
                                if (temptot1 > tempbmax[0])
                                    tempbmax[0] = temptot1;

                                // now work out what portion of bedload has to go where...
                                // only allow actual transfer of sediment if there is flow in a direction - i.e. some sedeiment transport

                                if (temptot1 > 0) {
                                    for (int p = 1; p <= 8; p += 2) {
                                        int x2 = x + deltaX[p];
                                        int y2 = y + deltaY[p];

                                        if (water_depth[x2][y2] >= water_depth_erosion_threshold) {
                                            if (index[x2][y2] == -9999)
                                                addGS(x2, y2);

                                            double factor = 0;

                                            if (vel_dir[x][y][p] > 0) {
                                                factor += taufactor * tempdir[p] / veltot;
                                            }

                                            if ((edge[x][y] > edge[x2][y2])) {
                                                if (p == 1 || p == 3 || p == 5 || p == 7) {
                                                    factor += (1 - taufactor)
                                                            * ((edge[x][y] - edge[x2][y2]) / temptot2);
                                                }
                                                //else factor += (1 - taufactor) * (((edge[x, y] - edge[x2, y2])*0.707) / temptot2);
                                            }

                                            for (int n = 1; n <= 9; n++) {
                                                if (temp_dist[n] > 0) {
                                                    if (n == 1 && isSuspended[n]) {
                                                        // put amount entrained by ss in to ss[,]
                                                        ss[x][y] = temp_dist[n];
                                                    } else {
                                                        switch (p) {
                                                        case 1:
                                                            su[x][y][n] = temp_dist[n] * factor;
                                                            break;

                                                        case 3:
                                                            sr[x][y][n] = temp_dist[n] * factor;
                                                            break;

                                                        case 5:
                                                            sd[x][y][n] = temp_dist[n] * factor;
                                                            break;

                                                        case 7:
                                                            sl[x][y][n] = temp_dist[n] * factor;
                                                            break;

                                                        }

                                                    }
                                                }

                                            }
                                        }
                                    }
                                }

                            }

                        }
                    }
                }
            });

            if (tempbmax[0] > ERODEFACTOR) {
                //time_factor /= 2;
                time_factor *= (ERODEFACTOR / tempbmax[0]) * 0.5;
            }

        } while (tempbmax[0] > ERODEFACTOR);

        //tempStatusPanel.Text = Convert.ToString(counter2);

        //
        Parallel.For(2, ymax, new ParallelOpInt() {
            public void run(int y) {
                int inc = 1;
                while (down_scan[y][inc] > 0) {
                    int x = down_scan[y][inc];
                    inc++;

                    if (water_depth[x][y] > water_depth_erosion_threshold && x < xmax && x > 1) {
                        double erodetot = 0;
                        if (index[x][y] == -9999)
                            addGS(x, y);
                        for (int n = 1; n <= 9; n++) {
                            if (n == 1 && isSuspended[n]) {
                                // updating entrainment of SS
                                Vsusptot[x][y] += ss[x][y];
                                grain[index[x][y]][n] -= ss[x][y];
                                erodetot -= ss[x][y];

                                // this next part is unusual. You have to stop susp sed deposition on the input cells, otherwies
                                // it drops sediment out, but cannot entrain as ss levels in input are too high leading to
                                // little mountains of sediment. This means a new array in order to check whether a cell is an 
                                // input point or not..
                                if (!inputpointsarray[x][y]) {
                                    // now calc ss to be dropped
                                    double coeff = (fallVelocity[n] * time_factor) / water_depth[x][y];
                                    if (coeff > 1)
                                        coeff = 1;
                                    double Vpdrop = coeff * Vsusptot[x][y];
                                    if (Vpdrop > 0.001)
                                        Vpdrop = 0.001; //only allow 1mm to be deposited per iteration
                                    grain[index[x][y]][n] += Vpdrop;
                                    erodetot += Vpdrop;
                                    Vsusptot[x][y] -= Vpdrop;
                                    //if (Vsusptot[x, y] < 0) Vsusptot[x, y] = 0; NOT this line.
                                }
                            } else {
                                //else update grain and elevations for bedload.
                                double val1 = (su[x][y][n] + sr[x][y][n] + sd[x][y][n] + sl[x][y][n]);
                                grain[index[x][y]][n] -= val1;
                                erodetot -= val1;

                                double val2 = (su[x][y + 1][n] + sd[x][y - 1][n] + sl[x + 1][y][n] + sr[x - 1][y][n]);
                                grain[index[x][y]][n] += val2;
                                erodetot += val2;
                            }
                        }

                        elev[x][y] += erodetot;
                        if (erodetot != 0)
                            sort_active(x, y);

                        //
                        // test lateral code...
                        //

                        erodetot = 0 - erodetot;
                        if (erodetot > 0) {
                            double olderodetot = erodetot * Math.pow(DX, 0.850);
                            double elev_update = 0;
                            double flowparams = water_depth[x][y] * Vel[x][y];

                            // remember to add divide by DX... done above
                            // also consider limiting slope to water depth? stop too high erosion of ourte: done
                            // do I need to divide by DX again to account for slope? or not..? yes, done above

                            if (elev[x - 1][y] > elev[x][y] && x > 2) {
                                double amt = 0;
                                double amt2 = 0;
                                if (water_depth[x - 1][y] > water_depth_erosion_threshold)
                                    amt = erodetot * lateral_constant * (elev[x - 1][y] - elev[x][y]) / DX;
                                if (water_depth[x - 1][y] < water_depth_erosion_threshold
                                        && edge[x - 1][y] > 0) {
                                    amt2 = mult_factor * bed_proportion * Tau[x][y] * edge[x - 1][y]
                                            * time_factor;
                                    if (amt2 > erodetot / 2)
                                        amt2 = erodetot / 2;
                                    amt += amt2;
                                }
                                if (amt > 0) {
                                    amt *= 1 - (veg[x - 1][y][1] * (1 - veg_lat_restriction));
                                    if ((elev[x - 1][y] - amt) < bedrock[x - 1][y])
                                        amt = 0;
                                    if (amt > ERODEFACTOR * 0.1)
                                        amt = ERODEFACTOR * 0.1;
                                    //if (amt > erodetot/2) amt = erodetot/2;
                                    elev_update += amt;
                                    elev[x - 1][y] -= amt;
                                    slide_GS(x - 1, y, amt, x, y);
                                }
                            }
                            if (elev[x + 1][y] > elev[x][y] && x < xmax - 1) {
                                double amt = 0;
                                double amt2 = 0;
                                if (water_depth[x + 1][y] > water_depth_erosion_threshold)
                                    amt = erodetot * lateral_constant * (elev[x + 1][y] - elev[x][y]) / DX;
                                if (water_depth[x + 1][y] < water_depth_erosion_threshold
                                        && edge[x + 1][y] > 0) {
                                    amt2 = mult_factor * bed_proportion * Tau[x][y] * edge[x + 1][y]
                                            * time_factor;
                                    if (amt2 > erodetot / 2)
                                        amt2 = erodetot / 2;
                                    amt += amt2;
                                }
                                if (amt > 0) {
                                    amt *= 1 - (veg[x + 1][y][1] * (1 - veg_lat_restriction));
                                    if ((elev[x + 1][y] - amt) < bedrock[x + 1][y])
                                        amt = 0;
                                    if (amt > ERODEFACTOR * 0.1)
                                        amt = ERODEFACTOR * 0.1;
                                    //if (amt > erodetot/2) amt = erodetot/2;
                                    elev_update += amt;
                                    elev[x + 1][y] -= amt;
                                    slide_GS(x + 1, y, amt, x, y);
                                }
                            }

                            if (elev[x][y - 1] > elev[x][y]) {
                                double amt = 0;
                                double amt2 = 0;
                                if (water_depth[x][y - 1] > water_depth_erosion_threshold)
                                    amt = erodetot * lateral_constant * (elev[x][y - 1] - elev[x][y]) / DX;
                                if (water_depth[x][y - 1] < water_depth_erosion_threshold
                                        && edge[x][y - 1] > 0) {
                                    amt2 = mult_factor * bed_proportion * Tau[x][y] * edge[x][y - 1]
                                            * time_factor;
                                    if (amt2 > erodetot / 2)
                                        amt2 = erodetot / 2;
                                    amt += amt2;
                                }
                                if (amt > 0) {
                                    amt *= 1 - (veg[x][y - 1][1] * (1 - veg_lat_restriction));
                                    if ((elev[x][y - 1] - amt) < bedrock[x][y - 1])
                                        amt = 0;
                                    if (amt > ERODEFACTOR * 0.1)
                                        amt = ERODEFACTOR * 0.1;
                                    //if (amt > erodetot/2) amt = erodetot/2;
                                    elev_update += amt;
                                    elev[x][y - 1] -= amt;
                                    slide_GS(x, y - 1, amt, x, y);
                                }
                            }
                            if (elev[x][y + 1] > elev[x][y]) {
                                double amt = 0;
                                double amt2 = 0;
                                if (water_depth[x][y + 1] > water_depth_erosion_threshold)
                                    amt = erodetot * lateral_constant * (elev[x][y + 1] - elev[x][y]) / DX;
                                if (water_depth[x][y + 1] < water_depth_erosion_threshold
                                        && edge[x][y + 1] > 0) {
                                    amt2 = mult_factor * bed_proportion * Tau[x][y] * edge[x][y + 1]
                                            * time_factor;
                                    if (amt2 > erodetot / 2)
                                        amt2 = erodetot / 2;
                                    amt += amt2;
                                }
                                if (amt > 0) {
                                    amt *= 1 - (veg[x][y + 1][1] * (1 - veg_lat_restriction));
                                    if ((elev[x][y + 1] - amt) < bedrock[x][y + 1])
                                        amt = 0;
                                    if (amt > ERODEFACTOR * 0.1)
                                        amt = ERODEFACTOR * 0.1;
                                    //if (amt > erodetot/2) amt = erodetot/2;
                                    elev_update += amt;
                                    elev[x][y + 1] -= amt;
                                    slide_GS(x, y + 1, amt, x, y);
                                }
                            }

                            elev[x][y] += elev_update;
                        }
                    }
                }
            }
        });

        // now calculate sediment outputs from all four edges...
        for (int y = 2; y < ymax; y++) {
            if (water_depth[xmax][y] > water_depth_erosion_threshold || Vsusptot[xmax][y] > 0) {
                for (int n = 1; n <= 9; n++) {
                    if (isSuspended[n]) {
                        gtot2[n] += Vsusptot[xmax][y];
                        Vsusptot[xmax][y] = 0;
                    } else {
                        gtot2[n] += sr[xmax - 1][y][n];

                    }
                }
            }
            if (water_depth[1][y] > water_depth_erosion_threshold || Vsusptot[1][y] > 0) {
                for (int n = 1; n <= 9; n++) {
                    if (isSuspended[n]) {
                        gtot2[n] += Vsusptot[1][y];
                        Vsusptot[1][y] = 0;
                    } else {
                        gtot2[n] += sl[2][y][n];
                    }
                }
            }
        }

        for (int x = 2; x < xmax; x++) {
            if (water_depth[x][ymax] > water_depth_erosion_threshold || Vsusptot[x][ymax] > 0) {
                for (int n = 1; n <= 9; n++) {
                    if (isSuspended[n]) {
                        gtot2[n] += Vsusptot[x][ymax];
                        Vsusptot[x][ymax] = 0;
                    } else {
                        gtot2[n] += sd[x][ymax - 1][n];

                    }
                }
            }
            if (water_depth[x][1] > water_depth_erosion_threshold || Vsusptot[x][1] > 0) {
                for (int n = 1; n <= 9; n++) {
                    if (isSuspended[n]) {
                        gtot2[n] += Vsusptot[x][1];
                        Vsusptot[x][1] = 0;
                    } else {
                        gtot2[n] += su[x][2][n];
                    }
                }
            }
        }

        /// now update files for outputing sediment and re-circulating...
        /// 

        sediQ = 0;
        for (int n = 1; n <= G_MAX; n++) {
            if (temp_grain[n] < 0)
                temp_grain[n] = 0;
            if (_input.COMPUTE_RECIRCULATION && _input.REACH_MODE)
                temp_grain[n] += gtot2[n] * recirculate_proportion; // important to divide input by time factor, so it can be reduced if re-circulating too much...
            sediQ += gtot2[n] * DX * DX;
            globalsediq += gtot2[n] * DX * DX;
            sum_grain[n] += gtot2[n] * DX * DX; // Gez
        }

        return tempbmax[0];
    }

    double Fi(int index1, int t) {
        int n;
        double active_thickness = 0;
        double total = 0;

        // part to deal with tracers is now hard coded in order to speed things up - with
        // an if statement at the start.. saves un-necessary looping

        for (n = 1; n <= G_MAX; n++) {
            active_thickness += (grain[index1][n]);
        }

        total += (grain[index1][t]);

        if (active_thickness < 0.0001) {
            return (0.0);
        } else {
            return (total / active_thickness);
        }
    }

    void get_area() {
        int x, y;

        for (x = 1; x <= xmax; x++) {
            for (y = 1; y <= ymax; y++) {
                area_depth[x][y] = 1;
                area[x][y] = 0;
                if (elev[x][y] == -9999) {
                    area_depth[x][y] = 0.0;
                }

            }
        }
        get_area4();

    }

    void get_area4() {

        // new routine for determining drainage area 4/10/2010
        // instead of using sweeps this sorts all the elevations then works frmo the
        // highest to lowest - calculating drainage area - D-infinity basically.

        int x, y, n, x2, y2, dir;

        // zero load of arrays
        Double[] tempvalues, tempvalues2, xkey, ykey;
        tempvalues = new Double[(xmax + 2) * (ymax + 2)];
        tempvalues2 = new Double[(xmax + 2) * (ymax + 2)];
        xkey = new Double[(xmax + 2) * (ymax + 2)];
        ykey = new Double[(xmax + 2) * (ymax + 2)];

        // then createst temp array based on elevs then also one for x values. 
        int inc = 1;
        for (y = 1; y <= ymax; y++) {
            for (x = 1; x <= xmax; x++) {
                tempvalues[inc] = elev[x][y];
                xkey[inc] = (double) x;
                inc++;
            }
        }

        // then sorts according to elevations - but also sorts the key (xkey) according to these too..
        new ArraySort().sortMatched(tempvalues, xkey);

        // now does the same for y values
        inc = 1;
        for (y = 1; y <= ymax; y++) {
            for (x = 1; x <= xmax; x++) {
                tempvalues2[inc] = elev[x][y];
                ykey[inc] = (double) y;
                inc++;
            }
        }

        new ArraySort().sortMatched(tempvalues2, ykey);

        // then works through the list of x and y co-ordinates from highest to lowest...
        for (n = (xmax * ymax); n >= 1; n--) {
            x = xkey[n].intValue();
            //this.InfoStatusPanel.Text = Convert.ToString(x);
            y = ykey[n].intValue();
            //this.InfoStatusPanel.Text = Convert.ToString(y);

            if (area_depth[x][y] > 0) {
                // update area if area_depth is higher
                if (area_depth[x][y] > area[x][y])
                    area[x][y] = area_depth[x][y];

                double difftot = 0;

                // work out sum of +ve slopes in all 8 directions
                for (dir = 1; dir <= 8; dir++)//was 1 to 8 +=2
                {

                    x2 = x + deltaX[dir];
                    y2 = y + deltaY[dir];
                    if (x2 < 1)
                        x2 = 1;
                    if (y2 < 1)
                        y2 = 1;
                    if (x2 > xmax)
                        x2 = xmax;
                    if (y2 > ymax)
                        y2 = ymax;

                    // swap comment lines below for drainage area from D8 or Dinfinity
                    if (Math.IEEEremainder(dir, 2) != 0) {
                        if (elev[x2][y2] < elev[x][y])
                            difftot += elev[x][y] - elev[x2][y2];
                    } else {
                        if (elev[x2][y2] < elev[x][y])
                            difftot += (elev[x][y] - elev[x2][y2]) / 1.414;
                    }
                    //if(elev[x,y]-elev[x2,y2]>difftot)difftot=elev[x,y]-elev[x2,y2];
                }
                if (difftot > 0) {
                    // then distribute to all 8...
                    for (dir = 1; dir <= 8; dir++)//was 1 to 8 +=2
                    {

                        x2 = x + deltaX[dir];
                        y2 = y + deltaY[dir];
                        if (x2 < 1)
                            x2 = 1;
                        if (y2 < 1)
                            y2 = 1;
                        if (x2 > xmax)
                            x2 = xmax;
                        if (y2 > ymax)
                            y2 = ymax;

                        // swap comment lines below for drainage area from D8 or Dinfinity

                        if (Math.IEEEremainder(dir, 2) != 0) {
                            if (elev[x2][y2] < elev[x][y])
                                area_depth[x2][y2] += area_depth[x][y]
                                        * ((elev[x][y] - elev[x2][y2]) / difftot);
                        } else {
                            if (elev[x2][y2] < elev[x][y])
                                area_depth[x2][y2] += area_depth[x][y]
                                        * (((elev[x][y] - elev[x2][y2]) / 1.414) / difftot);
                        }

                        //if (elev[x, y] - elev[x2, y2] == difftot) area_depth[x2, y2] += area_depth[x, y];
                    }

                }
                // finally zero the area depth...
                area_depth[x][y] = 0;
            }
        }
    }

    void soilerosion(double time) {

        /** creep rate is 10*-2 * slope per year, so inputs time jump in years*/
        /** very important differnece here is that slide_GS() is called only if
            BOTH cells are not -9999 therfore if both have grainsize then do additions.
            this is to stop the progressive spread of selected cells upslope */

        int x, y;
        double temp;

        for (x = 1; x <= xmax; x++) {
            for (y = 1; y <= ymax; y++) {
                tempcreep[x][y] = 0;
            }
        }

        for (x = 2; x < xmax; x++) {
            for (y = 2; y < ymax; y++) {
                if (elev[x][y] > bedrock[x][y]) {
                    if (elev[x][y - 1] < elev[x][y] && elev[x][y - 1] > 0) {
                        temp = ((elev[x][y] - elev[x][y - 1]) / DX) * SOIL_RATE * time / DX
                                * Math.pow(area[x][y] * DX * DX, 0.5);
                        if ((elev[x][y] - temp) < bedrock[x][y])
                            temp = elev[x][y] - bedrock[x][y];
                        if (temp < 0)
                            temp = 0;
                        tempcreep[x][y] -= temp;
                        tempcreep[x][y - 1] += temp;
                        if (index[x][y] != -9999) {
                            slide_GS(x, y, temp, x, y - 1);
                        }
                    }
                    if (elev[x + 1][y - 1] < elev[x][y] && elev[x + 1][y - 1] > 0) {
                        temp = ((elev[x][y] - elev[x + 1][y - 1]) / root) * SOIL_RATE * time / root
                                * Math.pow(area[x][y] * DX * DX, 0.5);
                        if ((elev[x][y] - temp) < bedrock[x][y])
                            temp = elev[x][y] - bedrock[x][y];
                        if (temp < 0)
                            temp = 0;
                        tempcreep[x][y] -= temp;
                        tempcreep[x + 1][y - 1] += temp;
                        if (index[x][y] != -9999) {
                            slide_GS(x, y, temp, x + 1, y - 1);
                        }

                    }
                    if (elev[x + 1][y] < elev[x][y] && elev[x + 1][y] > 0) {
                        temp = ((elev[x][y] - elev[x + 1][y]) / DX) * SOIL_RATE * time / DX
                                * Math.pow(area[x][y] * DX * DX, 0.5);
                        if ((elev[x][y] - temp) < bedrock[x][y])
                            temp = elev[x][y] - bedrock[x][y];
                        if (temp < 0)
                            temp = 0;
                        tempcreep[x][y] -= temp;
                        tempcreep[x + 1][y] += temp;
                        if (index[x][y] != -9999) {
                            slide_GS(x, y, temp, x + 1, y);
                        }
                    }
                    if (elev[x + 1][y + 1] < elev[x][y] && elev[x + 1][y + 1] > 0) {
                        temp = ((elev[x][y] - elev[x + 1][y + 1]) / root) * SOIL_RATE * time / root
                                * Math.pow(area[x][y] * DX * DX, 0.5);
                        if ((elev[x][y] - temp) < bedrock[x][y])
                            temp = elev[x][y] - bedrock[x][y];
                        if (temp < 0)
                            temp = 0;
                        tempcreep[x][y] -= temp;
                        tempcreep[x + 1][y + 1] += temp;
                        if (index[x][y] != -9999) {
                            slide_GS(x, y, temp, x + 1, y + 1);
                        }
                    }
                    if (elev[x][y + 1] < elev[x][y] && elev[x][y + 1] > 0) {
                        temp = ((elev[x][y] - elev[x][y + 1]) / DX) * SOIL_RATE * time / DX
                                * Math.pow(area[x][y] * DX * DX, 0.5);
                        if ((elev[x][y] - temp) < bedrock[x][y])
                            temp = elev[x][y] - bedrock[x][y];
                        if (temp < 0)
                            temp = 0;
                        tempcreep[x][y] -= temp;
                        tempcreep[x][y + 1] += temp;
                        if (index[x][y] != -9999) {
                            slide_GS(x, y, temp, x, y + 1);
                        }
                    }
                    if (elev[x - 1][y + 1] < elev[x][y] && elev[x - 1][y + 1] > 0) {
                        temp = ((elev[x][y] - elev[x - 1][y + 1]) / root) * SOIL_RATE * time / root
                                * Math.pow(area[x][y] * DX * DX, 0.5);
                        if ((elev[x][y] - temp) < bedrock[x][y])
                            temp = elev[x][y] - bedrock[x][y];
                        if (temp < 0)
                            temp = 0;
                        tempcreep[x][y] -= temp;
                        tempcreep[x - 1][y + 1] += temp;
                        if (index[x][y] != -9999) {
                            slide_GS(x, y, temp, x - 1, y + 1);
                        }
                    }
                    if (elev[x - 1][y] < elev[x][y] && elev[x - 1][y] > 0) {
                        temp = ((elev[x][y] - elev[x - 1][y]) / DX) * SOIL_RATE * time / DX
                                * Math.pow(area[x][y] * DX * DX, 0.5);
                        if ((elev[x][y] - temp) < bedrock[x][y])
                            temp = elev[x][y] - bedrock[x][y];
                        if (temp < 0)
                            temp = 0;
                        tempcreep[x][y] -= temp;
                        tempcreep[x - 1][y] += temp;
                        if (index[x][y] != -9999) {
                            slide_GS(x, y, temp, x - 1, y);
                        }
                    }
                    if (elev[x - 1][y - 1] < elev[x][y] && elev[x - 1][y - 1] > 0) {
                        temp = ((elev[x][y] - elev[x - 1][y - 1]) / root) * SOIL_RATE * time / root
                                * Math.pow(area[x][y] * DX * DX, 0.5);
                        if ((elev[x][y] - temp) < bedrock[x][y])
                            temp = elev[x][y] - bedrock[x][y];
                        if (temp < 0)
                            temp = 0;
                        tempcreep[x][y] -= temp;
                        tempcreep[x - 1][y - 1] += temp;
                        if (index[x][y] != -9999) {
                            slide_GS(x, y, temp, x - 1, y - 1);
                        }
                    }
                }
            }
        }

        for (x = 1; x <= xmax; x++) {
            for (y = 1; y <= ymax; y++) {
                elev[x][y] += tempcreep[x][y];
            }
        }

    }

    void qroute() {
        double l_time_factor = time_factor;
        if (l_time_factor > (courant_number * (DX / Math.sqrt(9.8 * (maxdepth)))))
            l_time_factor = courant_number * (DX / Math.sqrt(9.8 * (maxdepth)));
        final double local_time_factor = l_time_factor;

        Parallel.For(1, ymax + 1, new ParallelOpInt() {

            public void run(int y) {
                int inc = 1;
                while (down_scan[y][inc] > 0) {
                    int x = down_scan[y][inc];
                    inc++;

                    Tau[x][y] = 0;
                    if (elev[x][y] > -9999) // to stop moving water in to -9999's on elev
                    {
                        // routing in x direction
                        if ((water_depth[x][y] > 0 || water_depth[x - 1][y] > 0) && elev[x - 1][y] > -9999) // need to check water and not -9999 on elev
                        {
                            double hflow = Math.max(elev[x][y] + water_depth[x][y], elev[x - 1][y]
                                    + water_depth[x - 1][y])
                                    - Math.max(elev[x - 1][y], elev[x][y]);

                            if (hflow > hflow_threshold) {
                                double tempslope = (((elev[x - 1][y] + water_depth[x - 1][y])) - (elev[x][y] + water_depth[x][y]))
                                        / DX;

                                if (x == xmax)
                                    tempslope = edgeslope;
                                if (x <= 2)
                                    tempslope = 0 - edgeslope;

                                //double oldqx = qx[x, y];
                                qx[x][y] = ((qx[x][y] - (9.8 * hflow * local_time_factor * tempslope)) / (1 + 9.8
                                        * hflow
                                        * local_time_factor
                                        * (mannings * mannings)
                                        * Math.abs(qx[x][y]) / Math.pow(hflow, (10 / 3))));
                                //if (oldqx != 0) qx[x, y] = (oldqx + qx[x, y]) / 2;

                                // need to have these lines to stop too much water moving from one cellt o another - resulting in -ve discharges
                                // whihc causes a large instability to develop - only in steep catchments really
                                if (qx[x][y] > 0
                                        && (qx[x][y] / hflow) / Math.sqrt(9.8 * hflow) > froude_limit)
                                    qx[x][y] = hflow * (Math.sqrt(9.8 * hflow) * froude_limit);
                                if (qx[x][y] < 0
                                        && Math.abs(qx[x][y] / hflow) / Math.sqrt(9.8 * hflow) > froude_limit)
                                    qx[x][y] = 0 - (hflow * (Math.sqrt(9.8 * hflow) * froude_limit));

                                if (qx[x][y] > 0
                                        && (qx[x][y] * local_time_factor / DX) > (water_depth[x][y] / 4))
                                    qx[x][y] = ((water_depth[x][y] * DX) / 5) / local_time_factor;
                                if (qx[x][y] < 0
                                        && Math.abs(qx[x][y] * local_time_factor / DX) > (water_depth[x - 1][y] / 4))
                                    qx[x][y] = 0 - ((water_depth[x - 1][y] * DX) / 5) / local_time_factor;

                                if (isSuspended[1]) {

                                    if (qx[x][y] > 0)
                                        qxs[x][y] = qx[x][y] * (Vsusptot[x][y] / water_depth[x][y]);
                                    if (qx[x][y] < 0)
                                        qxs[x][y] = qx[x][y] * (Vsusptot[x - 1][y] / water_depth[x - 1][y]);

                                    if (qxs[x][y] > 0
                                            && qxs[x][y] * local_time_factor > (Vsusptot[x][y] * DX) / 4)
                                        qxs[x][y] = ((Vsusptot[x][y] * DX) / 5) / local_time_factor;
                                    if (qxs[x][y] < 0
                                            && Math.abs(qxs[x][y] * local_time_factor) > (Vsusptot[x - 1][y] * DX) / 4)
                                        qxs[x][y] = 0 - ((Vsusptot[x - 1][y] * DX) / 5) / local_time_factor;
                                }

                            } else {
                                qx[x][y] = 0;
                                qxs[x][y] = 0;
                            }
                        }

                        //routing in the y direction
                        if ((water_depth[x][y] > 0 || water_depth[x][y - 1] > 0) && elev[x][y - 1] > -9999) {
                            double hflow = Math.max(elev[x][y] + water_depth[x][y], elev[x][y - 1]
                                    + water_depth[x][y - 1])
                                    - Math.max(elev[x][y], elev[x][y - 1]);

                            if (hflow > hflow_threshold) {
                                double tempslope = (((elev[x][y - 1] + water_depth[x][y - 1])) - (elev[x][y] + water_depth[x][y]))
                                        / DX;
                                if (y == ymax)
                                    tempslope = edgeslope;
                                if (y <= 2)
                                    tempslope = 0 - edgeslope;

                                //double oldqy = qy[x, y];
                                qy[x][y] = ((qy[x][y] - (9.8 * hflow * local_time_factor * tempslope)) / (1 + 9.8
                                        * hflow
                                        * local_time_factor
                                        * (mannings * mannings)
                                        * Math.abs(qy[x][y]) / Math.pow(hflow, (10 / 3))));
                                //if (oldqy != 0) qy[x, y] = (oldqy + qy[x, y]) / 2;

                                // need to have these lines to stop too much water moving from one cellt o another - resulting in -ve discharges
                                // whihc causes a large instability to develop - only in steep catchments really
                                if (qy[x][y] > 0
                                        && (qy[x][y] / hflow) / Math.sqrt(9.8 * hflow) > froude_limit)
                                    qy[x][y] = hflow * (Math.sqrt(9.8 * hflow) * froude_limit);
                                if (qy[x][y] < 0
                                        && Math.abs(qy[x][y] / hflow) / Math.sqrt(9.8 * hflow) > froude_limit)
                                    qy[x][y] = 0 - (hflow * (Math.sqrt(9.8 * hflow) * froude_limit));

                                if (qy[x][y] > 0
                                        && (qy[x][y] * local_time_factor / DX) > (water_depth[x][y] / 4))
                                    qy[x][y] = ((water_depth[x][y] * DX) / 5) / local_time_factor;
                                if (qy[x][y] < 0
                                        && Math.abs(qy[x][y] * local_time_factor / DX) > (water_depth[x][y - 1] / 4))
                                    qy[x][y] = 0 - ((water_depth[x][y - 1] * DX) / 5) / local_time_factor;

                                if (isSuspended[1]) {

                                    if (qy[x][y] > 0)
                                        qys[x][y] = qy[x][y] * (Vsusptot[x][y] / water_depth[x][y]);
                                    if (qy[x][y] < 0)
                                        qys[x][y] = qy[x][y] * (Vsusptot[x][y - 1] / water_depth[x][y - 1]);

                                    if (qys[x][y] > 0
                                            && qys[x][y] * local_time_factor > (Vsusptot[x][y] * DX) / 4)
                                        qys[x][y] = ((Vsusptot[x][y] * DX) / 5) / local_time_factor;
                                    if (qys[x][y] < 0
                                            && Math.abs(qys[x][y] * local_time_factor) > (Vsusptot[x][y - 1] * DX) / 4)
                                        qys[x][y] = 0 - ((Vsusptot[x][y - 1] * DX) / 5) / local_time_factor;

                                }
                            } else {
                                qy[x][y] = 0;
                                qys[x][y] = 0;
                            }
                        }

                    }
                }
            }
        });
    }

    void depth_update() {
        double l_time_factor = time_factor;
        if (l_time_factor > (courant_number * (DX / Math.sqrt(9.8 * (maxdepth)))))
            l_time_factor = courant_number * (DX / Math.sqrt(9.8 * (maxdepth)));

        final double local_time_factor = l_time_factor;

        maxdepth = 0;

        Parallel.For(1, ymax + 1, new ParallelOpInt() {

            public void run(int y) {
                int inc = 1;

                double tempmaxdepth = 0;

                while (down_scan[y][inc] > 0) {
                    int x = down_scan[y][inc];
                    inc++;

                    // update q directions....
                    if (water_depth[x][y] > water_depth_erosion_threshold) {
                        for (int n = 1; n <= 8; n++)
                            vel_dir[x][y][n] = 0;
                        if (qy[x][y] > 0)
                            vel_dir[x][y][1] = qy[x][y] / water_depth[x][y];
                        if (qx[x + 1][y] < 0)
                            vel_dir[x][y][3] = (0 - qx[x + 1][y]) / water_depth[x][y];
                        if (qy[x][y + 1] < 0)
                            vel_dir[x][y][5] = (0 - qy[x][y + 1]) / water_depth[x][y];
                        if (qx[x][y] > 0)
                            vel_dir[x][y][7] = qx[x][y] / water_depth[x][y];
                    }

                    // update water depths

                    water_depth[x][y] += local_time_factor
                            * (qx[x + 1][y] - qx[x][y] + qy[x][y + 1] - qy[x][y]) / DX;

                    // now update SS concs
                    // carried out here, as the diagonals have already been removed.. 
                    if (isSuspended[1]) {
                        Vsusptot[x][y] += local_time_factor
                                * (qxs[x + 1][y] - qxs[x][y] + qys[x][y + 1] - qys[x][y]) / DX;
                    }

                    if (water_depth[x][y] > water_depth_erosion_threshold) {
                        // line to remove any water depth on nodata cells (that shouldnt get there!)
                        if (elev[x][y] == -9999)
                            water_depth[x][y] = 0;

                        // calc max flow depth for time step calc
                        if (water_depth[x][y] > tempmaxdepth)
                            tempmaxdepth = water_depth[x][y];

                    }

                }
                if (tempmaxdepth > maxdepth)
                    maxdepth = tempmaxdepth;
            }
        });

    }

    void scan_area() {
        Parallel.For(1, ymax + 1, new ParallelOpInt() {

            public void run(int y) {
                int inc = 1;

                for (int x = 1; x <= xmax; x++) {
                    // zero scan bit..
                    down_scan[y][x] = 0;
                    // and work out scanned area.
                    if (water_depth[x][y] > 0 || water_depth[x - 1][y] > 0 || water_depth[x - 1][y - 1] > 0
                            || water_depth[x - 1][y + 1] > 0 || water_depth[x + 1][y - 1] > 0
                            || water_depth[x + 1][y + 1] > 0 || water_depth[x][y - 1] > 0
                            || water_depth[x + 1][y] > 0 || water_depth[x][y + 1] > 0) {
                        down_scan[y][inc] = x;
                        inc++;
                    }
                }
            }
        });
    }

    void calc_hillshade() // <JOE 20051605- begin>
    {
        //Local variables
        int x, y;

        double slopemax;
        double slope;
        int slopetot;
        double local_Illumination;

        // Initialize Hillshade Paramaters
        double azimuth = 315 * (3.141592654 / 180); // Default of 315 degrees converted to radians
        double altitude = 45 * (3.141592654 / 180); // Default of 45 degrees converted to radians

        for (x = 1; x <= xmax; x++) {
            for (y = 1; y <= ymax; y++) {
                if (elev[x][y] != -9999 && elev[x][y] > 0) {
                    slopemax = 0.0;
                    slope = 0.0;
                    slopetot = 0;

                    // Do slope analysis and Aspect Calculation first
                    if (elev[x][y] > elev[x][y - 1] && elev[x][y - 1] != -9999) // North 0
                    {
                        slope = Math.pow((elev[x][y] - elev[x][y - 1]) / root, 1);
                        if (slope > slopemax) {
                            slopemax = slope;
                            slopetot++;
                            aspect[x][y] = 0 * (3.141592654 / 180);
                        }

                    }
                    if (elev[x][y] > elev[x + 1][y - 1] && elev[x + 1][y - 1] != -9999) // Northeast 45
                    {
                        slope = Math.pow((elev[x][y] - elev[x + 1][y - 1]) / DX, 1);
                        if (slope > slopemax) {
                            slopemax = slope;
                            slopetot++;
                            aspect[x][y] = 45 * (3.141592654 / 180);
                        }
                    }
                    if (elev[x][y] > elev[x + 1][y] && elev[x + 1][y] != -9999) // East 90
                    {
                        slope = Math.pow((elev[x][y] - elev[x + 1][y]) / root, 1);
                        if (slope > slopemax) {
                            slopemax = slope;
                            slopetot++;
                            aspect[x][y] = 90 * (3.141592654 / 180);
                        }
                    }
                    if (elev[x][y] > elev[x + 1][y + 1] && elev[x + 1][y + 1] != -9999) // SouthEast 135
                    {
                        slope = Math.pow((elev[x][y] - elev[x + 1][y + 1]) / root, 1);
                        if (slope > slopemax) {
                            slopemax = slope;
                            slopetot++;
                            aspect[x][y] = 135 * (3.141592654 / 180);
                        }

                    }
                    if (elev[x][y] > elev[x][y + 1] && elev[x][y + 1] != -9999) // South 180
                    {
                        slope = Math.pow((elev[x][y] - elev[x][y + 1]) / DX, 1);
                        if (slope > slopemax) {
                            slopemax = slope;
                            slopetot++;
                            aspect[x][y] = 180 * (3.141592654 / 180);
                        }
                    }
                    if (elev[x][y] > elev[x - 1][y + 1] && elev[x - 1][y + 1] != -9999) // SouthWest 225
                    {
                        slope = Math.pow((elev[x][y] - elev[x - 1][y + 1]) / root, 1);
                        if (slope > slopemax) {
                            slopemax = slope;
                            slopetot++;
                            aspect[x][y] = 225 * (3.141592654 / 180);
                        }
                    }
                    if (elev[x][y] > elev[x - 1][y] && elev[x - 1][y] != -9999) // West 270
                    {
                        slope = Math.pow((elev[x][y] - elev[x - 1][y]) / root, 1);
                        if (slope > slopemax) {
                            slopemax = slope;
                            slopetot++;
                            aspect[x][y] = 270;
                        }
                    }
                    if (elev[x][y] > elev[x - 1][y - 1] && elev[x - 1][y - 1] != -9999) // Northwest 315
                    {
                        slope = Math.pow((elev[x][y] - elev[x - 1][y - 1]) / DX, 1);
                        if (slope > slopemax) {
                            slopemax = slope;
                            slopetot++;
                            aspect[x][y] = 315 * (3.141592654 / 180);
                        }
                    }

                    if (slope > 0)
                        slopeAnalysis[x][y] = slopemax;// Tom's: (slope/slopetot); ?

                    // Convert slope to radians
                    slopeAnalysis[x][y] = Math.atan(slopeAnalysis[x][y]);

                    // Do Hillshade Calculation
                    local_Illumination = 255 * ((Math.cos(azimuth) * Math.sin(slopeAnalysis[x][y]) * Math
                            .cos(aspect[x][y] - azimuth)) + (Math.sin(altitude) * Math
                            .cos(slopeAnalysis[x][y])));

                    hillshade[x][y] = Math.abs(local_Illumination);
                }
            }
        }

    } // End calc_hillshade() <JOE 20051605- end>

    /*
     * FV using the following vars that are defined through the GUI 
     * 
     * double _slabDepth;    // slab_depth_box
     * int _depoProbability; // depo-prob-box: in percentage
     * int _downstreamOffset; // offset_box
     * double _initialDepth; // init_depth_box
     * double _shadowAngle; // shadow_angle_box
     * double fractionDune; // fraction_dune
     */

    void dune1(double time) // does dune things from top to bottom
    {
        int dune_recirculate = 0;
        int x, y, n, prob, counter1 = 1, ytemp, ytemp2, x2, y2;
        int t, checkup = _input.SHADOW_CHECK_DISTANCE_CELLS;
        int flag = 1;

        double maxslabdepth = _input.MAX_SLAB_THICKNESS_M;
        int dep_probability = _input.DEPOSITION_PROBABILITY_PERCENT;
        int downstream_offset = _input.DOWNSTREAM_OFFSET;
        double number_slabs_per_col = _input.SLABS_ADDED_PER_COL_PER_ITER;
        double angle = _input.SHADOW_LANDSLIP_ANGLE_DEG;

        double fractiondune = _input.FRACTION_OF_DUNE;
        double slabdepth = maxslabdepth;

        double factor = Math.tan((angle * (3.141592654 / 180))) * (DX / dune_mult);
        Random xr = new Random();

        for (x = 1; x <= xmax; x++) {
            for (y = 1; y <= ymax; y++) {
                // subtract eleev from sand so splitting into sand and elev for dune part...

                double sandsum = 0;
                double sand_diff = 0;
                for (x2 = 0; x2 < dune_mult; x2++) {
                    for (y2 = 0; y2 < dune_mult; y2++) {
                        // this next line is needed to remove all sand from the far LH and RH cells
                        if (x == xmax)
                            sand2[(x * dune_mult) - x2][(y * dune_mult) - y2] = 0;
                        if (x == 1)
                            sand2[(x * dune_mult) - x2][(y * dune_mult) - y2] = 0;

                        if (sand2[(x * dune_mult) - x2][(y * dune_mult) - y2] > 0) {
                            sandsum += sand2[(x * dune_mult) - x2][(y * dune_mult) - y2];
                        }

                    }
                }

                // then check and see if sand is less than sand2 - meaning there has been addition of dune sand to flucial sand
                if (sand[x][y] < (sandsum / (dune_mult * dune_mult))) {
                    sand_diff = (sandsum / (dune_mult * dune_mult)) - sand[x][y];

                    //if waterdepth >0 then sand > elev and grain (at a rate)
                    //then also reduce sand 2 by same amount transferred.

                    double tempsandvol = sand_diff;// amount to be removed from sand2...

                    // now has to reduce sand vol..
                    double sand2tot = 0;
                    int sand2num = 0;
                    for (x2 = 0; x2 < dune_mult; x2++) {
                        for (y2 = 0; y2 < dune_mult; y2++) {
                            if (sand2[(x * dune_mult) - x2][(y * dune_mult) - y2] > 0) {
                                sand2tot += sand2[(x * dune_mult) - x2][(y * dune_mult) - y2];
                                sand2num++;
                            }

                        }
                    }
                    for (x2 = 0; x2 < dune_mult; x2++) {
                        for (y2 = 0; y2 < dune_mult; y2++) {
                            if (sand2[(x * dune_mult) - x2][(y * dune_mult) - y2] > 0) {
                                sand2[(x * dune_mult) - x2][(y * dune_mult) - y2] -= (tempsandvol * dune_mult * dune_mult)
                                        * (sand2[(x * dune_mult) - x2][(y * dune_mult) - y2] / sand2tot);

                            }

                        }
                    }
                }

                elev[x][y] -= sand[x][y];

                // check to add to sand from grain (if no water)
                // if so then update sand and sand2 (and elev if subtracting from grain and
                // adding to sand

                if (water_depth[x][y] < water_depth_erosion_threshold) {
                    if (elev[x][y] - init_elevs[x][y] > 0) {
                        double tempslabdepth = elev[x][y] - init_elevs[x][y];
                        //if (tempslabdepth < slabdepth) tempslabdepth = 0;
                        if (tempslabdepth > slabdepth)
                            tempslabdepth = slabdepth;
                        sand[x][y] += tempslabdepth;
                        elev[x][y] -= tempslabdepth;

                        // now also update sand2 values... averaged across cell.
                        for (x2 = 0; x2 < dune_mult; x2++) {
                            for (y2 = 0; y2 < dune_mult; y2++) {
                                sand2[(x * dune_mult) - x2][(y * dune_mult) - y2] += tempslabdepth;
                            }
                        }

                    }

                }

                //update elev2 from elev
                for (x2 = 0; x2 < dune_mult; x2++) {
                    for (y2 = 0; y2 < dune_mult; y2++) {
                        elev2[(x * dune_mult) - x2][(y * dune_mult) - y2] = elev[x][y];
                    }
                }

            }
        }

        /////////////////////////////////////////////////////////////
        // adding sand part
        /////////////////////////////////////////////////////////////

        //if (Math.IEEERemainder((int)(cycle / 60000), 2) == 0)
        //if (Math.IEEERemainder((int)(cycle), 2) == 0)
        {

            for (x = 1; x < (xmax) * dune_mult * fractiondune; x++) {
                //if(Math.IEEERemainder(Math.Abs(x/50)-1,3)==0)sand2[x, 1] = initial_sand_depth;
                sand2[rand(50, (xmax * dune_mult), xr)][1] += number_slabs_per_col;

            }
        }

        /////////////////////////////////////////////////////////////
        // end of adding sand code ///
        /////////////////////////////////////////////////////////////

        // check everywhere for sand landslides
        for (x = 1; x <= xmax * dune_mult; x++) {
            for (y = 1; y <= ymax * dune_mult; y++) {
                slide_4(x, y);
            }
        }

        for (n = 1; n <= fractiondune * (xmax * dune_mult) * (ymax * dune_mult); n++) {

            // creating random x and y co-ords to check...
            x = rand(1, (xmax * dune_mult) + 1, xr);
            y = rand(1, (ymax * dune_mult) + 1, xr);
            prob = 100;
            counter1 = 0;
            flag = 1;

            //if (sand2[x, y] >= slabdepth)
            if (sand2[x][y] > 0) {
                // to see if should be entrained or not...
                for (t = 1; t <= checkup; t++) {
                    ytemp = y - t;
                    if (dune_recirculate == 1 && ytemp < 1)
                        ytemp = (ymax * dune_mult) + ytemp;
                    if (dune_recirculate == 0 && ytemp < 1)
                        ytemp = 1;
                    if (((sand2[x][ytemp] + elev2[x][ytemp]) - (sand2[x][y] + elev2[x][y])) > (factor * (t))) {
                        flag = 0;
                        t = checkup;
                    }

                }

                // now having decided it can be eroded, now see if it can be moved
                if (flag == 1) {
                    // while random number greater than prob then move on one if poss..
                    while (prob > dep_probability) {
                        counter1++; // shift down one cell...
                        prob = rand(1, 100, xr); // new random number

                        //// try moving left and right??
                        //if (xr.Next(1, 100) > 70) x++;
                        //if (xr.Next(1, 100) > 70) x--;
                        //if (x < 1) x = (xmax * dune_mult);
                        //if (x > (xmax * dune_mult)) x = 1;

                        // now adding in the downsrtream offset if needed done by ensuring prob is 100
                        if (counter1 < downstream_offset)
                            prob = 100;

                        // seeing if in shadow or not...
                        for (t = 1; t < checkup; t++) {
                            ytemp = (y + counter1) - t;
                            ytemp2 = (y + counter1);
                            if (ytemp < 1)
                                ytemp = (ymax * dune_mult) + ytemp;
                            if (dune_recirculate == 1) {
                                if (ytemp > (ymax * dune_mult))
                                    ytemp = 0 + (ytemp - (ymax * dune_mult));
                                if (ytemp2 > (ymax * dune_mult))
                                    ytemp2 = 0 + (ytemp2 - (ymax * dune_mult));
                            }
                            if (((sand2[x][ytemp] + elev2[x][ytemp]) - (sand2[x][ytemp2] + elev2[x][ytemp2])) > (factor * t)) {
                                t = checkup;
                                prob = 0;
                            }
                        }
                        //if (elev[x, ytemp - 1] - elev[x, y] > factor) break;
                    }

                    double tempmax = sand2[x][y];
                    if (tempmax < 0)
                        tempmax = 0;

                    // now erode sand if there is enough there in the sand layer
                    if (slabdepth < tempmax)
                        tempmax = slabdepth;
                    if (tempmax < 0)
                        tempmax = 0;
                    sand2[x][y] -= tempmax;
                    // do landslides for just that cell and ones around.
                    slide_4(x, y);

                    ytemp = y + counter1;

                    if (dune_recirculate == 1) {
                        if (ytemp > (ymax * dune_mult)) {
                            ytemp = ytemp - (ymax * dune_mult);
                            sand_out += (tempmax * (DX / dune_mult) * (DX / dune_mult));
                        }
                    }

                    // now deposit sand

                    if (ytemp <= ymax * dune_mult) {
                        sand2[x][ytemp] += tempmax;

                        // do landslides for just that cell and ones around.
                        slide_4(x, ytemp);
                    }
                    if (ytemp > (ymax * dune_mult))
                        sand_out += (tempmax * (DX / dune_mult) * (DX / dune_mult));

                }
            }

        }

        // now do landslides everywhere 5 times... just to make sure..
        for (t = 1; t <= 5; t++) {
            for (x = 1; x <= xmax * dune_mult; x++) {
                for (y = 1; y <= ymax * dune_mult; y++) {
                    if (sand2[x][y] > 0)
                        slide_4(x, y);
                }
            }
        }

        // sand = mean sand2

        for (x = 1; x <= (xmax); x++) {
            for (y = 1; y <= (ymax); y++) {

                double sandsum = 0;
                for (x2 = 0; x2 < dune_mult; x2++) {
                    for (y2 = 0; y2 < dune_mult; y2++) {
                        // this next line is needed to remove all sand from the far LH and RH cells
                        if (x == xmax)
                            sand2[(x * dune_mult) - x2][(y * dune_mult) - y2] = 0;
                        if (x == 1)
                            sand2[(x * dune_mult) - x2][(y * dune_mult) - y2] = 0;

                        if (sand2[(x * dune_mult) - x2][(y * dune_mult) - y2] > 0) {
                            sandsum += sand2[(x * dune_mult) - x2][(y * dune_mult) - y2];
                        }

                    }
                }

                sand[x][y] = sandsum / (dune_mult * dune_mult);

                elev[x][y] += sand[x][y];

            }

        }
    }

    double mean_ws_elev(int x, int y) {
        double elevtot = 0;
        int counter = 0;

        for (int dir = 1; dir <= 8; dir++) {
            int x2, y2;
            x2 = x + deltaX[dir];
            y2 = y + deltaY[dir];

            if (water_depth[x2][y2] > water_depth_erosion_threshold) {
                elevtot += water_depth[x2][y2] + elev[x2][y2];
                counter++;
            }

        }
        if (counter > 0) {
            elevtot /= counter;
            return elevtot;
        }

        else
            return 0;
    }

    private void zero_values() {
        int x, y, z, n;

        for (y = 0; y <= ymax; y++) {
            for (x = 0; x <= xmax; x++) {
                Vel[x][y] = 0;
                area[x][y] = 0;
                elev[x][y] = 0;
                bedrock[x][y] = 0;
                init_elevs[x][y] = elev[x][y];
                water_depth[x][y] = 0;
                index[x][y] = -9999;
                inputpointsarray[x][y] = false;
                veg[x][y][0] = 0;// elevation
                veg[x][y][1] = 0; // density
                veg[x][y][2] = 0; // jw density
                veg[x][y][3] = 0; // height

                edge[x][y] = 0;
                edge2[x][y] = 0;

                sand[x][y] = 0;

                qx[x][y] = 0;
                qy[x][y] = 0;

                qxs[x][y] = 0;
                qys[x][y] = 0;

                for (n = 0; n <= 8; n++)
                    vel_dir[x][y][n] = 0;

                Vsusptot[x][y] = 0;

            }
        }

        for (x = 1; x < ((xmax * ymax) / LIMIT); x++) {
            for (y = 0; y <= G_MAX; y++) {
                grain[x][y] = 0;
            }
            for (z = 0; z <= 9; z++) {
                for (y = 0; y <= G_MAX - 2; y++) {

                    strata[x][z][y] = 0;
                }
            }
            catchment_input_x_coord[x] = 0;
            catchment_input_y_coord[x] = 0;
        }
    }

    void lateral3() {

        final Double[][] edge_temp, edge_temp2, water_depth2;
        final int[][] upscale, upscale_edge;

        edge_temp = new Double[xmax + 1][ymax + 1];
        edge_temp2 = new Double[xmax + 1][ymax + 1];
        water_depth2 = new Double[xmax + 1][ymax + 1];
        upscale = new int[(xmax + 1) * 2][(ymax + 1) * 2];
        upscale_edge = new int[(xmax + 1) * 2][(ymax + 1) * 2];

        final double mft = 0.01;// water_depth_erosion_threshold;//MIN_Q;// vel_dir threshold

        // first make water depth2 equal to water depth then remove single wet cells frmo water depth2 that have an undue influence..
        for (int y = 2; y < ymax; y++) {

            int inc = 1;
            while (down_scan[y][inc] > 0) {
                int x = down_scan[y][inc];

                edge_temp[x][y] = 0.0;
                if (x == 1)
                    x++;
                if (x == xmax)
                    x--;
                inc++;

                if (Vel[x][y] > mft) {
                    water_depth2[x][y] = Vel[x][y];
                    int tempcounter = 0;
                    for (int dir = 1; dir <= 8; dir++) {
                        int x2, y2;
                        x2 = x + deltaX[dir];
                        y2 = y + deltaY[dir];
                        if (Vel[x2][y2] < mft)
                            tempcounter++;
                    }
                    if (tempcounter > 6)
                        water_depth2[x][y] = 0.0;
                }
            }
        }

        // first determine which cells are at the edge of the channel

        for (int y = 2; y < ymax; y++) {

            for (int x = 2; x < xmax; x++) {

                edge[x][y] = -9999;

                if (water_depth2[x][y] < mft) {
                    // if water depth < threshold then if its next to a wet cell then its an edge cell
                    if (water_depth2[x][y - 1] > mft || water_depth2[x - 1][y] > mft
                            || water_depth2[x + 1][y] > mft || water_depth2[x][y + 1] > mft) {
                        edge[x][y] = 0;
                    }

                    // unless its a dry cell surrounded by wet...
                    if (water_depth2[x][y - 1] > mft && water_depth2[x - 1][y] > mft
                            && water_depth2[x + 1][y] > mft && water_depth2[x][y + 1] > mft) {
                        edge[x][y] = -9999;
                        edge2[x][y] = -9999;
                    }

                    // then update upscaled grid..
                    upscale[(x * 2)][(y * 2)] = 0; // if dry
                    upscale[(x * 2)][(y * 2) - 1] = 0;
                    upscale[(x * 2) - 1][(y * 2)] = 0;
                    upscale[(x * 2) - 1][(y * 2) - 1] = 0;

                }

                // update upscaled grid with wet cells (if wet)
                if (water_depth2[x][y] >= mft) {
                    upscale[(x * 2)][(y * 2)] = 1; // if wet
                    upscale[(x * 2)][(y * 2) - 1] = 1;
                    upscale[(x * 2) - 1][(y * 2)] = 1;
                    upscale[(x * 2) - 1][(y * 2) - 1] = 1;
                }

            }
        }

        // now determine edge cells on the new grid..

        for (int y = 2; y < ymax * 2; y++) {
            for (int x = 2; x < xmax * 2; x++) {
                upscale_edge[x][y] = 0;
                if (upscale[x][y] == 0) {
                    if (upscale[x][y - 1] == 1 || upscale[x - 1][y] == 1 || upscale[x + 1][y] == 1
                            || upscale[x][y + 1] == 1) {
                        upscale[x][y] = 2;
                    }

                }

            }
        }

        // now tall up inside and outside on upscaled grid

        for (int y = 2; y < ymax * 2; y++) {
            for (int x = 2; x < xmax * 2; x++) {
                if (upscale[x][y] == 2) {
                    int wetcells = 0;
                    int drycells = 0;
                    int water = 0;
                    int edge_cell_counter = 1;

                    // sum up dry cells and edge cells - 
                    // now manhattan neighbors
                    for (int dir = 1; dir <= 7; dir += 2) {
                        int x2, y2;
                        x2 = x + deltaX[dir];
                        y2 = y + deltaY[dir];

                        if (upscale[x2][y2] == 1)
                            wetcells += 1;
                        if (upscale[x2][y2] == 0)
                            drycells += 1;
                        if (upscale[x2][y2] == 2)
                            edge_cell_counter += 1;

                    }

                    if (edge_cell_counter > 3)
                        drycells += edge_cell_counter - 2;
                    //						

                    water = wetcells - drycells;
                    upscale_edge[x][y] = water;

                }

            }
        }

        // now update normal edge array..

        for (int x = 1; x <= xmax; x++) {
            for (int y = 1; y <= ymax; y++) {
                if (edge[x][y] == 0) {
                    edge[x][y] = (double) (upscale_edge[(x * 2)][(y * 2)]
                            + upscale_edge[(x * 2)][(y * 2) - 1] + upscale_edge[(x * 2) - 1][(y * 2)] + upscale_edge[(x * 2) - 1][(y * 2) - 1]);
                    if (edge[x][y] > 2)
                        edge[x][y] = 2; // important line to stop too great inside bends...
                    if (edge[x][y] < -2)
                        edge[x][y] = -2;

                }
            }
        }

        //then apply a smoothing filter over the top of this. here its done X number of times -

        final double smoothing_times = _input.EDGE_SMOOTHING_PASSAGE_COUNT; // double.Parse(avge_smoothbox.Text);
        final double downstream_shift = _input.CELLS_TO_SHIFT_DOWNSTREAM; // double.Parse(downstreamshiftbox.Text);

        for (int n = 1; n <= smoothing_times + downstream_shift; n++) {
            final int nn = n;

            Parallel.For(2, ymax, new ParallelOpInt() {

                @Override
                public void run(int y) {
                    int inc = 1;
                    while (down_scan[y][inc] > 0) {
                        int x = down_scan[y][inc];

                        edge_temp[x][y] = 0.0;
                        if (x == 1)
                            x++;
                        if (x == xmax)
                            x--;
                        inc++;

                        if (edge[x][y] > -9999) {
                            double mean = 0;
                            double num = 0;
                            double water_flag = 0;

                            // add in cell itself..
                            mean += edge[x][y];
                            num++;

                            for (int dir = 1; dir <= 8; dir++) {
                                int x2, y2;
                                x2 = x + deltaX[dir];
                                y2 = y + deltaY[dir];

                                if (water_depth2[x2][y2] > mft)
                                    water_flag++;

                                if (nn > smoothing_times && edge[x2][y2] > -9999
                                        && water_depth2[x2][y2] < mft
                                        && mean_ws_elev(x2, y2) > mean_ws_elev(x, y)) {

                                    //now to mean manhattan neighbours - only if they share a wet diagonal neighbour
                                    if ((Math.abs(deltaX[dir]) + Math.abs(deltaY[dir])) != 2) {
                                        if (deltaX[dir] == 1
                                                && deltaY[dir] == 0
                                                && (water_depth2[x + 1][y - 1] > mft || water_depth2[x + 1][y + 1] > mft)) {
                                            mean += (edge[x + deltaX[dir]][y + deltaY[dir]]);
                                            num++;
                                        }
                                        if (deltaX[dir] == 0
                                                && deltaY[dir] == 1
                                                && (water_depth2[x + 1][y + 1] > mft || water_depth2[x - 1][y + 1] > mft)) {
                                            mean += (edge[x + deltaX[dir]][y + deltaY[dir]]);
                                            num++;
                                        }
                                        if (deltaX[dir] == -1
                                                && deltaY[dir] == 0
                                                && (water_depth2[x - 1][y - 1] > mft || water_depth2[x - 1][y + 1] > mft)) {
                                            mean += (edge[x + deltaX[dir]][y + deltaY[dir]]);
                                            num++;
                                        }
                                        if (deltaX[dir] == 0
                                                && deltaY[dir] == -1
                                                && (water_depth2[x - 1][y - 1] > mft || water_depth2[x + 1][y - 1] > mft)) {
                                            mean += (edge[x + deltaX[dir]][y + deltaY[dir]]);
                                            num++;
                                        }
                                        //										mean+=(edge[x2,y2]);
                                        //										num++;
                                    }
                                    //now non manahttan neighbours, with concected by a dry cell checked..
                                    else {
                                        if (deltaX[dir] == -1
                                                && deltaY[dir] == -1
                                                && (water_depth2[x][y - 1] < mft || water_depth2[x - 1][y] < mft)) {
                                            mean += (edge[x + deltaX[dir]][y + deltaY[dir]]);
                                            num++;
                                        }
                                        if (deltaX[dir] == 1
                                                && deltaY[dir] == -1
                                                && (water_depth2[x][y - 1] < mft || water_depth2[x + 1][y] < mft)) {
                                            mean += (edge[x + deltaX[dir]][y + deltaY[dir]]);
                                            num++;
                                        }
                                        if (deltaX[dir] == 1
                                                && deltaY[dir] == 1
                                                && (water_depth2[x + 1][y] < mft || water_depth2[x][y + 1] < mft)) {
                                            mean += (edge[x + deltaX[dir]][y + deltaY[dir]]);
                                            num++;
                                        }
                                        if (deltaX[dir] == -1
                                                && deltaY[dir] == 1
                                                && (water_depth2[x][y + 1] < mft || water_depth2[x - 1][y] < mft)) {
                                            mean += (edge[x + deltaX[dir]][y + deltaY[dir]]);
                                            num++;
                                        }
                                    }

                                }

                                else if (nn <= smoothing_times && edge[x2][y2] > -9999
                                        && water_depth2[x2][y2] < mft) {

                                    //now to mean manhattan neighbours - only if they share a wet diagonal neighbour
                                    if ((Math.abs(deltaX[dir]) + Math.abs(deltaY[dir])) != 2) {
                                        if (deltaX[dir] == 1
                                                && deltaY[dir] == 0
                                                && (water_depth2[x + 1][y - 1] > mft || water_depth2[x + 1][y + 1] > mft)) {
                                            mean += (edge[x + deltaX[dir]][y + deltaY[dir]]);
                                            num++;
                                        }
                                        if (deltaX[dir] == 0
                                                && deltaY[dir] == 1
                                                && (water_depth2[x + 1][y + 1] > mft || water_depth2[x - 1][y + 1] > mft)) {
                                            mean += (edge[x + deltaX[dir]][y + deltaY[dir]]);
                                            num++;
                                        }
                                        if (deltaX[dir] == -1
                                                && deltaY[dir] == 0
                                                && (water_depth2[x - 1][y - 1] > mft || water_depth2[x - 1][y + 1] > mft)) {
                                            mean += (edge[x + deltaX[dir]][y + deltaY[dir]]);
                                            num++;
                                        }
                                        if (deltaX[dir] == 0
                                                && deltaY[dir] == -1
                                                && (water_depth2[x - 1][y - 1] > mft || water_depth2[x + 1][y - 1] > mft)) {
                                            mean += (edge[x + deltaX[dir]][y + deltaY[dir]]);
                                            num++;
                                        }
                                        //										mean+=(edge[x2,y2]);
                                        //										num++;
                                    }
                                    //now non manahttan neighbours, with concected by a dry cell checked..
                                    else {
                                        if (deltaX[dir] == -1
                                                && deltaY[dir] == -1
                                                && (water_depth2[x][y - 1] < mft || water_depth2[x - 1][y] < mft)) {
                                            mean += (edge[x + deltaX[dir]][y + deltaY[dir]]);
                                            num++;
                                        }
                                        if (deltaX[dir] == 1
                                                && deltaY[dir] == -1
                                                && (water_depth2[x][y - 1] < mft || water_depth2[x + 1][y] < mft)) {
                                            mean += (edge[x + deltaX[dir]][y + deltaY[dir]]);
                                            num++;
                                        }
                                        if (deltaX[dir] == 1
                                                && deltaY[dir] == 1
                                                && (water_depth2[x + 1][y] < mft || water_depth2[x][y + 1] < mft)) {
                                            mean += (edge[x + deltaX[dir]][y + deltaY[dir]]);
                                            num++;
                                        }
                                        if (deltaX[dir] == -1
                                                && deltaY[dir] == 1
                                                && (water_depth2[x][y + 1] < mft || water_depth2[x - 1][y] < mft)) {
                                            mean += (edge[x + deltaX[dir]][y + deltaY[dir]]);
                                            num++;
                                        }
                                    }

                                }

                            }

                            if (mean != 0)
                                edge_temp[x][y] = mean / num;

                            // removes too many cells - islands etc..

                            //if(num>5&&edge[x,y]>0)edge_temp[x,y]=0;
                            //if(num+water_flag>7&&edge[x,y]>0)edge_temp[x,y]=0;

                            //remove edge effects
                            if (x < 3 || x > (xmax - 3))
                                edge_temp[x][y] = 0.0;
                            if (y < 3 || y > (ymax - 3))
                                edge_temp[x][y] = 0.0;

                        }
                    }
                }
            });

            for (int y = 2; y < ymax; y++) {
                int inc = 1;
                while (down_scan[y][inc] > 0) {
                    int x = down_scan[y][inc];
                    if (x == 1)
                        x++;
                    if (x == xmax)
                        x--;
                    inc++;
                    if (edge[x][y] > -9999) {
                        edge[x][y] = edge_temp[x][y];

                    }

                }
            }
        }

        // trial line to remove too high inside bends,,
        for (int x = 1; x <= xmax; x++) {
            for (int y = 1; y <= ymax; y++) {
                edge2[x][y] = 0;
                if (edge[x][y] > -9999) {
                    if (edge[x][y] > 0)
                        edge[x][y] = 0;
                    //if (edge[x, y] < -0.25) edge[x, y] = -0.25;
                    edge[x][y] = 0 - edge[x][y];
                    edge[x][y] = 1 / ((2.131 * Math.pow(edge[x][y], -1.0794)) * DX);
                    if (edge[x][y] > (1 / (DX * 3)))
                        edge[x][y] = 1 / (DX * 3);
                    //edge[x, y] = 1 / edge[x, y];

                    edge2[x][y] = Math.abs(edge[x][y]);

                }
            }
        }

        //// now smooth across the channel..

        double tempdiff = 0;
        double counter = 0;
        do {
            counter++;
            Parallel.For(2, ymax, new ParallelOpInt() {

                @Override
                public void run(int y) {
                    int inc = 1;
                    while (down_scan[y][inc] > 0) {
                        int x = down_scan[y][inc];

                        edge_temp[x][y] = 0.0;
                        edge_temp2[x][y] = 0.0;
                        if (x == 1)
                            x++;
                        if (x == xmax)
                            x--;
                        inc++;
                        if (water_depth2[x][y] > mft && edge[x][y] == -9999)
                            edge[x][y] = 0;

                        if (edge[x][y] > -9999 && water_depth2[x][y] > mft) {
                            double mean = 0, mean2 = 0;
                            int num = 0, num2 = 0;
                            mean2 += edge2[x][y];
                            num2++;
                            for (int dir = 1; dir <= 8; dir += 2) {
                                int x2, y2;
                                x2 = x + deltaX[dir];
                                y2 = y + deltaY[dir];

                                if (water_depth2[x2][y2] > mft && edge[x2][y2] == -9999)
                                    edge[x2][y2] = 0;

                                if (edge[x2][y2] > -9999) {

                                    mean += (edge[x2][y2]);
                                    num++;

                                    mean2 += (edge2[x2][y2]);
                                    num2++;
                                }

                            }
                            edge_temp[x][y] = mean / num;
                            edge_temp2[x][y] = mean2 / num2;
                        }
                    }
                }
            });

            tempdiff = 0;
            for (int y = 2; y < ymax; y++) {
                int inc = 1;
                while (down_scan[y][inc] > 0) {
                    int x = down_scan[y][inc];
                    if (x == 1)
                        x++;
                    if (x == xmax)
                        x--;
                    inc++;
                    if (edge[x][y] > -9999 && water_depth2[x][y] > mft) {
                        if (Math.abs(edge2[x][y] - edge_temp2[x][y]) > tempdiff)
                            tempdiff = Math.abs(edge2[x][y] - edge_temp2[x][y]);
                        edge[x][y] = edge_temp[x][y];
                        edge2[x][y] = edge_temp2[x][y];
                    }

                }
            }
        } while (tempdiff > lateral_cross_channel_smoothing); //this makes it loop until the averaging across the stream stabilises
        // so that the difference between the old and new values are < 0.0001
        //tempStatusPanel.Text = Convert.ToString(counter);

    }

    void init_route(int flag, double reach_input_amount, double catchment_input_amount) {
        int x, y, inc;

        double w = water_depth_erosion_threshold;

        /*******************************/

        for (x = 1; x <= xmax; x++) {
            for (y = 1; y <= ymax; y++) {
                cross_scan[x][y] = 0;
                down_scan[y][x] = 0;
                //if(water_depth[x,y]==-9999)water_depth[x,y]=0;
            }
        }

        for (x = 1; x <= xmax; x++) {
            inc = 1;
            for (y = 1; y <= ymax; y++) {
                if (water_depth[x][y] > 0 || water_depth[x - 1][y] > 0 || water_depth[x + 1][y] > 0
                        || water_depth[x][y - 1] > 0 || water_depth[x][y + 1] > 0) {
                    cross_scan[x][inc] = y;
                    inc++;
                }
                //discharge[x,y]=0;			
            }
        }

        for (y = 1; y <= ymax; y++) {
            inc = 1;
            for (x = xmax; x >= 1; x--) {
                if (water_depth[x][y] > 0 || water_depth[x - 1][y] > 0 || water_depth[x + 1][y] > 0
                        || water_depth[x][y - 1] > 0 || water_depth[x][y + 1] > 0) {
                    down_scan[y][inc] = x;
                    inc++;
                }
                //if(input_type_flag==1)discharge[x,y]=0;
            }
        }

        // TODO monitor.info
        //		this.InfoStatusPanel.Text="Running";		// MJ 14/01/05

    }

    void slide_3() {
        int x, y, inc;
        double wet_factor;
        double factor = Math.tan((failureangle * (3.141592654 / 180))) * DX;
        double diff = 0;

        for (y = 2; y < ymax; y++) {
            inc = 1;
            while (down_scan[y][inc] > 0) {
                x = down_scan[y][inc];
                if (x == xmax)
                    x = xmax - 1;
                if (x == 1)
                    x = 2;

                inc++;
                /** check to see if under water **/
                wet_factor = factor;
                //if(water_depth[x,y]>0.01)wet_factor=factor/2;
                if (elev[x][y] <= (bedrock[x][y] + active))
                    wet_factor = 10000;

                /** chexk landslides in channel slowly */

                if (((elev[x][y] - elev[x + 1][y + 1]) / 1.41) > wet_factor && elev[x + 1][y + 1] > 0) {
                    diff = ((elev[x][y] - elev[x + 1][y + 1]) / 1.41) - wet_factor;
                    if ((elev[x][y] - diff) < (bedrock[x][y] + active))
                        diff = (elev[x][y] - (bedrock[x][y] + active));
                    elev[x][y] -= diff;
                    elev[x + 1][y + 1] += diff;
                    slide_GS(x, y, diff, x + 1, y + 1);
                }
                if ((elev[x][y] - elev[x][y + 1]) > wet_factor && elev[x][y + 1] > 0) {
                    diff = (elev[x][y] - elev[x][y + 1]) - wet_factor;
                    if ((elev[x][y] - diff) < (bedrock[x][y] + active))
                        diff = (elev[x][y] - (bedrock[x][y] + active));
                    elev[x][y] -= diff;
                    elev[x][y + 1] += diff;
                    slide_GS(x, y, diff, x, y + 1);
                }
                if (((elev[x][y] - elev[x - 1][y + 1]) / 1.41) > wet_factor && elev[x - 1][y + 1] > 0) {
                    diff = ((elev[x][y] - elev[x - 1][y + 1]) / 1.41) - wet_factor;
                    if ((elev[x][y] - diff) < (bedrock[x][y] + active))
                        diff = (elev[x][y] - (bedrock[x][y] + active));
                    elev[x][y] -= diff;
                    elev[x - 1][y + 1] += diff;
                    slide_GS(x, y, diff, x - 1, y + 1);
                }
                if ((elev[x][y] - elev[x - 1][y]) > wet_factor && elev[x - 1][y] > 0) {
                    diff = (elev[x][y] - elev[x - 1][y]) - wet_factor;
                    if ((elev[x][y] - diff) < (bedrock[x][y] + active))
                        diff = (elev[x][y] - (bedrock[x][y] + active));
                    elev[x][y] -= diff;
                    elev[x - 1][y] += diff;
                    slide_GS(x, y, diff, x - 1, y);
                }

                if (((elev[x][y] - elev[x - 1][y - 1]) / 1.41) > wet_factor && elev[x - 1][y - 1] > 0) {
                    diff = ((elev[x][y] - elev[x - 1][y - 1]) / 1.41) - wet_factor;
                    if ((elev[x][y] - diff) < (bedrock[x][y] + active))
                        diff = (elev[x][y] - (bedrock[x][y] + active));
                    elev[x][y] -= diff;
                    elev[x - 1][y - 1] += diff;
                    slide_GS(x, y, diff, x - 1, y - 1);
                }
                if ((elev[x][y] - elev[x][y - 1]) > wet_factor && elev[x][y - 1] > 0) {
                    diff = (elev[x][y] - elev[x][y - 1]) - wet_factor;
                    if ((elev[x][y] - diff) < (bedrock[x][y] + active))
                        diff = (elev[x][y] - (bedrock[x][y] + active));
                    elev[x][y] -= diff;
                    elev[x][y - 1] += diff;
                    slide_GS(x, y, diff, x, y - 1);
                }
                if (((elev[x][y] - elev[x + 1][y - 1]) / 1.41) > wet_factor && elev[x + 1][y - 1] > 0) {
                    diff = ((elev[x][y] - elev[x + 1][y - 1]) / 1.41) - wet_factor;
                    if ((elev[x][y] - diff) < (bedrock[x][y] + active))
                        diff = (elev[x][y] - (bedrock[x][y] + active));
                    elev[x][y] -= diff;
                    elev[x + 1][y - 1] += diff;
                    slide_GS(x, y, diff, x + 1, y - 1);
                }

                if ((elev[x][y] - elev[x + 1][y]) > wet_factor && elev[x + 1][y] > 0) {
                    diff = (elev[x][y] - elev[x + 1][y]) - wet_factor;
                    if ((elev[x][y] - diff) < (bedrock[x][y] + active))
                        diff = (elev[x][y] - (bedrock[x][y] + active));
                    elev[x][y] -= diff;
                    elev[x + 1][y] += diff;
                    slide_GS(x, y, diff, x + 1, y);
                }

            }
        }

    }

    void slide_4(int x, int y) // landslides from sand dunes...
    {
        double wet_factor;
        double factor = Math.tan((failureangle * (3.141592654 / 180))) * (DX / dune_mult);
        double diff = 0;

        wet_factor = factor;

        if ((((elev2[x][y] + sand2[x][y]) - (elev2[x + 1][y + 1] + sand2[x + 1][y + 1])) / 1.41) > wet_factor
                && (elev2[x + 1][y + 1] + sand2[x + 1][y + 1]) > 0) {
            diff = (((elev2[x][y] + sand2[x][y]) - (elev2[x + 1][y + 1] + sand2[x + 1][y + 1])) / 1.41)
                    - wet_factor;
            if (diff > sand2[x][y])
                diff = sand2[x][y];
            //if (((elev2[x, y] + sand2[x, y]) - diff) < (bedrock[x, y] + active)) diff = ((elev2[x, y] + sand2[x, y]) - (bedrock[x, y] + active));
            sand2[x][y] -= diff;
            sand2[x + 1][y + 1] += diff;
        }
        if (((elev2[x][y] + sand2[x][y]) - (elev2[x][y + 1] + sand2[x][y + 1])) > wet_factor
                && (elev2[x][y + 1] + sand2[x][y + 1]) > 0) {
            diff = ((elev2[x][y] + sand2[x][y]) - (elev2[x][y + 1] + sand2[x][y + 1])) - wet_factor;
            if (diff > sand2[x][y])
                diff = sand2[x][y];
            //if (((elev2[x, y] + sand2[x, y]) - diff) < (bedrock[x, y] + active)) diff = ((elev2[x, y] + sand2[x, y]) - (bedrock[x, y] + active));
            sand2[x][y] -= diff;
            sand2[x][y + 1] += diff;
        }
        if ((((elev2[x][y] + sand2[x][y]) - (elev2[x - 1][y + 1] + sand2[x - 1][y + 1])) / 1.41) > wet_factor
                && (elev2[x - 1][y + 1] + sand2[x - 1][y + 1]) > 0) {
            diff = (((elev2[x][y] + sand2[x][y]) - (elev2[x - 1][y + 1] + sand2[x - 1][y + 1])) / 1.41)
                    - wet_factor;
            if (diff > sand2[x][y])
                diff = sand2[x][y];
            // if (((elev2[x, y] + sand2[x, y]) - diff) < (bedrock[x, y] + active)) diff = ((elev2[x, y] + sand2[x, y]) - (bedrock[x, y] + active));
            sand2[x][y] -= diff;
            sand2[x - 1][y + 1] += diff;
        }
        if (((elev2[x][y] + sand2[x][y]) - (elev2[x - 1][y] + sand2[x - 1][y])) > wet_factor
                && (elev2[x - 1][y] + sand2[x - 1][y]) > 0) {
            diff = ((elev2[x][y] + sand2[x][y]) - (elev2[x - 1][y] + sand2[x - 1][y])) - wet_factor;
            if (diff > sand2[x][y])
                diff = sand2[x][y];
            //if (((elev2[x, y] + sand2[x, y]) - diff) < (bedrock[x, y] + active)) diff = ((elev2[x, y] + sand2[x, y]) - (bedrock[x, y] + active));
            sand2[x][y] -= diff;
            sand2[x - 1][y] += diff;
        }

        if ((((elev2[x][y] + sand2[x][y]) - (elev2[x - 1][y - 1] + sand2[x - 1][y - 1])) / 1.41) > wet_factor
                && (elev2[x - 1][y - 1] + sand2[x - 1][y - 1]) > 0) {
            diff = (((elev2[x][y] + sand2[x][y]) - (elev2[x - 1][y - 1] + sand2[x - 1][y - 1])) / 1.41)
                    - wet_factor;
            if (diff > sand2[x][y])
                diff = sand2[x][y];
            //if (((elev2[x, y] + sand2[x, y]) - diff) < (bedrock[x, y] + active)) diff = ((elev2[x, y] + sand2[x, y]) - (bedrock[x, y] + active));
            sand2[x][y] -= diff;
            sand2[x - 1][y - 1] += diff;

        }
        if (((elev2[x][y] + sand2[x][y]) - (elev2[x][y - 1] + sand2[x][y - 1])) > wet_factor
                && (elev2[x][y - 1] + sand2[x][y - 1]) > 0) {
            diff = ((elev2[x][y] + sand2[x][y]) - (elev2[x][y - 1] + sand2[x][y - 1])) - wet_factor;
            if (diff > sand2[x][y])
                diff = sand2[x][y];
            //if (((elev2[x, y] + sand2[x, y]) - diff) < (bedrock[x, y] + active)) diff = ((elev2[x, y] + sand2[x, y]) - (bedrock[x, y] + active));
            sand2[x][y] -= diff;
            sand2[x][y - 1] += diff;

        }
        if ((((elev2[x][y] + sand2[x][y]) - (elev2[x + 1][y - 1] + sand2[x + 1][y - 1])) / 1.41) > wet_factor
                && (elev2[x + 1][y - 1] + sand2[x + 1][y - 1]) > 0) {
            diff = (((elev2[x][y] + sand2[x][y]) - (elev2[x + 1][y - 1] + sand2[x + 1][y - 1])) / 1.41)
                    - wet_factor;
            if (diff > sand2[x][y])
                diff = sand2[x][y];
            //if (((elev2[x, y] + sand2[x, y]) - diff) < (bedrock[x, y] + active)) diff = ((elev2[x, y] + sand2[x, y]) - (bedrock[x, y] + active));
            sand2[x][y] -= diff;
            sand2[x + 1][y - 1] += diff;

        }

        if (((elev2[x][y] + sand2[x][y]) - (elev2[x + 1][y] + sand2[x + 1][y])) > wet_factor
                && (elev2[x + 1][y] + sand2[x + 1][y]) > 0) {
            diff = ((elev2[x][y] + sand2[x][y]) - (elev2[x + 1][y] + sand2[x + 1][y])) - wet_factor;
            if (diff > sand2[x][y])
                diff = sand2[x][y];
            //if (((elev2[x, y] + sand2[x, y]) - diff) < (bedrock[x, y] + active)) diff = ((elev2[x, y] + sand2[x, y]) - (bedrock[x, y] + active));
            sand2[x][y] -= diff;
            sand2[x + 1][y] += diff;

        }
    }

    void slide_5() {
        int x, y, inc = 0;
        double wet_factor;
        double factor = Math.tan((failureangle * (3.141592654 / 180))) * DX;
        if (_input.COMPUTE_LANDSLIDES)
            factor = DX * ((-265000 * j_mean) + 1.38);
        double diff = 0;
        double total = 0;

        if (_input.COMPUTE_DUNES) {
            for (x = 1; x <= xmax; x++) {
                for (y = 1; y <= ymax; y++) {
                    elev[x][y] -= sand[x][y];
                }
            }
        }

        do {
            total = 0;
            inc++;
            for (y = 2; y < ymax; y++) {
                for (x = 2; x < xmax; x++) {

                    wet_factor = factor;
                    //if(water_depth[x,y]>0.01)wet_factor=factor/2;
                    if (elev[x][y] <= (bedrock[x][y] + active))
                        wet_factor = 10 * DX;

                    /** chexk landslides in channel slowly */

                    if (((elev[x][y] - elev[x + 1][y + 1]) / 1.41) > wet_factor && elev[x + 1][y + 1] >= 0) {
                        diff = ((elev[x][y] - elev[x + 1][y + 1]) / 1.41) - wet_factor;
                        if ((elev[x][y] - diff) < (bedrock[x][y] + active))
                            diff = (elev[x][y] - (bedrock[x][y] + active));
                        elev[x][y] -= diff;
                        elev[x + 1][y + 1] += diff;
                        total += diff;
                    }
                    if ((elev[x][y] - elev[x][y + 1]) > wet_factor && elev[x][y + 1] >= 0) {
                        diff = (elev[x][y] - elev[x][y + 1]) - wet_factor;
                        if ((elev[x][y] - diff) < (bedrock[x][y] + active))
                            diff = (elev[x][y] - (bedrock[x][y] + active));
                        elev[x][y] -= diff;
                        elev[x][y + 1] += diff;
                        total += diff;
                    }
                    if (((elev[x][y] - elev[x - 1][y + 1]) / 1.41) > wet_factor && elev[x - 1][y + 1] >= 0) {
                        diff = ((elev[x][y] - elev[x - 1][y + 1]) / 1.41) - wet_factor;
                        if ((elev[x][y] - diff) < (bedrock[x][y] + active))
                            diff = (elev[x][y] - (bedrock[x][y] + active));
                        elev[x][y] -= diff;
                        elev[x - 1][y + 1] += diff;
                        total += diff;
                    }
                    if ((elev[x][y] - elev[x - 1][y]) > wet_factor && elev[x - 1][y] >= 0) {
                        diff = (elev[x][y] - elev[x - 1][y]) - wet_factor;
                        if ((elev[x][y] - diff) < (bedrock[x][y] + active))
                            diff = (elev[x][y] - (bedrock[x][y] + active));
                        elev[x][y] -= diff;
                        elev[x - 1][y] += diff;
                        total += diff;
                    }

                    if (((elev[x][y] - elev[x - 1][y - 1]) / 1.41) > wet_factor && elev[x - 1][y - 1] >= 0) {
                        diff = ((elev[x][y] - elev[x - 1][y - 1]) / 1.41) - wet_factor;
                        if ((elev[x][y] - diff) < (bedrock[x][y] + active))
                            diff = (elev[x][y] - (bedrock[x][y] + active));
                        elev[x][y] -= diff;
                        elev[x - 1][y - 1] += diff;
                        total += diff;
                    }
                    if ((elev[x][y] - elev[x][y - 1]) > wet_factor && elev[x][y - 1] >= 0) {
                        diff = (elev[x][y] - elev[x][y - 1]) - wet_factor;
                        if ((elev[x][y] - diff) < (bedrock[x][y] + active))
                            diff = (elev[x][y] - (bedrock[x][y] + active));
                        elev[x][y] -= diff;
                        elev[x][y - 1] += diff;
                        total += diff;
                    }
                    if (((elev[x][y] - elev[x + 1][y - 1]) / 1.41) > wet_factor && elev[x + 1][y - 1] >= 0) {
                        diff = ((elev[x][y] - elev[x + 1][y - 1]) / 1.41) - wet_factor;
                        if ((elev[x][y] - diff) < (bedrock[x][y] + active))
                            diff = (elev[x][y] - (bedrock[x][y] + active));
                        elev[x][y] -= diff;
                        elev[x + 1][y - 1] += diff;
                        total += diff;
                    }

                    if ((elev[x][y] - elev[x + 1][y]) > wet_factor && elev[x + 1][y] >= 0) {
                        diff = (elev[x][y] - elev[x + 1][y]) - wet_factor;
                        if ((elev[x][y] - diff) < (bedrock[x][y] + active))
                            diff = (elev[x][y] - (bedrock[x][y] + active));
                        elev[x][y] -= diff;
                        elev[x + 1][y] += diff;
                        total += diff;
                    }

                }
            }
        } while (total > 0 && inc < 200);

        if (_input.COMPUTE_DUNES) {
            for (x = 1; x <= xmax; x++) {
                for (y = 1; y <= ymax; y++) {
                    elev[x][y] += sand[x][y];
                }
            }
        }

    }

    void siberia(double time) {
        int x, y;
        double temp;
        double SibQ = 0;
        double SibQsf = 0;

        for (x = 1; x <= xmax; x++) {
            for (y = 1; y <= ymax; y++) {
                tempcreep[x][y] = 0;
            }
        }

        for (x = 2; x < xmax; x++) {
            for (y = 2; y < ymax; y++) {
                if (elev[x][y] > bedrock[x][y]) {
                    SibQ = Beta3 * Math.pow(area[x][y] * DX * DX, m3);
                    SibQsf = Beta1 * Math.pow((SibQ / DX), m1) * Math.pow(max_bed_slope2(x, y), n1) * time;
                    double max_slope = max_bed_slope2(x, y);
                    if (max_slope == 0)
                        max_slope = 10; // catch line to stop erosion if there is no slope
                    //if (max_slope > 0) max_slope *= 0.99;

                    if ((elev[x][y] - elev[x][y - 1]) / DX >= max_slope && elev[x][y - 1] > 0) {
                        temp = SibQsf;
                        if ((elev[x][y] - temp) < bedrock[x][y])
                            temp = elev[x][y] - bedrock[x][y];
                        if (temp < 0)
                            temp = 0;
                        tempcreep[x][y] -= temp;
                        tempcreep[x][y - 1] += temp;
                        if (index[x][y] != -9999) {
                            slide_GS(x, y, temp, x, y - 1);
                        }
                    }
                    if ((elev[x][y] - elev[x + 1][y - 1]) / root >= max_slope
                            && (elev[x + 1][y - 1] > 0 || x == xmax)) {
                        temp = SibQsf;
                        if ((elev[x][y] - temp) < bedrock[x][y])
                            temp = elev[x][y] - bedrock[x][y];
                        if (temp < 0)
                            temp = 0;
                        tempcreep[x][y] -= temp;
                        tempcreep[x + 1][y - 1] += temp;
                        if (index[x][y] != -9999) {
                            slide_GS(x, y, temp, x + 1, y - 1);
                        }

                    }
                    if ((elev[x][y] - elev[x + 1][y]) / DX >= max_slope && (elev[x + 1][y] > 0 || x == xmax)) {
                        temp = SibQsf;
                        if ((elev[x][y] - temp) < bedrock[x][y])
                            temp = elev[x][y] - bedrock[x][y];
                        if (temp < 0)
                            temp = 0;
                        tempcreep[x][y] -= temp;
                        tempcreep[x + 1][y] += temp;
                        if (index[x][y] != -9999) {
                            slide_GS(x, y, temp, x + 1, y);
                        }
                    }
                    if ((elev[x][y] - elev[x + 1][y + 1]) / root >= max_slope
                            && (elev[x + 1][y + 1] > 0 || x == xmax)) {
                        temp = SibQsf;
                        if ((elev[x][y] - temp) < bedrock[x][y])
                            temp = elev[x][y] - bedrock[x][y];
                        if (temp < 0)
                            temp = 0;
                        tempcreep[x][y] -= temp;
                        tempcreep[x + 1][y + 1] += temp;
                        if (index[x][y] != -9999) {
                            slide_GS(x, y, temp, x + 1, y + 1);
                        }
                    }
                    if ((elev[x][y] - elev[x][y + 1]) / DX >= max_slope && elev[x][y + 1] > 0) {
                        temp = SibQsf;
                        if ((elev[x][y] - temp) < bedrock[x][y])
                            temp = elev[x][y] - bedrock[x][y];
                        if (temp < 0)
                            temp = 0;
                        tempcreep[x][y] -= temp;
                        tempcreep[x][y + 1] += temp;
                        if (index[x][y] != -9999) {
                            slide_GS(x, y, temp, x, y + 1);
                        }
                    }
                    if ((elev[x][y] - elev[x - 1][y + 1]) / root >= max_slope && elev[x - 1][y + 1] > 0) {
                        temp = SibQsf;
                        if ((elev[x][y] - temp) < bedrock[x][y])
                            temp = elev[x][y] - bedrock[x][y];
                        if (temp < 0)
                            temp = 0;
                        tempcreep[x][y] -= temp;
                        tempcreep[x - 1][y + 1] += temp;
                        if (index[x][y] != -9999) {
                            slide_GS(x, y, temp, x - 1, y + 1);
                        }
                    }
                    if ((elev[x][y] - elev[x - 1][y]) / DX >= max_slope && elev[x - 1][y] > 0) {
                        temp = SibQsf;
                        if ((elev[x][y] - temp) < bedrock[x][y])
                            temp = elev[x][y] - bedrock[x][y];
                        if (temp < 0)
                            temp = 0;
                        tempcreep[x][y] -= temp;
                        tempcreep[x - 1][y] += temp;
                        if (index[x][y] != -9999) {
                            slide_GS(x, y, temp, x - 1, y);
                        }
                    }
                    if ((elev[x][y] - elev[x - 1][y - 1]) / root >= max_slope && elev[x - 1][y - 1] > 0) {
                        temp = SibQsf;
                        if ((elev[x][y] - temp) < bedrock[x][y])
                            temp = elev[x][y] - bedrock[x][y];
                        if (temp < 0)
                            temp = 0;
                        tempcreep[x][y] -= temp;
                        tempcreep[x - 1][y - 1] += temp;
                        if (index[x][y] != -9999) {
                            slide_GS(x, y, temp, x - 1, y - 1);
                        }
                    }

                }
            }
        }

        for (x = 2; x < xmax; x++) {
            for (y = 2; y < ymax; y++) {
                elev[x][y] += tempcreep[x][y];
            }
        }

    }

    public int rand(int min, int max, Random rx) {
        double d = rx.nextDouble();
        return min + (int) ((double) (max - min) * d);
    }

}
