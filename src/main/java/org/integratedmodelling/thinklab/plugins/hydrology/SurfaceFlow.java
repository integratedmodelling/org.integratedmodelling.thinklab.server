package org.integratedmodelling.thinklab.plugins.hydrology;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IExpression;
import org.integratedmodelling.thinklab.api.knowledge.IProperty;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.modelling.IModel;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.api.project.IProject;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.geospace.agents.Watershed;
import org.integratedmodelling.thinklab.geospace.gis.operators.BaseGISAccessor;
import org.integratedmodelling.thinklab.interfaces.annotations.Function;
import org.jgrasstools.gears.libs.modules.FlowNode;

@Function(id = "hydrology.waterflow", parameterNames = {}, returnTypes = { NS.SUBJECT_ACCESSOR })
public class SurfaceFlow extends BaseGISAccessor implements IExpression {

    boolean _inited = false;

    @Override
    public ISubject process(ISubject subject, ISubject context, IProperty property, IMonitor monitor)
            throws ThinklabException {

        if (!(subject instanceof Watershed)) {
            throw new ThinklabValidationException("water flow only runs within a Watershed subject");
        }

        if (!_inited) {
            classifyInputs();
            _inited = true;
        }

        Watershed watershed = (Watershed) subject;
        Set<FlowNode> done = new HashSet<FlowNode>();

        FlowNode previous = null;
        for (FlowNode fn : watershed.getStreamHeads()) {

            while (fn != null) {

                if (!done.contains(fn)) {

                    /*
                     * if node was not seen, accumulate inflows and outflows from all gathered
                     * processes. 
                     */

                    done.add(fn);
                }

                /*
                 * compute runoff to next cell downwards and move there
                 */

                previous = fn;
                fn = fn.goDownstream();
            }
        }

        return null;
    }

    private void classifyInputs() {
        // TODO Auto-generated method stub

    }

    @Override
    public void notifyExpectedOutput(IObservable observable, IObserver observer, String name) {
        // TODO Auto-generated method stub

    }

    @Override
    public void notifyModel(IModel model) {
        // TODO classify all available inputs as inflow and outflow; establish whether
        // they're instantaneous or integrated over their time scale; prepare accessors
        // for integration of all.
    }

    @Override
    public Object eval(Map<String, Object> parameters, IConcept... context) throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setProjectContext(IProject project) {
        // TODO Auto-generated method stub

    }

}
