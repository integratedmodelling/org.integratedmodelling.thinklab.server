package org.integratedmodelling.thinklab.plugins.hydrology.lisflood;

public class ArraySort {

    Double[] _a2;

    void sortMatched(Double[] a1, Double[] a2) {
        quickSort(a1, 0, a1.length - 1);
    }

    private void swap(Double[] a, int lft, int rt) {
        Double temp;
        temp = a[lft];
        a[lft] = a[rt];
        a[rt] = temp;

        // swap the matched array
        temp = _a2[lft];
        _a2[lft] = _a2[rt];
        _a2[rt] = temp;
    }

    public int pivot(int firstpl, int lastpl) {
        if (firstpl >= lastpl)
            return -1;
        else
            return firstpl;
    }

    private void quickSort(Double[] a, int first, int last) {
        int left, right;
        int pivindex = pivot(first, last);
        if (pivindex >= 0) {
            left = pivindex + 1;
            right = last;
            do {
                while (a[left] < a[pivindex] && left <= right)
                    left++;
                while (a[right] > a[pivindex])
                    right--;
                if (right > left)
                    swap(a, left, right);
            } while (left < right);
            swap(a, pivindex, right);
            quickSort(a, first, right - 1);
            quickSort(a, right + 1, last);
        }
    }

}
