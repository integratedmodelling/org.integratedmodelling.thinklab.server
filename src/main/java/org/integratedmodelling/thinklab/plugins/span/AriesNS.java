package org.integratedmodelling.thinklab.plugins.span;

import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.modelling.INamespace;

public class AriesNS {

    private static final String ARIES_NAMESPACE = "aries.es";

    public static IConcept TRAIL_PRESENCE;
    public static IConcept DIFFICULTY_OF_ACCESS;

    static final String TRAIL_PRESENCE_CONCEPT = "im.infrastructure:TrailPresence";
    static final String DIFFICULTY_OF_ACCESS_CONCEPT = "im.recreation:DifficultyOfAccess";
    
    public static void synchronize() {
        // TODO move concepts here
        INamespace namespace = Thinklab.get().getNamespace(ARIES_NAMESPACE);

        if (namespace == null) {
            throw new ThinklabRuntimeException("namespace " + ARIES_NAMESPACE
                    + " is not available: ARIES functionalities cannot be activated");
        }
        
        TRAIL_PRESENCE = Thinklab.get().getConcept(TRAIL_PRESENCE_CONCEPT);
        DIFFICULTY_OF_ACCESS = Thinklab.get().getConcept(DIFFICULTY_OF_ACCESS_CONCEPT);
        
    }

}
