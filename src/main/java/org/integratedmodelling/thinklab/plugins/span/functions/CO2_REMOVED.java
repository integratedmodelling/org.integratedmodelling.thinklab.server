package org.integratedmodelling.thinklab.plugins.span.functions;

import java.util.Map;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IExpression;
import org.integratedmodelling.thinklab.api.project.IProject;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.interfaces.annotations.Function;
import org.integratedmodelling.thinklab.plugins.span.SPANAccessor;

@Function(id = "span.co2-removed", parameterNames = {
        "span-version",
        "transition-threshold",
        "sink-threshold",
        "source-threshold",
        "use-threshold" }, returnTypes = { NS.SUBJECT_ACCESSOR })
public class CO2_REMOVED implements IExpression {

    IProject _project;

    @Override
    public Object eval(Map<String, Object> parameters, IConcept... context) throws ThinklabException {

        /*
         * these will be overridden by observations
         */
        double transitionThreshold = parameters.containsKey("transition-threshold") ? Double
                .parseDouble((String) parameters.get("transition-threshold").toString()) : 1.0;
        double sourceThreshold = parameters.containsKey("source-threshold") ? Double
                .parseDouble((String) parameters.get("source-threshold").toString()) : 0.0;
        double sinkThreshold = parameters.containsKey("sink-threshold") ? Double
                .parseDouble((String) parameters.get("sink-threshold").toString()) : 0.0;
        double useThreshold = parameters.containsKey("use-threshold") ? Double
                .parseDouble((String) parameters.get("use-threshold").toString()) : 0.0;
        double downscalingFactor = parameters.containsKey("downscaling-factor") ? Double
                .parseDouble((String) parameters.get("downscaling-factor").toString()) : 1.0;

        boolean animate = parameters.containsKey("animate") ? (Boolean) parameters.get("animate")
                : Boolean.FALSE;

        String version = parameters.containsKey("span-version") ? parameters.get("span-version").toString()
                : "1.0";

        return new SPANAccessor(SPANAccessor.CO2_REMOVED, sourceThreshold, sinkThreshold, useThreshold,
                transitionThreshold, downscalingFactor, animate, version);
    }

    @Override
    public void setProjectContext(IProject project) {
        _project = project;
    }

}
