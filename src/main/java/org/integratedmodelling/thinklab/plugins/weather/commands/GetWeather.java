package org.integratedmodelling.thinklab.plugins.weather.commands;

import java.util.HashMap;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.runtime.ISession;
import org.integratedmodelling.thinklab.command.Command;
import org.integratedmodelling.thinklab.interfaces.annotations.ThinklabCommand;
import org.integratedmodelling.thinklab.interfaces.commands.ICommandHandler;
import org.integratedmodelling.thinklab.rest.interfaces.IStatelessService;

@ThinklabCommand(
        name = "weather:get-weather",
        argumentNames = "station,from,to,step",
        argumentTypes = "thinklab:Text,thinklab:LongInteger,thinklab:LongInteger,thinklab:LongInteger",
        argumentDescriptions = "station id,date from,date to,step",
        optionDescriptions = "scenario(s)",
        optionLongNames = "scenario",
        optionNames = "s",
        optionArgumentLabels = "scenario",
        optionTypes = "thinklab:Text")
public class GetWeather implements ICommandHandler, IStatelessService {

    @Override
    public Object execute(Command command, ISession session) throws ThinklabException {

        HashMap<String, Object> ret = new HashMap<String, Object>();

        return ret;
    }
}
