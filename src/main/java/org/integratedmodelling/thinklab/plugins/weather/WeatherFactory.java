package org.integratedmodelling.thinklab.plugins.weather;

import java.io.File;
import java.util.Iterator;
import java.util.List;

import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.knowledge.ISemanticObject;
import org.integratedmodelling.thinklab.api.knowledge.kbox.IKbox;
import org.integratedmodelling.thinklab.api.knowledge.kbox.ISpatialKbox;
import org.integratedmodelling.thinklab.api.modelling.IScale;
import org.integratedmodelling.thinklab.common.utils.FixedReader;
import org.integratedmodelling.thinklab.geospace.literals.ShapeValue;
import org.integratedmodelling.thinklab.query.Queries;

import com.ibm.icu.util.Calendar;

public class WeatherFactory {

    public static final String    baseURL = "http://www.integratedmodelling.org/ghcn";
    private static WeatherFactory _this;

    private WeatherFactory() {

    }

    public static WeatherFactory get() {
        if (_this == null) {
            _this = new WeatherFactory();
        }
        return _this;
    }

    /*
     * ensures the passed kbox contains the full index to the weather stations.
     * CAUTION: very long-running operation - about 3h on a powerful desktop. Better
     * done on a real server and used through REST.
     */
    public void checkDatabase() throws ThinklabException {

        /*
         * Format of GHCN station file (ghcnd-stations.txt)
         * ------------------------------
           Variable   Columns   Type
           ------------------------------
           ID            1-11   Character
           LATITUDE     13-20   Real
           LONGITUDE    22-30   Real
           ELEVATION    32-37   Real
           STATE        39-40   Character
           NAME         42-71   Character
           GSNFLAG      73-75   Character
           HCNFLAG      77-79   Character
           WMOID        81-85   Character
           ------------------------------
         */
        IKbox kbox = Thinklab.get().requireKbox(Thinklab.DEFAULT_KBOX);

        List<ISemanticObject<?>> ret = kbox.query(Queries.select(Thinklab.c(Weather.NS.WEATHER_STATION)));
        if (ret.size() > 100)
            return;

        File fInv = new File(Thinklab.get().getDataPath() + File.separator + "data" + File.separator
                + "ghcnd-inventory.txt");
        File fSta = new File(Thinklab.get().getDataPath() + File.separator + "data" + File.separator
                + "ghcnd-stations.txt");

        List<FixedReader> stations = null;
        List<FixedReader> inventor = null;

        if (fInv.exists() && fSta.exists()) {
            stations = FixedReader.parse(fSta, new int[] { 0, 12, 21, 31, 38, 41, 72, 76, 80 });
            inventor = FixedReader.parse(fInv, new int[] { 0, 12, 21, 31, 35, 40, 45 });
        } else {
            stations = FixedReader.parse(baseURL + "/ghcnd-stations.txt", new int[] {
                    0,
                    12,
                    21,
                    31,
                    38,
                    41,
                    72,
                    76,
                    80 });
            inventor = FixedReader.parse(baseURL + "/ghcnd-inventory.txt", new int[] {
                    0,
                    12,
                    21,
                    31,
                    35,
                    40,
                    45 });
        }

        // reader for the inventory file; the following depends on it being ordered exactly like
        // the stations file.
        Iterator<FixedReader> inventory = inventor.iterator();
        FixedReader invLine = inventory.next();

        int i = 0;
        for (FixedReader fr : stations) {

            String id = fr.nextString().trim();
            double lat = fr.nextDouble();
            double lon = fr.nextDouble();
            double alt = fr.nextDouble();
            String state = fr.nextString().trim();
            String name = fr.nextString().trim();

            /*
             * make a new object and store it
             */
            WeatherStation ws = new WeatherStation(baseURL, id, lat, lon, alt, state, name);

            try {
                /*
                 * read the available data series from the inventory.
                 */
                while (invLine.nextString().trim().equals(id)) {

                    invLine.nextDouble();
                    invLine.nextDouble();
                    String varId = invLine.nextString();
                    int firstYear = invLine.nextInt();
                    int lastYear = invLine.nextInt();

                    ws._provided.put(varId, new Pair<Integer, Integer>(firstYear, lastYear));

                    if (inventory.hasNext())
                        invLine = inventory.next();
                }
            } catch (Exception e) {
                System.out.println(e);
            }

            /*
             * we peeked already, so reset counter for next inventory line if we have more
             */
            if (inventory.hasNext()) {
                invLine.reset();
            }

            Thinklab.get().logger().info("Storing " + i++ + "th station " + ws);
            kbox.store(ws);
        }

    }

    /**
     * Return the closest weather station that provides all the passed variables. The 
     * station will contain the distance in km. from the requested point. 
     * 
     * @param lat
     * @param lon
     * @param variables
     * @return
     * @throws ThinklabException 
     */
    public WeatherStation getClosestStationProviding(double lat, double lon, double maxDistanceInKm,
            String... variables) throws ThinklabException {

        IKbox kbox = Thinklab.get().requireKbox("weather");

        if (kbox instanceof ISpatialKbox) {

            ShapeValue location = new ShapeValue("EPSG:4326 POINT(" + lon + " " + lat + ")");

            List<ISemanticObject<?>> rets = ((ISpatialKbox) kbox)
                    .closestTo(Thinklab.c(Weather.NS.WEATHER_STATION), Thinklab.p(Weather.NS.HAS_LOCATION), location, maxDistanceInKm);

            for (ISemanticObject<?> o : rets) {

                WeatherStation ws = (WeatherStation) o.demote();
                boolean ok = true;
                for (String v : variables) {
                    if (!ws.provides(v)) {
                        ok = false;
                        break;
                    }
                }

                if (ok) {
                    ws.setRequestedPoint(location);
                    return ws;
                }
            }
        }
        return null;
    }

    /**
     * Return the weather station closest to the passed point, within the given range.
     * 
     * @param lat
     * @param lon
     * @param distanceInKm
     * @return
     * @throws ThinklabException
     */
    public WeatherStation getClosestWeatherStation(double lat, double lon, double distanceInKm)
            throws ThinklabException {

        WeatherStation ret = null;
        IKbox kbox = Thinklab.get().requireKbox("weather");

        if (kbox instanceof ISpatialKbox) {

            ShapeValue location = new ShapeValue("EPSG:4326 POINT(" + lon + " " + lat + ")");

            List<ISemanticObject<?>> rets = ((ISpatialKbox) kbox)
                    .closestTo(Thinklab.c(Weather.NS.WEATHER_STATION), Thinklab.p(Weather.NS.HAS_LOCATION), location, distanceInKm);

            if (rets.size() > 0) {
                ret = (WeatherStation) rets.get(0).demote();
                ret.setRequestedPoint(location);
            }
        }
        return ret;
    }

    /**
     * Return the weather at the passed scale. That should include time and space.
     * 
     * @param scale
     * @return
     * @throws ThinklabException
     */
    public static Weather getWeather(IScale scale) throws ThinklabException {
        return null;
    }

    /**
     * Return the weather at the given lat/lon. If no weather station can be found to supply
     * data or train the weather generator in the 100km range, return null. For weather to be
     * successfully generated if necessary, the station needs to provide at least min/max temperature
     * as well as precipitation.
     * 
     * @param lat
     * @param lon
     * @return
     * @throws ThinklabException
     */
    public static Weather getWeather(double lat, double lon) throws ThinklabException {

        WeatherStation station = WeatherFactory
                .get()
                .getClosestStationProviding(lat, lon, 100, Weather.MIN_TEMPERATURE_C, Weather.MAX_TEMPERATURE_C, Weather.PRECIPITATION_MM);

        if (station == null)
            return null;

        return new Weather(station);
    }

    /**
     * Compute day length based on day of the year and latitude
     * @param time
     * @param latitude
     * @return
     */
    public static double dayLength(Calendar time, double latitude) {

        final int[] DAYS = { 15, 45, 74, 105, 135, 166, 196, 227, 258, 288, 319, 349 };

        int month = time.get(java.util.Calendar.MONTH);

        double dayl = DAYS[month] - 80.;
        if (dayl < 0.0) {
            dayl = 285. + DAYS[month];
        }

        double decr = 23.45 * Math.sin(dayl / 365. * 6.2832) * 0.017453;
        double alat = latitude * 0.017453;
        double csh = (-0.02908 - Math.sin(decr) * Math.sin(alat)) / (Math.cos(decr) * Math.cos(alat));

        return 24.0 * (1.570796 - Math.atan(csh / Math.sqrt(1. - csh * csh))) / Math.PI;
    }

}
