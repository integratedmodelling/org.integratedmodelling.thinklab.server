package org.integratedmodelling.thinklab.plugins.weather;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.math3.analysis.UnivariateFunction;
import org.apache.commons.math3.analysis.interpolation.SplineInterpolator;
import org.apache.commons.math3.analysis.interpolation.UnivariateInterpolator;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabIOException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.lang.IRemoteSerializable;
import org.integratedmodelling.thinklab.api.annotations.Concept;
import org.integratedmodelling.thinklab.api.annotations.Property;
import org.integratedmodelling.thinklab.api.lang.IMetadataHolder;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.metadata.IMetadata;
import org.integratedmodelling.thinklab.common.utils.FixedReader;
import org.integratedmodelling.thinklab.geospace.GeoNS;
import org.integratedmodelling.thinklab.geospace.literals.ShapeValue;
import org.integratedmodelling.thinklab.interfaces.IStorageMetadataProvider;
import org.integratedmodelling.thinklab.modelling.lang.Metadata;
import org.joda.time.DateMidnight;
import org.joda.time.Days;
import org.joda.time.LocalDate;

/**
 * A weather station with all the data known for it. The closest station to any point on
 * the globe can be retrieved using the weather factory. Each Weather has one (and possibly
 * in the future more than one) station associated.
 * 
 * @author Ferd
 *
 */
@Concept(Weather.NS.WEATHER_STATION)
public class WeatherStation implements IMetadataHolder, IStorageMetadataProvider, IRemoteSerializable {

    public static class Data {
        String                     variable;
        int                        startYear;
        int                        endYear;

        /*
         * Data are one pair <year, daily data> per year, in increasing year
         * order. Nodata are NaNs. Array may be 365 or 366.
         */
        HashMap<Integer, double[]> data = new HashMap<Integer, double[]>();

        public String dump() {
            String s = "";
            for (int y : data.keySet()) {
                s += (s.isEmpty() ? "" : ",") + y;
            }
            return variable + "(" + s + ")";
        }

    }

    HashMap<String, Data>                   _variables = new HashMap<String, Data>();
    Metadata                                _metadata  = new Metadata();

    @Property(Weather.NS.HAS_STATION_ID)
    String                                  _id;

    @Property(Weather.NS.HAS_ELEVATION)
    double                                  _altitude;
    @Property(Weather.NS.HAS_LONGITUDE)
    double                                  _longitude;
    @Property(Weather.NS.HAS_LATITUDE)
    double                                  _latitude;
    @Property(Weather.NS.HAS_VARIABLES)
    HashMap<String, Pair<Integer, Integer>> _provided  = new HashMap<String, Pair<Integer, Integer>>();

    double[] requireYearData(String variable, int year) {

        double[] ret = null;
        Data dr = _variables.get(variable);
        if (dr == null) {
            dr = new Data();
            dr.variable = variable;
            dr.startYear = dr.endYear = year;
            ret = new double[daysInYear(year)];
            for (int i = 0; i < ret.length; i++)
                ret[i] = Double.NaN;
            dr.data.put(year, ret);
            _variables.put(variable, dr);
        } else {
            if (dr.startYear > year)
                dr.startYear = year;
            if (dr.endYear < year)
                dr.endYear = year;
            if (dr.data.containsKey(year)) {
                ret = dr.data.get(year);
            } else {
                ret = new double[daysInYear(year)];
                for (int i = 0; i < ret.length; i++)
                    ret[i] = Double.NaN;
                dr.data.put(year, ret);
            }
        }

        return ret;
    }

    /*
     * set to the distance from the original request when this is coming from a
     * nearest neighbor search.
     */
    double     _distanceKm = -1;

    ShapeValue _location;

    @Property(Weather.NS.HAS_START_YEAR)
    int        _firstKnownYear;
    @Property(Weather.NS.HAS_LAST_YEAR)
    int        _lastKnownYear;

    @Property(Weather.NS.HAS_DATA_URL)
    String     _dataURL;

    // for the DB
    public WeatherStation() {
    }

    public WeatherStation(String baseURL, String id, double lat, double lon, double alt, String state,
            String name) {

        _id = id;
        _altitude = alt;
        _longitude = lon;
        _latitude = lat;

        /*
         * URL of data file for this station. Specific to structure of GHCN
         * archive.
         */
        _dataURL = baseURL + "/ghcnd_all/" + id + ".dly";

        try {
            _location = new ShapeValue("EPSG:4326 POINT (" + lon + " " + lat + ")");
        } catch (ThinklabException e) {
            throw new ThinklabRuntimeException(e);
        }
    }

    public void initializeFromStorage() {
        try {
            _location = new ShapeValue("EPSG:4326 POINT (" + _longitude + " " + _latitude + ")");
        } catch (ThinklabValidationException e) {
            throw new ThinklabRuntimeException(e);
        }
    }

    @SuppressWarnings("unused")
    public Data fetchData(String variable) throws ThinklabIOException {

        if (_variables.containsKey(variable))
            return _variables.get(variable);

        String[] vars = variable.split(",");

        boolean found = false;

        for (String var : vars) {

            for (FixedReader fr : FixedReader.parse(_dataURL, new int[] {
                    0,
                    11,
                    15,
                    17,
                    21,
                    26,
                    27,
                    28,
                    29,
                    34,
                    35,
                    36,
                    37,
                    261,
                    266,
                    267,
                    268 })) {

                String ID = fr.nextString();
                int YEAR = fr.nextInt();
                int MONTH = fr.nextInt();
                String ELEMENT = fr.nextString();

                if (var.equals(ELEMENT)) {

                    found = true;

                    for (int day = 1; day <= 31; day++) {

                        int VALUE = fr.nextInt(5);
                        String MFLAG = fr.nextString(1);
                        String QFLAG = fr.nextString(1);
                        String SFLAG = fr.nextString(1);

                        /*
                         * determine if this is a real date; skip if not
                         */
                        int yearDay = -1;
                        try {
                            DateMidnight dm = new DateMidnight(YEAR, MONTH, day);
                            yearDay = dm.getDayOfYear();
                        } catch (Throwable e) {
                            // not a real date, continue
                        }

                        if (yearDay < 0) {
                            continue;
                        }

                        double value = Double.NaN;
                        if (VALUE != -9999) {

                            /*
                             * process data to final representation according to
                             * type
                             */
                            if (var.equals(Weather.PRECIPITATION_MM) || var.equals(Weather.MAX_TEMPERATURE_C)
                                    || var.equals(Weather.MIN_TEMPERATURE_C)
                                    || var.equals(Weather.WIND_SPEED_M_SEC)
                            // TODO whatever else we want
                            ) {
                                value = (double) VALUE / 10.0;
                            } else {
                                value = (double) VALUE;
                            }

                        }

                        /*
                         * store it
                         */
                        double[] dd = requireYearData(var, YEAR);
                        if (dd != null) {
                            dd[yearDay - 1] = value;
                        }
                    }
                }
            }

            /*
             * found var in var list - stop here.
             */
            if (found)
                break;
        }

        // if no data were available, prevent further reading by storing a null
        if (!_variables.containsKey(variable))
            _variables.put(variable, null);

        return _variables.get(variable);
    }

    @Override
    public String toString() {

        /*
         * list variables
         */
        String vdesc = "[";
        for (String d : _provided.keySet()) {
            vdesc += (vdesc.length() == 1 ? "" : ",") + d;
        }
        vdesc += "]";

        return _id + " @" + _latitude + "," + _longitude + " " + vdesc
                + (_distanceKm < 0.0 ? "" : (" (" + _distanceKm + "km away)"));

    }

    @Override
    public void addStorageMetadata(IMetadata metadata, IMonitor monitor) throws ThinklabException {
        ((Metadata) metadata).put(Weather.NS.HAS_LOCATION, _location);
    }

    @Override
    public IMetadata getMetadata() {
        return _metadata;
    }

    /**
     * This point is the one that we wanted originally; compute the distance in
     * km to record accuracy from the original request.
     * 
     * @param location
     */
    public void setRequestedPoint(ShapeValue location) {
        _distanceKm = GeoNS.getDistance(location, _location);
    }

    public static int daysInYear(int year) {
        LocalDate ld = new LocalDate(year, 1, 1);
        return Days.daysBetween(ld, ld.plusYears(1)).getDays();
    }

    /**
     * Return all the available daily data for the given variables in one
     * two-dimensional array.
     * 
     * @param wholeYears
     *            only return whole year time series
     * @param interpolateMissing
     *            interpolate missing data (stupidly)
     * @param skipLeapDays
     *            skip Feb 29 in leap years so that all years are 365 days
     * @param variables
     * @return array with an array per day, with the
     * @throws ThinklabException
     */
    public double[][] getYearlyData(boolean wholeYears, boolean interpolateMissing, boolean skipLeapDays,
            boolean consecutiveYears, String... variables) throws ThinklabException {

        Data[] records = new Data[variables.length];
        int i = 0;
        for (String v : variables) {
            Data dr = fetchData(v);
            if (dr == null)
                return null;
            records[i++] = dr;
        }

        /*
         * Determine years where all variables have records.
         */
        int minYear = 3000, maxYear = 0;
        for (Data dr : records) {
            if (dr.startYear < minYear) {
                minYear = dr.startYear;
            }
            if (dr.endYear > maxYear) {
                maxYear = dr.endYear;
            }
        }

        List<Integer> years = new ArrayList<Integer>();
        int prev = -1;
        for (int y = minYear; y <= maxYear; y++) {

            if (consecutiveYears && prev != -1 && y != prev + 1)
                break;

            prev = y;
            int n = 0;
            for (Data dr : records) {

                if (!dr.data.containsKey(y))
                    continue;

                if (wholeYears) {
                    /*
                     * TODO skip non-whole years - this is taken care of by interpolate() using the 
                     * ratio criterion.
                     */
                }
                n++;
            }
            if (n == records.length) {
                years.add(y);
            }
        }

        /*
         * build dataset. First collect the data arrays, skipping any year
         * when one or more is null due to too many no-data or not matching
         * expectations.
         */
        ArrayList<Integer> yearsFinal = new ArrayList<Integer>();
        ArrayList<ArrayList<double[]>> dataFinal = new ArrayList<ArrayList<double[]>>();

        for (Integer year : years) {
            ArrayList<double[]> dholder = new ArrayList<double[]>();
            for (int v = 0; v < records.length; v++) {
                double[] data = records[v].data.get(year);
                if (interpolateMissing) {
                    data = interpolate(data);
                }
                if (data != null && skipLeapDays) {
                    data = skipLeapDay(data);
                }
                if (data != null) {
                    dholder.add(data);
                }
            }

            if (dholder.size() == variables.length) {
                yearsFinal.add(year);
                dataFinal.add(dholder);
            }
        }

        /*
         * now create the final 
         */
        double[][] ret = null;
        int len = 0;
        for (Integer year : yearsFinal) {
            len += skipLeapDays ? 365 : daysInYear(year);
        }

        ret = new double[len][variables.length];
        int ofs = 0;
        for (int y = 0; y < yearsFinal.size(); y++) {

            int year = yearsFinal.get(y);
            int days = skipLeapDays ? 365 : daysInYear(year);
            ArrayList<double[]> dholder = dataFinal.get(y);

            for (int v = 0; v < records.length; v++) {
                for (int day = 0; day < days; day++) {
                    ret[ofs + day][v] = dholder.get(v)[day];
                }
            }

            ofs += days;
        }

        return ret;
    }

    private double[] skipLeapDay(double[] data) {
        if (data.length == 365)
            return data;

        double[] ret = new double[365];
        int n = 0;
        for (int i = 0; i < 365; i++) {
            ret[n++] = i <= 60 ? data[i] : data[i + 1];
        }
        return ret;
    }

    private double[] interpolate(double[] data) {

        int nans = 0;
        double firstValid = Double.NaN, lastValid = Double.NaN;
        for (double d : data) {
            if (Double.isNaN(d)) {
                nans++;
            } else {
                if (Double.isNaN(firstValid))
                    firstValid = d;
                lastValid = d;
            }
        }

        if (nans == 0)
            return data;

        /*
         * if last point is a NaN, set it to the first valid point to enable
         * interpolation. Do same for first point.
         */

        if (Double.isNaN(data[0])) {
            nans--;
        }
        if (Double.isNaN(data[data.length - 1])) {
            nans--;
        }

        /*
         * accept only 10% missing data max
         * FIXME using 90% (no less) because Malawi is the first case study :(
         */
        double nanProportion = (double) nans / (double) data.length;
        if (nanProportion > 0.9)
            return null;

        double[] x = new double[data.length - nans];
        double[] y = new double[data.length - nans];

        int n = 0;
        for (int i = 0; i < data.length; i++) {
            double val = data[i];
            if (Double.isNaN(val)) {
                if (i == 0) {
                    val = lastValid;
                } else if (i == data.length - 1) {
                    val = firstValid;
                }
            }

            if (!Double.isNaN(val)) {
                int idx = n++;
                x[idx] = i;
                y[idx] = val;
            }
        }

        UnivariateInterpolator interpolator = new SplineInterpolator();
        UnivariateFunction function = interpolator.interpolate(x, y);

        double[] ret = new double[data.length];
        for (int i = 0; i < data.length; i++) {
            if (Double.isNaN(data[i])) {
                ret[i] = function.value((double) i);
            } else {
                ret[i] = data[i];
            }
        }

        return ret;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof WeatherStation && ((WeatherStation) o)._id.equals(_id);
    }

    @Override
    public int hashCode() {
        return _id.hashCode();
    }

    /**
     * TODO return true if records for the passed variable are available. May be a multiple one,
     * if so they're in OR.
     * 
     * @param var
     * @return
     */
    public boolean provides(String var) {

        String[] vv = var.split(",");

        for (String v : vv) {
            if (_provided.containsKey(v)) {
                return true;
            }
        }
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public Object adapt() {
        // TODO Auto-generated method stub
        return null;
    }

}
