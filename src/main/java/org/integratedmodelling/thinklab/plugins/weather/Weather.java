package org.integratedmodelling.thinklab.plugins.weather;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.integratedmodelling.thinklab.api.modelling.IScale;
import org.integratedmodelling.thinklab.api.modelling.IState;

import com.vividsolutions.jts.geom.Polygon;

/**
 * The weather in a location. Also serves as a factory to obtain the weather given a lat/long pair, a frequency
 * of observation, a date range and a set of variables. Will use the GHCN dataset and if necessary
 * a weather generator to fulfill the request, picking the closest record to the requested
 * location. Weather can be modified with point events or bias and trends.
 * 
 * TODO due to the time required to initialize the database, this should be available
 * as a command usable via REST, and a client version implementing the same interface
 * as this class should be made available.
 * 
 * @author Ferd
 *
 */
public class Weather {

    /*
     * the concepts we need from the weather ontology.
     */
    public static class NS {
        public static final String WEATHER_STATION = "weather:WeatherStation";
        public static final String HAS_LOCATION = "weather:hasLocation";
        public static final String HAS_START_YEAR = "weather:hasStartYear";
        public static final String HAS_LAST_YEAR = "weather:hasEndYear";
        public static final String HAS_DATA_URL = "weather:hasDataURL";
        public static final String HAS_STATION_ID = "weather:hasStationId";
        public static final String HAS_ELEVATION = "weather:hasElevation";
        public static final String HAS_LONGITUDE = "weather:hasLongitude";
        public static final String HAS_LATITUDE = "weather:hasLatitude";
        public static final String HAS_VARIABLES = "weather:hasVariables";
    }

    /*
     * variable keys matched to GHCN codes. There's more.
     * TODO need cloudiness, insolation/shortwave radiation 
     */
    public final static String PRECIPITATION_MM = "PRCP";
    public final static String SNOWFALL_MM = "SNOW";
    public final static String SNOWDEPTH_MM = "SNDP";
    public final static String MAX_TEMPERATURE_C = "TMAX";
    public final static String MIN_TEMPERATURE_C = "TMIN";

    // this one will (should) get the first available of the 4 choices.
    public final static String AVG_CLOUDINESS_PCT = "ACMC,ACMH,ACSC,ACSH";
    public final static String WIND_SPEED_M_SEC = "AWND";

    /*
     * keys for frequency
     */
    public final static int HOURLY = 0;
    public final static int DAILY = 0;
    public final static int MONTHLY = 0;
    public final static int YEARLY = 0;

    /**
     * Chainable changes in any weather variable that will affect weather generation when
     * observations are retrieved. Use with the {@link Weather.apply()} function.
     * 
     * @author Ferd
     *
     */
    public static class Scenario {

        // modes of distribution of quantitative changes
        final static public int DISTRIBUTION_CONSTANT = 0;
        final static public int DISTRIBUTION_RANDOM = 1;

        // modes of application of quantitative amount of change
        final static public int APPLICATION_ABSOLUTE = 0;
        final static public int APPLICATION_PROPORTIONAL = 1;

        /*
         * Further changes to apply in sequence
         */
        ArrayList<Scenario> _scenarios = new ArrayList<Scenario>();

        int _distribution;
        int _application;
        double _amount;
        String _variable;
        Date _from = null;
        Date _to = null;

        /**
         * A change across the board, either proportional or absolute. Can be
         * chained to other changes using add().
         * 
         * @param variable
         * @param amount
         * @param mode
         * @param distribution
         * @return
         */
        public Scenario(String variable, double amount, int applicationMode, int distributionMode) {
            _variable = variable;
            _amount = amount;
            _application = applicationMode;
            _distribution = distributionMode;
        }

        /**
         * A proportional or absolute change in a variable that applies to a limited time span. Can be
         * chained to other changes using add().
         * 
         * @param variable
         * @param amount
         * @param from
         * @param to
         * @param applicationMode
         * @param distributionMode
         */
        public Scenario(String variable, double amount, Date from, Date to, int applicationMode,
                int distributionMode) {
            _variable = variable;
            _amount = amount;
            _application = applicationMode;
            _distribution = distributionMode;
            _from = from;
            _to = to;
        }

        /**
         * Add a new scenario (usually in a different variable). Same parameters as the corresponding
         * constructor. Returns this so multiple changes can be stringed together.
         * 
         * @param variable
         * @param amount
         * @param applicationMode
         * @param distributionMode
         * @return
         */
        public Scenario add(String variable, double amount, int applicationMode, int distributionMode) {
            _scenarios.add(new Scenario(variable, amount, applicationMode, distributionMode));
            return this;
        }

        /**
         * Add a new scenario (usually in a different variable). Same parameters as the corresponding
         * constructor. Returns this so multiple changes can be stringed together.
         *
         * @param variable
         * @param amount
         * @param from
         * @param to
         * @param applicationMode
         * @param distributionMode
         * @return
         */
        public Scenario add(String variable, double amount, Date from, Date to, int applicationMode,
                int distributionMode) {
            _scenarios.add(new Scenario(variable, amount, from, to, applicationMode, distributionMode));
            return this;
        }
    }

    /*
     * the stations we use. If more than one, there must be a polygon of influence
     * in the _polygons array. These are generated from the spatial scale using 
     * Voronoi tessellation.
     */
    ArrayList<WeatherStation> _stations = new ArrayList<WeatherStation>();
    ArrayList<Polygon> _polygons = new ArrayList<Polygon>();

    Scenario _scenario = null;

    /**
     * Generate weather according to the passed station. Will use a weather generator for
     * any year where the weather is unknown (this feature is limited to precipitation and
     * min/max temperatures for now).
     * 
     * @param station
     */
    public Weather(WeatherStation station) {
        _stations.add(station);
    }

    /**
     * This one is used to create spatially adjusted weather based on how many stations as
     * available and a map of average precipitation used to correct the weather proportionally
     * in each point. All stations must be in the range; if using a station outside of the
     * range, it will be adjusted to "pretend" it's in the center of the region.
     * 
     * @param scale
     * @param precipitationAverages
     * @param stations
     */
    public Weather(IScale scale, IState precipitationAverages, WeatherStation... stations) {
        for (WeatherStation s : stations)
            _stations.add(s);
        if (_stations.size() > 0) {
            /*
             * TODO generate station influence polygons
             */
        }
    }

    public List<WeatherStation> getWeatherStations() {
        return _stations;
    }

    /**
     * Return weather observations for the specified period and variables. It will use data from
     * the weather station if the period is known, interpolating if necessary, or train a weather
     * generator to produce data if the period is unknown.
     * 
     * @param start
     * @param end
     * @param frequencyKey
     * @param variables
     * @return
     */
    public double[][] getObservations(Date start, Date end, int frequencyKey, String... variables) {

        /*
         * Divide period in years and prepare storage.
         * for each variable:
         * 		fill in each available year (whole only). If nothing, check if ANY year exists. If not, return null.
         * for each variable:
         * 		if gaps, fill in gap years using weather generator (daily). Train WG to closest years.
         * 	    rescale results to requested coverage to return values
         *      modify as requested by scenarios
         */

        return null;
    }

    /**
     * Apply a scenario. Returns the same scenario so that a fluent idiom can be used to apply
     * multiple changes easily.
     * 
     * @param scenario
     * @return
     */
    public Scenario apply(Scenario scenario) {
        _scenario = scenario;
        return scenario;
    }

}
