package org.integratedmodelling.thinklab.plugins.weather.commands;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.runtime.ISession;
import org.integratedmodelling.thinklab.command.Command;
import org.integratedmodelling.thinklab.interfaces.annotations.ThinklabCommand;
import org.integratedmodelling.thinklab.interfaces.commands.ICommandHandler;
import org.integratedmodelling.thinklab.plugins.weather.WeatherFactory;
import org.integratedmodelling.thinklab.rest.interfaces.IStatelessService;

@ThinklabCommand(name = "weather:load-stations")
public class LoadStations implements ICommandHandler, IStatelessService {

    static final String PREVIOUS_CONTEXT_ID  = "_previous_context";
    static final String PREVIOUS_OBSERVER_ID = "_previous_observer";

    @Override
    public Object execute(Command command, ISession session) throws ThinklabException {

        /*
         * TODO check authentication - this should be an admin command only
         */
        WeatherFactory.get().checkDatabase();

        return null;

    }
}
