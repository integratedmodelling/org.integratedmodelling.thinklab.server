package org.integratedmodelling.thinklab.plugins.weather.commands;

import java.util.HashMap;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.runtime.ISession;
import org.integratedmodelling.thinklab.command.Command;
import org.integratedmodelling.thinklab.interfaces.annotations.ThinklabCommand;
import org.integratedmodelling.thinklab.interfaces.commands.ICommandHandler;
import org.integratedmodelling.thinklab.rest.interfaces.IStatelessService;

@ThinklabCommand(
        name = "weather:get-stations",
        argumentNames = "bbox,varlist",
        argumentTypes = "thinklab:Text,thinklab:Text",
        argumentDescriptions = "bounding box in lat/lon,variables requested")
public class GetStations implements ICommandHandler, IStatelessService {

    static final String PREVIOUS_CONTEXT_ID  = "_previous_context";
    static final String PREVIOUS_OBSERVER_ID = "_previous_observer";

    @Override
    public Object execute(Command command, ISession session) throws ThinklabException {

        HashMap<String, Object> ret = new HashMap<String, Object>();

        return ret;
    }
}
