package org.integratedmodelling.thinklab.visualization;

import java.awt.Image;
import java.util.Map;

import javax.activation.MimeType;
import javax.activation.MimeTypeParseException;

import org.integratedmodelling.common.utils.image.ImageUtil;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.modelling.IObservation;
import org.integratedmodelling.thinklab.api.modelling.IScale;
import org.integratedmodelling.thinklab.api.modelling.IState;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.api.modelling.visualization.IColormap;
import org.integratedmodelling.thinklab.api.modelling.visualization.IImageMedia;
import org.integratedmodelling.thinklab.api.modelling.visualization.IImageViewport;
import org.integratedmodelling.thinklab.api.modelling.visualization.ILegend;
import org.integratedmodelling.thinklab.api.modelling.visualization.IMedia;
import org.integratedmodelling.thinklab.api.modelling.visualization.IViewport;
import org.integratedmodelling.thinklab.api.space.ISpatialExtent;
import org.integratedmodelling.thinklab.api.time.ITemporalExtent;
import org.integratedmodelling.thinklab.common.visualization.ContourPlot;
import org.integratedmodelling.thinklab.geospace.extents.Grid;
import org.integratedmodelling.thinklab.geospace.extents.SpaceExtent;
import org.integratedmodelling.thinklab.geospace.interfaces.IGridMask;
import org.integratedmodelling.thinklab.geospace.literals.ShapeValue;
import org.integratedmodelling.thinklab.visualization.geospace.GeoImageFactory;
import org.integratedmodelling.utils.image.processing.ImageProc;
import org.restlet.data.MediaType;

public class ImageMedia implements IImageMedia {

    public static final int COVERAGE_MAP = 0;
    public static final int EARTH_IMAGE_BACKGROUND = 1;
    public static final int CONTOUR_MAP = 2;

    private double SMOOTH_FACTOR = 1.8;

    IImageViewport _viewport;
    IObservation _observation;
    IScale.Index _index;

    private IColormap _colormap;
    //    private StateClass _type;
    private ILegend _legend;
    private IMonitor _monitor;

    /*
     * true if no-data appear in the data.
     */
    boolean _hasNull = false;

    //    private IClassification _classification;
    //    private int[] _data;
    //    private double[] _ddata;

    // default for spatial maps
    int _mapType = COVERAGE_MAP;

    //    /*
    //     * data to match visualized _data to their meaning. Only set
    //     * if data are categorized.
    //     */
    //    private HashMap<Object, Integer> _values = null;

    //    /*
    //     * number of values (excluding the null)
    //     */
    //    private boolean _needsZeroInColormap;
    //    private int _offset;
    //    private int _levels;
    private MimeType _mtype;

    public ImageMedia(IObservation observation, IScale.Index index, IViewport viewport, MediaType type,
            Map<String, Object> options) {
        _viewport = (IImageViewport) viewport;
        _observation = observation;
        _index = index;
        try {
            _mtype = new MimeType(type.getName());
        } catch (MimeTypeParseException e) {
            throw new ThinklabRuntimeException(e);
        }
        //        if (observation instanceof State) {
        //            analyze((State) observation);
        //        }

        if (options != null) {
            if (options.containsKey("image-type")) {
                String tp = options.get("image-type").toString();
                if (tp.equals("coverage")) {
                    _mapType = COVERAGE_MAP;
                } else if (tp.equals("contour")) {
                    _mapType = CONTOUR_MAP;
                }
            }
        }
    }

    @Override
    public MimeType getMIMEType() {
        return _mtype;
    }

    @Override
    public ILegend getLegend() {
        return _legend;
    }

    @Override
    public IColormap getColormap() {
        if (_colormap == null && _observation instanceof IState) {
            _colormap = VisualizationFactory.getColormap((IState) _observation, _index);
        }
        return _colormap;
    }

    @Override
    public IViewport getViewport() {
        return _viewport;
    }

    @Override
    public Image getImage() throws ThinklabException {

        ISpatialExtent space = _observation.getScale().getSpace();
        ITemporalExtent time = _observation.getScale().getTime();

        if (_observation instanceof ISubject) {

            if (space != null) {
                return getSatelliteImage(space);
            }

        } else if (_observation instanceof IState) {

            if (space != null) {
                return makeMap(space, VisualizationFactory.getDisplayData((IState) _observation, _index), _mapType);
            }
            if (time != null) {

            }
        }

        return null;
    }

    @Override
    public IMedia scale(IViewport viewport) {
        // TODO Auto-generated method stub
        return null;
    }

    public Image getSatelliteImage(ISpatialExtent spaceExt) throws ThinklabException {
        ShapeValue shape = ((SpaceExtent) spaceExt).getShape();
        return GeoImageFactory.get()
                .getImagery(shape.getEnvelope(), shape, _viewport.getWidth(), _viewport.getHeight(), 0);
    }

    public Image makeMap(ISpatialExtent spaceExt, int[] data, int type) throws ThinklabException {

        if (data == null) {
            return null;
        }
        int n = 0;
        for (int z : data) {
            if (z > 0) {
                n++;
            }
        }
        if (spaceExt instanceof SpaceExtent && ((SpaceExtent) spaceExt).isGrid()) {

            Grid space = ((SpaceExtent) spaceExt).getGrid();
            IGridMask mask = space.getActivationLayer();

            int[] xy = _viewport.getSizeFor(space.getXCells(), space.getYCells());

            if (type == COVERAGE_MAP) {

                return ImageUtil
                        .createImage(ImageUtil.upsideDown(data, space.getXCells()), space.getXCells(), xy[0], xy[1], getColormap());

            } else if (type == EARTH_IMAGE_BACKGROUND) {

                return GeoImageFactory
                        .get()
                        .getRasterImagery(space.getEnvelope(), xy[0], xy[1], data, space.getXCells(), getColormap());

            } else if (type == CONTOUR_MAP) {

                int cols = space.getXCells();
                int rows = space.getYCells();

                double[][] plotdata = new double[rows][cols];

                if (data != null) {

                    for (int row = 0; row < rows; row++) {
                        for (int col = 0; col < cols; col++) {
                            double d = data[space.getIndex(col, row)];
                            boolean active = mask == null || mask.isActive(space.getIndex(col, row));
                            plotdata[rows - row - 1][col] = (!active || Double.isNaN(d)) ? 0.0 : d;
                        }
                    }
                }

                return ContourPlot.createPlot(xy[0], xy[1], ImageProc
                        .gaussianSmooth0(plotdata, SMOOTH_FACTOR));

            }
        }
        return null;
    }

    //    /*
    //     * defines type, colormap and legend
    //     */
    //    private void analyze(State state) {
    //
    //        IObserver observer = Observer.getRepresentativeObserver(state.getObserver());
    //        IMetadata metadata = state.getMetadata();
    //
    //        double lowerBound = Double.NaN;
    //        double upperBound = Double.NaN;
    //        String cmapDef = null;
    //
    //        if (metadata.get(NS.LOWER_BOUND_PROPERTY) != null) {
    //            lowerBound = metadata.getDouble(NS.LOWER_BOUND_PROPERTY);
    //        }
    //        if (metadata.get(NS.UPPER_BOUND_PROPERTY) != null) {
    //            upperBound = metadata.getDouble(NS.UPPER_BOUND_PROPERTY);
    //        }
    //        if (metadata.get(NS.COLORMAP_PROPERTY) != null) {
    //            cmapDef = metadata.getString(NS.COLORMAP_PROPERTY);
    //        }
    //
    //        /*
    //         * FIXME all this and the whole type thing is probably entirely unnecessary.
    //         */
    //        if (observer instanceof INumericObserver) {
    //
    //            _classification = ((INumericObserver) observer).getDiscretization();
    //
    //            if (_classification != null) {
    //                _type = StateClass.DISCRETIZED_RANGES;
    //            } else if (!Double.isNaN(lowerBound) && !Double.isNaN(upperBound)) {
    //                _type = StateClass.QUANTITATIVE_BOUNDED;
    //            } else {
    //                _type = StateClass.QUANTITATIVE_UNBOUNDED;
    //            }
    //        } else if (observer instanceof IClassifyingObserver) {
    //
    //            _classification = ((IClassifyingObserver) observer).getClassification();
    //
    //            if (_classification != null) {
    //                if (_classification.isDiscretization()) {
    //
    //                    _type = StateClass.DISCRETIZED_RANGES;
    //
    //                } else {
    //
    //                    _type = _classification.hasZeroRank() ? StateClass.ORDERED_CLASSIFICATION_Z : StateClass.ORDERED_CLASSIFICATION_NZ;
    //                }
    //            } else {
    //
    //                /*
    //                 * create an emergency classification from children - this is for 
    //                 * classifications coming from datasources, accessors or other
    //                 * observations that do not have a full annotation.
    //                 */
    //                _classification = new Classification(state.getObservable().getType());
    //                _type = _classification.hasZeroRank() ? StateClass.ORDERED_CLASSIFICATION_Z : StateClass.ORDERED_CLASSIFICATION_NZ;
    //            }
    //
    //        } else if (observer instanceof ICategorizingObserver) {
    //
    //            _type = StateClass.ENUMERATED;
    //        }
    //
    //        if ((_data = classifyData((State) state, lowerBound, upperBound)) != null) {
    //            _colormap = createColormap((State) state, lowerBound, upperBound, cmapDef);
    //            _legend = createLegend(state.getObserver(), lowerBound, upperBound, metadata);
    //        }
    //    }
    //
    //    private IColormap createColormap(State _state2, double lowerBound, double upperBound, String cmapDef) {
    //
    //        IColormap ret = makeColormapFromConcepts();
    //        int levels = _levels == 0 ? 1 : _levels;
    //
    //        if (ret == null && cmapDef != null) {
    //            ret = ColorMap.getColormap(cmapDef, levels, _needsZeroInColormap);
    //        }
    //
    //        if (ret /* still */== null) {
    //
    //            if (_values != null) {
    //                ret = _type.equals(StateClass.ENUMERATED) ? ColorMap.random(_levels) : (levels < 10 ? (levels > 2 ? ColorMap
    //                        .getColormap("YlOrRd()", levels, _needsZeroInColormap) : ColorMap
    //                        .getColormap("Blues()", levels, _needsZeroInColormap)) : ColorMap.jet(levels));
    //            } else {
    //                ret = ColorMap.jet(levels);
    //            }
    //        }
    //
    //        return ret;
    //    }
    //
    //    /*
    //     * if we have a legend and it contains concepts,
    //     * check concepts metadata; if they all have colors associated, use those and that's it.
    //    */
    //    private IColormap makeColormapFromConcepts() {
    //
    //        IColormap ret = null;
    //
    //        if (_values != null) {
    //
    //            Color[] colors = new Color[_levels];
    //
    //            for (Object o : _values.keySet()) {
    //                if (!(o instanceof IConcept)) {
    //                    return null;
    //                }
    //
    //                IConcept c = (IConcept) o;
    //                String col = c.getMetadata().getString(NS.COLOR_PROPERTY);
    //                if (col == null) {
    //                    return null;
    //                }
    //
    //                Color color = null;
    //                try {
    //                    color = ColorStringParser.parse(col);
    //                } catch (Exception e) {
    //                    return null;
    //                }
    //
    //                colors[_values.get(o)] = color;
    //            }
    //
    //            ret = new ColorMap(colors);
    //        }
    //
    //        return ret;
    //    }
    //
    //    int[] classifyData(State state, double lowerBound, double upperBound) {
    //
    //        BitSet mask = null;
    //        if (state.getSpace() instanceof SpaceExtent && ((SpaceExtent) state.getSpace()).isGrid()) {
    //            try {
    //                mask = (RasterActivationLayer) ((SpaceExtent) state.getSpace()).getGrid()
    //                        .getActivationLayer();
    //            } catch (ThinklabValidationException e) {
    //                // just leave the mask null
    //            }
    //        }
    //
    //        if (state.getDataClass() == null) {
    //
    //            // no data produced. Should warn.
    //            if (_monitor != null) {
    //                _monitor.warn(state.getDirectType() + " produced no data");
    //            }
    //            return null;
    //
    //        } else if (state.getDataClass().equals(String.class)) {
    //
    //            return matchEnumeration(state, mask);
    //
    //        } else if (state.getDataClass().equals(IndexedCategoricalDistribution.class)) {
    //
    //            return matchDistribution(state, lowerBound, upperBound, mask);
    //
    //        } else if (state.getDataClass().equals(Double.class)) {
    //
    //            return matchNumbers(state, lowerBound, upperBound, mask);
    //
    //        } else if (_classification != null) {
    //
    //            return matchClassification(state, mask);
    //        }
    //
    //        return null;
    //    }
    //
    //    private int[] matchNumbers(State state, double lowerBound, double upperBound, BitSet mask) {
    //
    //        int[] ret = new int[_index.size()];
    //
    //        // compute boundaries from data
    //        boolean isReal = false;
    //        boolean hasUnk = false;
    //        boolean isUnk = true;
    //
    //        Double min = null, max = null;
    //
    //        for (Iterator<Object> it = state.iterator(_index); it.hasNext();) {
    //
    //            Object vs = it.next(); // (mask != null && !mask.get(i)) ? null : state.getValue(i);
    //
    //            Double v = null;
    //            if (vs != null) {
    //                if (vs instanceof Boolean) {
    //                    v = ((Boolean) vs) ? 1.0 : Double.NaN;
    //                } else if (vs instanceof Number) {
    //                    v = ((Number) vs).doubleValue();
    //                }
    //            }
    //
    //            if (v != null && !Double.isNaN(v)) {
    //
    //                isUnk = false;
    //
    //                if (min == null) {
    //                    min = v;
    //                } else {
    //                    if (v < min) {
    //                        min = v;
    //                    }
    //                }
    //                if (max == null) {
    //                    max = v;
    //                } else {
    //                    if (v > max) {
    //                        max = v;
    //                    }
    //                }
    //
    //                if (!isReal && (v - Math.rint(v) != 0)) {
    //                    isReal = true;
    //                }
    //
    //            } else {
    //                hasUnk = true;
    //            }
    //        }
    //
    //        if (isUnk) {
    //            return ret;
    //        }
    //
    //        if (Double.isNaN(lowerBound)) {
    //            lowerBound = min;
    //        }
    //        if (Double.isNaN(upperBound)) {
    //            upperBound = max;
    //        }
    //
    //        /*
    //         * if we have no-data, reserve the 0 for that and flag this so we can use
    //         * transparency. Otherwise 0 is a fine value and the colormap will decide
    //         * how it's displayed.
    //         */
    //        this._needsZeroInColormap = hasUnk;
    //        this._offset = hasUnk ? 1 : 0;
    //        this._levels = hasUnk ? 255 : 256;
    //        this._ddata = new double[_index.size()];
    //
    //        int i = 0;
    //        for (Iterator<Object> it = state.iterator(_index); it.hasNext();) {
    //
    //            Object o = it.next();
    //            Double v = null;
    //            if (mask == null || mask.get(i)) {
    //                if (o instanceof Boolean) {
    //                    v = ((Boolean) o) ? 1.0 : 0.0;
    //                } else if (o instanceof Number) {
    //                    v = ((Number) o).doubleValue();
    //                }
    //            }
    //
    //            if (v == null || Double.isNaN(v)) {
    //                ret[i] = 0;
    //            } else {
    //                ret[i] = (int) (((v - lowerBound) / (upperBound - lowerBound)) * (_levels - 1)) + _offset;
    //            }
    //
    //            _ddata[i] = v == null ? Double.NaN : v;
    //
    //            i++;
    //        }
    //
    //        return ret;
    //    }
    //
    //    private int[] matchClassification(State state, BitSet mask) {
    //
    //        _values = new HashMap<Object, Integer>();
    //
    //        /*
    //         * TODO this makes non-zero classifications look weird
    //         */
    //        this._needsZeroInColormap = true;
    //
    //        int idx = 1;
    //
    //        for (Iterator<Object> it = state.iterator(_index); it.hasNext();) {
    //
    //            Object o = it.next(); // (mask != null && !mask.get(i)) ? null : state.getValue(i);
    //            if (o == null) {
    //                _hasNull = true;
    //            } else if (!_values.containsKey(o)) {
    //                _values.put(o, idx++);
    //            }
    //        }
    //
    //        _levels = _values.size() + 1;
    //        _offset = 1;
    //
    //        int i = 1;
    //        for (IConcept c : _classification.getConceptOrder()) {
    //            _values.put(c, i++);
    //        }
    //
    //        int[] ret = new int[_index.size()];
    //
    //        int min = Integer.MAX_VALUE, max = Integer.MIN_VALUE;
    //        i = 0;
    //        for (Iterator<Object> it = state.iterator(_index); it.hasNext();) {
    //            IConcept c = (IConcept) it.next();
    //            boolean active = mask.get(i);
    //            if (c != null && active) {
    //                int dioporco = _values.get(c);
    //                System.out.println("" + dioporco);
    //            }
    //            ret[i++] = c == null ? 0 : ((mask != null && !mask.get(i)) ? 0 : _values.get(c));
    //        }
    //
    //        return ret;
    //    }
    //
    //    private int[] matchDistribution(State state, double lowerBound, double upperBound, BitSet mask) {
    //
    //        /*
    //         * TODO
    //         * discrete distribution require both
    //         * preserving the distribution values and keeping the quantitative
    //         * relationship of their means.
    //         * 
    //         * Here we just visualize the MEAN and nothing else. We should
    //         * prepare a legend but that would need to contain the whole
    //         * data as the match between _data and the precise distribution
    //         * is lost.
    //         */
    //        int[] ret = new int[_index.size()];
    //
    //        // compute boundaries from data
    //        boolean isReal = false;
    //        boolean hasUnk = false;
    //        boolean isUnk = true;
    //
    //        Double min = null, max = null;
    //
    //        for (Iterator<Object> it = state.iterator(_index); it.hasNext();) {
    //            Object o = it.next();
    //            IndexedCategoricalDistribution dist = /*(mask != null && !mask.get(i)) ? null
    //                                                  : */(IndexedCategoricalDistribution) o;
    //            if (dist != null) {
    //
    //                double v = dist.getMean();
    //                isUnk = false;
    //
    //                if (min == null) {
    //                    min = v;
    //                } else {
    //                    if (v < min) {
    //                        min = v;
    //                    }
    //                }
    //                if (max == null) {
    //                    max = v;
    //                } else {
    //                    if (v > max) {
    //                        max = v;
    //                    }
    //                }
    //
    //                if (!isReal && (v - Math.rint(v) != 0)) {
    //                    isReal = true;
    //                }
    //
    //            } else {
    //                hasUnk = true;
    //            }
    //        }
    //
    //        if (isUnk) {
    //            return ret;
    //        }
    //
    //        if (Double.isNaN(lowerBound)) {
    //            lowerBound = min;
    //        }
    //        if (Double.isNaN(upperBound)) {
    //            upperBound = max;
    //        }
    //
    //        /*
    //         * if we have no-data, reserve the 0 for that and flag this so we can use
    //         * transparency. Otherwise 0 is a fine value and the colormap will decide
    //         * how it's displayed.
    //         */
    //        this._needsZeroInColormap = hasUnk;
    //        this._offset = hasUnk ? 1 : 0;
    //        this._levels = hasUnk ? 255 : 256;
    //        this._ddata = new double[_index.size()];
    //        int i = 0;
    //        for (Iterator<Object> it = state.iterator(_index); it.hasNext();) {
    //
    //            Object o = it.next();
    //            IndexedCategoricalDistribution dist = /*(mask != null && !mask.get(i)) ? null
    //                                                  :*/(IndexedCategoricalDistribution) o;
    //            double v = dist == null ? Double.NaN : dist.getMean();
    //
    //            if (dist == null) {
    //                ret[i] = 0;
    //            } else {
    //                ret[i] = (int) (((v - lowerBound) / (upperBound - lowerBound)) * (_levels - 1)) + _offset;
    //            }
    //
    //            _ddata[i] = v;
    //            i++;
    //        }
    //
    //        return ret;
    //    }
    //
    //    private int[] matchEnumeration(State state, BitSet mask) {
    //
    //        _values = new HashMap<Object, Integer>();
    //
    //        int idx = 1;
    //
    //        for (Iterator<Object> it = state.iterator(_index); it.hasNext();) {
    //            Object o = it.next(); //(mask != null && !mask.get(i)) ? null : state.getValue(i);
    //            if (o == null) {
    //                _hasNull = true;
    //            } else if (!_values.containsKey(o)) {
    //                _values.put(o, idx++);
    //            }
    //        }
    //
    //        this._needsZeroInColormap = true;
    //        _levels = _values.size() + 1;
    //        _offset = 1;
    //
    //        int[] ret = new int[_index.size()];
    //        int i = 0;
    //        for (Iterator<Object> it = state.iterator(_index); it.hasNext();) {
    //            Object c = it.next(); // (mask != null && !mask.get(i)) ? null : state.getValue(i);
    //            ret[i] = c == null ? 0 : _values.get(c);
    //            i++;
    //        }
    //
    //        return ret;
    //    }

    //    private ILegend createLegend(IObserver observer, double lowerBound, double upperBound, IMetadata metadata) {
    //        return new Legend(_values, _data, _ddata, observer, lowerBound, upperBound, _colormap, metadata);
    //    }

}
