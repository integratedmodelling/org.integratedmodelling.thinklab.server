package org.integratedmodelling.thinklab.visualization;

import java.util.Map;

import org.integratedmodelling.exceptions.ThinklabRuntimeException;
import org.integratedmodelling.thinklab.api.modelling.IObservation;
import org.integratedmodelling.thinklab.api.modelling.IScale;
import org.integratedmodelling.thinklab.api.modelling.IState;
import org.integratedmodelling.thinklab.api.modelling.visualization.IMedia;
import org.integratedmodelling.thinklab.api.modelling.visualization.IViewport;
import org.integratedmodelling.thinklab.api.modelling.visualization.IVisualizationFactory;
import org.integratedmodelling.thinklab.common.data.IndexedCategoricalDistribution;
import org.integratedmodelling.thinklab.modelling.states.State;
import org.restlet.data.MediaType;

public class VisualizationFactory extends
        org.integratedmodelling.thinklab.common.visualization.VisualizationFactory implements
        IVisualizationFactory {

    private static VisualizationFactory _this = null;

    public static VisualizationFactory get() {
        if (_this == null) {
            _this = new VisualizationFactory();
        }
        return _this;
    }

    // public IStateVisualization getVisualization(IState state, ISubject context) {
    // return new StateVisualization(state, context, null, null);
    // }

    // @Override
    public double[] getStateDataAsNumbers(IState state) {

        double[] ret = new double[(int) state.getValueCount()];
        int nullcount = 0;
        for (int i = 0; i < state.getValueCount(); i++) {

            Double d = Double.NaN;
            Object o = ((State) state).getValue(i);

            if (o instanceof Number) {
                d = ((Number) o).doubleValue();
            } else if (o instanceof IndexedCategoricalDistribution) {
                d = ((IndexedCategoricalDistribution) o).getMean();
            } else if (o instanceof Boolean) {
                d = ((Boolean) o) ? 1.0 : 0.0;
            } else if (o != null) {
                throw new ThinklabRuntimeException("internal: unexpected state value in VisualizationFactory.getStateData");
            }

            if (Double.isNaN(d))
                nullcount++;

            ret[i] = d;
        }

        return ret;
    }

    @Override
    public IMedia getMedia(IObservation observation, IScale.Index index, IViewport viewport, String mimeType,
            Map<String, Object> options) {

        MediaType type = new MediaType(mimeType);

        if (type.getParent().equals(MediaType.IMAGE_ALL)) {
            return new ImageMedia(observation, index, viewport, type, options);
        }

        return null;
    }

}
