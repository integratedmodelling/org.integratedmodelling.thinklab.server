/**
 * DurationValue.java
 * ----------------------------------------------------------------------------------
 *
 * Copyright (C) 2008 www.integratedmodelling.org
 * Created: Jan 17, 2008
 *
 * ----------------------------------------------------------------------------------
 * This file is part of ThinklabTimePlugin.
 *
 * ThinklabTimePlugin is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * ThinklabTimePlugin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * ----------------------------------------------------------------------------------
 *
 * @copyright 2008 www.integratedmodelling.org
 * @author    Ferdinando Villa (fvilla@uvm.edu)
 * @date      Jan 17, 2008
 * @license   http://www.gnu.org/licenses/gpl.txt GNU General Public License v3
 * @link      http://www.integratedmodelling.org
 **/
package org.integratedmodelling.thinklab.time.literals;

import static javax.measure.unit.SI.MILLI;
import static javax.measure.unit.SI.SECOND;

import javax.measure.converter.UnitConverter;
import javax.measure.unit.Unit;

import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.thinklab.annotation.SemanticLiteral;
import org.integratedmodelling.thinklab.api.annotations.Literal;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.lang.IParseable;
import org.integratedmodelling.thinklab.api.time.ITimeDuration;
import org.integratedmodelling.thinklab.api.time.ITimeInstant;
import org.integratedmodelling.thinklab.common.utils.MiscUtilities;
import org.integratedmodelling.thinklab.time.Time;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;

/**
 * A parsed literal representing a time duration. Linked to the DurationValue OWL class to be used as an
 * extended literal for it. Maximum resolution is milliseconds. Can be initialized
 * by a string expressing number with units; unit must express time and the syntax is the one
 * supported by the implementation of IUnit.
 *
 * @author Ferdinando Villa
 * FIXME make it wrap something real
 */
@Literal(concept = "time:DurationValue", javaClass = DurationValue.class, datatype = "")
public class DurationValue extends SemanticLiteral<Long> implements ITimeDuration, IParseable {

    String literal = null;
    int precision = TemporalPrecision.MILLISECOND;
    int origQuantity = 1;
    String origUnit = "";

    public DurationValue() throws ThinklabException {
        this((Long) null);
    }

    public DurationValue(Long milliseconds) {
        this(Time.DURATION_CONCEPT, milliseconds);
    }

    public DurationValue(IConcept concept, Long milliseconds) {
        super(concept, milliseconds);
    }

    public DurationValue(IConcept c) throws ThinklabException {
        super(c, 0l);
    }

    public DurationValue(String s) throws ThinklabValidationException {
        parse(s);
    }

    @Override
    public void parse(String s) throws ThinklabValidationException {

        /*
         * insert a space to break strings like 1s or 2year
         */
        int brk = -1;
        if (!s.contains(" ")) {
            for (int i = 0; i < s.length(); i++) {
                if (!Character.isDigit(s.charAt(i))) {
                    brk = i;
                    origUnit = s.substring(i);
                    break;
                }
            }
        } else {
            origUnit = s.substring(s.indexOf(" ") + 1);
        }

        if (brk > 0) {
            s = s.substring(0, brk) + " " + s.substring(brk);
        }

        if (Character.isDigit(s.charAt(0))) {
            this.origQuantity = MiscUtilities.readIntegerFromString(s);
        }

        try {
            precision = TemporalPrecision.getPrecisionFromUnit(s);
            literal = s;
            /* oh do I like this */
            UnitConverter duration = Unit.valueOf(origUnit).getConverterTo(MILLI(SECOND));
            value = (long) duration.convert((double) origQuantity);
            concept = Time.DURATION_CONCEPT;
        } catch (Exception e) {
            throw new ThinklabValidationException(e);
        }
    }

    public void wrap(Object o) {
        value = ((Number) o).longValue();
    }

    public boolean isNumber() {
        return false;
    }

    public boolean isText() {
        return false;
    }

    public boolean isBoolean() {
        return false;
    }

    public boolean isClass() {
        return false;
    }

    @Override
    public boolean isObject() {
        return false;
    }

    @Override
    public boolean isLiteral() {
        return true;
    }

    @Override
    public String toString() {
        return literal == null ? (value + " ms") : literal;
    }

    @Override
    public Object clone() {
        DurationValue ret = null;
        try {
            ret = new DurationValue(concept);
            ret.value = value;
        } catch (ThinklabException e) {
        }
        return ret;
    }

    @Override
    public long getMilliseconds() {
        return value;
    }

    @Override
    public Pair<ITimeInstant, ITimeInstant> localize() {

        DateTime date = new DateTime();
        TimeValue start = null, end = null;
        long val = value;

        switch (precision) {

        case TemporalPrecision.MILLISECOND:
            start = new TimeValue(date);
            end = new TimeValue(date.plus(val));
            break;
        case TemporalPrecision.SECOND:
            val = value / DateTimeConstants.MILLIS_PER_SECOND;
            start = new TimeValue(new DateTime(date.getYear(), date.getMonthOfYear(), date.getDayOfMonth(),
                    date.getHourOfDay(), date.getMinuteOfHour(), date.getSecondOfMinute(), 0));
            end = new TimeValue(start.getTimeData().plusSeconds((int) val));
            break;
        case TemporalPrecision.MINUTE:
            val = value / DateTimeConstants.MILLIS_PER_MINUTE;
            start = new TimeValue(new DateTime(date.getYear(), date.getMonthOfYear(), date.getDayOfMonth(),
                    date.getHourOfDay(), date.getMinuteOfHour(), 0, 0));
            end = new TimeValue(start.getTimeData().plusMinutes((int) val));
            break;
        case TemporalPrecision.HOUR:
            val = value / DateTimeConstants.MILLIS_PER_HOUR;
            start = new TimeValue(new DateTime(date.getYear(), date.getMonthOfYear(), date.getDayOfMonth(),
                    date.getHourOfDay(), 0, 0, 0));
            end = new TimeValue(start.getTimeData().plusHours((int) val));
            break;
        case TemporalPrecision.DAY:
            val = value / DateTimeConstants.MILLIS_PER_DAY;
            start = new TimeValue(new DateTime(date.getYear(), date.getMonthOfYear(), date.getDayOfMonth(),
                    0, 0, 0, 0));
            end = new TimeValue(start.getTimeData().plusDays((int) val));
            break;
        case TemporalPrecision.MONTH:
            start = new TimeValue(new DateTime(date.getYear(), date.getMonthOfYear(), 1, 0, 0, 0, 0));
            end = new TimeValue(start.getTimeData().plusMonths(origQuantity));
            break;
        case TemporalPrecision.YEAR:
            start = new TimeValue(new DateTime(date.getYear(), 1, 1, 0, 0, 0, 0));
            end = new TimeValue(start.getTimeData().plusYears(origQuantity));
            break;
        }

        return new Pair<ITimeInstant, ITimeInstant>(start, end);
    }

    public int getOriginalQuantity() {
        return origQuantity;
    }

    public String getOriginalUnit() {
        return origUnit;
    }

    @Override
    public boolean is(Object object) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public String asText() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int compareTo(ITimeDuration other) {
        long otherValue = other.getMilliseconds();
        if (value == otherValue) {
            return 0;
        }
        if (value < otherValue) {
            return -1;
        }
        return 1;
    }
}
