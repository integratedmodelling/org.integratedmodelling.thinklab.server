/**
 * PeriodValue.java
 * ----------------------------------------------------------------------------------
 *
 * Copyright (C) 2008 www.integratedmodelling.org
 * Created: Jan 17, 2008
 *
 * ----------------------------------------------------------------------------------
 * This file is part of ThinklabTimePlugin.
 *
 * ThinklabTimePlugin is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * ThinklabTimePlugin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * ----------------------------------------------------------------------------------
 *
 * @copyright 2008 www.integratedmodelling.org
 * @author    Ferdinando Villa (fvilla@uvm.edu)
 * @date      Jan 17, 2008
 * @license   http://www.gnu.org/licenses/gpl.txt GNU General Public License v3
 * @link      http://www.integratedmodelling.org
 **/
package org.integratedmodelling.thinklab.time.literals;

import java.util.BitSet;
import java.util.Iterator;

import org.integratedmodelling.collections.NumericInterval;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.annotation.SemanticLiteral;
import org.integratedmodelling.thinklab.api.annotations.Literal;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IProperty;
import org.integratedmodelling.thinklab.api.lang.IParseable;
import org.integratedmodelling.thinklab.api.metadata.IMetadata;
import org.integratedmodelling.thinklab.api.modelling.IExtent;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.IScale;
import org.integratedmodelling.thinklab.api.modelling.IScale.Index;
import org.integratedmodelling.thinklab.api.modelling.ITopologicallyComparable;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.api.space.ISpatialExtent;
import org.integratedmodelling.thinklab.api.time.ITemporalExtent;
import org.integratedmodelling.thinklab.api.time.ITimeInstant;
import org.integratedmodelling.thinklab.api.time.ITimePeriod;
import org.integratedmodelling.thinklab.time.Time;
import org.joda.time.Interval;

@Literal(concept = "time:PeriodValue", javaClass = Interval.class, datatype = "")
public class PeriodValue extends SemanticLiteral<Object> // inheritance graph prevents us from specifying <Interval>
        implements ITimePeriod, IParseable {

    private static IConcept getBaseTimeConcept() {
        return Thinklab.c(Time.PERIOD_TYPE_ID);
    }

    @Override
    public void parse(String s) throws ThinklabValidationException {
        try {
            /* literal is two dates separated by a pound sign */
            concept = getBaseTimeConcept();
        } catch (Exception e) {
            throw new ThinklabValidationException(e);
        }
    }

    @Override
    public int[] getDimensionSizes(boolean rowFirst) {
        return new int[] { (int) getMultiplicity() };
    }

    @Override
    public int[] getDimensionOffsets(int linearOffset, boolean rowFirst) {
        return new int[] { linearOffset };
    }

    public PeriodValue(IConcept c, Interval interval) throws ThinklabException {
        super(c, interval);
    }

    public PeriodValue(Interval interval) throws ThinklabException {
        super(getBaseTimeConcept(), interval);
    }

    public PeriodValue(String s) throws ThinklabException {
        parse(s);
    }

    public PeriodValue(long x, long x2) {
        super(getBaseTimeConcept(), new Interval(x, x2));
    }

    protected Interval getValue() {
        // simple wrapper for casting for consistency
        return (Interval) value;
    }

    @Override
    public String toString() {
        return value.toString();
    }

    @Override
    public Object clone() {
        PeriodValue ret = null;
        try {
            ret = new PeriodValue(concept, getInterval());
        } catch (ThinklabException e) {
        }
        return ret;
    }

    public Interval getInterval() {
        return (Interval) value;
    }

    @Override
    public boolean is(Object other) {
        if (!other.getClass().equals(getClass())) {
            return false;
        }
        return ((PeriodValue) other).getStart().equals(getStart())
                && ((PeriodValue) other).getEnd().equals(getEnd());
    }

    @Override
    public String asText() {
        // ??
        return toString();
    }

    /**
     * Does this time period contain the time instant?
     *
     * (t,t+1] Semantics:
     * This class modifies the behavior of the underlying Joda Interval (and most interval semantics)
     * in that the start value is EXCLUSIVE and the end value is INCLUSIVE.
     *
     * This is to avoid circular-dependency problems during observation.
     *
     * @param time
     * @return
     */
    @Override
    public boolean contains(ITimeInstant time) {
        long millisInstant = time.getMillis();
        return contains(millisInstant);
    }

    /**
     * @see #contains(TimeValue)
     */
    @Override
    public boolean contains(long millisInstant) {
        long thisStart = getInterval().getStartMillis();
        long thisEnd = getInterval().getEndMillis();
        return (millisInstant > thisStart && millisInstant <= thisEnd);
    }

    @Override
    public double getCoveredExtent() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public boolean endsBefore(ITimeInstant instant) {
        return (getEnd().compareTo(instant) < 0);
    }

    @Override
    public boolean endsBefore(ITemporalExtent other) {
        return (getEnd().compareTo(other.getStart()) < 0);
    }

    @Override
    public boolean overlaps(ITemporalExtent other) {
        return (getStart().compareTo(other.getEnd()) < 0) && (getEnd().compareTo(other.getStart()) > 0);
    }

    @Override
    public ITimeInstant getStart() {
        return new TimeValue(getValue().getStartMillis());
    }

    @Override
    public ITimeInstant getEnd() {
        return new TimeValue(getValue().getEndMillis());
    }

    @Override
    public ITemporalExtent getExtent(int stateIndex) {
        if (stateIndex != 0) {
            return null;
        }
        return this;
    }

    @Override
    public IConcept getDomainConcept() {
        return Time.TIME_DOMAIN;
    }

    @Override
    public IProperty getDomainProperty() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IProperty getCoverageProperty() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ITimePeriod collapse() {
        return this;
    }

    @Override
    public boolean isCovered(int stateIndex) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public IExtent merge(IExtent extent, boolean force) throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Pair<ITopologicallyComparable<?>, Double> checkCoverage(ITopologicallyComparable<?> obj)
            throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isConsistent() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isEmpty() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public long getValueCount() {
        return 1;
    }

    @Override
    public IObserver getObserver() {
        return null;
    }

    @Override
    public boolean isSpatiallyDistributed() {
        return false;
    }

    @Override
    public boolean isTemporallyDistributed() {
        return true;
    }

    @Override
    public boolean isTemporal() {
        return true;
    }

    @Override
    public boolean isSpatial() {
        return false;
    }

    @Override
    public ISpatialExtent getSpace() {
        return null;
    }

    @Override
    public ITemporalExtent getTime() {
        return this;
    }

    @Override
    public IMetadata getMetadata() {
        return null;
    }

    @Override
    public IObservable getObservable() {
        return null;
    }

    @Override
    public long getMultiplicity() {
        return 1;
    }

    @Override
    public ITemporalExtent intersection(IExtent other) throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ITemporalExtent union(IExtent other) throws ThinklabException {
        if (!(other instanceof ITemporalExtent) || !overlaps(other)) {
            return null;
        }
        ITimeInstant otherStart = ((ITemporalExtent) other).getStart();
        ITimeInstant otherEnd = ((ITemporalExtent) other).getStart();
        long start = otherStart.compareTo(getStart()) < 0 ? otherStart.getMillis() : getValue()
                .getStartMillis();
        long end = otherEnd.compareTo(getEnd()) > 0 ? otherEnd.getMillis() : getValue().getEndMillis();
        return new PeriodValue(start, end);
    }

    @Override
    public boolean contains(IExtent other) throws ThinklabException {
        if (!(other instanceof ITemporalExtent)) {
            return false;
        }
        return contains(((ITemporalExtent) other).getStart()) && contains(((ITemporalExtent) other).getEnd());
    }

    @Override
    public boolean overlaps(IExtent other) throws ThinklabException {
        if (!(other instanceof ITemporalExtent)) {
            return false;
        }
        return overlaps((ITemporalExtent) other);
    }

    @Override
    public boolean intersects(IExtent other) throws ThinklabException {
        if (!(other instanceof ITemporalExtent)) {
            return false;
        }
        // TODO
        return false;
    }

    @Override
    public ITopologicallyComparable<IExtent> union(ITopologicallyComparable<?> other)
            throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ITopologicallyComparable<IExtent> intersection(ITopologicallyComparable<?> other)
            throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IScale getScale() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Object getValue(int index) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Iterator<Object> iterator(Index index) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Class<?> getDataClass() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public BitSet getMask() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ITopologicallyComparable<?> getExtent() {
        /*
         * interval implements the temporal semantics agreed for the agent system.
         */
        return new NumericInterval((double) getStart().getMillis(), (double) getEnd().getMillis(), false,
                true);
    }

}
