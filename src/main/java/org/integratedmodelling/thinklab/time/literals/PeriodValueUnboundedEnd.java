package org.integratedmodelling.thinklab.time.literals;

import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.modelling.IExtent;
import org.integratedmodelling.thinklab.api.modelling.ITopologicallyComparable;
import org.integratedmodelling.thinklab.api.time.ITemporalExtent;
import org.integratedmodelling.thinklab.api.time.ITimeInstant;

public class PeriodValueUnboundedEnd extends PeriodValue {

    public PeriodValueUnboundedEnd(long startTime) {
        super(startTime, Long.MAX_VALUE);
    }

    @Override
    public String toString() {
        return "Unbounded time period starting at " + getStart();
    }

    @Override
    public Object clone() {
        return new PeriodValueUnboundedEnd(getValue().getStartMillis());
    }

    @Override
    public boolean contains(ITimeInstant time) {
        return (time.compareTo(getStart()) >= 0);
    }

    @Override
    public boolean contains(long millisInstant) {
        return getStart().getMillis() <= millisInstant;
    }

    @Override
    public boolean endsBefore(ITimeInstant instant) {
        return false;
    }

    @Override
    public boolean endsBefore(ITemporalExtent other) {
        return false;
    }

    @Override
    public boolean overlaps(ITemporalExtent other) {
        if (other instanceof PeriodValueUnboundedEnd) {
            return true; // all unbounded periods eventually overlap prior to infinity
        }
        return contains(other.getEnd());
    }

    @Override
    public IExtent merge(IExtent extent, boolean force) throws ThinklabException {
        // TODO Auto-generated method stub
        return super.merge(extent, force);
    }

    @Override
    public Pair<ITopologicallyComparable<?>, Double> checkCoverage(ITopologicallyComparable<?> obj)
            throws ThinklabException {
        // TODO Auto-generated method stub
        return super.checkCoverage(obj);
    }

    @Override
    public long getValueCount() {
        // TODO Auto-generated method stub
        return super.getValueCount();
    }

    @Override
    public boolean isTemporallyDistributed() {
        // TODO Auto-generated method stub
        return super.isTemporallyDistributed();
    }

    @Override
    public long getMultiplicity() {
        // TODO Auto-generated method stub
        return super.getMultiplicity();
    }

    @Override
    public ITemporalExtent intersection(IExtent other) throws ThinklabException {
        // TODO Auto-generated method stub
        return super.intersection(other);
    }

    @Override
    public ITemporalExtent union(IExtent other) throws ThinklabException {
        ITimeInstant otherStart = ((ITemporalExtent) other).getStart();
        long start = otherStart.compareTo(getStart()) < 0 ? otherStart.getMillis() : getValue()
                .getStartMillis();
        return new PeriodValueUnboundedEnd(start);
    }

    @Override
    public boolean contains(IExtent o) throws ThinklabException {
        // TODO Auto-generated method stub
        return super.contains(o);
    }

    @Override
    public boolean overlaps(IExtent o) throws ThinklabException {
        // TODO Auto-generated method stub
        return super.overlaps(o);
    }

    @Override
    public boolean intersects(IExtent o) throws ThinklabException {
        // TODO Auto-generated method stub
        return super.intersects(o);
    }

    @Override
    public ITopologicallyComparable<IExtent> union(ITopologicallyComparable<?> other)
            throws ThinklabException {
        // TODO Auto-generated method stub
        return super.union(other);
    }

    @Override
    public ITopologicallyComparable<IExtent> intersection(ITopologicallyComparable<?> other)
            throws ThinklabException {
        // TODO Auto-generated method stub
        return super.intersection(other);
    }

}
