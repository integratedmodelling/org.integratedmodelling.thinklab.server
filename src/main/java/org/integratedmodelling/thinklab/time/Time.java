/**
 * TimePlugin.java
 * ----------------------------------------------------------------------------------
 *
 * Copyright (C) 2008 www.integratedmodelling.org
 * Created: Jan 17, 2008
 *
 * ----------------------------------------------------------------------------------
 * This file is part of ThinklabTimePlugin.
 *
 * ThinklabTimePlugin is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * ThinklabTimePlugin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * ----------------------------------------------------------------------------------
 *
 * @copyright 2008 www.integratedmodelling.org
 * @author    Ferdinando Villa (fvilla@uvm.edu)
 * @date      Jan 17, 2008
 * @license   http://www.gnu.org/licenses/gpl.txt GNU General Public License v3
 * @link      http://www.integratedmodelling.org
 **/
package org.integratedmodelling.thinklab.time;

import java.io.File;

import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.ISemanticObject;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.modelling.states.State;

/**
 * @author Ferd, Luke
 * 
 * Will have non-static methods to persist temporal states and the like.
 *
 */
public class Time {

    private static Time _this = null;

    public static final String PLUGIN_ID = "org.integratedmodelling.thinklab.time";

    public static final String CONTINUOUS_TIME_OBSERVABLE_INSTANCE = "time:ContinuousTimeObservableInstance";
    public static final String ABSOLUTE_TIME_OBSERVABLE_INSTANCE = "time:AbsoluteTimeObservableInstance";

    public static final String DATETIME_TYPE_ID = getProperty("DateTimeTypeID", "time:DateTimeValue");
    public static final String TIMERECORD_TYPE_ID = getProperty("TemporalLocationRecordTypeID", "time:TemporalLocationRecord");
    public static final String TEMPORALGRID_TYPE_ID = getProperty("TemporalGridTypeID", "time:RegularTemporalGrid");
    public static final String PERIOD_TYPE_ID = getProperty("PeriodTypeID", "time:PeriodValue");
    public static final String DURATION_TYPE_ID = getProperty("DurationTypeID", "time:DurationValue");

    public static final String STARTS_AT_PROPERTY_ID = getProperty("StartsAtPropertyID", "time:startsAt");
    public static final String ENDS_AT_PROPERTY_ID = getProperty("EndsAtPropertyID", "time:endsAt");
    public static final String STEP_SIZE_PROPERTY_ID = getProperty("StepSizePropertyID", "time:inStepsOf");

    public static final IConcept TIME_GRID_CONCEPT = Thinklab.c(TEMPORALGRID_TYPE_ID);
    public static final IConcept TIME_RECORD_CONCEPT = Thinklab.c(TIMERECORD_TYPE_ID);
    public static final IConcept DATE_TIME_CONCEPT = Thinklab.c(DATETIME_TYPE_ID);
    public static final IConcept DURATION_CONCEPT = Thinklab.c(DURATION_TYPE_ID);
    public static final IConcept TIME_DOMAIN = Thinklab.c(NS.TIME_DOMAIN);

    public static final ISemanticObject<?> absoluteTimeInstance = null;// km.requireInstance(ABSOLUTE_TIME_OBSERVABLE_INSTANCE);
    public static final ISemanticObject<?> continuousTimeInstance = null; // km.requireInstance(CONTINUOUS_TIME_OBSERVABLE_INSTANCE);

    private static String getProperty(String key1, String key2) {
        return Thinklab.get().getProperties().getProperty(key1, key2);
    }

    public static IConcept domainConcept() {
        return TIME_DOMAIN;
    }

    public static Time get() {

        if (_this == null) {
            _this = new Time();
        }
        return _this;
    }

    /**
     * Called upon to persist a temporally distributed, non-spatial state to a 
     * sensible format. Should probably be a CSV file with data in the first column.
     * 
     * @param state
     * @param file
     */
    public void persistState(State state, File file) {
        // TODO Auto-generated method stub

    }
}
