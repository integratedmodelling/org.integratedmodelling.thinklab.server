package org.integratedmodelling.thinklab.time.extents;

import org.integratedmodelling.thinklab.api.time.ITimeInstant;
import org.integratedmodelling.thinklab.time.literals.TimeValue;

public class InfiniteTemporalGridBoundedStart extends InfiniteTemporalGrid {

    private final long startMs;

    public InfiniteTemporalGridBoundedStart(long startMs, long stepMs) {
        super(stepMs);
        this.startMs = startMs;
    }

    @Override
    public ITimeInstant getStart() {
        return new TimeValue(startMs);
    }
}
