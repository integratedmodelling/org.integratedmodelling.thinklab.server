package org.integratedmodelling.thinklab.time.extents;

import java.util.BitSet;
import java.util.Iterator;
import java.util.List;

import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.collections.Triple;
import org.integratedmodelling.exceptions.ThinklabCircularDependencyException;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IProperty;
import org.integratedmodelling.thinklab.api.knowledge.ISemanticObject;
import org.integratedmodelling.thinklab.api.lang.IList;
import org.integratedmodelling.thinklab.api.metadata.IMetadata;
import org.integratedmodelling.thinklab.api.modelling.IExtent;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.IScale;
import org.integratedmodelling.thinklab.api.modelling.IScale.Index;
import org.integratedmodelling.thinklab.api.modelling.ITopologicallyComparable;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.api.space.ISpatialExtent;
import org.integratedmodelling.thinklab.api.time.ITemporalExtent;
import org.integratedmodelling.thinklab.api.time.ITimeDuration;
import org.integratedmodelling.thinklab.api.time.ITimeInstant;
import org.integratedmodelling.thinklab.api.time.ITimePeriod;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.time.Time;
import org.integratedmodelling.thinklab.time.literals.DurationValue;

public class InfiniteTemporalGrid implements ITemporalExtent {

    private final ITimeDuration resolution;

    public InfiniteTemporalGrid(long stepMs) {
        resolution = new DurationValue(stepMs);
    }

    public ITimeDuration getResolution() {
        return resolution;
    }

    @Override
    public int[] getDimensionSizes(boolean rowFirst) {
        return new int[] { (int) getMultiplicity() };
    }

    @Override
    public int[] getDimensionOffsets(int linearOffset, boolean rowFirst) {
        return new int[] { linearOffset };
    }

    @Override
    public ITimeInstant getStart() {
        return null;
    }

    @Override
    public ITimeInstant getEnd() {
        return null;
    }

    @Override
    public boolean isSpatiallyDistributed() {
        return false;
    }

    @Override
    public boolean isTemporallyDistributed() {
        return true;
    }

    @Override
    public ITemporalExtent intersection(IExtent other) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IExtent union(IExtent other) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean contains(IExtent o) throws ThinklabException {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean overlaps(IExtent o) throws ThinklabException {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean intersects(IExtent o) throws ThinklabException {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public ITimePeriod collapse() {
        return null;
    }

    @Override
    public ITemporalExtent getExtent(int index) {
        return null;
    }

    @Override
    public boolean isCovered(int stateIndex) {
        return true;
    }

    @Override
    public IConcept getDomainConcept() {
        return Time.TIME_DOMAIN;
    }

    @Override
    public IObserver getObserver() {
        return null;
    }

    @Override
    public IProperty getDomainProperty() {
        return Thinklab.p(NS.TEMPORAL_EXTENT_PROPERTY);
    }

    @Override
    public boolean isTemporal() {
        return true;
    }

    @Override
    public boolean isSpatial() {
        return false;
    }

    @Override
    public ISpatialExtent getSpace() {
        return null;
    }

    @Override
    public ITemporalExtent getTime() {
        return this;
    }

    @Override
    public boolean isConsistent() {
        return true;
    }

    @Override
    public IProperty getCoverageProperty() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ITemporalExtent merge(IExtent extent, boolean force) throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Pair<ITopologicallyComparable<?>, Double> checkCoverage(ITopologicallyComparable<?> obj) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ITopologicallyComparable<IExtent> union(ITopologicallyComparable<?> other)
            throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ITopologicallyComparable<IExtent> intersection(ITopologicallyComparable<?> other)
            throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public double getCoveredExtent() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public long getMultiplicity() {
        // this is inherited from ITopology, but in this context it means the same thing as 'value count'
        return getValueCount();
    }

    @Override
    public long getValueCount() {
        return INFINITE;
    }

    @Override
    public double asDouble() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public boolean asBoolean() {
        return false;
    }

    @Override
    public int asInteger() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public long asLong() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public float asFloat() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public String asString() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public INamespace getNamespace() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IList getSemantics() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ITimeDuration demote() {
        return resolution;
    }

    @Override
    public IConcept getDirectType() {
        return Time.TIME_DOMAIN;
    }

    @Override
    public boolean is(Object other) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public int getRelationshipsCount() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int getRelationshipsCount(IProperty property) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public ISemanticObject<?> get(IProperty property) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<Pair<IProperty, ISemanticObject<?>>> getRelationships() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<Triple<IProperty, ISemanticObject<?>, Integer>> getCountedRelationships() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<ISemanticObject<?>> getRelationships(IProperty property) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isLiteral() {
        return true; // TODO this is a wild guess
    }

    @Override
    public boolean isConcept() {
        return false;
    }

    @Override
    public boolean isObject() {
        return false;
    }

    @Override
    public boolean isCyclic() {
        return false; // TODO this is a wild guess
    }

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public List<ISemanticObject<?>> getSortedRelationships(IProperty property)
            throws ThinklabCircularDependencyException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IMetadata getMetadata() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IObservable getObservable() {
        return null;
    }

    @Override
    public IScale getScale() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Object getValue(int index) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Iterator<Object> iterator(Index index) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Class<?> getDataClass() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public BitSet getMask() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ITopologicallyComparable<?> getExtent() {
        // TODO Auto-generated method stub
        return null;
    }
}
