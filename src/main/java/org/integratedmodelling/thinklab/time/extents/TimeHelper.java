package org.integratedmodelling.thinklab.time.extents;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.time.ITemporalExtent;
import org.integratedmodelling.thinklab.api.time.ITemporalSeries;
import org.integratedmodelling.thinklab.api.time.ITimeInstant;
import org.integratedmodelling.thinklab.api.time.ITimePeriod;
import org.integratedmodelling.thinklab.modelling.lang.agents.TemporalSeries;
import org.integratedmodelling.thinklab.time.literals.PeriodValue;

public class TimeHelper {
    public static ITimeInstant max(ITimeInstant t1, ITimeInstant t2) {
        if (t1.compareTo(t2) > 0) {
            return t1;
        }
        return t2;
    }

    public static ITimeInstant min(ITimeInstant t1, ITimeInstant t2) {
        if (t1.compareTo(t2) < 0) {
            return t1;
        }
        return t2;
    }

    public static ITemporalExtent getConstrainedTemporalExtent(ITemporalExtent extentToConstrain,
            ITemporalExtent constrainingExtent) throws ThinklabException {
        ITemporalExtent result;
        if (extentToConstrain == null) {
            result = constrainingExtent.collapse();
        } else if (constrainingExtent == null) {
            result = extentToConstrain;
        } else {
            ITimePeriod collapsedRootScale = constrainingExtent.collapse();

            // get the start/end of the result extent. As long as they are not null, they will have
            // meaningful values (INFINITE == very big number, NEGATIVE_INFINITE == very small number)
            ITimeInstant startTime = max(extentToConstrain.getStart(), collapsedRootScale.getStart());
            ITimeInstant endTime = min(extentToConstrain.getEnd(), collapsedRootScale.getEnd());

            // initialize a single time period covering the whole result extent
            ITemporalSeries<Object> newTemporalSeries = new TemporalSeries<Object>();
            newTemporalSeries.put(new PeriodValue(startTime.getMillis(), endTime.getMillis()), new Object());

            for (int i = 0; i < extentToConstrain.getValueCount(); i++) {
                ITimeInstant pStart = extentToConstrain.getExtent(i).getStart();
                if (pStart.compareTo(endTime) >= 0) {
                    //we're outside the range of our overall time period, so stop processing.
                    break;
                }
                if (pStart.compareTo(startTime) > 0) {
                    // bisect the appropriate time segment in the Temporal Scale
                    newTemporalSeries.bisect(pStart, new Object());
                }
            }
            result = newTemporalSeries;
        }

        return result;
    }
}
