/**
 * IrregularEventSequence.java
 * ----------------------------------------------------------------------------------
 *
 * Copyright (C) 2008 www.integratedmodelling.org
 * Created: Jan 17, 2008
 *
 * ----------------------------------------------------------------------------------
 * This file is part of ThinklabTimePlugin.
 *
 * ThinklabTimePlugin is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * ThinklabTimePlugin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * ----------------------------------------------------------------------------------
 *
 * @copyright 2008 www.integratedmodelling.org
 * @author    Ferdinando Villa (fvilla@uvm.edu)
 * @date      Jan 17, 2008
 * @license   http://www.gnu.org/licenses/gpl.txt GNU General Public License v3
 * @link      http://www.integratedmodelling.org
 **/
package org.integratedmodelling.thinklab.time.extents;

import java.util.BitSet;

import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IProperty;
import org.integratedmodelling.thinklab.api.modelling.IExtent;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.IScale;
import org.integratedmodelling.thinklab.api.modelling.ITopologicallyComparable;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.api.space.ISpatialExtent;
import org.integratedmodelling.thinklab.api.time.ITemporalExtent;
import org.integratedmodelling.thinklab.api.time.ITimeInstant;
import org.integratedmodelling.thinklab.api.time.ITimePeriod;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.modelling.states.State;
import org.integratedmodelling.thinklab.time.Time;

public class IrregularEventSequence extends State implements ITemporalExtent {

    protected IrregularEventSequence(IObservable observable) {
        super(observable);
        // TODO Auto-generated constructor stub
    }

    @Override
    public Object getValue(int offset) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int[] getDimensionSizes(boolean rowFirst) {
        return new int[] { (int) getMultiplicity() };
    }

    @Override
    public int[] getDimensionOffsets(int linearOffset, boolean rowFirst) {
        return new int[] { linearOffset };
    }

    @Override
    public Object demote() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getValueCount() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public boolean isSpatiallyDistributed() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isTemporallyDistributed() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public long getMultiplicity() {
        // TODO Auto-generated method stub
        return 1L;
    }

    @Override
    public ITemporalExtent intersection(IExtent other) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IExtent union(IExtent other) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean contains(IExtent o) throws ThinklabException {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean overlaps(IExtent o) throws ThinklabException {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean intersects(IExtent o) throws ThinklabException {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public ITimePeriod collapse() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ITemporalExtent getExtent(int stateIndex) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isCovered(int stateIndex) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public IConcept getDomainConcept() {
        return Time.TIME_DOMAIN;
    }

    @Override
    public IObserver getObserver() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IProperty getDomainProperty() {
        return Thinklab.p(NS.TEMPORAL_EXTENT_PROPERTY);
    }

    @Override
    public boolean isTemporal() {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public boolean isSpatial() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public ISpatialExtent getSpace() {
        return null;
    }

    @Override
    public ITemporalExtent getTime() {
        return this;
    }

    @Override
    public boolean isConsistent() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public IProperty getCoverageProperty() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IExtent merge(IExtent extent, boolean force) throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Pair<ITopologicallyComparable<?>, Double> checkCoverage(ITopologicallyComparable<?> obj) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ITopologicallyComparable<IExtent> union(ITopologicallyComparable<?> other)
            throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ITopologicallyComparable<IExtent> intersection(ITopologicallyComparable<?> other)
            throws ThinklabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public double getCoveredExtent() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public Class<?> getDataClass() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isEmpty() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public ITimeInstant getStart() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ITimeInstant getEnd() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IScale getScale() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public BitSet getMask() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ITopologicallyComparable<?> getExtent() {
        // TODO Auto-generated method stub
        return null;
    }
}
