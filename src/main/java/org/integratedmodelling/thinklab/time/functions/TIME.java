package org.integratedmodelling.thinklab.time.functions;

import java.util.Map;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IExpression;
import org.integratedmodelling.thinklab.api.project.IProject;
import org.integratedmodelling.thinklab.api.time.ITemporalExtent;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.interfaces.annotations.Function;
import org.integratedmodelling.thinklab.time.extents.InfiniteTemporalGrid;
import org.integratedmodelling.thinklab.time.extents.InfiniteTemporalGridBoundedStart;
import org.integratedmodelling.thinklab.time.extents.RegularTemporalGrid;
import org.integratedmodelling.thinklab.time.literals.DurationValue;
import org.integratedmodelling.thinklab.time.literals.PeriodValue;
import org.integratedmodelling.thinklab.time.literals.TimeValue;
import org.joda.time.DateTime;

@Function(
        id = "time",
        parameterNames = { "start", "end", "duration", "resolution" },
        returnTypes = { NS.TIME_DOMAIN })
public class TIME implements IExpression {

    /*
     * example: observe ... over time( start=2013-01-01, end=2014-01-01, resolution="1 d" );
     */
    @Override
    public ITemporalExtent eval(Map<String, Object> parameters, IConcept... context) throws ThinklabException {
        ITemporalExtent result = null;
        TimeValue start = null;
        TimeValue end = null;

        /*
         * support simple year specification without start/end
         */
        if (parameters.containsKey("year")) {
            int year = (int) Double.parseDouble(parameters.get("year").toString());
            start = new TimeValue(new DateTime(year + "-01-01"));
            end = new TimeValue(new DateTime((year + 1) + "-01-01"));
        } else {
            start = getInstant("start", parameters);
            end = getInstant("end", parameters);
        }

        DurationValue duration = getDuration("duration", parameters);
        DurationValue resolution = getDuration("resolution", parameters);

        if (end == null && duration == null) {
            // unbounded time period (simulation will have to set the limits)
            if (start == null) {
                // unbounded start & end
                result = new InfiniteTemporalGrid(resolution.getMilliseconds());
            } else {
                // unbounded end
                result = new InfiniteTemporalGridBoundedStart(start.getMillis(), resolution.getMilliseconds());
            }
        } else {
            // guaranteed either end != null or duration != null

            if (start == null && (end == null || duration == null)) {
                // not anchored in time. Default to "starting right now"
                // TODO this doesn't make sense - most raw data will be in the past, meaning this assumption
                // forces coverage == 0.
                start = new TimeValue();
            }

            // guaranteed at most one of start, end, duration are null

            if (start == null) {
                start = new TimeValue(end.getMillis() - duration.getMilliseconds());
            } else if (end == null) {
                end = new TimeValue(start.getMillis() + duration.getMilliseconds());
            } else if (duration == null) {
                duration = new DurationValue(end.getMillis() - start.getMillis());
            }

            // guaranteed start, end, duration are all non-null

            if (duration.getMilliseconds() != end.getMillis() - start.getMillis()) {
                throw new ThinklabValidationException(
                        "'start', 'end', and 'duration' were all given for time(), and their values were not compatible.");
            }

            if (start.compareTo(end) > 0) {
                throw new ThinklabValidationException(
                        "'start' must be before 'end' (got a negative duration) for time().");
            }

            if (resolution == null || resolution.getMilliseconds() >= duration.getMilliseconds()) {
                // only 1 period to generate, so create a PeriodValue
                result = new PeriodValue(start.getMillis(), end.getMillis());
            } else {
                // multiple periods to generate
                result = new RegularTemporalGrid(start, end, resolution.getMilliseconds());
            }
        }

        return result;
    }

    private DurationValue getDuration(String key, Map<String, Object> parameters) {
        DurationValue result = null;
        String parameter = getParameterOrNull(key, parameters);
        if (parameter != null) {
            try {
                result = new DurationValue(parameter);
            } catch (ThinklabValidationException e) {
                // bad formatting. leave result null.
                /*
                 * FIXME actually this should throw an exception (but in the client, which is not using
                 * actual function implementations at the moment).
                 */
                e.printStackTrace();
            }
        }
        return result;
    }

    private TimeValue getInstant(String key, Map<String, Object> parameters)
            throws ThinklabValidationException {
        TimeValue result = null;
        String param = getParameterOrNull(key, parameters);
        if (param != null) {
            result = new TimeValue(param);
            // DateTime dt = new DateTime(result.getMillis());
        }
        return result;
    }

    private String getParameterOrNull(String key, Map<String, Object> parameters) {
        Object ret = parameters.containsKey(key) ? parameters.get(key) : null;
        if (ret instanceof Double) {
            // we don't need doubles anywhere, but if we have numbers (e.g. for years), we want them in int
            // format.
            ret = ((Double) ret).intValue();
        }
        return ret == null ? null : ret.toString();
    }

    @Override
    public void setProjectContext(IProject project) {
        // TODO Auto-generated method stub

    }

}
