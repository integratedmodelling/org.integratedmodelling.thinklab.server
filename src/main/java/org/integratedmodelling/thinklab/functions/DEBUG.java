package org.integratedmodelling.thinklab.functions;

import java.util.Map;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IExpression;
import org.integratedmodelling.thinklab.api.listeners.INotification;
import org.integratedmodelling.thinklab.api.project.IProject;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.interfaces.annotations.Function;

@Function(id = "system.debugging", parameterNames = {}, returnTypes = { NS.BOOLEAN })
public class DEBUG implements IExpression {

    @Override
    public Object eval(Map<String, Object> parameters, IConcept... context) throws ThinklabException {
        return Thinklab.get().getNotificationLevel() >= INotification.DEBUG;
    }

    @Override
    public void setProjectContext(IProject project) {
    }

}
