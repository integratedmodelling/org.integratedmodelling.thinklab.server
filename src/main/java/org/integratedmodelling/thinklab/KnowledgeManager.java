/**
 * Copyright 2011 The ARIES Consortium (http://www.ariesonline.org) and
 * www.integratedmodelling.org. 

   This file is part of Thinklab.

   Thinklab is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published
   by the Free Software Foundation, either version 3 of the License,
   or (at your option) any later version.

   Thinklab is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Thinklab.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.integratedmodelling.thinklab;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabIOException;
import org.integratedmodelling.exceptions.ThinklabInternalErrorException;
import org.integratedmodelling.exceptions.ThinklabResourceNotFoundException;
import org.integratedmodelling.list.ReferenceList;
import org.integratedmodelling.thinklab.annotation.AnnotationFactory;
import org.integratedmodelling.thinklab.api.factories.IKnowledgeFactory;
import org.integratedmodelling.thinklab.api.knowledge.IAuthority;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IOntology;
import org.integratedmodelling.thinklab.api.knowledge.IProperty;
import org.integratedmodelling.thinklab.api.knowledge.ISemanticObject;
import org.integratedmodelling.thinklab.api.knowledge.kbox.IKbox;
import org.integratedmodelling.thinklab.api.lang.IList;
import org.integratedmodelling.thinklab.api.lang.IModelResolver;
import org.integratedmodelling.thinklab.api.lang.IReferenceList;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.api.modelling.resolution.IObservationKbox;
import org.integratedmodelling.thinklab.command.CommandManager;
import org.integratedmodelling.thinklab.common.authority.AuthorityFactory;
import org.integratedmodelling.thinklab.common.interfaces.IOWLManager;
import org.integratedmodelling.thinklab.common.owl.OWL;
import org.integratedmodelling.thinklab.common.owl.Ontology;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.kbox.neo4j.NeoKBox;
import org.integratedmodelling.thinklab.modelling.ModelManager;
import org.integratedmodelling.thinklab.modelling.resolver.ObservationKbox;
import org.integratedmodelling.thinklab.resources.Resources;

/**
 * Main knowledge manager functionalities. Thinklab holds one of these and proxies all knowledge
 * operations to it.
 * 
 * @author Ferd
 * 
 */
public class KnowledgeManager implements IKnowledgeFactory, IOWLManager {

    protected CommandManager       _commandManager    = new CommandManager();

    protected AnnotationFactory    _annotationFactory = new AnnotationFactory();

    private HashMap<String, IKbox> _kboxes            = new HashMap<String, IKbox>();

    private OWL                    _manager;

    /*
     * the resolver for core ontologies
     */
    private IModelResolver         _resolver;

    protected KnowledgeManager(IModelResolver resolver) {
        _resolver = resolver;
    }

    public void initialize() throws ThinklabException {

        // Thinklab.get().logger.info("loading knowledge from "
        // + Env.CONFIG.getLoadPath(IServer.KNOWLEDGE_STORAGE_AREA) + " ...");
        //
        try {
            _manager = new OWL(_resolver, Resources.getKnowledgePath());
        } catch (Exception e) {
            throw new ThinklabIOException(e);
        }

        /*
         * declare namespaces for each core ontology
         */
        int nOnt = 0;
        for (INamespace o : _manager.getNamespaces()) {
            ((ModelManager) (Thinklab.get().getModelManager())).addNamespace(o);
            nOnt++;
        }

        Thinklab.get().logger.info("loaded " + nOnt + " core ontologies");

        /*
         * register common XSD types
         */
        _manager.registerDatatypeMapping("http://www.w3.org/2001/XMLSchema#Name", requireConcept(NS.TEXT));
        _manager.registerDatatypeMapping("http://www.w3.org/2001/XMLSchema#Literal", requireConcept(NS.TEXT));
        _manager.registerDatatypeMapping("http://www.w3.org/2001/XMLSchema#anyURI", requireConcept(NS.TEXT));
        _manager.registerDatatypeMapping("http://www.w3.org/2001/XMLSchemaPlainLiteral",
                requireConcept(NS.TEXT));
        _manager.registerDatatypeMapping("http://www.w3.org/2001/XMLSchema#decimal",
                requireConcept(NS.DOUBLE));
        _manager.registerDatatypeMapping("http://www.w3.org/2001/XMLSchema#boolean",
                requireConcept(NS.BOOLEAN));
        _manager.registerDatatypeMapping("http://www.w3.org/2001/XMLSchema#language", requireConcept(NS.TEXT));
        _manager.registerDatatypeMapping("http://www.w3.org/2001/XMLSchema#integer",
                requireConcept(NS.INTEGER));
        _manager.registerDatatypeMapping("http://www.w3.org/2001/XMLSchema#int", requireConcept(NS.INTEGER));
        _manager.registerDatatypeMapping("http://www.w3.org/2001/XMLSchema#negativeInteger",
                requireConcept(NS.INTEGER));
        _manager.registerDatatypeMapping("http://www.w3.org/2001/XMLSchema#nonNegativeInteger",
                requireConcept(NS.INTEGER));
        _manager.registerDatatypeMapping("http://www.w3.org/2001/XMLSchema#long", requireConcept(NS.LONG));
        _manager.registerDatatypeMapping("http://www.w3.org/2001/XMLSchema#float", requireConcept(NS.FLOAT));
        _manager.registerDatatypeMapping("http://www.w3.org/2001/XMLSchema#real", requireConcept(NS.DOUBLE));
        _manager.registerDatatypeMapping("http://www.w3.org/2001/XMLSchema#double", requireConcept(NS.DOUBLE));
        _manager.registerDatatypeMapping("http://www.w3.org/2001/XMLSchema#string", requireConcept(NS.TEXT));
        _manager.registerDatatypeMapping("http://www.w3.org/2001/XMLSchema#shape", requireConcept(NS.POLYGON));
        _manager.registerDatatypeMapping("http://www.w3.org/2001/XMLSchema#float", requireConcept(NS.FLOAT));
        _manager.registerDatatypeMapping("http://www.w3.org/2001/XMLSchema#short", requireConcept(NS.INTEGER));
        _manager.registerDatatypeMapping("http://www.w3.org/2001/XMLSchema#unsignedShort",
                requireConcept(NS.INTEGER));
        _manager.registerDatatypeMapping("http://www.w3.org/2001/XMLSchema#unsignedInt",
                requireConcept(NS.INTEGER));
        _manager.registerDatatypeMapping("http://www.w3.org/2001/XMLSchema#unsignedByte",
                requireConcept(NS.INTEGER));
        _manager.registerDatatypeMapping("http://www.w3.org/2001/XMLSchema#unsignedLong",
                requireConcept(NS.LONG));
        _manager.registerDatatypeMapping("http://www.integratedmodelling.org/ks/geospace/geospace.owl#line",
                requireConcept(NS.LINE));
        _manager.registerDatatypeMapping(
                "http://www.integratedmodelling.org/ks/geospace/geospace.owl#polygon",
                requireConcept(NS.POLYGON));
        _manager.registerDatatypeMapping("http://www.integratedmodelling.org/ks/geospace/geospace.owl#point",
                requireConcept(NS.POINT));

    }

    @Override
    public ISemanticObject<?> parse(String literal, IConcept concept) throws ThinklabException {
        return _annotationFactory.parse(literal, concept);
    }

    @Override
    public ISemanticObject<?> annotate(Object object) throws ThinklabException {
        return _annotationFactory.annotate(object);
    }

    @Override
    public Object instantiate(IList semantics) throws ThinklabException {

        if (!(semantics instanceof IReferenceList)) {
            semantics = ReferenceList.list(semantics.toArray());
        }
        return _annotationFactory.instantiate((IReferenceList) semantics);
    }

    public IReferenceList conceptualize(Object object) throws ThinklabException {
        return _annotationFactory.conceptualize(object);
    }

    @Override
    public IConcept getLeastGeneralCommonConcept(IConcept... cc) {
        return getLeastGeneralCommonConcept(Arrays.asList(cc));
    }

    @Override
    public IKbox createKbox(String uri) throws ThinklabException {

        IKbox ret = null;
        if (!uri.contains("://")) {
            File kf = new File(Thinklab.get().getScratchArea("kbox") + File.separator + uri);
            kf.mkdirs();
            try {
                ret = new NeoKBox(kf.toURI().toURL().toString());
                ret.open();
                _kboxes.put(uri, ret);
            } catch (MalformedURLException e) {
                throw new ThinklabInternalErrorException(e);
            }
        }

        return ret;
    }

    /**
     * FIXME - TEMPORARY for testing; should replace createKBox when testing is
     * complete.
     * 
     * @param uri
     * @return
     * @throws ThinklabException
     */
    public IKbox createObservationKbox(String uri) throws ThinklabException {

        IKbox ret = null;
        if (!uri.contains("://")) {
            File kf = new File(Thinklab.get().getScratchArea("kbox") + File.separator + uri);
            kf.mkdirs();
            try {
                ret = new ObservationKbox(kf.toURI().toURL().toString());
                ret.open();
                _kboxes.put(uri, ret);
            } catch (MalformedURLException e) {
                throw new ThinklabInternalErrorException(e);
            }
        }

        return ret;
    }

    /**
     * FIXME - TEMPORARY for testing; should replace requireKBox when testing is
     * complete.
     * 
     * @param uri
     * @return
     * @throws ThinklabException
     */
    public IObservationKbox requireObservationKbox(String uri) throws ThinklabException {

        IKbox ret = null;
        if (_kboxes.containsKey(uri))
            ret = _kboxes.get(uri);
        else
            ret = createObservationKbox(uri);
        return (IObservationKbox) ret;
    }

    @Override
    public void dropKbox(String uri) throws ThinklabException {

        if (_kboxes.containsKey(uri)) {
            IKbox kbox = _kboxes.get(uri);
            kbox.clear();
        }
        _kboxes.remove(uri);
    }

    @Override
    public IObservationKbox requireKbox(String uri) throws ThinklabException {

        return requireObservationKbox(uri);
        //
        // IKbox ret = null;
        // if (_kboxes.containsKey(uri))
        // ret = _kboxes.get(uri);
        // else
        // ret = createKbox(uri);
        // return ret;
    }

    @Override
    public IProperty getProperty(String prop) {
        return _manager.getProperty(prop);
    }

    @Override
    public IConcept getConcept(String conc) {
        return _manager.getConcept(conc);
    }

    public void shutdown() {

        for (IKbox kbox : _kboxes.values()) {

            /*
             * TODO if kbox properties contains a temporary tag, clear it before closing.
             */

            kbox.close();
        }

        /* TODO any other cleanup actions */

        _annotationFactory = null;
        _commandManager = null;
        _kboxes.clear();
    }

    public IConcept requireConcept(String id) throws ThinklabException {
        IConcept ret = getConcept(id);
        if (ret == null) {
            throw new ThinklabResourceNotFoundException("concept " + id + " is unknown");
        }
        return ret;
    }

    public IProperty requireProperty(String id) throws ThinklabException {
        IProperty ret = getProperty(id);
        if (ret == null) {
            throw new ThinklabResourceNotFoundException("property " + id + " is unknown");
        }
        return ret;
    }

    public IConcept getLeastGeneralCommonConcept(String semanticType, String otherConcept)
            throws ThinklabException {
        return getLeastGeneralCommonConcept(requireConcept(semanticType), requireConcept(otherConcept));
    }

    public IConcept getLeastGeneralCommonConcept(IConcept concept1, IConcept c) {
        return concept1.getLeastGeneralCommonConcept(c);
    }

    public IConcept getLeastGeneralCommonConcept(Collection<IConcept> cc) {

        IConcept ret = null;
        Iterator<IConcept> ii = cc.iterator();

        if (ii.hasNext()) {

            ret = ii.next();

            if (ret != null)
                while (ii.hasNext()) {
                    ret = ret.getLeastGeneralCommonConcept(ii.next());
                    if (ret == null)
                        break;
                }
        }

        return ret;
    }

    public CommandManager getCommandManager() {
        return _commandManager;
    }

    public void registerAnnotation(Class<?> clls, String value) throws ThinklabException {
        _annotationFactory.registerAnnotationConcept(requireConcept(value), clls);
    }

    public void registerLiteralAnnotation(Class<?> clls, String concept, String datatype, Class<?> javaClass)
            throws ThinklabException {
        _annotationFactory.registerLiteralAnnotation(clls, requireConcept(concept), datatype, javaClass);
    }

    @Override
    public void registerAnnotatedClass(Class<?> cls, IConcept concept) {
        _annotationFactory.registerAnnotationConcept(concept, cls);
    }

    public boolean isJavaLiteralClass(Class<?> cls) {
        return _annotationFactory.isJavaLiteralClass(cls);
    }

    public boolean isLiteralConcept(IConcept concept) {
        return _annotationFactory.isLiteralConcept(concept);
    }

    public ISemanticObject<?> getSemanticObject(IReferenceList list, Object object) {
        return _annotationFactory.getSemanticObject(list, object);
    }

    public IConcept getLiteralConceptForJavaClass(Class<? extends Object> class1) {
        return _annotationFactory.getLiteralConceptForJavaClass(class1);
    }

    public ISemanticObject<?> getSemanticLiteral(IReferenceList semantics) {
        return _annotationFactory.getSemanticLiteral(semantics);
    }

    @Override
    public ISemanticObject<?> entify(IList semantics) throws ThinklabException {
        if (!(semantics instanceof IReferenceList)) {
            semantics = ReferenceList.list(semantics.toArray());
        }
        return _annotationFactory.entify((IReferenceList) semantics);
    }

    @Override
    public IConcept getXSDMapping(String string) {
        return _annotationFactory.getXSDMapping(string);
    }

    @Override
    public IOntology refreshOntology(URL url, String name) throws ThinklabException {
        return _manager.refreshOntology(url, name);
    }

    @Override
    public void releaseOntology(String s) {
        _manager.releaseOntology(getOntology(s));
    }

    @Override
    public void releaseAllOntologies() {
        _manager.clear();
    }

    @Override
    public IOntology getOntology(String ontName) {
        return _manager.getOntology(ontName);
    }

    @Override
    public Collection<IOntology> getOntologies(boolean includeInternal) {
        return _manager.getOntologies(includeInternal);
    }

    @Override
    public IOntology createOntology(String id, String ontologyPrefix) throws ThinklabException {
        return _manager.requireOntology(id, ontologyPrefix);
    }

    @Override
    public Collection<IConcept> getRootConcepts() {

        ArrayList<IConcept> ret = new ArrayList<IConcept>();
        for (IOntology onto : getOntologies(true)) {
            for (IConcept c : onto.getConcepts()) {
                Collection<IConcept> pp = c.getParents();
                if (pp.size() == 0 || (pp.size() == 1 && pp.iterator().next().is(getRootConcept())))
                    ret.add(c);
            }
        }
        return ret;

    }

    @Override
    public Collection<IConcept> getConcepts() {
        ArrayList<IConcept> ret = new ArrayList<IConcept>();
        for (IOntology onto : getOntologies(true)) {
            for (IConcept c : onto.getConcepts()) {
                ret.add(c);
            }
        }
        return ret;
    }

    @Override
    public IConcept getRootConcept() {
        return _manager.getRootConcept();
    }

    @Override
    public File exportOntology(String ontologyId) throws ThinklabException {

        IOntology ontology = getOntology(ontologyId);

        if (ontology == null)
            return null;

        if (((Ontology) ontology).getResourceUrl() != null) {
            try {
                URL url = new URL(((Ontology) ontology).getResourceUrl());
                if (url.getProtocol().startsWith("file")) {
                    return new File(url.getFile());
                }
            } catch (MalformedURLException e) {
                // just move on
            }
        }

        File ret;
        try {
            ret = File.createTempFile("ont", "owl");
        } catch (IOException e) {
            throw new ThinklabIOException(e);
        }

        if (!ontology.write(ret.toString()))
            return null;

        return ret;
    }

    public void extractCoreOntologies(File tf) throws ThinklabIOException {
        _manager.extractCoreOntologies(tf);
    }

    @Override
    public OWL getOWLManager() {
        return _manager;
    }

    @Override
    public INamespace getCoreNamespace(String ns) {
        return _manager.getNamespace(ns);
    }

    public IOntology requireOntology(String id, String ontologyNamespacePrefix) {
        return _manager.requireOntology(id, ontologyNamespacePrefix);
    }

    @Override
    public IAuthority getAuthority(String id) {
        return AuthorityFactory.get().getAuthority(id);
    }

    @Override
    public IOntology requireOntology(String id) {
        return requireOntology(id, "http://integratedmodelling.org/ks");
    }

}
