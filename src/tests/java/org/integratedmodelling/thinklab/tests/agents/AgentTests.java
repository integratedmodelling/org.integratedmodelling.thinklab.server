package org.integratedmodelling.thinklab.tests.agents;

import static org.junit.Assert.fail;

import java.io.File;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.api.modelling.resolution.IResolutionStrategy;
import org.integratedmodelling.thinklab.api.time.ITimePeriod;
import org.integratedmodelling.thinklab.debug.CallTracer;
import org.integratedmodelling.thinklab.modelling.resolver.ProvenanceGraph;
import org.integratedmodelling.thinklab.runtime.Context;
import org.integratedmodelling.thinklab.runtime.Session;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class AgentTests {

    private static final String PROJECT_DIRECTORY = System.getenv("AGENT_TESTS_PROJECT_DIRECTORY");
    private static Thinklab     thinklab;
    private static Session      session;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        Thinklab.boot();
        thinklab = Thinklab.get();

        CallTracer.indent("Loading project directory " + PROJECT_DIRECTORY + "...");
        thinklab.registerProjectDirectory(new File(PROJECT_DIRECTORY));
        CallTracer.unIndent();

        CallTracer.msg("Calling Thinklab.load()...");
        thinklab.load(true, null);
        session = new Session();
    }

    private static Context getSubjectProxy() {
        return new Context(session, session.getSessionMonitor(0), "test");
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        Thinklab.shutdown();
    }

    @Test
    public void testFail() {
        fail("Failure test");
    }

    @Test
    public void testSuccess() {
        thinklab.info("Empty test - should succeed...");
    }

    @Test
    public void seattleTest2013() throws ThinklabException {
        agentTest("seattle-2013");
    }

    @Test
    public void seattleTestAtemporal() throws ThinklabException {
        // agentTest("seattle");
    }

    @Test
    public void westernWashingtonTestAtemporal() throws ThinklabException {
        // agentTest("western-washington");
    }

    private void agentTest(String model) throws ThinklabException {
        CallTracer.msg("\n========== Starting agent test for model '" + model + "'... ==========");

        CallTracer.indent("getSubjectProxy(seattle)");
        Context seattle = getSubjectProxy();
        CallTracer.unIndent();

        CallTracer.indent("\nobserve(thinklab.agents.sandbox.locations." + model + ")");
        seattle = (Context) seattle.observe("thinklab.agents.sandbox.locations." + model);
        CallTracer.unIndent();

        CallTracer.indent("\nwith(thinklab.agents.sandbox.agents.farmer)");
        seattle = (Context) seattle.observe("thinklab.agents.sandbox.agents.farmer");
        CallTracer.unIndent();

        CallTracer.dump(System.out);

        dumpResolutionStrategy(seattle);
        dumpSubjectTree(seattle);
        ITimePeriod simulationTimePeriod = seattle.getSubject().getScale().getTime()
                .collapse();
        seattle.getCausalGraph().dump(System.out, simulationTimePeriod);
    }

    private void dumpResolutionStrategy(Context seattle) {
        CallTracer.indent("Dumping Resolution Strategy...");
        IResolutionStrategy strategy = seattle.getSubject().getResolutionStrategy();

        CallTracer.indent(strategy.toString());
        ProvenanceGraph provenance;
        for (int i = 0; i < strategy.getStepCount(); i++) {
//            provenance = (ProvenanceGraph) strategy.getProvenance(i);
//            CallTracer.indent("Provenance step " + Integer.toString(i) + ":");
//            CallTracer.msg(provenance.dump());
//            CallTracer.unIndent();
        }
        CallTracer.unIndent();

        CallTracer.unIndent();
        System.out.println("-----");
        CallTracer.dump(System.out);
    }

    private void dumpSubjectTree(Context seattle) {
        CallTracer.indent("Dumping subject dependency graph...");
        dumpSubjectTree(seattle);
        CallTracer.unIndent();
        System.out.println("-----");
        CallTracer.dump(System.out);
    }

    private void dumpSubjectTree(ISubject rootSubject) {
        CallTracer.indent("", rootSubject);
        for (ISubject dependency : rootSubject.getSubjects()) {
            dumpSubjectTree(dependency);
        }
        CallTracer.unIndent();
    }
}
