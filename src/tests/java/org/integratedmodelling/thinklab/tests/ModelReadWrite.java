package org.integratedmodelling.thinklab.tests;

import java.io.File;
import java.net.URL;
import java.util.HashSet;

import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.knowledge.ISemanticObject;
import org.integratedmodelling.thinklab.api.knowledge.kbox.IKbox;
import org.integratedmodelling.thinklab.api.knowledge.query.IQuery;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.query.Queries;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class ModelReadWrite {

    static INamespace _ns;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        Thinklab.boot();
        URL test1 = ClassLoader.getSystemResource("org/integratedmodelling/thinklab/tests/tql/test1.tql");
        _ns = Thinklab.get().loadFile(new File(test1.getFile()), null, null, true, null,
                new HashSet<String>());
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        Thinklab.shutdown();
    }

    @Test
    public void testKbox() throws Exception {

        IKbox kbox = Thinklab.get().requireKbox(Thinklab.DEFAULT_KBOX);

        /*
         * find models that observe rainfall
         */
        IQuery query = Queries.select(NS.MODEL).restrict(NS.HAS_OBSERVABLE,
                Queries.select("habitat:Rainfall"));

        System.out.println("Models observing rainfall:");
        for (ISemanticObject<?> o : kbox.query(query)) {
            System.out.println(o);
        }

        /*
         * TODO assert something
         */

    }

    @Test
    public void observe1() throws Exception {
        //
        //		ISubjectGenerator ctx = (ISubjectGenerator)_ns.getModelObject("puget");
        //		ISubject result = Thinklab.get().observe(ctx);
        //		
        //		NetCDFDataset ncds = new NetCDFDataset(result);
        //		ncds.write("observe1.nc");
        //		
        //		System.out.println(result);

        /*
         * TODO assert something or compare datasets
         */
    }

}
