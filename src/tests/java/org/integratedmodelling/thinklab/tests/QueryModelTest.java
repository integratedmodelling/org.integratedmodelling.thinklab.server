package org.integratedmodelling.thinklab.tests;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.ISemanticObject;
import org.integratedmodelling.thinklab.api.knowledge.kbox.IKbox;
import org.integratedmodelling.thinklab.api.knowledge.query.IQuery;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.api.modelling.resolution.IResolutionContext;
import org.integratedmodelling.thinklab.common.vocabulary.NS;
import org.integratedmodelling.thinklab.common.vocabulary.Observable;
import org.integratedmodelling.thinklab.geospace.literals.ShapeValue;
import org.integratedmodelling.thinklab.modelling.monitoring.Monitor;
import org.integratedmodelling.thinklab.modelling.resolver.ModelData;
import org.integratedmodelling.thinklab.modelling.resolver.ObservationQueries;
import org.integratedmodelling.thinklab.modelling.resolver.ResolutionContext;
import org.integratedmodelling.thinklab.query.Queries;
import org.integratedmodelling.thinklab.runtime.Session;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

// AFRICA
//observe Region new-region over space(
//	    grid = "1000 m", 
//	    shape = "EPSG:4326 POLYGON((-6.503995449922083 36.52766684957048,9.316317050077895 38.75444432272052,25.312410800077952 33.94374402669498,33.92569205007812 32.91687399868528,44.47256705007835 13.325935397090706,45.17569205007812 12.469212201490341,52.031160800078396 13.838529475223318,51.15225455007756 6.05362169107499,43.76944205007767 -3.074232761851319,42.53897330007829 -9.881819385656149,52.38272330007784 -9.881819385656149,50.09756705007834 -28.844268144265655,36.91397330007828 -25.085179589567577,30.410067050078346 -33.06353619373946,22.499910800077952 -35.38867252944525,16.69912955007778 -35.5318494614218,12.831942050077668 -28.381327715301044,8.261629550077961 -13.666888391998682,10.195223300077836 -6.751436698677438,6.152254550077916 -1.317780202459526,5.62491080007795 2.5484503905538815,-1.933682949922016 2.19718987834508,-9.66805794992233 2.5484503905538815,-18.2813391999216 10.228892884906615,-19.51180794992188 18.56338633260285,-17.92977669992216 27.29410065672155,-6.503995449922083 36.52766684957048))"
//	);
// ASIA
//observe Region new-region over space(
//	    grid = "1000 m", 
//	    shape = "EPSG:4326 POLYGON((31.07285476011994 70.60907313650104,28.963479760119718 66.2271580444994,26.326761010119885 57.97915277647183,30.545511010119437 51.9376892540352,35.643167260119824 49.603797778085216,38.10410476011949 48.33455577236872,37.40097976011971 45.32920349312135,26.678323510120222 40.57248303391086,25.096292260119604 37.15181539630241,30.721292260119607 33.71546765706166,33.358011010119434 34.588260733017016,33.88535476011994 28.449655040560262,43.90488601011966 11.856912167490952,51.81504226012005 12.20075479326808,77.83066726011982 4.028977730161787,108.06504226012366 -9.459583469067354,125.64316726012251 -11.705717620920182,132.67441726012026 -10.152431372007152,143.57285476011634 -9.979355923170457,149.3736360101228 -11.361254426422146,162.55722976012197 -12.564974802363446,153.94394851011998 4.554840789557522,144.80332351011663 27.82964367289689,149.72519851011776 39.3600325316893,161.3267610101217 49.0308749044493,168.00644851011552 52.36908696821061,173.80722976012194 52.0459307278916,178.90488601012055 61.26510519441699,182.77207351011887 71.68794167450855,168.5337922601169 73.57515149040425,151.83457351012333 76.59607263627282,111.22910476012309 80.87113506745307,38.27988601011966 80.44315976387572,31.07285476011994 70.60907313650104))"
//	);

public class QueryModelTest {

    public static final String KBOX_NAME         = "querytest";
    private static ShapeValue  shp;
    private static ShapeValue  roi;
    private static ShapeValue  swr;
    private static IKbox       kbox;
    private static IMonitor    monitor;

    /*
     * Change as needed - whole thing is overridden by 'project.directory' in .thinklab/thinklab.properties.
     */
    static final String        PROJECT_DIRECTORY = "githop";

    static String              projectDir        = System.getProperty("user.home") + File.separator
                                                         + PROJECT_DIRECTORY;

    private static boolean observes(ModelData md, IObservable observable) {

        for (ModelData.Observable o : md.observables) {
            if (o.is(observable)) {
                return true;
            }
        }
        return false;
    }

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {

        Thinklab.boot();

        String pdir = Thinklab.get().getProperties().getProperty("project.directory", projectDir)
                + File.separator;

        Thinklab.get().registerProject(new File(pdir + "im"));
        Thinklab.get().registerProject(new File(pdir + "org.aries"));
        Thinklab.get().registerProject(new File(pdir + "org.aries.carbon"));
        Thinklab.get().registerProject(new File(pdir + "org.aries.water"));
        Thinklab.get().registerProject(new File(pdir + "thinklab.testsuite"));
        Thinklab.get().load(false, null);

        shp = new ShapeValue(
                "EPSG:4326 POLYGON((-111.012 33.281, -109.845 33.281, -109.845 31.328, -111.012 31.328, -111.012 33.281))");
        roi = new ShapeValue(
                "EPSG:4326 POLYGON ((-111.73104623117726 32.19460046484993, -110.17098763742499 32.138800728791715, -109.86337044992493 29.229294058208446, -111.97274544992155 30.05047726816482, -111.73104623117726 32.19460046484993))");
        swr = new ShapeValue(
                "EPSG:4326 POLYGON ((-114.816209 42.002018, -114.598309 41.994518, -114.048209 41.993818, -113.883409 41.988018, -113.000809 41.999018, -112.193009 42.001218, -112.165309 41.996318, -111.049308 42.001618, -111.046808 42.000918, -111.046508 41.956118, -111.046708 40.998918, -110.500708 40.994718, -109.534908 40.998118, -109.232008 41.002018, -108.526308 40.999618, -106.974207 41.003419, -106.454907 41.002119, -106.191207 40.996719, -104.855307 40.998019, -104.504207 41.001819, -102.051606 41.002919, -102.051606 39.847619, -102.045406 38.857619, -102.044606 38.004919, -102.041606 37.696718, -102.042206 36.993118, -102.698106 36.995118, -102.760106 37.000018, -103.001706 36.999918, -103.001906 36.500418, -103.041906 36.500218, -103.043506 34.015118, -103.064706 32.995318, -103.064506 32.000517, -103.980406 31.999117, -106.125506 32.002517, -106.618506 32.000317, -106.619506 31.994717, -106.631206 31.989817, -106.638906 31.981417, -106.638506 31.977817, -106.630806 31.971817, -106.619506 31.971317, -106.619006 31.965917, -106.625506 31.957517, -106.623906 31.953717, -106.614506 31.955117, -106.614806 31.950517, -106.623706 31.945517, -106.622806 31.934217, -106.629106 31.927217, -106.628006 31.923917, -106.624306 31.925317, -106.612806 31.920417, -106.634606 31.909117, -106.645506 31.898717, -106.645506 31.895317, -106.634606 31.889617, -106.629406 31.883117, -106.635806 31.871617, -106.635206 31.865817, -106.617406 31.848817, -106.602006 31.844017, -106.604906 31.829217, -106.602706 31.825017, -106.588206 31.822117, -106.576706 31.810417, -106.566806 31.813317, -106.546306 31.806217, -106.527706 31.789817, -106.528306 31.785517, -106.531906 31.783917, -108.209207 31.783617, -108.208607 31.333417, -111.074807 31.332217, -112.246108 31.704217, -113.126008 31.972817, -113.817708 32.190217, -114.813908 32.495917, -114.816008 32.507917, -114.807708 32.508717, -114.802508 32.512517, -114.802908 32.515717, -114.810108 32.518817, -114.813108 32.524317, -114.802808 32.535517, -114.805208 32.546817, -114.795408 32.551117, -114.791508 32.558617, -114.792408 32.568017, -114.806808 32.558917, -114.813508 32.561117, -114.812208 32.569317, -114.801808 32.577117, -114.803908 32.581417, -114.799708 32.592217, -114.808008 32.604617, -114.808708 32.619117, -114.798808 32.624917, -114.790508 32.622117, -114.782208 32.625017, -114.779008 32.633617, -114.763908 32.643617, -114.764808 32.649117, -114.750308 32.660717, -114.744308 32.678917, -114.730308 32.699317, -114.729508 32.705217, -114.702408 32.745117, -114.688008 32.737617, -114.617608 32.728217, -114.614808 32.733117, -114.582708 32.734917, -114.581708 32.741317, -114.564508 32.742317, -114.564508 32.749517, -114.539108 32.750217, -114.538908 32.756917, -114.527208 32.757117, -114.532408 32.777817, -114.529208 32.795917, -114.510208 32.816417, -114.493608 32.823717, -114.469008 32.845117, -114.463208 32.909217, -114.476008 32.922917, -114.480908 32.935217, -114.468308 32.954617, -114.468708 32.971517, -114.476308 32.975117, -114.489408 32.970017, -114.492508 32.971417, -114.501008 33.006917, -114.511308 33.023417, -114.523608 33.030917, -114.570408 33.036417, -114.578408 33.035217, -114.589808 33.026217, -114.618808 33.027217, -114.627808 33.030717, -114.638808 33.044317, -114.646008 33.048617, -114.658808 33.033317, -114.662308 33.032717, -114.672908 33.041117, -114.674308 33.057217, -114.687008 33.071017, -114.688808 33.083517, -114.705608 33.087617, -114.707808 33.091117, -114.706208 33.105317, -114.697308 33.130017, -114.687108 33.142217, -114.679408 33.159517, -114.679808 33.170617, -114.675408 33.185517, -114.678808 33.203417, -114.674008 33.224317, -114.689408 33.245817, -114.674508 33.255617, -114.672408 33.260517, -114.677008 33.270217, -114.684408 33.276017, -114.722508 33.287617, -114.731208 33.303217, -114.708008 33.323417, -114.700908 33.337017, -114.698008 33.352417, -114.707208 33.380217, -114.724908 33.401617, -114.723808 33.406517, -114.702308 33.408217, -114.687408 33.417917, -114.675108 33.418217, -114.656708 33.412817, -114.643308 33.416717, -114.630208 33.427417, -114.622308 33.447517, -114.623208 33.455417, -114.602508 33.481017, -114.591908 33.499017, -114.580508 33.506517, -114.568408 33.510117, -114.560808 33.517517, -114.559008 33.531117, -114.524408 33.553217, -114.536808 33.571217, -114.540608 33.591417, -114.529208 33.606617, -114.521908 33.611917, -114.531108 33.624317, -114.530208 33.628817, -114.523808 33.634717, -114.532708 33.647817, -114.532708 33.652417, -114.514308 33.659117, -114.516208 33.662517, -114.530308 33.666817, -114.531208 33.676017, -114.524008 33.685917, -114.496108 33.697717, -114.494208 33.708017, -114.497008 33.719517, -114.512208 33.734617, -114.504608 33.751117, -114.504908 33.760317, -114.527808 33.815317, -114.520108 33.824617, -114.529308 33.847417, -114.529808 33.857517, -114.526208 33.859717, -114.514808 33.858617, -114.506608 33.863617, -114.503208 33.869217, -114.504508 33.876917, -114.523208 33.893117, -114.525408 33.900917, -114.515508 33.898017, -114.507908 33.902817, -114.514508 33.914217, -114.534408 33.925717, -114.535108 33.935217, -114.522008 33.955417, -114.499908 33.962017, -114.468408 33.992417, -114.459708 33.994417, -114.458208 33.997917, -114.466808 34.004617, -114.466308 34.010217, -114.450208 34.012617, -114.440508 34.019317, -114.435008 34.037817, -114.439408 34.053817, -114.436208 34.079717, -114.434008 34.087417, -114.415208 34.108017, -114.406408 34.111417, -114.389808 34.110417, -114.367108 34.118417, -114.351108 34.133917, -114.336108 34.134017, -114.320808 34.138717, -114.287208 34.170517, -114.268608 34.170217, -114.256108 34.173117, -114.229308 34.187217, -114.224908 34.193917, -114.223408 34.205117, -114.211808 34.211517, -114.178208 34.239917, -114.172808 34.247917, -114.166308 34.249817, -114.165108 34.259117, -114.136608 34.258017, -114.132508 34.258917, -114.131208 34.262717, -114.137008 34.277017, -114.139408 34.296017, -114.137708 34.303617, -114.157508 34.319117, -114.176908 34.349317, -114.199508 34.361417, -114.226108 34.365917, -114.234308 34.376617, -114.263608 34.400917, -114.288708 34.406617, -114.294108 34.420517, -114.312308 34.432717, -114.326508 34.437617, -114.334808 34.449717, -114.343508 34.451317, -114.374408 34.447217, -114.385208 34.456217, -114.386908 34.462917, -114.381608 34.477017, -114.382508 34.494717, -114.378108 34.507317, -114.380508 34.528417, -114.404908 34.569117, -114.422408 34.580717, -114.435008 34.593217, -114.436608 34.596517, -114.424808 34.601517, -114.424208 34.610417, -114.438808 34.621917, -114.442808 34.644717, -114.458108 34.657917, -114.451208 34.668217, -114.465008 34.690717, -114.470708 34.711817, -114.487608 34.716817, -114.491508 34.725217, -114.516108 34.736517, -114.529708 34.750717, -114.552708 34.766917, -114.570708 34.793817, -114.577608 34.818317, -114.587008 34.835917, -114.602708 34.848717, -114.623908 34.859717, -114.634808 34.874017, -114.636708 34.889117, -114.630908 34.907317, -114.633308 34.924617, -114.629808 34.943917, -114.635208 34.964417, -114.629008 34.986117, -114.633008 35.002117, -115.102908 35.379417, -115.853309 35.970017, -116.544109 36.501617, -117.000109 36.847217, -117.496509 37.217717, -117.875909 37.497317, -118.714109 38.102117, -119.453809 38.622417, -120.00101 38.999817, -120.00571 39.222617, -119.99621 40.245417, -120.00041 41.184018, -119.99831 41.615318, -120.00001 41.994718, -119.79011 41.997518, -118.69611 41.991818, -118.25961 41.996918, -117.19781 42.000418, -116.397609 41.996318, -115.961909 41.998518, -115.031809 41.996018, -114.816209 42.002018))");

        kbox = Thinklab.get().requireKbox(Thinklab.DEFAULT_KBOX);
        monitor = new Monitor(0, new Session());
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {

        kbox = null;
        Thinklab.shutdown();
    }

    @Test
    public void basicQueries() throws ThinklabException {

        System.out.println("======== Basic Properties Queries ============================");

        /*
         * all private models
         */
        int n = 0;
        for (ISemanticObject<?> o : kbox.query(Queries.select(NS.MODEL_DATA).restrict(NS.IS_PRIVATE,
                Queries.is(true)))) {
            ModelData md = (ModelData) o.demote();
            assertTrue(md.isPrivate);
            n++;
        }
        System.out.println(n + " private models");

        /*
         * models with data (count the public ones to compare with the and() below)
         */
        n = 0;
        for (ISemanticObject<?> o : kbox.query(Queries.select(NS.MODEL_DATA).restrict(NS.IS_DATA_MODEL,
                Queries.is(true)))) {
            ModelData md = (ModelData) o.demote();
            assertTrue(md.isResolved);
            if (!md.isPrivate)
                n++;
        }
        System.out.println(n + " public data models (query only on data status)");

        /*
         * public models (count those with data)
         */
        n = 0;
        int d = 0;
        int s = 0;
        for (ISemanticObject<?> o : kbox.query(Queries.select(NS.MODEL_DATA).restrict(NS.IS_PRIVATE,
                Queries.is(false)))) {
            ModelData md = (ModelData) o.demote();
            assertTrue(!md.isPrivate);
            if (md.isResolved)
                d++;
            if (md.spaceExtent != null)
                s++;
            n++;
        }
        System.out.println(n + " public models of which " + d + " have data and " + s
                + " are spatial (queried ony on public status)");

        /*
         * all public data models
         */
        n = 0;
        for (ISemanticObject<?> o : kbox.query(Queries.select(NS.MODEL_DATA).restrict(
                Queries.and(Queries.restriction(NS.IS_PRIVATE, Queries.is(false)),
                        Queries.restriction(NS.IS_DATA_MODEL, Queries.is(true)))))) {
            ModelData md = (ModelData) o.demote();
            assertTrue(!md.isPrivate && md.isResolved);
            n++;
        }
        System.out.println(n + " public data models (queried with logical and)");

        /*
         * all with space
         */
        n = 0;
        for (ISemanticObject<?> o : kbox.query(Queries.select(NS.MODEL_DATA).restrict(
                Queries.has(NS.SPATIAL_EXTENT_PROPERTY)))) {
            ModelData md = (ModelData) o.demote();
            assertTrue(md.spaceExtent != null);
            n++;
        }
        System.out.println(n + " spatial models");

        /*
         * all non-spatial
         */
        n = 0;
        for (ISemanticObject<?> o : kbox.query(Queries.select(NS.MODEL_DATA).restrict(
                Queries.no(Queries.has(NS.SPATIAL_EXTENT_PROPERTY))))) {
            ModelData md = (ModelData) o.demote();
            assertTrue(md.spaceExtent == null);
            n++;
        }
        System.out.println(n + " non-spatial models");

    }

    public static IQuery withTrait(IConcept type, IConcept value) {
        return Queries.restriction(
                NS.HAS_TRAIT,
                Queries.select(NS.CONCEPT_PAIR).restrict(NS.HAS_FIRST_FIELD, Queries.is(type))
                        .restrict(NS.HAS_SECOND_FIELD, Queries.is(value)));
    }

    public static IQuery withoutTraits() {
        return Queries.no(Queries.has(NS.HAS_TRAIT));
    }

    @Test
    public void rawSemanticQueries() throws Exception {

        System.out.println("======== Raw Semantic Queries ============================");

        IConcept elevation = Thinklab.c("im.geo:ElevationSeaLevel");
        IConcept measurement = Thinklab.c(NS.MEASUREMENT);
        IConcept region = Thinklab.c("im.geo:Region");
        IConcept annual = Thinklab.c("im:Annual");
        IConcept average = Thinklab.c("im:Average");
        IConcept temporalRepresentativity = Thinklab.c("im:TemporalRepresentativity");
        IConcept dataReduction = Thinklab.c("im:DataReduction");
        IConcept quantification = Thinklab.c(NS.QUANTIFICATION);
        IConcept classification = Thinklab.c(NS.CLASSIFICATION);

        /*
         * get all measurement of elevation querying the primary observable
         */
        int n = 0;
        int r = 0;
        for (ISemanticObject<?> o : kbox.query(Queries.select(NS.MODEL_DATA).restrict(
                Queries.and(Queries.restriction(NS.HAS_TYPE, Queries.is(elevation)),
                        Queries.restriction(NS.HAS_OBSERVATION_TYPE, Queries.is(measurement)))))) {
            ModelData md = (ModelData) o.demote();
            assertTrue(md.type.is(elevation) && md.oType.is(measurement));
            if (md.iType != null && md.iType.is(region))
                r++;
            n++;
        }
        System.out.println(n + " measurements of elevation of which " + r
                + " in regions (primary observable)");

        /*
         * get all measurement of elevation using a query on the linked observable (default)
         */
        n = 0;
        r = 0;
        for (ISemanticObject<?> o : kbox.query(Queries.select(NS.MODEL_DATA).restrict(
                NS.HAS_OBSERVABLE,
                Queries.select(NS.OBSERVABLE).restrict(
                        Queries.and(Queries.restriction(NS.HAS_TYPE, Queries.is(elevation)),
                                Queries.restriction(NS.HAS_OBSERVATION_TYPE, Queries.is(measurement))))))) {
            ModelData md = (ModelData) o.demote();
            assertTrue(ObservationQueries.hasObservable(md, elevation, measurement, null));
            if (md.iType != null && md.iType.is(region))
                r++;
            n++;
        }
        System.out
                .println(n + " measurements of elevation of which " + r + " in regions (linked observable)");
        /*
         * get all classifications of elevation
         */
        n = 0;
        for (ISemanticObject<?> o : kbox.query(Queries.select(NS.MODEL_DATA).restrict(
                Queries.and(Queries.restriction(NS.HAS_TYPE, Queries.is(elevation)),
                        Queries.restriction(NS.HAS_OBSERVATION_TYPE, Queries.is(classification)))))) {
            ModelData md = (ModelData) o.demote();
            assertTrue(md.type.is(elevation) && md.oType.is(classification));
            n++;
        }
        System.out.println(n + " classifications of elevation");

        /*
         * all measurement of elevation in regions
         */
        n = 0;
        for (ISemanticObject<?> o : kbox.query(Queries.select(NS.MODEL_DATA).restrict(
                Queries.and(Queries.restriction(NS.HAS_TYPE, Queries.is(elevation)),
                        Queries.restriction(NS.HAS_INHERENT_SUBJECT_TYPE, Queries.is(region)),
                        Queries.restriction(NS.HAS_OBSERVATION_TYPE, Queries.is(measurement)))))) {
            ModelData md = (ModelData) o.demote();
            assertTrue(md.type.is(elevation) && md.oType.is(measurement) && md.iType != null
                    && md.iType.is(region));
            n++;
        }
        System.out.println(n + " measurements of elevation in regions (using query)");

        /*
         * all quantifications
         */
        n = 0;
        for (ISemanticObject<?> o : kbox.query(Queries.select(NS.MODEL_DATA).restrict(
                NS.HAS_OBSERVATION_TYPE, Queries.is(quantification)))) {
            ModelData md = (ModelData) o.demote();
            assertTrue(md.oType.is(quantification));
            n++;
        }
        System.out.println(n + " quantifications");

        /*
         * all with traits
         */
        n = 0;
        for (ISemanticObject<?> o : kbox.query(Queries.select(NS.MODEL_DATA).restrict(
                Queries.has(NS.HAS_TRAIT)))) {
            ModelData md = (ModelData) o.demote();
            assertTrue(md.traits != null);
            n++;
        }
        System.out.println(n + " models with explicit traits");

        /*
         * all with datareduction => average
         */
        n = 0;
        for (ISemanticObject<?> o : kbox.query(Queries.select(NS.MODEL_DATA).restrict(
                withTrait(dataReduction, average)))) {
            ModelData md = (ModelData) o.demote();
            assertTrue(md.traits != null && ObservationQueries.containsTrait(md, dataReduction, average));
            n++;
        }
        System.out.println(n + " models with average datareduction traits");

        // /*
        // * all without datareduction trait or with datareduction trait = average
        // */
        // n = 0;
        // for (ISemanticObject<?> o : kbox.query(Queries.select(NS.MODEL_DATA).restrict(
        // ObservationQueries.traitQuery(dataReduction, average)))) {
        // ModelData md = (ModelData) o.demote();
        // assertTrue(ObservationQueries.containsOnlyTrait(md, dataReduction, average));
        // n++;
        // }
        // System.out.println(n + " models with only average datareduction traits or no traits");

        /*
         * all with datareduction => average
         */
        n = 0;
        for (ISemanticObject<?> o : kbox.query(Queries.select(NS.MODEL_DATA).restrict(
                Queries.and(withTrait(dataReduction, average), withTrait(temporalRepresentativity, annual))))) {
            ModelData md = (ModelData) o.demote();
            assertTrue(md.traits != null && ObservationQueries.containsTrait(md, dataReduction, average)
                    && ObservationQueries.containsTrait(md, temporalRepresentativity, annual));
            n++;
        }
        System.out.println(n + " models with average datareduction and annual temporal traits");

        /*
         * all without traits
         */
        n = 0;
        for (ISemanticObject<?> o : kbox.query(Queries.select(NS.MODEL_DATA).restrict(
                Queries.no(Queries.has(NS.HAS_TRAIT))))) {
            ModelData md = (ModelData) o.demote();
            assertTrue(md.traits == null);
            n++;
        }
        System.out.println(n + " models without explicit traits");

        /*
         * all without traits or with datareduction => average
         */
        n = 0;
        for (ISemanticObject<?> o : kbox.query(Queries.select(NS.MODEL_DATA).restrict(
                Queries.or(withTrait(dataReduction, average), withoutTraits())))) {
            ModelData md = (ModelData) o.demote();
            assertTrue(md.traits == null || ObservationQueries.containsTrait(md, dataReduction, average));
            n++;
        }
        System.out.println(n + " models with average datareduction or without traits");

        /*
         * all with inherency
         */
        n = 0;
        HashSet<IConcept> cset = new HashSet<IConcept>();
        for (ISemanticObject<?> o : kbox.query(Queries.select(NS.MODEL_DATA).restrict(
                Queries.has(NS.HAS_INHERENT_SUBJECT_TYPE)))) {
            ModelData md = (ModelData) o.demote();
            assertTrue(md.iType != null);
            cset.add(md.iType);
            n++;
        }
        System.out.println(n + " models with inherent subjects " + Arrays.toString(cset.toArray()));

        /*
         * all without inherency
         */
        n = 0;
        for (ISemanticObject<?> o : kbox.query(Queries.select(NS.MODEL_DATA).restrict(
                Queries.no(Queries.has(NS.HAS_INHERENT_SUBJECT_TYPE))))) {
            ModelData md = (ModelData) o.demote();
            assertTrue(md.iType == null);
            n++;
        }
        System.out.println(n + " models without inherent subjects");
    }

    @Test
    public void observationQueries() throws Exception {

        System.out.println("======== Observation Semantic Queries ======================");

        IConcept elevation = Thinklab.c("im.geo:ElevationSeaLevel");
        IConcept measurement = Thinklab.c(NS.MEASUREMENT);
        IConcept region = Thinklab.c("im.geo:Region");
        IConcept annual = Thinklab.c("im:Annual");
        IConcept average = Thinklab.c("im:Average");
        IConcept temporalRepresentativity = Thinklab.c("im:TemporalRepresentativity");
        IConcept dataReduction = Thinklab.c("im:DataReduction");
        IConcept quantification = Thinklab.c(NS.QUANTIFICATION);
        IConcept classification = Thinklab.c(NS.CLASSIFICATION);
        IConcept direct = Thinklab.c(NS.DIRECT_OBSERVATION);

        IResolutionContext context = ResolutionContext.root(monitor, null);
        IObservable observable = new Observable(region, direct, "");

        int n = 0;
        IQuery query = Queries.select(NS.MODEL_DATA).restrict(
                ObservationQueries.observableQuery(context.forObservable(observable), observable));
        for (ISemanticObject<?> o : kbox.query(query)) {
            ModelData md = (ModelData) o.demote();
            assertTrue(observes(md, observable));
            n++;
        }
        System.out.println(n + " models of region in empty context");

        n = 0;
        observable = new Observable(elevation, measurement, "");
        query = Queries.select(NS.MODEL_DATA).restrict(
                ObservationQueries.observableQuery(context.forObservable(observable), observable));
        for (ISemanticObject<?> o : kbox.query(query)) {
            ModelData md = (ModelData) o.demote();
            assertTrue(observes(md, observable));
            n++;
        }
        System.out.println(n + " models of elevation measurement in empty context");

        n = 0;
        observable = new Observable(elevation, measurement, region, "");
        query = Queries.select(NS.MODEL_DATA).restrict(
                ObservationQueries.observableQuery(context.forObservable(observable), observable));
        for (ISemanticObject<?> o : kbox.query(query)) {
            ModelData md = (ModelData) o.demote();
            assertTrue(observes(md, observable));
            n++;
        }
        System.out.println(n + " models of elevation measurement in regions, empty context");
    }

    @Test
    public void extentQueries() throws Exception {

        System.out.println("======== Extent Queries ==================================");

        /*
         * spatial models intersecting shp
         */
        int n = 0;
        for (ISemanticObject<?> o : kbox.query(Queries.select(NS.MODEL_DATA).restrict(
                NS.SPATIAL_EXTENT_PROPERTY, Queries.intersects(roi)))) {
            ModelData md = (ModelData) o.demote();
            assertTrue(md.spaceExtent != null && md.spaceExtent.intersects(shp));
            n++;
        }
        System.out.println(n + " spatially intersecting shp");

        /*
         * with any kind of temporal extent
         */
        n = 0;
        for (ISemanticObject<?> o : kbox.query(Queries.select(NS.MODEL_DATA).restrict(
                NS.HAS_TIME_MULTIPLICITY, Queries.compare(0, Queries.GT)))) {
            ModelData md = (ModelData) o.demote();
            assertTrue(md.isTemporal);
            n++;
        }
        System.out.println(n + " with temporal extent");

    }

    @Test
    public void checkForDuplicates() throws Exception {

        System.out.println("======== Duplicates detection ============================");

        HashSet<String> names = new HashSet<String>();
        HashMap<String, Integer> duplicates = new HashMap<String, Integer>();

        for (ISemanticObject<?> o : kbox.query(Queries.select(NS.MODEL_DATA))) {
            ModelData md = (ModelData) o.demote();
            if (names.contains(md.name)) {
                if (duplicates.containsKey(md.name)) {
                    duplicates.put(md.name, duplicates.get(md.name) + 1);
                } else {
                    duplicates.put(md.name, 2);
                }
            }
            names.add(md.name);
        }

        for (String s : duplicates.keySet()) {
            System.out.println(s + ": " + duplicates.get(s) + " occurrences");
        }

        if (duplicates.size() == 0) {
            System.out.println("no duplicate models detected");
        }

        assertTrue(duplicates.size() == 0);
    }

}
