package org.integratedmodelling.thinklab.tests;

import java.util.Enumeration;

import org.junit.Test;
import org.rosuda.JRI.REXP;
import org.rosuda.JRI.RMainLoopCallbacks;
import org.rosuda.JRI.RVector;
import org.rosuda.JRI.Rengine;

public class RTest {

    /**
     * R_HOME must be set correctly (Windows): The directory containing R.dll
     * must be in your PATH (Mac): Well, it's a Mac, so it just works ;).
     * (unix): R must be compiled using --enable-R-shlib and the directory
     * containing libR.so must be in LD_LIBRARY_PATH. Also libjvm.so and other
     * dependent Java libraries must be on LD_LIBRARY_PATH. JRI library must be
     * in the current directory or any directory listed in java.library.path.
     * Alternatively you can specify its path with -Djava.library.path= when
     * starting the JVM. When you use the latter, make sure you check
     * java.library.path property first such that you won't break your Java.
     * Depending on your system, the R verison and other features you want to
     * use, you may have to set additional settings such as R_SHARE_DIR,
     * R_INCLUDE_DIR and R_DOC_DIR.
     * 
     * @author Ferd
     * 
     */

    class TextConsole implements RMainLoopCallbacks {

        @Override
        public void rBusy(Rengine arg0, int arg1) {
            // TODO Auto-generated method stub

        }

        @Override
        public String rChooseFile(Rengine arg0, int arg1) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public void rFlushConsole(Rengine arg0) {
            // TODO Auto-generated method stub

        }

        @Override
        public void rLoadHistory(Rengine arg0, String arg1) {
            // TODO Auto-generated method stub

        }

        @Override
        public String rReadConsole(Rengine arg0, String arg1, int arg2) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public void rSaveHistory(Rengine arg0, String arg1) {
            // TODO Auto-generated method stub
            System.out.println(arg1);
        }

        @Override
        public void rShowMessage(Rengine arg0, String arg1) {
            // TODO Auto-generated method stub
            System.out.println(arg1);

        }

        @Override
        public void rWriteConsole(Rengine arg0, String arg1, int arg2) {
            // TODO Auto-generated method stub
            System.out.println(arg1);
        }

    }

    @Test
    public void test() {

        /**
         * Just testing the R interface as cut-and-paste fodder.
         */
        Rengine re = new Rengine(new String[] {}, false, new TextConsole());
        REXP x;
        re.eval("print(1:10/3)");
        System.out.println(x = re.eval("iris"));
        RVector v = x.asVector();
        if (v.getNames() != null) {
            System.out.println("has names:");
            for (Enumeration<?> e = v.getNames().elements(); e.hasMoreElements();) {
                System.out.println(e.nextElement());
            }
        }

        if (true) {
            System.out.println("Now the console is yours ... have fun");
            re.startMainLoop();
        } else {
            re.end();
            System.out.println("end");
        }
    }

}
