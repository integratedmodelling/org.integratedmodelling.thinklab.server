/**
 * 
 */
package org.integratedmodelling.thinklab.tests;

import java.util.Arrays;

import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.plugins.weather.Weather;
import org.integratedmodelling.thinklab.plugins.weather.WeatherFactory;
import org.integratedmodelling.thinklab.plugins.weather.WeatherGenerator;
import org.integratedmodelling.thinklab.plugins.weather.WeatherStation;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Ferd
 *
 */
public class WeatherTest {

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        Thinklab.boot();
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        Thinklab.shutdown();
    }

    @Test
    public void testLoadStations() throws Exception {
        WeatherFactory.get().checkDatabase();
    }

    @Test
    public void testLookupWeather() throws Exception {

        WeatherStation ws = WeatherFactory.get().getClosestStationProviding(-15.3833333, 35.33333330000005,
                100.0, Weather.PRECIPITATION_MM, Weather.MIN_TEMPERATURE_C, Weather.MAX_TEMPERATURE_C);

        if (ws != null) {

            Thinklab.get().info("Found " + ws);

            System.out.println(ws.fetchData(Weather.PRECIPITATION_MM).dump());
            System.out.println(ws.fetchData(Weather.MIN_TEMPERATURE_C).dump());
            System.out.println(ws.fetchData(Weather.MAX_TEMPERATURE_C).dump());

            System.out.println("generating simulated weather");

            WeatherGenerator wg = new WeatherGenerator(ws, null, 13);

            double minT[] = new double[366], maxT[] = new double[366], rain[] = new double[366];

            for (int i = 0; i < 10; i++) {
                wg.generateDaily(minT, maxT, rain, false);
                System.out.println(i + ": minT = " + Arrays.toString(minT));
                System.out.println(i + ": maxT = " + Arrays.toString(maxT));
                System.out.println(i + ": rain = " + Arrays.toString(rain));
            }

        } else {
            Thinklab.get().info("No station found");
        }
    }
}
