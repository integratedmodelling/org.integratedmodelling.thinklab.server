namespace org.integratedmodelling.test1
	covering 
		shape(wkt="EPSG:4326 POLYGON((-21.2660 27.6363, -21.2660 61.0088, 39.8693 61.0088, 39.8693 27.6363, -21.2660 27.6363))")
	resolve from test1 
		using {                    // redefine ranking criteria to resolve these models
			qa:evidence 100
			qa:open 50
			qa:quality 80
			qa:timeliness 40
		}
	store to test1;

/*
 * ---------------------------------------------------------------------------------------------
 * example models
 * ---------------------------------------------------------------------------------------------
 */

define model rainfall-global of 
		(concept Rainfall is habitat:Rainfall)
	as measure (measure habitat:Rainfall in m) in mm 
		using geospace.physics:GroundElevation named elevation;

define model rainfall-local of 
		(concept RainfallLocal is habitat:Rainfall)
	as measure habitat:Rainfall in m;

define model elevation-class of ElevationClass 
	as classify (measure geospace.physics:GroundElevation in m)
		LowElevation when < 100,
		MediumElevation when 100 to 500,
		HighElevation when > 500;
		
/**
 * test model for identifications. Should compute states for all dependencies and
 * return a plain observation with just the observable, plus all states in the context.
 */
define model identification of DummySystem 
	using elevation-class;
		
/*
 * ---------------------------------------------------------------------------------------------
 * example annotations of datasources
 * These are stored in the 'store to' kbox and used for resolution, no need to name them
 * ---------------------------------------------------------------------------------------------
 */

model wcs(service="http://ecoinformatics.uvm.edu/geoserver/wcs", 
			  id="global:annual_precip_global",
			  no-data = -32768.0)
     as measure habitat:Rainfall in mm;

model wcs(service="http://ecoinformatics.uvm.edu/geoserver/wcs", id="global:dem90m")
     as measure geospace.physics:GroundElevation in m
     with metadata {
              dc:comment "SRTM DEM 90m resolution, hydrologically adjusted"
              dc:originator "NASA"
              dc:rights "cc-attr-nomod"
              qa:open 100
              qa:quality 80
              qa:review 75
       };	
/*
 * ---------------------------------------------------------------------------------------------
 * example contexts
 * ---------------------------------------------------------------------------------------------
 */	
	
define context puget as
     grid(resolution="10 km", 
	  shape="EPSG:4326 POLYGON((-124.7625 24.5210, -66.9326  24.5210, -66.9326 49.3845, -124.7625 49.3845, -124.7625 24.5210))");
	  
	