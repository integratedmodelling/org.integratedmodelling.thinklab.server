package org.integratedmodelling.thinklab.tests;

import static org.junit.Assert.fail;

import java.io.File;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.Thinklab;
import org.integratedmodelling.thinklab.runtime.Context;
import org.integratedmodelling.thinklab.runtime.Session;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class AgentTests {

    private static final String[] PROJECT_DIRECTORIES = new String[] { "C:\\Users\\Ferd\\githop" };
    private static Thinklab       thinklab;
    private static Session        session;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        Thinklab.boot();
        thinklab = Thinklab.get();
        for (String projectDirectory : PROJECT_DIRECTORIES) {
            thinklab.info("Loading project directory " + projectDirectory + "...");
            thinklab.registerProjectDirectory(new File(projectDirectory));
        }
        thinklab.info("Calling Thinklab.load()...");
        thinklab.load(true, null);
        session = new Session();
    }

    private static Context getSubjectProxy() {
        return new Context(session, session.getSessionMonitor(0), "test");
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        Thinklab.shutdown();
    }

    @Test
    public void testFail() {
        fail("Failure test");
    }

    @Test
    public void testSuccess() {
        thinklab.info("Empty test - should succeed...");
    }

    @Test
    public void testOneObservationCycle() throws ThinklabException {
        thinklab.info("Loading subject proxy for Seattle...");
        Context seattle = (Context) getSubjectProxy().observe("aries.locations.test.tanzania-grr").observe(
                "im.geo:ElevationSeaLevel", "im.landcover:LandCoverType");

        // create a visualization in this directory - defaults to ${HOME}/thinklab/archive/<parameter>;
        // seattle.visualize("test/luke");
    }
}
