echo ----------------------------
java -Xms512M -Xmx1024M -XX:MaxPermSize=256m -Djava.library.path="lib/ext-osx/" -cp "lib/*" org.integratedmodelling.thinklab.main.RESTServer > thinklab.log
echo -----------------------------
echo Thinklab REST server application stopped

