#include "stdafx.h"
#include "time.h"
using namespace std;
#include "randModule.h"


//Module randModule
//
//    'The multiple recursive generator of L'Ecuyer as given in Law and Kelton's 
//    'Third Edition, Section 7.3.2 and the C code is given in Appendix 7B.
//    'The C code is also found on the web site http://www.mhhe.com/engcs/industrial/lawkelton/
//
//    'Note that the VB code below works because a Long variable is 64 bits, not 32 bits.
//
//    'The only procedure contained here is the function defined as
//    '                  Function Variate(ByVal strm As Short) As Double
//    'which returns a UNIF(0,1) random variate based on the random number stream given by "strm"  
//
//
//    'Each random number seed is actually a vector of six variables.  The array below rndSeed(,)
//    'contains 3000 initial seed vectors that are spaced at least 10,000,000,000,000,000 apart.  
//    'The user will specify a row between 1 and 1000.
//    'The row will be used for arrivals.  The arrival row index + 1000 will be used for 
//    'services, and another 1000 will yield the row for failures and repairs.  
//    'if the user specifies 0 for the row, then a random number (from the VB random generator)
//    'is used to specify a row between 0 and 1000.



const __int64 M1  = 4294967087;  // 2^32 - 209
const __int64 M2 = 4294944443;   // 2^32 - 22853
const int MAXSTREAMS = 100;
const double EPSILON = 0.000000287; // equivalent to 0.0 (5 stan.dev. from mean)
    
    // The following constants are to numerically integrate and obtain the inverse normal
    
const double  P0  = -0.322232431088;
const double  P1 = -1.0;
const double  P2 = -0.342242088547;
const double  P3 = -0.0204231210245;
const double  P4 = -0.0000453642210148;
const double  Q0 = -0.099348462606;
const double  Q1 = -0.588581570495;
const double  Q2 = -0.531103462366;
const double  Q3 = -0.10353775285;
const double  Q4 = -0.0038560700634;
const double  EPS1 = 0.5 - EPSILON;
const double  EPS2 = 0.5 + EPSILON;
const double  EPS3 = 1.0 - EPSILON;

static __int64 rndSeed[][6] = {0, 0, 1, 0, 0, 1, 
       1772212344, 1374954571, 2377447708, 540628578, 1843308759, 549575061, 
       2602294560, 1764491502, 3872775590, 4089362440, 2683806282, 437563332, 
       376810349, 1545165407, 3443838735, 3650079346, 1898051052, 2606578666, 
       1847817841, 3038743716, 2014183350, 2883836363, 3242147124, 1955620878, 
       1075987441, 3468627582, 2694529948, 368150488, 2026479331, 2067041056, 
       134547324, 4246812979, 1700384422, 2358888058, 83616724, 3045736624, 
       2816844169, 885735878, 1824365395, 2629582008, 3405363962, 1835381773,
       675808621, 434584068, 4021752986, 3831444678, 4193349505, 2833414845, 
       2876117643, 1466108979, 163986545, 1530526354, 68578399, 1111539974, 
       411040508, 544377427, 2887694751, 702892456, 758163486, 2462939166, 
       3631741414, 3388407961, 1205439229, 581001230, 3728119407, 94602786, 
       4267066799, 3221182590, 2432930550, 813784585, 1980232156, 2376040999, 
       1601564418, 2988901653, 4114588926, 2447029331, 4071707675, 3696447685, 
       3878417653, 2549122180, 1351098226, 3888036970, 1344540382, 2430069028, 
       197118588, 1885407936, 576504243, 439732583, 103559440, 3361573194, 
       4024454184, 2530169746, 2135879297, 2516366026, 260078159, 2905856966,
       2331743881, 2059737664, 186644977, 401315249, 72328980, 1082588425, 
       694808921, 2851138195, 1756125381, 1738505503, 2662188364, 3598740668, 
       2834735415, 2017577369, 3257393066, 3823680297, 2315410613, 637316697, 
       4132025555, 3700940887, 838767760, 2818574268, 1375004287, 2172829019, 
       3489426225, 4112746453, 2594929844, 1633976546, 1583084490, 4147801476, 
       226509369, 146935912, 3937833922, 1061829953, 813052180, 3597307501, 
       3256044974, 3681783929, 575085515, 1457436746, 1015379089, 237144042, 
       3893937255, 2700371479, 2540977187, 448868868, 2639602201, 3320881846, 
       2151265850, 2125810247, 1191988643, 1600692767, 135550530, 395838860, 
       3135267209, 3623600367, 2626505177, 4281737957, 2062913934, 2592194162, 
       390422829, 1811400753, 322793407, 3490624807, 3235017317, 371104964, 
       2656265790, 3996442108, 3386349306, 1626832241, 1574763932, 3706202892, 
       3589952455, 1163511069, 2697607059, 534184903, 3766759256, 251252346, 
       2882420196, 3003606956, 3629906612, 2537203610, 2746594184, 1661091636, 
       1183183944, 583879472, 4255459700, 713754974, 2922254235, 1161913813, 
       4132807367, 3628950852, 399250763, 3845478137, 1309889890, 213170736, 
       3112893239, 1632688723, 1523209273, 1177690048, 3460621046, 2768883982, 
       2661550465, 1554695591, 4025759551, 3938508444, 4250175274, 1555765950, 
       2684621241, 1586910242, 3611689202, 206694063, 6969092, 1062099384, 
       235778005, 1845387954, 3114888710, 2203759286, 3114065342, 2496367282, 
       2441823102, 2650658691, 1337568664, 1857164665, 1915815707, 884425315, 
       4101909040, 4166390745, 2860172669, 3497960192, 3577628224, 4213141252, 
       3699546758, 3377270633, 68718870, 2774746460, 1020495218, 1575238977, 
       1139564619, 2523670566, 3355096665, 495198678, 2395315759, 849476900, 
       635963092, 311652233, 262540809, 3184560243, 2651025088, 789619078, 
       1271360621, 4223486533, 1390580044, 2888955378, 3184629747, 630626237, 
       1678221843, 231113094, 4130363024, 3852779467, 1604842289, 4212522078, 
       2778593270, 1004903484, 2973098739, 665751699, 2332674775, 295462521, 
       4198005471, 972878412, 1558189575, 55781237, 3893360589, 1732945031, 
       1152796221, 1778239045, 1459562806, 3905398533, 3769475924, 2644342737, 
       2510032067, 3734379515, 531296007, 701842463, 2074273193, 2292680950, 
       2971989859, 2256121557, 2142214447, 3137353222, 773588230, 3902637774, 
       4229774628, 121176046, 3728703289, 3849382323, 2003165682, 1467065642, 
       323392186, 3824746296, 1629368039, 739902158, 1994020456, 1037183254, 
       513283862, 2188379510, 1957928689, 813409383, 3806082123, 635316674, 
       552826185, 2555996419, 2271338972, 3547921306, 1228993928, 2361559081, 
       1295138976, 1495710207, 723074677, 3820787179, 3845682156, 4174822106, 
       2521565200, 31792620, 3784456031, 1065580104, 3937944782, 4215959677, 
       3400057703, 2744571285, 1393759441, 1601776871, 3165609536, 835804949, 
       4088663797, 662403715, 1972116992, 2505435866, 3440128606, 777899473, 
       3780961670, 2157269301, 1083414147, 2821221342, 3067184313, 2993244053, 
       2795653207, 2163909033, 3809789650, 2585746232, 3313980374, 3096075721, 
       2378158017, 3726102874, 2689185822, 2369776558, 432649940, 1111256897, 
       3456908798, 495588917, 1480249157, 2815947509, 1071333363, 1928430914, 
       717921372, 2683845918, 930457772, 2530097953, 2417205907, 3022951420, 
       1689355612, 1039193903, 3613681802, 1080205242, 2013101883, 2468502905, 
       2504068936, 3230484718, 1748896401, 1453957344, 848547757, 161200170, 
       4201049117, 444124251, 2581583939, 1322446501, 681505596, 1663004988, 
       2930586493, 3370079320, 198796253, 3012797217, 3840048005, 184999535, 
       2532724791, 1241080524, 3800041958, 3303947134, 3174626846, 3073510302, 
       14809834, 3618645306, 1942542077, 669052232, 651470251, 1391560038, 
       995707287, 1759822820, 2501644613, 3721319358, 3968667126, 2535846961, 
       979173856, 1997457820, 1968292063, 1380431809, 3055802991, 540373583, 
       3681007982, 2977347732, 571110841, 2217121181, 2733362413, 32186934, 
       1975286634, 958506122, 46451179, 3475987512, 2626451279, 3517401221, 
       330144009, 1060219535, 1447223839, 3165226575, 599904263, 3911112835, 
       2537161090, 3500106754, 926795530, 728440468, 1129037665, 2789187437, 
       3793615118, 2750706029, 2156058298, 3079033430, 2780569996, 3936920391, 
       1198120767, 2694820585, 2834525274, 188651758, 308670432, 1369385984, 
       2702470127, 19874369, 4012779639, 3121258228, 1839121226, 3861314075, 
       2554275712, 719378996, 2400430690, 1141204239, 1627924328, 1297183842, 
       705258195, 4161016439, 1462878351, 3976713745, 1141257086, 3425581727, 
       3358325932, 2544614576, 1863746614, 1124881023, 3147032679, 3087920200, 
       2977113134, 3977111791, 4253114310, 142443885, 243603301, 3716506593, 
       3431919302, 1245400869, 1324788929, 2860157607, 941144108, 3857335131, 
       3906194044, 1681219081, 2843586309, 4089310580, 2176521638, 690520146, 
       3745569546, 792495396, 1806539660, 4272439405, 1894505679, 1392532334, 
       865747759, 3079408284, 1984574005, 613765073, 1551526411, 4256443592, 
       2814963340, 2834960950, 2968542314, 309822219, 1823774832, 1839552557, 
       2548640113, 2862571993, 4042326055, 2483508296, 2762715198, 593338793, 
       2568040427, 2120166173, 942509028, 2137567779, 2626314860, 2259019634, 
       1311709266, 3938696051, 1456683566, 3795230520, 1388639258, 2800468070, 
       3117451406, 928974817, 2781895567, 321394107, 110152236, 2088746743, 
       1875039110, 3442179808, 3890426888, 1777391452, 1835432708, 562722092, 
       1448528649, 2330442989, 2407439339, 3707873573, 4196869323, 2107506543, 
       2780921782, 358207338, 3529199325, 4247445586, 2383769411, 3241037506, 
       1378002714, 2723574102, 1749215013, 2907202697, 2962834356, 1188034898, 
       120313119, 1494806970, 2910978351, 722434700, 1419161042, 2517182236, 
       1195864685, 3838840335, 1111591396, 3060365866, 2463752639, 3666213547, 
       1333023911, 1979613259, 2132866392, 1394964690, 1264353313, 3231502632, 
       3768306921, 1680121724, 805680272, 1951253770, 1486168061, 2955477946, 
       109224445, 2861250694, 4196750427, 939526471, 1752437524, 1145799039, 
       408306775, 2148819781, 3316724552, 137163319, 4180687869, 574961213, 
       2386386992, 3078479797, 167075008, 1253465513, 4072841169, 2167533564, 
       2315827434, 3392038067, 2279258261, 1432017357, 568998206, 1206469405, 
       1678601670, 1150247641, 2499326926, 699715048, 1501364333, 1669290797, 
       3957493358, 1670076821, 804110344, 2866685751, 836848697, 3107121052, 
       19764713, 3522310913, 46276655, 1383766567, 2804181892, 581103203, 
       2167472521, 2014671778, 682905578, 958597651, 1861637882, 2944058806};

void SetSeed(int& initValue) 
{ //   ' in case the seed should be saved
   //     ' initValue is returned as a value between 0 and MAXSTREAMS
	long long hold = initValue;
	if (hold <= 0) time(&hold);
	initValue =  int(hold % MAXSTREAMS);
	if (initValue == 0) initValue = MAXSTREAMS;
}



    //' =====================================================================================
    //' 
    //'    RandomNumber Class
    //'     used only as base class for other variate generators
    //'     if random numbers needed, use Uniform()
    //'
    //' =====================================================================================

RandomNumber::RandomNumber(int strm)
{
	SetSeed(strm);
	stream = strm;
    name = "RandomNumber";
}



double RandomNumber::GenerateRN(void) 
{
	// The multiple recursive generator of L'Ecuyer as given in Law and Kelton's 
    // Third Edition, Section 7.3.2
	__int64 p, y;
    __int64 s10 = rndSeed[stream][0];
	__int64 s11 = rndSeed[stream][1];
	__int64 s12 = rndSeed[stream][2];
	__int64 s20 = rndSeed[stream][3];
	__int64 s21 = rndSeed[stream][4];
	__int64 s22 = rndSeed[stream][5];

    p = 1403580 * s11 - 810728 * s10;
    p = p % M1;
    if (p < 0) p += M1;
	s10 = s11 ; 
	s11 = s12 ; 
	s12 = p;

    p = 527612 * s22 - 1370589 * s20;
    p = p % M2;
    if (p < 0) p += M2;

    s20 = s21 ; 
	s21 = s22 ; 
	s22 = p;

    rndSeed[stream][0] = s10 ; 
	rndSeed[stream][1] = s11 ; 
	rndSeed[stream][2] = s12;
    rndSeed[stream][3] = s20 ; 
	rndSeed[stream][4] = s21 ; 
	rndSeed[stream][5] = s22;

    y = (s12 - s22) % M1;
	if (y < 0) y += M1;
    else if (y == 0) y = 4294967086;

    oldNumber = double(y) / double(M1);
	return oldNumber;
}



    //' =====================================================================================
    //' 
    //'    Uniform Class
    //'
    //' =====================================================================================

Uniform::Uniform(int strm) : RandomNumber(strm)
{
    name = "Uniform";
    low = 0.0;
    high = 1.0;
    delta = 1.0;
	mean = 0.5;
	stdDev = 0.288675;
}
 
Uniform::Uniform(double value_for_low, double value_for_high, int strm) : RandomNumber(strm)
{
    name = "Uniform";
    low = value_for_low;
    high = value_for_high;
    delta = high-low;
	mean = 0.5*(low+high);
	stdDev = sqrt(delta*delta/12.0);
}

double Uniform::Generate(void)
{
    oldValue = low + delta * GenerateRN();
    return oldValue;
}

double Uniform::Generate(double value_for_low, double value_for_high)
{
	low = value_for_low;
    high = value_for_high;
    delta = high - low;
	mean = 0.5*(low+high);
	stdDev = sqrt(delta*delta/12.0);
    return Generate();
}

void Uniform::SetValues(double value_for_low, double value_for_high)
{
	low = value_for_low;
    high = value_for_high;
    delta = high - low;
	mean = 0.5*(low+high);
	stdDev = sqrt(delta*delta/12.0);
}


 /*   ' =====================================================================================
    ' 
    '    Bernoulli Class
    '
    ' =====================================================================================*/

Bernoulli::Bernoulli(int strm) : RandomNumber(strm)
{
	name = "Bernoulli";
    prob = 0.5;
	mean = 0.5;
	stdDev = 0.5;
}

Bernoulli::Bernoulli(double probabilyOfSucesss, int strm) : RandomNumber(strm)
{
	name = "Bernoulli";
    prob = probabilyOfSucesss;
	if (prob > 1.0) prob = 1.0;
	if (prob < 0.0) prob = 0.0;
	mean  = prob;
	stdDev = sqrt(prob*(1.0-prob));
}

bool Bernoulli::Generate(void)
{
	if (GenerateRN() < prob){
		oldValue = 1.0;
		return true;
	}
	else{
		oldValue = 0.0;
		return false;
	}
}

bool Bernoulli::Generate(double probabilyOfSucesss)
{
	prob = probabilyOfSucesss;
	mean  = prob;
	stdDev = sqrt(prob*(1.0-prob));
	return Generate();
}

//    ' =====================================================================================
//    ' 
//    '    Exponential Class
//    '
//    ' =====================================================================================
//
Exponential::Exponential(int strm) : RandomNumber(strm)
{
	name = "Exponential";
    mean = 0.0;
}

Exponential::Exponential(double value_for_mean, int strm) : RandomNumber(strm)
{
	name = "Exponential";
	mean = value_for_mean;
	stdDev = mean;
    if (mean < 0.0) mean = 0.0;
}

double Exponential::Generate(void)
{
	oldValue = -log(GenerateRN()) * mean;
	return oldValue;
}

double Exponential::Generate(double value_for_mean)
{
	mean = value_for_mean;
	stdDev = mean;
	if (mean < 0.0) mean = 0.0;
	return Generate();
}


//    ' =====================================================================================
//    ' 
//    '    Normal Class
//    '
//    ' =====================================================================================
//
Normal::Normal(int strm) : RandomNumber(strm)
{
	name = "Normal";
	mean = 0.0;
	stdDev = 1.0;
}

Normal::Normal(double value_for_mean, double value_for_stdDev, int strm) : RandomNumber(strm)
{
	name = "Normal";
	mean = value_for_mean;
	stdDev = value_for_stdDev;
}

double Normal::Generate(void)
{
	double y, z;
	if (stdDev < EPSILON) return mean;
	
	if (GenerateRN() < 0.5) { 
		if (oldNumber > EPS1)  z = 0.0;
        else if (oldNumber < EPSILON) z = -5.0;
		else  {
			y = sqrt(-2.0 * log(oldNumber));
            z = -y + ((((y * P4 + P3) * y + P2) * y + P1) * y + P0) / 
                            ((((y * Q4 + Q3) * y + Q2) * y + Q1) * y + Q0);
		}
	}
	else {
		if (oldNumber < EPS2) z = 0.0;
        else if (oldNumber > EPS3)  z = 5.0;
		else {
            y = sqrt(-2.0 * log(1.0 - oldNumber));
            z = y - ((((y * P4 + P3) * y + P2) * y + P1) * y + P0) / 
                            ((((y * Q4 + Q3) * y + Q2) * y + Q1) * y + Q0);
		}
	}
    
	oldValue = mean + z * stdDev;
    return oldValue;
}

double Normal::Generate(double value_for_mean, double value_for_stdDev)
{
	mean = value_for_mean;
    stdDev = value_for_stdDev;
    return Generate();
}

void Normal::SetValues(double value_for_mean, double value_for_stdDev)
{
	mean = value_for_mean; 
	stdDev = value_for_stdDev;
}


//    ' =====================================================================================
//    ' 
//    '    Gamma Class
//    '
//    ' =====================================================================================

Gamma::Gamma(int strm) : RandomNumber(strm)
{
	name = "Gamma";
	mean = shape = scale = 0.0;
}

Gamma::Gamma(double value_for_mean, double value_for_stdDev, int strm) : RandomNumber(strm)
{
	name = "Gamma";
	mean = value_for_mean;
	stdDev = value_for_stdDev;
	if (mean < EPSILON) {
        mean = shape = scale = 0.0;
		return;
	}
	if (stdDev < EPSILON) {
		stdDev = 0.0;
        oldValue = mean;
        scale = mean;
        shape = 0.0;
		return;
	}
	scale = stdDev * stdDev / mean;
    shape = mean * mean / (stdDev * stdDev);
}

void Gamma::PutScale(double value_for_scale)
{
	scale = value_for_scale;
	if (scale < EPSILON) {
		scale = stdDev = shape = mean = 0.0;
		return;
	}
	mean = scale*shape;
	stdDev = scale * sqrt(shape);
}

void Gamma::PutShape(double value_for_shape)
{
	shape = value_for_shape;
	if (shape < EPSILON) {
		scale = stdDev = shape = mean = 0.0;
		return;
	}
	mean = scale*shape;
	stdDev = scale * sqrt(shape);
}

double Gamma::Generate(void)
{	
	int k;
	double a, b, q, d, u1, u2, y, w, v, z;
            
	if (mean < EPSILON) return 0.0;
	if (stdDev < EPSILON) return mean;

    // first test if erlang
    k = int(shape + 0.5);
	if ((k > 0) && (abs(shape - k) < 0.01)) {
		q = mean / k;
		oldValue = 0.0;
		for (int i=0; i<k; ++i)
			oldValue += q * (-log(GenerateRN()));
        return oldValue;
	}

	// not erlang
	if (shape > 1.0) {
        a = 1.0 / sqrt(2.0 * shape - 1.0);
        b = shape - 1.386294361;   //     ' log(4) = 1.386294361   
        q = shape + 1.0 / a;
        d = 2.504077397;  //             ' 1+log(4.5) = 2.504077397 
		for (int i=0; i<500; ++i) {
            u1 = GenerateRN();
            u2 = GenerateRN();
            v = a * log(u1 / (1.0 - u1));
            y = shape * exp(v);
            z = u1 * u1 * u2;
            w = b + q * v - y;
            oldValue = scale * y;
            if ((w + d - 4.5 * z) >= 0) return oldValue;
            if (w >= log(z)) return oldValue;
		}
		//  '  should not make normal exit from this for-loop
		//cout << "ERROR in generating gamma variate.  scale = " << scale
		//	        << ", shape = " << shape << endl;
		exit(1);
	}     // end for shape > 1.0


        b = (2.718281828 + shape) / 2.718281828; // ' e = 2.7818281828
		for (int i=0; i<500; ++i) {
            u1 = GenerateRN();
            u2 = GenerateRN();
            q = b * u1;
			if (q > 1) {
                y = -log((b - q) / shape);
				if (u2 <= pow(y,(shape - 1))) {
                    oldValue = scale * y;
                    return oldValue;
				}
			}
			else {  //  ' now q <= 1
                y = pow(q,(1.0 / shape));
				if (u2 <= exp(-y)) {
					oldValue = scale * y;
                    return oldValue;
				}
			}
		}   // end of for
		//  '  should not make normal exit from this for-loop
		//cout << "ERROR while generating gamma variate.  scale = " << scale
		//	        << ", shape = " << shape << endl;
		exit(2);
}    // end of Gamma::Generate(void)

double Gamma::Generate(double value_for_mean, double value_for_stdDev)
{
	mean = value_for_mean;
	stdDev = value_for_stdDev;
	if (mean < EPSILON) {
        mean = shape = scale = 0.0;
		return 0.0;
	}
	if (stdDev < EPSILON) {
		stdDev = 0.0;
        oldValue = mean;
        scale = mean;
        shape = 0.0;
		return mean;
	}
	scale = stdDev * stdDev / mean;
    shape = mean * mean / (stdDev * stdDev);
	return Generate();
}


void Gamma::SetValues(double value_for_mean, double value_for_stdDev)
{
	mean = value_for_mean;
	stdDev = value_for_stdDev;
	if (mean < EPSILON) {
        mean = shape = scale = 0.0;
		return; 
	}
	if (stdDev < EPSILON) {
		stdDev = 0.0;
        oldValue = mean;
        scale = mean;
        shape = 0.0;
		return;
	}
	scale = stdDev * stdDev / mean;
    shape = mean * mean / (stdDev * stdDev);
}
//
//    ' =====================================================================================
//    ' 
//    '    GamNorm Class
//    '
//    ' =====================================================================================

GamNorm::GamNorm(int strm)
{
    name = "GamNorm";
    gamVar = new Gamma(strm);
    normVar = new Normal(strm);
	mean = stdDev = 0.0;
	useNorm = true;
}

GamNorm::GamNorm(double value_for_mean, double value_for_stdDev, int strm)
{
    name = "GamNorm";
    gamVar = new Gamma(value_for_mean, value_for_stdDev,strm);
    normVar = new Normal(value_for_mean, value_for_stdDev,strm);
	mean = value_for_mean;
	if (mean<EPSILON) mean = 0.0;
	stdDev = value_for_stdDev;
	if (stdDev<EPSILON) {oldValue = mean; stdDev = 0.0;}
	useNorm = (mean >= (5.01 * stdDev) );
}

double GamNorm::Generate(void)
{
	if (stdDev<EPSILON) return mean;

	if (useNorm) oldValue = normVar->Generate();
	else oldValue = gamVar->Generate();
	return oldValue;
}

double GamNorm::Generate(double value_for_mean, double value_for_stdDev)
{
	mean = value_for_mean;
	stdDev = value_for_stdDev;
	gamVar->SetValues(value_for_mean, value_for_stdDev);
	normVar->SetValues(value_for_mean, value_for_stdDev);
	if (mean<EPSILON) {
		mean = 0.0;
		oldValue = mean;
		return mean;
	}
	if (stdDev<EPSILON) {
		oldValue = mean; 
		stdDev = 0.0;
		return mean;
	}

	useNorm = (mean >= (5.01 * stdDev) );
	if (useNorm) {
		oldValue = normVar->Generate(mean, stdDev);
	}
	else {
		oldValue = gamVar->Generate(mean, stdDev);
	}
	return oldValue;
}

void GamNorm::SetValues(double value_for_mean, double value_for_stdDev)
{
	mean = value_for_mean;
	stdDev = value_for_stdDev;
	gamVar->SetValues(value_for_mean, value_for_stdDev);
	normVar->SetValues(value_for_mean, value_for_stdDev);
	if (mean<EPSILON) {
		mean = 0.0;
		return;
	}
	if (stdDev<EPSILON) {
		stdDev = 0.0;
	}
	useNorm = (mean >= (5.01 * stdDev) );
}

//    ' =====================================================================================
//    ' 
//    '    LogNormal Class
//    '
//    ' =====================================================================================
//
LogNormal::LogNormal(int strm) : Normal(strm)
{
	name = "LogNormal"; // default is lognormal of standardized normal
	mean = 0.0;   // mean of normal
	stdDev = 1.0;  // for normal
	logMean = 1.64872127;   // value of sqrt(e)
	logStdDev = 2.161197416;   // value of sqrt((e-1)*e)
}

LogNormal::LogNormal(double value_for_mean, double value_for_stdDev, int strm)  : Normal(strm)
{
	name = "LogNormal";
    logMean = value_for_mean;
    logStdDev = value_for_stdDev;
    FixMeanStDev();
}

void LogNormal::FixMeanStDev(void)   // private subroutine to fix mean and stdDev
{
	double mu2, sigma2;
	if (logMean < EPSILON) {
		logMean = 0.0;
		return;
	}
	if (logStdDev < EPSILON) {
        logStdDev = 0.0;
		return;
	}
	mu2 = logMean * logMean;
    sigma2 = logStdDev * logStdDev;
    mean = 0.5 * log((mu2 * mu2) / (sigma2 + mu2)); //  ' mean of normal
    stdDev = sqrt(log((mu2 + sigma2) / mu2));  //        ' stddev of normal
}

double LogNormal::Generate(void)
{
	if (logMean < EPSILON) {
		oldLogValue = 0.0;
		return 0.0;
	}
	if (logStdDev < EPSILON) {
        oldLogValue = logMean;
		return logMean;
	}
	oldLogValue = exp(Normal::Generate());
    return oldLogValue;
}

double LogNormal::Generate(double value_for_mean, double value_for_stdDev)
{
    logMean = value_for_mean;
    logStdDev = value_for_stdDev;
    FixMeanStDev();
	return Generate();
}


//    ' =====================================================================================
//    ' 
//    '    LogNormal Class for Correlated Variates
//    '
//    ' =====================================================================================
//

CorrelatedLogNormal::CorrelatedLogNormal(int strm) : Normal(strm)
{
    name = "CorrelatedLogNormal";
	m1 = 0.0;   // mean of normal
	s1 = 1.0;  // for normal
	logMean1 = 1.64872127;   // value of sqrt(e)
	logStdDev1 = 2.161197416;   // value of sqrt((e-1)*e)
	m2 = 0.0;   // mean of normal
	reducedS2 = 1.0;  // for normal
	logMean2 = 1.64872127;   // value of sqrt(e)
	logStdDev2 = 2.161197416;   // value of sqrt((e-1)*e)
	logRho = 0.0;
}

CorrelatedLogNormal::CorrelatedLogNormal(double value_for_mean1, double value_for_stdDev1,
		                     double value_for_mean2, double value_for_stdDev2,
						     double value_for_correlation_coef, int strm) : Normal(strm)
{
    name = "CorrelatedLogNormal";
	logMean1 = value_for_mean1;
    logStdDev1 = value_for_stdDev1;
    logMean2 = value_for_mean2;
    logStdDev2 = value_for_stdDev2;
    logRho = value_for_correlation_coef;
    FixMeanStDev();
}

void CorrelatedLogNormal::FixMeanStDev()
{
	double mu2, sigma2, s2, r;
    double coefVar1, coefVar2;
	if ((logMean1 < EPSILON) || (logMean2 < EPSILON)) {
        logMean1 = 0.0;
        logMean2 = 0.0;
        return;
	}
	if ((logStdDev1 < EPSILON) || (logStdDev2 < EPSILON)) {
        logStdDev1 = 0.0;
        logStdDev2 = 0.0;
        return;
	}
	if (logRho > 1.0)
		logRho = 1.0;
	if (logRho < -1.0)
		logRho = -1.0;

    coefVar1 = logStdDev1 / logMean1;
    coefVar2 = logStdDev2 / logMean2;

    mu2 = logMean1 * logMean1;
    sigma2 = logStdDev1 * logStdDev1;
    m1 = 0.5 * log((mu2 * mu2) / (sigma2 + mu2)); //  ' mean of normal
    s1 = sqrt(log(1 + coefVar1 * coefVar1)); //   ' stddev of normal

    mu2 = logMean2 * logMean2;
    sigma2 = logStdDev2 * logStdDev2;
    m2 = 0.5 * log((mu2 * mu2) / (sigma2 + mu2)); //  ' mean of normal
    s2 = sqrt(log(1.0 + coefVar2 * coefVar2)); //       ' stddev of normal

    r = log(1.0 + logRho * coefVar1 * coefVar2) / (s1 * s2);
    rFactor = r * s2 / s1;
    reducedS2 = s2 * sqrt(1.0 - r * r);
    //  note stdDevValue is fixed, but meanValue varies each time
}

double CorrelatedLogNormal::Generate(void)
{
	if (logMean1 < EPSILON) {
        oldLogValue = 0.0;
        return 0.0;
	}
	if (logStdDev1 < EPSILON) {
        oldLogValue = logMean1;
        return logMean1;
	}
    mean = m1;
    stdDev = s1;
	oldLogValue =  exp(Normal::Generate());
    return oldLogValue;
}

double CorrelatedLogNormal::Generate(double value_for_mean1, double value_for_stdDev1,
		                     double value_for_mean2, double value_for_stdDev2,
						     double value_for_correlation_coef)
{
	logMean1 = value_for_mean1;
    logStdDev1 = value_for_stdDev1;
    logMean2 = value_for_mean2;
    logStdDev2 = value_for_stdDev2;
    logRho = value_for_correlation_coef;
    FixMeanStDev();
	if (logMean1 < EPSILON) {
        oldLogValue = 0.0;
        return 0.0;
	}
	if (logStdDev1 < EPSILON) {
        oldLogValue = logMean1;
        return logMean1;
	}
    mean = m1;
    stdDev = s1;
	oldLogValue =  exp(Normal::Generate());
    return oldLogValue;
}

double CorrelatedLogNormal::Generate2(void)
//  generates according the second mean and standard deviation
//  correlated with the random variate previously generated by Generate()
{
	if (logMean2 < EPSILON) {
        oldLogValue2 = 0.0;
        return 0.0;
	}
	if (logStdDev2 < EPSILON) {
        oldLogValue2 = logMean2;
        return logMean2;
	}
    mean = m2 + rFactor * (oldValue - m1);
    stdDev = reducedS2;
	oldLogValue2 =  exp(Normal::Generate());
    return oldLogValue2;
}     

double CorrelatedLogNormal::Generate2(double value_for_first_logValue)
//  generates according the second mean and standard deviation
//  correlated with the random variate value_for_first_logValue
{
	if (logMean2 < EPSILON) {
        oldLogValue2 = 0.0;
        return 0.0;
	}
	if (logStdDev2 < EPSILON) {
        oldLogValue2 = logMean2;
        return logMean2;
	}
    mean = m2 + rFactor * (log(value_for_first_logValue) - m1);
    stdDev = reducedS2;
	oldLogValue2 =  exp(Normal::Generate());
    return oldLogValue2;
}

