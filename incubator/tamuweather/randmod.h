
#ifndef randModule_h_
#define randModule_h_

#include "math.h"


//Module randModule
//
//    'The multiple recursive generator of L'Ecuyer as given in Law and Kelton's 
//    'Third Edition, Section 7.3.2 and the C code is given in Appendix 7B.
//    'The C code is also found on the web site http://www.mhhe.com/engcs/industrial/lawkelton/
//
//    'Note that the VB code below works because a Long variable is 64 bits, not 32 bits.
//
//    'The only procedure contained here is the function defined as
//    '                  Function Variate(ByVal strm As Short) As Double
//    'which returns a UNIF(0,1) random variate based on the random number stream given by "strm"  
//
//
//    'Each random number seed is actually a vector of six variables.  The array below rndSeed(,)
//    'contains 3000 initial seed vectors that are spaced at least 10,000,000,000,000,000 apart.  
//    'The user will specify a row between 1 and 1000.
//    'The row will be used for arrivals.  The arrival row index + 1000 will be used for 
//    'services, and another 1000 will yield the row for failures and repairs.  
//    'If the user specifies 0 for the row, then a random number (from the VB random generator)
//    'is used to specify a row between 0 and 1000.


//void SetSeed(int* initValue);
       
   /* ' =====================================================================================
    ' 
    '    RandomNumber Class
    '     used only as base class for other variate generators
    '     if random numbers needed, use Uniform()
    '
    ' =====================================================================================*/


class RandomNumber
{
public:
	double GetRV(void) {return oldValue;}
	double GetMean(void) {return mean;}
	double GetStdDev(void) {return stdDev;}

private:
	int stream;

protected:
	char *name;
	double mean;
	double stdDev;
	double oldNumber;
	double oldValue;  // value of most recently generated random variate
	double GenerateRN(void);
    RandomNumber(int strm);

};   // end class RandomNumber

class Uniform : public RandomNumber
{
private:
	double low;
	double high;
	double delta;

public:
	Uniform(int strm);
	Uniform(double low, double high, int strm);
	double Generate(void);
	double Generate(double value_for_low, double value_for_high);
	double GetLow(void) {return low;}
	double GetHigh(void) {return high;}
	void SetValues(double value_for_low, double value_for_high);
};  // end class Uniform

class Bernoulli : public RandomNumber  // default is 50/50 split
{
private:
	double prob;

public:
	Bernoulli(int strm);
	Bernoulli(double probabilyOfSucesss, int strm);
	bool Generate(void);
	bool Generate(double probabilyOfSucesss);
	double GetProbSuccess(void) {return prob;}
};  // end of Bernoulli class

class Exponential : public RandomNumber
{
public:
	Exponential(int strm);
	Exponential(double value_for_mean, int strm);
	double Generate(void);
	double Generate(double value_for_mean);
};     // end of Exponential class

class Normal : public RandomNumber
{
public:
	Normal(int strm);
	Normal(double value_for_mean, double value_for_stdDev, int strm);
	double Generate(void);
	double Generate(double value_for_mean, double value_for_stdDev);
	void SetValues(double value_for_mean, double value_for_stdDev);
};    // end of Normal class

class Gamma : public RandomNumber
{
private:
	double shape, scale;

public:
	Gamma(int strm);
	Gamma(double value_for_mean, double value_for_stdDev, int strm);
	double Generate(void);
	double Generate(double value_for_mean, double value_for_stdDev);
	double GetShape(void) {return shape;}
	void PutShape(double value_for_shape);
	double GetScale(void) {return scale;}
	void PutScale(double value_for_scale);
	void SetValues(double value_for_mean, double value_for_stdDev);
};  // end of Gamma class

class GamNorm
{
private:
	double mean, stdDev;
	bool useNorm;
	Gamma* gamVar;
	Normal* normVar;

protected:
	char *name;
	double oldValue;

public:
	GamNorm(int strm);
	GamNorm(double value_for_mean, double value_for_stdDev, int strm);
	double Generate(void);
	double Generate(double value_for_mean, double value_for_stdDev);
	double GetMean(void) {return mean;}
	double GetStdDev(void) {return stdDev;}
	double GetRV(void) {return oldValue;}
	void SetValues(double value_for_mean, double value_for_stdDev);
	~GamNorm(void) {delete gamVar; delete normVar;}
};    // end of GamNorm class


class LogNormal : Normal
{
private:
	double logMean;
	double logStdDev;
	double oldLogValue;
	void FixMeanStDev(void);

public:
	LogNormal(int strm);
	LogNormal(double value_for_mean, double value_for_stdDev, int strm);
	double Generate(void);
	double Generate(double value_for_mean, double value_for_stdDev);
	double GetRV(void) {return oldLogValue;}
	double GetMean(void) {return logMean;}
	double GetStdDev(void) {return logStdDev;}
};    // end of LogNormal class


class CorrelatedLogNormal : Normal
	// variable 1 is always generated first, then variable 2 is generated
	// based on the value for variable 1
{
private:
	double logMean1;
	double logStdDev1;
	double logMean2;
	double logStdDev2;
	double oldLogValue;
	double oldLogValue2;
	double logRho;  // ' correlation coefficient
    double m1, m2, s1, reducedS2;
    double rFactor; // ' equals rho*s2/s1
	void FixMeanStDev(void);

public:
	CorrelatedLogNormal(int strm);
	CorrelatedLogNormal(double value_for_mean1, double value_for_stdDev1,
		                     double value_for_mean2, double value_for_stdDev2,
						     double value_for_correlation_coef, int strm);
	double Generate(void);
	double Generate(double value_for_mean1, double value_for_stdDev1,
		                     double value_for_mean2, double value_for_stdDev2,
						     double value_for_correlation_coef);
	double Generate2(void);
	double Generate2(double value_for_first_logValue);
	double GetRV(void) {return oldLogValue;}
	double GetRV2(void) {return oldLogValue2;}
	double GetMean(void) {return logMean1;}
	double GetStdDev(void) {return logStdDev1;}
	double GetMean2(void) {return logMean2;}
	double GetStdDev2(void) {return logStdDev2;}
	double GetCorrelation(void) {return logRho;}
};    // end of CorrelatedLogNormal class
      
class SamplStat 
//        ' uses Welford's algorithm to avoid round-off error 
//        '        of squared terms if data values are large
{
protected:
	char* name;
	double ssq, minVal, maxVal, meanVal;
	long num;

public:
	SamplStat(char* statName = "SamplStat");
	double mean() {return meanVal;}
	double stdDev();
	double mina() {return minVal;}
	double maxa() {return maxVal;}
	long sampleSize() {return num;}
	void AddValue(double dataValue);
	void Reset();
};   // end of SamplStat class

//   ==================================================
//   
//    Student-t and Chi-Squared statistics
//
//   ==================================================

double StudentT(double degreesOfFreedom);
double ChiSquaredRight(double degreesOfFreedom);
double ChiSquaredLeft(double degreesOfFreedom);

class SemiEmpirical : public Uniform
{
private:
	double* bounds;
	int* heights;
	int totalHeights;
	int intervals;
	double* cumProbabilities;

public:
	SemiEmpirical(int strm);
	SemiEmpirical(double values_for_bounds[], int values_for_heights[], 
		               int value_for_intervals, int strm);
	~SemiEmpirical();
	double Generate(void);
	double Generate(double value_for_given);
	double Generate(double values_for_bounds[], int values_for_heights[], 
		               int value_for_intervals);
	double Generate(double values_for_bounds[], int values_for_heights[], 
		               int value_for_intervals, double value_for_given);
	void SetValues(double values_for_bounds[], int values_for_heights[], 
		               int value_for_intervals);
};  // end class SemiEmpirical

#endif randModule_h_