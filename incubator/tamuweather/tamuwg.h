#ifndef tamuWG_h_
#define tamuWG_h_

#include "randModule.h"
//constants
const int GENERATEYEARS = 1; //number of years to be generated
const int NUMDAYS = 366; //number of days.  
// Data members are stored with indices 1-365 (day 0 is always blank).  
// The program currently throws out Feb. 29 of a leap year, 
//but if a leap year were dsired, change this value to 367.
//  (Daymet website already excludes Feb. 29.)
const int NUMMONTHS = 12; //number of months
const int NUMINTERVALS = 10; // for semi-empirical distributions

class TamuWeather
{
private:
	char*   m_fileName;  // name of historical data file
	int     m_yearCount;  // number of years of historical data used

	// following are the various correlation coefficients
	double  m_rhoMin; //between minimum temperatures on consecutive days
	double  m_rhoMax; //between maximum temperatures on consecutive days
	double  m_rhoDaily; //between min and max temperatures on the same day
	double  m_rhoMinMax; //between min temperature and max temperature on the next day
	double  m_rhoMaxMin; //between max temperature and min temperature on the next day

	// temperature data from the end of the year to start next year, if desired
	//  first two are for rain data, next four are temperature data
	bool    m_wetSeries;
	int     m_seriesLength;
	double  m_minPrevResidual;
	double  m_minPrevStdDev;
	double  m_maxPrevResidual;
	double  m_maxPrevStdDev;

	// the following vectors contain the temperature parameters the fourier transform
	double  m_minWetParameters[7];
	double  m_maxWetParameters[7];
	double  m_minDryParameters[7];
	double  m_maxDryParameters[7];
	double  m_minWetStdDevParameters[7];
	double  m_maxWetStdDevParameters[7];
	double  m_minDryStdDevParameters[7];
	double  m_maxDryStdDevParameters[7];

	// create Bernoulli distribution giving prob rain on Dec 31
	//  used in case of random start
	Bernoulli* m_probRain;
	//create normal distribution for temperature
	Normal* m_tempDist;
	//create semi-empirical distributions for length of wet & dry series and rain
	SemiEmpirical* m_wetDist[NUMMONTHS]; 
	SemiEmpirical* m_dryDist[NUMMONTHS];  
	SemiEmpirical* m_rainDist[NUMMONTHS];  

public:
	TamuWeather(char* filename, int strm);
	~TamuWeather(void);
	char* GetName(void) {return m_fileName;}
	int   GetYearCount(void) {return m_yearCount;}
	void  GenerateDaily(double minTemp[], double maxTemp[], double rain[], bool rndStart = false);
};

#endif


