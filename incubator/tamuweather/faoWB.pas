{enter soil type}
FieldCapacity:=VFC[soiltexture];//FC, depends on soil texture, we have default values
PermanentWP:=VWP[soiltexture];//PWP, depends on soil texture, we have default values
Zr:=0.6;//enter, I think this is layer depth in m
ADT:=1000*(FieldCapacity-PermanentWP)*Zr;
DailyWaterStorage [1]:= 0; //for initialisation
StorageIni:=0;
DrfIni:=ADT-StorageIni;
For dayRotation3:= 1 to 1460 do //this is for 4 years, for 1 year just do until 365 days
begin
{ETP calculations}
DailyCumulativeTemp[dayRotation3];//code cumulative temperature since seeding somewhere else
GDD:=DailyCumulativeTemp[dayRotation3]; //GDD, cumulative degree days

{Kc parameter is specific for each crop type and also depends on growth stage, following calculations are for wheat only}
{ 0- 562 GDD: Kc=0.7
563- 1313: aumento de Kc desde 0.7 hasta 1.15 (se podr�a ir sumando por cada GDD lo que corresponde, en este caso 6*10^-4 por cada GDD)
1314- 1989: Kc=1.15
1990- 2600: disminuci�n de la Kc desde 1.15 hasta 0.25 (por cada GDD se restar�a 14.754*10^-4)
           }
DayHArvest=AugustDay;
DaySeeding= NovDay;
{I have it for 4 years rotations so there is plenty of code down here that is not needed}
If (dayRotation3<DayHarvest) then
begin
  if DailyCumulativeTemp[dayRotation3]<=562 then
      Kc[dayRotation3]:=0.7
  else if (DailyCumulativeTemp[dayRotation3]>562) AND (DailyCumulativeTemp[dayRotation3]<=1313)then
      Kc[dayRotation3]:=0.7+((DailyCumulativeTemp[dayRotation3]-563)*0.0006)
  else if (DailyCumulativeTemp[dayRotation3]>1313) AND (DailyCumulativeTemp[dayRotation3]<=1989)then
      Kc[dayRotation3]:=1.5
  else if (DailyCumulativeTemp[dayRotation3]>1989) AND (DailyCumulativeTemp[dayRotation3]<=2600)then
      Kc[dayRotation3]:=1.15-((DailyCumulativeTemp[dayRotation3]-1989)*0.0014754)
  else
     Kc[dayRotation3]:=0.25;
end
else if (dayRotation3>= DayHarvest) AND (dayRotation3< DaySeeding) then
begin
 Kc[dayRotation3]:=0.25;
end
else If (dayRotation3>= DaySeeding) AND (dayRotation3<( DayHarvest +365)) then
begin
  if DailyCumulativeTemp[dayRotation3]<=562 then
      Kc[dayRotation3]:=0.7
  else if (DailyCumulativeTemp[dayRotation3]>562) AND (DailyCumulativeTemp[dayRotation3]<=1313)then
      Kc[dayRotation3]:=0.7+((DailyCumulativeTemp[dayRotation3]-563)*0.0006)
  else if (DailyCumulativeTemp[dayRotation3]>1313) AND (DailyCumulativeTemp[dayRotation3]<=1989)then
      Kc[dayRotation3]:=1.5
  else if (DailyCumulativeTemp[dayRotation3]>1989) AND (DailyCumulativeTemp[dayRotation3]<=2600)then
      Kc[dayRotation3]:=1.15-((DailyCumulativeTemp[dayRotation3]-1989)*0.0014754)
  else
     Kc[dayRotation3]:=0.25;
end
else if (dayRotation3>=(365+ DayHarvest)) AND (dayRotation3<(365+ DaySeeding)) then
begin
 Kc[dayRotation3]:=0.25;
end
else If ((dayRotation3>= DaySeeding +365)) AND (dayRotation3<( DayHarvest +365+365)) then
begin
  if DailyCumulativeTemp[dayRotation3]<=562 then
      Kc[dayRotation3]:=0.7
  else if (DailyCumulativeTemp[dayRotation3]>562) AND (DailyCumulativeTemp[dayRotation3]<=1313)then
      Kc[dayRotation3]:=0.7+((DailyCumulativeTemp[dayRotation3]-563)*0.0006)
  else if (DailyCumulativeTemp[dayRotation3]>1313) AND (DailyCumulativeTemp[dayRotation3]<=1989)then
      Kc[dayRotation3]:=1.5
  else if (DailyCumulativeTemp[dayRotation3]>1989) AND (DailyCumulativeTemp[dayRotation3]<=2600)then
      Kc[dayRotation3]:=1.15-((DailyCumulativeTemp[dayRotation3]-1989)*0.0014754)
  else
     Kc[dayRotation3]:=0.25;
end
else if (dayRotation3>=(365+365+AugDay)) AND (dayRotation3<(365+365+NovDay)) then
begin
 Kc[dayRotation3]:=0.25;
end
else If ((dayRotation3>=NovDay+365+365)) AND (dayRotation3<(AugDay+365+365+365)) then
begin
  if DailyCumulativeTemp[dayRotation3]<=562 then
      Kc[dayRotation3]:=0.7
  else if (DailyCumulativeTemp[dayRotation3]>562) AND (DailyCumulativeTemp[dayRotation3]<=1313)then
      Kc[dayRotation3]:=0.7+((DailyCumulativeTemp[dayRotation3]-563)*0.0006)
  else if (DailyCumulativeTemp[dayRotation3]>1313) AND (DailyCumulativeTemp[dayRotation3]<=1989)then
      Kc[dayRotation3]:=1.5
  else if (DailyCumulativeTemp[dayRotation3]>1989) AND (DailyCumulativeTemp[dayRotation3]<=2600)then
      Kc[dayRotation3]:=1.15-((DailyCumulativeTemp[dayRotation3]-1989)*0.0014754)
  else
     Kc[dayRotation3]:=0.25;
end
else if (dayRotation3>=(365+365+365+AugDay)) then
begin
 Kc[dayRotation3]:=0.25;
end;
{END OF CALCULATIONS FOR Kc}
Latitude:=42.853*PI/180;
{these are some parameters that are used to calculate ETC}
CAlDay[dayRotation3]:= (dayRotation3)*0.0172;
DAilyD[dayRotation3]:=0.409*SIN(CAlDay[dayRotation3]-1.39);
dailyf[dayRotation3]:=ARCCOS(-TAN(Latitude)*TAN(DAilyD[dayRotation3]));
dailysenf[dayRotation3]:=sin(dailyf[dayRotation3]);
dailysend[dayRotation3]:=SIN(0.409*SIN(CAlDay[dayRotation3]-1.39));
dailycosd[dayRotation3]:=POWER((1-(Power(dailysend[dayRotation3],2))), 0.5);
dailycosdcoslat[dayRotation3]:=dailycosd[dayRotation3]*COS(Latitude);
dailyDr[dayRotation3]:=1+0.033*COS(CAlDay[dayRotation3]);
dailysendsenlat[dayRotation3]:=dailysend[dayRotation3]*SIN(Latitude);
DailyRa[dayRotation3]:=37.6*dailyDr[dayRotation3]*((dailyf[dayRotation3]*dailysendsenlat[dayRotation3])+(dailycosdcoslat[dayRotation3]*dailysenf [dayRotation3]));
{ POTENTIAL ETP}
DailyETP0_1[dayRotation3]:=0.0023*DailyRa[dayRotation3]/2.45*(DailyMeantemperature[dayRotation3]+17.8)*POWER((DailyMaxtemperature[dayRotation3]-DailyMintemperature[dayRotation3]),0.5);
{CALIBRATED POTENTIAL ETP}
DailyETP0[dayRotation3]:=-0.0788+0.7478*DailyETP0_1[dayRotation3];
{ ETP}
DailyETP[dayRotation3]:=DailyETP0[dayRotation3]*Kc[dayRotation3];

{ Ro[wheat], SPECIFIC PARAMETER FOR CROP TYPE, in this case for wheat}
RoDaily[dayRotation3]:=Ro[wheat]+0.04*(5-DailyETP[dayRotation3]);
AFADaily[dayRotation3]:=ADT*RoDaily[dayRotation3];

{THIS IS THE BULK OF THE SOIL WATER SIMPLE MODEL}
  If dayRotation3=1 then //To initialize the first day
  begin
DailyDri[1]:=DrfIni;
IF((ADT-DailyDri[1])/(ADT-AFADaily[dayRotation3])<1)then
  DailyKs[dayRotation3]:=(ADT-DailyDri[1])/(ADT-AFADaily[dayRotation3])
  else
  DailyKs[dayRotation3]:=1;

     IF(StorageIni+DailyRain[DayRotation3])>DailyETP[DayRotation3] then
      DailyWaterAET[DayRotation3]:= DailyETP[DayRotation3]*DailyKs[dayRotation3]
    else
     DailyWaterAET[DayRotation3]:= StorageIni+DailyRain[DayRotation3];

DailyWaterER[1]:=DailyRain[1]-DailyWaterAET[1];

   If ((StorageIni+DailyWaterAET[1])>0) AND((StorageIni+DailyWaterAET[1])<ADT) then
       DailyWaterStorage [1]:=StorageIni+DailyWaterER[1]
   else if (StorageIni+DailyWaterER[1]<=0) then
       DailyWaterStorage [1]:=0
     else
       DailyWaterStorage [1]:=ADT;

  DailyWaterDeficit[dayRotation3]:= DailyETP[1]-DailyWaterAET[1];
  DrfDaily[1]:=ADT-DailyWaterStorage [1];

   if (DailyRain[1]-DailyWaterAET[1]+StorageIni-DailyWaterStorage [1])>0 then
   DailyWaterSurplus[dayRotation3]:=(DailyRain[1]-DailyWaterAET[1]+StorageIni-DailyWaterStorage [1])
   else
   DailyWaterSurplus[dayRotation3]:=0;

  end //END OF CALCULATION SFOR TH EFIRST DAY
  else // CALCULATIONS FOR the REST OF THE DAYS FROM HERE
  begin
DailyDri[DayRotation3]:=DrfDaily[pred(dayRotation3)];
IF((ADT-DailyDri[dayRotation3])/(ADT-AFADaily[dayRotation3])<1)then
  DailyKs[dayRotation3]:=(ADT-DailyDri[dayRotation3])/(ADT-AFADaily[dayRotation3])
  else
  DailyKs[dayRotation3]:=1;
   IF(DailyWaterStorage [pred(DayRotation3)]+DailyRain[DayRotation3])>DailyETP[DayRotation3] then
      DailyWaterAET[DayRotation3]:= DailyETP[DayRotation3]*DailyKs[dayRotation3]
    else
     DailyWaterAET[DayRotation3]:= DailyWaterStorage [pred(DayRotation3)]+DailyRain[DayRotation3];

   DailyWaterER[DayRotation3]:=DailyRain[DayRotation3]-DailyWaterAET[DayRotation3];

   If ((DailyWaterStorage [pred(DayRotation3)]+DailyWaterER[DayRotation3])>0) AND((DailyWaterStorage [pred(DayRotation3)]+DailyWaterER[DayRotation3])<ADT) then
       DailyWaterStorage [DayRotation3]:=DailyWaterStorage [pred(DayRotation3)]+DailyWaterER[DayRotation3]
   else if (DailyWaterStorage [pred(DayRotation3)]+DailyWaterER[DayRotation3]<=0) then
       DailyWaterStorage [DayRotation3]:=0
     else
       DailyWaterStorage [DayRotation3]:=ADT;

  DailyWaterDeficit[DayRotation3]:= DailyETP[DayRotation3]-DailyWaterAET[DayRotation3];

   if (DailyRain[DayRotation3]-DailyWaterAET[DayRotation3]+DailyWaterStorage [pred(DayRotation3)]-DailyWaterStorage [DayRotation3])>0 then
   DailyWaterSurplus[DayRotation3]:=(DailyRain[DayRotation3]-DailyWaterAET[DayRotation3]+DailyWaterStorage [pred(DayRotation3)]-DailyWaterStorage [DayRotation3])
   else
   DailyWaterSurplus[DayRotation3]:=0;
  DrfDaily[DayRotation3]:=ADT-DailyWaterStorage [DayRotation3];
  end; //END OF CALCULATIONS FOR REST OF DAYS
{CALCULATION FOR EVERY DAY OF soil water content (WC) in % vol, WFPS (water filled pore space in %),
And HER: Hydrologically Effective Rainfall (in mm) This value is close to drainage in the absence of lateral water flows-this is our assumption for here}
  DailyWC[DayRotation3]:=DailyWaterStorage [DayRotation3]/Zr/1000+PermanentWP ;
 DailyWFPS[DayRotation3]:=0.01*(100*DailyWC[DayRotation3])/((1-(BulkDensity[L]/2.65)));
  DailyHER[DayRotation3]:=DailyWaterSurplus[DayRotation3]; //enter somewhere
end;
