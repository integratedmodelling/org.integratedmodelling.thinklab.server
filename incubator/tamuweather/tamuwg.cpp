#include "StdAfx.h"
#include "tamWG.h"
#include "randModule.h"
#include <fstream>
#include <cmath>
#include <iostream>
#include <iomanip>

using namespace std;
//function definitions
const double EPS = 0.00001;
const double PI = 3.14159265;
const char* outfileStr = "";// "Addison.csv";
ofstream outfile;
void initRegArrays(double** array1, double** array2);
void constructRegMatrix(double** regMatrix, double xTymin[], double xTymax[], 
						                       int day, double ymin, double ymax);
void constructRegMatrix(double** regMatrix);
void initializeBounds(void);
void initializeFreq(void);
void assignLARSbounds(double bounds[], int maxConsDays);
void calcParameters(double parameters[], double** regMatrix, double xTy[]);

double fourierFit(double parameters[], int index);
void createTvect(double Tvect[], double minTemp, double newMinMean, 
				    double minTempPrevResidual, double maxTempPrevResidual);
double createV_Cinv(double VC[], double newMinStd, double newMaxStd, 
				         double minPrevStd, double maxPrevStd, double rhoDaily,
				         double rhoMin, double rhoMax, double rhoMinMx, double rhoMxMn);
// returns the value of V times Cinv times V-transpose

int _FindMonth(int date);
double** dmatrix(short num_rows, short num_columns);
void free_dmatrix(double** the_matrix, short num_rows, short num_col);
void inv_in_place(double**, short);
int GetInterval(int member, double bounds[]);
int GetInterval(double member, double bounds[]);
int ReadInData(char* fileName, double& finalMin, double& finalMax, bool bheaderrow);
double ReadAgain(char* fileName, int& length, bool& wet);


const int jul[NUMMONTHS] = {31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334}; //last julian day for each month
const int daysPerMonth[NUMMONTHS] = {31,28,31,30,31,30,31,31,30,31,30,31};
//stores daily stats for each of 365 days
typedef struct {
	int wetDays;
	int dryDays;
	double wetMinTempSum;
	double wetMaxTempSum;
	double dryMinTempSum;
	double dryMaxTempSum;
	double wetMinTempStdDev;
	double wetMaxTempStdDev;
	double dryMinTempStdDev;
	double dryMaxTempStdDev;
} _dailyData;
_dailyData dailyStats[NUMDAYS];

//more function definitions
double standardize(double temp, double meanPara[], double stdDevPara[], int day);
double calcResiduals(int day, double temp, double rain, bool minimum);

//global variables
//int currentMonth;
//int year;
//int day;
//int monthOrigin; //stores the month in which the current series originates.  takes values 0-(NUMMONTHS-1).
int maxConsWetDays[NUMMONTHS]; //stores max # of consecutive wet days for a given month
int maxConsDryDays[NUMMONTHS]; //stores max # of consecutive dry days for a given month
double rainMax[12]; //stores max amount of rain in cm for a given month
double xTwetMinTemp[7]; //X'*Y -- ' denotes transpose
double xTwetMaxTemp[7];
double xTdryMinTemp[7];
double xTdryMaxTemp[7];
double** wetRegMatrix; //matrix used for regression of temperatures X'*X
double** dryRegMatrix;
double xTwetMinTempStdDev[7]; //X'*Y matrix for the regressions to be calculated on standard deviation
double xTwetMaxTempStdDev[7];
double xTdryMinTempStdDev[7];
double xTdryMaxTempStdDev[7];
double** wetTempStdDevRegMatrix; //X'*X matrix for the regressions on standard deviations
double** dryTempStdDevRegMatrix;

//create histograms of series length and precipitation
int wetLengthFreq[NUMMONTHS][NUMINTERVALS];
double wetLengthBounds[NUMMONTHS][NUMINTERVALS+1];
int dryLengthFreq[NUMMONTHS][NUMINTERVALS];
double dryLengthBounds[NUMMONTHS][NUMINTERVALS+1];
int rainFreq[NUMMONTHS][NUMINTERVALS];
double rainBounds[NUMMONTHS][NUMINTERVALS+1];
double rhoMinMin; //correlation between minimum temperatures on subsequent days
double rhoMaxMax; //correlation between maximum temperatures on subsequent days
double rhoDaily; //correlation between min and max temperatures on the same day
double rhoMinMax; //correlation of the min temperature with the max temperature on the next day
double rhoMaxMin; //correlation of the max temperature with the min temperature on the next day
double ssqMin, ssqMax;

double minTempWetParameters[7];
double maxTempWetParameters[7];
double minTempDryParameters[7];
double maxTempDryParameters[7];
double minTempWetStdDevParameters[7];
double maxTempWetStdDevParameters[7];
double minTempDryStdDevParameters[7];
double maxTempDryStdDevParameters[7];

double initMinTemp, initMaxTemp;

double** Cinv; // made global so it doesn't continually have to be created and distroyed

void _ErrMsg(char* s)  {
	// CString str;
	AfxMessageBox(s);
	exit(99);
}

void initData(void)
// sets all data to zero
{
	outfile.open(outfileStr);
	Cinv = dmatrix(3,3);
	//monthOrigin = 0; //stores the month in which the current series originates.  takes values 0-(NUMMONTHS-1).
	int i;
	for (i=1; i<NUMDAYS; i++){
		dailyStats[i].wetDays = 0;
		dailyStats[i].dryDays = 0;
	    dailyStats[i].wetMinTempSum = 0;
	    dailyStats[i].wetMaxTempSum = 0;
	    dailyStats[i].dryMinTempSum = 0;
	    dailyStats[i].dryMaxTempSum = 0;
	    dailyStats[i].wetMinTempStdDev = 0;
	    dailyStats[i].wetMaxTempStdDev = 0;
	    dailyStats[i].dryMinTempStdDev = 0;
	    dailyStats[i].dryMaxTempStdDev = 0;
	}
	for (i=0; i<12; ++i) {
		maxConsWetDays[i] = 0; //stores max # of consecutive wet days for a given month
		maxConsDryDays[i] = 0; //stores max # of consecutive dry days for a given month
		rainMax[i] = 0; //stores max amount of rain in cm for a given month
	}
	for (i=0; i<7; ++i) {
		xTwetMinTemp[i] = 0; //X'*Y -- ' denotes transpose
		xTwetMaxTemp[i] = 0;
		xTdryMinTemp[i] = 0;
		xTdryMaxTemp[i] = 0;
		xTwetMinTempStdDev[i] = 0; //X'*Y matrix for the regressions to be calculated on standard deviation
		xTwetMaxTempStdDev[i] = 0;
		xTdryMinTempStdDev[i] = 0;
		xTdryMaxTempStdDev[i] = 0;
	}
	wetRegMatrix = dmatrix(7,7);
	dryRegMatrix = dmatrix(7,7);
	initRegArrays(wetRegMatrix, dryRegMatrix);
	wetTempStdDevRegMatrix = dmatrix(7,7);
	dryTempStdDevRegMatrix = dmatrix(7,7);
	initRegArrays(wetTempStdDevRegMatrix, dryTempStdDevRegMatrix);
}


TamuWeather::TamuWeather(char* filename, int strm)
{
	//name = filename;
	double holdVal, holdMean, finalMin, finalMax;	
	int holdN;
	m_fileName = filename;
	

	//process data

	initData();

	m_yearCount = ReadInData(filename, finalMin, finalMax, true);

	//finish filling out the symmetric regression matrices for wet and dry series.
	constructRegMatrix(wetRegMatrix);
	constructRegMatrix(dryRegMatrix);

	//initialize arrays for bounds and frequencies of the semi-empirical distributions
	initializeBounds();
	initializeFreq();

	//process regression matrices for min and max temperature
	inv_in_place(wetRegMatrix,7);
	inv_in_place(dryRegMatrix,7);

	//calulate a0, a1, a2 regression coefficients for wet & dry, min & max mean temperatures
	calcParameters(minTempWetParameters, wetRegMatrix, xTwetMinTemp);
	calcParameters(maxTempWetParameters, wetRegMatrix, xTwetMaxTemp);
	calcParameters(minTempDryParameters, dryRegMatrix, xTdryMinTemp);
	calcParameters(maxTempDryParameters, dryRegMatrix, xTdryMaxTemp);

	//calculate the daily stan dev.  store by day into dailyStats. // historical data
	for (int i=1; i<NUMDAYS; ++i) {
		holdN = dailyStats[i].wetDays;
		if (holdN > 1) {
			holdMean = fourierFit(minTempWetParameters, i);
			holdVal = dailyStats[i].wetMinTempStdDev + holdN * holdMean * holdMean;
			holdVal -= 2.0 * dailyStats[i].wetMinTempSum * holdMean;
			dailyStats[i].wetMinTempStdDev = sqrt(holdVal / double(holdN-1));
			holdMean = fourierFit(maxTempWetParameters, i);
			holdVal = dailyStats[i].wetMaxTempStdDev + holdN * holdMean * holdMean;
			holdVal -= 2.0 * dailyStats[i].wetMaxTempSum * holdMean;
			dailyStats[i].wetMaxTempStdDev = sqrt(holdVal / double(holdN-1));
		} else {
			dailyStats[i].wetMinTempStdDev = -1.0;
			dailyStats[i].wetMaxTempStdDev = -1.0;
		}
		holdN = dailyStats[i].dryDays;
		if (holdN > 1) {
			holdMean = fourierFit(minTempDryParameters, i);
			holdVal = dailyStats[i].dryMinTempStdDev + holdN * holdMean * holdMean;
			holdVal -= 2.0 * dailyStats[i].dryMinTempSum * holdMean;
			dailyStats[i].dryMinTempStdDev = sqrt(holdVal / double(holdN-1));
			holdMean = fourierFit(maxTempDryParameters, i);
			holdVal = dailyStats[i].dryMaxTempStdDev + holdN * holdMean * holdMean;
			holdVal -= 2.0 * dailyStats[i].dryMaxTempSum * holdMean;
			dailyStats[i].dryMaxTempStdDev = sqrt(holdVal / double(holdN-1));
		} else {
			dailyStats[i].dryMinTempStdDev = -1.0;
			dailyStats[i].dryMaxTempStdDev = -1.0;
		}
	}
	
	for (int i=1; i<NUMDAYS; i++){
		if (dailyStats[i].wetMinTempStdDev != -1){
			constructRegMatrix(wetTempStdDevRegMatrix, xTwetMinTempStdDev, xTwetMaxTempStdDev, i, dailyStats[i].wetMinTempStdDev, dailyStats[i].wetMaxTempStdDev);
		}
		if (dailyStats[i].dryMinTempStdDev != -1){
			constructRegMatrix(dryTempStdDevRegMatrix, xTdryMinTempStdDev, xTdryMaxTempStdDev, i, dailyStats[i].dryMinTempStdDev, dailyStats[i].dryMaxTempStdDev);
		}
	}
	//finish filling in symmetric regression matrices
	constructRegMatrix(wetTempStdDevRegMatrix);
	constructRegMatrix(dryTempStdDevRegMatrix);

	inv_in_place(wetTempStdDevRegMatrix,7);
	inv_in_place(dryTempStdDevRegMatrix,7);

	//calulate a0, a1, a2 regression coefficients for wet & dry, min & max temperature standard deviations
	calcParameters(minTempWetStdDevParameters, wetTempStdDevRegMatrix, xTwetMinTempStdDev);
	calcParameters(maxTempWetStdDevParameters, wetTempStdDevRegMatrix, xTwetMaxTempStdDev);
	calcParameters(minTempDryStdDevParameters, dryTempStdDevRegMatrix, xTdryMinTempStdDev);
	calcParameters(maxTempDryStdDevParameters, dryTempStdDevRegMatrix, xTdryMaxTempStdDev);

	double prob = ReadAgain(filename, m_seriesLength, m_wetSeries);
	m_probRain = new Bernoulli(prob, strm); 
	//calculate 5 different correlations to be used 
	holdVal = sqrt(ssqMax*ssqMin);
	rhoMinMin /= ssqMin;
	rhoMaxMax /= ssqMax;
	rhoDaily /= holdVal;
	rhoMinMax /= holdVal;
	rhoMaxMin /= holdVal;
	
	/*To check correlations*/
	//cout << "correlation for min temperatures: " << rhoMinMin << endl;
	//cout << "correlation for max temperatures: " << rhoMaxMax << endl;
	//cout << "daily correlation for temperatures: " << rhoDaily << endl; //should be close to 0.6
	//cout << "correlation for minmax temperatures: " << rhoMinMax << endl;
	//cout << "correlation for maxmin temperatures: " << rhoMaxMin << endl;

	// determine data to start new year in case that is desired
	if(m_wetSeries){
		m_minPrevResidual = finalMin - fourierFit(minTempWetParameters, NUMDAYS-1);
		m_minPrevStdDev = fourierFit(minTempWetStdDevParameters, NUMDAYS-1);
		m_maxPrevResidual = finalMax - fourierFit(maxTempWetParameters, NUMDAYS-1);
		m_maxPrevStdDev = fourierFit(maxTempWetStdDevParameters, NUMDAYS-1);
	}
	else{
		m_minPrevResidual = finalMin - fourierFit(minTempDryParameters, NUMDAYS-1);
		m_minPrevStdDev = fourierFit(minTempDryStdDevParameters, NUMDAYS-1);
		m_maxPrevResidual = finalMax - fourierFit(maxTempDryParameters, NUMDAYS-1);
		m_maxPrevStdDev = fourierFit(maxTempDryStdDevParameters, NUMDAYS-1);
	}

	int i;
	m_tempDist = new Normal(strm);
	for (i=0; i<NUMMONTHS; ++i) {
		m_wetDist[i] = new SemiEmpirical(wetLengthBounds[i], wetLengthFreq[i], 
			                                                NUMINTERVALS, strm);
		m_dryDist[i] = new SemiEmpirical(dryLengthBounds[i], dryLengthFreq[i], 
			                                                NUMINTERVALS, strm);
		m_rainDist[i] = new SemiEmpirical(rainBounds[i], rainFreq[i], 
			                                                NUMINTERVALS, strm);
	}
	for (i=0; i<7; ++i) {
		m_minWetParameters[i] = minTempWetParameters[i];
		m_maxWetParameters[i] = maxTempWetParameters[i];
		m_minDryParameters[i] = minTempDryParameters[i];
		m_maxDryParameters[i] = maxTempDryParameters[i];
		m_minWetStdDevParameters[i] = minTempWetStdDevParameters[i];
		m_maxWetStdDevParameters[i] = maxTempWetStdDevParameters[i];
		m_minDryStdDevParameters[i] = minTempDryStdDevParameters[i];
		m_maxDryStdDevParameters[i] = maxTempDryStdDevParameters[i];
	}
	m_rhoMin = rhoMinMin;
	m_rhoMax = rhoMaxMax;
	m_rhoDaily = rhoDaily;
	m_rhoMinMax = rhoMinMax;
	m_rhoMaxMin = rhoMaxMin;

	// seriesLength, at end of historical data, represents length of final
	//  cycle, we need to convert it to length remaining, thus, generate
	//  the conditional length given it is at least as long from historical data
	if (m_wetSeries)
			m_seriesLength = (int)(m_wetDist[11]->Generate(m_seriesLength) + 0.5) 
			                                 - m_seriesLength; 	
		else
			m_seriesLength = (int)(m_dryDist[11]->Generate(m_seriesLength) + 0.5) 
			                                 - m_seriesLength;	
}

TamuWeather::~TamuWeather(void)
{
	delete m_probRain;
	delete m_tempDist;
	for (int i=0; i<NUMMONTHS; ++i) {
		delete m_wetDist[i];
		delete m_dryDist[i];
		delete m_rainDist[i];
	}
}

void TamuWeather::GenerateDaily(double minT[], double maxT[], double rain[], bool randomStart)
{
	int day=1;
	int year=0;
	int month,d,i ;
	double newMinMean, newMaxMean, newMinStdev, newMaxStdev;
	double newMinVar, newMaxVar;
	double ratio, term1, term2;
	double Tvect[3]; //T vector used in calculating maximum temperature mean following method described in Scheuer and Stroller (1962)
	double VCinv[3]; // result of vector V times matrix Cinv
	double minTempMean; //stores minimum temperature mean
	double minTempStdDev; //stores minimum temperature standard deviation
	double minTemp; //stores generated minimum temperature ~N(minTempMean, minTempStdDev)
	double maxTempMean; //stores maximum temperature mean
	double maxTempStdDev; //stores maximum temperature standard deviation
	double maxTemp; //stores generated maximum temperature ~N(maxTempMean, maxTempStdDev)
	//generate weather
	//choose method via boolean value newBeginning
	//newBeginning = true: start generating weather on Jan. 1, deriving temeperature and series length data from the last year of input data.
	//newBeginning = false: start generating weather on Dec. 1 of the previous year to introduce randomness into the model.  In other words, calculate any year, not just the year after the input data
	if (randomStart) {
		// generate random stuff on Dec 31 for starting new year
		//  will assume that Dec is first day of new series of random length
		if (m_probRain->Generate()) {
			m_wetSeries = true;
			m_seriesLength = (int)(m_wetDist[11]->Generate() + 0.5);
			newMinMean = fourierFit(m_minWetParameters, NUMDAYS-1);
			newMinStdev = fourierFit(m_minWetStdDevParameters, NUMDAYS-1);
			newMaxMean = fourierFit(m_maxWetParameters, NUMDAYS-1);
			newMaxStdev = fourierFit(m_maxWetStdDevParameters, NUMDAYS-1);
		}
		else  {
			m_wetSeries = false;
			m_seriesLength = (int)(m_dryDist[11]->Generate() + 0.5);
			newMinMean = fourierFit(m_minDryParameters, NUMDAYS-1);
			newMinStdev = fourierFit(m_minDryStdDevParameters, NUMDAYS-1);
			newMaxMean = fourierFit(m_maxDryParameters, NUMDAYS-1);
			newMaxStdev = fourierFit(m_maxDryStdDevParameters, NUMDAYS-1);
		}
	
		m_minPrevResidual = m_tempDist->Generate(newMinMean, newMinStdev)
			                                                   - newMinMean;
		ratio = newMaxStdev / newMinStdev;
		maxTempMean = newMaxMean + m_rhoDaily * ratio * m_minPrevResidual;
		maxTempStdDev = newMaxStdev * sqrt(1.0 - m_rhoDaily*m_rhoDaily);
		m_maxPrevResidual = m_tempDist->Generate(maxTempMean, maxTempStdDev)
			                                                    - newMaxMean;
		m_minPrevStdDev = newMinStdev;
		m_maxPrevStdDev = newMaxStdev;
		--m_seriesLength;
	}                    // end random start

	//generate weather using SemiEmpirical distrbutions and minTemp/maxTemp parameters
	//to introduce randomness on Jan 1, start generating from December 31st of the previous year
	for (year=0; year<GENERATEYEARS; ++year) {
	 day = 1;
	 for (month=0; month<12; ++month) {
	  for (d=0; d<daysPerMonth[month]; ++d) {
		//month = FindMonth(day);
		//if there are no more days left in a series, 
		//  then switch to a series of the different type and generate the new series length
		if (m_seriesLength <= 0){
			m_wetSeries = !m_wetSeries;
			if (m_wetSeries)
				m_seriesLength = (int)(m_wetDist[month]->Generate() + 0.5);
			else
				m_seriesLength = (int)(m_dryDist[month]->Generate() + 0.5);
		}
		//generate min & max temperature according to a normal distribution
		//means and standard deviations calculated by a method described in 
		//"On the Generation of Normal Random Vectors" by Scheuer & Stoller (1962)
		//generates precipation according to a SemiEmpirical distribution
		
		if(m_wetSeries){
			rain[day] = m_rainDist[month]->Generate();  
			// mean and stdev if no correlation - wet
			newMinMean = fourierFit(m_minWetParameters, day);
			newMaxMean = fourierFit(m_maxWetParameters, day);
			newMinStdev = fourierFit(m_minWetStdDevParameters, day);
			newMaxStdev = fourierFit(m_maxWetStdDevParameters, day);
		}
		else {
			rain[day] = 0.0;
			// mean and stdev if no correlation - dry
			newMinMean = fourierFit(m_minDryParameters, day);
			newMaxMean = fourierFit(m_maxDryParameters, day);
			newMinStdev = fourierFit(m_minDryStdDevParameters, day);
			newMaxStdev = fourierFit(m_maxDryStdDevParameters, day);
		}
		newMinVar = newMinStdev*newMinStdev;
		newMaxVar = newMaxStdev*newMaxStdev;
		// min mean with correlation
		ratio = newMinStdev /(1.0-m_rhoDaily*m_rhoDaily);
		term1 = (m_rhoMaxMin - m_rhoDaily*m_rhoMin) 
		              * (m_maxPrevResidual / m_maxPrevStdDev);
	    term2 = (m_rhoMin - m_rhoDaily*m_rhoMaxMin)
		              * (m_minPrevResidual / m_minPrevStdDev);
	    minTempMean = newMinMean + (term1 + term2) * ratio;
		// min stdev with correlation
	    ratio = newMinVar / (1.0 - m_rhoDaily*m_rhoDaily);
	    term1 = m_rhoMaxMin*(m_rhoMaxMin - m_rhoDaily*m_rhoMin);
	    term2 = m_rhoMin*(m_rhoMin - m_rhoDaily*m_rhoMaxMin);
	    minTempStdDev = sqrt(newMinVar - (term1 + term2)*ratio);

		minTemp = m_tempDist->Generate(minTempMean, minTempStdDev);

		// max mean and stdev with correlation
		createTvect(Tvect, minTemp, newMinMean, 
				                 m_minPrevResidual, m_maxPrevResidual);
		maxTempStdDev = newMaxVar - createV_Cinv(VCinv, newMinStdev, newMaxStdev,  
				                      m_minPrevStdDev, m_maxPrevStdDev, m_rhoDaily,
				                       m_rhoMin, m_rhoMax, m_rhoMinMax, m_rhoMaxMin); 
		// note that when the matrix product V * Cinv is created, it also returns
		// the value of V*Cinv*V-transpose
		maxTempStdDev = sqrt(maxTempStdDev);
		//maxTempMean = newMaxMean + matrixProduct(Vvect, CinvMatrix, Tvect);
		maxTempMean = newMaxMean;
		for (i=0; i<3; ++i) 
			maxTempMean += VCinv[i]*Tvect[i];
		//maxTempStdDev = sqrt(newMaxVar - matrixProduct(Vvect, CinvMatrix, Vvect));

		maxTemp = m_tempDist->Generate(maxTempMean, maxTempStdDev);

		if (maxTemp < minTemp){
			maxTemp = (minTemp + maxTemp) / 2.0;
			minTemp = maxTemp;
		}
		m_minPrevResidual = minTemp - newMinMean;
		m_maxPrevResidual = maxTemp - newMaxMean;
		m_minPrevStdDev = newMinStdev;
		m_maxPrevStdDev = newMaxStdev;
			//store in arrays
		minT[day] = minTemp;
		maxT[day] = maxTemp;

		if (outfile)  
		    outfile << year << ",  " << day << ",  " << minTemp << ",  "
			        << maxTemp << ",  " << rain[day] << endl;
	
		
		//decrement series length, go to the next day
		m_seriesLength--;
		day++;
	  }}}
	outfile.close();
}


int ReadInData(char* fileName, double& finMin, double& finMax)
// readin historical data and calculate quantities dependent on daily data
// returns number of years of data that were read in
// finMin and finMax refer to temperature on last day
//   used to start the next year if it is desired to continue
{
	int consWetDays = 0; //counter for consecutive wet days
	int consDryDays = 0; //counter for consecutive dry days
	ifstream inFile; //stream of incoming empirical weather data
	inFile.open(fileName);
	if (bheaderrow == true)
	{
		char buff[1000];
		inFile.readline(buff);

	}

	double holdMax, holdMin, scratch1, holdRain, scratch2, scratch3;
	int y=0, d, holdD, yearCount=0;
	int monthOrigin=0;
	long scratch4;
	while (inFile.good())
	{
		inFile >> y >> d >> holdMax >> holdMin >> scratch1 >> holdRain 
			                                 >> scratch2 >> scratch3 >> scratch4; 
		if (y<0) break;
		if (d == 365) {
			++yearCount;
		}
		if (d < 0)  {
			_ErrMsg("Error: negative daily index while reading in historical temperatures.");
		}
			//if (year%4 == 0 && day > 60) day -= 1; 
		//throws out Feb. 29 in case data contains leap years.  
		//Data from Daymet website does not include leap years.
		if (holdMax < holdMin) {
			_ErrMsg("Error: maximum temperature is reported below minimum temperature.");
		}
		if (holdRain < 0.0) {
			_ErrMsg("Error: negative precipitation reported.");
		}
		finMin = holdMin; finMax = holdMax;
		//calculate maximum values to be used as upper bounds in processing phase for the semi-empirical distributions
		if (holdRain > EPS){
			constructRegMatrix(wetRegMatrix, xTwetMinTemp, xTwetMaxTemp, d, holdMin, holdMax);
			consWetDays++;
			//if the number of consecutive dry days is nonzero, then it must be the start of a new series.  Therefore, change the month of origin to the current month and set consDryDays to 0.
			if (consDryDays > 0){
				monthOrigin = _FindMonth(d);
				consDryDays = 0;
			}
			//store in the maxConsWetDays array for a given month if the series is the longest instance ever in that month.
			if (consWetDays > maxConsWetDays[monthOrigin])
				maxConsWetDays[monthOrigin] = consWetDays;

            // accumulate for daily standard deviations
			dailyStats[d].wetMinTempSum += holdMin;
			dailyStats[d].wetMaxTempSum += holdMax;
			dailyStats[d].wetMinTempStdDev += holdMin*holdMin;
			dailyStats[d].wetMaxTempStdDev += holdMax*holdMax;
			++dailyStats[d].wetDays;
		}
		else {
			constructRegMatrix(dryRegMatrix, xTdryMinTemp, xTdryMaxTemp, d, holdMin, holdMax);
			consDryDays++;
			//if the number of consecutive wet days is nonzero, then it must be the start of a new series.  Therefore, change the month of origin to the current month and set consWetDays to 0.
			if (consWetDays > 0){
				monthOrigin = _FindMonth(d);
				consWetDays = 0;
			}
			//store in the maxConsWetDays array for a given month if the series is the longest instance ever in that month.
			if (consDryDays > maxConsDryDays[monthOrigin])
				maxConsDryDays[monthOrigin] = consDryDays;
			
            // accumulate for daily standard deviations
			dailyStats[d].dryMinTempSum += holdMin;
			dailyStats[d].dryMaxTempSum += holdMax;
			dailyStats[d].dryMinTempStdDev += holdMin*holdMin;
			dailyStats[d].dryMaxTempStdDev += holdMax*holdMax;
			++dailyStats[d].dryDays;
		}
		//if rain amount is on this day is greater than the most rain on this month, store in rainMax array
		holdD = _FindMonth(d);
		if (holdRain > rainMax[holdD]){
			rainMax[holdD] = holdRain;
		}
	}

	if (y>=0) 
	{
		_ErrMsg("ERROR: Either no input file or it did not end with -1.");
	}
	//finish filling out the symmetric regression matrices for wet and dry series.
 
	//close reading stream
	inFile.close();
	//cout << "\n Parameters based on " << yearCount << " years of data." << endl; 
	return yearCount;
}

double ReadAgain(char* fileName, int& length, bool& wet)
// return value is probability of rain on first day, used if random start
// readin historical data and calculate quantities dependent on daily data
// if wet=true, the final year ended wet and there had been length number of wet days
// if wet=false, the year ended dry with length equal to number of dry days
{
	ifstream inFile; //stream of incoming empirical weather data
	inFile.open(fileName);
	double residualMin, oldMin;
	double residualMax, oldMax;
	int sumRain=0, kount=0;
	int currentMonth;
	int monthOrigin = 0; //stores the month in which the current series started
	int lengthWetSeries = 0; //incrementor counting length of a wet series
	int lengthDrySeries = 0; //incrementor counting length of dry series
	int interval; //stores interval in which a data member should be placed

	double holdMax, holdMin, scratch1, holdRain, scratch2, scratch3;
	int y, d;
	long scratch4;
	bool firstDataPoint=true; 
	while (inFile.good()){
		inFile >> y >> d >> holdMax >> holdMin >> scratch1 >> holdRain 
			                                 >> scratch2 >> scratch3 >> scratch4; 
		if (y<0) break;
		
		if (d < 0)  {
			_ErrMsg("Error2: negative index during second read of historical temperatures."); 
		}
		//Data from Daymet website does not include leap years.
		// now determine build frequencies for semi-empirical distributions
		currentMonth = _FindMonth(d);
		if (holdRain > EPS){
			lengthWetSeries++;
			if (lengthDrySeries > 0){
				interval = GetInterval(lengthDrySeries, dryLengthBounds[monthOrigin]);
				dryLengthFreq[monthOrigin][interval]++;
				lengthDrySeries = 0;
				monthOrigin = _FindMonth(d);
			}
			interval = GetInterval(holdRain, rainBounds[currentMonth]);
			rainFreq[currentMonth][interval]++;
			if ((d<5) || (d>360)) {
				++sumRain;
				++kount;
			}
		}
		else{
			lengthDrySeries++;
			if (lengthWetSeries > 0){
				interval = GetInterval(lengthWetSeries, wetLengthBounds[monthOrigin]);
				wetLengthFreq[monthOrigin][interval]++;
				lengthWetSeries = 0;
				monthOrigin = _FindMonth(d);
			}
			if ((d<5) || (d>360)) {
				++kount;
			}
		}   // end of rain frequencies

		//  now build sums for temperature correlations
		residualMin = calcResiduals(d, holdMin, holdRain, true);
		residualMax = calcResiduals(d, holdMax, holdRain, false);
		if (firstDataPoint) {
			firstDataPoint = false;
			ssqMin = residualMin*residualMin;
			ssqMax = residualMax*residualMax;
	        rhoDaily = residualMin*residualMax;
			rhoMinMin=0.0;
	        rhoMaxMax=0.0;
	        rhoMinMax=0.0;
	        rhoMaxMin=0.0;
		}
		else {
			ssqMin += residualMin*residualMin;
			ssqMax += residualMax*residualMax;
	        rhoDaily += residualMin*residualMax;
			rhoMinMin += oldMin*residualMin;
	        rhoMaxMax += oldMax*residualMax;
	        rhoMinMax += oldMin*residualMax;
	        rhoMaxMin += oldMax*residualMin;
		}
		oldMin = residualMin;
		oldMax = residualMax;
		initMinTemp = holdMin;
		initMaxTemp = holdMax;

	}    // end while
	//set the series length to 
	if (lengthWetSeries > 0) {
		length = lengthWetSeries;
		wet = true;
	}
	else if(lengthDrySeries > 0) {
		length = lengthDrySeries;
		wet = false;
	}
	else {
		_ErrMsg("ERROR:  cannot end with serisLength=0 (see ReadAgain()).");
		length = 0;
	}
	//close reading stream
	inFile.close();
	return (double)sumRain / (double)kount;
}


void initRegArrays(double** regArray1, double** regArray2)
//initializes two 3x3 regression matrices.  sets all values to zero.
//it is an unchecked runtime error for regArrays to be smaller than a 3x3 matrix
{
	for (int i=0; i<7; i++){
		for (int j=0; j<7;j++){
			regArray1[i][j] = 0;
			regArray2[i][j] = 0;
		}
	}
}



void constructRegMatrix(double** regMatrix, double xTymin[], double xTymax[], int day, double ymin, double ymax)
//constructs regression matrices X'*Y and X'*X for arrays declared in the main program. ' denotes a transposed matrix.
//it is an unchecked runtime error for regMatrix, xTymin, and xTymax to be smaller than 3x3, 3x1, and 3x1 matrices, respectively
{
	regMatrix[0][0] += 1.0;
	regMatrix[0][1] += cos(2.0 * PI * (double)day / 365.0);
	regMatrix[0][2] += sin(2.0 * PI * (double)day / 365.0);
	regMatrix[0][3] += cos(2 * 2.0 * PI * (double)day / 365.0);
	regMatrix[0][4] += sin(2 * 2.0 * PI * (double)day / 365.0);
	regMatrix[0][5] += cos(3 * 2.0 * PI * (double)day / 365.0);
	regMatrix[0][6] += sin(3 * 2.0 * PI * (double)day / 365.0);
	regMatrix[1][1] += cos(2.0 * PI * (double)day / 365.0) * cos(2.0 * PI * (double)day / 365.0);
	regMatrix[1][2] += cos(2.0 * PI * (double)day / 365.0) * sin(2.0 * PI * (double)day / 365.0);
	regMatrix[1][3] += cos(2.0 * PI * (double)day / 365.0) * cos(2 * 2.0 * PI * (double)day / 365.0);
	regMatrix[1][4] += cos(2.0 * PI * (double)day / 365.0) * sin(2 * 2.0 * PI * (double)day / 365.0);
	regMatrix[1][5] += cos(2.0 * PI * (double)day / 365.0) * cos(3 * 2.0 * PI * (double)day / 365.0);
	regMatrix[1][6] += cos(2.0 * PI * (double)day / 365.0) * sin(3 * 2.0 * PI * (double)day / 365.0);
	regMatrix[2][2] += sin(2.0 * PI * (double)day / 365.0) * sin(2.0 * PI * (double)day / 365.0);
	regMatrix[2][3] += sin(2.0 * PI * (double)day / 365.0) * cos(2 * 2.0 * PI * (double)day / 365.0);
	regMatrix[2][4] += sin(2.0 * PI * (double)day / 365.0) * sin(2 * 2.0 * PI * (double)day / 365.0);
	regMatrix[2][5] += sin(2.0 * PI * (double)day / 365.0) * cos(3 * 2.0 * PI * (double)day / 365.0);
	regMatrix[2][6] += sin(2.0 * PI * (double)day / 365.0) * sin(3 * 2.0 * PI * (double)day / 365.0);
	regMatrix[3][3] += cos(2 * 2.0 * PI * (double)day / 365.0) * cos(2 * 2.0 * PI * (double)day / 365.0);
	regMatrix[3][4] += cos(2 * 2.0 * PI * (double)day / 365.0) * sin(2 * 2.0 * PI * (double)day / 365.0);
	regMatrix[3][5] += cos(2 * 2.0 * PI * (double)day / 365.0) * cos(3 * 2.0 * PI * (double)day / 365.0);
	regMatrix[3][6] += cos(2 * 2.0 * PI * (double)day / 365.0) * sin(3 * 2.0 * PI * (double)day / 365.0);
	regMatrix[4][4] += sin(2 * 2.0 * PI * (double)day / 365.0) * sin(2 * 2.0 * PI * (double)day / 365.0);
	regMatrix[4][5] += sin(2 * 2.0 * PI * (double)day / 365.0) * cos(3 * 2.0 * PI * (double)day / 365.0);
	regMatrix[4][6] += sin(2 * 2.0 * PI * (double)day / 365.0) * sin(3 * 2.0 * PI * (double)day / 365.0);
	regMatrix[5][5] += cos(3 * 2.0 * PI * (double)day / 365.0) * cos(3 * 2.0 * PI * (double)day / 365.0);
	regMatrix[5][6] += cos(3 * 2.0 * PI * (double)day / 365.0) * sin(3 * 2.0 * PI * (double)day / 365.0);
	regMatrix[6][6] += sin(3 * 2.0 * PI * (double)day / 365.0) * sin(3 * 2.0 * PI * (double)day / 365.0);
	xTymin[0] += ymin;
	xTymin[1] += ymin * cos(2.0 * PI * (double)day / 365.0);
	xTymin[2] += ymin * sin(2.0 * PI * (double)day / 365.0);
	xTymin[3] += ymin * cos(2 * 2.0 * PI * (double)day / 365.0);
	xTymin[4] += ymin * sin(2 * 2.0 * PI * (double)day / 365.0);
	xTymin[5] += ymin * cos(3 * 2.0 * PI * (double)day / 365.0);
	xTymin[6] += ymin * sin(3 * 2.0 * PI * (double)day / 365.0);
	xTymax[0] += ymax;
	xTymax[1] += ymax * cos(2.0 * PI * (double)day / 365.0);
	xTymax[2] += ymax * sin(2.0 * PI * (double)day / 365.0);
	xTymax[3] += ymax * cos(2 * 2.0 * PI * (double)day / 365.0);
	xTymax[4] += ymax * sin(2 * 2.0 * PI * (double)day / 365.0);
	xTymax[5] += ymax * cos(3 * 2.0 * PI * (double)day / 365.0);
	xTymax[6] += ymax * sin(3 * 2.0 * PI * (double)day / 365.0);
}

void constructRegMatrix(double** regMatrix)
//finishes constructing a symmetric regression matrix
{
	regMatrix[1][0] = regMatrix[0][1];
	regMatrix[2][0] = regMatrix[0][2];
	regMatrix[2][1] = regMatrix[1][2];
	regMatrix[3][0] = regMatrix[0][3];
	regMatrix[3][1] = regMatrix[1][3];
	regMatrix[3][2] = regMatrix[2][3];
	regMatrix[4][0] = regMatrix[0][4];
	regMatrix[4][1] = regMatrix[1][4];
	regMatrix[4][2] = regMatrix[2][4];
	regMatrix[4][3] = regMatrix[3][4];
	regMatrix[5][0] = regMatrix[0][5];
	regMatrix[5][1] = regMatrix[1][5];
	regMatrix[5][2] = regMatrix[2][5];
	regMatrix[5][3] = regMatrix[3][5];
	regMatrix[5][4] = regMatrix[4][5];
	regMatrix[6][0] = regMatrix[0][6];
	regMatrix[6][1] = regMatrix[1][6];
	regMatrix[6][2] = regMatrix[2][6];
	regMatrix[6][3] = regMatrix[3][6];
	regMatrix[6][4] = regMatrix[4][6];
	regMatrix[6][5] = regMatrix[5][6];
}

void initializeBounds(void)//(double wetLengthBounds[][NUMINTERVALS+1], double dryLengthBounds[][NUMINTERVALS+1], double rainBounds[][NUMINTERVALS+1], int maxConsWetDays[], int maxConsDryDays[], double rainMax[])
//initializes the bounds array for the semi-empirical distribution
//bounds for series lengths can either be the result of a slight modification 
//  of our algorithm for rain or an algorithm that duplicates bounds used by LARS-WG. 
//  this option may be toggled by de-commenting, commenting the appropriate 
//      function calls within this function.
//for rain: to avoid coarse resolution of lower values, 
//  determine bounds in the follwing manner: for a given month, 
// take the maximum rain ever occurred for that month and set it as the highest bound. 
//  Let n = highest bound / 10.  
// starting at zero, bounds are incremented in the following fashion: 
//      1/2*n, 1/2*n, 3/4*n, 3/4*n, n, n, 5/4*n, 5/4*n, 3/2*n, 3/2*n
{
	for (int i=0; i<NUMMONTHS; i++){
		//set lower and upper bounds
		wetLengthBounds[i][0] = 0.5;
		dryLengthBounds[i][0] = 0.5;
		rainBounds[i][0] = 0;
		rainBounds[i][NUMINTERVALS] = rainMax[i];

		/*method to obtain LARS-WG bounds*/
		assignLARSbounds(wetLengthBounds[i], maxConsWetDays[i]);
		assignLARSbounds(dryLengthBounds[i], maxConsDryDays[i]);
		
		//assign intervals for rain using the methodology described above
		for(int j=1; j<NUMINTERVALS; j++)
			rainBounds[i][j] = rainBounds[i][j-1] 
		             + (0.5 + 0.25*(double)((j-1)/2))*rainMax[i]/(double)NUMINTERVALS;
	}
}

void assignLARSbounds(double bounds[], int maxConsDays)
//takes in maximum amount of consecutive wet or dry days and stores bound values in bounds[]
//each bound is slightly larger than the preceeding bound, typically one or two units wider than the preceeding boumd
{
	int adjustment = 0; //keeps track of how much needs to be added to NUMINTERVALS to include maxConsDays
	int finalAdj = 0; //stores the width of the last interval's adjustment (i.e., if finalAdj is 6, interval NUMINTERVALS will have a width of 7 provided iterations is 0)
	int iterations = 0; //keeps track of how many times finalAdj reaches NUMINTERVALS-1, the maximum amount of adjustment in one step allowed by the algorithm
	while (NUMINTERVALS + adjustment < maxConsDays){
		//increment finalAdj and add it to the cumulative adjustment
		adjustment += ++finalAdj;
		//if the maximum one-step finalAdj is reached, increment iterations and reset finalAdj
		if (finalAdj >= NUMINTERVALS-1){
			iterations++;
			finalAdj = 0;
		}
	}
	//for each entry, add 1) the previous entry, 2) the base difference of 1, 3) an adjustment based on any iterations that may have occured, 4) an adjustment based on final adjustment
	for(int j=1; j<NUMINTERVALS+1; j++)
		bounds[j] = bounds[j-1] + 1 + iterations * (j-1) + max(finalAdj + (j-NUMINTERVALS),0);
}


void initializeFreq(void)//int wetLengthFreq[][NUMINTERVALS], int dryLengthFreq[][NUMINTERVALS], int rainFreq[][NUMINTERVALS])
//sets all frequency matrices to zero
{
	for (int i=0; i<NUMMONTHS; i++)
		for(int j=0; j<NUMINTERVALS; j++){
			wetLengthFreq[i][j] = 0;
			dryLengthFreq[i][j] = 0;
			rainFreq[i][j] = 0;
		}
}

void calcParameters(double parameters[], double** regMatrix, double xTy[])
//calculates a0, a1, and a2 parameters for the regression given regMatrix (X'*X) and a given X'*Y.  results stored in double parameters[3]
//it is an unchecked runtime error for any matrix to have a dimension smaller than 3
{
	for (int i=0; i<7; i++)
		parameters[i] = regMatrix[i][0]*xTy[0] + regMatrix[i][1]*xTy[1] + regMatrix[i][2]*xTy[2] + regMatrix[i][3]*xTy[3] + regMatrix[i][4]*xTy[4] + regMatrix[i][5]*xTy[5] + regMatrix[i][6]*xTy[6];
}


double standardize(double temp, double meanPara[], double stdDevPara[], int day)
//returns a normalized residual given the day and 
//regression parameters for the mean and standard deviation
{
	double mean = fourierFit(meanPara, day);
	double stdDev = fourierFit(stdDevPara, day);
	return (temp - mean)/stdDev;
}

double calcResiduals(int day, double temp, double rain, bool minimum)
//calculates normalized residuals of temperatures for every day.  takes in flag boolean that indicates whether to calculate parameters for maximum or minimum temperatures.
{
	if (rain > EPS) {
		if (minimum) 
			return standardize(temp, minTempWetParameters, minTempWetStdDevParameters, day);
		else 
			return standardize(temp, maxTempWetParameters, maxTempWetStdDevParameters, day);
	}
	else {
		if (minimum) 
			return standardize(temp, minTempDryParameters, minTempDryStdDevParameters, day);
		else 
			return standardize(temp, maxTempDryParameters, maxTempDryStdDevParameters, day);
	}
}



double fourierFit(double parameters[], int index)
//returns a regression mean given the regression parameters and an index (for our purposes, the day)
{
	return parameters[0] + parameters[1]*cos(2.0 * PI * (double)index / 365.0) 
		       + parameters[2]*sin(2.0 * PI * (double)index / 365.0) 
			      + parameters[3]*cos(2* 2.0 * PI * (double)index / 365.0) 
				     + parameters[4]*sin(2* 2.0 * PI * (double)index / 365.0) 
					    + parameters[5]*cos(3* 2.0 * PI * (double)index / 365.0) 
						   + parameters[6]*sin(3* 2.0 * PI * (double)index / 365.0);
}


void createTvect(double Tvect[], double minTemp, double minMean, 
				        double minTempPrevResidual, double maxTempPrevResidual)
//creates Tvect, the vector of temperature residuals that is later used to calculate 
//   the mean of the max temperature
{
	Tvect[0] = minTemp - minMean;
	Tvect[1] = maxTempPrevResidual;
	Tvect[2] = minTempPrevResidual;
} 


double createV_Cinv(double VC[], double newMinStd, double newMaxStd,  
				         double minPrevStd, double maxPrevStd, double rhoDaily,
				         double rhoMin, double rhoMax, double rhoMinMx, double rhoMxMn)
// creates V matrix, then C matrix, takes inverse of C matrix, finally multiplies V*Cinv
//  VC[] is output vector containing results (3 components)
// returns the value of V times Cinv times V-transpose
{
	double vec[3], rc=0.0;
	vec[0] = rhoDaily * newMinStd * newMaxStd;
	vec[1] = rhoMax * maxPrevStd * newMaxStd;
	vec[2] = rhoMinMx * minPrevStd * newMaxStd;
	
	Cinv[0][0] = newMinStd*newMinStd;
	Cinv[1][1] = maxPrevStd * maxPrevStd;
	Cinv[2][2] = minPrevStd * minPrevStd;
	Cinv[0][1] = rhoMxMn * maxPrevStd * newMinStd;
	Cinv[0][2] = rhoMin * minPrevStd * newMinStd;
	Cinv[1][2] = rhoDaily * maxPrevStd * minPrevStd;
	Cinv[1][0] = Cinv[0][1];
	Cinv[2][0] = Cinv[0][2];
	Cinv[2][1] = Cinv[1][2];
	inv_in_place(Cinv,3);
	int i,j;
	for (j=0; j<3; ++j) {
		VC[j] = 0.0;
		for (i=0; i<3; ++i)
			VC[j] += vec[i]*Cinv[i][j];
		rc += VC[j] * vec[j];
	}
	return rc;
}


int _FindMonth(int date)   // date is julian date
// month 0 is Jan, month 11 is Dec
{
	for (int i=0; i<11; ++i)  
		if (date <= jul[i])
            return i;
    return 11;
}

//  program is based on modifications of the numerical recipes program
//  'gaussj'.  Modified so that it only gets inverse and it does not solve a 
//  linear system; also, it uses double precision although it returns 
//  single precision matrix.
    
//  aa is input matrix of dimension n x n.  
//  On output, the matrix contains the inverse of the input.


#define ZERO 1.0e-7
#define SWAP(a,b) {double temp=(a);(a)=(b);(b)=temp;}

short* svector(short dim);
void free_svector(short* the_vector, short dim);

void inv_in_place(double** mat, short dim)
//  the input matrix "a" will be destroyed and its inverse
//  will be placed in "a".  The matrix is of dimension dim x dim
{
	short *indxc, *indxr, *ipiv;
	int i,jrow,j,k;
	double big,dum,pivinv,anorm;
	
	anorm = 0;
	for (i=0;i<dim;++i)   
	  for (j=0;j<dim;++j) 
	  {
	     big = fabs(mat[i][j]);
	     if ( big>anorm ) anorm = big;
	   }

	indxc=svector(dim);
	indxr=svector(dim);
	ipiv=svector(dim);

	for (j=0;j<dim;j++) ipiv[j]=0;   //  sets all pivots to zero  
	for (k=0;k<dim;k++) {        //  row index  
		big=fabs(mat[k][k]);  jrow = k;
		for (i=k+1;i<dim;i++) {
						if (fabs(mat[i][k]) > big) {
							big=fabs(mat[i][k]);
							jrow = i;
						}
					}   //  found largest element in col.  
	    if (big <= ZERO)  
		{
			printf("\nsingular or near singular in  inv_in_place\n");
			exit(1);
		}
		indxr[k] = jrow;
	    if (jrow > k)    
			for (j=0;j<dim;++j)  SWAP(mat[k][j],mat[jrow][j]);
	             //   now transformation    
		pivinv=1.0/mat[k][k];
		mat[k][k]=1.0;
		for (j=0;j<dim;++j) mat[k][j] *= pivinv;
		for (i=0;i<dim;i++)
			if (i != k) {
				dum=mat[i][k];
				mat[i][k]=0.0;
				for (j=0;j<dim;j++) mat[i][j] -= mat[k][j]*dum;
			}
	}     //  end all pivot operations  
	for (j=dim-1;j>=0;--j) {
	    if (j != indxr[j])  for (i=0,k=indxr[j];i<dim;++i)  SWAP(mat[i][j],mat[i][k]);
	}

	free_svector(ipiv,dim);
	free_svector(indxr,dim);
	free_svector(indxc,dim);
}


double** dmatrix(short num_rows, short num_columns)
{
	short i;

	double** the_matrix;

	the_matrix = (double**) malloc( num_rows*sizeof(double*));
	if (!the_matrix)
	{
		printf( "\n*** ERROR in allocation of memory (1) \n");
		exit(1);
	}

	for (i=0; i<num_rows; ++i)
	{
		the_matrix[i]=(double*) malloc( num_columns*sizeof(double));
		if (!the_matrix[i])
		{
			printf( "\n*** ERROR in allocation of memory (2)\n");
			exit(1);
		}
	}
	return the_matrix;
}

void free_dmatrix(double** the_matrix, short num_rows, short num_col)
{
	short i;
	for (i=num_rows-1; i>=0; --i)
		free( (char*) the_matrix[i] );
	free( (char*) the_matrix );
}


short* svector(short dim)
//  allocates space for a vector of short of dimension dim
{
	short* the_vector;
	the_vector = (short*)malloc(dim*sizeof(short));
	if (!the_vector)
	{
		printf("\n***ERROR in allocation of space for vector\n");
		exit(1);
	}
	return the_vector;
}

void free_svector(short* the_vector, short dim)
// releases space that was allocated to a vector of short
{
	free( (char*) the_vector);
}

#undef SWAP
#undef ZERO

int GetInterval(int member, double bounds[]) //places data member into within appropriate bounds
{
	//for (int i=1; i<NUMINTERVALS+1; i++)
	//	cout << bounds[i] << endl;
	for (int i=1; i<NUMINTERVALS+1; i++)
		if (member <= bounds[i])
			return i-1;
	_ErrMsg("Error: data member out of bounds in determining paramters for temperatures--");
	return 9;
}

int GetInterval(double member, double bounds[]) //places data member into within appropriate bounds
{
	for (int i=1; i<NUMINTERVALS+1; i++)
		if (member <= bounds[i])
			return i-1;
	_ErrMsg("Error: data member out of bounds during parameter setting for temperatures.");
	return 9;
}

//void pause(void)
//{
//	char ch;
//	cout << "\nhit key to continue";
//	cin >> ch;
//}