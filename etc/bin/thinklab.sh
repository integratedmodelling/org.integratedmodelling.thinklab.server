#!/bin/sh

if [[ -z "JAVA_OPTS" ]]
	JAVA_OPTS="-Xms1024M -Xmx8196M -XX:MaxPermSize=256m -server"
fi

if [[ -z "THINKLAB_HOME" ]]
	export THINKLAB_HOME=/opt/thinklab
fi

# change the following two to define the REST root URL context and the port the server will run on
if [[ -z "THINKLAB_REST_CONTEXT" ]] 
	export THINKLAB_REST_CONTEXT="tl0"
fi

if [[ -z "THINKLAB_REST_PORT" ]]
	export THINKLAB_REST_PORT=8182
fi

# this points to the property file that configures the collaboration environment. If absent, the server will not serve collaboration functions,.
if [[ -z "THINKLAB_AUTH_PROPERTIES" ]]
	export THINKLAB_AUTH_PROPERTIES=/opt/collab/collab.properties
fi

case "$1" in
start)
  echo "Starting Thinklab."
  /usr/bin/sudo -u ${THINKLAB_USER} -H ${THINKLAB_HOME}/thinklab.sh
;;
stop)
  echo "Shutting down Thinklab."
  /usr/bin/wget -q --delete-after "http://HOSTADDRESS:PORT/sabnzbd/api?mode=shutdown&apikey=ENTERAPIKEYHERE"
;;
*)
  echo "Usage: $0 {start|stop}"
  exit 1
esac

exit 0