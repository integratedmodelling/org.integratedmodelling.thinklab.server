echo ----------------------------
java -Xms512M -Xmx${THINKLAB_MAX_MEMORY} -XX:MaxPermSize=256m -Djava.library.path="lib/ext/" -cp "lib/*" org.integratedmodelling.thinklab.main.RESTServer > thinklab.log
echo -----------------------------
echo Thinklab REST server application stopped

