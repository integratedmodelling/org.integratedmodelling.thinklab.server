@ECHO off
ECHO ----------------------------
java -Xms512M -Xmx%THINKLAB_MAX_MEMORY% -XX:MaxPermSize=256m -Djava.library.path="lib/ext/" -cp "lib/*" org.integratedmodelling.thinklab.main.RESTServer > thinklab.log
ECHO -----------------------------
ECHO Thinklab REST server application stopped

